
// KEURIG PROJECT

package SkavaPackage;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

import java.util.ArrayList;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;


public class Project1 {
	
	public static WebDriver driver;
	public static File newFile = new File("D:\\Viyantha\\Automation\\Selenium Test Report\\KEURIG\\Keurig_FileInput.xls"); // Input Data for the Application
	public static Workbook workbook;
	public static WritableWorkbook workbookcopy;
	public static Workbook book;  // Excel Workbook Object
	public static Sheet sheet1; 
	public static Sheet sheet2; 
	public static Sheet sheet3; 
	public static Sheet sheet4;
	public static Sheet sheet5;
	public static WritableSheet wsheet1;
	public static WritableSheet wsheet2;
	public static WritableSheet wsheet3;
	public static WritableSheet wsheet4;
	public static WritableSheet wsheet5;
	
	
	public static String idForTxtFile = new SimpleDateFormat("dd.MM.yyyy_HH.mm.ss").format(new Date());
	public static File file = new File("D:\\Viyantha\\Automation\\Selenium Test Report\\KEURIG\\KeurigOutputFile_" + idForTxtFile + ".txt");
	
	public static void main(String[] args) throws InterruptedException, IOException
	{
	
			try
			{
				HomePageNavigation();
				HomePageCSS();
				QualityPageCSS();
				VarietyPageNavigation();
				VarietyPageCSS();
				SustainabilityPage();
	
			}
			catch(Exception e)
			{
				System.out.println("Exception Is "+e);
				 
			}
	}
	
	@Test(priority = 0)
	
	public static void HomePageNavigation() throws InterruptedException, IOException, BiffException, RowsExceededException, WriteException {
		
		try{
			
		book = Workbook.getWorkbook(newFile);
		sheet2=book.getSheet("ExpectedURL'S");
		
		 // Write text on  text file
		
		FileWriter fw = new FileWriter(file, true);  
		BufferedWriter bw = new BufferedWriter(fw);
		
		workbook = Workbook.getWorkbook(new File("D:\\Viyantha\\Automation\\Selenium Test Report\\KEURIG\\Keurig_FileInput.xls"));
		workbookcopy = Workbook.createWorkbook(new File("D:\\Viyantha\\Automation\\Selenium Test Report\\KEURIG\\Keurig_FileOutput_" + idForTxtFile + ".xls"), workbook);
		
		wsheet2 = workbookcopy.getSheet(1);
		
		//DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		//capabilities.setCapability("chrome.binary","C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe");
		//System.setProperty("webdriver.chrome.driver", "C:\\Users\\viyantharani.v\\Downloads\\chromedriver (2).exe");
		//driver = new ChromeDriver(capabilities);
		
		driver = new FirefoxDriver();
		
		//DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
		//capabilities.setCapability("internetExplorer.binary","C:\\Program Files (x86)\\Internet Explorer\\iexplore.exe");
		//System.setProperty("webdriver.ie.driver", "C:\\Users\\viyantharani.v\\Downloads\\IEDriverServer_x64_2.53.0\\IEDriverServer.exe");
		//driver = new InternetExplorerDriver(capabilities);
		
		driver.manage().deleteAllCookies();
		String EHPurl = sheet2.getCell(1,1).getContents(); //column,row
		driver.get("https://www.greenmountaincoffee.com/"); // Keurig Production Home Page
		driver.manage().window().maximize();
		Thread.sleep(5000);
		String AHPurl = driver.getCurrentUrl();
		System.out.println("Expected Home Page URL is: "+EHPurl);
		System.out.println("Actual Home Page URL is: "+AHPurl);
		//Assert.assertEquals(AHPurl, "https://www.greenmountaincoffee.com/" );  //Actual,Expected - If Failed Execution Stops here
		if(EHPurl.equals(AHPurl))
		{
			System.out.println("Keurig URL is Opened Correctly - Pass");
			Label HPurlNAVP = new Label(2,1,"PASS"); 
			wsheet2.addCell(HPurlNAVP); 
		}
		else
		{
			System.out.println("Keurig URL is not Opened Correctly - Fail");
			bw.write("Keurig URL is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label HPurlNAVF = new Label(2,1,"FAIL"); 
			wsheet2.addCell(HPurlNAVF);
		}
			 
		// Header
		
		// Quality Link in Header
		
		driver.findElement(By.xpath(".//*[@id='id_skgm_hw_headerLayoutCell']/div[1]/div[2]/div/div[1]/a[1]/div")).click();  
		Thread.sleep(5000);
		String AQualityurl = driver.getCurrentUrl();
		System.out.println("Actual Quality Page URL is: "+AQualityurl);
		String EQualityurl = sheet2.getCell(1,4).getContents();
		System.out.println("Expected Quality Page URL is: "+EQualityurl);
		//Assert.assertEquals(AQualityurl, "https://www.greenmountaincoffee.com/quality" );
		if(EQualityurl.equals(AQualityurl))
		{
			System.out.println("Quality Page is Opened Correctly - Pass");
			Label QualityurlNAVP = new Label(2,4,"PASS"); 
			wsheet2.addCell(QualityurlNAVP); 
		}
		else
		{
			System.out.println("Quality Page is not Opened Correctly - Fail");
			bw.write("Quality Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label QualityurlNAVF = new Label(2,4,"FAIL"); 
			wsheet2.addCell(QualityurlNAVF);
		}
		driver.navigate().back();  // Browser Back
		Thread.sleep(10000);
		
		// Variety Link in Header
		
		driver.findElement(By.xpath(".//*[@id='id_skgm_hw_headerLayoutCell']/div[1]/div[2]/div/div[1]/a[2]/div")).click();  
		Thread.sleep(5000);
		String AVarietyurl = driver.getCurrentUrl();
		System.out.println("Actual Variety Page URL is: "+AVarietyurl);
		String EVarietyurl = sheet2.getCell(1,5).getContents();
		System.out.println("Expected Variety Page URL is: "+EVarietyurl);
		//Assert.assertEquals(AVarietyurl, "https://www.greenmountaincoffee.com/variety" );
		if(EVarietyurl.equals(AVarietyurl))
		{
			System.out.println("Variety Page is Opened Correctly - Pass");
			Label VarietyurlNAVP = new Label(2,5,"PASS"); 
			wsheet2.addCell(VarietyurlNAVP);
		}
		else
		{
			System.out.println("Variety Page is not Opened Correctly - Fail");
			bw.write("Variety Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label VarietyurlNAVF = new Label(2,5,"FAIL"); 
			wsheet2.addCell(VarietyurlNAVF);
		}
		driver.navigate().back();  // Browser Back
		Thread.sleep(5000);
	
		// Sustainability Link in Header
		
		driver.findElement(By.xpath(".//*[@id='id_skgm_hw_headerLayoutCell']/div[1]/div[2]/div/div[1]/a[3]/div")).click();  
		Thread.sleep(5000);
		String ASustainabilityurl = driver.getCurrentUrl();
		System.out.println("Actual Sustainability Page URL is: "+ASustainabilityurl);
		String ESustainabilityurl = sheet2.getCell(1,6).getContents();
		System.out.println("Expected Sustainability Page URL is: "+ESustainabilityurl);
		//Assert.assertEquals(ASustainabilityurl, "https://www.greenmountaincoffee.com/sustainability" );
		if(ESustainabilityurl.equals(ASustainabilityurl))
		{
			System.out.println("Sustainability Page is Opened Correctly - Pass");
			Label SustainabilityurlNAVP = new Label(2,6,"PASS"); 
			wsheet2.addCell(SustainabilityurlNAVP);
		}
		else
		{
			System.out.println("Sustainability Page is not Opened Correctly - Fail");
			bw.write("Sustainability Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label SustainabilityurlNAVF = new Label(2,6,"FAIL"); 
			wsheet2.addCell(SustainabilityurlNAVF);
		}
		driver.navigate().back();  // Browser Back
		Thread.sleep(5000);
			
		 // Facebook Icon in Header
		
		String winHandleBeforef = driver.getWindowHandle();
		driver.findElement(By.xpath(".//*[@id='id_skgm_hw_headerLayoutCell']/div[1]/div[2]/div/div[2]/div[3]")).click(); 
		Thread.sleep(5000);
		//Get the list of window handles
		ArrayList<String> tabsf = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(tabsf.size());
		//Use the list of window handles to switch between windows
	    driver.switchTo().window((String) tabsf.get(1));
		Thread.sleep(15000);
		String AFacebookurl = driver.getCurrentUrl();
		System.out.println("Actual Facebook Page URL is: "+AFacebookurl);
		String EFacebookurl = sheet2.getCell(1,7).getContents();
		System.out.println("Expected Facebook Page URL is: "+EFacebookurl);
		if(EFacebookurl.equals(AFacebookurl))
		{
			System.out.println("Facebook Page is Opened Correctly - Pass");
			Label FacebookurlNAVP = new Label(2,7,"PASS"); 
			wsheet2.addCell(FacebookurlNAVP);
		}
		else
		{
			System.out.println("Facebook Page is not Opened Correctly - Fail");
			bw.write("Facebook Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label FacebookurlNAVF = new Label(2,7,"FAIL"); 
			wsheet2.addCell(FacebookurlNAVF);
		}
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(winHandleBeforef); // Home Window
		
		 // Twitter Icon in Header
		
		String winHandleBeforet = driver.getWindowHandle();
		driver.findElement(By.xpath(".//*[@id='id_skgm_hw_headerLayoutCell']/div[1]/div[2]/div/div[2]/div[5]/div")).click(); 
		Thread.sleep(5000);
		//Get the list of window handles
		ArrayList<String> tabst = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(tabst.size());
		//Use the list of window handles to switch between windows
		driver.switchTo().window((String) tabst.get(1));
		Thread.sleep(15000);
		String ATwitterurl = driver.getCurrentUrl();
		System.out.println("Actual Twitter Page URL is: "+ATwitterurl);
		String ETwitterurl = sheet2.getCell(1,8).getContents();
		System.out.println("Expected Twitter Page URL is: "+ETwitterurl);
		if(ETwitterurl.equals(ATwitterurl))
		{
			System.out.println("Twitter Page is Opened Correctly - Pass");
			Label TwitterurlNAVP = new Label(2,8,"PASS"); 
			wsheet2.addCell(TwitterurlNAVP);
		}
		else
		{
			System.out.println("Twitter Page is not Opened Correctly - Fail");
			bw.write("Twitter Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label TwitterurlNAVF = new Label(2,8,"FAIL"); 
			wsheet2.addCell(TwitterurlNAVF);
		}
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(winHandleBeforet);
	
		// Pinterest Icon in Header
		
		String winHandleBeforep = driver.getWindowHandle();
		driver.findElement(By.xpath(".//*[@id='id_skgm_hw_headerLayoutCell']/div[1]/div[2]/div/div[2]/div[7]")).click();  
		Thread.sleep(5000);
		//Get the list of window handles
		ArrayList<String> tabsp = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(tabsp.size());
		//Use the list of window handles to switch between windows
		driver.switchTo().window((String) tabsp.get(1));
		Thread.sleep(15000);
		String APinteresturl = driver.getCurrentUrl();
		System.out.println("Actual Pinterest Page URL is: "+APinteresturl);
		String EPinteresturl = sheet2.getCell(1,9).getContents();
		System.out.println("Expected Pinterest Page URL is: "+EPinteresturl);
		if(EPinteresturl.equals(APinteresturl))
		{
			System.out.println("Pinterest Page is Opened Correctly - Pass");
			Label PinteresturlNAVP = new Label(2,9,"PASS"); 
			wsheet2.addCell(PinteresturlNAVP);
		}
		else
		{
			System.out.println("Pinterest Page is not Opened Correctly - Fail");
			bw.write("Pinterest Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label PinteresturlNAVF = new Label(2,9,"FAIL"); 
			wsheet2.addCell(PinteresturlNAVF);
		}
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(winHandleBeforep);
		
		 // Shop at Keurig.com in Header
		
		String winHandleBefores = driver.getWindowHandle();
		driver.findElement(By.xpath(".//*[@id='id_skgm_hw_headerLayoutCell']/div[1]/div[2]/div/div[2]/a/div[1]")).click(); 
		Thread.sleep(5000);
		//Get the list of window handles
		ArrayList<String> tabss = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(tabss.size());
		//Use the list of window handles to switch between windows
		driver.switchTo().window((String) tabss.get(1));
		Thread.sleep(15000);
		String AShopurl = driver.getCurrentUrl();
		System.out.println("Actual Shop at Keurig.com URL is: "+AShopurl);
		String EShopurl = sheet2.getCell(1,10).getContents();
		System.out.println("Expected Shop at Keurig.com URL is: "+EShopurl);
		if(EShopurl.equals(AShopurl))
		{
			System.out.println("Shop at Keurig.com Page is Opened Correctly - Pass");
			Label ShopurlNAVP = new Label(2,10,"PASS"); 
			wsheet2.addCell(ShopurlNAVP);
		}
		else
		{
			System.out.println("Shop at Keurig.com Page is not Opened Correctly - Fail");
			bw.write("Shop at Keurig.com Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label ShopurlNAVF = new Label(2,10,"FAIL"); 
			wsheet2.addCell(ShopurlNAVF);
		}
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(winHandleBefores);
		
		/*
		
		// Floating Header
		 
		// Quality Link in Header
		
		driver.findElement(By.xpath(".//*[@id='id_skgm_hw_headerLayoutCell']/div[1]/div[2]/div/div[1]/div[1]")).click();  
		Thread.sleep(5000);
		String FAQualityurl = driver.getCurrentUrl();
		System.out.println("Actual Quality Page URL is: "+FAQualityurl);
		String FEQualityurl = sheet2.getCell(1,4).getContents();
		System.out.println("Expected Quality Page URL is: "+FEQualityurl);
		//Assert.assertEquals(AQualityurl, "https://www.greenmountaincoffee.com/quality" );
		if(FEQualityurl.equals(FAQualityurl))
		{
			System.out.println("Quality Page is Opened Correctly - Pass");
			bw.write("Quality Page is Opened Correctly - Pass");  
			//Adding new line 
		    bw.newLine();
		}
		else
		{
			System.out.println("Quality Page is not Opened Correctly - Fail");
			bw.write("Quality Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		driver.navigate().back();  // Browser Back
		Thread.sleep(5000);
		
		// Variety Link in Header
		
		driver.findElement(By.xpath(".//*[@id='id_skgm_hw_headerLayoutCell']/div[1]/div[2]/div/div[1]/div[3]")).click();  
		Thread.sleep(5000);
		String FAVarietyurl = driver.getCurrentUrl();
		System.out.println("Actual Variety Page URL is: "+FAVarietyurl);
		String FEVarietyurl = sheet2.getCell(1,5).getContents();
		System.out.println("Expected Variety Page URL is: "+FEVarietyurl);
		//Assert.assertEquals(AVarietyurl, "https://www.greenmountaincoffee.com/variety" );
		if(FEVarietyurl.equals(FAVarietyurl))
		{
			System.out.println("Variety Page is Opened Correctly - Pass");
			bw.write("Variety Page is Opened Correctly - Pass");  
			//Adding new line 
		    bw.newLine();
		}
		else
		{
			System.out.println("Variety Page is not Opened Correctly - Fail");
			bw.write("Variety Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		driver.navigate().back();  // Browser Back
		Thread.sleep(5000);
		
		// Sustainability Link in Header
		
		driver.findElement(By.xpath(".//*[@id='id_skgm_hw_headerLayoutCell']/div[1]/div[2]/div/div[1]/div[5]")).click();  
		Thread.sleep(5000);
		String FASustainabilityurl = driver.getCurrentUrl();
		System.out.println("Actual Sustainability Page URL is: "+FASustainabilityurl);
		String FESustainabilityurl = sheet2.getCell(1,6).getContents();
		System.out.println("Expected Sustainability Page URL is: "+FESustainabilityurl);
		//Assert.assertEquals(ASustainabilityurl, "https://www.greenmountaincoffee.com/sustainability" );
		if(FESustainabilityurl.equals(FASustainabilityurl))
		{
			System.out.println("Sustainability Page is Opened Correctly - Pass");
			bw.write("Sustainability Page is Opened Correctly - Pass");  
			//Adding new line 
		    bw.newLine();
		}
		else
		{
			System.out.println("Sustainability Page is not Opened Correctly - Fail");
			bw.write("Sustainability Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		driver.navigate().back();  // Browser Back
		Thread.sleep(5000);
		
		// Facebook Icon in Header
			
		String FwinHandleBeforef = driver.getWindowHandle();
		driver.findElement(By.xpath(".//*[@id='id_skgm_hw_headerLayoutCell']/div[1]/div[2]/div/div[2]/div[3]")).click();  
		Thread.sleep(5000);
		//Get the list of window handles
		ArrayList<String> Ftabsf = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(Ftabsf.size());
		//Use the list of window handles to switch between windows
	    driver.switchTo().window((String) Ftabsf.get(1));
		Thread.sleep(10000);
		String FAFacebookurl = driver.getCurrentUrl();
		System.out.println("Actual Facebook Page URL is: "+FAFacebookurl);
		String FEFacebookurl = sheet2.getCell(1,7).getContents();
		System.out.println("Expected Facebook Page URL is: "+FEFacebookurl);
		if(FEFacebookurl.equals(FAFacebookurl))
		{
			System.out.println("Facebook Page is Opened Correctly - Pass");
			bw.write("Facebook Page is Opened Correctly - Pass");  
			//Adding new line 
		    bw.newLine();
		}
		else
		{
			System.out.println("Facebook Page is not Opened Correctly - Fail");
			bw.write("Facebook Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(FwinHandleBeforef); // Home Window
		
		// Twitter Icon in Header
		
		String FwinHandleBeforet = driver.getWindowHandle();
		driver.findElement(By.xpath(".//*[@id='id_skgm_hw_headerLayoutCell']/div[1]/div[2]/div/div[2]/div[5]/div")).click();  
		Thread.sleep(5000);
		//Get the list of window handles
		ArrayList<String> Ftabst = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(Ftabst.size());
		//Use the list of window handles to switch between windows
		driver.switchTo().window((String) Ftabst.get(1));
		Thread.sleep(15000);
		String FATwitterurl = driver.getCurrentUrl();
		System.out.println("Actual Twitter Page URL is: "+FATwitterurl);
		String FETwitterurl = sheet2.getCell(1,8).getContents();
		System.out.println("Expected Twitter Page URL is: "+FETwitterurl);
		if(FETwitterurl.equals(FATwitterurl))
		{
			System.out.println("Twitter Page is Opened Correctly - Pass");
			bw.write("Twitter Page is Opened Correctly - Pass");  
			//Adding new line 
		    bw.newLine();
		}
		else
		{
			System.out.println("Twitter Page is not Opened Correctly - Fail");
			bw.write("Twitter Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(FwinHandleBeforet);
		
		// Pinterest Icon in Header
		
		String FwinHandleBeforep = driver.getWindowHandle();
		driver.findElement(By.xpath(".//*[@id='id_skgm_hw_headerLayoutCell']/div[1]/div[2]/div/div[2]/div[7]")).click();  
		Thread.sleep(5000);
		//Get the list of window handles
		ArrayList<String> Ftabsp = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(Ftabsp.size());
		//Use the list of window handles to switch between windows
		driver.switchTo().window((String) Ftabsp.get(1));
		Thread.sleep(10000);
		String FAPinteresturl = driver.getCurrentUrl();
		System.out.println("Actual Pinterest Page URL is: "+FAPinteresturl);
		String FEPinteresturl = sheet2.getCell(1,9).getContents();
		System.out.println("Expected Pinterest Page URL is: "+FEPinteresturl);
		if(FEPinteresturl.equals(FAPinteresturl))
		{
			System.out.println("Pinterest Page is Opened Correctly - Pass");
			bw.write("Pinterest Page is Opened Correctly - Pass");  
			//Adding new line 
		    bw.newLine();
		}
		else
		{
			System.out.println("Pinterest Page is not Opened Correctly - Fail");
			bw.write("Pinterest Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(FwinHandleBeforep);
		
		// Shop at Keurig.com in Header
		
		String FwinHandleBefores = driver.getWindowHandle();
		driver.findElement(By.xpath(".//*[@id='id_skgm_hw_headerLayoutCell']/div[1]/div[2]/div/div[2]/a/div[1]")).click();  
		Thread.sleep(5000);
		//Get the list of window handles
		ArrayList<String> Ftabss = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(Ftabss.size());
		//Use the list of window handles to switch between windows
		driver.switchTo().window((String) Ftabss.get(1));
		Thread.sleep(5000);
		String FAShopurl = driver.getCurrentUrl();
		System.out.println("Actual Shop at Keurig.com URL is: "+FAShopurl);
		String FEShopurl = sheet2.getCell(1,10).getContents();
		System.out.println("Expected Shop at Keurig.com URL is: "+FEShopurl);
		if(FEShopurl.equals(FAShopurl))
		{
			System.out.println("Shop at Keurig.com Page is Opened Correctly - Pass");
			bw.write("Shop at Keurig.com Page is Opened Correctly - Pass");  
			//Adding new line 
		    bw.newLine();
		}
		else
		{
			System.out.println("Shop at Keurig.com Page is not Opened Correctly - Fail");
			bw.write("Shop at Keurig.com Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(FwinHandleBefores);
		
		*/
		
		 // Try a Sample Button
		
		driver.findElement(By.id("idSkcbannerTryButtonTxt")).click(); 
		Thread.sleep(5000);
		String ATrySampleurl = driver.getCurrentUrl();
		System.out.println("Actual Try a Sample Page URL is: "+ATrySampleurl);
		String ETrySampleurl = sheet2.getCell(1,12).getContents();
		System.out.println("Expected Try a Sample Page URL is: "+ETrySampleurl);
		if(ETrySampleurl.equals(ATrySampleurl))
		{
			System.out.println("Try a Sample Page is Opened Correctly - Pass");
			Label TrySampleurlNAVP = new Label(2,12,"PASS"); 
			wsheet2.addCell(TrySampleurlNAVP);
		}
		else
		{
			System.out.println("Try a Sample Page is not Opened Correctly - Fail");
			bw.write("Try a Sample Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label TrySampleurlNAVF = new Label(2,12,"FAIL"); 
			wsheet2.addCell(TrySampleurlNAVF);
		}
		driver.navigate().back();  // Browser Back
		Thread.sleep(5000);
		
		// Learn about our Keurig Brewers
		
		driver.findElement(By.id("idSkcbannerfreesampleLinkTxt")).click();  
		Thread.sleep(5000);
		
		// Learn More at Keurig.com Button
		
		String winHandleBeforeL = driver.getWindowHandle();
		driver.findElement(By.className("sk_cls_lmPopupLearnMore")).click();  
		Thread.sleep(5000);
		//Get the list of window handles
		ArrayList<String> tabsl = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(tabsl.size());
		//Use the list of window handles to switch between windows
		driver.switchTo().window((String) tabsl.get(1));
		Thread.sleep(5000);
		String ALearnmoreurl = driver.getCurrentUrl();
		System.out.println("Actual Learn More at Keurig.com Page URL is: "+ALearnmoreurl);
		String ELearnmoreurl = sheet2.getCell(1,13).getContents();
		System.out.println("Expected Try a Sample Page URL is: "+ELearnmoreurl);
		if(ELearnmoreurl.equals(ALearnmoreurl))
		{
			System.out.println("Learn More at Keurig.com Page is Opened Correctly - Pass");
			Label LearnmoreurlNAVP = new Label(2,13,"PASS"); 
			wsheet2.addCell(LearnmoreurlNAVP);
		}
		else
		{
			System.out.println("Learn More at Keurig.com Page is not Opened Correctly - Fail");
			bw.write("Learn More at Keurig.com Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label LearnmoreurlNAVF = new Label(2,13,"FAIL"); 
			wsheet2.addCell(LearnmoreurlNAVF);
		}
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(winHandleBeforeL);
		
		// Close Learn about our Keurig Brewers
		
		driver.findElement(By.className("sk_cls_closeButtonText")).click();  
		Thread.sleep(5000);
		
		// Watch TV Commercials Video
		
		driver.findElement(By.xpath(".//*[@id='idSkcbannerWatchVideoFreeSample']/div[1]/div")).click();  
		Thread.sleep(25000);
		
		// Close - Watch TV Commercials Video
		
		driver.findElement(By.xpath(".//*[@id='id_skbcVdoClsBtn']/div[2]")).click();  
		Thread.sleep(5000);
		
		// Quality Video
		
		driver.findElement(By.xpath(".//*[@id='sk_id_quality']/div[2]/img")).click();  
		Thread.sleep(25000);
		
		// Close - Quality Video
		
		driver.findElement(By.xpath(".//*[@id='id_skbcVdoClsBtn']")).click();  
		Thread.sleep(5000);
		
		// FireFox
		
		JavascriptExecutor jsemq = (JavascriptExecutor)driver;
		jsemq.executeScript("window.scrollBy(0,250)", "");
		Thread.sleep(5000);
		
		// Chrome
		
		//JavascriptExecutor jsemq = (JavascriptExecutor)driver;
		//jsemq.executeScript("window.scrollBy(0,300)", "");
		//Thread.sleep(5000);
		
		// More about Quality Button
		
		driver.findElement(By.className("sk_quality_more_text")).click(); 
		Thread.sleep(5000);
		String AMoreQualityurl = driver.getCurrentUrl();
		System.out.println("Actual More about Quality Page URL is: "+AMoreQualityurl);
		String EMoreQualityurl = sheet2.getCell(1,14).getContents();
		System.out.println("Expected More about Quality Page URL is: "+EMoreQualityurl);
		if(EMoreQualityurl.equals(AMoreQualityurl))
		{
			System.out.println("More about Quality Page is Opened Correctly - Pass");
			Label MoreQualityurlNAVP = new Label(2,14,"PASS"); 
			wsheet2.addCell(MoreQualityurlNAVP);
		}
		else
		{
			System.out.println("More about Quality Page is not Opened Correctly - Fail");
			bw.write("More about Quality Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label MoreQualityurlNAVF = new Label(2,14,"FAIL"); 
			wsheet2.addCell(MoreQualityurlNAVF);
		}
		driver.navigate().back();  // Browser Back
		
		// FireFox
		Thread.sleep(10000);
		
		// Chrome
		//Thread.sleep(13000);
		
		// Quality Number tab 2
		// Used Absolute X-Path
		
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[4]/div/div[2]/div[2]/div[2]")).click();  
		Thread.sleep(5000);
		
		// Quality Number tab 3
		
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[4]/div/div[2]/div[2]/div[3]")).click();  
		Thread.sleep(5000);
		
		// Quality Number tab 1
		
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[4]/div/div[2]/div[2]/div[1]")).click();  
		Thread.sleep(5000);

		JavascriptExecutor jsevv = (JavascriptExecutor)driver;
		jsevv.executeScript("window.scrollBy(0,250)", "");
		Thread.sleep(10000);
		
		WebDriverWait waitvarietyvideo = new WebDriverWait(driver, 15);
		waitvarietyvideo.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='sk_id_variety']/div[2]/img")));
		// Variety Video
		driver.findElement(By.xpath(".//*[@id='sk_id_variety']/div[2]/img")).click();  
		Thread.sleep(25000);
		
		// Close - Variety Video
		
		driver.findElement(By.id("id_skbcVdoClsBtn")).click();  
		Thread.sleep(10000);
		
		// FireFox - Comment and Chrome - Uncomment
		
		//JavascriptExecutor jsevf = (JavascriptExecutor)driver;
		//jsevf.executeScript("window.scrollBy(0,800)", "");
		//Thread.sleep(10000);
		
		driver.findElement(By.xpath(".//*[@id='id_ourCoffScroller']/div[3]/img")).click();  // Variety - Our Coffees - Decaf
		Thread.sleep(8000);
		
		driver.findElement(By.xpath(".//*[@id='id_ourCoffeeSubCon']/div[3]")).click();  // Variety - Our Coffees - Arrow Symbol
		Thread.sleep(8000);
		
		driver.findElement(By.xpath(".//*[@id='id_ourCoffScroller']/div[5]/img")).click();  // Variety - Our Coffees - Coffeehouse
		Thread.sleep(8000);
		
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[6]/div/div[3]/div/div[5]/div")).click(); // More about Variety Button
		Thread.sleep(10000);
		String AMoreVarietyurl = driver.getCurrentUrl();
		System.out.println("Actual More about Variety Page URL is: "+AMoreVarietyurl);
		String EMoreVarietyurl = sheet2.getCell(1,15).getContents();
		System.out.println("Expected More about Variety Page URL is: "+AMoreVarietyurl);
		if(EMoreVarietyurl.equals(AMoreVarietyurl))
		{
			System.out.println("More about Variety Page is Opened Correctly - Pass");
			Label MoreVarietyurlNAVP = new Label(2,15,"PASS"); 
			wsheet2.addCell(MoreVarietyurlNAVP);
		}
		else
		{
			System.out.println("More about Variety Page is not Opened Correctly - Fail");
			bw.write("More about Variety Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label MoreVarietyurlNAVF = new Label(2,15,"FAIL"); 
			wsheet2.addCell(MoreVarietyurlNAVF);
		}
		driver.navigate().back();  // Browser Back
		Thread.sleep(15000);
		
		JavascriptExecutor jsesv = (JavascriptExecutor)driver;
		jsesv.executeScript("window.scrollBy(0,900)", "");
		Thread.sleep(15000);
		
		driver.findElement(By.xpath(".//*[@id='sk_id_responsibility']/div[2]/img")).click();  // Sustainability Video
		Thread.sleep(25000);
		
		driver.findElement(By.xpath(".//*[@id='id_skbcVdoClsBtn']")).click();  // Close -Sustainability Video
		Thread.sleep(15000);
		
		JavascriptExecutor jsess = (JavascriptExecutor)driver;
		jsess.executeScript("window.scrollBy(0,600)", "");
		Thread.sleep(15000);
		
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[8]/div/div[2]/div[2]/div[2]")).click();  // Sustainability Number tab 2
		Thread.sleep(5000);
		
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[8]/div/div[2]/div[2]/div[3]")).click();  // Sustainability Number tab 3
		Thread.sleep(5000);
		
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[8]/div/div[2]/div[2]/div[1]")).click();  // Sustainability Number tab 1
		Thread.sleep(10000);
		
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[8]/div/div[1]/div/div[5]/div")).click(); // More about Sustainability Button
		Thread.sleep(5000);
		String AMoreSustainabilityurl = driver.getCurrentUrl();
		System.out.println("Actual More about Sustainability Page URL is: "+AMoreSustainabilityurl);
		String EMoreSustainabilityurl = sheet2.getCell(1,16).getContents();
		System.out.println("Expected More about Sustainability Page URL is: "+EMoreSustainabilityurl);
		if(EMoreSustainabilityurl.equals(AMoreSustainabilityurl))
		{
			System.out.println("More about Sustainability Page is Opened Correctly - Pass");
			Label MoreSustainabilityurlNAVP = new Label(2,16,"PASS"); 
			wsheet2.addCell(MoreSustainabilityurlNAVP);
		}
		else
		{
			System.out.println("More about Sustainability Page is not Opened Correctly - Fail");
			bw.write("More about Sustainability Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label MoreSustainabilityurlNAVF = new Label(2,16,"FAIL"); 
			wsheet2.addCell(MoreSustainabilityurlNAVF);
		}
		driver.navigate().back();  // Browser Back
		Thread.sleep(5000);
		
		JavascriptExecutor jseft = (JavascriptExecutor)driver;
		jseft.executeScript("window.scrollBy(0,250)", "");
		Thread.sleep(15000);
	
		// Footer 
		
		driver.findElement(By.xpath(".//*[@id='id_skgm_fw_footerLayoutCell']/div/div[2]/div[1]/div/a[3]/div")).click();  // Variety Link in Footer
		Thread.sleep(5000);
		String AVarietyurlF = driver.getCurrentUrl();
		System.out.println("Footer - Actual Variety Page URL is: "+AVarietyurlF);
		String EVarietyurlF = sheet2.getCell(1,5).getContents();
		System.out.println("Footer - Expected Variety Page URL is: "+EVarietyurlF);
		if(EVarietyurlF.equals(AVarietyurlF))
		{
			System.out.println("Variety Page is Opened Correctly in Footer - Pass");
			Label VarietyurlFNAVP = new Label(2,20,"PASS"); 
			wsheet2.addCell(VarietyurlFNAVP);
		}
		else
		{
			System.out.println("Variety Page is not Opened Correctly in Footer - Fail");
			bw.write("Variety Page is not Opened Correctly in Footer - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label VarietyurlFNAVF = new Label(2,20,"FAIL"); 
			wsheet2.addCell(VarietyurlFNAVF);
		}
		driver.navigate().back();  // Browser Back
		Thread.sleep(5000);
		
		driver.findElement(By.xpath(".//*[@id='id_skgm_fw_footerLayoutCell']/div/div[2]/div[1]/div/a[4]/div")).click();  // Sustainability Link in Footer
		Thread.sleep(5000);
		String ASustainabilityurlF = driver.getCurrentUrl();
		System.out.println("Footer - Actual Sustainability Page URL is: "+ASustainabilityurlF);
		String ESustainabilityurlF = sheet2.getCell(1,6).getContents();
		System.out.println("Footer - Expected Sustainability Page URL is: "+ESustainabilityurlF);
		if(ESustainabilityurlF.equals(ASustainabilityurlF))
		{
			System.out.println("Sustainability Page is Opened Correctly in Footer - Pass");
			Label SustainabilityurlFNAVP = new Label(2,21,"PASS"); 
			wsheet2.addCell(SustainabilityurlFNAVP);
		}
		else
		{
			System.out.println("Sustainability Page is not Opened Correctly in Footer - Fail");
			bw.write("Sustainability Page is not Opened Correctly in Footer - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label SustainabilityurlFNAVF = new Label(2,21,"FAIL"); 
			wsheet2.addCell(SustainabilityurlFNAVF);
		}
		driver.navigate().back();  // Browser Back
		Thread.sleep(5000);
		
		String FwinHandleBeforefb = driver.getWindowHandle();
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[9]/div/div/div[2]/div/div[2]/div[2]/div[2]/div[2]/div")).click();  // Facebook Icon in Footer
		Thread.sleep(5000);
		//Get the list of window handles
		ArrayList<String> Ftabsfb = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(Ftabsfb.size());
		//Use the list of window handles to switch between windows
	    driver.switchTo().window((String) Ftabsfb.get(1));
		Thread.sleep(15000);
		String FBAFacebookurl = driver.getCurrentUrl();
		System.out.println("Footer - Actual Facebook Page URL is: "+FBAFacebookurl);
		String FBEFacebookurl = sheet2.getCell(1,22).getContents();
		System.out.println("Footer - Expected Facebook Page URL is: "+FBEFacebookurl);
		if(FBEFacebookurl.equals(FBAFacebookurl))
		{
			System.out.println("Footer - Facebook Page is Opened Correctly - Pass");
			Label FFBAFacebookurlNAVP = new Label(2,22,"PASS"); 
			wsheet2.addCell(FFBAFacebookurlNAVP);
		}
		else
		{
			System.out.println("Footer - Facebook Page is not Opened Correctly - Fail");
			bw.write("Footer - Facebook Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label FFBAFacebookurlNAVF = new Label(2,22,"FAIL"); 
			wsheet2.addCell(FFBAFacebookurlNAVF);
		}
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(FwinHandleBeforefb); // Home Window
		
		String FwinHandleBeforetf = driver.getWindowHandle();
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[9]/div/div/div[2]/div/div[2]/div[2]/div[2]/div[3]/div")).click();  // Twitter Icon in Footer
		Thread.sleep(5000);
		//Get the list of window handles
		ArrayList<String> Ftabstf = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(Ftabstf.size());
		//Use the list of window handles to switch between windows
		driver.switchTo().window((String) Ftabstf.get(1));
		Thread.sleep(20000);
		String FATwitterurlf = driver.getCurrentUrl();
		System.out.println("Footer - Actual Twitter Page URL is: "+FATwitterurlf);
		String FETwitterurlf = sheet2.getCell(1,23).getContents();
		System.out.println("Footer - Expected Twitter Page URL is: "+FETwitterurlf);
		if(FETwitterurlf.equals(FATwitterurlf))
		{
			System.out.println("Footer - Twitter Page is Opened Correctly - Pass");
			Label FATwitterurlfNAVP = new Label(2,23,"PASS"); 
			wsheet2.addCell(FATwitterurlfNAVP);
		}
		else
		{
			System.out.println("Twitter Page is not Opened Correctly - Fail");
			bw.write("Twitter Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label FATwitterurlfNAVF = new Label(2,23,"FAIL"); 
			wsheet2.addCell(FATwitterurlfNAVF);
		}
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(FwinHandleBeforetf);
		
		String FwinHandleBeforei = driver.getWindowHandle();
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[9]/div/div/div[2]/div/div[2]/div[2]/div[2]/div[4]/div")).click();  // Instagram Icon in Footer
		Thread.sleep(5000);
		//Get the list of window handles
		ArrayList<String> Ftabsi = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(Ftabsi.size());
		//Use the list of window handles to switch between windows
		driver.switchTo().window((String) Ftabsi.get(1));
		Thread.sleep(15000);
		String Ainstagramurli = driver.getCurrentUrl();
		System.out.println(" Footer - Actual Instagram Page URL is: "+Ainstagramurli);
		String Einstagramurli = sheet2.getCell(1,24).getContents();
		System.out.println(" Footer - Expected Instagram Page URL is: "+Einstagramurli);
		if(Einstagramurli.equals(Ainstagramurli))
		{
			System.out.println("Footer - Instagram Page is Opened Correctly - Pass");
			Label FAinstagramurliNAVP = new Label(2,24,"PASS"); 
			wsheet2.addCell(FAinstagramurliNAVP);
		}
		else
		{
			System.out.println("Footer - Instagram Page is not Opened Correctly - Fail");
			bw.write("Footer - Instagram Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label FAinstagramurliNAVF = new Label(2,24,"FAIL"); 
			wsheet2.addCell(FAinstagramurliNAVF);
		}
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(FwinHandleBeforei);
		
		String winHandleBeforetou = driver.getWindowHandle();
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[9]/div/div/div[2]/div/div[3]/div/div[1]/a/div")).click();  // Terms of Use Link in Footer
		Thread.sleep(5000);
		//Get the list of window handles
		ArrayList<String> Ftabstou = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(Ftabstou.size());
		//Use the list of window handles to switch between windows
	    driver.switchTo().window((String) Ftabstou.get(1));
		Thread.sleep(15000);
		String ATermsofUseurl = driver.getCurrentUrl();
		System.out.println("Footer - Actual Terms of Use Page URL is: "+ATermsofUseurl);
		String ETermsofUseurl = sheet2.getCell(1,25).getContents();
		System.out.println("Footer - Expected Terms of Use Page URL is: "+ETermsofUseurl);
		if(ETermsofUseurl.equals(ATermsofUseurl))
		{
			System.out.println("Footer - Terms of Use Page is Opened Correctly - Pass");
			Label TermsofUseurlNAVP = new Label(2,25,"PASS"); 
			wsheet2.addCell(TermsofUseurlNAVP);
		}
		else
		{
			System.out.println("Footer - Terms of Use Page is not Opened Correctly - Fail");
			bw.write("Footer - Terms of Use Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label TermsofUseurlNAVF = new Label(2,25,"FAIL"); 
			wsheet2.addCell(TermsofUseurlNAVF);
		}
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(winHandleBeforetou); // Home Window
		
		String winHandleBeforepp = driver.getWindowHandle();
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[9]/div/div/div[2]/div/div[3]/div/div[2]/a/div")).click();  // Privacy Link in Footer
		Thread.sleep(5000);
		//Get the list of window handles
		ArrayList<String> Ftabspp = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(Ftabspp.size());
		//Use the list of window handles to switch between windows
	    driver.switchTo().window((String) Ftabspp.get(1));
		Thread.sleep(15000);
		String Aprivacyurl = driver.getCurrentUrl();
		System.out.println("Footer - Actual Privacy Page URL is: "+Aprivacyurl);
		String Eprivacyurl = sheet2.getCell(1,26).getContents();
		System.out.println("Footer - Expected Privacy Page URL is: "+Eprivacyurl);
		if(Eprivacyurl.equals(Aprivacyurl))
		{
			System.out.println("Footer - Privacy Page is Opened Correctly - Pass");
			Label privacyurlNAVP = new Label(2,26,"PASS"); 
			wsheet2.addCell(privacyurlNAVP);
		}
		else
		{
			System.out.println("Footer - Privacy Page is not Opened Correctly - Fail");
			bw.write("Footer - Privacy Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label privacyurlNAVF = new Label(2,26,"FAIL"); 
			wsheet2.addCell(privacyurlNAVF);
		}
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(winHandleBeforepp); // Home Window
		
		// Contact Us Link in Footer
		
		// driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[9]/div/div/div[2]/div/div[3]/div/div[3]/a/div")).click();  
		// Thread.sleep(5000);
		
		String winHandleBeforeca = driver.getWindowHandle();
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[9]/div/div/div[2]/div/div[3]/div/div[4]/a/div")).click();  // CA Transparency Act Link in Footer
		Thread.sleep(5000);
		//Get the list of window handles
		ArrayList<String> Ftabsca = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(Ftabsca.size());
		//Use the list of window handles to switch between windows
	    driver.switchTo().window((String) Ftabsca.get(1));
		Thread.sleep(15000);
		String ACAurl = driver.getCurrentUrl();
		System.out.println("Footer - Actual CA Transparency Act Page URL is: "+ACAurl);
		String ECAurl = sheet2.getCell(1,28).getContents();
		System.out.println("Footer - Expected CA Transparency Act Page URL is: "+ECAurl);
		if(ECAurl.equals(ACAurl))
		{
			System.out.println("Footer - CA Transparency Act Page is Opened Correctly - Pass");
			Label CAurlNAVP = new Label(2,28,"PASS"); 
			wsheet2.addCell(CAurlNAVP);
		}
		else
		{
			System.out.println("Footer - CA Transparency Act Page is not Opened Correctly - Fail");
			bw.write("Footer - CA Transparency Act Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label CAurlNAVF = new Label(2,28,"FAIL"); 
			wsheet2.addCell(CAurlNAVF);
		}
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(winHandleBeforeca); // Home Window
		
		
		driver.findElement(By.xpath(".//*[@id='id_skgm_fw_footerLayoutCell']/div/div[2]/div[1]/div/a[2]/div")).click();  // Quality Link in Footer
		Thread.sleep(5000);
		String AQualityurlF = driver.getCurrentUrl();
		System.out.println("Footer - Actual Quality Page URL is: "+AQualityurlF);
		String EQualityurlF = sheet2.getCell(1,4).getContents();
		System.out.println("Footer - Expected Quality Page URL is: "+EQualityurlF);
		if(EQualityurlF.equals(AQualityurlF))
		{
			System.out.println("Quality Page is Opened Correctly in Footer - Pass");
			Label QualityurlFNAVP = new Label(2,19,"PASS"); 
			wsheet2.addCell(QualityurlFNAVP);
		}
		else
		{
			System.out.println("Quality Page is not Opened Correctly in Footer - Fail");
			bw.write("Quality Page is not Opened Correctly in Footer - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label QualityurlFNAVF = new Label(2,19,"FAIL"); 
			wsheet2.addCell(QualityurlFNAVF);
		}
		
		// GMC Logo in Quality Page
		
		driver.findElement(By.xpath("html/body/div[3]/div[1]/div[2]/div/div[3]/a/div")).click(); 
		Thread.sleep(10000);
	
		bw.close();
		
		
		//Thread.sleep(10000);
		//workbookcopy.write();
		//workbookcopy.close();
		//workbook.close();
		
		//driver.close();
		//driver.quit();
		
		}
		
		catch(Exception e)
		{
			System.out.println("Exception Is "+e);
			 
		}
		
	}
		
    @Test(priority = 1)
	
	public static void HomePageCSS() throws InterruptedException, BiffException, IOException, RowsExceededException, WriteException {	
    	
    	try{
    		
    	book = Workbook.getWorkbook(newFile);
		sheet1=book.getSheet("HomePage");
		
		//workbook = Workbook.getWorkbook(new File("D:\\Viyantha\\Automation\\Selenium Test Report\\KEURIG\\Keurig_FileInput.xls"));
		//workbookcopy = Workbook.createWorkbook(new File("D:\\Viyantha\\Automation\\Selenium Test Report\\KEURIG\\Keurig_FileOutput_" + idForTxtFile + ".xls"), workbook);
		
		wsheet1 = workbookcopy.getSheet(0);
		
		// Write text on  text file
		
		FileWriter fw = new FileWriter(file, true);  
		BufferedWriter bw = new BufferedWriter(fw);
		
    	// Quality Link in Header
    	
    	WebElement QualityHP = driver.findElement(By.xpath(".//*[@id='id_skgm_hw_headerLayoutCell']/div[1]/div[2]/div/div[1]/a[1]/div")); 
    	String QualityHPColor = QualityHP.getCssValue("color");
		System.out.println("Actual Color of Quality Text in Home Page is : "	+ QualityHPColor );
		// Convert RGB to Hex Value
		Color QualityHHP = Color.fromString(QualityHPColor);
		String HexQualityHPColor = QualityHHP.asHex();
		System.out.println("Actual Hex Color of Quality Text in Home Page is : "	+ HexQualityHPColor );
	
		String QualityHPFontSize = QualityHP.getCssValue("font-size");
		System.out.println("Actual Font Size of Quality Text in Home Page is : "	+ QualityHPFontSize);
		String QualityHPFontFamily = QualityHP.getCssValue("font-family");
		System.out.println("Actual Font Family of Quality Text in Home Page is : "	+ QualityHPFontFamily);
		
		String ExpQualityHPColor = sheet1.getCell(1,2).getContents(); //column,row
		System.out.println("Expected Color of Quality Text in Home Page is : "	+ ExpQualityHPColor );
		String ExpQualityHPFontSize = sheet1.getCell(1,3).getContents();
		System.out.println("Expected Font Size of Quality Text in Home Page is : "	+ ExpQualityHPFontSize);
		String ExpQualityHPFontFamily = sheet1.getCell(1,4).getContents();
		System.out.println("Expected Font Family of Quality Text in Home Page is : "	+ ExpQualityHPFontFamily);
		
		if(HexQualityHPColor.equals(ExpQualityHPColor))
		{
			System.out.println("Quality Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Quality Home Page Color is not in match with creative - Fail");
			bw.write("Quality Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(QualityHPFontSize.equals(ExpQualityHPFontSize))
		{
			System.out.println("Quality Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Quality Home Page Font Size is not in match with creative - Fail");
			bw.write("Quality Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(QualityHPFontFamily.equals(ExpQualityHPFontFamily))
		{
			System.out.println("Quality Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Quality Home Page Font Family is not in match with creative - Fail");
			bw.write("Quality Home Page Color is Font Family in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexQualityHPColor.equals(ExpQualityHPColor) && QualityHPFontSize.equals(ExpQualityHPFontSize) && QualityHPFontFamily.equals(ExpQualityHPFontFamily))
		{
			Label QualityHPCSSP = new Label(1,5,"PASS"); 
			wsheet1.addCell(QualityHPCSSP); 
		}
		else
		{
			Label QualityHPCSSF = new Label(1,5,"FAIL"); 
			wsheet1.addCell(QualityHPCSSF); 
		}
		
		// Variety Link in Header
		
		Thread.sleep(3000);
    	WebElement VarietyHP = driver.findElement(By.xpath(".//*[@id='id_skgm_hw_headerLayoutCell']/div[1]/div[2]/div/div[1]/a[2]/div")); 
    	String VarietyHPColor = VarietyHP.getCssValue("color");
		System.out.println("Actual Color of Variety Text in Home Page is : "	+ VarietyHPColor );
		// Convert RGB to Hex Value
		Color VarietyHHP = Color.fromString(VarietyHPColor);
		String HexVarietyHPColor = VarietyHHP.asHex();
		System.out.println("Actual Hex Color of Variety Text in Home Page is : "	+ HexVarietyHPColor );
		
		String VarietyHPFontSize = VarietyHP.getCssValue("font-size");
		System.out.println("Actual Font Size of Variety Text in Home Page is : "	+ VarietyHPFontSize);
		String VarietyHPFontFamily = VarietyHP.getCssValue("font-family");
		System.out.println("Actual Font Family of Variety Text in Home Page is : "	+ VarietyHPFontFamily);
		
		String ExpVarietyHPColor = sheet1.getCell(2,2).getContents(); //column,row
		System.out.println("Expected Color of Variety Text in Home Page is : "	+ ExpVarietyHPColor );
		String ExpVarietyHPFontSize = sheet1.getCell(2,3).getContents();
		System.out.println("Expected Font Size of Variety Text in Home Page is : "	+ ExpVarietyHPFontSize);
		String ExpVarietyHPFontFamily = sheet1.getCell(2,4).getContents();
		System.out.println("Expected Font Family of Variety Text in Home Page is : "	+ ExpVarietyHPFontFamily);
		
		if(HexVarietyHPColor.equals(ExpVarietyHPColor))
		{
			System.out.println("Variety Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Variety Home Page Color is not in match with creative - Fail");
			bw.write("Variety Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(VarietyHPFontSize.equals(ExpVarietyHPFontSize))
		{
			System.out.println("Variety Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Variety Home Page Font Size is not in match with creative - Fail");
			bw.write("Variety Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(VarietyHPFontFamily.equals(ExpVarietyHPFontFamily))
		{
			System.out.println("Variety Home Page Font Family is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("Variety Home Page Font Family is not in match with creative - Fail");
			bw.write("Variety Home Page Color is Font Family in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexVarietyHPColor.equals(ExpVarietyHPColor) && VarietyHPFontSize.equals(ExpVarietyHPFontSize) && VarietyHPFontFamily.equals(ExpVarietyHPFontFamily))
		{
			Label VarietyHPCSSP = new Label(2,5,"PASS"); 
			wsheet1.addCell(VarietyHPCSSP); 
		}
		else
		{
			Label VarietyHPCSSF = new Label(2,5,"FAIL"); 
			wsheet1.addCell(VarietyHPCSSF); 
		}
		
		// Sustainability Link in Header
    	
		Thread.sleep(3000);
		WebElement SustainabilityHP = driver.findElement(By.xpath(".//*[@id='id_skgm_hw_headerLayoutCell']/div[1]/div[2]/div/div[1]/a[3]/div")); 
    	String SustainabilityHPColor = SustainabilityHP.getCssValue("color");
		System.out.println("Actual Color of Sustainability Text in Home Page is : "	+ SustainabilityHPColor );
		// Convert RGB to Hex Value
		Color SustainabilityHHP = Color.fromString(SustainabilityHPColor);
		String HexSustainabilityHPColor = SustainabilityHHP.asHex();
		System.out.println("Actual Hex Color of Sustainability Text in Home Page is : "	+ HexSustainabilityHPColor );
		
		String SustainabilityHPFontSize = SustainabilityHP.getCssValue("font-size");
		System.out.println("Actual Font Size of Sustainability Text in Home Page is : "	+ SustainabilityHPFontSize);
		String SustainabilityHPFontFamily = SustainabilityHP.getCssValue("font-family");
		System.out.println("Actual Font Family of Sustainability Text in Home Page is : "	+ SustainabilityHPFontFamily);
		
		String ExpSustainabilityHPColor = sheet1.getCell(3,2).getContents(); //column,row
		System.out.println("Expected Color of Sustainability Text in Home Page is : "	+ ExpSustainabilityHPColor );
		String ExpSustainabilityHPFontSize = sheet1.getCell(3,3).getContents();
		System.out.println("Expected Font Size of Sustainability Text in Home Page is : "	+ ExpSustainabilityHPFontSize);
		String ExpSustainabilityHPFontFamily = sheet1.getCell(3,4).getContents();
		System.out.println("Expected Font Family of Sustainability Text in Home Page is : "	+ ExpSustainabilityHPFontFamily);
		
		if(HexSustainabilityHPColor.equals(ExpSustainabilityHPColor))
		{
			System.out.println("Sustainability Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Sustainability Home Page Color is not in match with creative - Fail");
			bw.write("Sustainability Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(SustainabilityHPFontSize.equals(ExpSustainabilityHPFontSize))
		{
			System.out.println("Sustainability Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Sustainability Home Page Font Size is not in match with creative - Fail");
			bw.write("Sustainability Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(SustainabilityHPFontFamily.equals(ExpSustainabilityHPFontFamily))
		{
			System.out.println("Sustainability Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Sustainability Home Page Font Family is not in match with creative - Fail");
			bw.write("Sustainability Home Page Color is Font Family in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexSustainabilityHPColor.equals(ExpSustainabilityHPColor) && SustainabilityHPFontSize.equals(ExpSustainabilityHPFontSize) && SustainabilityHPFontFamily.equals(ExpSustainabilityHPFontFamily))
		{
			Label SustainabilityHPCSSP = new Label(3,5,"PASS"); 
			wsheet1.addCell(SustainabilityHPCSSP); 
		}
		else
		{
			Label SustainabilityHPCSSF = new Label(3,5,"FAIL"); 
			wsheet1.addCell(SustainabilityHPCSSF); 
		}
		
		// Share Text in Header
    	
		Thread.sleep(3000);
		WebElement ShareHP = driver.findElement(By.className("skgm_hw_shareLbl")); 
    	String ShareHPColor = ShareHP.getCssValue("color");
		System.out.println("Actual Color of Share Text in Home Page is : "	+ ShareHPColor );
		// Convert RGB to Hex Value
		Color ShareHHP = Color.fromString(ShareHPColor);
		String HexShareHPColor = ShareHHP.asHex();
		System.out.println("Actual Hex Color of Share Text in Home Page is : "	+ HexShareHPColor );
		
		String ShareHPFontSize = ShareHP.getCssValue("font-size");
		System.out.println("Actual Font Size of Share Text in Home Page is : "	+ ShareHPFontSize);
		String ShareHPFontFamily = ShareHP.getCssValue("font-family");
		System.out.println("Actual Font Family of Share Text in Home Page is : "	+ ShareHPFontFamily);
		
		String ExpShareHPColor = sheet1.getCell(4,2).getContents(); //column,row
		System.out.println("Expected Color of Share Text in Home Page is : "	+ ExpShareHPColor );
		String ExpShareHPFontSize = sheet1.getCell(4,3).getContents();
		System.out.println("Expected Font Size of Share Text in Home Page is : "	+ ExpShareHPFontSize);
		String ExpShareHPFontFamily = sheet1.getCell(4,4).getContents();
		System.out.println("Expected Font Family of Share Text in Home Page is : "	+ ExpShareHPFontFamily);
		
		if(HexShareHPColor.equals(ExpShareHPColor))
		{
			System.out.println("Share Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Share Home Page Color is not in match with creative - Fail");
			bw.write("Share Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(ShareHPFontSize.equals(ExpShareHPFontSize))
		{
			System.out.println("Share Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Share Home Page Font Size is not in match with creative - Fail");
			bw.write("Share Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(ShareHPFontFamily.equals(ExpShareHPFontFamily))
		{
			System.out.println("Share Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Share Home Page Font Family is not in match with creative - Fail");
			bw.write("Share Home Page Color is Font Family in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexShareHPColor.equals(ExpShareHPColor) && ShareHPFontSize.equals(ExpShareHPFontSize) && ShareHPFontFamily.equals(ExpShareHPFontFamily))
		{
			Label ShareHPCSSP = new Label(4,5,"PASS"); 
			wsheet1.addCell(ShareHPCSSP); 
		}
		else
		{
			Label ShareHPCSSF = new Label(4,5,"FAIL"); 
			wsheet1.addCell(ShareHPCSSF); 
		}
		
		// FaceBook Share Icon in Header
		
		Thread.sleep(3000);
		WebElement ShareFBHP = driver.findElement(By.xpath(".//*[@id='id_skgm_hw_headerLayoutCell']/div[1]/div[2]/div/div[2]/div[3]")); 
		String ShareFBHPFontFamily = ShareFBHP.getCssValue("font-family");
		System.out.println("Actual Font Family of Share FaceBook Icon in Home Page is : "	+ ShareFBHPFontFamily);
		String ExpShareFBHPFontFamily = sheet1.getCell(5,4).getContents();
		System.out.println("Expected Font Family of Share FaceBook Icon in Home Page is : "	+ ExpShareFBHPFontFamily);

		if(ShareFBHPFontFamily.equals(ExpShareFBHPFontFamily))
		{
			System.out.println("Share FaceBook Home Page Font Family is in match with creative - Pass");
			Label ShareFBHPCSSP = new Label(5,5,"PASS"); 
			wsheet1.addCell(ShareFBHPCSSP);
		}
		else
		{
			System.out.println("Share FaceBook Home Page Font Family is not in match with creative - Fail");
			bw.write("Share FaceBook Home Page Color is Font Family in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label ShareFBHPCSSF = new Label(5,5,"FAIL"); 
			wsheet1.addCell(ShareFBHPCSSF);
		}
		
		// Twitter Share Icon in Header
		
		Thread.sleep(3000);
		WebElement ShareTwHP = driver.findElement(By.xpath(".//*[@id='id_skgm_hw_headerLayoutCell']/div[1]/div[2]/div/div[2]/div[5]")); 
		String ShareTwHPFontFamily = ShareTwHP.getCssValue("font-family");
		System.out.println("Actual Font Family of Share Twitter Icon in Home Page is : "	+ ShareTwHPFontFamily);
		String ExpShareTwHPFontFamily = sheet1.getCell(6,4).getContents();
		System.out.println("Expected Font Family of Share Twitter Icon in Home Page is : "	+ ExpShareTwHPFontFamily);

		if(ShareTwHPFontFamily.equals(ExpShareTwHPFontFamily))
		{
			System.out.println("Share Twitter Home Page Font Family is in match with creative - Pass");
			Label ShareTwHPCSSP = new Label(6,5,"PASS"); 
			wsheet1.addCell(ShareTwHPCSSP);
		}
		else
		{
			System.out.println("Share Twitter Home Page Font Family is not in match with creative - Fail");
			bw.write("Share Twitter Home Page Color is Font Family in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label ShareTwHPCSSF = new Label(6,5,"FAIL"); 
			wsheet1.addCell(ShareTwHPCSSF);
		}
				
		// Pinterest Share Icon in Header
				
		Thread.sleep(3000);
		WebElement SharePiHP = driver.findElement(By.xpath(".//*[@id='id_skgm_hw_headerLayoutCell']/div[1]/div[2]/div/div[2]/div[7]")); 
		String SharePiHPFontFamily = SharePiHP.getCssValue("font-family");
		System.out.println("Actual Font Family of Share Pinterest Icon in Home Page is : "	+ SharePiHPFontFamily);
		String ExpSharePiHPFontFamily = sheet1.getCell(7,4).getContents();
		System.out.println("Expected Font Family of Share Pinterest Icon in Home Page is : "	+ ExpSharePiHPFontFamily);

		if(SharePiHPFontFamily.equals(ExpSharePiHPFontFamily))
		{
			System.out.println("Share Pinterest Home Page Font Family is in match with creative - Pass");
			Label SharePiHPCSSP = new Label(7,5,"PASS"); 
			wsheet1.addCell(SharePiHPCSSP);
		}
		else
		{
			System.out.println("Share Pinterest Home Page Font Family is not in match with creative - Fail");
			bw.write("Share Pinterest Home Page Color is Font Family in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label SharePiHPCSSF = new Label(7,5,"FAIL"); 
			wsheet1.addCell(SharePiHPCSSF);
		}
		
		// Shop at Keurig.com in Header
		
		Thread.sleep(3000);
		WebElement ShopKHP = driver.findElement(By.xpath(".//*[@id='id_skgm_hw_headerLayoutCell']/div[1]/div[2]/div/div[2]/a/div[1]")); 
		String ShopKHPColor = ShopKHP.getCssValue("color");
		System.out.println("Actual Color of Shop at Keurig.com Text in Home Page is : "	+ ShopKHPColor );
		// Convert RGB to Hex Value
		Color ShopHHP = Color.fromString(ShopKHPColor);
		String HexShopKHPColor = ShopHHP.asHex();
		System.out.println("Actual Hex Color of Shop at Keurig.com Text in Home Page is : "	+ HexShopKHPColor );
		
		String ShopKHPFontSize = ShopKHP.getCssValue("font-size");
		System.out.println("Actual Font Size of Shop at Keurig.com Text in Home Page is : "	+ ShopKHPFontSize);
		String ShopKHPFontFamily = ShopKHP.getCssValue("font-family");
		System.out.println("Actual Font Family of Shop at Keurig.com Text in Home Page is : "	+ ShopKHPFontFamily);
		
		String ExpShopKHPColor = sheet1.getCell(8,2).getContents(); //column,row
		System.out.println("Expected Color of Shop at Keurig.com Text in Home Page is : "	+ ExpShopKHPColor );
		String ExpShopKHPFontSize = sheet1.getCell(8,3).getContents();
		System.out.println("Expected Font Size of Shop at Keurig.com Text in Home Page is : "	+ ExpShopKHPFontSize);
		String ExpShopKHPFontFamily = sheet1.getCell(8,4).getContents();
		System.out.println("Expected Font Family of Shop at Keurig.com Text in Home Page is : "	+ ExpShopKHPFontFamily);
		
		if(HexShopKHPColor.equals(ExpShopKHPColor))
		{
			System.out.println("Shop at Keurig.com Text in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Shop at Keurig.com Text in Home Page Color is not in match with creative - Fail");
			bw.write("Shop at Keurig.com Text in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(ShopKHPFontSize.equals(ExpShopKHPFontSize))
		{
			System.out.println("Shop at Keurig.com Text in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Shop at Keurig.com Text in Home Page Font Size is not in match with creative - Fail");
			bw.write("Shop at Keurig.com Text in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(ShopKHPFontFamily.equals(ExpShopKHPFontFamily))
		{
			System.out.println("Shop at Keurig.com Text in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Shop at Keurig.com Text in Home Page Font Family is not in match with creative - Fail");
			bw.write("Shop at Keurig.com Text in Home Page Color is Font Family in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexShopKHPColor.equals(ExpShopKHPColor) && ShopKHPFontSize.equals(ExpShopKHPFontSize) && ShopKHPFontFamily.equals(ExpShopKHPFontFamily))
		{
			Label ShopKHPCSSP = new Label(8,5,"PASS"); 
			wsheet1.addCell(ShopKHPCSSP); 
		}
		else
		{
			Label ShopKHPCSSF = new Label(8,5,"FAIL"); 
			wsheet1.addCell(ShopKHPCSSF); 
		}
		
		// Try a Sample Button
		
		Thread.sleep(3000);
		WebElement TryHP = driver.findElement(By.id("idSkcbannerTryButtonTxt")); 
		String TryHPColor = TryHP.getCssValue("color");
		System.out.println("Actual Color of Try a Sample Button Text in Home Page is : "	+ TryHPColor );
		// Convert RGB to Hex Value
		Color TryHHP = Color.fromString(TryHPColor);
		String HexTryHPColor = TryHHP.asHex();
		System.out.println("Actual Hex Color of Try a Sample Button Text in Home Page is : "	+ HexTryHPColor );
		
		String TryHPFontFamily = TryHP.getCssValue("font-family");
		System.out.println("Actual Font Family of Try a Sample Button Text in Home Page is : "	+ TryHPFontFamily);
		
		String ExpTryHPColor = sheet1.getCell(9,2).getContents(); //column,row
		System.out.println("Expected Color of Try a Sample Button Text in Home Page is : "	+ ExpTryHPColor );
		String ExpTryHPFontFamily = sheet1.getCell(9,4).getContents();
		System.out.println("Expected Font Family of Try a Sample Button Text in Home Page is : "	+ ExpTryHPFontFamily);
		
		if(HexTryHPColor.equals(ExpTryHPColor))
		{
			System.out.println("Try a Sample Button Text in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Try a Sample Button Text in Home Page Color is not in match with creative - Fail");
			bw.write("Try a Sample Button Text in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(TryHPFontFamily.equals(ExpTryHPFontFamily))
		{
			System.out.println("Try a Sample Button Text in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Try a Sample Button Text in Home Page Font Family is not in match with creative - Fail");
			bw.write("Try a Sample Button Text in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexTryHPColor.equals(ExpTryHPColor) && TryHPFontFamily.equals(ExpTryHPFontFamily))
		{
			Label TryHPCSSP = new Label(9,5,"PASS"); 
			wsheet1.addCell(TryHPCSSP); 
		}
		else
		{
			Label TryHPCSSF = new Label(9,5,"FAIL"); 
			wsheet1.addCell(TryHPCSSF); 
		}
		
		// Don't have one?
		
		Thread.sleep(3000);
		WebElement DontHP = driver.findElement(By.id("idSkcbannerfreesampletext")); 
		String DontHPColor = DontHP.getCssValue("color");
		System.out.println("Actual Color of Don't have one? Text in Home Page is : "	+ DontHPColor );
		// Convert RGB to Hex Value
		Color DontHHP = Color.fromString(DontHPColor);
		String HexDontHPColor = DontHHP.asHex();
		System.out.println("Actual Hex Color of Don't have one? Text in Home Page is : "	+ HexDontHPColor );		
		
		String DontHPFontSize = DontHP.getCssValue("font-size");
		System.out.println("Actual Font Size of Don't have one? Text in Home Page is : "	+ DontHPFontSize);
		String DontHPFontFamily = DontHP.getCssValue("font-family");
		System.out.println("Actual Font Family of Don't have one? Text in Home Page is : "	+ DontHPFontFamily);
		
		String ExpDontHPColor = sheet1.getCell(10,2).getContents(); //column,row
		System.out.println("Expected Color of Don't have one? Text in Home Page is : "	+ ExpDontHPColor );
		String ExpDontHPFontSize = sheet1.getCell(10,3).getContents();
		System.out.println("Expected Font Size of Don't have one? Text in Home Page is : "	+ ExpDontHPFontSize);
		String ExpDontHPFontFamily = sheet1.getCell(10,4).getContents();
		System.out.println("Expected Font Family of Don't have one? Text in Home Page is : "	+ ExpDontHPFontFamily);
		
		if(HexDontHPColor.equals(ExpDontHPColor))
		{
			System.out.println("Don't have one? Text in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Don't have one? Text in Home Page Color is not in match with creative - Fail");
			bw.write("Don't have one? Text in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(DontHPFontSize.equals(ExpDontHPFontSize))
		{
			System.out.println("Don't have one? Text in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Don't have one? Text in Home Page Font Size is not in match with creative - Fail");
			bw.write("Don't have one? Text in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(DontHPFontFamily.equals(ExpDontHPFontFamily))
		{
			System.out.println("Don't have one? Text in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Don't have one? Text in Home Page Font Family is not in match with creative - Fail");
			bw.write("Don't have one? Text in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexDontHPColor.equals(ExpDontHPColor) && DontHPFontSize.equals(ExpDontHPFontSize) && DontHPFontFamily.equals(ExpDontHPFontFamily))
		{
			Label DontHPCSSP = new Label(10,5,"PASS"); 
			wsheet1.addCell(DontHPCSSP); 
		}
		else
		{
			Label DontHPCSSF = new Label(10,5,"FAIL"); 
			wsheet1.addCell(DontHPCSSF); 
		}
		
		// Learn about our Keurig Brewers
		
		Thread.sleep(3000);
		WebElement LearnHP = driver.findElement(By.id("idSkcbannerfreesampleLinkTxt")); 
		String LearnHPColor = LearnHP.getCssValue("color");
		System.out.println("Actual Color of Learn about our Keurig Brewers Text in Home Page is : "	+ LearnHPColor );
		// Convert RGB to Hex Value
		Color LearnHHP = Color.fromString(LearnHPColor);
		String HexLearnHPColor = LearnHHP.asHex();
		System.out.println("Actual Hex Color of Learn about our Keurig Brewers Text in Home Page is : "	+ HexLearnHPColor );
		
		String LearnHPFontSize = LearnHP.getCssValue("font-size");
		System.out.println("Actual Font Size of Learn about our Keurig Brewers Text in Home Page is : "	+ LearnHPFontSize);
		String LearnHPFontFamily = LearnHP.getCssValue("font-family");
		System.out.println("Actual Font Family of Learn about our Keurig Brewers Text in Home Page is : "	+ LearnHPFontFamily);
		
		String ExpLearnHPColor = sheet1.getCell(11,2).getContents(); //column,row
		System.out.println("Expected Color of Learn about our Keurig Brewers Text in Home Page is : "	+ ExpLearnHPColor );
		String ExpLearnHPFontSize = sheet1.getCell(11,3).getContents();
		System.out.println("Expected Font Size of Learn about our Keurig Brewers Text in Home Page is : "	+ ExpLearnHPFontSize);
		String ExpLearnHPFontFamily = sheet1.getCell(11,4).getContents();
		System.out.println("Expected Font Family of Learn about our Keurig Brewers Text in Home Page is : "	+ ExpLearnHPFontFamily);
		
		if(HexLearnHPColor.equals(ExpLearnHPColor))
		{
			System.out.println("Learn about our Keurig Brewers Text in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Learn about our Keurig Brewers Text in Home Page Color is not in match with creative - Fail");
			bw.write("Learn about our Keurig Brewers Text in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(LearnHPFontSize.equals(ExpLearnHPFontSize))
		{
			System.out.println("Learn about our Keurig Brewers Text in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Learn about our Keurig Brewers Text in Home Page Font Size is not in match with creative - Fail");
			bw.write("Learn about our Keurig Brewers Text in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(LearnHPFontFamily.equals(ExpLearnHPFontFamily))
		{
			System.out.println("Learn about our Keurig Brewers Text in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Learn about our Keurig Brewers Text in Home Page Font Family is not in match with creative - Fail");
			bw.write("Learn about our Keurig Brewers Text in Home Page Color is Font Family in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(HexLearnHPColor.equals(ExpLearnHPColor) && LearnHPFontSize.equals(ExpLearnHPFontSize) && LearnHPFontFamily.equals(ExpLearnHPFontFamily))
		{
			Label LearnHPCSSP = new Label(11,5,"PASS"); 
			wsheet1.addCell(LearnHPCSSP); 
		}
		else
		{
			Label LearnHPCSSF = new Label(11,5,"FAIL"); 
			wsheet1.addCell(LearnHPCSSF); 
		}
		
		
		// Watch the TV Commercial
		
		Thread.sleep(3000);
		WebElement WatchHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[2]/div/div/div/div[1]/div/div[4]/div[2]")); 
		String WatchHPColor = WatchHP.getCssValue("color");
		System.out.println("Actual Color of Watch the TV Commercial Text in Home Page is : "	+ WatchHPColor );
		// Convert RGB to Hex Value
		Color WatchHHP = Color.fromString(WatchHPColor);
		String HexWatchHPColor = WatchHHP.asHex();
		System.out.println("Actual Hex Color of Watch the TV Commercial Text in Home Page is : "	+ HexWatchHPColor );
		
		String WatchHPFontSize = WatchHP.getCssValue("font-size");
		System.out.println("Actual Font Size of Watch the TV Commercial Text in Home Page is : "	+ WatchHPFontSize);
		String WatchHPFontFamily = WatchHP.getCssValue("font-family");
		System.out.println("Actual Font Family of Watch the TV Commercial Text in Home Page is : "	+ WatchHPFontFamily);
		
		String ExpWatchHPColor = sheet1.getCell(12,2).getContents(); //column,row
		System.out.println("Expected Color of Watch the TV Commercial Text in Home Page is : "	+ ExpWatchHPColor );
		String ExpWatchHPFontSize = sheet1.getCell(12,3).getContents();
		System.out.println("Expected Font Size of Watch the TV Commercial Text in Home Page is : "	+ ExpWatchHPFontSize);
		String ExpWatchHPFontFamily = sheet1.getCell(12,4).getContents();
		System.out.println("Expected Font Family of Watch the TV Commercial Text in Home Page is : "	+ ExpWatchHPFontFamily);
		
		if(HexWatchHPColor.equals(ExpWatchHPColor))
		{
			System.out.println("Watch the TV Commercial Text in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Watch the TV Commercial Text in Home Page Color is not in match with creative - Fail");
			bw.write("Watch the TV Commercial Text in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(WatchHPFontSize.equals(ExpWatchHPFontSize))
		{
			System.out.println("Watch the TV Commercial Text in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Watch the TV Commercial Text in Home Page Font Size is not in match with creative - Fail");
			bw.write("Watch the TV Commercial Text in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(WatchHPFontFamily.equals(ExpWatchHPFontFamily))
		{
			System.out.println("Watch the TV Commercial Text in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Watch the TV Commercial Text in Home Page Font Family is not in match with creative - Fail");
			bw.write("Watch the TV Commercial Text in Home Page Color is Font Family in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexWatchHPColor.equals(ExpWatchHPColor) && WatchHPFontSize.equals(ExpWatchHPFontSize) && WatchHPFontFamily.equals(ExpWatchHPFontFamily))
		{
			Label WatchHPCSSP = new Label(12,5,"PASS"); 
			wsheet1.addCell(WatchHPCSSP); 
		}
		else
		{
			Label WatchHPCSSF = new Label(12,5,"FAIL"); 
			wsheet1.addCell(WatchHPCSSF); 
		}
		
		// Quality Title
		
		Thread.sleep(3000);
		WebElement QualityTitleHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[3]/div/div/div/div[1]/div[1]"));
		String QualityTitleHPFontSize = QualityTitleHP.getCssValue("font-size");
		System.out.println("Actual Font Size of Quality Title Text in Home Page is : "	+ QualityTitleHPFontSize);
		String QualityTitleHPFontFamily = QualityTitleHP.getCssValue("font-family");
		System.out.println("Actual Font Family of Quality Title Text in Home Page is : "	+ QualityTitleHPFontFamily);
		String QualityTitleContent = QualityTitleHP.getText();
		System.out.println("Actual Quality Title Content in Home Page is : "	+ QualityTitleContent);
		 
		String ExpQualityTitleHPFontSize = sheet1.getCell(2,7).getContents();
		System.out.println("Expected Font Size of Quality Title Text in Home Page is : "	+ ExpQualityTitleHPFontSize);
		String ExpQualityTitleHPFontFamily = sheet1.getCell(3,7).getContents();
		System.out.println("Expected Font Family of Quality Title Text in Home Page is : "	+ ExpQualityTitleHPFontFamily);
		String ExpQualityTitleContent = sheet1.getCell(4,7).getContents();
		System.out.println("Expected Quality Title Content in Home Page is : "	+ ExpQualityTitleContent);
		
		if(QualityTitleHPFontSize.equals(ExpQualityTitleHPFontSize))
		{
			System.out.println("Quality Title Text in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Quality Title Text in Home Page Font Size is not in match with creative - Fail");
			bw.write("Quality Title Text in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(QualityTitleHPFontFamily.equals(ExpQualityTitleHPFontFamily))
		{
			System.out.println("Quality Title Text in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Quality Title Text in Home Page Font Family is not in match with creative - Fail");
			bw.write("Quality Title Text in Home Page Color is Font Family in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(QualityTitleContent.equals(ExpQualityTitleContent))
		{
			System.out.println("Quality Title Content in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Quality Title Content in Home Page is not in match with creative - Fail");
			bw.write("Quality Title Content in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(QualityTitleHPFontSize.equals(ExpQualityTitleHPFontSize) && QualityTitleHPFontFamily.equals(ExpQualityTitleHPFontFamily) && QualityTitleContent.equals(ExpQualityTitleContent))
		{
			Label QualityTitleHPCSSP = new Label(5,7,"PASS"); 
			wsheet1.addCell(QualityTitleHPCSSP); 
		}
		else
		{
			Label QualityTitleHPCSSF = new Label(5,7,"FAIL"); 
			wsheet1.addCell(QualityTitleHPCSSF); 
		}
		
		// Quality Description
		
		Thread.sleep(3000);
		WebElement QualityDescHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[3]/div/div/div/div[1]/div[3]"));
		String QualityDescHPFontSize = QualityDescHP.getCssValue("font-size");
		System.out.println("Actual Font Size of Quality Desc Text in Home Page is : "	+ QualityDescHPFontSize);
		String QualityDescHPFontFamily = QualityDescHP.getCssValue("font-family");
		System.out.println("Actual Font Family of Quality Desc Text in Home Page is : "	+ QualityDescHPFontFamily);
		String QualityDescHPContent = QualityDescHP.getText();
		System.out.println("Actual Quality Desc Content in Home Page is : "	+ QualityDescHPContent);
		 
		String ExpQualityDescHPFontSize = sheet1.getCell(2,8).getContents();
		System.out.println("Expected Font Size of Quality Desc Text in Home Page is : "	+ ExpQualityDescHPFontSize);
		String ExpQualityDescHPFontFamily = sheet1.getCell(3,8).getContents();
		System.out.println("Expected Font Family of Quality Desc Text in Home Page is : "	+ ExpQualityDescHPFontFamily);
		String ExpQualityDescHPContent = sheet1.getCell(4,8).getContents();
		System.out.println("Expected Quality Desc Content in Home Page is : "	+ ExpQualityDescHPContent);
		
		if(QualityDescHPFontSize.equals(ExpQualityDescHPFontSize))
		{
			System.out.println("Quality Desc Text in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Quality Desc Text in Home Page Font Size is not in match with creative - Fail");
			bw.write("Quality Desc Text in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(QualityDescHPFontFamily.equals(ExpQualityDescHPFontFamily))
		{
			System.out.println("Quality Desc Text in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Quality Desc Text in Home Page Font Family is not in match with creative - Fail");
			bw.write("Quality Desc Text in Home Page Color is Font Family in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(QualityDescHPContent.equals(ExpQualityDescHPContent))
		{
			System.out.println("Quality Desc Content in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Quality Desc Content in Home Page is not in match with creative - Fail");
			bw.write("Quality Desc Content in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(QualityDescHPFontSize.equals(ExpQualityDescHPFontSize) && QualityDescHPFontFamily.equals(ExpQualityDescHPFontFamily) && QualityDescHPContent.equals(ExpQualityDescHPContent))
		{
			Label QualityDescHPCSSP = new Label(5,8,"PASS"); 
			wsheet1.addCell(QualityDescHPCSSP); 
		}
		else
		{
			Label QualityDescHPCSSF = new Label(5,8,"FAIL"); 
			wsheet1.addCell(QualityDescHPCSSF); 
		}
			
		// Quality Text in Side Banner
		
		Thread.sleep(3000);
		WebElement QualitySBHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[4]/div/div[1]/div/div[1]")); 
		String QualitySBHPColor = QualitySBHP.getCssValue("color");
		System.out.println("Actual Color of Quality Text in Side Banner in Home Page is : "	+ QualitySBHPColor );
		// Convert RGB to Hex Value
		Color QualitySB1HP = Color.fromString(QualitySBHPColor);
		String HexQualitySBHPColor = QualitySB1HP.asHex();
		System.out.println("Actual Hex Color of Quality Text in Side Banner in Home Page is : "	+ HexQualitySBHPColor );
		
		String QualitySBHPFontSize = QualitySBHP.getCssValue("font-size");
		System.out.println("Actual Font Size of Quality Text in Side Banner in Home Page is : "	+ QualitySBHPFontSize);
		String QualitySBHPFontFamily = QualitySBHP.getCssValue("font-family");
		System.out.println("Actual Font Family of Quality Text in Side Banner in Home Page is : "	+ QualitySBHPFontFamily);
				
		String ExpQualitySBHPColor = sheet1.getCell(1,9).getContents(); //column,row
		System.out.println("Expected Color of Quality Text in Side Banner in Home Page is : "	+ ExpQualitySBHPColor );
		String ExpQualitySBHPFontSize = sheet1.getCell(2,9).getContents();
		System.out.println("Expected Font Size of Quality Text in Side Banner in Home Page is : "	+ ExpQualitySBHPFontSize);
		String ExpQualitySBHPFontFamily = sheet1.getCell(3,9).getContents();
		System.out.println("Expected Font Family of Quality Text in Side Banner in Home Page is : "	+ ExpQualitySBHPFontFamily);
				
		if(HexQualitySBHPColor.equals(ExpQualitySBHPColor))
		{
			System.out.println("Quality Text in Side Banner in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Quality Text in Side Banner in Home Page Color is not in match with creative - Fail");
			bw.write("Quality Text in Side Banner in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(QualitySBHPFontSize.equals(ExpQualitySBHPFontSize))
		{
			System.out.println("Quality Text in Side Banner in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Quality Text in Side Banner in Home Page Font Size is not in match with creative - Fail");
			bw.write("Quality Text in Side Banner in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(QualitySBHPFontFamily.equals(ExpQualitySBHPFontFamily))
		{
			System.out.println("Quality Text in Side Banner in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Quality Text in Side Banner in Home Page Font Family is not in match with creative - Fail");
			bw.write("Quality Text in Side Banner in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexQualitySBHPColor.equals(ExpQualitySBHPColor) && QualitySBHPFontSize.equals(ExpQualitySBHPFontSize) && QualitySBHPFontFamily.equals(ExpQualitySBHPFontFamily))
		{
			Label QualitySBHPCSSP = new Label(5,9,"PASS"); 
			wsheet1.addCell(QualitySBHPCSSP); 
		}
		else
		{
			Label QualitySBHPCSSF = new Label(5,9,"FAIL"); 
			wsheet1.addCell(QualitySBHPCSSF); 
		}
				
		// Quality Side Banner - A Different Approach
		
		WebElement QualitySBDiffHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[4]/div/div[1]/div/div[2]")); 
		String QualitySBDiffHPColor = QualitySBDiffHP.getCssValue("color");
		System.out.println("Actual Color of A Different Approach Text in Side Banner in Home Page is : "	+ QualitySBDiffHPColor );
		// Convert RGB to Hex Value
		Color QualitySBDiff1HP = Color.fromString(QualitySBDiffHPColor);
		String HexQualitySBDiffHPColor = QualitySBDiff1HP.asHex();
		System.out.println("Actual Hex Color of A Different Approach Text in Side Banner in Home Page is : "	+ HexQualitySBDiffHPColor );
		
		String QualitySBDiffHPFontSize = QualitySBDiffHP.getCssValue("font-size");
		System.out.println("Actual Font Size of A Different Approach Text in Side Banner in Home Page is : "	+ QualitySBDiffHPFontSize);
		String QualitySBDiffHPFontFamily = QualitySBDiffHP.getCssValue("font-family");
		System.out.println("Actual Font Family of A Different Approach Text in Side Banner in Home Page is : "	+ QualitySBDiffHPFontFamily);
		String QualitySBDiffHPContent = QualitySBDiffHP.getText();
		System.out.println("Actual A Different Approach Content in Home Page is : "	+ QualitySBDiffHPContent);
				
		String ExpQualitySBDiffHPColor = sheet1.getCell(1,10).getContents(); //column,row
		System.out.println("Expected Color of A Different Approach Text in Side Banner in Home Page is : "	+ ExpQualitySBDiffHPColor );
		String ExpQualitySBDiffHPFontSize = sheet1.getCell(2,10).getContents();
		System.out.println("Expected Font Size of A Different Approach Text in Side Banner in Home Page is : "	+ ExpQualitySBDiffHPFontSize);
		String ExpQualitySBDiffHPFontFamily = sheet1.getCell(3,10).getContents();
		System.out.println("Expected Font Family of A Different Approach Text in Side Banner in Home Page is : "	+ ExpQualitySBDiffHPFontFamily);
		String ExpQualitySBDiffHPContent = sheet1.getCell(4,10).getContents();
		System.out.println("Expected A Different Approach Content in Home Page is : "	+ ExpQualitySBDiffHPContent);
				
		if(HexQualitySBDiffHPColor.equals(ExpQualitySBDiffHPColor))
		{
			System.out.println("A Different Approach Text in Side Banner in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("A Different Approach Text in Side Banner in Home Page Color is not in match with creative - Fail");
			bw.write("A Different Approach Text in Side Banner in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(QualitySBDiffHPFontSize.equals(ExpQualitySBDiffHPFontSize))
		{
			System.out.println("A Different Approach Text in Side Banner in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("A Different Approach Text in Side Banner in Home Page Font Size is not in match with creative - Fail");
			bw.write("A Different Approach Text in Side Banner in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(QualitySBDiffHPFontFamily.equals(ExpQualitySBDiffHPFontFamily))
		{
			System.out.println("A Different Approach Text in Side Banner in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("A Different Approach Text in Side Banner in Home Page Font Family is not in match with creative - Fail");
			bw.write("A Different Approach Text in Side Banner in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(QualitySBDiffHPContent.equals(ExpQualitySBDiffHPContent))
		{
			System.out.println("A Different Approach Content in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("A Different Approach Content in Home Page is not in match with creative - Fail");
			bw.write("A Different Approach Content in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexQualitySBDiffHPColor.equals(ExpQualitySBDiffHPColor) && QualitySBDiffHPFontSize.equals(ExpQualitySBDiffHPFontSize) && QualitySBDiffHPFontFamily.equals(ExpQualitySBDiffHPFontFamily) && QualitySBDiffHPContent.equals(ExpQualitySBDiffHPContent))
		{
			Label QualitySBDiffHPCSSP = new Label(5,10,"PASS"); 
			wsheet1.addCell(QualitySBDiffHPCSSP); 
		}
		else
		{
			Label QualitySBDiffHPCSSF = new Label(5,10,"FAIL"); 
			wsheet1.addCell(QualitySBDiffHPCSSF); 
		}
		
		// Quality Side Banner Contents
		
		WebElement QualitySBContentsHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[4]/div/div[1]/div/div[4]")); 
		String QualitySBContentsHPColor = QualitySBContentsHP.getCssValue("color");
		System.out.println("Actual Color of Quality Side Banner Contents Text in Side Banner in Home Page is : "	+ QualitySBContentsHPColor );
		// Convert RGB to Hex Value
		Color QualitySBContent1HP = Color.fromString(QualitySBContentsHPColor);
		String HexQualitySBContentsHPColor = QualitySBContent1HP.asHex();
		System.out.println("Actual Hex Color of Quality Side Banner Contents Text in Side Banner in Home Page is : "	+ HexQualitySBContentsHPColor );
		
		String QualitySBContentsHPFontSize = QualitySBContentsHP.getCssValue("font-size");
		System.out.println("Actual Font Size of Quality Side Banner Contents Text in Side Banner in Home Page is : "	+ QualitySBContentsHPFontSize);
		String QualitySBContentsHPFontFamily = QualitySBContentsHP.getCssValue("font-family");
		System.out.println("Actual Font Family of Quality Side Banner Contents Text in Side Banner in Home Page is : "	+ QualitySBContentsHPFontFamily);
		String QualitySBContentsHPContent = QualitySBContentsHP.getText();
		System.out.println("Actual Quality Side Banner Contents in Home Page is : "	+ QualitySBContentsHPContent);
				
		String ExpQualitySBContentsHPColor = sheet1.getCell(1,11).getContents(); //column,row
		System.out.println("Expected Color of Quality Side Banner Contents Text in Side Banner in Home Page is : "	+ ExpQualitySBContentsHPColor );
		String ExpQualitySBContentsHPFontSize = sheet1.getCell(2,11).getContents();
		System.out.println("Expected Font Size of Quality Side Banner Contents Text in Side Banner in Home Page is : "	+ ExpQualitySBContentsHPFontSize);
		String ExpQualitySBContentsHPFontFamily = sheet1.getCell(3,11).getContents();
		System.out.println("Expected Font Family of Quality Side Banner Contents Text in Side Banner in Home Page is : "	+ ExpQualitySBContentsHPFontFamily);
		String ExpQualitySBContentsHPContent = sheet1.getCell(4,11).getContents();
		System.out.println("Expected Quality Side Banner Contents in Home Page is : "	+ ExpQualitySBContentsHPContent);
				
		if(HexQualitySBContentsHPColor.equals(ExpQualitySBContentsHPColor))
		{
			System.out.println("Quality Side Banner Contents Text in Side Banner in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Quality Side Banner Contents Text in Side Banner in Home Page Color is not in match with creative - Fail");
			bw.write("Quality Side Banner Contents Text in Side Banner in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(QualitySBContentsHPFontSize.equals(ExpQualitySBContentsHPFontSize))
		{
			System.out.println("Quality Side Banner Contents Text in Side Banner in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Quality Side Banner Contents Text in Side Banner in Home Page Font Size is not in match with creative - Fail");
			bw.write("Quality Side Banner Contents Text in Side Banner in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(QualitySBContentsHPFontFamily.equals(ExpQualitySBContentsHPFontFamily))
		{
			System.out.println("Quality Side Banner Contents Text in Side Banner in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Quality Side Banner Contents Text in Side Banner in Home Page Font Family is not in match with creative - Fail");
			bw.write("Quality Side Banner Contents Text in Side Banner in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(QualitySBContentsHPContent.equals(ExpQualitySBContentsHPContent))
		{
			System.out.println("Quality Side Banner Contents in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Quality Side Banner Contents in Home Page is not in match with creative - Fail");
			bw.write("Quality Side Banner Contents in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexQualitySBContentsHPColor.equals(ExpQualitySBContentsHPColor) && QualitySBContentsHPFontSize.equals(ExpQualitySBContentsHPFontSize) && QualitySBContentsHPFontFamily.equals(ExpQualitySBContentsHPFontFamily) && QualitySBContentsHPContent.equals(ExpQualitySBContentsHPContent))
		{
			Label QualitySBContentsHPCSSP = new Label(5,11,"PASS"); 
			wsheet1.addCell(QualitySBContentsHPCSSP); 
		}
		else
		{
			Label QualitySBContentsHPCSSF = new Label(5,11,"FAIL"); 
			wsheet1.addCell(QualitySBContentsHPCSSF); 
		}
		
		// Quality Side Banner - More about Quality
		
		WebElement QualitySBMoreHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[4]/div/div[1]/div/div[5]/div")); 
		String QualitySBMoreHPColor = QualitySBMoreHP.getCssValue("color");
		System.out.println("Actual Color of More about Quality Text in Side Banner in Home Page is : "	+ QualitySBMoreHPColor );
		// Convert RGB to Hex Value
		Color QualitySBMore1HP = Color.fromString(QualitySBMoreHPColor);
		String HexQualitySBMoreHPColor = QualitySBMore1HP.asHex();
		System.out.println("Actual Hex Color of More about Quality Text in Side Banner in Home Page is : "	+ HexQualitySBMoreHPColor );
		
		String QualitySBMoreHPFontSize = QualitySBMoreHP.getCssValue("font-size");
		System.out.println("Actual Font Size of More about Quality Text in Side Banner in Home Page is : "	+ QualitySBMoreHPFontSize);
		String QualitySBMoreHPFontFamily = QualitySBMoreHP.getCssValue("font-family");
		System.out.println("Actual Font Family of More about Quality Text in Side Banner in Home Page is : "	+ QualitySBMoreHPFontFamily);
		String QualitySBMoreHPContent = QualitySBMoreHP.getText();
		System.out.println("Actual More about Quality Content in Home Page is : "	+ QualitySBMoreHPContent);
				
		String ExpQualitySBMoreHPColor = sheet1.getCell(1,12).getContents(); //column,row
		System.out.println("Expected Color of More about Quality Text in Side Banner in Home Page is : "	+ ExpQualitySBMoreHPColor );
		String ExpQualitySBMoreHPFontSize = sheet1.getCell(2,12).getContents();
		System.out.println("Expected Font Size of More about Quality Text in Side Banner in Home Page is : "	+ ExpQualitySBMoreHPFontSize);
		String ExpQualitySBMoreHPFontFamily = sheet1.getCell(3,12).getContents();
		System.out.println("Expected Font Family of More about Quality Text in Side Banner in Home Page is : "	+ ExpQualitySBMoreHPFontFamily);
		String ExpQualitySBMoreHPContent = sheet1.getCell(4,12).getContents();
		System.out.println("Expected More about Quality Content in Home Page is : "	+ ExpQualitySBMoreHPContent);
				
		if(HexQualitySBMoreHPColor.equals(ExpQualitySBMoreHPColor))
		{
			System.out.println("More about Quality Text in Side Banner in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("More about Quality Text in Side Banner in Home Page Color is not in match with creative - Fail");
			bw.write("More about Quality Text in Side Banner in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(QualitySBMoreHPFontSize.equals(ExpQualitySBMoreHPFontSize))
		{
			System.out.println("More about Quality Text in Side Banner in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("More about Quality Text in Side Banner in Home Page Font Size is not in match with creative - Fail");
			bw.write("More about Quality Text in Side Banner in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(QualitySBMoreHPFontFamily.equals(ExpQualitySBMoreHPFontFamily))
		{
			System.out.println("More about Quality Text in Side Banner in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("More about Quality Text in Side Banner in Home Page Font Family is not in match with creative - Fail");
			bw.write("More about Quality Text in Side Banner in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(QualitySBMoreHPContent.equals(ExpQualitySBMoreHPContent))
		{
			System.out.println("More about Quality Content in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("More about Quality Content in Home Page is not in match with creative - Fail");
			bw.write("More about Quality Content in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexQualitySBMoreHPColor.equals(ExpQualitySBMoreHPColor) && QualitySBMoreHPFontSize.equals(ExpQualitySBMoreHPFontSize) && QualitySBMoreHPFontFamily.equals(ExpQualitySBMoreHPFontFamily) && QualitySBMoreHPContent.equals(ExpQualitySBMoreHPContent))
		{
			Label QualitySBMoreHPCSSP = new Label(5,12,"PASS"); 
			wsheet1.addCell(QualitySBMoreHPCSSP); 
		}
		else
		{
			Label QualitySBMoreHPCSSF = new Label(5,12,"FAIL"); 
			wsheet1.addCell(QualitySBMoreHPCSSF); 
		}
		
		// Quality Number tab 1 - From Vermont,with love

		
		Thread.sleep(3000);
		WebElement QualityN1TitleHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[4]/div/div[2]/div[1]/div[1]/div[1]")); 
		String QualityN1TitleHPColor = QualityN1TitleHP.getCssValue("color");
		System.out.println("Actual Color of From Vermont,with love Text in Quality Number tab 1 in Home Page is : "	+ QualityN1TitleHPColor );
		// Convert RGB to Hex Value
		Color HQualityN1TitleHPColor = Color.fromString(QualityN1TitleHPColor);
		String HexQualityN1TitleHPColor = HQualityN1TitleHPColor.asHex();
		System.out.println("Actual Hex Color of From Vermont,with love Text in Quality Number tab 1 in Home Page is : "	+ HexQualityN1TitleHPColor );
		
		String QualityN1TitleHPFontSize = QualityN1TitleHP.getCssValue("font-size");
		System.out.println("Actual Font Size of From Vermont,with love Text in Quality Number tab 1 in Home Page is : "	+ QualityN1TitleHPFontSize);
		String QualityN1TitleHPFontFamily = QualityN1TitleHP.getCssValue("font-family");
		System.out.println("Actual Font Family of From Vermont,with love Text in Quality Number tab 1 in Home Page is : "	+ QualityN1TitleHPFontFamily);
		String QualityN1TitleHPContent = QualityN1TitleHP.getText();
		System.out.println("Actual From Vermont,with love Content in Quality Number tab 1 in Home Page is : "	+ QualityN1TitleHPContent);
				
		String ExpQualityN1TitleHPColor = sheet1.getCell(1,13).getContents(); //column,row
		System.out.println("Expected Color of From Vermont,with love Text in Quality Number tab 1 in Home Page is : "	+ ExpQualityN1TitleHPColor );
		String ExpQualityN1TitleHPFontSize = sheet1.getCell(2,13).getContents();
		System.out.println("Expected Font Size of From Vermont,with love Text in Quality Number tab 1 in Home Page is : "	+ ExpQualityN1TitleHPFontSize);
		String ExpQualityN1TitleHPFontFamily = sheet1.getCell(3,13).getContents();
		System.out.println("Expected Font Family of From Vermont,with love Text in Quality Number tab 1 in Home Page is : "	+ ExpQualityN1TitleHPFontFamily);
		String ExpQualityN1TitleHPContent = sheet1.getCell(4,13).getContents();
		System.out.println("Expected From Vermont,with love Content in Quality Number tab 1 in Home Page is : "	+ ExpQualityN1TitleHPContent);
				
		if(HexQualityN1TitleHPColor.equals(ExpQualityN1TitleHPColor))
		{
			System.out.println("From Vermont,with love Text in Quality Number tab 1 in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("From Vermont,with love Text in Quality Number tab 1 in Home Page Color is not in match with creative - Fail");
			bw.write("From Vermont,with love Text in Quality Number tab 1 in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(QualityN1TitleHPFontSize.equals(ExpQualityN1TitleHPFontSize))
		{
			System.out.println("From Vermont,with love Text in Quality Number tab 1 in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("From Vermont,with love Text in Quality Number tab 1 in Home Page Font Size is not in match with creative - Fail");
			bw.write("From Vermont,with love Text in Quality Number tab 1 in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(QualityN1TitleHPFontFamily.equals(ExpQualityN1TitleHPFontFamily))
		{
			System.out.println("From Vermont,with love Text in Quality Number tab 1 in Home Page Font Family is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("From Vermont,with love Text in Quality Number tab 1 in Home Page Font Family is not in match with creative - Fail");
			bw.write("From Vermont,with love Text in Quality Number tab 1 in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(QualityN1TitleHPContent.equals(ExpQualityN1TitleHPContent))
		{
			System.out.println("From Vermont,with love Content in Quality Number tab 1 in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("From Vermont,with love Content in Quality Number tab 1 in Home Page is not in match with creative - Fail");
			bw.write("From Vermont,with love Content in Quality Number tab 1 in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexQualityN1TitleHPColor.equals(ExpQualityN1TitleHPColor) && QualityN1TitleHPFontSize.equals(ExpQualityN1TitleHPFontSize) && QualityN1TitleHPFontFamily.equals(ExpQualityN1TitleHPFontFamily) && QualityN1TitleHPContent.equals(ExpQualityN1TitleHPContent))
		{
			Label QualityN1TitleHPCSSP = new Label(5,13,"PASS"); 
			wsheet1.addCell(QualityN1TitleHPCSSP); 
		}
		else
		{
			Label QualityN1TitleHPCSSF = new Label(5,13,"FAIL"); 
			wsheet1.addCell(QualityN1TitleHPCSSF); 
		}
		
		// Quality Number tab 1 - Brewing exceptional coffee since 1981
			
		WebElement QualityN1DescHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[4]/div/div[2]/div[1]/div[1]/div[3]/div[2]")); 
		String QualityN1DescHPColor = QualityN1DescHP.getCssValue("color");
		System.out.println("Actual Color of Brewing exceptional coffee since 1981 Text in Quality Number tab 1 in Home Page is : "	+ QualityN1DescHPColor );
		// Convert RGB to Hex Value
		Color HQualityN1DescHPColor = Color.fromString(QualityN1DescHPColor);
		String HexQualityN1DescHPColor = HQualityN1DescHPColor.asHex();
		System.out.println("Actual Hex Color of Brewing exceptional coffee since 1981 Text in Quality Number tab 1 in Home Page is : "	+ HexQualityN1DescHPColor );
		
		String QualityN1DescHPFontSize = QualityN1DescHP.getCssValue("font-size");
		System.out.println("Actual Font Size of Brewing exceptional coffee since 1981 Text in Quality Number tab 1 in Home Page is : "	+ QualityN1DescHPFontSize);
		String QualityN1DescHPFontFamily = QualityN1DescHP.getCssValue("font-family");
		System.out.println("Actual Font Family of Brewing exceptional coffee since 1981 Text in Quality Number tab 1 in Home Page is : "	+ QualityN1DescHPFontFamily);
		String QualityN1DescHPContent = QualityN1DescHP.getText();
		System.out.println("Actual Brewing exceptional coffee since 1981 Content in Quality Number tab 1 in Home Page is : "	+ QualityN1DescHPContent);
				
		String ExpQualityN1DescHPColor = sheet1.getCell(1,14).getContents(); //column,row
		System.out.println("Expected Color of Brewing exceptional coffee since 1981 Text in Quality Number tab 1 in Home Page is : "	+ ExpQualityN1DescHPColor );
		String ExpQualityN1DescHPFontSize = sheet1.getCell(2,14).getContents();
		System.out.println("Expected Font Size of Brewing exceptional coffee since 1981 Text in Quality Number tab 1 in Home Page is : "	+ ExpQualityN1DescHPFontSize);
		String ExpQualityN1DescHPFontFamily = sheet1.getCell(3,14).getContents();
		System.out.println("Expected Font Family of Brewing exceptional coffee since 1981 Text in Quality Number tab 1 in Home Page is : "	+ ExpQualityN1DescHPFontFamily);
		String ExpQualityN1DescHPContent = sheet1.getCell(4,14).getContents();
		System.out.println("Expected Brewing exceptional coffee since 1981 Content in Quality Number tab 1 in Home Page is : "	+ ExpQualityN1DescHPContent);
				
		if(HexQualityN1DescHPColor.equals(ExpQualityN1DescHPColor))
		{
			System.out.println("Brewing exceptional coffee since 1981 Text in Quality Number tab 1 in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Brewing exceptional coffee since 1981 Text in Quality Number tab 1 in Home Page Color is not in match with creative - Fail");
			bw.write("Brewing exceptional coffee since 1981 Text in Quality Number tab 1 in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(QualityN1DescHPFontSize.equals(ExpQualityN1DescHPFontSize))
		{
			System.out.println("Brewing exceptional coffee since 1981 Text in Quality Number tab 1 in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Brewing exceptional coffee since 1981 Text in Quality Number tab 1 in Home Page Font Size is not in match with creative - Fail");
			bw.write("Brewing exceptional coffee since 1981 Text in Quality Number tab 1 in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(QualityN1DescHPFontFamily.equals(ExpQualityN1DescHPFontFamily))
		{
			System.out.println("Brewing exceptional coffee since 1981 Text in Quality Number tab 1 in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Brewing exceptional coffee since 1981 Text in Quality Number tab 1 in Home Page Font Family is not in match with creative - Fail");
			bw.write("Brewing exceptional coffee since 1981 Text in Quality Number tab 1 in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(QualityN1DescHPContent.equals(ExpQualityN1DescHPContent))
		{
			System.out.println("Brewing exceptional coffee since 1981 Content in Quality Number tab 1 in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Brewing exceptional coffee since 1981 Content in Quality Number tab 1 in Home Page is not in match with creative - Fail");
			bw.write("Brewing exceptional coffee since 1981 Content in Quality Number tab 1 in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexQualityN1DescHPColor.equals(ExpQualityN1DescHPColor) && QualityN1DescHPFontSize.equals(ExpQualityN1DescHPFontSize) && QualityN1DescHPFontFamily.equals(ExpQualityN1DescHPFontFamily) && QualityN1DescHPContent.equals(ExpQualityN1DescHPContent))
		{
			Label QualityN1DescHPCSSP = new Label(5,14,"PASS"); 
			wsheet1.addCell(QualityN1DescHPCSSP); 
		}
		else
		{
			Label QualityN1DescHPCSSF = new Label(5,14,"FAIL"); 
			wsheet1.addCell(QualityN1DescHPCSSF); 
		}
		
		
		// Quality Number tab 2 - GROWING TOGETHER
		
		//click action on Quality Number tab 2
		
		Thread.sleep(3000);
		JavascriptExecutor jsent = (JavascriptExecutor)driver;
		jsent.executeScript("window.scrollBy(0,1050)", "");
		Thread.sleep(8000);
		driver.findElement(By.xpath("//*[@id='id_2_tab']")).click();  
		Thread.sleep(3000);
		                          
		//String Number = driver.findElement(By.xpath("//*[@id='id_2_tab']")).getText();  // Quality Number tab 2
		//System.out.println("Number Tab is:" +Number);
		
		WebElement QualityN2TitleHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[4]/div/div[2]/div[1]/div[2]/div[1]")); 
		String QualityN2TitleHPColor = QualityN2TitleHP.getCssValue("color");
		System.out.println("Actual Color of GROWING TOGETHER Text in Quality Number tab 2 in Home Page is : "	+ QualityN2TitleHPColor );
		// Convert RGB to Hex Value
		Color HQualityN2TitleHPColor = Color.fromString(QualityN2TitleHPColor);
		String HexQualityN2TitleHPColor = HQualityN2TitleHPColor.asHex();
		System.out.println("Actual Hex Color of GROWING TOGETHER Text in Quality Number tab 2 in Home Page is : "	+ HexQualityN2TitleHPColor );
		
		String QualityN2TitleHPFontSize = QualityN2TitleHP.getCssValue("font-size");
		System.out.println("Actual Font Size of GROWING TOGETHER Text in Quality Number tab 2 in Home Page is : "	+ QualityN2TitleHPFontSize);
		String QualityN2TitleHPFontFamily = QualityN2TitleHP.getCssValue("font-family");
		System.out.println("Actual Font Family of GROWING TOGETHER Text in Quality Number tab 2 in Home Page is : "	+ QualityN2TitleHPFontFamily);
		String QualityN2TitleHPContent = QualityN2TitleHP.getText();
		System.out.println("Actual GROWING TOGETHER Content in Quality Number tab 2 in Home Page is : "	+ QualityN2TitleHPContent);
						
		String ExpQualityN2TitleHPColor = sheet1.getCell(1,15).getContents(); //column,row
		System.out.println("Expected Color of GROWING TOGETHER Text in Quality Number tab 2 in Home Page is : "	+ ExpQualityN2TitleHPColor );
		String ExpQualityN2TitleHPFontSize = sheet1.getCell(2,15).getContents();
		System.out.println("Expected Font Size of GROWING TOGETHER Text in Quality Number tab 2 in Home Page is : "	+ ExpQualityN2TitleHPFontSize);
		String ExpQualityN2TitleHPFontFamily = sheet1.getCell(3,15).getContents();
		System.out.println("Expected Font Family of GROWING TOGETHER Text in Quality Number tab 2 in Home Page is : "	+ ExpQualityN2TitleHPFontFamily);
		String ExpQualityN2TitleHPContent = sheet1.getCell(4,15).getContents();
		System.out.println("Expected GROWING TOGETHER Content in Quality Number tab 2 in Home Page is : "	+ ExpQualityN2TitleHPContent);
					
		if(HexQualityN2TitleHPColor.equals(ExpQualityN2TitleHPColor))
		{
			System.out.println("GROWING TOGETHER Text in Quality Number tab 2 in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("GROWING TOGETHER Text in Quality Number tab 2 in Home Page Color is not in match with creative - Fail");
			bw.write("GROWING TOGETHER Text in Quality Number tab 2 in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
					
		if(QualityN2TitleHPFontSize.equals(ExpQualityN2TitleHPFontSize))
		{
			System.out.println("GROWING TOGETHER Text in Quality Number tab 2 in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("GROWING TOGETHER Text in Quality Number tab 2 in Home Page Font Size is not in match with creative - Fail");
			bw.write("GROWING TOGETHER Text in Quality Number tab 2 in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
						
		if(QualityN2TitleHPFontFamily.equals(ExpQualityN2TitleHPFontFamily))
		{
			System.out.println("GROWING TOGETHER Text in Quality Number tab 2 in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("GROWING TOGETHER Text in Quality Number tab 2 in Home Page Font Family is not in match with creative - Fail");
			bw.write("GROWING TOGETHER Text in Quality Number tab 2 in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				

		if(QualityN2TitleHPContent.equals(ExpQualityN2TitleHPContent))
		{
			System.out.println("GROWING TOGETHER Content in Quality Number tab 2 in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("GROWING TOGETHER Content in Quality Number tab 2 in Home Page is not in match with creative - Fail");
			bw.write("GROWING TOGETHER Content in Quality Number tab 2 in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexQualityN2TitleHPColor.equals(ExpQualityN2TitleHPColor) && QualityN2TitleHPFontSize.equals(ExpQualityN2TitleHPFontSize) && QualityN2TitleHPFontFamily.equals(ExpQualityN2TitleHPFontFamily) && QualityN2TitleHPContent.equals(ExpQualityN2TitleHPContent))
		{
			Label QualityN2TitleHPCSSP = new Label(5,15,"PASS"); 
			wsheet1.addCell(QualityN2TitleHPCSSP); 
		}
		else
		{
			Label QualityN2TitleHPCSSF = new Label(5,15,"FAIL"); 
			wsheet1.addCell(QualityN2TitleHPCSSF); 
		}
		
		// Quality Number tab 2 - Description
		
		WebElement QualityN2DescHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[4]/div/div[2]/div[1]/div[2]/div[3]/div[2]")); 
		String QualityN2DescHPColor = QualityN2DescHP.getCssValue("color");
		System.out.println("Actual Color of Description Text in Quality Number tab 2 in Home Page is : "	+ QualityN2DescHPColor );
		// Convert RGB to Hex Value
		Color HQualityN2DescHPColor = Color.fromString(QualityN2DescHPColor);
		String HexQualityN2DescHPColor = HQualityN2DescHPColor.asHex();
		System.out.println("Actual Hex Color of Description Text in Quality Number tab 2 in Home Page is : "	+ HexQualityN2DescHPColor );
		
		String QualityN2DescHPFontSize = QualityN2DescHP.getCssValue("font-size");
		System.out.println("Actual Font Size of Description Text in Quality Number tab 2 in Home Page is : "	+ QualityN2DescHPFontSize);
		String QualityN2DescHPFontFamily = QualityN2DescHP.getCssValue("font-family");
		System.out.println("Actual Font Family of Description Text in Quality Number tab 2 in Home Page is : "	+ QualityN2DescHPFontFamily);
		String QualityN2DescHPContent = QualityN2DescHP.getText();
		System.out.println("Actual GROWING TOGETHER Content in Home Page is : "	+ QualityN2DescHPContent);
							
		String ExpQualityN2DescHPColor = sheet1.getCell(1,16).getContents(); //column,row
		System.out.println("Expected Color of Description Text in Quality Number tab 2 in Home Page is : "	+ ExpQualityN2DescHPColor );
		String ExpQualityN2DescHPFontSize = sheet1.getCell(2,16).getContents();
		System.out.println("Expected Font Size of Description Text in Quality Number tab 2 in Home Page is : "	+ ExpQualityN2DescHPFontSize);
		String ExpQualityN2DescHPFontFamily = sheet1.getCell(3,16).getContents();
		System.out.println("Expected Font Family of Description Text in Quality Number tab 2 in Home Page is : "	+ ExpQualityN2DescHPFontFamily);
		String ExpQualityN2DescHPContent = sheet1.getCell(4,16).getContents();
		System.out.println("Expected GROWING TOGETHER Content in Home Page is : "	+ ExpQualityN2DescHPContent);
							
		if(HexQualityN2DescHPColor.equals(ExpQualityN2DescHPColor))
		{
			System.out.println("Description in Quality Number tab 2 in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Description in Quality Number tab 2 in Home Page Color is not in match with creative - Fail");
			bw.write("Description in Quality Number tab 2 in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
						
		if(QualityN2DescHPFontSize.equals(ExpQualityN2DescHPFontSize))
		{
			System.out.println("Description in Quality Number tab 2 in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Description in Quality Number tab 2 in Home Page Font Size is not in match with creative - Fail");
			bw.write("Description in Quality Number tab 2 in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
								
		if(QualityN2DescHPFontFamily.equals(ExpQualityN2DescHPFontFamily))
		{
			System.out.println("Description in Quality Number tab 2 in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Description in Quality Number tab 2 in Home Page Font Family is not in match with creative - Fail");
			bw.write("Description in Quality Number tab 2 in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
						
		if(QualityN2DescHPContent.equals(ExpQualityN2DescHPContent))
		{
			System.out.println("Description Content in Quality Number tab 2 in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Description Content in Quality Number tab 2 in Home Page is not in match with creative - Fail");
			bw.write("Description Content in Quality Number tab 2 in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
			
		if(HexQualityN2DescHPColor.equals(ExpQualityN2DescHPColor) && QualityN2DescHPFontSize.equals(ExpQualityN2DescHPFontSize) && QualityN2DescHPFontFamily.equals(ExpQualityN2DescHPFontFamily) && QualityN2DescHPContent.equals(ExpQualityN2DescHPContent))
		{
			Label QualityN2DescHPCSSP = new Label(5,16,"PASS"); 
			wsheet1.addCell(QualityN2DescHPCSSP); 
		}
		else
		{
			Label QualityN2DescHPCSSF = new Label(5,16,"FAIL"); 
			wsheet1.addCell(QualityN2DescHPCSSF); 
		}
		
		// Quality Number tab 3 - THE SCIENCE OF ROASTING
		//Click Action on Number tab 3
		Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='id_3_tab']")).click();  
		Thread.sleep(3000);
				                          		
		WebElement QualityN3TitleHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[4]/div/div[2]/div[1]/div[3]/div[1]")); 
		String QualityN3TitleHPColor = QualityN3TitleHP.getCssValue("color");
		System.out.println("Actual Color of THE SCIENCE OF ROASTING Text in Quality Number tab 3 in Home Page is : "	+ QualityN3TitleHPColor );
		// Convert RGB to Hex Value
		Color HQualityN3TitleHPColor = Color.fromString(QualityN3TitleHPColor);
		String HexQualityN3TitleHPColor = HQualityN3TitleHPColor.asHex();
		System.out.println("Actual Hex Color of THE SCIENCE OF ROASTING Text in Quality Number tab 3 in Home Page is : "	+ HexQualityN3TitleHPColor );
		
		String QualityN3TitleHPFontSize = QualityN3TitleHP.getCssValue("font-size");
		System.out.println("Actual Font Size of THE SCIENCE OF ROASTING Text in Quality Number tab 3 in Home Page is : "	+ QualityN3TitleHPFontSize);
		String QualityN3TitleHPFontFamily = QualityN3TitleHP.getCssValue("font-family");
		System.out.println("Actual Font Family of THE SCIENCE OF ROASTING Text in Quality Number tab 3 in Home Page is : "	+ QualityN3TitleHPFontFamily);
		String QualityN3TitleHPContent = QualityN3TitleHP.getText();
		System.out.println("Actual THE SCIENCE OF ROASTING Content in Quality Number tab 3 in Home Page is : "	+ QualityN3TitleHPContent);
								
		String ExpQualityN3TitleHPColor = sheet1.getCell(1,17).getContents(); //column,row
		System.out.println("Expected Color of THE SCIENCE OF ROASTING Text in Quality Number tab 3 in Home Page is : "	+ ExpQualityN3TitleHPColor );
		String ExpQualityN3TitleHPFontSize = sheet1.getCell(2,17).getContents();
		System.out.println("Expected Font Size of THE SCIENCE OF ROASTING Text in Quality Number tab 3 in Home Page is : "	+ ExpQualityN3TitleHPFontSize);
		String ExpQualityN3TitleHPFontFamily = sheet1.getCell(3,17).getContents();
		System.out.println("Expected Font Family of THE SCIENCE OF ROASTING Text in Quality Number tab 3 in Home Page is : "	+ ExpQualityN3TitleHPFontFamily);
		String ExpQualityN3TitleHPContent = sheet1.getCell(4,17).getContents();
		System.out.println("Expected THE SCIENCE OF ROASTING Content in Quality Number tab 3 in Home Page is : "	+ ExpQualityN3TitleHPContent);
							
		if(HexQualityN3TitleHPColor.equals(ExpQualityN3TitleHPColor))
		{
			System.out.println("THE SCIENCE OF ROASTING Text in Quality Number tab 3 in Home Page Color is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("THE SCIENCE OF ROASTING Text in Quality Number tab 3 in Home Page Color is not in match with creative - Fail");
			bw.write("THE SCIENCE OF ROASTING Text in Quality Number tab 3 in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
						
		if(QualityN3TitleHPFontSize.equals(ExpQualityN3TitleHPFontSize))
		{
			System.out.println("THE SCIENCE OF ROASTING Text in Quality Number tab 3 in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("THE SCIENCE OF ROASTING Text in Quality Number tab 3 in Home Page Font Size is not in match with creative - Fail");
			bw.write("THE SCIENCE OF ROASTING Text in Quality Number tab 3 in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
								
		if(QualityN3TitleHPFontFamily.equals(ExpQualityN3TitleHPFontFamily))
		{
			System.out.println("THE SCIENCE OF ROASTING Text in Quality Number tab 3 in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("THE SCIENCE OF ROASTING Text in Quality Number tab 3 in Home Page Font Family is not in match with creative - Fail");
			bw.write("THE SCIENCE OF ROASTING Text in Quality Number tab 3 in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
						

		if(QualityN3TitleHPContent.equals(ExpQualityN3TitleHPContent))
		{
			System.out.println("THE SCIENCE OF ROASTING Content in Quality Number tab 3 in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("THE SCIENCE OF ROASTING Content in Quality Number tab 3 in Home Page is not in match with creative - Fail");
			bw.write("THE SCIENCE OF ROASTING Content in Quality Number tab 3 in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexQualityN3TitleHPColor.equals(ExpQualityN3TitleHPColor) && QualityN3TitleHPFontSize.equals(ExpQualityN3TitleHPFontSize) && QualityN3TitleHPFontFamily.equals(ExpQualityN3TitleHPFontFamily) && QualityN3TitleHPContent.equals(ExpQualityN3TitleHPContent))
		{
			Label QualityN3TitleHPCSSP = new Label(5,17,"PASS"); 
			wsheet1.addCell(QualityN3TitleHPCSSP); 
		}
		else
		{
			Label QualityN3TitleHPCSSF = new Label(5,17,"FAIL"); 
			wsheet1.addCell(QualityN3TitleHPCSSF); 
		}
				
		// Quality Number tab 3 - Description
				
		WebElement QualityN3DescHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[4]/div/div[2]/div[1]/div[3]/div[3]/div[2]")); 
		String QualityN3DescHPColor = QualityN3DescHP.getCssValue("color");
		System.out.println("Actual Color of Description Text in Quality Number tab 3 in Home Page is : "	+ QualityN3DescHPColor );
		// Convert RGB to Hex Value
		Color HQualityN3DescHPColor = Color.fromString(QualityN3DescHPColor);
		String HexQualityN3DescHPColor = HQualityN3DescHPColor.asHex();
		System.out.println("Actual Hex Color of Description Text in Quality Number tab 3 in Home Page is : "	+ HexQualityN3DescHPColor );
		
		String QualityN3DescHPFontSize = QualityN3DescHP.getCssValue("font-size");
		System.out.println("Actual Font Size of Description Text in Quality Number tab 3 in Home Page is : "	+ QualityN3DescHPFontSize);
		String QualityN3DescHPFontFamily = QualityN3DescHP.getCssValue("font-family");
		System.out.println("Actual Font Family of Description Text in Quality Number tab 3 in Home Page is : "	+ QualityN3DescHPFontFamily);
		String QualityN3DescHPContent = QualityN3DescHP.getText();
		System.out.println("Actual THE SCIENCE OF ROASTING Content in Quality Number tab 3 in Home Page is : "	+ QualityN3DescHPContent);
									
		String ExpQualityN3DescHPColor = sheet1.getCell(1,18).getContents(); //column,row
		System.out.println("Expected Color of Description Text in Quality Number tab 3 in Home Page is : "	+ ExpQualityN3DescHPColor );
		String ExpQualityN3DescHPFontSize = sheet1.getCell(2,18).getContents();
		System.out.println("Expected Font Size of Description Text in Quality Number tab 3 in Home Page is : "	+ ExpQualityN3DescHPFontSize);
		String ExpQualityN3DescHPFontFamily = sheet1.getCell(3,18).getContents();
		System.out.println("Expected Font Family of Description Text in Quality Number tab 3 in Home Page is : "	+ ExpQualityN3DescHPFontFamily);
		String ExpQualityN3DescHPContent = sheet1.getCell(4,18).getContents();
		System.out.println("Expected THE SCIENCE OF ROASTING Content in Quality Number tab 3 in Home Page is : "	+ ExpQualityN3DescHPContent);
									
		if(HexQualityN3DescHPColor.equals(ExpQualityN3DescHPColor))
		{
			System.out.println("Description in Quality Number tab 3 in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Description in Quality Number tab 3 in Home Page Color is not in match with creative - Fail");
			bw.write("Description in Quality Number tab 3 in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
								
		if(QualityN3DescHPFontSize.equals(ExpQualityN3DescHPFontSize))
		{
			System.out.println("Description in Quality Number tab 3 in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Description in Quality Number tab 3 in Home Page Font Size is not in match with creative - Fail");
			bw.write("Description in Quality Number tab 3 in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
										
		if(QualityN3DescHPFontFamily.equals(ExpQualityN3DescHPFontFamily))
		{
			System.out.println("Description in Quality Number tab 3 in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Description in Quality Number tab 3 in Home Page Font Family is not in match with creative - Fail");
			bw.write("Description in Quality Number tab 3 in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
								
		if(QualityN3DescHPContent.equals(ExpQualityN3DescHPContent))
		{
			System.out.println("Description Content in Quality Number tab 3 in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Description Content in Quality Number tab 3 in Home Page is not in match with creative - Fail");
			bw.write("Description Content in Quality Number tab 3 in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexQualityN3DescHPColor.equals(ExpQualityN3DescHPColor) && QualityN3DescHPFontSize.equals(ExpQualityN3DescHPFontSize) && QualityN3DescHPFontFamily.equals(ExpQualityN3DescHPFontFamily) && QualityN3DescHPContent.equals(ExpQualityN3DescHPContent))
		{
			Label QualityN3DescHPCSSP = new Label(5,18,"PASS"); 
			wsheet1.addCell(QualityN3DescHPCSSP); 
		}
		else
		{
			Label QualityN3DescHPCSSF = new Label(5,18,"FAIL"); 
			wsheet1.addCell(QualityN3DescHPCSSF); 
		}
		
		Thread.sleep(3000);
		JavascriptExecutor jsevt = (JavascriptExecutor)driver;
		jsevt.executeScript("window.scrollBy(0,500)", "");
		Thread.sleep(8000);
		
		// Variety Title
		
		WebElement VarietyTitleHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[5]/div/div/div/div[1]/div[1]"));
		String VarietyTitleHPFontSize = VarietyTitleHP.getCssValue("font-size");
		System.out.println("Actual Font Size of Variety Title Text in Home Page is : "	+ VarietyTitleHPFontSize);
		String VarietyTitleHPFontFamily = VarietyTitleHP.getCssValue("font-family");
		System.out.println("Actual Font Family of Variety Title Text in Home Page is : "	+ VarietyTitleHPFontFamily);
		String VarietyTitleHPContent = VarietyTitleHP.getText();
		System.out.println("Actual Variety Title Content in Home Page is : "	+ VarietyTitleHPContent);
				 
		String ExpVarietyTitleHPFontSize = sheet1.getCell(2,19).getContents();
		System.out.println("Expected Font Size of Variety Title Text in Home Page is : "	+ ExpVarietyTitleHPFontSize);
		String ExpVarietyTitleHPFontFamily = sheet1.getCell(3,19).getContents();
		System.out.println("Expected Font Family of Variety Title Text in Home Page is : "	+ ExpVarietyTitleHPFontFamily);
		String ExpVarietyTitleHPContent = sheet1.getCell(4,19).getContents();
		System.out.println("Expected Variety Title Content in Home Page is : "	+ ExpVarietyTitleHPContent);
			
		if(VarietyTitleHPFontSize.equals(ExpVarietyTitleHPFontSize))
		{
			System.out.println("Variety Title Text in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Variety Title Text in Home Page Font Size is not in match with creative - Fail");
			bw.write("Variety Title Text in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
			
		if(VarietyTitleHPFontFamily.equals(ExpVarietyTitleHPFontFamily))
		{
			System.out.println("Variety Title Text in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Variety Title Text in Home Page Font Family is not in match with creative - Fail");
			bw.write("Variety Title Text in Home Page Color is Font Family in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(VarietyTitleHPContent.equals(ExpVarietyTitleHPContent))
		{
			System.out.println("Variety Title Content in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Variety Title Content in Home Page is not in match with creative - Fail");
			bw.write("Variety Title Content in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(VarietyTitleHPFontSize.equals(ExpVarietyTitleHPFontSize) && VarietyTitleHPFontFamily.equals(ExpVarietyTitleHPFontFamily) && VarietyTitleHPContent.equals(ExpVarietyTitleHPContent))
		{
			Label VarietyTitleHPCSSP = new Label(5,19,"PASS"); 
			wsheet1.addCell(VarietyTitleHPCSSP); 
		}
		else
		{
			Label VarietyTitleHPCSSF = new Label(5,19,"FAIL"); 
			wsheet1.addCell(VarietyTitleHPCSSF); 
		}
				
		// Variety Description
				
		WebElement VarietyDescHP = driver.findElement(By.xpath(".//*[@id='sk_id_variety']/div[1]/div[3]"));
		String VarietyDescHPFontSize = VarietyDescHP.getCssValue("font-size");
		System.out.println("Actual Font Size of Variety Desc Text in Home Page is : "	+ VarietyDescHPFontSize);
		String VarietyDescHPFontFamily = VarietyDescHP.getCssValue("font-family");
		System.out.println("Actual Font Family of Variety Desc Text in Home Page is : "	+ VarietyDescHPFontFamily);
		String VarietyDescHPContent = VarietyDescHP.getText();
		System.out.println("Actual Variety Desc Content in Home Page is : "	+ VarietyDescHPContent);
				 
		String ExpVarietyDescHPFontSize = sheet1.getCell(2,20).getContents();
		System.out.println("Expected Font Size of Variety Desc Text in Home Page is : "	+ ExpVarietyDescHPFontSize);
		String ExpVarietyDescHPFontFamily = sheet1.getCell(3,20).getContents();
		System.out.println("Expected Font Family of Variety Desc Text in Home Page is : "	+ ExpVarietyDescHPFontFamily);
		String ExpVarietyDescHPContent = sheet1.getCell(4,20).getContents();
		System.out.println("Expected Variety Desc Content in Home Page is : "	+ ExpVarietyDescHPContent);
				
		if(VarietyDescHPFontSize.equals(ExpVarietyDescHPFontSize))
		{
			System.out.println("Variety Desc Text in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Variety Desc Text in Home Page Font Size is not in match with creative - Fail");
			bw.write("Variety Desc Text in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
			
		if(VarietyDescHPFontFamily.equals(ExpVarietyDescHPFontFamily))
		{
			System.out.println("Variety Desc Text in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Variety Desc Text in Home Page Font Family is not in match with creative - Fail");
			bw.write("Variety Desc Text in Home Page Color is Font Family in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(VarietyDescHPContent.equals(ExpVarietyDescHPContent))
		{
			System.out.println("Variety Desc Content in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Variety Desc Content in Home Page is not in match with creative - Fail");
			bw.write("Variety Desc Content in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(VarietyDescHPFontSize.equals(ExpVarietyDescHPFontSize) && VarietyDescHPFontFamily.equals(ExpVarietyDescHPFontFamily) && VarietyDescHPContent.equals(ExpVarietyDescHPContent))
		{
			Label VarietyDescHPCSSP = new Label(5,20,"PASS"); 
			wsheet1.addCell(VarietyDescHPCSSP); 
		}
		else
		{
			Label VarietyDescHPCSSF = new Label(5,20,"FAIL"); 
			wsheet1.addCell(VarietyDescHPCSSF); 
		}
		
		Thread.sleep(3000);
		JavascriptExecutor jsevsb = (JavascriptExecutor)driver;
		jsevsb.executeScript("window.scrollBy(0,800)", "");
		Thread.sleep(8000);
		
		// Variety Text in Side Banner
		
		WebElement VarietySBHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[6]/div/div[3]/div/div[1]")); 
		String VarietySBHPColor = VarietySBHP.getCssValue("color");
		System.out.println("Actual Color of Variety Text in Side Banner in Home Page is : "	+ VarietySBHPColor );
		// Convert RGB to Hex Value
		Color HVarietySBHPColor = Color.fromString(VarietySBHPColor);
		String HexVarietySBHPColor = HVarietySBHPColor.asHex();
		System.out.println("Actual Hex Color of Variety Text in Side Banner in Home Page is : "	+ HexVarietySBHPColor );
		
		String VarietySBHPFontSize = VarietySBHP.getCssValue("font-size");
		System.out.println("Actual Font Size of Variety Text in Side Banner in Home Page is : "	+ VarietySBHPFontSize);
		String VarietySBHPFontFamily = VarietySBHP.getCssValue("font-family");
		System.out.println("Actual Font Family of Variety Text in Side Banner in Home Page is : "	+ VarietySBHPFontFamily);
						
		String ExpVarietySBHPColor = sheet1.getCell(1,21).getContents(); //column,row
		System.out.println("Expected Color of Variety Text in Side Banner in Home Page is : "	+ ExpVarietySBHPColor );
		String ExpVarietySBHPFontSize = sheet1.getCell(2,21).getContents();
		System.out.println("Expected Font Size of Variety Text in Side Banner in Home Page is : "	+ ExpVarietySBHPFontSize);
		String ExpVarietySBHPFontFamily = sheet1.getCell(3,21).getContents();
		System.out.println("Expected Font Family of Variety Text in Side Banner in Home Page is : "	+ ExpVarietySBHPFontFamily);
						
		if(HexVarietySBHPColor.equals(ExpVarietySBHPColor))
		{
			System.out.println("Variety Text in Side Banner in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Variety Text in Side Banner in Home Page Color is not in match with creative - Fail");
			bw.write("Variety Text in Side Banner in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
						
		if(VarietySBHPFontSize.equals(ExpVarietySBHPFontSize))
		{
			System.out.println("Variety Text in Side Banner in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Variety Text in Side Banner in Home Page Font Size is not in match with creative - Fail");
			bw.write("Variety Text in Side Banner in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
						
		if(VarietySBHPFontFamily.equals(ExpVarietySBHPFontFamily))
		{
			System.out.println("Variety Text in Side Banner in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Variety Text in Side Banner in Home Page Font Family is not in match with creative - Fail");
			bw.write("Variety Text in Side Banner in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexVarietySBHPColor.equals(ExpVarietySBHPColor) && VarietySBHPFontSize.equals(ExpVarietySBHPFontSize) && VarietySBHPFontFamily.equals(ExpVarietySBHPFontFamily))
		{
			Label VarietySBHPCSSP = new Label(5,21,"PASS"); 
			wsheet1.addCell(VarietySBHPCSSP); 
		}
		else
		{
			Label VarietySBHPCSSF = new Label(5,21,"FAIL"); 
			wsheet1.addCell(VarietySBHPCSSF); 
		}
						
		// Variety Side Banner - Everything for you
				
		WebElement VarietySBEveryHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[6]/div/div[3]/div/div[2]")); 
		String VarietySBEveryHPColor = VarietySBEveryHP.getCssValue("color");
		System.out.println("Actual Color of Everything for you Text in Variety Side Banner in Home Page is : "	+ VarietySBEveryHPColor );
		// Convert RGB to Hex Value
		Color HVarietySBEveryHPColor = Color.fromString(VarietySBEveryHPColor);
		String HexVarietySBEveryHPColor = HVarietySBEveryHPColor.asHex();
		System.out.println("Actual Hex Color of Everything for you Text in Variety Side Banner in Home Page is : "	+ HexVarietySBEveryHPColor );
		
		String VarietySBEveryHPFontSize = VarietySBEveryHP.getCssValue("font-size");
		System.out.println("Actual Font Size of Everything for you Text in Variety Side Banner in Home Page is : "	+ VarietySBEveryHPFontSize);
		String VarietySBEveryHPFontFamily = VarietySBEveryHP.getCssValue("font-family");
		System.out.println("Actual Font Family of Everything for you Text in Variety Side Banner in Home Page is : "	+ VarietySBEveryHPFontFamily);
		String VarietySBEveryHPContent = VarietySBEveryHP.getText();
		System.out.println("Actual Everything for you Content in Variety Side Banner in Home Page is : "	+ VarietySBEveryHPContent);
						
		String ExpVarietySBEveryHPColor = sheet1.getCell(1,22).getContents(); //column,row
		System.out.println("Expected Color of Everything for you Text in Variety Side Banner in Home Page is : "	+ ExpVarietySBEveryHPColor );
		String ExpVarietySBEveryHPFontSize = sheet1.getCell(2,22).getContents();
		System.out.println("Expected Font Size of Everything for you Text in Variety Side Banner in Home Page is : "	+ ExpVarietySBEveryHPFontSize);
		String ExpVarietySBEveryHPFontFamily = sheet1.getCell(3,22).getContents();
		System.out.println("Expected Font Family of Everything for you Text in Variety Side Banner in Home Page is : "	+ ExpVarietySBEveryHPFontFamily);
		String ExpVarietySBEveryHPContent = sheet1.getCell(4,22).getContents();
		System.out.println("Expected Everything for you Content in Variety Side Banner in Home Page is : "	+ ExpVarietySBEveryHPContent);
						
		if(HexVarietySBEveryHPColor.equals(ExpVarietySBEveryHPColor))
		{
			System.out.println("Everything for you Text in Variety Side Banner in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Everything for you Text in Variety Side Banner in Home Page Color is not in match with creative - Fail");
			bw.write("Everything for you Text in Variety Side Banner in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
						
		if(VarietySBEveryHPFontSize.equals(ExpVarietySBEveryHPFontSize))
		{
			System.out.println("Everything for you Text in Variety Side Banner in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Everything for you Text in Variety Side Banner in Home Page Font Size is not in match with creative - Fail");
			bw.write("Everything for you Text in Variety Side Banner in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
						
		if(VarietySBEveryHPFontFamily.equals(ExpVarietySBEveryHPFontFamily))
		{
			System.out.println("Everything for you Text in Variety Side Banner in Home Page Font Family is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("Everything for you Text in Variety Side Banner in Home Page Font Family is not in match with creative - Fail");
			bw.write("Everything for you Text in Variety Side Banner in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				

		if(VarietySBEveryHPContent.equals(ExpVarietySBEveryHPContent))
		{
			System.out.println("Everything for you Content in Variety side Banner in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Everything for you Content in Variety side Banner in Home Page is not in match with creative - Fail");
			bw.write("Everything for you Content in Variety side Banner in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
			
		if(HexVarietySBEveryHPColor.equals(ExpVarietySBEveryHPColor) && VarietySBEveryHPFontSize.equals(ExpVarietySBEveryHPFontSize) && VarietySBEveryHPFontFamily.equals(ExpVarietySBEveryHPFontFamily) && VarietySBEveryHPContent.equals(ExpVarietySBEveryHPContent))
		{
			Label VarietySBEveryHPCSSP = new Label(5,22,"PASS"); 
			wsheet1.addCell(VarietySBEveryHPCSSP); 
		}
		else
		{
			Label VarietySBEveryHPCSSF = new Label(5,22,"FAIL"); 
			wsheet1.addCell(VarietySBEveryHPCSSF); 
		}
				
		
		// Variety Side Banner Contents
				
		WebElement VarietySBContentsHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[6]/div/div[3]/div/div[4]")); 
		String VarietySBContentsHPColor = VarietySBContentsHP.getCssValue("color");
		System.out.println("Actual Color of Variety Side Banner Contents Text in Side Banner in Home Page is : "	+ VarietySBContentsHPColor );
		// Convert RGB to Hex Value
		Color HVarietySBContentsHPColor = Color.fromString(VarietySBContentsHPColor);
		String HexVarietySBContentsHPColor = HVarietySBContentsHPColor.asHex();
		System.out.println("Actual Hex Color of Variety Side Banner Contents Text in Side Banner in Home Page is : "	+ HexVarietySBContentsHPColor );
		
		String VarietySBContentsHPFontSize = VarietySBContentsHP.getCssValue("font-size");
		System.out.println("Actual Font Size of Variety Side Banner Contents Text in Side Banner in Home Page is : "	+ VarietySBContentsHPFontSize);
		String VarietySBContentsHPFontFamily = VarietySBContentsHP.getCssValue("font-family");
		System.out.println("Actual Font Family of Variety Side Banner Contents Text in Side Banner in Home Page is : "	+ VarietySBContentsHPFontFamily);
		String VarietySBContentsHPContent = VarietySBContentsHP.getText();
		System.out.println("Actual Variety Side Banner Contents in Home Page is : "	+ VarietySBContentsHPContent);
						
		String ExpVarietySBContentsHPColor = sheet1.getCell(1,23).getContents(); //column,row
		System.out.println("Expected Color of Variety Side Banner Contents Text in Side Banner in Home Page is : "	+ ExpVarietySBContentsHPColor );
		String ExpVarietySBContentsHPFontSize = sheet1.getCell(2,23).getContents();
		System.out.println("Expected Font Size of Variety Side Banner Contents Text in Side Banner in Home Page is : "	+ ExpVarietySBContentsHPFontSize);
		String ExpVarietySBContentsHPFontFamily = sheet1.getCell(3,23).getContents();
		System.out.println("Expected Font Family of Variety Side Banner Contents Text in Side Banner in Home Page is : "	+ ExpVarietySBContentsHPFontFamily);
		String ExpVarietySBContentsHPContent = sheet1.getCell(4,23).getContents();
		System.out.println("Expected Variety Side Banner Contents in Home Page is : "	+ ExpVarietySBContentsHPContent);
						
		if(HexVarietySBContentsHPColor.equals(ExpVarietySBContentsHPColor))
		{
			System.out.println("Variety Side Banner Contents Text in Side Banner in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Variety Side Banner Contents Text in Side Banner in Home Page Color is not in match with creative - Fail");
			bw.write("Variety Side Banner Contents Text in Side Banner in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
						
		if(VarietySBContentsHPFontSize.equals(ExpVarietySBContentsHPFontSize))
		{
			System.out.println("Variety Side Banner Contents Text in Side Banner in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Variety Side Banner Contents Text in Side Banner in Home Page Font Size is not in match with creative - Fail");
			bw.write("Variety Side Banner Contents Text in Side Banner in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
						
		if(VarietySBContentsHPFontFamily.equals(ExpVarietySBContentsHPFontFamily))
		{
			System.out.println("Variety Side Banner Contents Text in Side Banner in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Variety Side Banner Contents Text in Side Banner in Home Page Font Family is not in match with creative - Fail");
			bw.write("Variety Side Banner Contents Text in Side Banner in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(VarietySBContentsHPContent.equals(ExpVarietySBContentsHPContent))
		{
			System.out.println("Variety Side Banner Contents in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Variety Side Banner Contents in Home Page is not in match with creative - Fail");
			bw.write("Variety Side Banner Contents in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexVarietySBContentsHPColor.equals(ExpVarietySBContentsHPColor) && VarietySBContentsHPFontSize.equals(ExpVarietySBContentsHPFontSize) && VarietySBContentsHPFontFamily.equals(ExpVarietySBContentsHPFontFamily) && VarietySBContentsHPContent.equals(ExpVarietySBContentsHPContent))
		{
			Label VarietySBContentsHPCSSP = new Label(5,23,"PASS"); 
			wsheet1.addCell(VarietySBContentsHPCSSP); 
		}
		else
		{
			Label VarietySBContentsHPCSSF = new Label(5,23,"FAIL"); 
			wsheet1.addCell(VarietySBContentsHPCSSF); 
		}
				
		// Variety Side Banner - More about Variety
				
		WebElement VarietySBMoreHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[6]/div/div[3]/div/div[5]/div")); 
		String VarietySBMoreHPColor = VarietySBMoreHP.getCssValue("color");
		System.out.println("Actual Color of More about Variety Text in Side Banner in Home Page is : "	+ VarietySBMoreHPColor );
		// Convert RGB to Hex Value
		Color HVarietySBMoreHPColor = Color.fromString(VarietySBMoreHPColor);
		String HexVarietySBMoreHPColor = HVarietySBMoreHPColor.asHex();
		System.out.println("Actual Hex Color of More about Variety Text in Side Banner in Home Page is : "	+ HexVarietySBMoreHPColor );
		
		String VarietySBMoreHPFontSize = VarietySBMoreHP.getCssValue("font-size");
		System.out.println("Actual Font Size of More about Variety Text in Side Banner in Home Page is : "	+ VarietySBMoreHPFontSize);
		String VarietySBMoreHPFontFamily = VarietySBMoreHP.getCssValue("font-family");
		System.out.println("Actual Font Family of More about Variety Text in Side Banner in Home Page is : "	+ VarietySBMoreHPFontFamily);
		String VarietySBMoreHPContent = VarietySBMoreHP.getText();
		System.out.println("Actual More about Variety Content in Home Page is : "	+ VarietySBMoreHPContent);
						
		String ExpVarietySBMoreHPColor = sheet1.getCell(1,24).getContents(); //column,row
		System.out.println("Expected Color of More about Variety Text in Side Banner in Home Page is : "	+ ExpVarietySBMoreHPColor );
		String ExpVarietySBMoreHPFontSize = sheet1.getCell(2,24).getContents();
		System.out.println("Expected Font Size of More about Variety Text in Side Banner in Home Page is : "	+ ExpVarietySBMoreHPFontSize);
		String ExpVarietySBMoreHPFontFamily = sheet1.getCell(3,24).getContents();
		System.out.println("Expected Font Family of More about Variety Text in Side Banner in Home Page is : "	+ ExpVarietySBMoreHPFontFamily);
		String ExpVarietySBMoreHPContent = sheet1.getCell(4,24).getContents();
		System.out.println("Expected More about Variety Content in Home Page is : "	+ ExpVarietySBMoreHPContent);
						
		if(HexVarietySBMoreHPColor.equals(ExpVarietySBMoreHPColor))
		{
			System.out.println("More about Variety Text in Side Banner in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("More about Variety Text in Side Banner in Home Page Color is not in match with creative - Fail");
			bw.write("More about Variety Text in Side Banner in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
					
		if(VarietySBMoreHPFontSize.equals(ExpVarietySBMoreHPFontSize))
		{
			System.out.println("More about Variety Text in Side Banner in Home Page Font Size is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("More about Variety Text in Side Banner in Home Page Font Size is not in match with creative - Fail");
			bw.write("More about Variety Text in Side Banner in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
						
		if(VarietySBMoreHPFontFamily.equals(ExpVarietySBMoreHPFontFamily))
		{
			System.out.println("More about Variety Text in Side Banner in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("More about Variety Text in Side Banner in Home Page Font Family is not in match with creative - Fail");
			bw.write("More about Variety Text in Side Banner in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				

		if(VarietySBMoreHPContent.equals(ExpVarietySBMoreHPContent))
		{
			System.out.println("More about Variety Content in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("More about Variety Content in Home Page is not in match with creative - Fail");
			bw.write("More about Variety Content in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexVarietySBMoreHPColor.equals(ExpVarietySBMoreHPColor) && VarietySBMoreHPFontSize.equals(ExpVarietySBMoreHPFontSize) && VarietySBMoreHPFontFamily.equals(ExpVarietySBMoreHPFontFamily) && VarietySBMoreHPContent.equals(ExpVarietySBMoreHPContent))
		{
			Label VarietySBMoreHPCSSP = new Label(5,24,"PASS"); 
			wsheet1.addCell(VarietySBMoreHPCSSP); 
		}
		else
		{
			Label VarietySBMoreHPCSSF = new Label(5,24,"FAIL"); 
			wsheet1.addCell(VarietySBMoreHPCSSF); 
		}
		
		// Variety - OUR COFFEES Section
		
		WebElement VarietySBOurHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[6]/div/div[2]/div[1]/div[1]")); 
		String VarietySBOurHPColor = VarietySBOurHP.getCssValue("color");
		System.out.println("Actual Color of OUR COFFEES in Variety Side Banner in Home Page is : "	+ VarietySBOurHPColor );
		// Convert RGB to Hex Value
		Color HVarietySBOurHPColor = Color.fromString(VarietySBOurHPColor);
		String HexVarietySBOurHPColor = HVarietySBOurHPColor.asHex();
		System.out.println("Actual Hex Color of OUR COFFEES in Variety Side Banner in Home Page is : "	+ HexVarietySBOurHPColor );
		
		String VarietySBOurHPFontSize = VarietySBOurHP.getCssValue("font-size");
		System.out.println("Actual Font Size of OUR COFFEES in Variety Side Banner in Home Page is : "	+ VarietySBOurHPFontSize);
		String VarietySBOurHPFontFamily = VarietySBOurHP.getCssValue("font-family");
		System.out.println("Actual Font Family of OUR COFFEES in Variety Side Banner in Home Page is : "	+ VarietySBOurHPFontFamily);
		String VarietySBOurHPContent = VarietySBOurHP.getText();
		System.out.println("Actual Content of OUR COFFEES in Variety Side Banner in Home Page is : "	+ VarietySBOurHPContent);
						
		String ExpVarietySBOurHPColor = sheet1.getCell(1,40).getContents(); //column,row
		System.out.println("Expected Color of OUR COFFEES in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurHPColor );
		String ExpVarietySBOurHPFontSize = sheet1.getCell(2,40).getContents();
		System.out.println("Expected Font Size of OUR COFFEES in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurHPFontSize);
		String ExpVarietySBOurHPFontFamily = sheet1.getCell(3,40).getContents();
		System.out.println("Expected Font Family of OUR COFFEES in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurHPFontFamily);
		String ExpVarietySBOurHPContent = sheet1.getCell(4,40).getContents();
		System.out.println("Expected Content of OUR COFFEES in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurHPContent);
						
		if(HexVarietySBOurHPColor.equals(ExpVarietySBOurHPColor))
		{
			System.out.println("OUR COFFEES in Variety Side Banner in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES in Variety Side Banner in Home Page Color is not in match with creative - Fail");
			bw.write("OUR COFFEES in Variety Side Banner in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
					
		if(VarietySBOurHPFontSize.equals(ExpVarietySBOurHPFontSize))
		{
			System.out.println("OUR COFFEES in Variety Side Banner in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES in Variety Side Banner in Home Page Font Size is not in match with creative - Fail");
			bw.write("OUR COFFEES in Variety Side Banner in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
						
		if(VarietySBOurHPFontFamily.equals(ExpVarietySBOurHPFontFamily))
		{
			System.out.println("OUR COFFEES in Variety Side Banner in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES in Variety Side Banner in Home Page Font Family is not in match with creative - Fail");
			bw.write("OUR COFFEES in Variety Side Banner in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				

		if(VarietySBOurHPContent.equals(ExpVarietySBOurHPContent))
		{
			System.out.println("OUR COFFEES Content in Variety Side Banner in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Content in Variety Side Banner in Home Page is not in match with creative - Fail");
			bw.write("OUR COFFEES Content in Variety Side Banner in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexVarietySBOurHPColor.equals(ExpVarietySBOurHPColor) && VarietySBOurHPFontSize.equals(ExpVarietySBOurHPFontSize) && VarietySBOurHPFontFamily.equals(ExpVarietySBOurHPFontFamily) && VarietySBOurHPContent.equals(ExpVarietySBOurHPContent))
		{
			Label VarietySBOurHPCSSP = new Label(5,40,"PASS"); 
			wsheet1.addCell(VarietySBOurHPCSSP); 
		}
		else
		{
			Label VarietySBOurHPCSSF = new Label(5,40,"FAIL"); 
			wsheet1.addCell(VarietySBOurHPCSSF); 
		}
		
		// Variety - OUR COFFEES Section - REGULAR
		
		WebElement VarietySBOurRegHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[6]/div/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/span")); 
		String VarietySBOurRegHPColor = VarietySBOurRegHP.getCssValue("color");
		System.out.println("Actual Color of OUR COFFEES Regular in Variety Side Banner in Home Page is : "	+ VarietySBOurRegHPColor );
		// Convert RGB to Hex Value
		Color HVarietySBOurRegHPColor = Color.fromString(VarietySBOurRegHPColor);
		String HexVarietySBOurRegHPColor = HVarietySBOurRegHPColor.asHex();
		System.out.println("Actual Hex Color of OUR COFFEES Regular in Variety Side Banner in Home Page is : "	+ HexVarietySBOurRegHPColor );
		
		String VarietySBOurRegHPFontSize = VarietySBOurRegHP.getCssValue("font-size");
		System.out.println("Actual Font Size of OUR COFFEES Regular in Variety Side Banner in Home Page is : "	+ VarietySBOurRegHPFontSize);
		String VarietySBOurRegHPFontFamily = VarietySBOurRegHP.getCssValue("font-family");
		System.out.println("Actual Font Family of OUR COFFEES Regular in Variety Side Banner in Home Page is : "	+ VarietySBOurRegHPFontFamily);
		String VarietySBOurRegHPContent = VarietySBOurRegHP.getText();
		System.out.println("Actual Content of OUR COFFEES Regular in Variety Side Banner in Home Page is : "	+ VarietySBOurRegHPContent);
						
		String ExpVarietySBOurRegHPColor = sheet1.getCell(1,41).getContents(); //column,row
		System.out.println("Expected Color of OUR COFFEES Regular in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurRegHPColor );
		String ExpVarietySBOurRegHPFontSize = sheet1.getCell(2,41).getContents();
		System.out.println("Expected Font Size of OUR COFFEES Regular in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurRegHPFontSize);
		String ExpVarietySBOurRegHPFontFamily = sheet1.getCell(3,41).getContents();
		System.out.println("Expected Font Family of OUR COFFEES Regular in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurRegHPFontFamily);
		String ExpVarietySBOurRegHPContent = sheet1.getCell(4,41).getContents();
		System.out.println("Expected Content of OUR COFFEES Regular in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurRegHPContent);
						
		if(HexVarietySBOurRegHPColor.equals(ExpVarietySBOurRegHPColor))
		{
			System.out.println("OUR COFFEES Regular in Variety Side Banner in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Regular in Variety Side Banner in Home Page Color is not in match with creative - Fail");
			bw.write("OUR COFFEES Regular in Variety Side Banner in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
					
		if(VarietySBOurRegHPFontSize.equals(ExpVarietySBOurRegHPFontSize))
		{
			System.out.println("OUR COFFEES Regular in Variety Side Banner in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Regular in Variety Side Banner in Home Page Font Size is not in match with creative - Fail");
			bw.write("OUR COFFEES Regular in Variety Side Banner in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
						
		if(VarietySBOurRegHPFontFamily.equals(ExpVarietySBOurRegHPFontFamily))
		{
			System.out.println("OUR COFFEES Regular in Variety Side Banner in Home Page Font Family is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("OUR COFFEES Regular in Variety Side Banner in Home Page Font Family is not in match with creative - Fail");
			bw.write("OUR COFFEES Regular in Variety Side Banner in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				

		if(VarietySBOurRegHPContent.equals(ExpVarietySBOurRegHPContent))
		{
			System.out.println("OUR COFFEES Regular Content in Variety Side Banner in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Regular Content in Variety Side Banner in Home Page is not in match with creative - Fail");
			bw.write("OUR COFFEES Regular Content in Variety Side Banner in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexVarietySBOurRegHPColor.equals(ExpVarietySBOurRegHPColor) && VarietySBOurRegHPFontSize.equals(ExpVarietySBOurRegHPFontSize) && VarietySBOurRegHPFontFamily.equals(ExpVarietySBOurRegHPFontFamily) && VarietySBOurRegHPContent.equals(ExpVarietySBOurRegHPContent))
		{
			Label VarietySBOurRegHPCSSP = new Label(5,41,"PASS"); 
			wsheet1.addCell(VarietySBOurRegHPCSSP); 
		}
		else
		{
			Label VarietySBOurRegHPCSSF = new Label(5,41,"FAIL"); 
			wsheet1.addCell(VarietySBOurRegHPCSSF); 
		}
		
		// Variety - OUR COFFEES Section - REGULAR - Varieties: 18 
		
		WebElement VarietySBOurRegVHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[6]/div/div[2]/div[1]/div[2]/div[1]/div[2]/div[2]")); 
		String VarietySBOurRegVHPColor = VarietySBOurRegVHP.getCssValue("color");
		System.out.println("Actual Color of OUR COFFEES Regular Varieties in Variety Side Banner in Home Page is : "	+ VarietySBOurRegVHPColor );
		// Convert RGB to Hex Value
		Color HVarietySBOurRegVHPColor = Color.fromString(VarietySBOurRegVHPColor);
		String HexVarietySBOurRegVHPColor = HVarietySBOurRegVHPColor.asHex();
		System.out.println("Actual Hex Color of OUR COFFEES Regular Varieties in Variety Side Banner in Home Page is : "	+ HexVarietySBOurRegVHPColor );
		
		String VarietySBOurRegVHPFontSize = VarietySBOurRegVHP.getCssValue("font-size");
		System.out.println("Actual Font Size of OUR COFFEES Regular Varieties in Variety Side Banner in Home Page is : "	+ VarietySBOurRegVHPFontSize);
		String VarietySBOurRegVHPFontFamily = VarietySBOurRegVHP.getCssValue("font-family");
		System.out.println("Actual Font Family of OUR COFFEES Regular Varieties in Variety Side Banner in Home Page is : "	+ VarietySBOurRegVHPFontFamily);
		String VarietySBOurRegVHPContent = VarietySBOurRegVHP.getText();
		System.out.println("Actual Content of OUR COFFEES Regular Varieties in Variety Side Banner in Home Page is : "	+ VarietySBOurRegVHPContent);
						
		String ExpVarietySBOurRegVHPColor = sheet1.getCell(1,42).getContents(); //column,row
		System.out.println("Expected Color of OUR COFFEES Regular Varieties in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurRegVHPColor );
		String ExpVarietySBOurRegVHPFontSize = sheet1.getCell(2,42).getContents();
		System.out.println("Expected Font Size of OUR COFFEES Regular Varieties in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurRegVHPFontSize);
		String ExpVarietySBOurRegVHPFontFamily = sheet1.getCell(3,42).getContents();
		System.out.println("Expected Font Family of OUR COFFEES Regular Varieties in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurRegVHPFontFamily);
		String ExpVarietySBOurRegVHPContent = sheet1.getCell(4,42).getContents();
		System.out.println("Expected Content of OUR COFFEES Regular Varieties in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurRegVHPContent);
						
		if(HexVarietySBOurRegVHPColor.equals(ExpVarietySBOurRegVHPColor))
		{
			System.out.println("OUR COFFEES Regular Varieties in Variety Side Banner in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Regular Varieties in Variety Side Banner in Home Page Color is not in match with creative - Fail");
			bw.write("OUR COFFEES Regular Varieties in Variety Side Banner in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
					
		if(VarietySBOurRegVHPFontSize.equals(ExpVarietySBOurRegVHPFontSize))
		{
			System.out.println("OUR COFFEES Regular Varieties in Variety Side Banner in Home Page Font Size is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("OUR COFFEES Regular Varieties in Variety Side Banner in Home Page Font Size is not in match with creative - Fail");
			bw.write("OUR COFFEES Regular Varieties in Variety Side Banner in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
						
		if(VarietySBOurRegVHPFontFamily.equals(ExpVarietySBOurRegVHPFontFamily))
		{
			System.out.println("OUR COFFEES Regular Varieties in Variety Side Banner in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Regular Varieties in Variety Side Banner in Home Page Font Family is not in match with creative - Fail");
			bw.write("OUR COFFEES Regular Varieties in Variety Side Banner in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				

		if(VarietySBOurRegVHPContent.equals(ExpVarietySBOurRegVHPContent))
		{
			System.out.println("OUR COFFEES Regular Varieties Content in Variety Side Banner in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Regular Varieties Content in Variety Side Banner in Home Page is not in match with creative - Fail");
			bw.write("OUR COFFEES Regular Varieties Content in Variety Side Banner in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexVarietySBOurRegVHPColor.equals(ExpVarietySBOurRegVHPColor) && VarietySBOurRegVHPFontSize.equals(ExpVarietySBOurRegVHPFontSize) && VarietySBOurRegVHPFontFamily.equals(ExpVarietySBOurRegVHPFontFamily) && VarietySBOurRegVHPContent.equals(ExpVarietySBOurRegVHPContent))
		{
			Label VarietySBOurRegVHPCSSP = new Label(5,42,"PASS"); 
			wsheet1.addCell(VarietySBOurRegVHPCSSP); 
		}
		else
		{
			Label VarietySBOurRegVHPCSSF = new Label(5,42,"FAIL"); 
			wsheet1.addCell(VarietySBOurRegVHPCSSF); 
		}
		
		// Variety - OUR COFFEES Section - REGULAR - DESC
		
		WebElement VarietySBOurRegdHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[6]/div/div[2]/div[1]/div[2]/div[1]/div[2]/div[3]")); 
		String VarietySBOurRegdHPColor = VarietySBOurRegdHP.getCssValue("color");
		System.out.println("Actual Color of OUR COFFEES Regular Desc in Variety Side Banner in Home Page is : "	+ VarietySBOurRegdHPColor );
		// Convert RGB to Hex Value
		Color HVarietySBOurRegdHPColor = Color.fromString(VarietySBOurRegdHPColor);
		String HexVarietySBOurRegdHPColor = HVarietySBOurRegdHPColor.asHex();
		System.out.println("Actual Hex Color of OUR COFFEES Regular Desc in Variety Side Banner in Home Page is : "	+ HexVarietySBOurRegdHPColor );
		
		String VarietySBOurRegdHPFontSize = VarietySBOurRegdHP.getCssValue("font-size");
		System.out.println("Actual Font Size of OUR COFFEES Regular Desc in Variety Side Banner in Home Page is : "	+ VarietySBOurRegdHPFontSize);
		String VarietySBOurRegdHPFontFamily = VarietySBOurRegdHP.getCssValue("font-family");
		System.out.println("Actual Font Family of OUR COFFEES Regular Desc in Variety Side Banner in Home Page is : "	+ VarietySBOurRegdHPFontFamily);
		String VarietySBOurRegdHPContent = VarietySBOurRegdHP.getText();
		System.out.println("Actual Content of OUR COFFEES Regular Desc in Variety Side Banner in Home Page is : "	+ VarietySBOurRegdHPContent);
						
		String ExpVarietySBOurRegdHPColor = sheet1.getCell(1,43).getContents(); //column,row
		System.out.println("Expected Color of OUR COFFEES Regular Desc in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurRegdHPColor );
		String ExpVarietySBOurRegdHPFontSize = sheet1.getCell(2,43).getContents();
		System.out.println("Expected Font Size of OUR COFFEES Regular Desc in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurRegdHPFontSize);
		String ExpVarietySBOurRegdHPFontFamily = sheet1.getCell(3,43).getContents();
		System.out.println("Expected Font Family of OUR COFFEES Regular Desc in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurRegdHPFontFamily);
		String ExpVarietySBOurRegdHPContent = sheet1.getCell(4,43).getContents();
		System.out.println("Expected Content of OUR COFFEES Regular Desc in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurRegdHPContent);
						
		if(HexVarietySBOurRegdHPColor.equals(ExpVarietySBOurRegdHPColor))
		{
			System.out.println("OUR COFFEES Regular Desc in Variety Side Banner in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Regular Desc in Variety Side Banner in Home Page Color is not in match with creative - Fail");
			bw.write("OUR COFFEES Regular Desc in Variety Side Banner in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
					
		if(VarietySBOurRegdHPFontSize.equals(ExpVarietySBOurRegdHPFontSize))
		{
			System.out.println("OUR COFFEES Regular Desc in Variety Side Banner in Home Page Font Size is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("OUR COFFEES Regular Desc in Variety Side Banner in Home Page Font Size is not in match with creative - Fail");
			bw.write("OUR COFFEES Regular Desc in Variety Side Banner in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
						
		if(VarietySBOurRegdHPFontFamily.equals(ExpVarietySBOurRegdHPFontFamily))
		{
			System.out.println("OUR COFFEES Regular Desc in Variety Side Banner in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Regular Desc in Variety Side Banner in Home Page Font Family is not in match with creative - Fail");
			bw.write("OUR COFFEES Regular Desc in Variety Side Banner in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				

		if(VarietySBOurRegdHPContent.equals(ExpVarietySBOurRegdHPContent))
		{
			System.out.println("OUR COFFEES Regular Desc Content in Variety Side Banner in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Regular Desc Content in Variety Side Banner in Home Page is not in match with creative - Fail");
			bw.write("OUR COFFEES Regular Desc Content in Variety Side Banner in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexVarietySBOurRegdHPColor.equals(ExpVarietySBOurRegdHPColor) && VarietySBOurRegdHPFontSize.equals(ExpVarietySBOurRegdHPFontSize) && VarietySBOurRegdHPFontFamily.equals(ExpVarietySBOurRegdHPFontFamily) && VarietySBOurRegdHPContent.equals(ExpVarietySBOurRegdHPContent))
		{
			Label VarietySBOurRegdHPCSSP = new Label(5,43,"PASS"); 
			wsheet1.addCell(VarietySBOurRegdHPCSSP); 
		}
		else
		{
			Label VarietySBOurRegdHPCSSF = new Label(5,43,"FAIL"); 
			wsheet1.addCell(VarietySBOurRegdHPCSSF); 
		}
		
		// Variety - OUR COFFEES Section - Flavored
		//click action on Variety - Flavored
		
		Thread.sleep(3000);
		//JavascriptExecutor jsevf = (JavascriptExecutor)driver;
		//jsevf.executeScript("window.scrollBy(0,1050)", "");
		//Thread.sleep(3000);
		//jsevf.executeScript("window.scrollBy(0,600)", "");
		//Thread.sleep(3000);
		//jsevf.executeScript("window.scrollBy(0,600)", "");
		//Thread.sleep(8000);
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[6]/div/div[2]/div[2]/div[1]/div/div[2]/img")).click();  
		Thread.sleep(3000);
		
		WebElement VarietySBOurFlaHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[6]/div/div[2]/div[1]/div[2]/div[2]/div[2]/div[1]/span")); 
		String VarietySBOurFlaHPColor = VarietySBOurFlaHP.getCssValue("color");
		System.out.println("Actual Color of OUR COFFEES Flavored in Variety Side Banner in Home Page is : "	+ VarietySBOurFlaHPColor );
		// Convert RGB to Hex Value
		Color HVarietySBOurFlaHPColor = Color.fromString(VarietySBOurFlaHPColor);
		String HexVarietySBOurFlaHPColor = HVarietySBOurFlaHPColor.asHex();
		System.out.println("Actual Hex Color of OUR COFFEES Flavored in Variety Side Banner in Home Page is : "	+ HexVarietySBOurFlaHPColor );
		
		String VarietySBOurFlaHPFontSize = VarietySBOurFlaHP.getCssValue("font-size");
		System.out.println("Actual Font Size of OUR COFFEES Flavored in Variety Side Banner in Home Page is : "	+ VarietySBOurFlaHPFontSize);
		String VarietySBOurFlaHPFontFamily = VarietySBOurFlaHP.getCssValue("font-family");
		System.out.println("Actual Font Family of OUR COFFEES Flavored in Variety Side Banner in Home Page is : "	+ VarietySBOurFlaHPFontFamily);
		String VarietySBOurFlaHPContent = VarietySBOurFlaHP.getText();
		System.out.println("Actual Content of OUR COFFEES Flavored in Variety Side Banner in Home Page is : "	+ VarietySBOurFlaHPContent);
						
		String ExpVarietySBOurFlaHPColor = sheet1.getCell(1,44).getContents(); //column,row
		System.out.println("Expected Color of OUR COFFEES Flavored in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurFlaHPColor );
		String ExpVarietySBOurFlaHPFontSize = sheet1.getCell(2,44).getContents();
		System.out.println("Expected Font Size of OUR COFFEES Flavored in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurFlaHPFontSize);
		String ExpVarietySBOurFlaHPFontFamily = sheet1.getCell(3,44).getContents();
		System.out.println("Expected Font Family of OUR COFFEES Flavored in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurFlaHPFontFamily);
		String ExpVarietySBOurFlaHPContent = sheet1.getCell(4,44).getContents();
		System.out.println("Expected Content of OUR COFFEES Flavored in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurFlaHPContent);
						
		if(HexVarietySBOurFlaHPColor.equals(ExpVarietySBOurFlaHPColor))
		{
			System.out.println("OUR COFFEES Flavored in Variety Side Banner in Home Page Color is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("OUR COFFEES Flavored in Variety Side Banner in Home Page Color is not in match with creative - Fail");
			bw.write("OUR COFFEES Flavored in Variety Side Banner in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
					
		if(VarietySBOurFlaHPFontSize.equals(ExpVarietySBOurFlaHPFontSize))
		{
			System.out.println("OUR COFFEES Flavored in Variety Side Banner in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Flavored in Variety Side Banner in Home Page Font Size is not in match with creative - Fail");
			bw.write("OUR COFFEES Flavored in Variety Side Banner in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
						
		if(VarietySBOurFlaHPFontFamily.equals(ExpVarietySBOurFlaHPFontFamily))
		{
			System.out.println("OUR COFFEES Flavored in Variety Side Banner in Home Page Font Family is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("OUR COFFEES Flavored in Variety Side Banner in Home Page Font Family is not in match with creative - Fail");
			bw.write("OUR COFFEES Flavored in Variety Side Banner in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				

		if(VarietySBOurFlaHPContent.equals(ExpVarietySBOurFlaHPContent))
		{
			System.out.println("OUR COFFEES Flavored Content in Variety Side Banner in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Flavored Content in Variety Side Banner in Home Page is not in match with creative - Fail");
			bw.write("OUR COFFEES Flavored Content in Variety Side Banner in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexVarietySBOurFlaHPColor.equals(ExpVarietySBOurFlaHPColor) && VarietySBOurFlaHPFontSize.equals(ExpVarietySBOurFlaHPFontSize) && VarietySBOurFlaHPFontFamily.equals(ExpVarietySBOurFlaHPFontFamily) && VarietySBOurFlaHPContent.equals(ExpVarietySBOurFlaHPContent))
		{
			Label VarietySBOurFlaHPCSSP = new Label(5,44,"PASS"); 
			wsheet1.addCell(VarietySBOurFlaHPCSSP); 
		}
		else
		{
			Label VarietySBOurFlaHPCSSF = new Label(5,44,"FAIL"); 
			wsheet1.addCell(VarietySBOurFlaHPCSSF); 
		}
		
		// Variety - OUR COFFEES Section - Flavored - Varieties: 7 
		
		WebElement VarietySBOurFlaVHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[6]/div/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]")); 
		String VarietySBOurFlaVHPColor = VarietySBOurFlaVHP.getCssValue("color");
		System.out.println("Actual Color of OUR COFFEES Flavored Varieties in Variety Side Banner in Home Page is : "	+ VarietySBOurFlaVHPColor );
		// Convert RGB to Hex Value
		Color HVarietySBOurFlaVHPColor = Color.fromString(VarietySBOurFlaVHPColor);
		String HexVarietySBOurFlaVHPColor = HVarietySBOurFlaVHPColor.asHex();
		System.out.println("Actual Hex Color of OUR COFFEES Flavored Varieties in Variety Side Banner in Home Page is : "	+ HexVarietySBOurFlaVHPColor );
		
		String VarietySBOurFlaVHPFontSize = VarietySBOurFlaVHP.getCssValue("font-size");
		System.out.println("Actual Font Size of OUR COFFEES Flavored Varieties in Variety Side Banner in Home Page is : "	+ VarietySBOurFlaVHPFontSize);
		String VarietySBOurFlaVHPFontFamily = VarietySBOurFlaVHP.getCssValue("font-family");
		System.out.println("Actual Font Family of OUR COFFEES Flavored Varieties in Variety Side Banner in Home Page is : "	+ VarietySBOurFlaVHPFontFamily);
		String VarietySBOurFlaVHPContent = VarietySBOurFlaVHP.getText();
		System.out.println("Actual Content of OUR COFFEES Flavored Varieties in Variety Side Banner in Home Page is : "	+ VarietySBOurFlaVHPContent);
						
		String ExpVarietySBOurFlaVHPColor = sheet1.getCell(1,45).getContents(); //column,row
		System.out.println("Expected Color of OUR COFFEES Flavored Varieties in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurFlaVHPColor );
		String ExpVarietySBOurFlaVHPFontSize = sheet1.getCell(2,45).getContents();
		System.out.println("Expected Font Size of OUR COFFEES Flavored Varieties in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurFlaVHPFontSize);
		String ExpVarietySBOurFlaVHPFontFamily = sheet1.getCell(3,45).getContents();
		System.out.println("Expected Font Family of OUR COFFEES Flavored Varieties in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurFlaVHPFontFamily);
		String ExpVarietySBOurFlaVHPContent = sheet1.getCell(4,45).getContents();
		System.out.println("Expected Content of OUR COFFEES Flavored Varieties in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurFlaVHPContent);
						
		if(HexVarietySBOurFlaVHPColor.equals(ExpVarietySBOurFlaVHPColor))
		{
			System.out.println("OUR COFFEES Flavored Varieties in Variety Side Banner in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Flavored Varieties in Variety Side Banner in Home Page Color is not in match with creative - Fail");
			bw.write("OUR COFFEES Flavored Varieties in Variety Side Banner in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
					
		if(VarietySBOurFlaVHPFontSize.equals(ExpVarietySBOurFlaVHPFontSize))
		{
			System.out.println("OUR COFFEES Flavored Varieties in Variety Side Banner in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Flavored Varieties in Variety Side Banner in Home Page Font Size is not in match with creative - Fail");
			bw.write("OUR COFFEES Flavored Varieties in Variety Side Banner in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
						
		if(VarietySBOurFlaVHPFontFamily.equals(ExpVarietySBOurFlaVHPFontFamily))
		{
			System.out.println("OUR COFFEES Flavored Varieties in Variety Side Banner in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Flavored Varieties in Variety Side Banner in Home Page Font Family is not in match with creative - Fail");
			bw.write("OUR COFFEES Flavored Varieties in Variety Side Banner in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				

		if(VarietySBOurFlaVHPContent.equals(ExpVarietySBOurFlaVHPContent))
		{
			System.out.println("OUR COFFEES Flavored Varieties Content in Variety Side Banner in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Flavored Varieties Content in Variety Side Banner in Home Page is not in match with creative - Fail");
			bw.write("OUR COFFEES Flavored Varieties Content in Variety Side Banner in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexVarietySBOurFlaVHPColor.equals(ExpVarietySBOurFlaVHPColor) && VarietySBOurFlaVHPFontSize.equals(ExpVarietySBOurFlaVHPFontSize) && VarietySBOurFlaVHPFontFamily.equals(ExpVarietySBOurFlaVHPFontFamily) && VarietySBOurFlaVHPContent.equals(ExpVarietySBOurFlaVHPContent))
		{
			Label VarietySBOurFlaVHPCSSP = new Label(5,45,"PASS"); 
			wsheet1.addCell(VarietySBOurFlaVHPCSSP); 
		}
		else
		{
			Label VarietySBOurFlaVHPCSSF = new Label(5,45,"FAIL"); 
			wsheet1.addCell(VarietySBOurFlaVHPCSSF); 
		}
		
		// Variety - OUR COFFEES Section - Flavored - DESC
		
		WebElement VarietySBOurFladHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[6]/div/div[2]/div[1]/div[2]/div[2]/div[2]/div[3]")); 
		String VarietySBOurFladHPColor = VarietySBOurFladHP.getCssValue("color");
		System.out.println("Actual Color of OUR COFFEES Flavored Desc in Variety Side Banner in Home Page is : "	+ VarietySBOurFladHPColor );
		// Convert RGB to Hex Value
		Color HVarietySBOurFladHPColor = Color.fromString(VarietySBOurFladHPColor);
		String HexVarietySBOurFladHPColor = HVarietySBOurFladHPColor.asHex();
		System.out.println("Actual Hex Color of OUR COFFEES Flavored Desc in Variety Side Banner in Home Page is : "	+ HexVarietySBOurFladHPColor );
		
		String VarietySBOurFladHPFontSize = VarietySBOurFladHP.getCssValue("font-size");
		System.out.println("Actual Font Size of OUR COFFEES Flavored Desc in Variety Side Banner in Home Page is : "	+ VarietySBOurFladHPFontSize);
		String VarietySBOurFladHPFontFamily = VarietySBOurFladHP.getCssValue("font-family");
		System.out.println("Actual Font Family of OUR COFFEES Flavored Desc in Variety Side Banner in Home Page is : "	+ VarietySBOurFladHPFontFamily);
		String VarietySBOurFladHPContent = VarietySBOurFladHP.getText();
		System.out.println("Actual Content of OUR COFFEES Flavored Desc in Variety Side Banner in Home Page is : "	+ VarietySBOurFladHPContent);
						
		String ExpVarietySBOurFladHPColor = sheet1.getCell(1,46).getContents(); //column,row
		System.out.println("Expected Color of OUR COFFEES Flavored Desc in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurFladHPColor );
		String ExpVarietySBOurFladHPFontSize = sheet1.getCell(2,46).getContents();
		System.out.println("Expected Font Size of OUR COFFEES Flavored Desc in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurFladHPFontSize);
		String ExpVarietySBOurFladHPFontFamily = sheet1.getCell(3,46).getContents();
		System.out.println("Expected Font Family of OUR COFFEES Flavored Desc in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurFladHPFontFamily);
		String ExpVarietySBOurFladHPContent = sheet1.getCell(4,46).getContents();
		System.out.println("Expected Content of OUR COFFEES Flavored Desc in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurFladHPContent);
						
		if(HexVarietySBOurFladHPColor.equals(ExpVarietySBOurFladHPColor))
		{
			System.out.println("OUR COFFEES Flavored Desc in Variety Side Banner in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Flavored Desc in Variety Side Banner in Home Page Color is not in match with creative - Fail");
			bw.write("OUR COFFEES Flavored Desc in Variety Side Banner in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
					
		if(VarietySBOurFladHPFontSize.equals(ExpVarietySBOurFladHPFontSize))
		{
			System.out.println("OUR COFFEES Flavored Desc in Variety Side Banner in Home Page Font Size is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("OUR COFFEES Flavored Desc in Variety Side Banner in Home Page Font Size is not in match with creative - Fail");
			bw.write("OUR COFFEES Flavored Desc in Variety Side Banner in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
						
		if(VarietySBOurFladHPFontFamily.equals(ExpVarietySBOurFladHPFontFamily))
		{
			System.out.println("OUR COFFEES Flavored Desc in Variety Side Banner in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Flavored Desc in Variety Side Banner in Home Page Font Family is not in match with creative - Fail");
			bw.write("OUR COFFEES Flavored Desc in Variety Side Banner in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				

		if(VarietySBOurFladHPContent.equals(ExpVarietySBOurFladHPContent))
		{
			System.out.println("OUR COFFEES Flavored Desc Content in Variety Side Banner in Home Page is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("OUR COFFEES Flavored Desc Content in Variety Side Banner in Home Page is not in match with creative - Fail");
			bw.write("OUR COFFEES Flavored Desc Content in Variety Side Banner in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexVarietySBOurFladHPColor.equals(ExpVarietySBOurFladHPColor) && VarietySBOurFladHPFontSize.equals(ExpVarietySBOurFladHPFontSize) && VarietySBOurFladHPFontFamily.equals(ExpVarietySBOurFladHPFontFamily) && VarietySBOurFladHPContent.equals(ExpVarietySBOurFladHPContent))
		{
			Label VarietySBOurFladHPCSSP = new Label(5,46,"PASS"); 
			wsheet1.addCell(VarietySBOurFladHPCSSP); 
		}
		else
		{
			Label VarietySBOurFladHPCSSF = new Label(5,46,"FAIL"); 
			wsheet1.addCell(VarietySBOurFladHPCSSF); 
		}
		
		//Variety - OUR COFFEES Section - Decaf
		//click action on Variety - Decaf
		
		Thread.sleep(3000);
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[6]/div/div[2]/div[2]/div[1]/div/div[3]/img")).click();  
		Thread.sleep(3000);
		
		WebElement VarietySBOurDecafHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[6]/div/div[2]/div[1]/div[2]/div[3]/div[2]/div[1]/span")); 
		String VarietySBOurDecafHPColor = VarietySBOurDecafHP.getCssValue("color");
		System.out.println("Actual Color of OUR COFFEES Decaf in Variety Side Banner in Home Page is : "	+ VarietySBOurDecafHPColor );
		// Convert RGB to Hex Value
		Color HVarietySBOurDecafHPColor = Color.fromString(VarietySBOurDecafHPColor);
		String HexVarietySBOurDecafHPColor = HVarietySBOurDecafHPColor.asHex();
		System.out.println("Actual Hex Color of OUR COFFEES Decaf in Variety Side Banner in Home Page is : "	+ HexVarietySBOurDecafHPColor );
		
		String VarietySBOurDecafHPFontSize = VarietySBOurDecafHP.getCssValue("font-size");
		System.out.println("Actual Font Size of OUR COFFEES Decaf in Variety Side Banner in Home Page is : "	+ VarietySBOurDecafHPFontSize);
		String VarietySBOurDecafHPFontFamily = VarietySBOurDecafHP.getCssValue("font-family");
		System.out.println("Actual Font Family of OUR COFFEES Decaf in Variety Side Banner in Home Page is : "	+ VarietySBOurDecafHPFontFamily);
		String VarietySBOurDecafHPContent = VarietySBOurDecafHP.getText();
		System.out.println("Actual Content of OUR COFFEES Decaf in Variety Side Banner in Home Page is : "	+ VarietySBOurDecafHPContent);
						
		String ExpVarietySBOurDecafHPColor = sheet1.getCell(1,47).getContents(); //column,row
		System.out.println("Expected Color of OUR COFFEES Decaf in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurDecafHPColor );
		String ExpVarietySBOurDecafHPFontSize = sheet1.getCell(2,47).getContents();
		System.out.println("Expected Font Size of OUR COFFEES Decaf in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurDecafHPFontSize);
		String ExpVarietySBOurDecafHPFontFamily = sheet1.getCell(3,47).getContents();
		System.out.println("Expected Font Family of OUR COFFEES Decaf in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurDecafHPFontFamily);
		String ExpVarietySBOurDecafHPContent = sheet1.getCell(4,47).getContents();
		System.out.println("Expected Content of OUR COFFEES Decaf in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurDecafHPContent);
						
		if(HexVarietySBOurDecafHPColor.equals(ExpVarietySBOurDecafHPColor))
		{
			System.out.println("OUR COFFEES Decaf in Variety Side Banner in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Decaf in Variety Side Banner in Home Page Color is not in match with creative - Fail");
			bw.write("OUR COFFEES Decaf in Variety Side Banner in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
					
		if(VarietySBOurDecafHPFontSize.equals(ExpVarietySBOurDecafHPFontSize))
		{
			System.out.println("OUR COFFEES Decaf in Variety Side Banner in Home Page Font Size is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("OUR COFFEES Decaf in Variety Side Banner in Home Page Font Size is not in match with creative - Fail");
			bw.write("OUR COFFEES Decaf in Variety Side Banner in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
						
		if(VarietySBOurDecafHPFontFamily.equals(ExpVarietySBOurDecafHPFontFamily))
		{
			System.out.println("OUR COFFEES Decaf in Variety Side Banner in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Decaf in Variety Side Banner in Home Page Font Family is not in match with creative - Fail");
			bw.write("OUR COFFEES Decaf in Variety Side Banner in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				

		if(VarietySBOurDecafHPContent.equals(ExpVarietySBOurDecafHPContent))
		{
			System.out.println("OUR COFFEES Decaf Content in Variety Side Banner in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Decaf Content in Variety Side Banner in Home Page is not in match with creative - Fail");
			bw.write("OUR COFFEES Decaf Content in Variety Side Banner in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		} 
		
		if(HexVarietySBOurDecafHPColor.equals(ExpVarietySBOurDecafHPColor) && VarietySBOurDecafHPFontSize.equals(ExpVarietySBOurDecafHPFontSize) && VarietySBOurDecafHPFontFamily.equals(ExpVarietySBOurDecafHPFontFamily) && VarietySBOurDecafHPContent.equals(ExpVarietySBOurDecafHPContent))
		{
			Label VarietySBOurDecafHPCSSP = new Label(5,47,"PASS"); 
			wsheet1.addCell(VarietySBOurDecafHPCSSP); 
		}
		else
		{
			Label VarietySBOurDecafHPCSSF = new Label(5,47,"FAIL"); 
			wsheet1.addCell(VarietySBOurDecafHPCSSF); 
		}
		
		//Variety - OUR COFFEES Section - Decaf - Varieties: 7
		
		WebElement VarietySBOurDecafVHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[6]/div/div[2]/div[1]/div[2]/div[3]/div[2]/div[2]")); 
		String VarietySBOurDecafVHPColor = VarietySBOurDecafVHP.getCssValue("color");
		System.out.println("Actual Color of OUR COFFEES Decaf Varieties in Variety Side Banner in Home Page is : "	+ VarietySBOurDecafVHPColor );
		// Convert RGB to Hex Value
		Color HVarietySBOurDecafVHPColor = Color.fromString(VarietySBOurDecafVHPColor);
		String HexVarietySBOurDecafVHPColor = HVarietySBOurDecafVHPColor.asHex();
		System.out.println("Actual Hex Color of OUR COFFEES Decaf Varieties in Variety Side Banner in Home Page is : "	+ HexVarietySBOurDecafVHPColor );
		
		String VarietySBOurDecafVHPFontSize = VarietySBOurDecafVHP.getCssValue("font-size");
		System.out.println("Actual Font Size of OUR COFFEES Decaf Varieties in Variety Side Banner in Home Page is : "	+ VarietySBOurDecafVHPFontSize);
		String VarietySBOurDecafVHPFontFamily = VarietySBOurDecafVHP.getCssValue("font-family");
		System.out.println("Actual Font Family of OUR COFFEES Decaf Varieties in Variety Side Banner in Home Page is : "	+ VarietySBOurDecafVHPFontFamily);
		String VarietySBOurDecafVHPContent = VarietySBOurDecafVHP.getText();
		System.out.println("Actual Content of OUR COFFEES Decaf Varieties in Variety Side Banner in Home Page is : "	+ VarietySBOurDecafVHPContent);
						
		String ExpVarietySBOurDecafVHPColor = sheet1.getCell(1,48).getContents(); //column,row
		System.out.println("Expected Color of OUR COFFEES Decaf Varieties in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurDecafVHPColor );
		String ExpVarietySBOurDecafVHPFontSize = sheet1.getCell(2,48).getContents();
		System.out.println("Expected Font Size of OUR COFFEES Decaf Varieties in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurDecafVHPFontSize);
		String ExpVarietySBOurDecafVHPFontFamily = sheet1.getCell(3,48).getContents();
		System.out.println("Expected Font Family of OUR COFFEES Decaf Varieties in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurDecafVHPFontFamily);
		String ExpVarietySBOurDecafVHPContent = sheet1.getCell(4,48).getContents();
		System.out.println("Expected Content of OUR COFFEES Decaf Varieties in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurDecafVHPContent);
						
		if(HexVarietySBOurDecafVHPColor.equals(ExpVarietySBOurDecafVHPColor))
		{
			System.out.println("OUR COFFEES Decaf Varieties in Variety Side Banner in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Decaf Varieties in Variety Side Banner in Home Page Color is not in match with creative - Fail");
			bw.write("OUR COFFEES Decaf Varieties in Variety Side Banner in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
					
		if(VarietySBOurDecafVHPFontSize.equals(ExpVarietySBOurDecafVHPFontSize))
		{
			System.out.println("OUR COFFEES Decaf Varieties in Variety Side Banner in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Decaf Varieties in Variety Side Banner in Home Page Font Size is not in match with creative - Fail");
			bw.write("OUR COFFEES Decaf Varieties in Variety Side Banner in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
						
		if(VarietySBOurDecafVHPFontFamily.equals(ExpVarietySBOurDecafVHPFontFamily))
		{
			System.out.println("OUR COFFEES Decaf Varieties in Variety Side Banner in Home Page Font Family is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("OUR COFFEES Decaf Varieties in Variety Side Banner in Home Page Font Family is not in match with creative - Fail");
			bw.write("OUR COFFEES Decaf Varieties in Variety Side Banner in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				

		if(VarietySBOurDecafVHPContent.equals(ExpVarietySBOurDecafVHPContent))
		{
			System.out.println("OUR COFFEES Decaf Varieties Content in Variety Side Banner in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Decaf Varieties Content in Variety Side Banner in Home Page is not in match with creative - Fail");
			bw.write("OUR COFFEES Decaf Varieties Content in Variety Side Banner in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		} 
		
		if(HexVarietySBOurDecafVHPColor.equals(ExpVarietySBOurDecafVHPColor) && VarietySBOurDecafVHPFontSize.equals(ExpVarietySBOurDecafVHPFontSize) && VarietySBOurDecafVHPFontFamily.equals(ExpVarietySBOurDecafVHPFontFamily) && VarietySBOurDecafVHPContent.equals(ExpVarietySBOurDecafVHPContent))
		{
			Label VarietySBOurDecafVHPCSSP = new Label(5,48,"PASS"); 
			wsheet1.addCell(VarietySBOurDecafVHPCSSP); 
		}
		else
		{
			Label VarietySBOurDecafVHPCSSF = new Label(5,48,"FAIL"); 
			wsheet1.addCell(VarietySBOurDecafVHPCSSF); 
		}
		
		//Variety - OUR COFFEES Section - Decaf - Desc
		
		WebElement VarietySBOurDecafdHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[6]/div/div[2]/div[1]/div[2]/div[3]/div[2]/div[3]")); 
		String VarietySBOurDecafdHPColor = VarietySBOurDecafdHP.getCssValue("color");
		System.out.println("Actual Color of OUR COFFEES Decaf Desc in Variety Side Banner in Home Page is : "	+ VarietySBOurDecafdHPColor );
		// Convert RGB to Hex Value
		Color HVarietySBOurDecafdHPColor = Color.fromString(VarietySBOurDecafdHPColor);
		String HexVarietySBOurDecafdHPColor = HVarietySBOurDecafdHPColor.asHex();
		System.out.println("Actual Hex Color of OUR COFFEES Decaf Desc in Variety Side Banner in Home Page is : "	+ HexVarietySBOurDecafdHPColor );
		
		String VarietySBOurDecafdHPFontSize = VarietySBOurDecafdHP.getCssValue("font-size");
		System.out.println("Actual Font Size of OUR COFFEES Decaf Desc in Variety Side Banner in Home Page is : "	+ VarietySBOurDecafdHPFontSize);
		String VarietySBOurDecafdHPFontFamily = VarietySBOurDecafdHP.getCssValue("font-family");
		System.out.println("Actual Font Family of OUR COFFEES Decaf Desc in Variety Side Banner in Home Page is : "	+ VarietySBOurDecafdHPFontFamily);
		String VarietySBOurDecafdHPContent = VarietySBOurDecafdHP.getText();
		System.out.println("Actual Content of OUR COFFEES Decaf Desc in Variety Side Banner in Home Page is : "	+ VarietySBOurDecafdHPContent);
						
		String ExpVarietySBOurDecafdHPColor = sheet1.getCell(1,49).getContents(); //column,row
		System.out.println("Expected Color of OUR COFFEES Decaf Desc in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurDecafdHPColor );
		String ExpVarietySBOurDecafdHPFontSize = sheet1.getCell(2,49).getContents();
		System.out.println("Expected Font Size of OUR COFFEES Decaf Desc in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurDecafdHPFontSize);
		String ExpVarietySBOurDecafdHPFontFamily = sheet1.getCell(3,49).getContents();
		System.out.println("Expected Font Family of OUR COFFEES Decaf Desc in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurDecafdHPFontFamily);
		String ExpVarietySBOurDecafdHPContent = sheet1.getCell(4,49).getContents();
		System.out.println("Expected Content of OUR COFFEES Decaf Desc in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurDecafdHPContent);
						
		if(HexVarietySBOurDecafdHPColor.equals(ExpVarietySBOurDecafdHPColor))
		{
			System.out.println("OUR COFFEES Decaf Desc in Variety Side Banner in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Decaf Desc in Variety Side Banner in Home Page Color is not in match with creative - Fail");
			bw.write("OUR COFFEES Decaf Desc in Variety Side Banner in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
					
		if(VarietySBOurDecafdHPFontSize.equals(ExpVarietySBOurDecafdHPFontSize))
		{
			System.out.println("OUR COFFEES Decaf Desc in Variety Side Banner in Home Page Font Size is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("OUR COFFEES Decaf Desc in Variety Side Banner in Home Page Font Size is not in match with creative - Fail");
			bw.write("OUR COFFEES Decaf Desc in Variety Side Banner in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
						
		if(VarietySBOurDecafdHPFontFamily.equals(ExpVarietySBOurDecafdHPFontFamily))
		{
			System.out.println("OUR COFFEES Decaf Desc in Variety Side Banner in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Decaf Desc in Variety Side Banner in Home Page Font Family is not in match with creative - Fail");
			bw.write("OUR COFFEES Decaf Desc in Variety Side Banner in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				

		if(VarietySBOurDecafdHPContent.equals(ExpVarietySBOurDecafdHPContent))
		{
			System.out.println("OUR COFFEES Decaf Desc Content in Variety Side Banner in Home Page is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("OUR COFFEES Decaf Desc Content in Variety Side Banner in Home Page is not in match with creative - Fail");
			bw.write("OUR COFFEES Decaf Desc Content in Variety Side Banner in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		} 
		
		if(HexVarietySBOurDecafdHPColor.equals(ExpVarietySBOurDecafdHPColor) && VarietySBOurDecafdHPFontSize.equals(ExpVarietySBOurDecafdHPFontSize) && VarietySBOurDecafdHPFontFamily.equals(ExpVarietySBOurDecafdHPFontFamily) && VarietySBOurDecafdHPContent.equals(ExpVarietySBOurDecafdHPContent))
		{
			Label VarietySBOurDecafdHPCSSP = new Label(5,49,"PASS"); 
			wsheet1.addCell(VarietySBOurDecafdHPCSSP); 
		}
		else
		{
			Label VarietySBOurDecafdHPCSSF = new Label(5,49,"FAIL"); 
			wsheet1.addCell(VarietySBOurDecafdHPCSSF); 
		}
		
		//Variety - OUR COFFEES Section - Seasonal
		//click action on Variety Arrow and Seasonal
		
		Thread.sleep(3000);
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[6]/div/div[2]/div[2]/div[3]")).click();  
		Thread.sleep(3000);
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[6]/div/div[2]/div[2]/div[1]/div/div[4]/img")).click();
		
		WebElement VarietySBOurSeaHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[6]/div/div[2]/div[1]/div[2]/div[4]/div[2]/div[1]/span")); 
		String VarietySBOurSeaHPColor = VarietySBOurSeaHP.getCssValue("color");
		System.out.println("Actual Color of OUR COFFEES Seasonal in Variety Side Banner in Home Page is : "	+ VarietySBOurSeaHPColor );
		// Convert RGB to Hex Value
		Color HVarietySBOurSeaHPColor = Color.fromString(VarietySBOurSeaHPColor);
		String HexVarietySBOurSeaHPColor = HVarietySBOurSeaHPColor.asHex();
		System.out.println("Actual Hex Color of OUR COFFEES Seasonal in Variety Side Banner in Home Page is : "	+ HexVarietySBOurSeaHPColor );
		
		String VarietySBOurSeaHPFontSize = VarietySBOurSeaHP.getCssValue("font-size");
		System.out.println("Actual Font Size of OUR COFFEES Seasonal in Variety Side Banner in Home Page is : "	+ VarietySBOurSeaHPFontSize);
		String VarietySBOurSeaHPFontFamily = VarietySBOurSeaHP.getCssValue("font-family");
		System.out.println("Actual Font Family of OUR COFFEES Seasonal in Variety Side Banner in Home Page is : "	+ VarietySBOurSeaHPFontFamily);
		String VarietySBOurSeaHPContent = VarietySBOurSeaHP.getText();
		System.out.println("Actual Content of OUR COFFEES Seasonal in Variety Side Banner in Home Page is : "	+ VarietySBOurSeaHPContent);
						
		String ExpVarietySBOurSeaHPColor = sheet1.getCell(1,50).getContents(); //column,row
		System.out.println("Expected Color of OUR COFFEES Seasonal in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurSeaHPColor );
		String ExpVarietySBOurSeaHPFontSize = sheet1.getCell(2,50).getContents();
		System.out.println("Expected Font Size of OUR COFFEES Seasonal in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurSeaHPFontSize);
		String ExpVarietySBOurSeaHPFontFamily = sheet1.getCell(3,50).getContents();
		System.out.println("Expected Font Family of OUR COFFEES Seasonal in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurSeaHPFontFamily);
		String ExpVarietySBOurSeaHPContent = sheet1.getCell(4,50).getContents();
		System.out.println("Expected Content of OUR COFFEES Seasonal in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurSeaHPContent);
						
		if(HexVarietySBOurSeaHPColor.equals(ExpVarietySBOurSeaHPColor))
		{
			System.out.println("OUR COFFEES Seasonal in Variety Side Banner in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Seasonal in Variety Side Banner in Home Page Color is not in match with creative - Fail");
			bw.write("OUR COFFEES Seasonal in Variety Side Banner in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
					
		if(VarietySBOurSeaHPFontSize.equals(ExpVarietySBOurSeaHPFontSize))
		{
			System.out.println("OUR COFFEES Seasonal in Variety Side Banner in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Seasonal in Variety Side Banner in Home Page Font Size is not in match with creative - Fail");
			bw.write("OUR COFFEES Seasonal in Variety Side Banner in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
						
		if(VarietySBOurSeaHPFontFamily.equals(ExpVarietySBOurSeaHPFontFamily))
		{
			System.out.println("OUR COFFEES Seasonal in Variety Side Banner in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Seasonal in Variety Side Banner in Home Page Font Family is not in match with creative - Fail");
			bw.write("OUR COFFEES Seasonal in Variety Side Banner in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				

		if(VarietySBOurSeaHPContent.equals(ExpVarietySBOurSeaHPContent))
		{
			System.out.println("OUR COFFEES Seasonal Content in Variety Side Banner in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Seasonal Content in Variety Side Banner in Home Page is not in match with creative - Fail");
			bw.write("OUR COFFEES Seasonal Content in Variety Side Banner in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		} 
		
		if(HexVarietySBOurSeaHPColor.equals(ExpVarietySBOurSeaHPColor) && VarietySBOurSeaHPFontSize.equals(ExpVarietySBOurSeaHPFontSize) && VarietySBOurSeaHPFontFamily.equals(ExpVarietySBOurSeaHPFontFamily) && VarietySBOurSeaHPContent.equals(ExpVarietySBOurSeaHPContent))
		{
			Label VarietySBOurSeaHPCSSP = new Label(5,50,"PASS"); 
			wsheet1.addCell(VarietySBOurSeaHPCSSP); 
		}
		else
		{
			Label VarietySBOurSeaHPCSSF = new Label(5,50,"FAIL"); 
			wsheet1.addCell(VarietySBOurSeaHPCSSF); 
		}
		
		
		//Variety - OUR COFFEES Section - Seasonal - Varieties: 9
		
		WebElement VarietySBOurSeaVHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[6]/div/div[2]/div[1]/div[2]/div[4]/div[2]/div[2]")); 
		String VarietySBOurSeaVHPColor = VarietySBOurSeaVHP.getCssValue("color");
		System.out.println("Actual Color of OUR COFFEES Seasonal Varieties in Variety Side Banner in Home Page is : "	+ VarietySBOurSeaVHPColor );
		// Convert RGB to Hex Value
		Color HVarietySBOurSeaVHPColor = Color.fromString(VarietySBOurSeaVHPColor);
		String HexVarietySBOurSeaVHPColor = HVarietySBOurSeaVHPColor.asHex();
		System.out.println("Actual Hex Color of OUR COFFEES Seasonal Varieties in Variety Side Banner in Home Page is : "	+ HexVarietySBOurSeaVHPColor );
		
		String VarietySBOurSeaVHPFontSize = VarietySBOurSeaVHP.getCssValue("font-size");
		System.out.println("Actual Font Size of OUR COFFEES Seasonal Varieties in Variety Side Banner in Home Page is : "	+ VarietySBOurSeaVHPFontSize);
		String VarietySBOurSeaVHPFontFamily = VarietySBOurSeaVHP.getCssValue("font-family");
		System.out.println("Actual Font Family of OUR COFFEES Seasonal Varieties in Variety Side Banner in Home Page is : "	+ VarietySBOurSeaVHPFontFamily);
		String VarietySBOurSeaVHPContent = VarietySBOurSeaVHP.getText();
		System.out.println("Actual Content of OUR COFFEES Seasonal Varieties in Variety Side Banner in Home Page is : "	+ VarietySBOurSeaVHPContent);
						
		String ExpVarietySBOurSeaVHPColor = sheet1.getCell(1,51).getContents(); //column,row
		System.out.println("Expected Color of OUR COFFEES Seasonal Varieties in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurSeaVHPColor );
		String ExpVarietySBOurSeaVHPFontSize = sheet1.getCell(2,51).getContents();
		System.out.println("Expected Font Size of OUR COFFEES Seasonal Varieties in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurSeaVHPFontSize);
		String ExpVarietySBOurSeaVHPFontFamily = sheet1.getCell(3,51).getContents();
		System.out.println("Expected Font Family of OUR COFFEES Seasonal Varieties in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurSeaVHPFontFamily);
		String ExpVarietySBOurSeaVHPContent = sheet1.getCell(4,51).getContents();
		System.out.println("Expected Content of OUR COFFEES Seasonal Varieties in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurSeaVHPContent);
						
		if(HexVarietySBOurSeaVHPColor.equals(ExpVarietySBOurSeaVHPColor))
		{
			System.out.println("OUR COFFEES Seasonal Varieties in Variety Side Banner in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Seasonal Varieties in Variety Side Banner in Home Page Color is not in match with creative - Fail");
			bw.write("OUR COFFEES Seasonal Varieties in Variety Side Banner in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
					
		if(VarietySBOurSeaVHPFontSize.equals(ExpVarietySBOurSeaVHPFontSize))
		{
			System.out.println("OUR COFFEES Seasonal Varieties in Variety Side Banner in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Seasonal Varieties in Variety Side Banner in Home Page Font Size is not in match with creative - Fail");
			bw.write("OUR COFFEES Seasonal Varieties in Variety Side Banner in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
						
		if(VarietySBOurSeaVHPFontFamily.equals(ExpVarietySBOurSeaVHPFontFamily))
		{
			System.out.println("OUR COFFEES Seasonal Varieties in Variety Side Banner in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Seasonal Varieties in Variety Side Banner in Home Page Font Family is not in match with creative - Fail");
			bw.write("OUR COFFEES Seasonal Varieties in Variety Side Banner in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				

		if(VarietySBOurSeaVHPContent.equals(ExpVarietySBOurSeaVHPContent))
		{
			System.out.println("OUR COFFEES Seasonal Varieties Content in Variety Side Banner in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Seasonal Varieties Content in Variety Side Banner in Home Page is not in match with creative - Fail");
			bw.write("OUR COFFEES Seasonal Varieties Content in Variety Side Banner in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		} 
		
		if(HexVarietySBOurSeaVHPColor.equals(ExpVarietySBOurSeaVHPColor) && VarietySBOurSeaVHPFontSize.equals(ExpVarietySBOurSeaVHPFontSize) && VarietySBOurSeaVHPFontFamily.equals(ExpVarietySBOurSeaVHPFontFamily) && VarietySBOurSeaVHPContent.equals(ExpVarietySBOurSeaVHPContent))
		{
			Label VarietySBOurSeaVHPCSSP = new Label(5,51,"PASS"); 
			wsheet1.addCell(VarietySBOurSeaVHPCSSP); 
		}
		else
		{
			Label VarietySBOurSeaVHPCSSF = new Label(5,51,"FAIL"); 
			wsheet1.addCell(VarietySBOurSeaVHPCSSF); 
		}
		
		
		
		//Variety - OUR COFFEES Section - Seasonal - Desc
		
		WebElement VarietySBOurSeadHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[6]/div/div[2]/div[1]/div[2]/div[4]/div[2]/div[3]")); 
		String VarietySBOurSeadHPColor = VarietySBOurSeadHP.getCssValue("color");
		System.out.println("Actual Color of OUR COFFEES Seasonal Desc in Variety Side Banner in Home Page is : "	+ VarietySBOurSeadHPColor );
		// Convert RGB to Hex Value
		Color HVarietySBOurSeadHPColor = Color.fromString(VarietySBOurSeadHPColor);
		String HexVarietySBOurSeadHPColor = HVarietySBOurSeadHPColor.asHex();
		System.out.println("Actual Hex Color of OUR COFFEES Seasonal Desc in Variety Side Banner in Home Page is : "	+ HexVarietySBOurSeadHPColor );
		
		String VarietySBOurSeadHPFontSize = VarietySBOurSeadHP.getCssValue("font-size");
		System.out.println("Actual Font Size of OUR COFFEES Seasonal Desc in Variety Side Banner in Home Page is : "	+ VarietySBOurSeadHPFontSize);
		String VarietySBOurSeadHPFontFamily = VarietySBOurSeadHP.getCssValue("font-family");
		System.out.println("Actual Font Family of OUR COFFEES Seasonal Desc in Variety Side Banner in Home Page is : "	+ VarietySBOurSeadHPFontFamily);
		String VarietySBOurSeadHPContent = VarietySBOurSeadHP.getText();
		System.out.println("Actual Content of OUR COFFEES Seasonal Desc in Variety Side Banner in Home Page is : "	+ VarietySBOurSeadHPContent);
						
		String ExpVarietySBOurSeadHPColor = sheet1.getCell(1,52).getContents(); //column,row
		System.out.println("Expected Color of OUR COFFEES Seasonal Desc in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurSeadHPColor );
		String ExpVarietySBOurSeadHPFontSize = sheet1.getCell(2,52).getContents();
		System.out.println("Expected Font Size of OUR COFFEES Seasonal Desc in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurSeadHPFontSize);
		String ExpVarietySBOurSeadHPFontFamily = sheet1.getCell(3,52).getContents();
		System.out.println("Expected Font Family of OUR COFFEES Seasonal Desc in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurSeadHPFontFamily);
		String ExpVarietySBOurSeadHPContent = sheet1.getCell(4,52).getContents();
		System.out.println("Expected Content of OUR COFFEES Seasonal Desc in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurSeadHPContent);
						
		if(HexVarietySBOurSeadHPColor.equals(ExpVarietySBOurSeadHPColor))
		{
			System.out.println("OUR COFFEES Seasonal Desc in Variety Side Banner in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Seasonal Desc in Variety Side Banner in Home Page Color is not in match with creative - Fail");
			bw.write("OUR COFFEES Seasonal Desc in Variety Side Banner in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
					
		if(VarietySBOurSeadHPFontSize.equals(ExpVarietySBOurSeadHPFontSize))
		{
			System.out.println("OUR COFFEES Seasonal Desc in Variety Side Banner in Home Page Font Size is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("OUR COFFEES Seasonal Desc in Variety Side Banner in Home Page Font Size is not in match with creative - Fail");
			bw.write("OUR COFFEES Seasonal Desc in Variety Side Banner in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
						
		if(VarietySBOurSeadHPFontFamily.equals(ExpVarietySBOurSeadHPFontFamily))
		{
			System.out.println("OUR COFFEES Seasonal Desc in Variety Side Banner in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Seasonal Desc in Variety Side Banner in Home Page Font Family is not in match with creative - Fail");
			bw.write("OUR COFFEES Seasonal Desc in Variety Side Banner in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				

		if(VarietySBOurSeadHPContent.equals(ExpVarietySBOurSeadHPContent))
		{
			System.out.println("OUR COFFEES Seasonal Desc Content in Variety Side Banner in Home Page is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("OUR COFFEES Seasonal Desc Content in Variety Side Banner in Home Page is not in match with creative - Fail");
			bw.write("OUR COFFEES Seasonal Desc Content in Variety Side Banner in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		} 
		
		if(HexVarietySBOurSeadHPColor.equals(ExpVarietySBOurSeadHPColor) && VarietySBOurSeadHPFontSize.equals(ExpVarietySBOurSeadHPFontSize) && VarietySBOurSeadHPFontFamily.equals(ExpVarietySBOurSeadHPFontFamily) && VarietySBOurSeadHPContent.equals(ExpVarietySBOurSeadHPContent))
		{
			Label VarietySBOurSeadHPCSSP = new Label(5,52,"PASS"); 
			wsheet1.addCell(VarietySBOurSeadHPCSSP); 
		}
		else
		{
			Label VarietySBOurSeadHPCSSF = new Label(5,52,"FAIL"); 
			wsheet1.addCell(VarietySBOurSeadHPCSSF); 
		}
		
		//Variety - OUR COFFEES Section - Coffeehouse
		//click action on Variety Coffeehouse
		
		Thread.sleep(3000);
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[6]/div/div[2]/div[2]/div[1]/div/div[5]/img")).click();
		
		WebElement VarietySBOurCofHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[6]/div/div[2]/div[1]/div[2]/div[5]/div[2]/div[1]/span")); 
		String VarietySBOurCofHPColor = VarietySBOurCofHP.getCssValue("color");
		System.out.println("Actual Color of OUR COFFEES Coffeehouse in Variety Side Banner in Home Page is : "	+ VarietySBOurCofHPColor );
		// Convert RGB to Hex Value
		Color HVarietySBOurCofHPColor = Color.fromString(VarietySBOurCofHPColor);
		String HexVarietySBOurCofHPColor = HVarietySBOurCofHPColor.asHex();
		System.out.println("Actual Hex Color of OUR COFFEES Coffeehouse in Variety Side Banner in Home Page is : "	+ HexVarietySBOurCofHPColor );
		
		String VarietySBOurCofHPFontSize = VarietySBOurCofHP.getCssValue("font-size");
		System.out.println("Actual Font Size of OUR COFFEES Coffeehouse in Variety Side Banner in Home Page is : "	+ VarietySBOurCofHPFontSize);
		String VarietySBOurCofHPFontFamily = VarietySBOurCofHP.getCssValue("font-family");
		System.out.println("Actual Font Family of OUR COFFEES Coffeehouse in Variety Side Banner in Home Page is : "	+ VarietySBOurCofHPFontFamily);
		String VarietySBOurCofHPContent = VarietySBOurCofHP.getText();
		System.out.println("Actual Content of OUR COFFEES Coffeehouse in Variety Side Banner in Home Page is : "	+ VarietySBOurCofHPContent);
						
		String ExpVarietySBOurCofHPColor = sheet1.getCell(1,53).getContents(); //column,row
		System.out.println("Expected Color of OUR COFFEES Coffeehouse in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurCofHPColor );
		String ExpVarietySBOurCofHPFontSize = sheet1.getCell(2,53).getContents();
		System.out.println("Expected Font Size of OUR COFFEES Coffeehouse in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurCofHPFontSize);
		String ExpVarietySBOurCofHPFontFamily = sheet1.getCell(3,53).getContents();
		System.out.println("Expected Font Family of OUR COFFEES Coffeehouse in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurCofHPFontFamily);
		String ExpVarietySBOurCofHPContent = sheet1.getCell(4,53).getContents();
		System.out.println("Expected Content of OUR COFFEES Coffeehouse in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurCofHPContent);
						
		if(HexVarietySBOurCofHPColor.equals(ExpVarietySBOurCofHPColor))
		{
			System.out.println("OUR COFFEES Coffeehouse in Variety Side Banner in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Coffeehouse in Variety Side Banner in Home Page Color is not in match with creative - Fail");
			bw.write("OUR COFFEES Coffeehouse in Variety Side Banner in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
					
		if(VarietySBOurCofHPFontSize.equals(ExpVarietySBOurCofHPFontSize))
		{
			System.out.println("OUR COFFEES Coffeehouse in Variety Side Banner in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Coffeehouse in Variety Side Banner in Home Page Font Size is not in match with creative - Fail");
			bw.write("OUR COFFEES Coffeehouse in Variety Side Banner in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
						
		if(VarietySBOurCofHPFontFamily.equals(ExpVarietySBOurCofHPFontFamily))
		{
			System.out.println("OUR COFFEES Coffeehouse in Variety Side Banner in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Coffeehouse in Variety Side Banner in Home Page Font Family is not in match with creative - Fail");
			bw.write("OUR COFFEES Coffeehouse in Variety Side Banner in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				

		if(VarietySBOurCofHPContent.equals(ExpVarietySBOurCofHPContent))
		{
			System.out.println("OUR COFFEES Coffeehouse Content in Variety Side Banner in Home Page is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("OUR COFFEES Coffeehouse Content in Variety Side Banner in Home Page is not in match with creative - Fail");
			bw.write("OUR COFFEES Coffeehouse Content in Variety Side Banner in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		} 
		
		if(HexVarietySBOurCofHPColor.equals(ExpVarietySBOurCofHPColor) && VarietySBOurCofHPFontSize.equals(ExpVarietySBOurCofHPFontSize) && VarietySBOurCofHPFontFamily.equals(ExpVarietySBOurCofHPFontFamily) && VarietySBOurCofHPContent.equals(ExpVarietySBOurCofHPContent))
		{
			Label VarietySBOurCofHPCSSP = new Label(5,53,"PASS"); 
			wsheet1.addCell(VarietySBOurCofHPCSSP); 
		}
		else
		{
			Label VarietySBOurCofHPCSSF = new Label(5,53,"FAIL"); 
			wsheet1.addCell(VarietySBOurCofHPCSSF); 
		}
		
		//Variety - OUR COFFEES Section - Coffeehouse - Varieties: 3
		
		WebElement VarietySBOurCofVHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[6]/div/div[2]/div[1]/div[2]/div[5]/div[2]/div[2]")); 
		String VarietySBOurCofVHPColor = VarietySBOurCofVHP.getCssValue("color");
		System.out.println("Actual Color of OUR COFFEES Coffeehouse Varieties in Variety Side Banner in Home Page is : "	+ VarietySBOurCofVHPColor );
		// Convert RGB to Hex Value
		Color HVarietySBOurCofVHPColor = Color.fromString(VarietySBOurCofVHPColor);
		String HexVarietySBOurCofVHPColor = HVarietySBOurCofVHPColor.asHex();
		System.out.println("Actual Hex Color of OUR COFFEES Coffeehouse Varieties in Variety Side Banner in Home Page is : "	+ HexVarietySBOurCofVHPColor );
		
		String VarietySBOurCofVHPFontSize = VarietySBOurCofVHP.getCssValue("font-size");
		System.out.println("Actual Font Size of OUR COFFEES Coffeehouse Varieties in Variety Side Banner in Home Page is : "	+ VarietySBOurCofVHPFontSize);
		String VarietySBOurCofVHPFontFamily = VarietySBOurCofVHP.getCssValue("font-family");
		System.out.println("Actual Font Family of OUR COFFEES Coffeehouse Varieties in Variety Side Banner in Home Page is : "	+ VarietySBOurCofVHPFontFamily);
		String VarietySBOurCofVHPContent = VarietySBOurCofVHP.getText();
		System.out.println("Actual Content of OUR COFFEES Coffeehouse Varieties in Variety Side Banner in Home Page is : "	+ VarietySBOurCofVHPContent);
						
		String ExpVarietySBOurCofVHPColor = sheet1.getCell(1,54).getContents(); //column,row
		System.out.println("Expected Color of OUR COFFEES Coffeehouse Varieties in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurCofVHPColor );
		String ExpVarietySBOurCofVHPFontSize = sheet1.getCell(2,54).getContents();
		System.out.println("Expected Font Size of OUR COFFEES Coffeehouse Varieties in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurCofVHPFontSize);
		String ExpVarietySBOurCofVHPFontFamily = sheet1.getCell(3,54).getContents();
		System.out.println("Expected Font Family of OUR COFFEES Coffeehouse Varieties in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurCofVHPFontFamily);
		String ExpVarietySBOurCofVHPContent = sheet1.getCell(4,54).getContents();
		System.out.println("Expected Content of OUR COFFEES Coffeehouse Varieties in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurCofVHPContent);
						
		if(HexVarietySBOurCofVHPColor.equals(ExpVarietySBOurCofVHPColor))
		{
			System.out.println("OUR COFFEES Coffeehouse Varieties in Variety Side Banner in Home Page Color is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("OUR COFFEES Coffeehouse Varieties in Variety Side Banner in Home Page Color is not in match with creative - Fail");
			bw.write("OUR COFFEES Coffeehouse Varieties in Variety Side Banner in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
					
		if(VarietySBOurCofVHPFontSize.equals(ExpVarietySBOurCofVHPFontSize))
		{
			System.out.println("OUR COFFEES Coffeehouse Varieties in Variety Side Banner in Home Page Font Size is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("OUR COFFEES Coffeehouse Varieties in Variety Side Banner in Home Page Font Size is not in match with creative - Fail");
			bw.write("OUR COFFEES Coffeehouse Varieties in Variety Side Banner in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
						
		if(VarietySBOurCofVHPFontFamily.equals(ExpVarietySBOurCofVHPFontFamily))
		{
			System.out.println("OUR COFFEES Coffeehouse Varieties in Variety Side Banner in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Coffeehouse Varieties in Variety Side Banner in Home Page Font Family is not in match with creative - Fail");
			bw.write("OUR COFFEES Coffeehouse Varieties in Variety Side Banner in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				

		if(VarietySBOurCofVHPContent.equals(ExpVarietySBOurCofVHPContent))
		{
			System.out.println("OUR COFFEES Coffeehouse Varieties Content in Variety Side Banner in Home Page is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("OUR COFFEES Coffeehouse Varieties Content in Variety Side Banner in Home Page is not in match with creative - Fail");
			bw.write("OUR COFFEES Coffeehouse Varieties Content in Variety Side Banner in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		} 
		
		if(HexVarietySBOurCofVHPColor.equals(ExpVarietySBOurCofVHPColor) && VarietySBOurCofVHPFontSize.equals(ExpVarietySBOurCofVHPFontSize) && VarietySBOurCofVHPFontFamily.equals(ExpVarietySBOurCofVHPFontFamily) && VarietySBOurCofVHPContent.equals(ExpVarietySBOurCofVHPContent))
		{
			Label VarietySBOurCofVHPCSSP = new Label(5,54,"PASS"); 
			wsheet1.addCell(VarietySBOurCofVHPCSSP); 
		}
		else
		{
			Label VarietySBOurCofVHPCSSF = new Label(5,54,"FAIL"); 
			wsheet1.addCell(VarietySBOurCofVHPCSSF); 
		}
		
		//Variety - OUR COFFEES Section - Coffeehouse - Desc
		
		WebElement VarietySBOurCofdHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[6]/div/div[2]/div[1]/div[2]/div[5]/div[2]/div[3]")); 
		String VarietySBOurCofdHPColor = VarietySBOurCofdHP.getCssValue("color");
		System.out.println("Actual Color of OUR COFFEES Coffeehouse Desc in Variety Side Banner in Home Page is : "	+ VarietySBOurCofdHPColor );
		// Convert RGB to Hex Value
		Color HVarietySBOurCofdHPColor = Color.fromString(VarietySBOurCofdHPColor);
		String HexVarietySBOurCofdHPColor = HVarietySBOurCofdHPColor.asHex();
		System.out.println("Actual Hex Color of OUR COFFEES Coffeehouse Desc in Variety Side Banner in Home Page is : "	+ HexVarietySBOurCofdHPColor );
		
		String VarietySBOurCofdHPFontSize = VarietySBOurCofdHP.getCssValue("font-size");
		System.out.println("Actual Font Size of OUR COFFEES Coffeehouse Desc in Variety Side Banner in Home Page is : "	+ VarietySBOurCofdHPFontSize);
		String VarietySBOurCofdHPFontFamily = VarietySBOurCofdHP.getCssValue("font-family");
		System.out.println("Actual Font Family of OUR COFFEES Coffeehouse Desc in Variety Side Banner in Home Page is : "	+ VarietySBOurCofdHPFontFamily);
		String VarietySBOurCofdHPContent = VarietySBOurCofdHP.getText();
		System.out.println("Actual Content of OUR COFFEES Coffeehouse Desc in Variety Side Banner in Home Page is : "	+ VarietySBOurCofdHPContent);
						
		String ExpVarietySBOurCofdHPColor = sheet1.getCell(1,55).getContents(); //column,row
		System.out.println("Expected Color of OUR COFFEES Coffeehouse Desc in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurCofdHPColor );
		String ExpVarietySBOurCofdHPFontSize = sheet1.getCell(2,55).getContents();
		System.out.println("Expected Font Size of OUR COFFEES Coffeehouse Desc in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurCofdHPFontSize);
		String ExpVarietySBOurCofdHPFontFamily = sheet1.getCell(3,55).getContents();
		System.out.println("Expected Font Family of OUR COFFEES Coffeehouse Desc in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurCofdHPFontFamily);
		String ExpVarietySBOurCofdHPContent = sheet1.getCell(4,55).getContents();
		System.out.println("Expected Content of OUR COFFEES Coffeehouse Desc in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurCofdHPContent);
						
		if(HexVarietySBOurCofdHPColor.equals(ExpVarietySBOurCofdHPColor))
		{
			System.out.println("OUR COFFEES Coffeehouse Desc in Variety Side Banner in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Coffeehouse Desc in Variety Side Banner in Home Page Color is not in match with creative - Fail");
			bw.write("OUR COFFEES Coffeehouse Desc in Variety Side Banner in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
					
		if(VarietySBOurCofdHPFontSize.equals(ExpVarietySBOurCofdHPFontSize))
		{
			System.out.println("OUR COFFEES Coffeehouse Desc in Variety Side Banner in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Coffeehouse Desc in Variety Side Banner in Home Page Font Size is not in match with creative - Fail");
			bw.write("OUR COFFEES Coffeehouse Desc in Variety Side Banner in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
						
		if(VarietySBOurCofdHPFontFamily.equals(ExpVarietySBOurCofdHPFontFamily))
		{
			System.out.println("OUR COFFEES Coffeehouse Desc in Variety Side Banner in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Coffeehouse Desc in Variety Side Banner in Home Page Font Family is not in match with creative - Fail");
			bw.write("OUR COFFEES Coffeehouse Desc in Variety Side Banner in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				

		if(VarietySBOurCofdHPContent.equals(ExpVarietySBOurCofdHPContent))
		{
			System.out.println("OUR COFFEES Coffeehouse Desc Content in Variety Side Banner in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Coffeehouse Desc Content in Variety Side Banner in Home Page is not in match with creative - Fail");
			bw.write("OUR COFFEES Coffeehouse Desc Content in Variety Side Banner in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		} 
		
		if(HexVarietySBOurCofdHPColor.equals(ExpVarietySBOurCofdHPColor) && VarietySBOurCofdHPFontSize.equals(ExpVarietySBOurCofdHPFontSize) && VarietySBOurCofdHPFontFamily.equals(ExpVarietySBOurCofdHPFontFamily) && VarietySBOurCofdHPContent.equals(ExpVarietySBOurCofdHPContent))
		{
			Label VarietySBOurCofdHPCSSP = new Label(5,55,"PASS"); 
			wsheet1.addCell(VarietySBOurCofdHPCSSP); 
		}
		else
		{
			Label VarietySBOurCofdHPCSSF = new Label(5,55,"FAIL"); 
			wsheet1.addCell(VarietySBOurCofdHPCSSF); 
		}
		
		//Variety - OUR COFFEES Section - Organic
		//click action on Variety Organic
		
		Thread.sleep(3000);
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[6]/div/div[2]/div[2]/div[1]/div/div[6]/img")).click();
		
		WebElement VarietySBOurOrgHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[6]/div/div[2]/div[1]/div[2]/div[6]/div[2]/div[1]/span")); 
		String VarietySBOurOrgHPColor = VarietySBOurOrgHP.getCssValue("color");
		System.out.println("Actual Color of OUR COFFEES Organic in Variety Side Banner in Home Page is : "	+ VarietySBOurOrgHPColor );
		// Convert RGB to Hex Value
		Color HVarietySBOurOrgHPColor = Color.fromString(VarietySBOurOrgHPColor);
		String HexVarietySBOurOrgHPColor = HVarietySBOurOrgHPColor.asHex();
		System.out.println("Actual Hex Color of OUR COFFEES Organic in Variety Side Banner in Home Page is : "	+ HexVarietySBOurOrgHPColor );
		
		String VarietySBOurOrgHPFontSize = VarietySBOurOrgHP.getCssValue("font-size");
		System.out.println("Actual Font Size of OUR COFFEES Organic in Variety Side Banner in Home Page is : "	+ VarietySBOurOrgHPFontSize);
		String VarietySBOurOrgHPFontFamily = VarietySBOurOrgHP.getCssValue("font-family");
		System.out.println("Actual Font Family of OUR COFFEES Organic in Variety Side Banner in Home Page is : "	+ VarietySBOurOrgHPFontFamily);
		String VarietySBOurOrgHPContent = VarietySBOurOrgHP.getText();
		System.out.println("Actual Content of OUR COFFEES Organic in Variety Side Banner in Home Page is : "	+ VarietySBOurOrgHPContent);
						
		String ExpVarietySBOurOrgHPColor = sheet1.getCell(1,56).getContents(); //column,row
		System.out.println("Expected Color of OUR COFFEES Organic in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurOrgHPColor );
		String ExpVarietySBOurOrgHPFontSize = sheet1.getCell(2,56).getContents();
		System.out.println("Expected Font Size of OUR COFFEES Organic in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurOrgHPFontSize);
		String ExpVarietySBOurOrgHPFontFamily = sheet1.getCell(3,56).getContents();
		System.out.println("Expected Font Family of OUR COFFEES Organic in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurOrgHPFontFamily);
		String ExpVarietySBOurOrgHPContent = sheet1.getCell(4,56).getContents();
		System.out.println("Expected Content of OUR COFFEES Organic in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurOrgHPContent);
						
		if(HexVarietySBOurOrgHPColor.equals(ExpVarietySBOurOrgHPColor))
		{
			System.out.println("OUR COFFEES Organic in Variety Side Banner in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Organic in Variety Side Banner in Home Page Color is not in match with creative - Fail");
			bw.write("OUR COFFEES Organic in Variety Side Banner in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
					
		if(VarietySBOurOrgHPFontSize.equals(ExpVarietySBOurOrgHPFontSize))
		{
			System.out.println("OUR COFFEES Organic in Variety Side Banner in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Organic in Variety Side Banner in Home Page Font Size is not in match with creative - Fail");
			bw.write("OUR COFFEES Organic in Variety Side Banner in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
						
		if(VarietySBOurOrgHPFontFamily.equals(ExpVarietySBOurOrgHPFontFamily))
		{
			System.out.println("OUR COFFEES Organic in Variety Side Banner in Home Page Font Family is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("OUR COFFEES Organic in Variety Side Banner in Home Page Font Family is not in match with creative - Fail");
			bw.write("OUR COFFEES Organic in Variety Side Banner in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				

		if(VarietySBOurOrgHPContent.equals(ExpVarietySBOurOrgHPContent))
		{
			System.out.println("OUR COFFEES Organic Content in Variety Side Banner in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Organic Content in Variety Side Banner in Home Page is not in match with creative - Fail");
			bw.write("OUR COFFEES Organic Content in Variety Side Banner in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		} 
		
		if(HexVarietySBOurOrgHPColor.equals(ExpVarietySBOurOrgHPColor) && VarietySBOurOrgHPFontSize.equals(ExpVarietySBOurOrgHPFontSize) && VarietySBOurOrgHPFontFamily.equals(ExpVarietySBOurOrgHPFontFamily) && VarietySBOurOrgHPContent.equals(ExpVarietySBOurOrgHPContent))
		{
			Label VarietySBOurOrgHPCSSP = new Label(5,56,"PASS"); 
			wsheet1.addCell(VarietySBOurOrgHPCSSP); 
		}
		else
		{
			Label VarietySBOurOrgHPCSSF = new Label(5,56,"FAIL"); 
			wsheet1.addCell(VarietySBOurOrgHPCSSF); 
		}
		
		//Variety - OUR COFFEES Section - Organic - Varieties: 7
		
		WebElement VarietySBOurOrgVHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[6]/div/div[2]/div[1]/div[2]/div[6]/div[2]/div[2]")); 
		String VarietySBOurOrgVHPColor = VarietySBOurOrgVHP.getCssValue("color");
		System.out.println("Actual Color of OUR COFFEES Organic Varieties in Variety Side Banner in Home Page is : "	+ VarietySBOurOrgVHPColor );
		// Convert RGB to Hex Value
		Color HVarietySBOurOrgVHPColor = Color.fromString(VarietySBOurOrgVHPColor);
		String HexVarietySBOurOrgVHPColor = HVarietySBOurOrgVHPColor.asHex();
		System.out.println("Actual Hex Color of OUR COFFEES Organic Varieties in Variety Side Banner in Home Page is : "	+ HexVarietySBOurOrgVHPColor );
		
		String VarietySBOurOrgVHPFontSize = VarietySBOurOrgVHP.getCssValue("font-size");
		System.out.println("Actual Font Size of OUR COFFEES Organic Varieties in Variety Side Banner in Home Page is : "	+ VarietySBOurOrgVHPFontSize);
		String VarietySBOurOrgVHPFontFamily = VarietySBOurOrgVHP.getCssValue("font-family");
		System.out.println("Actual Font Family of OUR COFFEES Organic Varieties in Variety Side Banner in Home Page is : "	+ VarietySBOurOrgVHPFontFamily);
		String VarietySBOurOrgVHPContent = VarietySBOurOrgVHP.getText();
		System.out.println("Actual Content of OUR COFFEES Organic Varieties in Variety Side Banner in Home Page is : "	+ VarietySBOurOrgVHPContent);
						
		String ExpVarietySBOurOrgVHPColor = sheet1.getCell(1,57).getContents(); //column,row
		System.out.println("Expected Color of OUR COFFEES Organic Varieties in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurOrgVHPColor );
		String ExpVarietySBOurOrgVHPFontSize = sheet1.getCell(2,57).getContents();
		System.out.println("Expected Font Size of OUR COFFEES Organic Varieties in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurOrgVHPFontSize);
		String ExpVarietySBOurOrgVHPFontFamily = sheet1.getCell(3,57).getContents();
		System.out.println("Expected Font Family of OUR COFFEES Organic Varieties in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurOrgVHPFontFamily);
		String ExpVarietySBOurOrgVHPContent = sheet1.getCell(4,57).getContents();
		System.out.println("Expected Content of OUR COFFEES Organic Varieties in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurOrgVHPContent);
						
		if(HexVarietySBOurOrgVHPColor.equals(ExpVarietySBOurOrgVHPColor))
		{
			System.out.println("OUR COFFEES Organic Varieties in Variety Side Banner in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Organic Varieties in Variety Side Banner in Home Page Color is not in match with creative - Fail");
			bw.write("OUR COFFEES Organic Varieties in Variety Side Banner in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
					
		if(VarietySBOurOrgVHPFontSize.equals(ExpVarietySBOurOrgVHPFontSize))
		{
			System.out.println("OUR COFFEES Organic Varieties in Variety Side Banner in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Organic Varieties in Variety Side Banner in Home Page Font Size is not in match with creative - Fail");
			bw.write("OUR COFFEES Organic Varieties in Variety Side Banner in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
						
		if(VarietySBOurOrgVHPFontFamily.equals(ExpVarietySBOurOrgVHPFontFamily))
		{
			System.out.println("OUR COFFEES Organic Varieties in Variety Side Banner in Home Page Font Family is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("OUR COFFEES Organic Varieties in Variety Side Banner in Home Page Font Family is not in match with creative - Fail");
			bw.write("OUR COFFEES Organic Varieties in Variety Side Banner in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				

		if(VarietySBOurOrgVHPContent.equals(ExpVarietySBOurOrgVHPContent))
		{
			System.out.println("OUR COFFEES Organic Varieties Content in Variety Side Banner in Home Page is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("OUR COFFEES Organic Varieties Content in Variety Side Banner in Home Page is not in match with creative - Fail");
			bw.write("OUR COFFEES Organic Varieties Content in Variety Side Banner in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		} 
		
		if(HexVarietySBOurOrgVHPColor.equals(ExpVarietySBOurOrgVHPColor) && VarietySBOurOrgVHPFontSize.equals(ExpVarietySBOurOrgVHPFontSize) && VarietySBOurOrgVHPFontFamily.equals(ExpVarietySBOurOrgVHPFontFamily) && VarietySBOurOrgVHPContent.equals(ExpVarietySBOurOrgVHPContent))
		{
			Label VarietySBOurOrgVHPCSSP = new Label(5,57,"PASS"); 
			wsheet1.addCell(VarietySBOurOrgVHPCSSP); 
		}
		else
		{
			Label VarietySBOurOrgVHPCSSF = new Label(5,57,"FAIL"); 
			wsheet1.addCell(VarietySBOurOrgVHPCSSF); 
		}
		
		//Variety - OUR COFFEES Section - Organic - Desc
		
		WebElement VarietySBOurOrgdHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[6]/div/div[2]/div[1]/div[2]/div[6]/div[2]/div[3]")); 
		String VarietySBOurOrgdHPColor = VarietySBOurOrgdHP.getCssValue("color");
		System.out.println("Actual Color of OUR COFFEES Organic Desc in Variety Side Banner in Home Page is : "	+ VarietySBOurOrgdHPColor );
		// Convert RGB to Hex Value
		Color HVarietySBOurOrgdHPColor = Color.fromString(VarietySBOurOrgdHPColor);
		String HexVarietySBOurOrgdHPColor = HVarietySBOurOrgdHPColor.asHex();
		System.out.println("Actual Hex Color of OUR COFFEES Organic Desc in Variety Side Banner in Home Page is : "	+ HexVarietySBOurOrgdHPColor );
		
		String VarietySBOurOrgdHPFontSize = VarietySBOurOrgdHP.getCssValue("font-size");
		System.out.println("Actual Font Size of OUR COFFEES Organic Desc in Variety Side Banner in Home Page is : "	+ VarietySBOurOrgdHPFontSize);
		String VarietySBOurOrgdHPFontFamily = VarietySBOurOrgdHP.getCssValue("font-family");
		System.out.println("Actual Font Family of OUR COFFEES Organic Desc in Variety Side Banner in Home Page is : "	+ VarietySBOurOrgdHPFontFamily);
		String VarietySBOurOrgdHPContent = VarietySBOurOrgdHP.getText();
		System.out.println("Actual Content of OUR COFFEES Organic Desc in Variety Side Banner in Home Page is : "	+ VarietySBOurOrgdHPContent);
						
		String ExpVarietySBOurOrgdHPColor = sheet1.getCell(1,58).getContents(); //column,row
		System.out.println("Expected Color of OUR COFFEES Organic Desc in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurOrgdHPColor );
		String ExpVarietySBOurOrgdHPFontSize = sheet1.getCell(2,58).getContents();
		System.out.println("Expected Font Size of OUR COFFEES Organic Desc in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurOrgdHPFontSize);
		String ExpVarietySBOurOrgdHPFontFamily = sheet1.getCell(3,58).getContents();
		System.out.println("Expected Font Family of OUR COFFEES Organic Desc in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurOrgdHPFontFamily);
		String ExpVarietySBOurOrgdHPContent = sheet1.getCell(4,58).getContents();
		System.out.println("Expected Content of OUR COFFEES Organic Desc in Variety Side Banner in Home Page is : "	+ ExpVarietySBOurOrgdHPContent);
						
		if(HexVarietySBOurOrgdHPColor.equals(ExpVarietySBOurOrgdHPColor))
		{
			System.out.println("OUR COFFEES Organic Desc in Variety Side Banner in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Organic Desc in Variety Side Banner in Home Page Color is not in match with creative - Fail");
			bw.write("OUR COFFEES Organic Desc in Variety Side Banner in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
					
		if(VarietySBOurOrgdHPFontSize.equals(ExpVarietySBOurOrgdHPFontSize))
		{
			System.out.println("OUR COFFEES Organic Desc in Variety Side Banner in Home Page Font Size is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("OUR COFFEES Organic Desc in Variety Side Banner in Home Page Font Size is not in match with creative - Fail");
			bw.write("OUR COFFEES Organic Desc in Variety Side Banner in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
						
		if(VarietySBOurOrgdHPFontFamily.equals(ExpVarietySBOurOrgdHPFontFamily))
		{
			System.out.println("OUR COFFEES Organic Desc in Variety Side Banner in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Organic Desc in Variety Side Banner in Home Page Font Family is not in match with creative - Fail");
			bw.write("OUR COFFEES Organic Desc in Variety Side Banner in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				

		if(VarietySBOurOrgdHPContent.equals(ExpVarietySBOurOrgdHPContent))
		{
			System.out.println("OUR COFFEES Organic Desc Content in Variety Side Banner in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("OUR COFFEES Organic Desc Content in Variety Side Banner in Home Page is not in match with creative - Fail");
			bw.write("OUR COFFEES Organic Desc Content in Variety Side Banner in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		} 
		
		if(HexVarietySBOurOrgdHPColor.equals(ExpVarietySBOurOrgdHPColor) && VarietySBOurOrgdHPFontSize.equals(ExpVarietySBOurOrgdHPFontSize) && VarietySBOurOrgdHPFontFamily.equals(ExpVarietySBOurOrgdHPFontFamily) && VarietySBOurOrgdHPContent.equals(ExpVarietySBOurOrgdHPContent))
		{
			Label VarietySBOurOrgdHPCSSP = new Label(5,58,"PASS"); 
			wsheet1.addCell(VarietySBOurOrgdHPCSSP); 
		}
		else
		{
			Label VarietySBOurOrgdHPCSSF = new Label(5,58,"FAIL"); 
			wsheet1.addCell(VarietySBOurOrgdHPCSSF); 
		}
		
		
		// Sustainability Title
		
		Thread.sleep(3000);
		JavascriptExecutor jsest = (JavascriptExecutor)driver;
		jsest.executeScript("window.scrollBy(0,1000)", "");
		Thread.sleep(3000);
		
		WebElement SustainabilityTitleHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[7]/div/div/div/div[1]/div[1]"));
		String SustainabilityTitleHPFontSize = SustainabilityTitleHP.getCssValue("font-size");
		System.out.println("Actual Font Size of Sustainability Title Text in Home Page is : "	+ SustainabilityTitleHPFontSize);
		String SustainabilityTitleHPFontFamily = SustainabilityTitleHP.getCssValue("font-family");
		System.out.println("Actual Font Family of Sustainability Title Text in Home Page is : "	+ SustainabilityTitleHPFontFamily);
		String SustainabilityTitleHPContent = SustainabilityTitleHP.getText();
		System.out.println("Actual Sustainability Title Content in Home Page is : "	+ SustainabilityTitleHPContent);
				 
		String ExpSustainabilityTitleHPFontSize = sheet1.getCell(2,25).getContents();
		System.out.println("Expected Font Size of Sustainability Title Text in Home Page is : "	+ ExpSustainabilityTitleHPFontSize);
		String ExpSustainabilityTitleHPFontFamily = sheet1.getCell(3,25).getContents();
		System.out.println("Expected Font Family of Sustainability Title Text in Home Page is : "	+ ExpSustainabilityTitleHPFontFamily);
		String ExpSustainabilityTitleHPContent = sheet1.getCell(4,25).getContents();
		System.out.println("Expected Sustainability Title Content in Home Page is : "	+ ExpSustainabilityTitleHPContent);
			
		if(SustainabilityTitleHPFontSize.equals(ExpSustainabilityTitleHPFontSize))
		{
			System.out.println("Sustainability Title Text in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Sustainability Title Text in Home Page Font Size is not in match with creative - Fail");
			bw.write("Sustainability Title Text in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
			
		if(SustainabilityTitleHPFontFamily.equals(ExpSustainabilityTitleHPFontFamily))
		{
			System.out.println("Sustainability Title Text in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Sustainability Title Text in Home Page Font Family is not in match with creative - Fail");
			bw.write("Sustainability Title Text in Home Page Color is Font Family in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(SustainabilityTitleHPContent.equals(ExpSustainabilityTitleHPContent))
		{
			System.out.println("Sustainability Title Content in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Sustainability Title Content in Home Page is not in match with creative - Fail");
			bw.write("Sustainability Title Content in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(SustainabilityTitleHPFontSize.equals(ExpSustainabilityTitleHPFontSize) && SustainabilityTitleHPFontFamily.equals(ExpSustainabilityTitleHPFontFamily) && SustainabilityTitleHPContent.equals(ExpSustainabilityTitleHPContent))
		{
			Label SustainabilityTitleHPCSSP = new Label(5,25,"PASS"); 
			wsheet1.addCell(SustainabilityTitleHPCSSP); 
		}
		else
		{
			Label SustainabilityTitleHPCSSF = new Label(5,25,"FAIL"); 
			wsheet1.addCell(SustainabilityTitleHPCSSF); 
		}
				
		// Sustainability Description
				
		WebElement SustainabilityDescHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[7]/div/div/div/div[1]/div[3]"));
		String SustainabilityDescHPFontSize = SustainabilityDescHP.getCssValue("font-size");
		System.out.println("Actual Font Size of Sustainability Desc Text in Home Page is : "	+ SustainabilityDescHPFontSize);
		String SustainabilityDescHPFontFamily = SustainabilityDescHP.getCssValue("font-family");
		System.out.println("Actual Font Family of Sustainability Desc Text in Home Page is : "	+ SustainabilityDescHPFontFamily);
		String SustainabilityDescHPContent = SustainabilityDescHP.getText();
		System.out.println("Actual Sustainability Desc Content in Home Page is : "	+ SustainabilityDescHPContent);
				 
		String ExpSustainabilityDescHPFontSize = sheet1.getCell(2,26).getContents();
		System.out.println("Expected Font Size of Sustainability Desc Text in Home Page is : "	+ ExpSustainabilityDescHPFontSize);
		String ExpSustainabilityDescHPFontFamily = sheet1.getCell(3,26).getContents();
		System.out.println("Expected Font Family of Sustainability Desc Text in Home Page is : "	+ ExpSustainabilityDescHPFontFamily);
		String ExpSustainabilityDescHPContent = sheet1.getCell(4,26).getContents();
		System.out.println("Expected Sustainability Desc Content in Home Page is : "	+ ExpSustainabilityDescHPContent);
				
		if(SustainabilityDescHPFontSize.equals(ExpSustainabilityDescHPFontSize))
		{
			System.out.println("Sustainability Desc Text in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Sustainability Desc Text in Home Page Font Size is not in match with creative - Fail");
			bw.write("Sustainability Desc Text in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
			
		if(SustainabilityDescHPFontFamily.equals(ExpSustainabilityDescHPFontFamily))
		{
			System.out.println("Sustainability Desc Text in Home Page Font Family is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("Sustainability Desc Text in Home Page Font Family is not in match with creative - Fail");
			bw.write("Sustainability Desc Text in Home Page Color is Font Family in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(SustainabilityDescHPContent.equals(ExpSustainabilityDescHPContent))
		{
			System.out.println("Sustainability Desc Content in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Sustainability Desc Content in Home Page is not in match with creative - Fail");
			bw.write("Sustainability Desc Content in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(SustainabilityDescHPFontSize.equals(ExpSustainabilityDescHPFontSize) && SustainabilityDescHPFontFamily.equals(ExpSustainabilityDescHPFontFamily) && SustainabilityDescHPContent.equals(ExpSustainabilityDescHPContent))
		{
			Label SustainabilityDescHPCSSP = new Label(5,26,"PASS"); 
			wsheet1.addCell(SustainabilityDescHPCSSP); 
		}
		else
		{
			Label SustainabilityDescHPCSSF = new Label(5,26,"FAIL"); 
			wsheet1.addCell(SustainabilityDescHPCSSF); 
		}
		
		// Sustainability Text in Side Banner
		
		Thread.sleep(3000);
		JavascriptExecutor jsessbt = (JavascriptExecutor)driver;
		jsessbt.executeScript("window.scrollBy(0,1000)", "");
		Thread.sleep(3000);
		
		WebElement SustainabilitySBHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[8]/div/div[1]/div/div[1]")); 
		String SustainabilitySBHPColor = SustainabilitySBHP.getCssValue("color");
		System.out.println("Actual Color of Sustainability Text in Side Banner in Home Page is : "	+ SustainabilitySBHPColor );
		// Convert RGB to Hex Value
		Color HSustainabilitySBHPColor = Color.fromString(SustainabilitySBHPColor);
		String HexSustainabilitySBHPColor = HSustainabilitySBHPColor.asHex();
		System.out.println("Actual Hex Color of Sustainability Text in Side Banner in Home Page is : "	+ HexSustainabilitySBHPColor );
		
		String SustainabilitySBHPFontSize = SustainabilitySBHP.getCssValue("font-size");
		System.out.println("Actual Font Size of Sustainability Text in Side Banner in Home Page is : "	+ SustainabilitySBHPFontSize);
		String SustainabilitySBHPFontFamily = SustainabilitySBHP.getCssValue("font-family");
		System.out.println("Actual Font Family of Sustainability Text in Side Banner in Home Page is : "	+ SustainabilitySBHPFontFamily);
						
		String ExpSustainabilitySBHPColor = sheet1.getCell(1,27).getContents(); //column,row
		System.out.println("Expected Color of Sustainability Text in Side Banner in Home Page is : "	+ ExpSustainabilitySBHPColor );
		String ExpSustainabilitySBHPFontSize = sheet1.getCell(2,27).getContents();
		System.out.println("Expected Font Size of Sustainability Text in Side Banner in Home Page is : "	+ ExpSustainabilitySBHPFontSize);
		String ExpSustainabilitySBHPFontFamily = sheet1.getCell(3,27).getContents();
		System.out.println("Expected Font Family of Sustainability Text in Side Banner in Home Page is : "	+ ExpSustainabilitySBHPFontFamily);
						
		if(HexSustainabilitySBHPColor.equals(ExpSustainabilitySBHPColor))
		{
			System.out.println("Sustainability Text in Side Banner in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Sustainability Text in Side Banner in Home Page Color is not in match with creative - Fail");
			bw.write("Sustainability Text in Side Banner in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
						
		if(SustainabilitySBHPFontSize.equals(ExpSustainabilitySBHPFontSize))
		{
			System.out.println("Sustainability Text in Side Banner in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Sustainability Text in Side Banner in Home Page Font Size is not in match with creative - Fail");
			bw.write("Sustainability Text in Side Banner in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
						
		if(SustainabilitySBHPFontFamily.equals(ExpSustainabilitySBHPFontFamily))
		{
			System.out.println("Sustainability Text in Side Banner in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Sustainability Text in Side Banner in Home Page Font Family is not in match with creative - Fail");
			bw.write("Sustainability Text in Side Banner in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexSustainabilitySBHPColor.equals(ExpSustainabilitySBHPColor) && SustainabilitySBHPFontSize.equals(ExpSustainabilitySBHPFontSize) && SustainabilitySBHPFontFamily.equals(ExpSustainabilitySBHPFontFamily))
		{
			Label SustainabilitySBHPCSSP = new Label(5,27,"PASS"); 
			wsheet1.addCell(SustainabilitySBHPCSSP); 
		}
		else
		{
			Label SustainabilitySBHPCSSF = new Label(5,27,"FAIL"); 
			wsheet1.addCell(SustainabilitySBHPCSSF); 
		}
						
		// Sustainability Side Banner - THE POWER OF COFFEE 
				
		WebElement SustainabilitySBPowerHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[8]/div/div[1]/div/div[2]")); 
		String SustainabilitySBPowerHPColor = SustainabilitySBPowerHP.getCssValue("color");
		System.out.println("Actual Color of THE POWER OF COFFEE Text in Sustainability Side Banner in Home Page is : "	+ SustainabilitySBPowerHPColor );
		// Convert RGB to Hex Value
		Color HSustainabilitySBPowerHPColor = Color.fromString(SustainabilitySBPowerHPColor);
		String HexSustainabilitySBPowerHPColor = HSustainabilitySBPowerHPColor.asHex();
		System.out.println("Actual Hex Color of THE POWER OF COFFEE Text in Sustainability Side Banner in Home Page is : "	+ HexSustainabilitySBPowerHPColor );
		
		String SustainabilitySBPowerHPFontSize = SustainabilitySBPowerHP.getCssValue("font-size");
		System.out.println("Actual Font Size of THE POWER OF COFFEE Text in Sustainability Side Banner in Home Page is : "	+ SustainabilitySBPowerHPFontSize);
		String SustainabilitySBPowerHPFontFamily = SustainabilitySBPowerHP.getCssValue("font-family");
		System.out.println("Actual Font Family of THE POWER OF COFFEE Text in Sustainability Side Banner in Home Page is : "	+ SustainabilitySBPowerHPFontFamily);
		String SustainabilitySBPowerHPContent = SustainabilitySBPowerHP.getText();
		System.out.println("Actual THE POWER OF COFFEE Content in Sustainability Side Banner in Home Page is : "	+ SustainabilitySBPowerHPContent);
						
		String ExpSustainabilitySBPowerHPColor = sheet1.getCell(1,28).getContents(); //column,row
		System.out.println("Expected Color of THE POWER OF COFFEE Text in Sustainability Side Banner in Home Page is : "	+ ExpSustainabilitySBPowerHPColor );
		String ExpSustainabilitySBPowerHPFontSize = sheet1.getCell(2,28).getContents();
		System.out.println("Expected Font Size of THE POWER OF COFFEE Text in Sustainability Side Banner in Home Page is : "	+ ExpSustainabilitySBPowerHPFontSize);
		String ExpSustainabilitySBPowerHPFontFamily = sheet1.getCell(3,28).getContents();
		System.out.println("Expected Font Family of THE POWER OF COFFEE Text in Sustainability Side Banner in Home Page is : "	+ ExpSustainabilitySBPowerHPFontFamily);
		String ExpSustainabilitySBPowerHPContent = sheet1.getCell(4,28).getContents();
		System.out.println("Expected THE POWER OF COFFEE Content in Sustainability Side Banner in Home Page is : "	+ ExpSustainabilitySBPowerHPContent);
						
		if(HexSustainabilitySBPowerHPColor.equals(ExpSustainabilitySBPowerHPColor))
		{
			System.out.println("THE POWER OF COFFEE Text in Sustainability Side Banner in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("THE POWER OF COFFEE Text in Sustainability Side Banner in Home Page Color is not in match with creative - Fail");
			bw.write("THE POWER OF COFFEE Text in Sustainability Side Banner in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
						
		if(SustainabilitySBPowerHPFontSize.equals(ExpSustainabilitySBPowerHPFontSize))
		{
			System.out.println("THE POWER OF COFFEE Text in Sustainability Side Banner in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("THE POWER OF COFFEE Text in Sustainability Side Banner in Home Page Font Size is not in match with creative - Fail");
			bw.write("THE POWER OF COFFEE Text in Sustainability Side Banner in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
						
		if(SustainabilitySBPowerHPFontFamily.equals(ExpSustainabilitySBPowerHPFontFamily))
		{
			System.out.println("THE POWER OF COFFEE Text in Sustainability Side Banner in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("THE POWER OF COFFEE Text in Sustainability Side Banner in Home Page Font Family is not in match with creative - Fail");
			bw.write("THE POWER OF COFFEE Text in Sustainability Side Banner in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				

		if(SustainabilitySBPowerHPContent.equals(ExpSustainabilitySBPowerHPContent))
		{
			System.out.println("THE POWER OF COFFEE Content in Sustainability side Banner in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("THE POWER OF COFFEE Content in Sustainability side Banner in Home Page is not in match with creative - Fail");
			bw.write("THE POWER OF COFFEE Content in Sustainability side Banner in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexSustainabilitySBPowerHPColor.equals(ExpSustainabilitySBPowerHPColor) && SustainabilitySBPowerHPFontSize.equals(ExpSustainabilitySBPowerHPFontSize) && SustainabilitySBPowerHPFontFamily.equals(ExpSustainabilitySBPowerHPFontFamily) && SustainabilitySBPowerHPContent.equals(ExpSustainabilitySBPowerHPContent))
		{
			Label SustainabilitySBPowerHPCSSP = new Label(5,28,"PASS"); 
			wsheet1.addCell(SustainabilitySBPowerHPCSSP); 
		}
		else
		{
			Label SustainabilitySBPowerHPCSSF = new Label(5,28,"FAIL"); 
			wsheet1.addCell(SustainabilitySBPowerHPCSSF); 
		}
				
		// Sustainability Side Banner Contents
				
		WebElement SustainabilitySBContentsHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[8]/div/div[1]/div/div[4]")); 
		String SustainabilitySBContentsHPColor = SustainabilitySBContentsHP.getCssValue("color");
		System.out.println("Actual Color of Sustainability Side Banner Contents Text in Side Banner in Home Page is : "	+ SustainabilitySBContentsHPColor );
		// Convert RGB to Hex Value
		Color HSustainabilitySBContentsHPColor = Color.fromString(SustainabilitySBContentsHPColor);
		String HexSustainabilitySBContentsHPColor = HSustainabilitySBContentsHPColor.asHex();
		System.out.println("Actual Hex Color of Sustainability Side Banner Contents Text in Side Banner in Home Page is : "	+ HexSustainabilitySBContentsHPColor );
		
		String SustainabilitySBContentsHPFontSize = SustainabilitySBContentsHP.getCssValue("font-size");
		System.out.println("Actual Font Size of Sustainability Side Banner Contents Text in Side Banner in Home Page is : "	+ SustainabilitySBContentsHPFontSize);
		String SustainabilitySBContentsHPFontFamily = SustainabilitySBContentsHP.getCssValue("font-family");
		System.out.println("Actual Font Family of Sustainability Side Banner Contents Text in Side Banner in Home Page is : "	+ SustainabilitySBContentsHPFontFamily);
		String SustainabilitySBContentsHPContent = SustainabilitySBContentsHP.getText();
		System.out.println("Actual Sustainability Side Banner Contents in Home Page is : "	+ SustainabilitySBContentsHPContent);
						
		String ExpSustainabilitySBContentsHPColor = sheet1.getCell(1,29).getContents(); //column,row
		System.out.println("Expected Color of Sustainability Side Banner Contents Text in Side Banner in Home Page is : "	+ ExpSustainabilitySBContentsHPColor );
		String ExpSustainabilitySBContentsHPFontSize = sheet1.getCell(2,29).getContents();
		System.out.println("Expected Font Size of Sustainability Side Banner Contents Text in Side Banner in Home Page is : "	+ ExpSustainabilitySBContentsHPFontSize);
		String ExpSustainabilitySBContentsHPFontFamily = sheet1.getCell(3,29).getContents();
		System.out.println("Expected Font Family of Sustainability Side Banner Contents Text in Side Banner in Home Page is : "	+ ExpSustainabilitySBContentsHPFontFamily);
		String ExpSustainabilitySBContentsHPContent = sheet1.getCell(4,29).getContents();
		System.out.println("Expected Sustainability Side Banner Contents in Home Page is : "	+ ExpSustainabilitySBContentsHPContent);
						
		if(HexSustainabilitySBContentsHPColor.equals(ExpSustainabilitySBContentsHPColor))
		{
			System.out.println("Sustainability Side Banner Contents Text in Side Banner in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Sustainability Side Banner Contents Text in Side Banner in Home Page Color is not in match with creative - Fail");
			bw.write("Sustainability Side Banner Contents Text in Side Banner in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
						
		if(SustainabilitySBContentsHPFontSize.equals(ExpSustainabilitySBContentsHPFontSize))
		{
			System.out.println("Sustainability Side Banner Contents Text in Side Banner in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Sustainability Side Banner Contents Text in Side Banner in Home Page Font Size is not in match with creative - Fail");
			bw.write("Sustainability Side Banner Contents Text in Side Banner in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
						
		if(SustainabilitySBContentsHPFontFamily.equals(ExpSustainabilitySBContentsHPFontFamily))
		{
			System.out.println("Sustainability Side Banner Contents Text in Side Banner in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Sustainability Side Banner Contents Text in Side Banner in Home Page Font Family is not in match with creative - Fail");
			bw.write("Sustainability Side Banner Contents Text in Side Banner in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(SustainabilitySBContentsHPContent.equals(ExpSustainabilitySBContentsHPContent))
		{
			System.out.println("Sustainability Side Banner Contents in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Sustainability Side Banner Contents in Home Page is not in match with creative - Fail");
			bw.write("Sustainability Side Banner Contents in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexSustainabilitySBContentsHPColor.equals(ExpSustainabilitySBContentsHPColor) && SustainabilitySBContentsHPFontSize.equals(ExpSustainabilitySBContentsHPFontSize) && SustainabilitySBContentsHPFontFamily.equals(ExpSustainabilitySBContentsHPFontFamily) && SustainabilitySBContentsHPContent.equals(ExpSustainabilitySBContentsHPContent))
		{
			Label SustainabilitySBContentsHPCSSP = new Label(5,29,"PASS"); 
			wsheet1.addCell(SustainabilitySBContentsHPCSSP); 
		}
		else
		{
			Label SustainabilitySBContentsHPCSSF = new Label(5,29,"FAIL"); 
			wsheet1.addCell(SustainabilitySBContentsHPCSSF); 
		}
				
		// Sustainability Side Banner - More about Sustainability
				
		WebElement SustainabilitySBMoreHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[8]/div/div[1]/div/div[5]/div")); 
		String SustainabilitySBMoreHPColor = SustainabilitySBMoreHP.getCssValue("color");
		System.out.println("Actual Color of More about Sustainability Text in Side Banner in Home Page is : "	+ SustainabilitySBMoreHPColor );
		// Convert RGB to Hex Value
		Color HSustainabilitySBMoreHPColor = Color.fromString(SustainabilitySBMoreHPColor);
		String HexSustainabilitySBMoreHPColor = HSustainabilitySBMoreHPColor.asHex();
		System.out.println("Actual Hex Color of More about Sustainability Text in Side Banner in Home Page is : "	+ HexSustainabilitySBMoreHPColor );
		
		String SustainabilitySBMoreHPFontSize = SustainabilitySBMoreHP.getCssValue("font-size");
		System.out.println("Actual Font Size of More about Sustainability Text in Side Banner in Home Page is : "	+ SustainabilitySBMoreHPFontSize);
		String SustainabilitySBMoreHPFontFamily = SustainabilitySBMoreHP.getCssValue("font-family");
		System.out.println("Actual Font Family of More about Sustainability Text in Side Banner in Home Page is : "	+ SustainabilitySBMoreHPFontFamily);
		String SustainabilitySBMoreHPContent = SustainabilitySBMoreHP.getText();
		System.out.println("Actual More about Sustainability Content in Home Page is : "	+ SustainabilitySBMoreHPContent);
						
		String ExpSustainabilitySBMoreHPColor = sheet1.getCell(1,30).getContents(); //column,row
		System.out.println("Expected Color of More about Sustainability Text in Side Banner in Home Page is : "	+ ExpSustainabilitySBMoreHPColor );
		String ExpSustainabilitySBMoreHPFontSize = sheet1.getCell(2,30).getContents();
		System.out.println("Expected Font Size of More about Sustainability Text in Side Banner in Home Page is : "	+ ExpSustainabilitySBMoreHPFontSize);
		String ExpSustainabilitySBMoreHPFontFamily = sheet1.getCell(3,30).getContents();
		System.out.println("Expected Font Family of More about Sustainability Text in Side Banner in Home Page is : "	+ ExpSustainabilitySBMoreHPFontFamily);
		String ExpSustainabilitySBMoreHPContent = sheet1.getCell(4,30).getContents();
		System.out.println("Expected More about Sustainability Content in Home Page is : "	+ ExpSustainabilitySBMoreHPContent);
						
		if(HexSustainabilitySBMoreHPColor.equals(ExpSustainabilitySBMoreHPColor))
		{
			System.out.println("More about Sustainability Text in Side Banner in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("More about Sustainability Text in Side Banner in Home Page Color is not in match with creative - Fail");
			bw.write("More about Sustainability Text in Side Banner in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
					
		if(SustainabilitySBMoreHPFontSize.equals(ExpSustainabilitySBMoreHPFontSize))
		{
			System.out.println("More about Sustainability Text in Side Banner in Home Page Font Size is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("More about Sustainability Text in Side Banner in Home Page Font Size is not in match with creative - Fail");
			bw.write("More about Sustainability Text in Side Banner in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
						
		if(SustainabilitySBMoreHPFontFamily.equals(ExpSustainabilitySBMoreHPFontFamily))
		{
			System.out.println("More about Sustainability Text in Side Banner in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("More about Sustainability Text in Side Banner in Home Page Font Family is not in match with creative - Fail");
			bw.write("More about Sustainability Text in Side Banner in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				

		if(SustainabilitySBMoreHPContent.equals(ExpSustainabilitySBMoreHPContent))
		{
			System.out.println("More about Sustainability Content in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("More about Sustainability Content in Home Page is not in match with creative - Fail");
			bw.write("More about Sustainability Content in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexSustainabilitySBMoreHPColor.equals(ExpSustainabilitySBMoreHPColor) && SustainabilitySBMoreHPFontSize.equals(ExpSustainabilitySBMoreHPFontSize) && SustainabilitySBMoreHPFontFamily.equals(ExpSustainabilitySBMoreHPFontFamily) && SustainabilitySBMoreHPContent.equals(ExpSustainabilitySBMoreHPContent))
		{
			Label SustainabilitySBMoreHPCSSP = new Label(5,30,"PASS"); 
			wsheet1.addCell(SustainabilitySBMoreHPCSSP); 
		}
		else
		{
			Label SustainabilitySBMoreHPCSSF = new Label(5,30,"FAIL"); 
			wsheet1.addCell(SustainabilitySBMoreHPCSSF); 
		}
		
		
		// Sustainability Number tab 1 - Title - WHAT DOES BEING FAIR TRADE CERTIFIED� MEAN?

		
		WebElement SustainabilityN1TitleHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[8]/div/div[2]/div[1]/div[1]/div[1]")); 
		String SustainabilityN1TitleHPColor = SustainabilityN1TitleHP.getCssValue("color");
		System.out.println("Actual Color of Title Text in Sustainability Number tab 1 in Home Page is : "	+ SustainabilityN1TitleHPColor );
		// Convert RGB to Hex Value
		Color HSustainabilityN1TitleHPColor = Color.fromString(SustainabilityN1TitleHPColor);
		String HexSustainabilityN1TitleHPColor = HSustainabilityN1TitleHPColor.asHex();
		System.out.println("Actual Hex Color of Title Text in Sustainability Number tab 1 in Home Page is : "	+ HexSustainabilityN1TitleHPColor );
		
		String SustainabilityN1TitleHPFontSize = SustainabilityN1TitleHP.getCssValue("font-size");
		System.out.println("Actual Font Size of Title Text in Sustainability Number tab 1 in Home Page is : "	+ SustainabilityN1TitleHPFontSize);
		String SustainabilityN1TitleHPFontFamily = SustainabilityN1TitleHP.getCssValue("font-family");
		System.out.println("Actual Font Family of Title Text in Sustainability Number tab 1 in Home Page is : "	+ SustainabilityN1TitleHPFontFamily);
		String SustainabilityN1TitleHPContent = SustainabilityN1TitleHP.getText();
		System.out.println("Actual Title Text Content in Sustainability Number tab 1 in Home Page is : "	+ SustainabilityN1TitleHPContent);
				
		String ExpSustainabilityN1TitleHPColor = sheet1.getCell(1,31).getContents(); //column,row
		System.out.println("Expected Color of Title Text in Sustainability Number tab 1 in Home Page is : "	+ ExpSustainabilityN1TitleHPColor );
		String ExpSustainabilityN1TitleHPFontSize = sheet1.getCell(2,31).getContents();
		System.out.println("Expected Font Size of Title Text in Sustainability Number tab 1 in Home Page is : "	+ ExpSustainabilityN1TitleHPFontSize);
		String ExpSustainabilityN1TitleHPFontFamily = sheet1.getCell(3,31).getContents();
		System.out.println("Expected Font Family of Title Text in Sustainability Number tab 1 in Home Page is : "	+ ExpSustainabilityN1TitleHPFontFamily);
		String ExpSustainabilityN1TitleHPContent = sheet1.getCell(4,31).getContents();
		System.out.println("Expected Title Text Content in Sustainability Number tab 1 in Home Page is : "	+ ExpSustainabilityN1TitleHPContent);
				
		if(HexSustainabilityN1TitleHPColor.equals(ExpSustainabilityN1TitleHPColor))
		{
			System.out.println("Title Text in Sustainability Number tab 1 in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Title Text in Sustainability Number tab 1 in Home Page Color is not in match with creative - Fail");
			bw.write("Title Text in Sustainability Number tab 1 in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(SustainabilityN1TitleHPFontSize.equals(ExpSustainabilityN1TitleHPFontSize))
		{
			System.out.println("Title Text in Sustainability Number tab 1 in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Title Text in Sustainability Number tab 1 in Home Page Font Size is not in match with creative - Fail");
			bw.write("Title Text in Sustainability Number tab 1 in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(SustainabilityN1TitleHPFontFamily.equals(ExpSustainabilityN1TitleHPFontFamily))
		{
			System.out.println("Title Text in Sustainability Number tab 1 in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Title Text in Sustainability Number tab 1 in Home Page Font Family is not in match with creative - Fail");
			bw.write("Title Text in Sustainability Number tab 1 in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(SustainabilityN1TitleHPContent.equals(ExpSustainabilityN1TitleHPContent))
		{
			System.out.println("Title Text Content in Sustainability Number tab 1 in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Title Text Content in Sustainability Number tab 1 in Home Page is not in match with creative - Fail");
			bw.write("Title Text Content in Sustainability Number tab 1 in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexSustainabilityN1TitleHPColor.equals(ExpSustainabilityN1TitleHPColor) && SustainabilityN1TitleHPFontSize.equals(ExpSustainabilityN1TitleHPFontSize) && SustainabilityN1TitleHPFontFamily.equals(ExpSustainabilityN1TitleHPFontFamily) && SustainabilityN1TitleHPContent.equals(ExpSustainabilityN1TitleHPContent))
		{
			Label SustainabilityN1TitleHPCSSP = new Label(5,31,"PASS"); 
			wsheet1.addCell(SustainabilityN1TitleHPCSSP); 
		}
		else
		{
			Label SustainabilityN1TitleHPCSSF = new Label(5,31,"FAIL"); 
			wsheet1.addCell(SustainabilityN1TitleHPCSSF); 
		}
		
		// Sustainability Number tab 1 - Desc - Our farmers:
		
		WebElement SustainabilityN1DescHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[8]/div/div[2]/div[1]/div[1]/div[3]/div[1]")); 
		String SustainabilityN1DescHPColor = SustainabilityN1DescHP.getCssValue("color");
		System.out.println("Actual Color of Description Text in Sustainability Number tab 1 in Home Page is : "	+ SustainabilityN1DescHPColor );
		// Convert RGB to Hex Value
		Color HSustainabilityN1DescHPColor = Color.fromString(SustainabilityN1DescHPColor);
		String HexSustainabilityN1DescHPColor = HSustainabilityN1DescHPColor.asHex();
		System.out.println("Actual Hex Color of Description Text in Sustainability Number tab 1 in Home Page is : "	+ HexSustainabilityN1DescHPColor );
		
		String SustainabilityN1DescHPFontSize = SustainabilityN1DescHP.getCssValue("font-size");
		System.out.println("Actual Font Size of Description Text in Sustainability Number tab 1 in Home Page is : "	+ SustainabilityN1DescHPFontSize);
		String SustainabilityN1DescHPFontFamily = SustainabilityN1DescHP.getCssValue("font-family");
		System.out.println("Actual Font Family of Description Text in Sustainability Number tab 1 in Home Page is : "	+ SustainabilityN1DescHPFontFamily);
		String SustainabilityN1DescHPContent = SustainabilityN1DescHP.getText();
		System.out.println("Actual Description Text Content in Sustainability Number tab 1 in Home Page is : "	+ SustainabilityN1DescHPContent);
				
		String ExpSustainabilityN1DescHPColor = sheet1.getCell(1,32).getContents(); //column,row
		System.out.println("Expected Color of Description Text in Sustainability Number tab 1 in Home Page is : "	+ ExpSustainabilityN1DescHPColor );
		String ExpSustainabilityN1DescHPFontSize = sheet1.getCell(2,32).getContents();
		System.out.println("Expected Font Size of Description Text in Sustainability Number tab 1 in Home Page is : "	+ ExpSustainabilityN1DescHPFontSize);
		String ExpSustainabilityN1DescHPFontFamily = sheet1.getCell(3,32).getContents();
		System.out.println("Expected Font Family of Description Text in Sustainability Number tab 1 in Home Page is : "	+ ExpSustainabilityN1DescHPFontFamily);
		String ExpSustainabilityN1DescHPContent = sheet1.getCell(4,32).getContents();
		System.out.println("Expected Description Text Content in Sustainability Number tab 1 in Home Page is : "	+ ExpSustainabilityN1DescHPContent);
				
		if(HexSustainabilityN1DescHPColor.equals(ExpSustainabilityN1DescHPColor))
		{
			System.out.println("Description Text in Sustainability Number tab 1 in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Description Text in Sustainability Number tab 1 in Home Page Color is not in match with creative - Fail");
			bw.write("Description Text in Sustainability Number tab 1 in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(SustainabilityN1DescHPFontSize.equals(ExpSustainabilityN1DescHPFontSize))
		{
			System.out.println("Description Text in Sustainability Number tab 1 in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Description Text in Sustainability Number tab 1 in Home Page Font Size is not in match with creative - Fail");
			bw.write("Description Text in Sustainability Number tab 1 in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(SustainabilityN1DescHPFontFamily.equals(ExpSustainabilityN1DescHPFontFamily))
		{
			System.out.println("Description Text in Sustainability Number tab 1 in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Description Text in Sustainability Number tab 1 in Home Page Font Family is not in match with creative - Fail");
			bw.write("Description Text in Sustainability Number tab 1 in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(SustainabilityN1DescHPContent.equals(ExpSustainabilityN1DescHPContent))
		{
			System.out.println("Description Text Content in Sustainability Number tab 1 in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Description Text Content in Sustainability Number tab 1 in Home Page is not in match with creative - Fail");
			bw.write("Description Text Content in Sustainability Number tab 1 in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexSustainabilityN1DescHPColor.equals(ExpSustainabilityN1DescHPColor) && SustainabilityN1DescHPFontSize.equals(ExpSustainabilityN1DescHPFontSize) && SustainabilityN1DescHPFontFamily.equals(ExpSustainabilityN1DescHPFontFamily) && SustainabilityN1DescHPContent.equals(ExpSustainabilityN1DescHPContent))
		{
			Label SustainabilityN1DescHPCSSP = new Label(5,32,"PASS"); 
			wsheet1.addCell(SustainabilityN1DescHPCSSP); 
		}
		else
		{
			Label SustainabilityN1DescHPCSSF = new Label(5,32,"FAIL"); 
			wsheet1.addCell(SustainabilityN1DescHPCSSF); 
		}
		
		// Sustainability Number tab 1 - Desc1 - Receive a fair price for quality products
		
		WebElement SustainabilityN1Desc1HP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[8]/div/div[2]/div[1]/div[1]/div[3]/div[2]/ul/li[1]/span")); 
		String SustainabilityN1Desc1HPColor = SustainabilityN1Desc1HP.getCssValue("color");
		System.out.println("Actual Color of Description1 Text in Sustainability Number tab 1 in Home Page is : "	+ SustainabilityN1Desc1HPColor );
		// Convert RGB to Hex Value
		Color HSustainabilityN1Desc1HPColor = Color.fromString(SustainabilityN1Desc1HPColor);
		String HexSustainabilityN1Desc1HPColor = HSustainabilityN1Desc1HPColor.asHex();
		System.out.println("Actual Hex Color of Description1 Text in Sustainability Number tab 1 in Home Page is : "	+ HexSustainabilityN1Desc1HPColor );
		
		String SustainabilityN1Desc1HPFontSize = SustainabilityN1Desc1HP.getCssValue("font-size");
		System.out.println("Actual Font Size of Description1 Text in Sustainability Number tab 1 in Home Page is : "	+ SustainabilityN1Desc1HPFontSize);
		String SustainabilityN1Desc1HPFontFamily = SustainabilityN1Desc1HP.getCssValue("font-family");
		System.out.println("Actual Font Family of Description1 Text in Sustainability Number tab 1 in Home Page is : "	+ SustainabilityN1Desc1HPFontFamily);
		String SustainabilityN1Desc1HPContent = SustainabilityN1Desc1HP.getText();
		System.out.println("Actual Description1 Text Content in Sustainability Number tab 1 in Home Page is : "	+ SustainabilityN1Desc1HPContent);
				
		String ExpSustainabilityN1Desc1HPColor = sheet1.getCell(1,33).getContents(); //column,row
		System.out.println("Expected Color of Description1 Text in Sustainability Number tab 1 in Home Page is : "	+ ExpSustainabilityN1Desc1HPColor );
		String ExpSustainabilityN1Desc1HPFontSize = sheet1.getCell(2,33).getContents();
		System.out.println("Expected Font Size of Description1 Text in Sustainability Number tab 1 in Home Page is : "	+ ExpSustainabilityN1Desc1HPFontSize);
		String ExpSustainabilityN1Desc1HPFontFamily = sheet1.getCell(3,33).getContents();
		System.out.println("Expected Font Family of Description1 Text in Sustainability Number tab 1 in Home Page is : "	+ ExpSustainabilityN1Desc1HPFontFamily);
		String ExpSustainabilityN1Desc1HPContent = sheet1.getCell(4,33).getContents();
		System.out.println("Expected Description1 Text Content in Sustainability Number tab 1 in Home Page is : "	+ ExpSustainabilityN1Desc1HPContent);
				
		if(HexSustainabilityN1Desc1HPColor.equals(ExpSustainabilityN1Desc1HPColor))
		{
			System.out.println("Description1 Text in Sustainability Number tab 1 in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Description1 Text in Sustainability Number tab 1 in Home Page Color is not in match with creative - Fail");
			bw.write("Description1 Text in Sustainability Number tab 1 in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(SustainabilityN1Desc1HPFontSize.equals(ExpSustainabilityN1Desc1HPFontSize))
		{
			System.out.println("Description1 Text in Sustainability Number tab 1 in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Description1 Text in Sustainability Number tab 1 in Home Page Font Size is not in match with creative - Fail");
			bw.write("Description1 Text in Sustainability Number tab 1 in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(SustainabilityN1Desc1HPFontFamily.equals(ExpSustainabilityN1Desc1HPFontFamily))
		{
			System.out.println("Description1 Text in Sustainability Number tab 1 in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Description1 Text in Sustainability Number tab 1 in Home Page Font Family is not in match with creative - Fail");
			bw.write("Description1 Text in Sustainability Number tab 1 in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(SustainabilityN1Desc1HPContent.equals(ExpSustainabilityN1Desc1HPContent))
		{
			System.out.println("Description1 Text Content in Sustainability Number tab 1 in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Description1 Text Content in Sustainability Number tab 1 in Home Page is not in match with creative - Fail");
			bw.write("Description1 Text Content in Sustainability Number tab 1 in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexSustainabilityN1Desc1HPColor.equals(ExpSustainabilityN1Desc1HPColor) && SustainabilityN1Desc1HPFontSize.equals(ExpSustainabilityN1Desc1HPFontSize) && SustainabilityN1Desc1HPFontFamily.equals(ExpSustainabilityN1Desc1HPFontFamily) && SustainabilityN1Desc1HPContent.equals(ExpSustainabilityN1Desc1HPContent))
		{
			Label SustainabilityN1Desc1HPCSSP = new Label(5,33,"PASS"); 
			wsheet1.addCell(SustainabilityN1Desc1HPCSSP); 
		}
		else
		{
			Label SustainabilityN1Desc1HPCSSF = new Label(5,33,"FAIL"); 
			wsheet1.addCell(SustainabilityN1Desc1HPCSSF); 
		}
		
		// Sustainability Number tab 1 - Desc2 - Can reinvest in their crops and communities
		
		WebElement SustainabilityN1Desc2HP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[8]/div/div[2]/div[1]/div[1]/div[3]/div[2]/ul/li[2]/span")); 
		String SustainabilityN1Desc2HPColor = SustainabilityN1Desc2HP.getCssValue("color");
		System.out.println("Actual Color of Description2 Text in Sustainability Number tab 1 in Home Page is : "	+ SustainabilityN1Desc2HPColor );
		// Convert RGB to Hex Value
		Color HSustainabilityN1Desc2HPColor = Color.fromString(SustainabilityN1Desc2HPColor);
		String HexSustainabilityN1Desc2HPColor = HSustainabilityN1Desc2HPColor.asHex();
		System.out.println("Actual Hex Color of Description2 Text in Sustainability Number tab 1 in Home Page is : "	+ HexSustainabilityN1Desc2HPColor );
		
		String SustainabilityN1Desc2HPFontSize = SustainabilityN1Desc2HP.getCssValue("font-size");
		System.out.println("Actual Font Size of Description2 Text in Sustainability Number tab 1 in Home Page is : "	+ SustainabilityN1Desc2HPFontSize);
		String SustainabilityN1Desc2HPFontFamily = SustainabilityN1Desc2HP.getCssValue("font-family");
		System.out.println("Actual Font Family of Description2 Text in Sustainability Number tab 1 in Home Page is : "	+ SustainabilityN1Desc2HPFontFamily);
		String SustainabilityN1Desc2HPContent = SustainabilityN1Desc2HP.getText();
		System.out.println("Actual Description2 Text Content in Sustainability Number tab 1 in Home Page is : "	+ SustainabilityN1Desc2HPContent);
				
		String ExpSustainabilityN1Desc2HPColor = sheet1.getCell(1,34).getContents(); //column,row
		System.out.println("Expected Color of Description2 Text in Sustainability Number tab 1 in Home Page is : "	+ ExpSustainabilityN1Desc2HPColor );
		String ExpSustainabilityN1Desc2HPFontSize = sheet1.getCell(2,34).getContents();
		System.out.println("Expected Font Size of Description2 Text in Sustainability Number tab 1 in Home Page is : "	+ ExpSustainabilityN1Desc2HPFontSize);
		String ExpSustainabilityN1Desc2HPFontFamily = sheet1.getCell(3,34).getContents();
		System.out.println("Expected Font Family of Description2 Text in Sustainability Number tab 1 in Home Page is : "	+ ExpSustainabilityN1Desc2HPFontFamily);
		String ExpSustainabilityN1Desc2HPContent = sheet1.getCell(4,34).getContents();
		System.out.println("Expected Description2 Text Content in Sustainability Number tab 1 in Home Page is : "	+ ExpSustainabilityN1Desc2HPContent);
				
		if(HexSustainabilityN1Desc2HPColor.equals(ExpSustainabilityN1Desc2HPColor))
		{
			System.out.println("Description2 Text in Sustainability Number tab 1 in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Description2 Text in Sustainability Number tab 1 in Home Page Color is not in match with creative - Fail");
			bw.write("Description2 Text in Sustainability Number tab 1 in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(SustainabilityN1Desc2HPFontSize.equals(ExpSustainabilityN1Desc2HPFontSize))
		{
			System.out.println("Description2 Text in Sustainability Number tab 1 in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Description2 Text in Sustainability Number tab 1 in Home Page Font Size is not in match with creative - Fail");
			bw.write("Description2 Text in Sustainability Number tab 1 in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(SustainabilityN1Desc2HPFontFamily.equals(ExpSustainabilityN1Desc2HPFontFamily))
		{
			System.out.println("Description2 Text in Sustainability Number tab 1 in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Description2 Text in Sustainability Number tab 1 in Home Page Font Family is not in match with creative - Fail");
			bw.write("Description2 Text in Sustainability Number tab 1 in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(SustainabilityN1Desc2HPContent.equals(ExpSustainabilityN1Desc2HPContent))
		{
			System.out.println("Description2 Text Content in Sustainability Number tab 1 in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Description2 Text Content in Sustainability Number tab 1 in Home Page is not in match with creative - Fail");
			bw.write("Description2 Text Content in Sustainability Number tab 1 in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexSustainabilityN1Desc2HPColor.equals(ExpSustainabilityN1Desc2HPColor) && SustainabilityN1Desc2HPFontSize.equals(ExpSustainabilityN1Desc2HPFontSize) && SustainabilityN1Desc2HPFontFamily.equals(ExpSustainabilityN1Desc2HPFontFamily) && SustainabilityN1Desc2HPContent.equals(ExpSustainabilityN1Desc2HPContent))
		{
			Label SustainabilityN1Desc2HPCSSP = new Label(5,34,"PASS"); 
			wsheet1.addCell(SustainabilityN1Desc2HPCSSP); 
		}
		else
		{
			Label SustainabilityN1Desc2HPCSSF = new Label(5,34,"FAIL"); 
			wsheet1.addCell(SustainabilityN1Desc2HPCSSF); 
		}
		
		// Sustainability Number tab 1 - Desc3 - Achieve better health care, education and quality of life
		
		WebElement SustainabilityN1Desc3HP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[8]/div/div[2]/div[1]/div[1]/div[3]/div[2]/ul/li[3]/span")); 
		String SustainabilityN1Desc3HPColor = SustainabilityN1Desc3HP.getCssValue("color");
		System.out.println("Actual Color of Description3 Text in Sustainability Number tab 1 in Home Page is : "	+ SustainabilityN1Desc3HPColor );
		// Convert RGB to Hex Value
		Color HSustainabilityN1Desc3HPColor = Color.fromString(SustainabilityN1Desc3HPColor);
		String HexSustainabilityN1Desc3HPColor = HSustainabilityN1Desc3HPColor.asHex();
		System.out.println("Actual Hex Color of Description3 Text in Sustainability Number tab 1 in Home Page is : "	+ HexSustainabilityN1Desc3HPColor );
		
		String SustainabilityN1Desc3HPFontSize = SustainabilityN1Desc3HP.getCssValue("font-size");
		System.out.println("Actual Font Size of Description3 Text in Sustainability Number tab 1 in Home Page is : "	+ SustainabilityN1Desc3HPFontSize);
		String SustainabilityN1Desc3HPFontFamily = SustainabilityN1Desc3HP.getCssValue("font-family");
		System.out.println("Actual Font Family of Description3 Text in Sustainability Number tab 1 in Home Page is : "	+ SustainabilityN1Desc3HPFontFamily);
		String SustainabilityN1Desc3HPContent = SustainabilityN1Desc3HP.getText();
		System.out.println("Actual Description3 Text Content in Sustainability Number tab 1 in Home Page is : "	+ SustainabilityN1Desc3HPContent);
				
		String ExpSustainabilityN1Desc3HPColor = sheet1.getCell(1,35).getContents(); //column,row
		System.out.println("Expected Color of Description3 Text in Sustainability Number tab 1 in Home Page is : "	+ ExpSustainabilityN1Desc3HPColor );
		String ExpSustainabilityN1Desc3HPFontSize = sheet1.getCell(2,35).getContents();
		System.out.println("Expected Font Size of Description3 Text in Sustainability Number tab 1 in Home Page is : "	+ ExpSustainabilityN1Desc3HPFontSize);
		String ExpSustainabilityN1Desc3HPFontFamily = sheet1.getCell(3,35).getContents();
		System.out.println("Expected Font Family of Description3 Text in Sustainability Number tab 1 in Home Page is : "	+ ExpSustainabilityN1Desc3HPFontFamily);
		String ExpSustainabilityN1Desc3HPContent = sheet1.getCell(4,35).getContents();
		System.out.println("Expected Description3 Text Content in Sustainability Number tab 1 in Home Page is : "	+ ExpSustainabilityN1Desc3HPContent);
				
		if(HexSustainabilityN1Desc3HPColor.equals(ExpSustainabilityN1Desc3HPColor))
		{
			System.out.println("Description3 Text in Sustainability Number tab 1 in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Description3 Text in Sustainability Number tab 1 in Home Page Color is not in match with creative - Fail");
			bw.write("Description3 Text in Sustainability Number tab 1 in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(SustainabilityN1Desc3HPFontSize.equals(ExpSustainabilityN1Desc3HPFontSize))
		{
			System.out.println("Description3 Text in Sustainability Number tab 1 in Home Page Font Size is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("Description3 Text in Sustainability Number tab 1 in Home Page Font Size is not in match with creative - Fail");
			bw.write("Description3 Text in Sustainability Number tab 1 in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(SustainabilityN1Desc3HPFontFamily.equals(ExpSustainabilityN1Desc3HPFontFamily))
		{
			System.out.println("Description3 Text in Sustainability Number tab 1 in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Description3 Text in Sustainability Number tab 1 in Home Page Font Family is not in match with creative - Fail");
			bw.write("Description3 Text in Sustainability Number tab 1 in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(SustainabilityN1Desc3HPContent.equals(ExpSustainabilityN1Desc3HPContent))
		{
			System.out.println("Description3 Text Content in Sustainability Number tab 1 in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Description3 Text Content in Sustainability Number tab 1 in Home Page is not in match with creative - Fail");
			bw.write("Description3 Text Content in Sustainability Number tab 1 in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexSustainabilityN1Desc3HPColor.equals(ExpSustainabilityN1Desc3HPColor) && SustainabilityN1Desc3HPFontSize.equals(ExpSustainabilityN1Desc3HPFontSize) && SustainabilityN1Desc3HPFontFamily.equals(ExpSustainabilityN1Desc3HPFontFamily) && SustainabilityN1Desc3HPContent.equals(ExpSustainabilityN1Desc3HPContent))
		{
			Label SustainabilityN1Desc3HPCSSP = new Label(5,35,"PASS"); 
			wsheet1.addCell(SustainabilityN1Desc3HPCSSP); 
		}
		else
		{
			Label SustainabilityN1Desc3HPCSSF = new Label(5,35,"FAIL"); 
			wsheet1.addCell(SustainabilityN1Desc3HPCSSF); 
		}
		
		
		
		// Sustainability Number tab 2 - Title - OUR HANDS-ON EFFORTS
		
		//click action on Sustainability Number tab 2
		
		Thread.sleep(3000);
		//JavascriptExecutor jsesnt2 = (JavascriptExecutor)driver;
		//jsesnt2.executeScript("window.scrollBy(0,1050)", "");
		//Thread.sleep(3000);
		//jsesnt2.executeScript("window.scrollBy(0,1050)", "");
		//Thread.sleep(3000);
		//jsesnt2.executeScript("window.scrollBy(0,1050)", "");
		//Thread.sleep(3000);
		//jsesnt2.executeScript("window.scrollBy(0,1050)", "");
		//Thread.sleep(8000);
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[8]/div/div[2]/div[2]/div[2]")).click();  
		Thread.sleep(3000);
	
		WebElement SustainabilityN2TitleHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[8]/div/div[2]/div[1]/div[2]/div[1]")); 
		String SustainabilityN2TitleHPColor = SustainabilityN2TitleHP.getCssValue("color");
		System.out.println("Actual Color of Title Text in Sustainability Number tab 2 in Home Page is : "	+ SustainabilityN2TitleHPColor );
		// Convert RGB to Hex Value
		Color HSustainabilityN2TitleHPColor = Color.fromString(SustainabilityN2TitleHPColor);
		String HexSustainabilityN2TitleHPColor = HSustainabilityN2TitleHPColor.asHex();
		System.out.println("Actual Hex Color of Title Text in Sustainability Number tab 2 in Home Page is : "	+ HexSustainabilityN2TitleHPColor );
		
		String SustainabilityN2TitleHPFontSize = SustainabilityN2TitleHP.getCssValue("font-size");
		System.out.println("Actual Font Size of Title Text in Sustainability Number tab 2 in Home Page is : "	+ SustainabilityN2TitleHPFontSize);
		String SustainabilityN2TitleHPFontFamily = SustainabilityN2TitleHP.getCssValue("font-family");
		System.out.println("Actual Font Family of Title Text in Sustainability Number tab 2 in Home Page is : "	+ SustainabilityN2TitleHPFontFamily);
		String SustainabilityN2TitleHPContent = SustainabilityN2TitleHP.getText();
		System.out.println("Actual Title Text Content in Sustainability Number tab 2 in Home Page is : "	+ SustainabilityN2TitleHPContent);
				
		String ExpSustainabilityN2TitleHPColor = sheet1.getCell(1,36).getContents(); //column,row
		System.out.println("Expected Color of Title Text in Sustainability Number tab 2 in Home Page is : "	+ ExpSustainabilityN2TitleHPColor );
		String ExpSustainabilityN2TitleHPFontSize = sheet1.getCell(2,36).getContents();
		System.out.println("Expected Font Size of Title Text in Sustainability Number tab 2 in Home Page is : "	+ ExpSustainabilityN2TitleHPFontSize);
		String ExpSustainabilityN2TitleHPFontFamily = sheet1.getCell(3,36).getContents();
		System.out.println("Expected Font Family of Title Text in Sustainability Number tab 2 in Home Page is : "	+ ExpSustainabilityN2TitleHPFontFamily);
		String ExpSustainabilityN2TitleHPContent = sheet1.getCell(4,36).getContents();
		System.out.println("Expected Title Text Content in Sustainability Number tab 2 in Home Page is : "	+ ExpSustainabilityN2TitleHPContent);
				
		if(HexSustainabilityN2TitleHPColor.equals(ExpSustainabilityN2TitleHPColor))
		{
			System.out.println("Title Text in Sustainability Number tab 2 in Home Page Color is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("Title Text in Sustainability Number tab 2 in Home Page Color is not in match with creative - Fail");
			bw.write("Title Text in Sustainability Number tab 2 in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(SustainabilityN2TitleHPFontSize.equals(ExpSustainabilityN2TitleHPFontSize))
		{
			System.out.println("Title Text in Sustainability Number tab 2 in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Title Text in Sustainability Number tab 2 in Home Page Font Size is not in match with creative - Fail");
			bw.write("Title Text in Sustainability Number tab 2 in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(SustainabilityN2TitleHPFontFamily.equals(ExpSustainabilityN2TitleHPFontFamily))
		{
			System.out.println("Title Text in Sustainability Number tab 2 in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Title Text in Sustainability Number tab 2 in Home Page Font Family is not in match with creative - Fail");
			bw.write("Title Text in Sustainability Number tab 2 in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(SustainabilityN2TitleHPContent.equals(ExpSustainabilityN2TitleHPContent))
		{
			System.out.println("Title Text Content in Sustainability Number tab 2 in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Title Text Content in Sustainability Number tab 2 in Home Page is not in match with creative - Fail");
			bw.write("Title Text Content in Sustainability Number tab 2 in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexSustainabilityN2TitleHPColor.equals(ExpSustainabilityN2TitleHPColor) && SustainabilityN2TitleHPFontSize.equals(ExpSustainabilityN2TitleHPFontSize) && SustainabilityN2TitleHPFontFamily.equals(ExpSustainabilityN2TitleHPFontFamily) && SustainabilityN2TitleHPContent.equals(ExpSustainabilityN2TitleHPContent))
		{
			Label SustainabilityN2TitleHPCSSP = new Label(5,36,"PASS"); 
			wsheet1.addCell(SustainabilityN2TitleHPCSSP); 
		}
		else
		{
			Label SustainabilityN2TitleHPCSSF = new Label(5,36,"FAIL"); 
			wsheet1.addCell(SustainabilityN2TitleHPCSSF); 
		}
		
		// Sustainability Number tab 2 - Desc - With local partners, we strive to:
		
		WebElement SustainabilityN2DescHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[8]/div/div[2]/div[1]/div[2]/div[3]/div[2]")); 
		String SustainabilityN2DescHPColor = SustainabilityN2DescHP.getCssValue("color");
		System.out.println("Actual Color of Description Text in Sustainability Number tab 2 in Home Page is : "	+ SustainabilityN2DescHPColor );
		// Convert RGB to Hex Value
		Color HSustainabilityN2DescHPColor = Color.fromString(SustainabilityN2DescHPColor);
		String HexSustainabilityN2DescHPColor = HSustainabilityN2DescHPColor.asHex();
		System.out.println("Actual Hex Color of Description Text in Sustainability Number tab 2 in Home Page is : "	+ HexSustainabilityN2DescHPColor );
		
		String SustainabilityN2DescHPFontSize = SustainabilityN2DescHP.getCssValue("font-size");
		System.out.println("Actual Font Size of Description Text in Sustainability Number tab 2 in Home Page is : "	+ SustainabilityN2DescHPFontSize);
		String SustainabilityN2DescHPFontFamily = SustainabilityN2DescHP.getCssValue("font-family");
		System.out.println("Actual Font Family of Description Text in Sustainability Number tab 2 in Home Page is : "	+ SustainabilityN2DescHPFontFamily);
		String SustainabilityN2DescHPContent = SustainabilityN2DescHP.getText();
		System.out.println("Actual Description Text Content in Sustainability Number tab 2 in Home Page is : "	+ SustainabilityN2DescHPContent);
				
		String ExpSustainabilityN2DescHPColor = sheet1.getCell(1,37).getContents(); //column,row
		System.out.println("Expected Color of Description Text in Sustainability Number tab 2 in Home Page is : "	+ ExpSustainabilityN2DescHPColor );
		String ExpSustainabilityN2DescHPFontSize = sheet1.getCell(2,37).getContents();
		System.out.println("Expected Font Size of Description Text in Sustainability Number tab 2 in Home Page is : "	+ ExpSustainabilityN2DescHPFontSize);
		String ExpSustainabilityN2DescHPFontFamily = sheet1.getCell(3,37).getContents();
		System.out.println("Expected Font Family of Description Text in Sustainability Number tab 2 in Home Page is : "	+ ExpSustainabilityN2DescHPFontFamily);
		String ExpSustainabilityN2DescHPContent = sheet1.getCell(4,37).getContents();
		System.out.println("Expected Description Text Content in Sustainability Number tab 2 in Home Page is : "	+ ExpSustainabilityN2DescHPContent);
				
		if(HexSustainabilityN2DescHPColor.equals(ExpSustainabilityN2DescHPColor))
		{
			System.out.println("Description Text in Sustainability Number tab 2 in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Description Text in Sustainability Number tab 2 in Home Page Color is not in match with creative - Fail");
			bw.write("Description Text in Sustainability Number tab 2 in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(SustainabilityN2DescHPFontSize.equals(ExpSustainabilityN2DescHPFontSize))
		{
			System.out.println("Description Text in Sustainability Number tab 2 in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Description Text in Sustainability Number tab 2 in Home Page Font Size is not in match with creative - Fail");
			bw.write("Description Text in Sustainability Number tab 2 in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(SustainabilityN2DescHPFontFamily.equals(ExpSustainabilityN2DescHPFontFamily))
		{
			System.out.println("Description Text in Sustainability Number tab 2 in Home Page Font Family is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("Description Text in Sustainability Number tab 2 in Home Page Font Family is not in match with creative - Fail");
			bw.write("Description Text in Sustainability Number tab 2 in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(SustainabilityN2DescHPContent.equals(ExpSustainabilityN2DescHPContent))
		{
			System.out.println("Description Text Content in Sustainability Number tab 2 in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Description Text Content in Sustainability Number tab 2 in Home Page is not in match with creative - Fail");
			bw.write("Description Text Content in Sustainability Number tab 2 in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexSustainabilityN2DescHPColor.equals(ExpSustainabilityN2DescHPColor) && SustainabilityN2DescHPFontSize.equals(ExpSustainabilityN2DescHPFontSize) && SustainabilityN2DescHPFontFamily.equals(ExpSustainabilityN2DescHPFontFamily) && SustainabilityN2DescHPContent.equals(ExpSustainabilityN2DescHPContent))
		{
			Label SustainabilityN2DescHPCSSP = new Label(5,37,"PASS"); 
			wsheet1.addCell(SustainabilityN2DescHPCSSP); 
		}
		else
		{
			Label SustainabilityN2DescHPCSSF = new Label(5,37,"FAIL"); 
			wsheet1.addCell(SustainabilityN2DescHPCSSF); 
		}
		
		
		// Sustainability Number tab 3 - Title - INTERCAMBIO 
		
		//click action on Sustainability Number tab 3
		
		Thread.sleep(3000);
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[8]/div/div[2]/div[2]/div[3]")).click();  
		Thread.sleep(3000);
	
		WebElement SustainabilityN3TitleHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[8]/div/div[2]/div[1]/div[3]/div[1]")); 
		String SustainabilityN3TitleHPColor = SustainabilityN3TitleHP.getCssValue("color");
		System.out.println("Actual Color of Title Text in Sustainability Number tab 3 in Home Page is : "	+ SustainabilityN3TitleHPColor );
		// Convert RGB to Hex Value
		Color HSustainabilityN3TitleHPColor = Color.fromString(SustainabilityN3TitleHPColor);
		String HexSustainabilityN3TitleHPColor = HSustainabilityN3TitleHPColor.asHex();
		System.out.println("Actual Hex Color of Title Text in Sustainability Number tab 3 in Home Page is : "	+ HexSustainabilityN3TitleHPColor );
		
		String SustainabilityN3TitleHPFontSize = SustainabilityN3TitleHP.getCssValue("font-size");
		System.out.println("Actual Font Size of Title Text in Sustainability Number tab 3 in Home Page is : "	+ SustainabilityN3TitleHPFontSize);
		String SustainabilityN3TitleHPFontFamily = SustainabilityN3TitleHP.getCssValue("font-family");
		System.out.println("Actual Font Family of Title Text in Sustainability Number tab 3 in Home Page is : "	+ SustainabilityN3TitleHPFontFamily);
		String SustainabilityN3TitleHPContent = SustainabilityN3TitleHP.getText();
		System.out.println("Actual Title Text Content in Sustainability Number tab 3 in Home Page is : "	+ SustainabilityN3TitleHPContent);
				
		String ExpSustainabilityN3TitleHPColor = sheet1.getCell(1,38).getContents(); //column,row
		System.out.println("Expected Color of Title Text in Sustainability Number tab 3 in Home Page is : "	+ ExpSustainabilityN3TitleHPColor );
		String ExpSustainabilityN3TitleHPFontSize = sheet1.getCell(2,38).getContents();
		System.out.println("Expected Font Size of Title Text in Sustainability Number tab 3 in Home Page is : "	+ ExpSustainabilityN3TitleHPFontSize);
		String ExpSustainabilityN3TitleHPFontFamily = sheet1.getCell(3,38).getContents();
		System.out.println("Expected Font Family of Title Text in Sustainability Number tab 3 in Home Page is : "	+ ExpSustainabilityN3TitleHPFontFamily);
		String ExpSustainabilityN3TitleHPContent = sheet1.getCell(4,38).getContents();
		System.out.println("Expected Title Text Content in Sustainability Number tab 3 in Home Page is : "	+ ExpSustainabilityN3TitleHPContent);
				
		if(HexSustainabilityN3TitleHPColor.equals(ExpSustainabilityN3TitleHPColor))
		{
			System.out.println("Title Text in Sustainability Number tab 3 in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Title Text in Sustainability Number tab 3 in Home Page Color is not in match with creative - Fail");
			bw.write("Title Text in Sustainability Number tab 3 in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(SustainabilityN3TitleHPFontSize.equals(ExpSustainabilityN3TitleHPFontSize))
		{
			System.out.println("Title Text in Sustainability Number tab 3 in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Title Text in Sustainability Number tab 3 in Home Page Font Size is not in match with creative - Fail");
			bw.write("Title Text in Sustainability Number tab 3 in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(SustainabilityN3TitleHPFontFamily.equals(ExpSustainabilityN3TitleHPFontFamily))
		{
			System.out.println("Title Text in Sustainability Number tab 3 in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Title Text in Sustainability Number tab 3 in Home Page Font Family is not in match with creative - Fail");
			bw.write("Title Text in Sustainability Number tab 3 in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(SustainabilityN3TitleHPContent.equals(ExpSustainabilityN3TitleHPContent))
		{
			System.out.println("Title Text Content in Sustainability Number tab 3 in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Title Text Content in Sustainability Number tab 3 in Home Page is not in match with creative - Fail");
			bw.write("Title Text Content in Sustainability Number tab 3 in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexSustainabilityN3TitleHPColor.equals(ExpSustainabilityN3TitleHPColor) && SustainabilityN3TitleHPFontSize.equals(ExpSustainabilityN3TitleHPFontSize) && SustainabilityN3TitleHPFontFamily.equals(ExpSustainabilityN3TitleHPFontFamily) && SustainabilityN3TitleHPContent.equals(ExpSustainabilityN3TitleHPContent))
		{
			Label SustainabilityN3TitleHPCSSP = new Label(5,38,"PASS"); 
			wsheet1.addCell(SustainabilityN3TitleHPCSSP); 
		}
		else
		{
			Label SustainabilityN3TitleHPCSSF = new Label(5,38,"FAIL"); 
			wsheet1.addCell(SustainabilityN3TitleHPCSSF); 
		}
		
		// Sustainability Number tab 3 - Desc 
		
		WebElement SustainabilityN3DescHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[8]/div/div[2]/div[1]/div[3]/div[3]/div[2]")); 
		String SustainabilityN3DescHPColor = SustainabilityN3DescHP.getCssValue("color");
		System.out.println("Actual Color of Description Text in Sustainability Number tab 3 in Home Page is : "	+ SustainabilityN3DescHPColor );
		// Convert RGB to Hex Value
		Color HSustainabilityN3DescHPColor = Color.fromString(SustainabilityN3DescHPColor);
		String HexSustainabilityN3DescHPColor = HSustainabilityN3DescHPColor.asHex();
		System.out.println("Actual Hex Color of Description Text in Sustainability Number tab 3 in Home Page is : "	+ HexSustainabilityN3DescHPColor );
		
		String SustainabilityN3DescHPFontSize = SustainabilityN3DescHP.getCssValue("font-size");
		System.out.println("Actual Font Size of Description Text in Sustainability Number tab 3 in Home Page is : "	+ SustainabilityN3DescHPFontSize);
		String SustainabilityN3DescHPFontFamily = SustainabilityN3DescHP.getCssValue("font-family");
		System.out.println("Actual Font Family of Description Text in Sustainability Number tab 3 in Home Page is : "	+ SustainabilityN3DescHPFontFamily);
		String SustainabilityN3DescHPContent = SustainabilityN3DescHP.getText();
		System.out.println("Actual Description Text Content in Sustainability Number tab 3 in Home Page is : "	+ SustainabilityN3DescHPContent);
				
		String ExpSustainabilityN3DescHPColor = sheet1.getCell(1,39).getContents(); //column,row
		System.out.println("Expected Color of Description Text in Sustainability Number tab 3 in Home Page is : "	+ ExpSustainabilityN3DescHPColor );
		String ExpSustainabilityN3DescHPFontSize = sheet1.getCell(2,39).getContents();
		System.out.println("Expected Font Size of Description Text in Sustainability Number tab 3 in Home Page is : "	+ ExpSustainabilityN3DescHPFontSize);
		String ExpSustainabilityN3DescHPFontFamily = sheet1.getCell(3,39).getContents();
		System.out.println("Expected Font Family of Description Text in Sustainability Number tab 3 in Home Page is : "	+ ExpSustainabilityN3DescHPFontFamily);
		String ExpSustainabilityN3DescHPContent = sheet1.getCell(4,39).getContents();
		System.out.println("Expected Description Text Content in Sustainability Number tab 3 in Home Page is : "	+ ExpSustainabilityN3DescHPContent);
				
		if(HexSustainabilityN3DescHPColor.equals(ExpSustainabilityN3DescHPColor))
		{
			System.out.println("Description Text in Sustainability Number tab 3 in Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Description Text in Sustainability Number tab 3 in Home Page Color is not in match with creative - Fail");
			bw.write("Description Text in Sustainability Number tab 3 in Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(SustainabilityN3DescHPFontSize.equals(ExpSustainabilityN3DescHPFontSize))
		{
			System.out.println("Description Text in Sustainability Number tab 3 in Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Description Text in Sustainability Number tab 3 in Home Page Font Size is not in match with creative - Fail");
			bw.write("Description Text in Sustainability Number tab 3 in Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(SustainabilityN3DescHPFontFamily.equals(ExpSustainabilityN3DescHPFontFamily))
		{
			System.out.println("Description Text in Sustainability Number tab 3 in Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Description Text in Sustainability Number tab 3 in Home Page Font Family is not in match with creative - Fail");
			bw.write("Description Text in Sustainability Number tab 3 in Home Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(SustainabilityN3DescHPContent.equals(ExpSustainabilityN3DescHPContent))
		{
			System.out.println("Description Text Content in Sustainability Number tab 3 in Home Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Description Text Content in Sustainability Number tab 3 in Home Page is not in match with creative - Fail");
			bw.write("Description Text Content in Sustainability Number tab 3 in Home Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexSustainabilityN3DescHPColor.equals(ExpSustainabilityN3DescHPColor) && SustainabilityN3DescHPFontSize.equals(ExpSustainabilityN3DescHPFontSize) && SustainabilityN3DescHPFontFamily.equals(ExpSustainabilityN3DescHPFontFamily) && SustainabilityN3DescHPContent.equals(ExpSustainabilityN3DescHPContent))
		{
			Label SustainabilityN3DescHPCSSP = new Label(5,39,"PASS"); 
			wsheet1.addCell(SustainabilityN3DescHPCSSP); 
		}
		else
		{
			Label SustainabilityN3DescHPCSSF = new Label(5,39,"FAIL"); 
			wsheet1.addCell(SustainabilityN3DescHPCSSF); 
		}
				
		// Footer
		
		Thread.sleep(3000);
		JavascriptExecutor jsefoot = (JavascriptExecutor)driver;
		jsefoot.executeScript("window.scrollBy(0,500)", "");
		Thread.sleep(3000);
		
		// Quality Link in Footer
		
		WebElement QualityHPF = driver.findElement(By.xpath(".//*[@id='id_skgm_fw_footerLayoutCell']/div/div[2]/div[1]/div/a[2]/div")); 
    	String QualityFHPColor = QualityHPF.getCssValue("color");
		System.out.println("Actual Color of Quality Text in Footer in Home Page is : "	+ QualityFHPColor );
		// Convert RGB to Hex Value
		Color QuafooterHP = Color.fromString(QualityFHPColor);
		String HexQualityFHPColor = QuafooterHP.asHex();
		System.out.println("Actual Hex Color of Quality Text in Footer in Home Page is : "	+ HexQualityFHPColor );
		
		String QualityFHPFontSize = QualityHPF.getCssValue("font-size");
		System.out.println("Actual Font Size of Quality Text in Footer in Home Page is : "	+ QualityFHPFontSize);
		String QualityFHPFontFamily = QualityHPF.getCssValue("font-family");
		System.out.println("Actual Font Family of Quality Text in Footer in Home Page is : "	+ QualityFHPFontFamily);
		
		String ExpQualityFHPColor = sheet1.getCell(1,60).getContents(); //column,row
		System.out.println("Expected Color of Quality Text in Footer in Home Page is : "	+ ExpQualityFHPColor );
		String ExpQualityFHPFontSize = sheet1.getCell(2,60).getContents();
		System.out.println("Expected Font Size of Quality Text in Footer in Home Page is : "	+ ExpQualityFHPFontSize);
		String ExpQualityFHPFontFamily = sheet1.getCell(3,60).getContents();
		System.out.println("Expected Font Family of Quality Text in Footer in Home Page is : "	+ ExpQualityFHPFontFamily);
		
		if(HexQualityFHPColor.equals(ExpQualityFHPColor))
		{
			System.out.println("Footer - Quality Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Footer - Quality Home Page Color is not in match with creative - Fail");
			bw.write("Footer - Quality Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(QualityFHPFontSize.equals(ExpQualityFHPFontSize))
		{
			System.out.println("Footer - Quality Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Footer - Quality Home Page Font Size is not in match with creative - Fail");
			bw.write("Footer - Quality Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(QualityFHPFontFamily.equals(ExpQualityFHPFontFamily))
		{
			System.out.println("Footer - Quality Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Footer - Quality Home Page Font Family is not in match with creative - Fail");
			bw.write("Footer - Quality Home Page Color is Font Family in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexQualityFHPColor.equals(ExpQualityFHPColor) && QualityFHPFontSize.equals(ExpQualityFHPFontSize) && QualityFHPFontFamily.equals(ExpQualityFHPFontFamily))
		{
			Label QualityHPFCSSP = new Label(5,60,"PASS"); 
			wsheet1.addCell(QualityHPFCSSP); 
		}
		else
		{
			Label QualityHPFCSSF = new Label(5,60,"FAIL"); 
			wsheet1.addCell(QualityHPFCSSF); 
		}
		
		// Variety Link in Footer
    	
    	WebElement VarietyFHP = driver.findElement(By.xpath(".//*[@id='id_skgm_fw_footerLayoutCell']/div/div[2]/div[1]/div/a[3]/div")); 
    	String VarietyFHPColor = VarietyFHP.getCssValue("color");
    	System.out.println("Footer - Actual Color of Variety Text in Home Page is : "	+ VarietyFHPColor );
    	// Convert RGB to Hex Value
		Color VarfooterHP = Color.fromString(VarietyFHPColor);
		String HexVarietyFHPColor = VarfooterHP.asHex();
		System.out.println("Footer - Actual Hex Color of Variety Text in Home Page is : "	+ HexVarietyFHPColor );
		
		String VarietyFHPFontSize = VarietyFHP.getCssValue("font-size");
		System.out.println("Footer - Actual Font Size of Variety Text in Home Page is : "	+ VarietyFHPFontSize);
		String VarietyFHPFontFamily = VarietyFHP.getCssValue("font-family");
		System.out.println("Footer - Actual Font Family of Variety Text in Home Page is : "	+ VarietyFHPFontFamily);
		
		String ExpVarietyFHPColor = sheet1.getCell(1,61).getContents(); //column,row
		System.out.println("Footer - Expected Color of Variety Text in Home Page is : "	+ ExpVarietyFHPColor );
		String ExpVarietyFHPFontSize = sheet1.getCell(2,61).getContents();
		System.out.println("Footer - Expected Font Size of Variety Text in Home Page is : "	+ ExpVarietyFHPFontSize);
		String ExpVarietyFHPFontFamily = sheet1.getCell(3,61).getContents();
		System.out.println("Footer - Expected Font Family of Variety Text in Home Page is : "	+ ExpVarietyFHPFontFamily);
		
		if(HexVarietyFHPColor.equals(ExpVarietyFHPColor))
		{
			System.out.println("Footer - Variety Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Footer - Variety Home Page Color is not in match with creative - Fail");
			bw.write("Footer - Variety Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(VarietyFHPFontSize.equals(ExpVarietyFHPFontSize))
		{
			System.out.println("Footer - Variety Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Footer - Variety Home Page Font Size is not in match with creative - Fail");
			bw.write("Footer - Variety Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(VarietyFHPFontFamily.equals(ExpVarietyFHPFontFamily))
		{
			System.out.println("Footer - Variety Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Footer - Variety Home Page Font Family is not in match with creative - Fail");
			bw.write("Footer - Variety Home Page Color is Font Family in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexVarietyFHPColor.equals(ExpVarietyFHPColor) && VarietyFHPFontSize.equals(ExpVarietyFHPFontSize) && VarietyFHPFontFamily.equals(ExpVarietyFHPFontFamily))
		{
			Label VarietyFHPCSSP = new Label(5,61,"PASS"); 
			wsheet1.addCell(VarietyFHPCSSP); 
		}
		else
		{
			Label VarietyFHPCSSF = new Label(5,61,"FAIL"); 
			wsheet1.addCell(VarietyFHPCSSF); 
		}
		
		// Sustainability Link in Footer
    	
    	WebElement SustainabilityFHP = driver.findElement(By.xpath(".//*[@id='id_skgm_fw_footerLayoutCell']/div/div[2]/div[1]/div/a[4]/div")); 
    	String SustainabilityFHPColor = SustainabilityFHP.getCssValue("color");
		System.out.println("Footer - Actual Color of Sustainability Text in Home Page is : "	+ SustainabilityFHPColor );
		// Convert RGB to Hex Value
		Color SusfooterHP = Color.fromString(SustainabilityFHPColor);
		String HexSustainabilityFHPColor = SusfooterHP.asHex();
		System.out.println("Footer - Actual Hex Color of Sustainability Text in Home Page is : "	+ HexSustainabilityFHPColor );
		
		String SustainabilityFHPFontSize = SustainabilityFHP.getCssValue("font-size");
		System.out.println("Footer - Actual Font Size of Sustainability Text in Home Page is : "	+ SustainabilityFHPFontSize);
		String SustainabilityFHPFontFamily = SustainabilityFHP.getCssValue("font-family");
		System.out.println("Footer - Actual Font Family of Sustainability Text in Home Page is : "	+ SustainabilityFHPFontFamily);
		
		String ExpSustainabilityFHPColor = sheet1.getCell(1,62).getContents(); //column,row
		System.out.println("Footer - Expected Color of Sustainability Text in Home Page is : "	+ ExpSustainabilityFHPColor );
		String ExpSustainabilityFHPFontSize = sheet1.getCell(2,62).getContents();
		System.out.println("Footer - Expected Font Size of Sustainability Text in Home Page is : "	+ ExpSustainabilityFHPFontSize);
		String ExpSustainabilityFHPFontFamily = sheet1.getCell(3,62).getContents();
		System.out.println("Footer - Expected Font Family of Sustainability Text in Home Page is : "	+ ExpSustainabilityFHPFontFamily);
		
		if(HexSustainabilityFHPColor.equals(ExpSustainabilityFHPColor))
		{
			System.out.println("Footer - Sustainability Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Footer - Sustainability Home Page Color is not in match with creative - Fail");
			bw.write("Footer - Sustainability Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(SustainabilityFHPFontSize.equals(ExpSustainabilityFHPFontSize))
		{
			System.out.println("Footer - Sustainability Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Footer - Sustainability Home Page Font Size is not in match with creative - Fail");
			bw.write("Footer - Sustainability Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(SustainabilityFHPFontFamily.equals(ExpSustainabilityFHPFontFamily))
		{
			System.out.println("Footer - Sustainability Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Footer - Sustainability Home Page Font Family is not in match with creative - Fail");
			bw.write("Footer - Sustainability Home Page Color is Font Family in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexSustainabilityFHPColor.equals(ExpSustainabilityFHPColor) && SustainabilityFHPFontSize.equals(ExpSustainabilityFHPFontSize) && SustainabilityFHPFontFamily.equals(ExpSustainabilityFHPFontFamily))
		{
			Label SustainabilityFHPCSSP = new Label(5,62,"PASS"); 
			wsheet1.addCell(SustainabilityFHPCSSP); 
		}
		else
		{
			Label SustainabilityFHPCSSF = new Label(5,62,"FAIL"); 
			wsheet1.addCell(SustainabilityFHPCSSF); 
		}
		
		// Follow Us Text in Footer
    	
    	WebElement FollowHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[9]/div/div/div[2]/div/div[2]/div[2]/div[1]")); 
    	String FollowHPColor = FollowHP.getCssValue("color");
		System.out.println("Footer - Actual Color of Follow Us Text in Home Page is : "	+ FollowHPColor );
		// Convert RGB to Hex Value
		Color FollowfooterHP = Color.fromString(FollowHPColor);
		String HexFollowHPColor = FollowfooterHP.asHex();
		System.out.println("Footer - Actual Hex Color of Follow Us Text in Home Page is : "	+ HexFollowHPColor );
		
		String FollowHPFontSize = FollowHP.getCssValue("font-size");
		System.out.println("Footer - Actual Font Size of Follow Us Text in Home Page is : "	+ FollowHPFontSize);
		String FollowHPFontFamily = FollowHP.getCssValue("font-family");
		System.out.println("Footer - Actual Font Family of Follow Us Text in Home Page is : "	+ FollowHPFontFamily);
		
		String ExpFollowHPColor = sheet1.getCell(1,63).getContents(); //column,row
		System.out.println("Footer - Expected Color of Follow Us Text in Home Page is : "	+ ExpFollowHPColor );
		String ExpFollowHPFontSize = sheet1.getCell(2,63).getContents();
		System.out.println("Footer - Expected Font Size of Follow Us Text in Home Page is : "	+ ExpFollowHPFontSize);
		String ExpFollowHPFontFamily = sheet1.getCell(3,63).getContents();
		System.out.println("Footer - Expected Font Family of Follow Us Text in Home Page is : "	+ ExpFollowHPFontFamily);
		
		if(HexFollowHPColor.equals(ExpFollowHPColor))
		{
			System.out.println("Footer - Follow Us Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Footer - Follow Us Home Page Color is not in match with creative - Fail");
			bw.write("Footer - Follow Us Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(FollowHPFontSize.equals(ExpFollowHPFontSize))
		{
			System.out.println("Footer - Follow Us Home Page Font Size is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("Footer - Follow Us Home Page Font Size is not in match with creative - Fail");
			bw.write("Footer - Follow Us Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(FollowHPFontFamily.equals(ExpFollowHPFontFamily))
		{
			System.out.println("Footer - Follow Us Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Footer - Follow Us Home Page Font Family is not in match with creative - Fail");
			bw.write("Footer - Follow Us Home Page Color is Font Family in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexFollowHPColor.equals(ExpFollowHPColor) && FollowHPFontSize.equals(ExpFollowHPFontSize) && FollowHPFontFamily.equals(ExpFollowHPFontFamily))
		{
			Label FollowHPCSSP = new Label(5,63,"PASS"); 
			wsheet1.addCell(FollowHPCSSP); 
		}
		else
		{
			Label FollowHPCSSF = new Label(5,63,"FAIL"); 
			wsheet1.addCell(FollowHPCSSF); 
		}
		
		// FaceBook Share Icon in Footer
		
		WebElement ShareFBFHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[9]/div/div/div[2]/div/div[2]/div[2]/div[2]/div[2]")); 
		String ShareFBFHPFontFamily = ShareFBFHP.getCssValue("font-family");
		System.out.println("Footer - Actual Font Family of Share FaceBook Icon in Home Page is : "	+ ShareFBFHPFontFamily);
		String ExpShareFBFHPFontFamily = sheet1.getCell(3,64).getContents();
		System.out.println("Footer - Expected Font Family of Share FaceBook Icon in Home Page is : "	+ ExpShareFBFHPFontFamily);

		if(ShareFBFHPFontFamily.equals(ExpShareFBFHPFontFamily))
		{
			System.out.println("Footer - Share FaceBook Home Page Font Family is in match with creative - Pass");
			Label ShareFBFHPCSSP = new Label(5,64,"PASS"); 
			wsheet1.addCell(ShareFBFHPCSSP);
		}
		else
		{
			System.out.println("Footer - Share FaceBook Home Page Font Family is not in match with creative - Fail");
			bw.write("Footer - Share FaceBook Home Page Color is Font Family in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label ShareFBFHPCSSF = new Label(5,64,"FAIL"); 
			wsheet1.addCell(ShareFBFHPCSSF);
		}
		
		// Twitter Share Icon in Footer
		
		WebElement ShareTwFHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[9]/div/div/div[2]/div/div[2]/div[2]/div[2]/div[3]/div")); 
		String ShareTwFHPFontFamily = ShareTwFHP.getCssValue("font-family");
		System.out.println("Footer - Actual Font Family of Share Twitter Icon in Home Page is : "	+ ShareTwFHPFontFamily);
		String ExpShareTwFHPFontFamily = sheet1.getCell(3,65).getContents();
		System.out.println("Footer - Expected Font Family of Share Twitter Icon in Home Page is : "	+ ExpShareTwFHPFontFamily);

		if(ShareTwFHPFontFamily.equals(ExpShareTwFHPFontFamily))
		{
			System.out.println("Footer - Share Twitter Home Page Font Family is in match with creative - Pass");
			Label ShareTwFHPCSSP = new Label(5,65,"PASS"); 
			wsheet1.addCell(ShareTwFHPCSSP);
		}
		else
		{
			System.out.println("Footer - Share Twitter Home Page Font Family is not in match with creative - Fail");
			bw.write("Footer - Share Twitter Home Page Color is Font Family in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label ShareTwFHPCSSF = new Label(5,65,"FAIL"); 
			wsheet1.addCell(ShareTwFHPCSSF);
		}
				
		// Instagram Share Icon in Footer
				
		WebElement ShareiFHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[9]/div/div/div[2]/div/div[2]/div[2]/div[2]/div[4]/div")); 
		String ShareiFHPFontFamily = ShareiFHP.getCssValue("font-family");
		System.out.println("Footer - Actual Font Family of Share Instagram Icon in Home Page is : "	+ ShareiFHPFontFamily);
		String ExpShareiFHPFontFamily = sheet1.getCell(3,66).getContents();
		System.out.println("Footer - Expected Font Family of Share Instagram Icon in Home Page is : "	+ ExpShareiFHPFontFamily);

		if(ShareiFHPFontFamily.equals(ExpShareiFHPFontFamily))
		{
			System.out.println("Footer - Share Instagram Home Page Font Family is in match with creative - Pass");
			Label ShareiFHPCSSP = new Label(5,66,"PASS"); 
			wsheet1.addCell(ShareiFHPCSSP);
		}
		else
		{
			System.out.println("Footer - Share Instagram Home Page Font Family is not in match with creative - Fail");
			bw.write("Footer - Share Instagram Home Page Color is Font Family in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label ShareiFHPCSSF = new Label(5,66,"FAIL"); 
			wsheet1.addCell(ShareiFHPCSSF);
		}
		
		// Terms of Use in Footer
    	
    	WebElement TermsHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[9]/div/div/div[2]/div/div[3]/div/div[1]/a/div")); 
    	String TermsHPColor = TermsHP.getCssValue("color");
		System.out.println("Footer - Actual Color of Terms of Use in Home Page is : "	+ TermsHPColor );
		// Convert RGB to Hex Value
		Color TermsfooterHP = Color.fromString(TermsHPColor);
		String HexTermsHPColor = TermsfooterHP.asHex();
		System.out.println("Footer - Actual Hex Color of Terms of Use in Home Page is : "	+ HexTermsHPColor );
		
		String TermsHPFontSize = TermsHP.getCssValue("font-size");
		System.out.println("Footer - Actual Font Size of Terms of Use in Home Page is : "	+ TermsHPFontSize);
		String TermsHPFontFamily = TermsHP.getCssValue("font-family");
		System.out.println("Footer - Actual Font Family of Terms of Use in Home Page is : "	+ TermsHPFontFamily);
		
		String ExpTermsHPColor = sheet1.getCell(1,67).getContents(); //column,row
		System.out.println("Footer - Expected Color of Terms of Use in Home Page is : "	+ ExpTermsHPColor );
		String ExpTermsHPFontSize = sheet1.getCell(2,67).getContents();
		System.out.println("Footer - Expected Font Size of Terms of Use in Home Page is : "	+ ExpTermsHPFontSize);
		String ExpTermsHPFontFamily = sheet1.getCell(3,67).getContents();
		System.out.println("Footer - Expected Font Family of Terms of Use in Home Page is : "	+ ExpTermsHPFontFamily);
		
		if(HexTermsHPColor.equals(ExpTermsHPColor))
		{
			System.out.println("Footer - Terms of Use Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Footer - Terms of Use Home Page Color is not in match with creative - Fail");
			bw.write("Footer - Terms of Use Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(TermsHPFontSize.equals(ExpTermsHPFontSize))
		{
			System.out.println("Footer - Terms of Use Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Footer - Terms of Use Home Page Font Size is not in match with creative - Fail");
			bw.write("Footer - Terms of Use Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(TermsHPFontFamily.equals(ExpTermsHPFontFamily))
		{
			System.out.println("Footer - Terms of Use Home Page Font Family is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("Footer - Terms of Use Home Page Font Family is not in match with creative - Fail");
			bw.write("Footer - Terms of Use Home Page Color is Font Family in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexTermsHPColor.equals(ExpTermsHPColor) && TermsHPFontSize.equals(ExpTermsHPFontSize) && TermsHPFontFamily.equals(ExpTermsHPFontFamily))
		{
			Label TermsHPCSSP = new Label(5,67,"PASS"); 
			wsheet1.addCell(TermsHPCSSP); 
		}
		else
		{
			Label TermsHPCSSF = new Label(5,67,"FAIL"); 
			wsheet1.addCell(TermsHPCSSF); 
		}
		
		// Privacy in Footer
    	
    	WebElement PrivacyHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[9]/div/div/div[2]/div/div[3]/div/div[2]/a/div")); 
    	String PrivacyHPColor = PrivacyHP.getCssValue("color");
		System.out.println("Footer - Actual Color of Privacy in Home Page is : "	+ PrivacyHPColor );
		// Convert RGB to Hex Value
		Color PrivacyfooterHP = Color.fromString(PrivacyHPColor);
		String HexPrivacyHPColor = PrivacyfooterHP.asHex();
		System.out.println("Footer - Actual Hex Color of Privacy in Home Page is : "	+ HexPrivacyHPColor );
		
		String PrivacyHPFontSize = PrivacyHP.getCssValue("font-size");
		System.out.println("Footer - Actual Font Size of Privacy in Home Page is : "	+ PrivacyHPFontSize);
		String PrivacyHPFontFamily = PrivacyHP.getCssValue("font-family");
		System.out.println("Footer - Actual Font Family of Privacy in Home Page is : "	+ PrivacyHPFontFamily);
		
		String ExpPrivacyHPColor = sheet1.getCell(1,68).getContents(); //column,row
		System.out.println("Footer - Expected Color of Privacy in Home Page is : "	+ ExpPrivacyHPColor );
		String ExpPrivacyHPFontSize = sheet1.getCell(2,68).getContents();
		System.out.println("Footer - Expected Font Size of Privacy in Home Page is : "	+ ExpPrivacyHPFontSize);
		String ExpPrivacyHPFontFamily = sheet1.getCell(3,68).getContents();
		System.out.println("Footer - Expected Font Family of Privacy in Home Page is : "	+ ExpPrivacyHPFontFamily);
		
		if(HexPrivacyHPColor.equals(ExpPrivacyHPColor))
		{
			System.out.println("Footer - Privacy Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Footer - Privacy Home Page Color is not in match with creative - Fail");
			bw.write("Footer - Privacy Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(PrivacyHPFontSize.equals(ExpPrivacyHPFontSize))
		{
			System.out.println("Footer - Privacy Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Footer - Privacy Home Page Font Size is not in match with creative - Fail");
			bw.write("Footer - Privacy Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(PrivacyHPFontFamily.equals(ExpPrivacyHPFontFamily))
		{
			System.out.println("Footer - Privacy Home Page Font Family is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("Footer - Privacy Home Page Font Family is not in match with creative - Fail");
			bw.write("Footer - Privacy Home Page Color is Font Family in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexPrivacyHPColor.equals(ExpPrivacyHPColor) && PrivacyHPFontSize.equals(ExpPrivacyHPFontSize) && PrivacyHPFontFamily.equals(ExpPrivacyHPFontFamily))
		{
			Label PrivacyHPCSSP = new Label(5,68,"PASS"); 
			wsheet1.addCell(PrivacyHPCSSP); 
		}
		else
		{
			Label PrivacyHPCSSF = new Label(5,68,"FAIL"); 
			wsheet1.addCell(PrivacyHPCSSF); 
		}
		
		// Contact Us in Footer
    	
    	WebElement ContactHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[9]/div/div/div[2]/div/div[3]/div/div[3]/a/div")); 
    	String ContactHPColor = ContactHP.getCssValue("color");
		System.out.println("Footer - Actual Color of Contact Us in Home Page is : "	+ ContactHPColor );
		// Convert RGB to Hex Value
		Color ContactfooterHP = Color.fromString(ContactHPColor);
		String HexContactHPColor = ContactfooterHP.asHex();
		System.out.println("Footer - Actual Hex Color of Contact Us in Home Page is : "	+ HexContactHPColor );
		
		String ContactHPFontSize = ContactHP.getCssValue("font-size");
		System.out.println("Footer - Actual Font Size of Contact Us in Home Page is : "	+ ContactHPFontSize);
		String ContactHPFontFamily = ContactHP.getCssValue("font-family");
		System.out.println("Footer - Actual Font Family of Contact Us in Home Page is : "	+ ContactHPFontFamily);
		
		String ExpContactHPColor = sheet1.getCell(1,69).getContents(); //column,row
		System.out.println("Footer - Expected Color of Contact Us in Home Page is : "	+ ExpContactHPColor );
		String ExpContactHPFontSize = sheet1.getCell(2,69).getContents();
		System.out.println("Footer - Expected Font Size of Contact Us in Home Page is : "	+ ExpContactHPFontSize);
		String ExpContactHPFontFamily = sheet1.getCell(3,69).getContents();
		System.out.println("Footer - Expected Font Family of Contact Us in Home Page is : "	+ ExpContactHPFontFamily);
		
		if(HexContactHPColor.equals(ExpContactHPColor))
		{
			System.out.println("Footer - Contact Us Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Footer - Contact Us Home Page Color is not in match with creative - Fail");
			bw.write("Footer - Contact Us Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(ContactHPFontSize.equals(ExpContactHPFontSize))
		{
			System.out.println("Footer - Contact Us Home Page Font Size is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("Footer - Contact Us Home Page Font Size is not in match with creative - Fail");
			bw.write("Footer - Contact Us Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(ContactHPFontFamily.equals(ExpContactHPFontFamily))
		{
			System.out.println("Footer - Contact Us Home Page Font Family is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("Footer - Contact Us Home Page Font Family is not in match with creative - Fail");
			bw.write("Footer - Contact Us Home Page Color is Font Family in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexContactHPColor.equals(ExpContactHPColor) && ContactHPFontSize.equals(ExpContactHPFontSize) && ContactHPFontFamily.equals(ExpContactHPFontFamily))
		{
			Label ContactHPCSSP = new Label(5,69,"PASS"); 
			wsheet1.addCell(ContactHPCSSP); 
		}
		else
		{
			Label ContactHPCSSF = new Label(5,69,"FAIL"); 
			wsheet1.addCell(ContactHPCSSF); 
		}
		
		
		// CA Transparency Act in Footer
    	
    	WebElement CAHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[9]/div/div/div[2]/div/div[3]/div/div[4]/a/div")); 
    	String CAHPColor = CAHP.getCssValue("color");
		System.out.println("Footer - Actual Color of CA Transparency Act in Home Page is : "	+ CAHPColor );
		// Convert RGB to Hex Value
		Color CAfooterHP = Color.fromString(CAHPColor);
		String HexCAHPColor = CAfooterHP.asHex();
		System.out.println("Footer - Actual Hex Color of CA Transparency Act in Home Page is : "	+ HexCAHPColor );
		
		String CAHPFontSize = CAHP.getCssValue("font-size");
		System.out.println("Footer - Actual Font Size of CA Transparency Act in Home Page is : "	+ CAHPFontSize);
		String CAHPFontFamily = CAHP.getCssValue("font-family");
		System.out.println("Footer - Actual Font Family of CA Transparency Act in Home Page is : "	+ CAHPFontFamily);
		
		String ExpCAHPColor = sheet1.getCell(1,70).getContents(); //column,row
		System.out.println("Footer - Expected Color of CA Transparency Act in Home Page is : "	+ ExpCAHPColor );
		String ExpCAHPFontSize = sheet1.getCell(2,70).getContents();
		System.out.println("Footer - Expected Font Size of CA Transparency Act in Home Page is : "	+ ExpCAHPFontSize);
		String ExpCAHPFontFamily = sheet1.getCell(3,70).getContents();
		System.out.println("Footer - Expected Font Family of CA Transparency Act in Home Page is : "	+ ExpCAHPFontFamily);
		
		if(HexCAHPColor.equals(ExpCAHPColor))
		{
			System.out.println("Footer - CA Transparency Act Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Footer - CA Transparency Act Home Page Color is not in match with creative - Fail");
			bw.write("Footer - CA Transparency Act Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(CAHPFontSize.equals(ExpCAHPFontSize))
		{
			System.out.println("Footer - CA Transparency Act Home Page Font Size is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("Footer - CA Transparency Act Home Page Font Size is not in match with creative - Fail");
			bw.write("Footer - CA Transparency Act Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(CAHPFontFamily.equals(ExpCAHPFontFamily))
		{
			System.out.println("Footer - CA Transparency Act Home Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Footer - CA Transparency Act Home Page Font Family is not in match with creative - Fail");
			bw.write("Footer - CA Transparency Act Home Page Color is Font Family in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexCAHPColor.equals(ExpCAHPColor) && CAHPFontSize.equals(ExpCAHPFontSize) && CAHPFontFamily.equals(ExpCAHPFontFamily))
		{
			Label CAHPCSSP = new Label(5,70,"PASS"); 
			wsheet1.addCell(CAHPCSSP); 
		}
		else
		{
			Label CAHPCSSF = new Label(5,70,"FAIL"); 
			wsheet1.addCell(CAHPCSSF); 
		}
		
		// Copyright Text in Footer
    	
    	WebElement CopyHP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[9]/div/div/div[2]/div/div[2]/div[3]/div")); 
    	String CopyHPColor = CopyHP.getCssValue("color");
		System.out.println("Footer - Actual Color of Copyright Text in Home Page is : "	+ CopyHPColor );
		// Convert RGB to Hex Value
		Color CopyfooterHP = Color.fromString(CopyHPColor);
		String HexCopyHPColor = CopyfooterHP.asHex();
		System.out.println("Footer - Actual Hex Color of Copyright Text in Home Page is : "	+ HexCopyHPColor );
		
		String CopyHPFontSize = CopyHP.getCssValue("font-size");
		System.out.println("Footer - Actual Font Size of Copyright Text in Home Page is : "	+ CopyHPFontSize);
		String CopyHPFontFamily = CopyHP.getCssValue("font-family");
		System.out.println("Footer - Actual Font Family of Copyright Text in Home Page is : "	+ CopyHPFontFamily);
		
		String ExpCopyHPColor = sheet1.getCell(1,71).getContents(); //column,row
		System.out.println("Footer - Expected Color of Copyright Text in Home Page is : "	+ ExpCopyHPColor );
		String ExpCopyHPFontSize = sheet1.getCell(2,71).getContents();
		System.out.println("Footer - Expected Font Size of Copyright Text in Home Page is : "	+ ExpCopyHPFontSize);
		String ExpCopyHPFontFamily = sheet1.getCell(3,71).getContents();
		System.out.println("Footer - Expected Font Family of Copyright Text in Home Page is : "	+ ExpCopyHPFontFamily);
		
		if(HexCopyHPColor.equals(ExpCopyHPColor))
		{
			System.out.println("Footer - Copyright Home Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Footer - Copyright Home Page Color is not in match with creative - Fail");
			bw.write("Footer - Copyright Home Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(CopyHPFontSize.equals(ExpCopyHPFontSize))
		{
			System.out.println("Footer - Copyright Home Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Footer - Copyright Home Page Font Size is not in match with creative - Fail");
			bw.write("Footer - Copyright Home Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(CopyHPFontFamily.equals(ExpCopyHPFontFamily))
		{
			System.out.println("Footer - Copyright Home Page Font Family is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("Footer - Copyright Home Page Font Family is not in match with creative - Fail");
			bw.write("Footer - Copyright Home Page Color is Font Family in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexCopyHPColor.equals(ExpCopyHPColor) && CopyHPFontSize.equals(ExpCopyHPFontSize) && CopyHPFontFamily.equals(ExpCopyHPFontFamily))
		{
			Label CopyHPCSSP = new Label(5,71,"PASS"); 
			wsheet1.addCell(CopyHPCSSP); 
		}
		else
		{
			Label CopyHPCSSF = new Label(5,71,"FAIL"); 
			wsheet1.addCell(CopyHPCSSF); 
		}
		
		bw.close();
		
    	}
    	catch(Exception e)
		{
			System.out.println("Exception Is "+e);
			 
		}
		
    }
    
    @Test(priority = 2)
	
   	public static void QualityPageCSS() throws InterruptedException, BiffException, IOException, RowsExceededException, WriteException {	
    	
    	try{
         	
       	book = Workbook.getWorkbook(newFile);
   		sheet3=book.getSheet("QualityPageCSS");
   		
   		//workbook = Workbook.getWorkbook(new File("D:\\Viyantha\\Automation\\Selenium Test Report\\KEURIG\\Keurig_FileInput.xls"));
		//workbookcopy = Workbook.createWorkbook(new File("D:\\Viyantha\\Automation\\Selenium Test Report\\KEURIG\\Keurig_FileOutput_" + idForTxtFile + ".xls"), workbook);
		
		wsheet3 = workbookcopy.getSheet(2);
   		
   		// Write text on  text file
   		
   		FileWriter fw = new FileWriter(file, true);  
   		BufferedWriter bw = new BufferedWriter(fw);
   		
   		// Quality Link in Home Page Footer
   		
   		driver.findElement(By.xpath(".//*[@id='id_skgm_fw_footerLayoutCell']/div/div[2]/div[1]/div/a[2]/div")).click();  
		Thread.sleep(5000);
	
		// Quality Main Banner Title
   		
   		WebElement QualityTitleQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[2]/div/div/div/div/div/div[1]/div[1]")); 
		String QualityTitleQPColor = QualityTitleQP.getCssValue("color");
		System.out.println("Actual Color of Quality Text in Main Banner Quality Page is : "	+ QualityTitleQP );
		// Convert RGB to Hex Value
		Color HQualityTitleQPColor = Color.fromString(QualityTitleQPColor);
		String HexQualityTitleQPColor = HQualityTitleQPColor.asHex();
		System.out.println("Actual Hex Color of Quality Text in Main Banner Quality Page is : "	+ HexQualityTitleQPColor );
		
		String QualityTitleQPFontSize = QualityTitleQP.getCssValue("font-size");
		System.out.println("Actual Font Size of Quality Text in Main Banner Quality Page is : "	+ QualityTitleQPFontSize);
		String QualityTitleQPFontFamily = QualityTitleQP.getCssValue("font-family");
		System.out.println("Actual Font Family of Quality Text in Main Banner Quality Page is : "	+ QualityTitleQPFontFamily);
		String QualityTitleQPContent = QualityTitleQP.getText();
		System.out.println("Actual Content of Quality Text in Main Banner Quality Page is : "	+ QualityTitleQPContent);
				
		String ExpQualityTitleQPColor = sheet3.getCell(1,1).getContents(); //column,row
		System.out.println("Expected Color of Quality Text in Main Banner Quality Page is : "	+ ExpQualityTitleQPColor );
		String ExpQualityTitleQPFontSize = sheet3.getCell(2,1).getContents();
		System.out.println("Expected Font Size of Quality Text in Main Banner Quality Page is : "	+ ExpQualityTitleQPFontSize);
		String ExpQualityTitleQPFontFamily = sheet3.getCell(3,1).getContents();
		System.out.println("Expected Font Family of Quality Text in Main Banner Quality Page is : "	+ ExpQualityTitleQPFontFamily);
		String ExpQualityTitleQPContent = sheet3.getCell(4,1).getContents();
		System.out.println("Expected Content of Quality Text in Main Banner Quality Page is : "	+ ExpQualityTitleQPContent);
				
		if(HexQualityTitleQPColor.equals(ExpQualityTitleQPColor))
		{
			System.out.println("Quality Text in Main Banner Quality Page Color is in match with creative - Pass");
		}
		else
		{
			System.out.println("Quality Text in Main Banner Quality Page Color is not in match with creative - Fail");
			bw.write("Quality Text in Main Banner Quality Page Color is not in match with creative - Fail"); 
		    bw.newLine();
		}
				
		if(QualityTitleQPFontSize.equals(ExpQualityTitleQPFontSize))
		{
			System.out.println("Quality Text in Main Banner Quality Page Font Size is in match with creative - Pass");
		}
		else
		{
			System.out.println("Quality Text in Main Banner Quality Page Font Size is not in match with creative - Fail");
			bw.write("Quality Text in Main Banner Quality Page Font Size is not in match with creative - Fail");  
		    bw.newLine();
		}
				
		if(QualityTitleQPFontFamily.equals(ExpQualityTitleQPFontFamily))
		{
			System.out.println("Quality Text in Main Banner Quality Page Font Family is in match with creative - Pass");
		}
		else
		{
			System.out.println("Quality Text in Main Banner Quality Page Font Family is not in match with creative - Fail");
			bw.write("Quality Text in Main Banner Quality Page Font Family is not in match with creative - Fail"); 
		    bw.newLine();
		}
		

		if(QualityTitleQPContent.equals(ExpQualityTitleQPContent))
		{
			System.out.println("Quality Text in Main Banner Quality Page is in match with creative - Pass");
		}
		else
		{
			System.out.println("Quality Content in Main Banner Quality Page is not in match with creative - Fail");
			bw.write("Quality Content in Main Banner Quality Page is not in match with creative - Fail"); 
		    bw.newLine();
		}
		
		if(HexQualityTitleQPColor.equals(ExpQualityTitleQPColor) && QualityTitleQPFontSize.equals(ExpQualityTitleQPFontSize) && QualityTitleQPFontFamily.equals(ExpQualityTitleQPFontFamily) && QualityTitleQPContent.equals(ExpQualityTitleQPContent))
		{
			Label QualityTitleQPCSSP = new Label(5,1,"PASS"); 
			wsheet3.addCell(QualityTitleQPCSSP); 
		}
		else
		{
			Label QualityTitleQPCSSF = new Label(5,1,"FAIL"); 
			wsheet3.addCell(QualityTitleQPCSSF); 
		}
   		
   		// Quality Main Banner Desc
   		
   		WebElement QualityDescQP = driver.findElement(By.className("sk_pageImage_Description")); 
		String QualityDescQPColor = QualityDescQP.getCssValue("color");
		System.out.println("Actual Color of Quality Desc in Main Banner Quality Page is : "	+ QualityDescQPColor );
		// Convert RGB to Hex Value
		Color HQualityDescQPColor = Color.fromString(QualityDescQPColor);
		String HexQualityDescQPColor = HQualityDescQPColor.asHex();
		System.out.println("Actual Hex Color of Quality Desc in Main Banner Quality Page is : "	+ HexQualityDescQPColor );
		
		String QualityDescQPFontSize = QualityDescQP.getCssValue("font-size");
		System.out.println("Actual Font Size of Quality Desc in Main Banner Quality Page is : "	+ QualityDescQPFontSize);
		String QualityDescQPFontFamily = QualityDescQP.getCssValue("font-family");
		System.out.println("Actual Font Family of Quality Desc in Main Banner Quality Page is : "	+ QualityDescQPFontFamily);
		String QualityDescQPContent = QualityDescQP.getText();
		System.out.println("Actual Content of Quality Desc in Main Banner Quality Page is : "	+ QualityDescQPContent);
				
		String ExpQualityDescQPColor = sheet3.getCell(1,2).getContents(); //column,row
		System.out.println("Expected Color of Quality Desc in Main Banner Quality Page is : "	+ ExpQualityDescQPColor );
		String ExpQualityDescQPFontSize = sheet3.getCell(2,2).getContents();
		System.out.println("Expected Font Size of Quality Desc in Main Banner Quality Page is : "	+ ExpQualityDescQPFontSize);
		String ExpQualityDescQPFontFamily = sheet3.getCell(3,2).getContents();
		System.out.println("Expected Font Family of Quality Desc in Main Banner Quality Page is : "	+ ExpQualityDescQPFontFamily);
		String ExpQualityDescQPContent = sheet3.getCell(4,2).getContents();
		System.out.println("Expected Content of Quality Desc in Main Banner Quality Page is : "	+ ExpQualityDescQPContent);
				
		if(HexQualityDescQPColor.equals(ExpQualityDescQPColor))
		{
			System.out.println("Quality Desc in Main Banner Quality Page Color is in match with creative - Pass");
		}
		else
		{
			System.out.println("Quality Desc in Main Banner Quality Page Color is not in match with creative - Fail");
			bw.write("Quality Desc in Main Banner Quality Page Color is not in match with creative - Fail"); 
		    bw.newLine();
		}
				
		if(QualityDescQPFontSize.equals(ExpQualityDescQPFontSize))
		{
			System.out.println("Quality Desc in Main Banner Quality Page Font Size is in match with creative - Pass");
		}
		else
		{
			System.out.println("Quality Desc in Main Banner Quality Page Font Size is not in match with creative - Fail");
			bw.write("Quality Desc in Main Banner Quality Page Font Size is not in match with creative - Fail");  
		    bw.newLine();
		}
				
		if(QualityDescQPFontFamily.equals(ExpQualityDescQPFontFamily))
		{
			System.out.println("Quality Desc in Main Banner Quality Page Font Family is in match with creative - Pass");
		}
		else
		{
			System.out.println("Quality Desc in Main Banner Quality Page Font Family is not in match with creative - Fail");
			bw.write("Quality Desc in Main Banner Quality Page Font Family is not in match with creative - Fail"); 
		    bw.newLine();
		}
		

		if(QualityDescQPContent.equals(ExpQualityDescQPContent))
		{
			System.out.println("Quality Desc Content in Main Banner Quality Page is in match with creative - Pass");
		}
		else
		{
			System.out.println("Quality Desc Content in Main Banner Quality Page is not in match with creative - Fail");
			bw.write("Quality Desc Content in Main Banner Quality Page is not in match with creative - Fail"); 
		    bw.newLine();
		}
		
		if(HexQualityDescQPColor.equals(ExpQualityDescQPColor) && QualityDescQPFontSize.equals(ExpQualityDescQPFontSize) && QualityDescQPFontFamily.equals(ExpQualityDescQPFontFamily) && QualityDescQPContent.equals(ExpQualityDescQPContent))
		{
			Label QualityDescQPCSSP = new Label(5,2,"PASS"); 
			wsheet3.addCell(QualityDescQPCSSP); 
		}
		else
		{
			Label QualityDescQPCSSF = new Label(5,2,"FAIL"); 
			wsheet3.addCell(QualityDescQPCSSF); 
		}
		
		// Quality Content Title - EXCEPTIONAL QUALITY, FROM SOURCE TO CUP 
   		
   		WebElement QualityContentTitleQP = driver.findElement(By.className("sk_quality_contentTitle")); 
		String QualityContentTitleQPColor = QualityContentTitleQP.getCssValue("color");
		System.out.println("Actual Color of Quality Content Text in Quality Page is : "	+ QualityContentTitleQPColor );
		// Convert RGB to Hex Value
		Color HQualityContentTitleQPColor = Color.fromString(QualityContentTitleQPColor);
		String HexQualityContentTitleQPColor = HQualityContentTitleQPColor.asHex();
		System.out.println("Actual Hex Color of Quality Content Text in Quality Page is : "	+ HexQualityContentTitleQPColor );
		
		String QualityContentTitleQPFontSize = QualityContentTitleQP.getCssValue("font-size");
		System.out.println("Actual Font Size of Quality Content Text in Quality Page is : "	+ QualityContentTitleQPFontSize);
		String QualityContentTitleQPFontFamily = QualityContentTitleQP.getCssValue("font-family");
		System.out.println("Actual Font Family of Quality Content Text in Quality Page is : "	+ QualityContentTitleQPFontFamily);
		String QualityContentTitleQPContent = QualityContentTitleQP.getText();
		System.out.println("Actual Content of Quality Content Text in Quality Page is : "	+ QualityContentTitleQPContent);
				
		String ExpQualityContentTitleQPColor = sheet3.getCell(1,3).getContents(); //column,row
		System.out.println("Expected Color of Quality Content Text in Quality Page is : "	+ ExpQualityContentTitleQPColor );
		String ExpQualityContentTitleQPFontSize = sheet3.getCell(2,3).getContents();
		System.out.println("Expected Font Size of Quality Content Text in Quality Page is : "	+ ExpQualityContentTitleQPFontSize);
		String ExpQualityContentTitleQPFontFamily = sheet3.getCell(3,3).getContents();
		System.out.println("Expected Font Family of Quality Content Text in Quality Page is : "	+ ExpQualityContentTitleQPFontFamily);
		String ExpQualityContentTitleQPContent = sheet3.getCell(4,3).getContents();
		System.out.println("Expected Content of Quality Content Text in Quality Page is : "	+ ExpQualityContentTitleQPContent);
				
		if(HexQualityContentTitleQPColor.equals(ExpQualityContentTitleQPColor))
		{
			System.out.println("Quality Content Text in Quality Page Color is in match with creative - Pass");
		}
		else
		{
			System.out.println("Quality Content Text in Quality Page Color is not in match with creative - Fail");
			bw.write("Quality Content Text in Quality Page Color is not in match with creative - Fail"); 
		    bw.newLine();
		}
				
		if(QualityContentTitleQPFontSize.equals(ExpQualityContentTitleQPFontSize))
		{
			System.out.println("Quality Content Text in Quality Page Font Size is in match with creative - Pass");
		}
		else
		{
			System.out.println("Quality Content Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("Quality Content Text in Quality Page Font Size is not in match with creative - Fail"); 
		    bw.newLine();
		}
				
		if(QualityContentTitleQPFontFamily.equals(ExpQualityContentTitleQPFontFamily))
		{
			System.out.println("Quality Content Text in Quality Page Font Family is in match with creative - Pass");
		}
		else
		{
			System.out.println("Quality Content Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("Quality Content Text in Quality Page Font Family is not in match with creative - Fail"); 
		    bw.newLine();
		}
		

		if(QualityContentTitleQPContent.equals(ExpQualityContentTitleQPContent))
		{
			System.out.println("Quality Content Text in Quality Page is in match with creative - Pass");
		}
		else
		{
			System.out.println("Quality Content Text in Quality Page is not in match with creative - Fail");
			bw.write("Quality Content Text in Quality Page is not in match with creative - Fail"); 
		    bw.newLine();
		}
		
		if(HexQualityContentTitleQPColor.equals(ExpQualityContentTitleQPColor) && QualityContentTitleQPFontSize.equals(ExpQualityContentTitleQPFontSize) && QualityContentTitleQPFontFamily.equals(ExpQualityContentTitleQPFontFamily) && QualityContentTitleQPContent.equals(ExpQualityContentTitleQPContent))
		{
			Label QualityContentTitleQPCSSP = new Label(5,3,"PASS"); 
			wsheet3.addCell(QualityContentTitleQPCSSP); 
		}
		else
		{
			Label QualityContentTitleQPCSSF = new Label(5,3,"FAIL"); 
			wsheet3.addCell(QualityContentTitleQPCSSF); 
		}
   		
   		// Quality Content Desc - EXCEPTIONAL QUALITY, FROM SOURCE TO CUP (Desc)
   		
   		WebElement QualityContentDescQP = driver.findElement(By.className("sk_quality_content")); 
		String QualityContentDescQPColor = QualityContentDescQP.getCssValue("color");
		System.out.println("Actual Color of Quality Content Desc in Quality Page is : "	+ QualityContentDescQPColor );
		// Convert RGB to Hex Value
		Color HQualityContentDescQPColor = Color.fromString(QualityContentDescQPColor);
		String HexQualityContentDescQPColor = HQualityContentDescQPColor.asHex();
		System.out.println("Actual Hex Color of Quality Content Desc in Quality Page is : "	+ HexQualityContentDescQPColor );
		
		String QualityContentDescQPFontSize = QualityContentDescQP.getCssValue("font-size");
		System.out.println("Actual Font Size of Quality Content Desc in Quality Page is : "	+ QualityContentDescQPFontSize);
		String QualityContentDescQPFontFamily = QualityContentDescQP.getCssValue("font-family");
		System.out.println("Actual Font Family of Quality Content Desc in Quality Page is : "	+ QualityContentDescQPFontFamily);
		String QualityContentDescQPContent = QualityContentDescQP.getText();
		System.out.println("Actual Content of Quality Content Desc in Quality Page is : "	+ QualityContentDescQPContent);
				
		String ExpQualityContentDescQPColor = sheet3.getCell(1,4).getContents(); //column,row
		System.out.println("Expected Color of Quality Content Desc in Quality Page is : "	+ ExpQualityContentDescQPColor );
		String ExpQualityContentDescQPFontSize = sheet3.getCell(2,4).getContents();
		System.out.println("Expected Font Size of Quality Content Desc in Quality Page is : "	+ ExpQualityContentDescQPFontSize);
		String ExpQualityContentDescQPFontFamily = sheet3.getCell(3,4).getContents();
		System.out.println("Expected Font Family of Quality Content Desc in Quality Page is : "	+ ExpQualityContentDescQPFontFamily);
		String ExpQualityContentDescQPContent = sheet3.getCell(4,4).getContents();
		System.out.println("Expected Content of Quality Content Desc in Quality Page is : "	+ ExpQualityContentDescQPContent);
				
		if(HexQualityContentDescQPColor.equals(ExpQualityContentDescQPColor))
		{
			System.out.println("Quality Content Desc in Quality Page Color is in match with creative - Pass");
		}
		else
		{
			System.out.println("Quality Content Desc in Quality Page Color is not in match with creative - Fail");
			bw.write("Quality Content Desc in Quality Page Color is not in match with creative - Fail"); 
		    bw.newLine();
		}
				
		if(QualityContentDescQPFontSize.equals(ExpQualityContentDescQPFontSize))
		{
			System.out.println("Quality Content Desc in Quality Page Font Size is in match with creative - Pass");
		}
		else
		{
			System.out.println("Quality Content Desc in Quality Page Font Size is not in match with creative - Fail");
			bw.write("Quality Content Desc in Quality Page Font Size is not in match with creative - Fail"); 
		    bw.newLine();
		}
				
		if(QualityContentDescQPFontFamily.equals(ExpQualityContentDescQPFontFamily))
		{
			System.out.println("Quality Content Desc in Quality Page Font Family is in match with creative - Pass");
		}
		else
		{
			System.out.println("Quality Content Desc in Quality Page Font Family is not in match with creative - Fail");
			bw.write("Quality Content Desc in Quality Page Font Family is not in match with creative - Fail"); 
		    bw.newLine();
		}
		

		if(QualityContentDescQPContent.equals(ExpQualityContentDescQPContent))
		{
			System.out.println("Quality Content Desc in Quality Page is in match with creative - Pass");
		}
		else
		{
			System.out.println("Quality Content Desc in Quality Page is not in match with creative - Fail");
			bw.write("Quality Content Desc in Quality Page is not in match with creative - Fail"); 
		    bw.newLine();
		}
		
		if(HexQualityContentDescQPColor.equals(ExpQualityContentDescQPColor) && QualityContentDescQPFontSize.equals(ExpQualityContentDescQPFontSize) && QualityContentDescQPFontFamily.equals(ExpQualityContentDescQPFontFamily) && QualityContentDescQPContent.equals(ExpQualityContentDescQPContent))
		{
			Label QualityContentDescQPCSSP = new Label(5,4,"PASS"); 
			wsheet3.addCell(QualityContentDescQPCSSP); 
		}
		else
		{
			Label QualityContentDescQPCSSF = new Label(5,4,"FAIL"); 
			wsheet3.addCell(QualityContentDescQPCSSF); 
		}
		
		// Quality Page HTML Text
		
		WebElement QualityHTMLQP = driver.findElement(By.className("sk_qualityPage_htmlText")); 
		String QualityHTMLQPColor = QualityHTMLQP.getCssValue("color");
		System.out.println("Actual Color of HTML Text in Quality Page is : "	+ QualityHTMLQPColor );
		// Convert RGB to Hex Value
		Color HQualityHTMLQPColor = Color.fromString(QualityHTMLQPColor);
		String HexQualityHTMLQPColor = HQualityHTMLQPColor.asHex();
		System.out.println("Actual Hex Color of HTML Text in Quality Page is : "	+ HexQualityHTMLQPColor );
		
		String QualityHTMLQPFontSize = QualityHTMLQP.getCssValue("font-size");
		System.out.println("Actual Font Size of HTML Text in Quality Page is : "	+ QualityHTMLQPFontSize);
		String QualityHTMLQPFontFamily = QualityHTMLQP.getCssValue("font-family");
		System.out.println("Actual Font Family of HTML Text in Quality Page is : "	+ QualityHTMLQPFontFamily);
		String QualityHTMLQPContent = QualityHTMLQP.getText();
		System.out.println("Actual Content of HTML Text in Quality Page is : "	+ QualityHTMLQPContent);
				
		String ExpQualityHTMLQPColor = sheet3.getCell(1,5).getContents(); //column,row
		System.out.println("Expected Color of HTML Text in Quality Page is : "	+ ExpQualityHTMLQPColor );
		String ExpQualityHTMLQPFontSize = sheet3.getCell(2,5).getContents();
		System.out.println("Expected Font Size of HTML Text in Quality Page is : "	+ ExpQualityHTMLQPFontSize);
		String ExpQualityHTMLQPFontFamily = sheet3.getCell(3,5).getContents();
		System.out.println("Expected Font Family of HTML Text in Quality Page is : "	+ ExpQualityHTMLQPFontFamily);
		String ExpQualityHTMLQPContent = sheet3.getCell(4,5).getContents();
		System.out.println("Expected Content of HTML Text in Quality Page is : "	+ ExpQualityHTMLQPContent);
				
		if(HexQualityHTMLQPColor.equals(ExpQualityHTMLQPColor))
		{
			System.out.println("HTML Text in Quality Page Color is in match with creative - Pass");
		}
		else
		{
			System.out.println("HTML Text in Quality Page Color is not in match with creative - Fail");
			bw.write("HTML Text in Quality Page Color is not in match with creative - Fail"); 
		    bw.newLine();
		}
				
		if(QualityHTMLQPFontSize.equals(ExpQualityHTMLQPFontSize))
		{
			System.out.println("HTML Text in Quality Page Font Size is in match with creative - Pass");
		}
		else
		{
			System.out.println("HTML Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("HTML Text in Quality Page Font Size is not in match with creative - Fail"); 
		    bw.newLine();
		}
				
		if(QualityHTMLQPFontFamily.equals(ExpQualityHTMLQPFontFamily))
		{
			System.out.println("HTML Text in Quality Page Font Family is in match with creative - Pass");
		}
		else
		{
			System.out.println("HTML Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("HTML Text in Quality Page Font Family is not in match with creative - Fail"); 
		    bw.newLine();
		}
		

		if(QualityHTMLQPContent.equals(ExpQualityHTMLQPContent))
		{
			System.out.println("HTML Text Content in Quality Page is in match with creative - Pass");
		}
		else
		{
			System.out.println("HTML Text Content in Quality Page is not in match with creative - Fail");
			bw.write("HTML Text Content in Quality Page is not in match with creative - Fail"); 
		    bw.newLine();
		}
		
		if(HexQualityHTMLQPColor.equals(ExpQualityHTMLQPColor) && QualityHTMLQPFontSize.equals(ExpQualityHTMLQPFontSize) && QualityHTMLQPFontFamily.equals(ExpQualityHTMLQPFontFamily) && QualityHTMLQPContent.equals(ExpQualityHTMLQPContent))
		{
			Label QualityHTMLQPCSSP = new Label(5,5,"PASS"); 
			wsheet3.addCell(QualityHTMLQPCSSP); 
		}
		else
		{
			Label QualityHTMLQPCSSF = new Label(5,5,"FAIL"); 
			wsheet3.addCell(QualityHTMLQPCSSF); 
		}
		
		// Quality Page - Finding
		
		WebElement QualityFindingQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[6]/div/div/div/div/div[2]/div/div[1]")); 
		
		String QualityFindingQPFontSize = QualityFindingQP.getCssValue("font-size");
		System.out.println("Actual Font Size of Finding Text in Quality Page is : "	+ QualityFindingQPFontSize);
		String QualityFindingQPFontFamily = QualityFindingQP.getCssValue("font-family");
		System.out.println("Actual Font Family of Finding Text in Quality Page is : "	+ QualityFindingQPFontFamily);
		String QualityFindingQPContent = QualityFindingQP.getText();
		System.out.println("Actual Content of Finding Text in Quality Page is : "	+ QualityFindingQPContent);
				
		String ExpQualityFindingQPFontSize = sheet3.getCell(2,6).getContents();
		System.out.println("Expected Font Size of Finding Text in Quality Page is : "	+ ExpQualityFindingQPFontSize);
		String ExpQualityFindingQPFontFamily = sheet3.getCell(3,6).getContents();
		System.out.println("Expected Font Family of Finding Text in Quality Page is : "	+ ExpQualityFindingQPFontFamily);
		String ExpQualityFindingQPContent = sheet3.getCell(4,6).getContents();
		System.out.println("Expected Content of Finding Text in Quality Page is : "	+ ExpQualityFindingQPContent);
				
				
		if(QualityFindingQPFontSize.equals(ExpQualityFindingQPFontSize))
		{
			System.out.println("Finding Text in Quality Page Font Size is in match with creative - Pass");
		}
		else
		{
			System.out.println("Finding Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("Finding Text in Quality Page Font Size is not in match with creative - Fail"); 
		    bw.newLine();
		}
				
		if(QualityFindingQPFontFamily.equals(ExpQualityFindingQPFontFamily))
		{
			System.out.println("Finding Text in Quality Page Font Family is in match with creative - Pass");
		}
		else
		{
			System.out.println("Finding Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("Finding Text in Quality Page Font Family is not in match with creative - Fail"); 
		    bw.newLine();
		}
		

		if(QualityFindingQPContent.equals(ExpQualityFindingQPContent))
		{
			System.out.println("Finding Text Content in Quality Page is in match with creative - Pass");
		}
		else
		{
			System.out.println("Finding Text Content in Quality Page is not in match with creative - Fail");
			bw.write("Finding Text Content in Quality Page is not in match with creative - Fail"); 
		    bw.newLine();
		}
		
		if(QualityFindingQPFontSize.equals(ExpQualityFindingQPFontSize) && QualityFindingQPFontFamily.equals(ExpQualityFindingQPFontFamily) && QualityFindingQPContent.equals(ExpQualityFindingQPContent))
		{
			Label QualityFindingQPCSSP = new Label(5,6,"PASS"); 
			wsheet3.addCell(QualityFindingQPCSSP); 
		}
		else
		{
			Label QualityFindingQPCSSF = new Label(5,6,"FAIL"); 
			wsheet3.addCell(QualityFindingQPCSSF); 
		}
		
		// Quality Page - The Farms
		
		WebElement QualityFarmsQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[6]/div/div/div/div/div[2]/div/div[2]")); 
		
		String QualityFarmsQPFontSize = QualityFarmsQP.getCssValue("font-size");
		System.out.println("Actual Font Size of The Farms Text in Quality Page is : "	+ QualityFarmsQPFontSize);
		String QualityFarmsQPFontFamily = QualityFarmsQP.getCssValue("font-family");
		System.out.println("Actual Font Family of The Farms Text in Quality Page is : "	+ QualityFarmsQPFontFamily);
		String QualityFarmsQPContent = QualityFarmsQP.getText();
		System.out.println("Actual Content of The Farms Text in Quality Page is : "	+ QualityFarmsQPContent);
				
		String ExpQualityFarmsQPFontSize = sheet3.getCell(2,7).getContents();
		System.out.println("Expected Font Size of The Farms Text in Quality Page is : "	+ ExpQualityFarmsQPFontSize);
		String ExpQualityFarmsQPFontFamily = sheet3.getCell(3,7).getContents();
		System.out.println("Expected Font Family of The Farms Text in Quality Page is : "	+ ExpQualityFarmsQPFontFamily);
		String ExpQualityFarmsQPContent = sheet3.getCell(4,7).getContents();
		System.out.println("Expected Content of The Farms Text in Quality Page is : "	+ ExpQualityFarmsQPContent);
						
		if(QualityFarmsQPFontSize.equals(ExpQualityFarmsQPFontSize))
		{
			System.out.println("The Farms Text in Quality Page Font Size is in match with creative - Pass");
		}
		else
		{
			System.out.println("The Farms Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("The Farms Text in Quality Page Font Size is not in match with creative - Fail"); 
		    bw.newLine();
		}
				
		if(QualityFarmsQPFontFamily.equals(ExpQualityFarmsQPFontFamily))
		{
			System.out.println("The Farms Text in Quality Page Font Family is in match with creative - Pass");
		}
		else
		{
			System.out.println("The Farms Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("The Farms Text in Quality Page Font Family is not in match with creative - Fail");  
		    bw.newLine();
		}
		

		if(QualityFarmsQPContent.equals(ExpQualityFarmsQPContent))
		{
			System.out.println("The Farms Text Content in Quality Page is in match with creative - Pass");
		}
		else
		{
			System.out.println("The Farms Text Content in Quality Page is not in match with creative - Fail");
			bw.write("The Farms Text Content in Quality Page is not in match with creative - Fail"); 
		    bw.newLine();
		}
		
		if(QualityFarmsQPFontSize.equals(ExpQualityFarmsQPFontSize) && QualityFarmsQPFontFamily.equals(ExpQualityFarmsQPFontFamily) && QualityFarmsQPContent.equals(ExpQualityFarmsQPContent))
		{
			Label QualityFarmsQPCSSP = new Label(5,7,"PASS"); 
			wsheet3.addCell(QualityFarmsQPCSSP); 
		}
		else
		{
			Label QualityFarmsQPCSSF = new Label(5,7,"FAIL"); 
			wsheet3.addCell(QualityFarmsQPCSSF); 
		}
		
		// Quality Page - Finding The Farms Desc
		
		WebElement QualityFarmsDescQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[6]/div/div/div/div/div[2]/div/div[4]")); 
		
		String QualityFarmsDescQPFontSize = QualityFarmsDescQP.getCssValue("font-size");
		System.out.println("Actual Font Size of The Farms Desc in Quality Page is : "	+ QualityFarmsDescQPFontSize);
		String QualityFarmsDescQPFontFamily = QualityFarmsDescQP.getCssValue("font-family");
		System.out.println("Actual Font Family of The Farms Desc in Quality Page is : "	+ QualityFarmsDescQPFontFamily);
		String QualityFarmsDescQPContent = QualityFarmsDescQP.getText();
		System.out.println("Actual Content of The Farms Desc in Quality Page is : "	+ QualityFarmsDescQPContent);
				
		String ExpQualityFarmsDescQPFontSize = sheet3.getCell(2,8).getContents();
		System.out.println("Expected Font Size of The Farms Desc in Quality Page is : "	+ ExpQualityFarmsDescQPFontSize);
		String ExpQualityFarmsDescQPFontFamily = sheet3.getCell(3,8).getContents();
		System.out.println("Expected Font Family of The Farms Desc in Quality Page is : "	+ ExpQualityFarmsDescQPFontFamily);
		String ExpQualityFarmsDescQPContent = sheet3.getCell(4,8).getContents();
		System.out.println("Expected Content of The Farms Desc in Quality Page is : "	+ ExpQualityFarmsDescQPContent);
				
		if(QualityFarmsDescQPFontSize.equals(ExpQualityFarmsDescQPFontSize))
		{
			System.out.println("The Farms Desc in Quality Page Font Size is in match with creative - Pass");
		}
		else
		{
			System.out.println("The Farms Desc in Quality Page Font Size is not in match with creative - Fail");
			bw.write("The Farms Desc in Quality Page Font Size is not in match with creative - Fail"); 
		    bw.newLine();
		}
				
		if(QualityFarmsDescQPFontFamily.equals(ExpQualityFarmsDescQPFontFamily))
		{
			System.out.println("The Farms Desc in Quality Page Font Family is in match with creative - Pass");
		}
		else
		{
			System.out.println("The Farms Desc in Quality Page Font Family is not in match with creative - Fail");
			bw.write("The Farms Desc in Quality Page Font Family is not in match with creative - Fail"); 
		    bw.newLine();
		}
		

		if(QualityFarmsDescQPContent.equals(ExpQualityFarmsDescQPContent))
		{
			System.out.println("The Farms Desc Content in Quality Page is in match with creative - Pass");
		}
		else
		{
			System.out.println("The Farms Desc Content in Quality Page is not in match with creative - Fail");
			bw.write("The Farms Desc Content in Quality Page is not in match with creative - Fail"); 
		    bw.newLine();
		}
		
		if(QualityFarmsDescQPFontSize.equals(ExpQualityFarmsDescQPFontSize) && QualityFarmsDescQPFontFamily.equals(ExpQualityFarmsDescQPFontFamily) && QualityFarmsDescQPContent.equals(ExpQualityFarmsDescQPContent))
		{
			Label QualityFarmsDescQPCSSP = new Label(5,8,"PASS"); 
			wsheet3.addCell(QualityFarmsDescQPCSSP); 
		}
		else
		{
			Label QualityFarmsDescQPCSSF = new Label(5,8,"FAIL"); 
			wsheet3.addCell(QualityFarmsDescQPCSSF); 
		}
		
		// Quality Page - BUILDING
		
		WebElement QualityBuildingQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[7]/div/div/div/div/div[2]/div/div[1]")); 
		
		String QualityBuildingQPFontSize = QualityBuildingQP.getCssValue("font-size");
		System.out.println("Actual Font Size of BUILDING Text in Quality Page is : "	+ QualityBuildingQPFontSize);
		String QualityBuildingQPFontFamily = QualityBuildingQP.getCssValue("font-family");
		System.out.println("Actual Font Family of BUILDING Text in Quality Page is : "	+ QualityBuildingQPFontFamily);
		String QualityBuildingQPContent = QualityBuildingQP.getText();
		System.out.println("Actual Content of BUILDING Text in Quality Page is : "	+ QualityBuildingQPContent);
				
		String ExpQualityBuildingQPFontSize = sheet3.getCell(2,9).getContents();
		System.out.println("Expected Font Size of BUILDING Text in Quality Page is : "	+ ExpQualityBuildingQPFontSize);
		String ExpQualityBuildingQPFontFamily = sheet3.getCell(3,9).getContents();
		System.out.println("Expected Font Family of BUILDING Text in Quality Page is : "	+ ExpQualityBuildingQPFontFamily);
		String ExpQualityBuildingQPContent = sheet3.getCell(4,9).getContents();
		System.out.println("Expected Content of BUILDING Text in Quality Page is : "	+ ExpQualityBuildingQPContent);
				
				
		if(QualityBuildingQPFontSize.equals(ExpQualityBuildingQPFontSize))
		{
			System.out.println("BUILDING Text in Quality Page Font Size is in match with creative - Pass");
		}
		else
		{
			System.out.println("BUILDING Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("BUILDING Text in Quality Page Font Size is not in match with creative - Fail"); 
		    bw.newLine();
		}
				
		if(QualityBuildingQPFontFamily.equals(ExpQualityBuildingQPFontFamily))
		{
			System.out.println("BUILDING Text in Quality Page Font Family is in match with creative - Pass");
		}
		else
		{
			System.out.println("BUILDING Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("BUILDING Text in Quality Page Font Family is not in match with creative - Fail"); 
		    bw.newLine();
		}
		

		if(QualityBuildingQPContent.equals(ExpQualityBuildingQPContent))
		{
			System.out.println("BUILDING Text Content in Quality Page is in match with creative - Pass");
		}
		else
		{
			System.out.println("BUILDING Text Content in Quality Page is not in match with creative - Fail");
			bw.write("BUILDING Text Content in Quality Page is not in match with creative - Fail"); 
		    bw.newLine();
		}
		
		if(QualityBuildingQPFontSize.equals(ExpQualityBuildingQPFontSize) && QualityBuildingQPFontFamily.equals(ExpQualityBuildingQPFontFamily) && QualityBuildingQPContent.equals(ExpQualityBuildingQPContent))
		{
			Label QualityBuildingQPCSSP = new Label(5,9,"PASS"); 
			wsheet3.addCell(QualityBuildingQPCSSP); 
		}
		else
		{
			Label QualityBuildingQPCSSF = new Label(5,9,"FAIL"); 
			wsheet3.addCell(QualityBuildingQPCSSF); 
		}
		
		// Quality Page - RELATIONSHIPS 
		
		WebElement QualityRelationQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[7]/div/div/div/div/div[2]/div/div[2]")); 
		
		String QualityRelationQPFontSize = QualityRelationQP.getCssValue("font-size");
		System.out.println("Actual Font Size of RELATIONSHIPS Text in Quality Page is : "	+ QualityRelationQPFontSize);
		String QualityRelationQPFontFamily = QualityRelationQP.getCssValue("font-family");
		System.out.println("Actual Font Family of RELATIONSHIPS Text in Quality Page is : "	+ QualityRelationQPFontFamily);
		String QualityRelationQPContent = QualityRelationQP.getText();
		System.out.println("Actual Content of RELATIONSHIPS Text in Quality Page is : "	+ QualityRelationQPContent);
				
		String ExpQualityRelationQPFontSize = sheet3.getCell(2,10).getContents();
		System.out.println("Expected Font Size of RELATIONSHIPS Text in Quality Page is : "	+ ExpQualityRelationQPFontSize);
		String ExpQualityRelationQPFontFamily = sheet3.getCell(3,10).getContents();
		System.out.println("Expected Font Family of RELATIONSHIPS Text in Quality Page is : "	+ ExpQualityRelationQPFontFamily);
		String ExpQualityRelationQPContent = sheet3.getCell(4,10).getContents();
		System.out.println("Expected Content of RELATIONSHIPS Text in Quality Page is : "	+ ExpQualityRelationQPContent);
						
		if(QualityRelationQPFontSize.equals(ExpQualityRelationQPFontSize))
		{
			System.out.println("RELATIONSHIPS Text in Quality Page Font Size is in match with creative - Pass");
		}
		else
		{
			System.out.println("RELATIONSHIPS Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("RELATIONSHIPS Text in Quality Page Font Size is not in match with creative - Fail"); 
		    bw.newLine();
		}
				
		if(QualityRelationQPFontFamily.equals(ExpQualityRelationQPFontFamily))
		{
			System.out.println("RELATIONSHIPS Text in Quality Page Font Family is in match with creative - Pass");
		}
		else
		{
			System.out.println("RELATIONSHIPS Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("RELATIONSHIPS Text in Quality Page Font Family is not in match with creative - Fail"); 
		    bw.newLine();
		}
		

		if(QualityRelationQPContent.equals(ExpQualityRelationQPContent))
		{
			System.out.println("RELATIONSHIPS Text Content in Quality Page is in match with creative - Pass");
		}
		else
		{
			System.out.println("RELATIONSHIPS Text Content in Quality Page is not in match with creative - Fail");
			bw.write("RELATIONSHIPS Text Content in Quality Page is not in match with creative - Fail"); 
		    bw.newLine();
		}
		
		if(QualityRelationQPFontSize.equals(ExpQualityRelationQPFontSize) && QualityRelationQPFontFamily.equals(ExpQualityRelationQPFontFamily) && QualityRelationQPContent.equals(ExpQualityRelationQPContent))
		{
			Label QualityRelationQPCSSP = new Label(5,10,"PASS"); 
			wsheet3.addCell(QualityRelationQPCSSP); 
		}
		else
		{
			Label QualityRelationQPCSSF = new Label(5,10,"FAIL"); 
			wsheet3.addCell(QualityRelationQPCSSF); 
		}
		
		
		// Quality Page - Building Relationship Desc
		
		WebElement QualityRelationDescQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[7]/div/div/div/div/div[2]/div/div[4]")); 
		
		String QualityRelationDescQPFontSize = QualityRelationDescQP.getCssValue("font-size");
		System.out.println("Actual Font Size of Building Relationship Desc in Quality Page is : "	+ QualityRelationDescQPFontSize);
		String QualityRelationDescQPFontFamily = QualityRelationDescQP.getCssValue("font-family");
		System.out.println("Actual Font Family of Building Relationship Desc in Quality Page is : "	+ QualityRelationDescQPFontFamily);
		String QualityRelationDescQPContent = QualityRelationDescQP.getText();
		System.out.println("Actual Content of Building Relationship Desc in Quality Page is : "	+ QualityRelationDescQPContent);
				
		String ExpQualityRelationDescQPFontSize = sheet3.getCell(2,11).getContents();
		System.out.println("Expected Font Size of Building Relationship Desc in Quality Page is : "	+ ExpQualityRelationDescQPFontSize);
		String ExpQualityRelationDescQPFontFamily = sheet3.getCell(3,11).getContents();
		System.out.println("Expected Font Family of Building Relationship Desc in Quality Page is : "	+ ExpQualityRelationDescQPFontFamily);
		String ExpQualityRelationDescQPContent = sheet3.getCell(4,11).getContents();
		System.out.println("Expected Content of Building Relationship Desc in Quality Page is : "	+ ExpQualityRelationDescQPContent);
				
		if(QualityRelationDescQPFontSize.equals(ExpQualityRelationDescQPFontSize))
		{
			System.out.println("Building Relationship Desc in Quality Page Font Size is in match with creative - Pass");
		}
		else
		{
			System.out.println("Building Relationship Desc in Quality Page Font Size is not in match with creative - Fail");
			bw.write("Building Relationship Desc in Quality Page Font Size is not in match with creative - Fail"); 
		    bw.newLine();
		}
				
		if(QualityRelationDescQPFontFamily.equals(ExpQualityRelationDescQPFontFamily))
		{
			System.out.println("Building Relationship Desc in Quality Page Font Family is in match with creative - Pass");
		}
		else
		{
			System.out.println("Building Relationship Desc in Quality Page Font Family is not in match with creative - Fail");
			bw.write("Building Relationship Desc in Quality Page Font Family is not in match with creative - Fail"); 
		    bw.newLine();
		}
		

		if(QualityRelationDescQPContent.equals(ExpQualityRelationDescQPContent))
		{
			System.out.println("Building Relationship Desc Content in Quality Page is in match with creative - Pass");
		}
		else
		{
			System.out.println("Building Relationship Desc Content in Quality Page is not in match with creative - Fail");
			bw.write("Building Relationship Desc Content in Quality Page is not in match with creative - Fail"); 
		    bw.newLine();
		}
		
		if(QualityRelationDescQPFontSize.equals(ExpQualityRelationDescQPFontSize) && QualityRelationDescQPFontFamily.equals(ExpQualityRelationDescQPFontFamily) && QualityRelationDescQPContent.equals(ExpQualityRelationDescQPContent))
		{
			Label QualityRelationDescQPCSSP = new Label(5,11,"PASS"); 
			wsheet3.addCell(QualityRelationDescQPCSSP); 
		}
		else
		{
			Label QualityRelationDescQPCSSF = new Label(5,11,"FAIL"); 
			wsheet3.addCell(QualityRelationDescQPCSSF); 
		}
		
		
		// Quality Page TEST 01
		
		WebElement QualityTest01QP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[7]/div/div/div/div/div[6]/div/div[1]/div[1]")); 
		String QualityTest01QPColor = QualityTest01QP.getCssValue("color");
		System.out.println("Actual Color of TEST 01 Text in Quality Page is : "	+ QualityTest01QPColor );
		// Convert RGB to Hex Value
		Color HQualityTest01QPColor = Color.fromString(QualityTest01QPColor);
		String HexQualityTest01QPColor = HQualityTest01QPColor.asHex();
		System.out.println("Actual Hex Color of TEST 01 Text in Quality Page is : "	+ HexQualityTest01QPColor );
		
		String QualityTest01QPFontSize = QualityTest01QP.getCssValue("font-size");
		System.out.println("Actual Font Size of TEST 01 Text in Quality Page is : "	+ QualityTest01QPFontSize);
		String QualityTest01QPFontFamily = QualityTest01QP.getCssValue("font-family");
		System.out.println("Actual Font Family of TEST 01 Text in Quality Page is : "	+ QualityTest01QPFontFamily);
		String QualityTest01QPContent = QualityTest01QP.getText();
		System.out.println("Actual Content of TEST 01 Text in Quality Page is : "	+ QualityTest01QPContent);
				
		String ExpQualityTest01QPColor = sheet3.getCell(1,12).getContents(); //column,row
		System.out.println("Expected Color of TEST 01 Text in Quality Page is : "	+ ExpQualityTest01QPColor );
		String ExpQualityTest01QPFontSize = sheet3.getCell(2,12).getContents();
		System.out.println("Expected Font Size of TEST 01 Text in Quality Page is : "	+ ExpQualityTest01QPFontSize);
		String ExpQualityTest01QPFontFamily = sheet3.getCell(3,12).getContents();
		System.out.println("Expected Font Family of TEST 01 Text in Quality Page is : "	+ ExpQualityTest01QPFontFamily);
		String ExpQualityTest01QPContent = sheet3.getCell(4,12).getContents();
		System.out.println("Expected Content of TEST 01 Text in Quality Page is : "	+ ExpQualityTest01QPContent);
				
		if(HexQualityTest01QPColor.equals(ExpQualityTest01QPColor))
		{
			System.out.println("TEST 01 Text in Quality Page Color is in match with creative - Pass");
		}
		else
		{
			System.out.println("TEST 01 Text in Quality Page Color is not in match with creative - Fail");
			bw.write("TEST 01 Text in Quality Page Color is not in match with creative - Fail"); 
		    bw.newLine();
		}
				
		if(QualityTest01QPFontSize.equals(ExpQualityTest01QPFontSize))
		{
			System.out.println("TEST 01 Text in Quality Page Font Size is in match with creative - Pass");
		}
		else
		{
			System.out.println("TEST 01 Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("TEST 01 Text in Quality Page Font Size is not in match with creative - Fail"); 
		    bw.newLine();
		}
				
		if(QualityTest01QPFontFamily.equals(ExpQualityTest01QPFontFamily))
		{
			System.out.println("TEST 01 Text in Quality Page Font Family is in match with creative - Pass");
		}
		else
		{
			System.out.println("TEST 01 Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("TEST 01 Text in Quality Page Font Family is not in match with creative - Fail");  
		    bw.newLine();
		}
		

		if(QualityTest01QPContent.equals(ExpQualityTest01QPContent))
		{
			System.out.println("TEST 01 Text Content in Quality Page is in match with creative - Pass");
		}
		else
		{
			System.out.println("TEST 01 Text Content in Quality Page is not in match with creative - Fail");
			bw.write("TEST 01 Text Content in Quality Page is not in match with creative - Fail"); 
		    bw.newLine();
		}
		
		if(HexQualityTest01QPColor.equals(ExpQualityTest01QPColor) && QualityTest01QPFontSize.equals(ExpQualityTest01QPFontSize) && QualityTest01QPFontFamily.equals(ExpQualityTest01QPFontFamily) && QualityTest01QPContent.equals(ExpQualityTest01QPContent))
		{
			Label QualityTest01QPCSSP = new Label(5,12,"PASS"); 
			wsheet3.addCell(QualityTest01QPCSSP); 
		}
		else
		{
			Label QualityTest01QPCSSF = new Label(5,12,"FAIL"); 
			wsheet3.addCell(QualityTest01QPCSSF); 
		}
		
		// Quality Page TEST 01 - At the Farm
		
		WebElement QualityTest01AtFarmQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[7]/div/div/div/div/div[6]/div/div[2]/div[1]")); 
		String QualityTest01AtFarmQPColor = QualityTest01AtFarmQP.getCssValue("color");
		System.out.println("Actual Color of TEST 01 At the Farm Text in Quality Page is : "	+ QualityTest01AtFarmQPColor );
		// Convert RGB to Hex Value
		Color HQualityTest01AtFarmQPColor = Color.fromString(QualityTest01AtFarmQPColor);
		String HexQualityTest01AtFarmQPColor = HQualityTest01AtFarmQPColor.asHex();
		System.out.println("Actual Hex Color of TEST 01 At the Farm Text in Quality Page is : "	+ HexQualityTest01AtFarmQPColor );
		
		String QualityTest01AtFarmQPFontSize = QualityTest01AtFarmQP.getCssValue("font-size");
		System.out.println("Actual Font Size of TEST 01 At the Farm Text in Quality Page is : "	+ QualityTest01AtFarmQPFontSize);
		String QualityTest01AtFarmQPFontFamily = QualityTest01AtFarmQP.getCssValue("font-family");
		System.out.println("Actual Font Family of TEST 01 At the Farm Text in Quality Page is : "	+ QualityTest01AtFarmQPFontFamily);
		String QualityTest01AtFarmQPContent = QualityTest01AtFarmQP.getText();
		System.out.println("Actual Content of TEST 01 At the Farm Text in Quality Page is : "	+ QualityTest01AtFarmQPContent);
				
		String ExpQualityTest01AtFarmQPColor = sheet3.getCell(1,13).getContents(); //column,row
		System.out.println("Expected Color of TEST 01 At the Farm Text in Quality Page is : "	+ ExpQualityTest01AtFarmQPColor );
		String ExpQualityTest01AtFarmQPFontSize = sheet3.getCell(2,13).getContents();
		System.out.println("Expected Font Size of TEST 01 At the Farm Text in Quality Page is : "	+ ExpQualityTest01AtFarmQPFontSize);
		String ExpQualityTest01AtFarmQPFontFamily = sheet3.getCell(3,13).getContents();
		System.out.println("Expected Font Family of TEST 01 At the Farm Text in Quality Page is : "	+ ExpQualityTest01AtFarmQPFontFamily);
		String ExpQualityTest01AtFarmQPContent = sheet3.getCell(4,13).getContents();
		System.out.println("Expected Content of TEST 01 At the Farm Text in Quality Page is : "	+ ExpQualityTest01AtFarmQPContent);
				
		if(HexQualityTest01AtFarmQPColor.equals(ExpQualityTest01AtFarmQPColor))
		{
			System.out.println("TEST 01 At the Farm Text in Quality Page Color is in match with creative - Pass");
		}
		else
		{
			System.out.println("TEST 01 At the Farm Text in Quality Page Color is not in match with creative - Fail");
			bw.write("TEST 01 At the Farm Text in Quality Page Color is not in match with creative - Fail"); 
		    bw.newLine();
		}
				
		if(QualityTest01AtFarmQPFontSize.equals(ExpQualityTest01AtFarmQPFontSize))
		{
			System.out.println("TEST 01 At the Farm Text in Quality Page Font Size is in match with creative - Pass");
		}
		else
		{
			System.out.println("TEST 01 At the Farm Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("TEST 01 At the Farm Text in Quality Page Font Size is not in match with creative - Fail"); 
		    bw.newLine();
		}
				
		if(QualityTest01AtFarmQPFontFamily.equals(ExpQualityTest01AtFarmQPFontFamily))
		{
			System.out.println("TEST 01 At the Farm Text in Quality Page Font Family is in match with creative - Pass");
		}
		else
		{
			System.out.println("TEST 01 At the Farm Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("TEST 01 At the Farm Text in Quality Page Font Family is not in match with creative - Fail"); 
		    bw.newLine();
		}
		

		if(QualityTest01AtFarmQPContent.equals(ExpQualityTest01AtFarmQPContent))
		{
			System.out.println("TEST 01 At the Farm Text Content in Quality Page is in match with creative - Pass");
		}
		else
		{
			System.out.println("TEST 01 At the Farm Text Content in Quality Page is not in match with creative - Fail");
			bw.write("TEST 01 At the Farm Text Content in Quality Page is not in match with creative - Fail"); 
		    bw.newLine();
		}
		
		if(HexQualityTest01AtFarmQPColor.equals(ExpQualityTest01AtFarmQPColor) && QualityTest01AtFarmQPFontSize.equals(ExpQualityTest01AtFarmQPFontSize) && QualityTest01AtFarmQPFontFamily.equals(ExpQualityTest01AtFarmQPFontFamily) && QualityTest01AtFarmQPContent.equals(ExpQualityTest01AtFarmQPContent))
		{
			Label QualityTest01AtFarmQPCSSP = new Label(5,13,"PASS"); 
			wsheet3.addCell(QualityTest01AtFarmQPCSSP); 
		}
		else
		{
			Label QualityTest01AtFarmQPCSSF = new Label(5,13,"FAIL"); 
			wsheet3.addCell(QualityTest01AtFarmQPCSSF); 
		}
		
		
		// Quality Page TEST 01 Desc
		
		WebElement QualityTest01DescQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[7]/div/div/div/div/div[6]/div/div[2]/div[2]")); 
		String QualityTest01DescQPColor = QualityTest01DescQP.getCssValue("color");
		System.out.println("Actual Color of TEST 01 Desc in Quality Page is : "	+ QualityTest01DescQPColor );
		// Convert RGB to Hex Value
		Color HQualityTest01DescQPColor = Color.fromString(QualityTest01DescQPColor);
		String HexQualityTest01DescQPColor = HQualityTest01DescQPColor.asHex();
		System.out.println("Actual Hex Color of TEST 01 Desc in Quality Page is : "	+ HexQualityTest01DescQPColor );
		
		String QualityTest01DescQPFontSize = QualityTest01DescQP.getCssValue("font-size");
		System.out.println("Actual Font Size of TEST 01 Desc in Quality Page is : "	+ QualityTest01DescQPFontSize);
		String QualityTest01DescQPFontFamily = QualityTest01DescQP.getCssValue("font-family");
		System.out.println("Actual Font Family of TEST 01 Desc in Quality Page is : "	+ QualityTest01DescQPFontFamily);
		String QualityTest01DescQPContent = QualityTest01DescQP.getText();
		System.out.println("Actual Content of TEST 01 Desc in Quality Page is : "	+ QualityTest01DescQPContent);
				
		String ExpQualityTest01DescQPColor = sheet3.getCell(1,14).getContents(); //column,row
		System.out.println("Expected Color of TEST 01 Desc in Quality Page is : "	+ ExpQualityTest01DescQPColor );
		String ExpQualityTest01DescQPFontSize = sheet3.getCell(2,14).getContents();
		System.out.println("Expected Font Size of TEST 01 Desc in Quality Page is : "	+ ExpQualityTest01DescQPFontSize);
		String ExpQualityTest01DescQPFontFamily = sheet3.getCell(3,14).getContents();
		System.out.println("Expected Font Family of TEST 01 Desc in Quality Page is : "	+ ExpQualityTest01DescQPFontFamily);
		String ExpQualityTest01DescQPContent = sheet3.getCell(4,14).getContents();
		System.out.println("Expected Content of TEST 01 Desc in Quality Page is : "	+ ExpQualityTest01DescQPContent);
				
		if(HexQualityTest01DescQPColor.equals(ExpQualityTest01DescQPColor))
		{
			System.out.println("TEST 01 Desc in Quality Page Color is in match with creative - Pass");
		}
		else
		{
			System.out.println("TEST 01 Desc in Quality Page Color is not in match with creative - Fail");
			bw.write("TEST 01 Desc in Quality Page Color is not in match with creative - Fail"); 
		    bw.newLine();
		}
				
		if(QualityTest01DescQPFontSize.equals(ExpQualityTest01DescQPFontSize))
		{
			System.out.println("TEST 01 Desc in Quality Page Font Size is in match with creative - Pass");
		}
		else
		{
			System.out.println("TEST 01 Desc in Quality Page Font Size is not in match with creative - Fail");
			bw.write("TEST 01 Desc in Quality Page Font Size is not in match with creative - Fail"); 
		    bw.newLine();
		}
				
		if(QualityTest01DescQPFontFamily.equals(ExpQualityTest01DescQPFontFamily))
		{
			System.out.println("TEST 01 Desc in Quality Page Font Family is in match with creative - Pass");
		}
		else
		{
			System.out.println("TEST 01 Desc in Quality Page Font Family is not in match with creative - Fail");
			bw.write("TEST 01 Desc in Quality Page Font Family is not in match with creative - Fail"); 
		    bw.newLine();
		}
		

		if(QualityTest01DescQPContent.equals(ExpQualityTest01DescQPContent))
		{
			System.out.println("TEST 01 Desc Content in Quality Page is in match with creative - Pass");
		}
		else
		{
			System.out.println("TEST 01 Desc Content in Quality Page is not in match with creative - Fail");
			bw.write("TEST 01 Desc Content in Quality Page is not in match with creative - Fail"); 
		    bw.newLine();
		}
		
		if(HexQualityTest01DescQPColor.equals(ExpQualityTest01DescQPColor) && QualityTest01DescQPFontSize.equals(ExpQualityTest01DescQPFontSize) && QualityTest01DescQPFontFamily.equals(ExpQualityTest01DescQPFontFamily) && QualityTest01DescQPContent.equals(ExpQualityTest01DescQPContent))
		{
			Label QualityTest01DescQPCSSP = new Label(5,14,"PASS"); 
			wsheet3.addCell(QualityTest01DescQPCSSP); 
		}
		else
		{
			Label QualityTest01DescQPCSSF = new Label(5,14,"FAIL"); 
			wsheet3.addCell(QualityTest01DescQPCSSF); 
		}

		// Quality Page - LEARNING 
		
		WebElement QualityLearningQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[8]/div/div/div/div/div[2]/div/div[1]")); 
		
		String QualityLearningQPFontSize = QualityLearningQP.getCssValue("font-size");
		System.out.println("Actual Font Size of LEARNING Text in Quality Page is : "	+ QualityLearningQPFontSize);
		String QualityLearningQPFontFamily = QualityLearningQP.getCssValue("font-family");
		System.out.println("Actual Font Family of LEARNING Text in Quality Page is : "	+ QualityLearningQPFontFamily);
		String QualityLearningQPContent = QualityLearningQP.getText();
		System.out.println("Actual Content of LEARNING Text in Quality Page is : "	+ QualityLearningQPContent);
				
		String ExpQualityLearningQPFontSize = sheet3.getCell(2,15).getContents();
		System.out.println("Expected Font Size of LEARNING Text in Quality Page is : "	+ ExpQualityLearningQPFontSize);
		String ExpQualityLearningQPFontFamily = sheet3.getCell(3,15).getContents();
		System.out.println("Expected Font Family of LEARNING Text in Quality Page is : "	+ ExpQualityLearningQPFontFamily);
		String ExpQualityLearningQPContent = sheet3.getCell(4,15).getContents();
		System.out.println("Expected Content of LEARNING Text in Quality Page is : "	+ ExpQualityLearningQPContent);
				
				
		if(QualityLearningQPFontSize.equals(ExpQualityLearningQPFontSize))
		{
			System.out.println("LEARNING Text in Quality Page Font Size is in match with creative - Pass");
		}
		else
		{
			System.out.println("LEARNING Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("LEARNING Text in Quality Page Font Size is not in match with creative - Fail"); 
		    bw.newLine();
		}
				
		if(QualityLearningQPFontFamily.equals(ExpQualityLearningQPFontFamily))
		{
			System.out.println("LEARNING Text in Quality Page Font Family is in match with creative - Pass");
		}
		else
		{
			System.out.println("LEARNING Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("LEARNING Text in Quality Page Font Family is not in match with creative - Fail"); 
		    bw.newLine();
		}
		

		if(QualityLearningQPContent.equals(ExpQualityLearningQPContent))
		{
			System.out.println("LEARNING Text Content in Quality Page is in match with creative - Pass");
		}
		else
		{
			System.out.println("LEARNING Text Content in Quality Page is not in match with creative - Fail");
			bw.write("LEARNING Text Content in Quality Page is not in match with creative - Fail"); 
		    bw.newLine();
		}
		
		if(QualityLearningQPFontSize.equals(ExpQualityLearningQPFontSize) && QualityLearningQPFontFamily.equals(ExpQualityLearningQPFontFamily) && QualityLearningQPContent.equals(ExpQualityLearningQPContent))
		{
			Label QualityLearningQPCSSP = new Label(5,15,"PASS"); 
			wsheet3.addCell(QualityLearningQPCSSP); 
		}
		else
		{
			Label QualityLearningQPCSSF = new Label(5,15,"FAIL"); 
			wsheet3.addCell(QualityLearningQPCSSF); 
		}
		
		// Quality Page - TOGETHER  
		
		WebElement QualityTogetherQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[8]/div/div/div/div/div[2]/div/div[2]")); 
		
		String QualityTogetherQPFontSize = QualityTogetherQP.getCssValue("font-size");
		System.out.println("Actual Font Size of TOGETHER Text in Quality Page is : "	+ QualityTogetherQPFontSize);
		String QualityTogetherQPFontFamily = QualityTogetherQP.getCssValue("font-family");
		System.out.println("Actual Font Family of TOGETHER Text in Quality Page is : "	+ QualityTogetherQPFontFamily);
		String QualityTogetherQPContent = QualityTogetherQP.getText();
		System.out.println("Actual Content of TOGETHER Text in Quality Page is : "	+ QualityTogetherQPContent);
				
		String ExpQualityTogetherQPFontSize = sheet3.getCell(2,16).getContents();
		System.out.println("Expected Font Size of TOGETHER Text in Quality Page is : "	+ ExpQualityTogetherQPFontSize);
		String ExpQualityTogetherQPFontFamily = sheet3.getCell(3,16).getContents();
		System.out.println("Expected Font Family of TOGETHER Text in Quality Page is : "	+ ExpQualityTogetherQPFontFamily);
		String ExpQualityTogetherQPContent = sheet3.getCell(4,16).getContents();
		System.out.println("Expected Content of TOGETHER Text in Quality Page is : "	+ ExpQualityTogetherQPContent);
						
		if(QualityTogetherQPFontSize.equals(ExpQualityTogetherQPFontSize))
		{
			System.out.println("TOGETHER Text in Quality Page Font Size is in match with creative - Pass");
		}
		else
		{
			System.out.println("TOGETHER Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("TOGETHER Text in Quality Page Font Size is not in match with creative - Fail"); 
		    bw.newLine();
		}
				
		if(QualityTogetherQPFontFamily.equals(ExpQualityTogetherQPFontFamily))
		{
			System.out.println("TOGETHER Text in Quality Page Font Family is in match with creative - Pass");
		}
		else
		{
			System.out.println("TOGETHER Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("TOGETHER Text in Quality Page Font Family is not in match with creative - Fail"); 
		    bw.newLine();
		}
		

		if(QualityTogetherQPContent.equals(ExpQualityTogetherQPContent))
		{
			System.out.println("TOGETHER Text Content in Quality Page is in match with creative - Pass");
		}
		else
		{
			System.out.println("TOGETHER Text Content in Quality Page is not in match with creative - Fail");
			bw.write("TOGETHER Text Content in Quality Page is not in match with creative - Fail"); 
		    bw.newLine();
		}
		
		if(QualityTogetherQPFontSize.equals(ExpQualityTogetherQPFontSize) && QualityTogetherQPFontFamily.equals(ExpQualityTogetherQPFontFamily) && QualityTogetherQPContent.equals(ExpQualityTogetherQPContent))
		{
			Label QualityTogetherQPCSSP = new Label(5,16,"PASS"); 
			wsheet3.addCell(QualityTogetherQPCSSP); 
		}
		else
		{
			Label QualityTogetherQPCSSF = new Label(5,16,"FAIL"); 
			wsheet3.addCell(QualityTogetherQPCSSF); 
		}
		
		// Quality Page - Learning Together Desc
		
		WebElement QualityTogetherDescQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[8]/div/div/div/div/div[2]/div/div[4]")); 
		
		String QualityTogetherDescQPFontSize = QualityTogetherDescQP.getCssValue("font-size");
		System.out.println("Actual Font Size of Learning Together Desc in Quality Page is : "	+ QualityTogetherDescQPFontSize);
		String QualityTogetherDescQPFontFamily = QualityTogetherDescQP.getCssValue("font-family");
		System.out.println("Actual Font Family of Learning Together Desc in Quality Page is : "	+ QualityTogetherDescQPFontFamily);
		String QualityTogetherDescQPContent = QualityTogetherDescQP.getText();
		System.out.println("Actual Content of Learning Together Desc in Quality Page is : "	+ QualityTogetherDescQPContent);
				
		String ExpQualityTogetherDescQPFontSize = sheet3.getCell(2,17).getContents();
		System.out.println("Expected Font Size of Learning Together Desc in Quality Page is : "	+ ExpQualityTogetherDescQPFontSize);
		String ExpQualityTogetherDescQPFontFamily = sheet3.getCell(3,17).getContents();
		System.out.println("Expected Font Family of Learning Together Desc in Quality Page is : "	+ ExpQualityTogetherDescQPFontFamily);
		String ExpQualityTogetherDescQPContent = sheet3.getCell(4,17).getContents();
		System.out.println("Expected Content of Learning Together Desc in Quality Page is : "	+ ExpQualityTogetherDescQPContent);
				
		if(QualityTogetherDescQPFontSize.equals(ExpQualityTogetherDescQPFontSize))
		{
			System.out.println("Learning Together Desc in Quality Page Font Size is in match with creative - Pass");
		}
		else
		{
			System.out.println("Learning Together Desc in Quality Page Font Size is not in match with creative - Fail");
			bw.write("Learning Together Desc in Quality Page Font Size is not in match with creative - Fail"); 
		    bw.newLine();
		}
				
		if(QualityTogetherDescQPFontFamily.equals(ExpQualityTogetherDescQPFontFamily))
		{
			System.out.println("Learning Together Desc in Quality Page Font Family is in match with creative - Pass");
		}
		else
		{
			System.out.println("Learning Together Desc in Quality Page Font Family is not in match with creative - Fail");
			bw.write("Learning Together Desc in Quality Page Font Family is not in match with creative - Fail"); 
		    bw.newLine();
		}
		

		if(QualityTogetherDescQPContent.equals(ExpQualityTogetherDescQPContent))
		{
			System.out.println("Learning Together Desc Content in Quality Page is in match with creative - Pass");
		}
		else
		{
			System.out.println("Learning Together Desc Content in Quality Page is not in match with creative - Fail");
			bw.write("Learning Together Desc Content in Quality Page is not in match with creative - Fail"); 
		    bw.newLine();
		}
		
		if(QualityTogetherDescQPFontSize.equals(ExpQualityTogetherDescQPFontSize) && QualityTogetherDescQPFontFamily.equals(ExpQualityTogetherDescQPFontFamily) && QualityTogetherDescQPContent.equals(ExpQualityTogetherDescQPContent))
		{
			Label QualityTogetherDescQPCSSP = new Label(5,17,"PASS"); 
			wsheet3.addCell(QualityTogetherDescQPCSSP); 
		}
		else
		{
			Label QualityTogetherDescQPCSSF = new Label(5,17,"FAIL"); 
			wsheet3.addCell(QualityTogetherDescQPCSSF); 
		}
		
		Thread.sleep(3000);
		JavascriptExecutor jseqpnt = (JavascriptExecutor)driver;
		jseqpnt.executeScript("window.scrollBy(0,2000)", "");
		Thread.sleep(8000);
		
		// SMALL CHANGES, BIG IMPACT - Num Tab 1
		
		WebElement SmallN1QP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[8]/div/div/div/div/div[3]/div[1]/div[1]/div[1]")); 
		String SmallN1QPColor = SmallN1QP.getCssValue("color");
		System.out.println("Actual Color of SMALL CHANGES, BIG IMPACT in Num Tab 1 in Quality Page is : "	+ SmallN1QPColor );
		// Convert RGB to Hex Value
		Color HSmallN1QPColor = Color.fromString(SmallN1QPColor);
		String HexSmallN1QPColor = HSmallN1QPColor.asHex();
		System.out.println("Actual Hex Color of SMALL CHANGES, BIG IMPACT in Num Tab 1 in Quality Page is : "	+ HexSmallN1QPColor );
		
		String SmallN1QPFontSize = SmallN1QP.getCssValue("font-size");
		System.out.println("Actual Font Size of SMALL CHANGES, BIG IMPACT in Num Tab 1 in Quality Page is : "	+ SmallN1QPFontSize);
		String SmallN1QPFontFamily = SmallN1QP.getCssValue("font-family");
		System.out.println("Actual Font Family of SMALL CHANGES, BIG IMPACT in Num Tab 1 in Quality Page is : "	+ SmallN1QPFontFamily);
		String SmallN1QPContent = SmallN1QP.getText();
		System.out.println("Actual Content of SMALL CHANGES, BIG IMPACT in Num Tab 1 in Quality Page is : "	+ SmallN1QPContent);
				
		String ExpSmallN1QPColor = sheet3.getCell(1,18).getContents(); //column,row
		System.out.println("Expected Color of SMALL CHANGES, BIG IMPACT in Num Tab 1 in Quality Page is : "	+ ExpSmallN1QPColor );
		String ExpSmallN1QPFontSize = sheet3.getCell(2,18).getContents();
		System.out.println("Expected Font Size of SMALL CHANGES, BIG IMPACT in Num Tab 1 in Quality Page is : "	+ ExpSmallN1QPFontSize);
		String ExpSmallN1QPFontFamily = sheet3.getCell(3,18).getContents();
		System.out.println("Expected Font Family of SMALL CHANGES, BIG IMPACT in Num Tab 1 in Quality Page is : "	+ ExpSmallN1QPFontFamily);
		String ExpSmallN1QPContent = sheet3.getCell(4,18).getContents();
		System.out.println("Expected Content of SMALL CHANGES, BIG IMPACT in Num Tab 1 in Quality Page is : "	+ ExpSmallN1QPContent);
				
		if(HexSmallN1QPColor.equals(ExpSmallN1QPColor))
		{
			System.out.println("SMALL CHANGES, BIG IMPACT in Num Tab 1 in Quality Page Color is in match with creative - Pass");
		}
		else
		{
			System.out.println("SMALL CHANGES, BIG IMPACT in Num Tab 1 in Quality Page Color is not in match with creative - Fail");
			bw.write("SMALL CHANGES, BIG IMPACT in Num Tab 1 in Quality Page Color is not in match with creative - Fail"); 
		    bw.newLine();
		}
				
		if(SmallN1QPFontSize.equals(ExpSmallN1QPFontSize))
		{
			System.out.println("SMALL CHANGES, BIG IMPACT in Num Tab 1 in Quality Page Font Size is in match with creative - Pass");
		}
		else
		{
			System.out.println("SMALL CHANGES, BIG IMPACT in Num Tab 1 in Quality Page Font Size is not in match with creative - Fail");
			bw.write("SMALL CHANGES, BIG IMPACT in Num Tab 1 in Quality Page Font Size is not in match with creative - Fail"); 
		    bw.newLine();
		}
				
		if(SmallN1QPFontFamily.equals(ExpSmallN1QPFontFamily))
		{
			System.out.println("SMALL CHANGES, BIG IMPACT in Num Tab 1 in Quality Page Font Family is in match with creative - Pass");
		}
		else
		{
			System.out.println("SMALL CHANGES, BIG IMPACT in Num Tab 1 in Quality Page Font Family is not in match with creative - Fail");
			bw.write("SMALL CHANGES, BIG IMPACT in Num Tab 1 in Quality Page Font Family is not in match with creative - Fail"); 
		    bw.newLine();
		}
		

		if(SmallN1QPContent.equals(ExpSmallN1QPContent))
		{
			System.out.println("SMALL CHANGES, BIG IMPACT in Num Tab 1 Content in Quality Page is in match with creative - Pass");
		}
		else
		{
			System.out.println("SMALL CHANGES, BIG IMPACT in Num Tab 1 Content in Quality Page is not in match with creative - Fail");
			bw.write("SMALL CHANGES, BIG IMPACT in Num Tab 1 Content in Quality Page is not in match with creative - Fail"); 
		    bw.newLine();
		}
		
		if(HexSmallN1QPColor.equals(ExpSmallN1QPColor) && SmallN1QPFontSize.equals(ExpSmallN1QPFontSize) && SmallN1QPFontFamily.equals(ExpSmallN1QPFontFamily) && SmallN1QPContent.equals(ExpSmallN1QPContent))
		{
			Label SmallN1QPCSSP = new Label(5,18,"PASS"); 
			wsheet3.addCell(SmallN1QPCSSP); 
		}
		else
		{
			Label SmallN1QPCSSF = new Label(5,18,"FAIL"); 
			wsheet3.addCell(SmallN1QPCSSF); 
		}
		
		// SMALL CHANGES, BIG IMPACT - GROWING TREES - Num Tab 1
		
		WebElement SmallN1GrowingQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[8]/div/div/div/div/div[3]/div[1]/div[1]/div[4]/div[1]")); 
		String SmallN1GrowingQPColor = SmallN1GrowingQP.getCssValue("color");
		System.out.println("Actual Color of GROWING TREES in Num Tab 1 in Quality Page is : "	+ SmallN1GrowingQPColor );
		// Convert RGB to Hex Value
		Color HSmallN1GrowingQPColor = Color.fromString(SmallN1GrowingQPColor);
		String HexSmallN1GrowingQPColor = HSmallN1GrowingQPColor.asHex();
		System.out.println("Actual Hex Color of GROWING TREES in Num Tab 1 in Quality Page is : "	+ HexSmallN1GrowingQPColor );
		
		String SmallN1GrowingQPFontSize = SmallN1GrowingQP.getCssValue("font-size");
		System.out.println("Actual Font Size of GROWING TREES in Num Tab 1 in Quality Page is : "	+ SmallN1GrowingQPFontSize);
		String SmallN1GrowingQPFontFamily = SmallN1GrowingQP.getCssValue("font-family");
		System.out.println("Actual Font Family of GROWING TREES in Num Tab 1 in Quality Page is : "	+ SmallN1GrowingQPFontFamily);
		String SmallN1GrowingQPContent = SmallN1GrowingQP.getText();
		System.out.println("Actual Content of GROWING TREES in Num Tab 1 in Quality Page is : "	+ SmallN1GrowingQPContent);
				
		String ExpSmallN1GrowingQPColor = sheet3.getCell(1,19).getContents(); //column,row
		System.out.println("Expected Color of GROWING TREES in Num Tab 1 in Quality Page is : "	+ ExpSmallN1GrowingQPColor );
		String ExpSmallN1GrowingQPFontSize = sheet3.getCell(2,19).getContents();
		System.out.println("Expected Font Size of GROWING TREES in Num Tab 1 in Quality Page is : "	+ ExpSmallN1GrowingQPFontSize);
		String ExpSmallN1GrowingQPFontFamily = sheet3.getCell(3,19).getContents();
		System.out.println("Expected Font Family of GROWING TREES in Num Tab 1 in Quality Page is : "	+ ExpSmallN1GrowingQPFontFamily);
		String ExpSmallN1GrowingQPContent = sheet3.getCell(4,19).getContents();
		System.out.println("Expected Content of GROWING TREES in Num Tab 1 in Quality Page is : "	+ ExpSmallN1GrowingQPContent);
				
		if(HexSmallN1GrowingQPColor.equals(ExpSmallN1GrowingQPColor))
		{
			System.out.println("GROWING TREES in Num Tab 1 in Quality Page Color is in match with creative - Pass");
		}
		else
		{
			System.out.println("GROWING TREES in Num Tab 1 in Quality Page Color is not in match with creative - Fail");
			bw.write("GROWING TREES in Num Tab 1 in Quality Page Color is not in match with creative - Fail"); 
		    bw.newLine();
		}
				
		if(SmallN1GrowingQPFontSize.equals(ExpSmallN1GrowingQPFontSize))
		{
			System.out.println("GROWING TREES in Num Tab 1 in Quality Page Font Size is in match with creative - Pass");
		}
		else
		{
			System.out.println("GROWING TREES in Num Tab 1 in Quality Page Font Size is not in match with creative - Fail");
			bw.write("GROWING TREES in Num Tab 1 in Quality Page Font Size is not in match with creative - Fail");  
		    bw.newLine();
		}
				
		if(SmallN1GrowingQPFontFamily.equals(ExpSmallN1GrowingQPFontFamily))
		{
			System.out.println("GROWING TREES in Num Tab 1 in Quality Page Font Family is in match with creative - Pass");
		}
		else
		{
			System.out.println("GROWING TREES in Num Tab 1 in Quality Page Font Family is not in match with creative - Fail");
			bw.write("GROWING TREES in Num Tab 1 in Quality Page Font Family is not in match with creative - Fail"); 
		    bw.newLine();
		}
		

		if(SmallN1GrowingQPContent.equals(ExpSmallN1GrowingQPContent))
		{
			System.out.println("GROWING TREES in Num Tab 1 Content in Quality Page is in match with creative - Pass");
		}
		else
		{
			System.out.println("GROWING TREES in Num Tab 1 Content in Quality Page is not in match with creative - Fail");
			bw.write("GROWING TREES in Num Tab 1 Content in Quality Page is not in match with creative - Fail"); 
		    bw.newLine();
		}
		
		if(HexSmallN1GrowingQPColor.equals(ExpSmallN1GrowingQPColor) && SmallN1GrowingQPFontSize.equals(ExpSmallN1GrowingQPFontSize) && SmallN1GrowingQPFontFamily.equals(ExpSmallN1GrowingQPFontFamily) && SmallN1GrowingQPContent.equals(ExpSmallN1GrowingQPContent))
		{
			Label SmallN1GrowingQPCSSP = new Label(5,19,"PASS"); 
			wsheet3.addCell(SmallN1GrowingQPCSSP); 
		}
		else
		{
			Label SmallN1GrowingQPCSSF = new Label(5,19,"FAIL"); 
			wsheet3.addCell(SmallN1GrowingQPCSSF); 
		}
		
		// SMALL CHANGES, BIG IMPACT - GROWING TREES - Desc -  Num Tab 1
		
		WebElement SmallN1DescQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[8]/div/div/div/div/div[3]/div[1]/div[1]/div[4]/div[2]")); 
		String SmallN1DescQPColor = SmallN1DescQP.getCssValue("color");
		System.out.println("Actual Color of Desc in Num Tab 1 in Quality Page is : "	+ SmallN1DescQPColor );
		// Convert RGB to Hex Value
		Color HSmallN1DescQPColor = Color.fromString(SmallN1DescQPColor);
		String HexSmallN1DescQPColor = HSmallN1DescQPColor.asHex();
		System.out.println("Actual Hex Color of Desc in Num Tab 1 in Quality Page is : "	+ HexSmallN1DescQPColor );
		
		String SmallN1DescQPFontSize = SmallN1DescQP.getCssValue("font-size");
		System.out.println("Actual Font Size of Desc in Num Tab 1 in Quality Page is : "	+ SmallN1DescQPFontSize);
		String SmallN1DescQPFontFamily = SmallN1DescQP.getCssValue("font-family");
		System.out.println("Actual Font Family of Desc in Num Tab 1 in Quality Page is : "	+ SmallN1DescQPFontFamily);
		String SmallN1DescQPContent = SmallN1DescQP.getText();
		System.out.println("Actual Content of Desc in Num Tab 1 in Quality Page is : "	+ SmallN1DescQPContent);
				
		String ExpSmallN1DescQPColor = sheet3.getCell(1,20).getContents(); //column,row
		System.out.println("Expected Color of Desc in Num Tab 1 in Quality Page is : "	+ ExpSmallN1DescQPColor );
		String ExpSmallN1DescQPFontSize = sheet3.getCell(2,20).getContents();
		System.out.println("Expected Font Size of Desc in Num Tab 1 in Quality Page is : "	+ ExpSmallN1DescQPFontSize);
		String ExpSmallN1DescQPFontFamily = sheet3.getCell(3,20).getContents();
		System.out.println("Expected Font Family of Desc in Num Tab 1 in Quality Page is : "	+ ExpSmallN1DescQPFontFamily);
		String ExpSmallN1DescQPContent = sheet3.getCell(4,20).getContents();
		System.out.println("Expected Content of Desc in Num Tab 1 in Quality Page is : "	+ ExpSmallN1DescQPContent);
				
		if(HexSmallN1DescQPColor.equals(ExpSmallN1DescQPColor))
		{
			System.out.println("Desc in Num Tab 1 in Quality Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Desc in Num Tab 1 in Quality Page Color is not in match with creative - Fail");
			bw.write("Desc in Num Tab 1 in Quality Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(SmallN1DescQPFontSize.equals(ExpSmallN1DescQPFontSize))
		{
			System.out.println("Desc in Num Tab 1 in Quality Page Font Size is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("Desc in Num Tab 1 in Quality Page Font Size is not in match with creative - Fail");
			bw.write("Desc in Num Tab 1 in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(SmallN1DescQPFontFamily.equals(ExpSmallN1DescQPFontFamily))
		{
			System.out.println("Desc in Num Tab 1 in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Desc in Num Tab 1 in Quality Page Font Family is not in match with creative - Fail");
			bw.write("Desc in Num Tab 1 in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(SmallN1DescQPContent.equals(ExpSmallN1DescQPContent))
		{
			System.out.println("Desc in Num Tab 1 Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Desc in Num Tab 1 Content in Quality Page is not in match with creative - Fail");
			bw.write("Desc in Num Tab 1 Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexSmallN1DescQPColor.equals(ExpSmallN1DescQPColor) && SmallN1DescQPFontSize.equals(ExpSmallN1DescQPFontSize) && SmallN1DescQPFontFamily.equals(ExpSmallN1DescQPFontFamily) && SmallN1DescQPContent.equals(ExpSmallN1DescQPContent))
		{
			Label SmallN1DescQPCSSP = new Label(5,20,"PASS"); 
			wsheet3.addCell(SmallN1DescQPCSSP); 
		}
		else
		{
			Label SmallN1DescQPCSSF = new Label(5,20,"FAIL"); 
			wsheet3.addCell(SmallN1DescQPCSSF); 
		}
		
		// Click Action on Number Tab 2
		
		Thread.sleep(5000);
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[8]/div/div/div/div/div[3]/div[2]/div[2]")).click(); 
		Thread.sleep(8000);
		
		// SMALL CHANGES, BIG IMPACT - Num Tab 2
		
		WebElement SmallN2QP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[8]/div/div/div/div/div[3]/div[1]/div[2]/div[1]")); 
		String SmallN2QPColor = SmallN2QP.getCssValue("color");
		System.out.println("Actual Color of SMALL CHANGES, BIG IMPACT in Num Tab 2 in Quality Page is : "	+ SmallN2QPColor );
		// Convert RGB to Hex Value
		Color HSmallN2QPColor = Color.fromString(SmallN2QPColor);
		String HexSmallN2QPColor = HSmallN2QPColor.asHex();
		System.out.println("Actual Hex Color of SMALL CHANGES, BIG IMPACT in Num Tab 2 in Quality Page is : "	+ HexSmallN2QPColor );
		
		String SmallN2QPFontSize = SmallN2QP.getCssValue("font-size");
		System.out.println("Actual Font Size of SMALL CHANGES, BIG IMPACT in Num Tab 2 in Quality Page is : "	+ SmallN2QPFontSize);
		String SmallN2QPFontFamily = SmallN2QP.getCssValue("font-family");
		System.out.println("Actual Font Family of SMALL CHANGES, BIG IMPACT in Num Tab 2 in Quality Page is : "	+ SmallN2QPFontFamily);
		String SmallN2QPContent = SmallN2QP.getText();
		System.out.println("Actual Content of SMALL CHANGES, BIG IMPACT in Num Tab 2 in Quality Page is : "	+ SmallN2QPContent);
				
		String ExpSmallN2QPColor = sheet3.getCell(1,21).getContents(); //column,row
		System.out.println("Expected Color of SMALL CHANGES, BIG IMPACT in Num Tab 2 in Quality Page is : "	+ ExpSmallN2QPColor );
		String ExpSmallN2QPFontSize = sheet3.getCell(2,21).getContents();
		System.out.println("Expected Font Size of SMALL CHANGES, BIG IMPACT in Num Tab 2 in Quality Page is : "	+ ExpSmallN2QPFontSize);
		String ExpSmallN2QPFontFamily = sheet3.getCell(3,21).getContents();
		System.out.println("Expected Font Family of SMALL CHANGES, BIG IMPACT in Num Tab 2 in Quality Page is : "	+ ExpSmallN2QPFontFamily);
		String ExpSmallN2QPContent = sheet3.getCell(4,21).getContents();
		System.out.println("Expected Content of SMALL CHANGES, BIG IMPACT in Num Tab 2 in Quality Page is : "	+ ExpSmallN2QPContent);
				
		if(HexSmallN2QPColor.equals(ExpSmallN2QPColor))
		{
			System.out.println("SMALL CHANGES, BIG IMPACT in Num Tab 2 in Quality Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("SMALL CHANGES, BIG IMPACT in Num Tab 2 in Quality Page Color is not in match with creative - Fail");
			bw.write("SMALL CHANGES, BIG IMPACT in Num Tab 2 in Quality Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(SmallN2QPFontSize.equals(ExpSmallN2QPFontSize))
		{
			System.out.println("SMALL CHANGES, BIG IMPACT in Num Tab 2 in Quality Page Font Size is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("SMALL CHANGES, BIG IMPACT in Num Tab 2 in Quality Page Font Size is not in match with creative - Fail");
			bw.write("SMALL CHANGES, BIG IMPACT in Num Tab 2 in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(SmallN2QPFontFamily.equals(ExpSmallN2QPFontFamily))
		{
			System.out.println("SMALL CHANGES, BIG IMPACT in Num Tab 2 in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("SMALL CHANGES, BIG IMPACT in Num Tab 2 in Quality Page Font Family is not in match with creative - Fail");
			bw.write("SMALL CHANGES, BIG IMPACT in Num Tab 2 in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(SmallN2QPContent.equals(ExpSmallN2QPContent))
		{
			System.out.println("SMALL CHANGES, BIG IMPACT in Num Tab 2 Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("SMALL CHANGES, BIG IMPACT in Num Tab 2 Content in Quality Page is not in match with creative - Fail");
			bw.write("SMALL CHANGES, BIG IMPACT in Num Tab 2 Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexSmallN2QPColor.equals(ExpSmallN2QPColor) && SmallN2QPFontSize.equals(ExpSmallN2QPFontSize) && SmallN2QPFontFamily.equals(ExpSmallN2QPFontFamily) && SmallN2QPContent.equals(ExpSmallN2QPContent))
		{
			Label SmallN2QPCSSP = new Label(5,21,"PASS"); 
			wsheet3.addCell(SmallN2QPCSSP); 
		}
		else
		{
			Label SmallN2QPCSSF = new Label(5,21,"FAIL"); 
			wsheet3.addCell(SmallN2QPCSSF); 
		}
		
		// SMALL CHANGES, BIG IMPACT - HARVESTING - Num Tab 2
		
		WebElement SmallN2HarvestingQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[8]/div/div/div/div/div[3]/div[1]/div[2]/div[4]/div[1]")); 
		String SmallN2HarvestingQPColor = SmallN2HarvestingQP.getCssValue("color");
		System.out.println("Actual Color of HARVESTING in Num Tab 2 in Quality Page is : "	+ SmallN2HarvestingQPColor );
		// Convert RGB to Hex Value
		Color HSmallN2HarvestingQPColor = Color.fromString(SmallN2HarvestingQPColor);
		String HexSmallN2HarvestingQPColor = HSmallN2HarvestingQPColor.asHex();
		System.out.println("Actual Hex Color of HARVESTING in Num Tab 2 in Quality Page is : "	+ HexSmallN2HarvestingQPColor );
		
		String SmallN2HarvestingQPFontSize = SmallN2HarvestingQP.getCssValue("font-size");
		System.out.println("Actual Font Size of HARVESTING in Num Tab 2 in Quality Page is : "	+ SmallN2HarvestingQPFontSize);
		String SmallN2HarvestingQPFontFamily = SmallN2HarvestingQP.getCssValue("font-family");
		System.out.println("Actual Font Family of HARVESTING in Num Tab 2 in Quality Page is : "	+ SmallN2HarvestingQPFontFamily);
		String SmallN2HarvestingQPContent = SmallN2HarvestingQP.getText();
		System.out.println("Actual Content of HARVESTING in Num Tab 2 in Quality Page is : "	+ SmallN2HarvestingQPContent);
				
		String ExpSmallN2HarvestingQPColor = sheet3.getCell(1,22).getContents(); //column,row
		System.out.println("Expected Color of HARVESTING in Num Tab 2 in Quality Page is : "	+ ExpSmallN2HarvestingQPColor );
		String ExpSmallN2HarvestingQPFontSize = sheet3.getCell(2,22).getContents();
		System.out.println("Expected Font Size of HARVESTING in Num Tab 2 in Quality Page is : "	+ ExpSmallN2HarvestingQPFontSize);
		String ExpSmallN2HarvestingQPFontFamily = sheet3.getCell(3,22).getContents();
		System.out.println("Expected Font Family of HARVESTING in Num Tab 2 in Quality Page is : "	+ ExpSmallN2HarvestingQPFontFamily);
		String ExpSmallN2HarvestingQPContent = sheet3.getCell(4,22).getContents();
		System.out.println("Expected Content of HARVESTING in Num Tab 2 in Quality Page is : "	+ ExpSmallN2HarvestingQPContent);
				
		if(HexSmallN2HarvestingQPColor.equals(ExpSmallN2HarvestingQPColor))
		{
			System.out.println("HARVESTING in Num Tab 2 in Quality Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("HARVESTING in Num Tab 2 in Quality Page Color is not in match with creative - Fail");
			bw.write("HARVESTING in Num Tab 2 in Quality Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(SmallN2HarvestingQPFontSize.equals(ExpSmallN2HarvestingQPFontSize))
		{
			System.out.println("HARVESTING in Num Tab 2 in Quality Page Font Size is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("HARVESTING in Num Tab 2 in Quality Page Font Size is not in match with creative - Fail");
			bw.write("HARVESTING in Num Tab 2 in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(SmallN2HarvestingQPFontFamily.equals(ExpSmallN2HarvestingQPFontFamily))
		{
			System.out.println("HARVESTING in Num Tab 2 in Quality Page Font Family is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("HARVESTING in Num Tab 2 in Quality Page Font Family is not in match with creative - Fail");
			bw.write("HARVESTING in Num Tab 2 in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(SmallN2HarvestingQPContent.equals(ExpSmallN2HarvestingQPContent))
		{
			System.out.println("HARVESTING in Num Tab 2 Content in Quality Page is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("HARVESTING in Num Tab 2 Content in Quality Page is not in match with creative - Fail");
			bw.write("HARVESTING in Num Tab 2 Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexSmallN2HarvestingQPColor.equals(ExpSmallN2HarvestingQPColor) && SmallN2HarvestingQPFontSize.equals(ExpSmallN2HarvestingQPFontSize) && SmallN2HarvestingQPFontFamily.equals(ExpSmallN2HarvestingQPFontFamily) && SmallN2HarvestingQPContent.equals(ExpSmallN2HarvestingQPContent))
		{
			Label SmallN2HarvestingQPCSSP = new Label(5,22,"PASS"); 
			wsheet3.addCell(SmallN2HarvestingQPCSSP); 
		}
		else
		{
			Label SmallN2HarvestingQPCSSF = new Label(5,22,"FAIL"); 
			wsheet3.addCell(SmallN2HarvestingQPCSSF); 
		}
		
		// SMALL CHANGES, BIG IMPACT - HARVESTING - Desc -  Num Tab 2
		
		WebElement SmallN2DescQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[8]/div/div/div/div/div[3]/div[1]/div[2]/div[4]/div[2]")); 
		String SmallN2DescQPColor = SmallN2DescQP.getCssValue("color");
		System.out.println("Actual Color of Desc in Num Tab 2 in Quality Page is : "	+ SmallN2DescQPColor );
		// Convert RGB to Hex Value
		Color HSmallN2DescQPColor = Color.fromString(SmallN2DescQPColor);
		String HexSmallN2DescQPColor = HSmallN2DescQPColor.asHex();
		System.out.println("Actual Hex Color of Desc in Num Tab 2 in Quality Page is : "	+ HexSmallN2DescQPColor );
		
		String SmallN2DescQPFontSize = SmallN2DescQP.getCssValue("font-size");
		System.out.println("Actual Font Size of Desc in Num Tab 2 in Quality Page is : "	+ SmallN2DescQPFontSize);
		String SmallN2DescQPFontFamily = SmallN2DescQP.getCssValue("font-family");
		System.out.println("Actual Font Family of Desc in Num Tab 2 in Quality Page is : "	+ SmallN2DescQPFontFamily);
		String SmallN2DescQPContent = SmallN2DescQP.getText();
		System.out.println("Actual Content of Desc in Num Tab 2 in Quality Page is : "	+ SmallN2DescQPContent);
				
		String ExpSmallN2DescQPColor = sheet3.getCell(1,23).getContents(); //column,row
		System.out.println("Expected Color of Desc in Num Tab 2 in Quality Page is : "	+ ExpSmallN2DescQPColor );
		String ExpSmallN2DescQPFontSize = sheet3.getCell(2,23).getContents();
		System.out.println("Expected Font Size of Desc in Num Tab 2 in Quality Page is : "	+ ExpSmallN2DescQPFontSize);
		String ExpSmallN2DescQPFontFamily = sheet3.getCell(3,23).getContents();
		System.out.println("Expected Font Family of Desc in Num Tab 2 in Quality Page is : "	+ ExpSmallN2DescQPFontFamily);
		String ExpSmallN2DescQPContent = sheet3.getCell(4,23).getContents();
		System.out.println("Expected Content of Desc in Num Tab 2 in Quality Page is : "	+ ExpSmallN2DescQPContent);
				
		if(HexSmallN2DescQPColor.equals(ExpSmallN2DescQPColor))
		{
			System.out.println("Desc in Num Tab 2 in Quality Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Desc in Num Tab 2 in Quality Page Color is not in match with creative - Fail");
			bw.write("Desc in Num Tab 2 in Quality Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(SmallN2DescQPFontSize.equals(ExpSmallN2DescQPFontSize))
		{
			System.out.println("Desc in Num Tab 2 in Quality Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Desc in Num Tab 2 in Quality Page Font Size is not in match with creative - Fail");
			bw.write("Desc in Num Tab 2 in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(SmallN2DescQPFontFamily.equals(ExpSmallN2DescQPFontFamily))
		{
			System.out.println("Desc in Num Tab 2 in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Desc in Num Tab 2 in Quality Page Font Family is not in match with creative - Fail");
			bw.write("Desc in Num Tab 2 in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(SmallN2DescQPContent.equals(ExpSmallN2DescQPContent))
		{
			System.out.println("Desc in Num Tab 2 Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Desc in Num Tab 2 Content in Quality Page is not in match with creative - Fail");
			bw.write("Desc in Num Tab 2 Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexSmallN2DescQPColor.equals(ExpSmallN2DescQPColor) && SmallN2DescQPFontSize.equals(ExpSmallN2DescQPFontSize) && SmallN2DescQPFontFamily.equals(ExpSmallN2DescQPFontFamily) && SmallN2DescQPContent.equals(ExpSmallN2DescQPContent))
		{
			Label SmallN2DescQPCSSP = new Label(5,23,"PASS"); 
			wsheet3.addCell(SmallN2DescQPCSSP); 
		}
		else
		{
			Label SmallN2DescQPCSSF = new Label(5,23,"FAIL"); 
			wsheet3.addCell(SmallN2DescQPCSSF); 
		}
		
		//Click Action on Number Tab 3
		
		Thread.sleep(5000);
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[8]/div/div/div/div/div[3]/div[2]/div[3]")).click(); 
		Thread.sleep(8000);
		
		// SMALL CHANGES, BIG IMPACT - Num Tab 3
		
		WebElement SmallN3QP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[8]/div/div/div/div/div[3]/div[1]/div[3]/div[1]")); 
		String SmallN3QPColor = SmallN3QP.getCssValue("color");
		System.out.println("Actual Color of SMALL CHANGES, BIG IMPACT in Num Tab 3 in Quality Page is : "	+ SmallN3QPColor );
		// Convert RGB to Hex Value
		Color HSmallN3QPColor = Color.fromString(SmallN3QPColor);
		String HexSmallN3QPColor = HSmallN3QPColor.asHex();
		System.out.println("Actual Hex Color of SMALL CHANGES, BIG IMPACT in Num Tab 3 in Quality Page is : "	+ HexSmallN3QPColor );
		
		String SmallN3QPFontSize = SmallN3QP.getCssValue("font-size");
		System.out.println("Actual Font Size of SMALL CHANGES, BIG IMPACT in Num Tab 3 in Quality Page is : "	+ SmallN3QPFontSize);
		String SmallN3QPFontFamily = SmallN3QP.getCssValue("font-family");
		System.out.println("Actual Font Family of SMALL CHANGES, BIG IMPACT in Num Tab 3 in Quality Page is : "	+ SmallN3QPFontFamily);
		String SmallN3QPContent = SmallN3QP.getText();
		System.out.println("Actual Content of SMALL CHANGES, BIG IMPACT in Num Tab 3 in Quality Page is : "	+ SmallN3QPContent);
				
		String ExpSmallN3QPColor = sheet3.getCell(1,24).getContents(); //column,row
		System.out.println("Expected Color of SMALL CHANGES, BIG IMPACT in Num Tab 3 in Quality Page is : "	+ ExpSmallN3QPColor );
		String ExpSmallN3QPFontSize = sheet3.getCell(2,24).getContents();
		System.out.println("Expected Font Size of SMALL CHANGES, BIG IMPACT in Num Tab 3 in Quality Page is : "	+ ExpSmallN3QPFontSize);
		String ExpSmallN3QPFontFamily = sheet3.getCell(3,24).getContents();
		System.out.println("Expected Font Family of SMALL CHANGES, BIG IMPACT in Num Tab 3 in Quality Page is : "	+ ExpSmallN3QPFontFamily);
		String ExpSmallN3QPContent = sheet3.getCell(4,24).getContents();
		System.out.println("Expected Content of SMALL CHANGES, BIG IMPACT in Num Tab 3 in Quality Page is : "	+ ExpSmallN3QPContent);
				
		if(HexSmallN3QPColor.equals(ExpSmallN3QPColor))
		{
			System.out.println("SMALL CHANGES, BIG IMPACT in Num Tab 3 in Quality Page Color is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("SMALL CHANGES, BIG IMPACT in Num Tab 3 in Quality Page Color is not in match with creative - Fail");
			bw.write("SMALL CHANGES, BIG IMPACT in Num Tab 3 in Quality Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(SmallN3QPFontSize.equals(ExpSmallN3QPFontSize))
		{
			System.out.println("SMALL CHANGES, BIG IMPACT in Num Tab 3 in Quality Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("SMALL CHANGES, BIG IMPACT in Num Tab 3 in Quality Page Font Size is not in match with creative - Fail");
			bw.write("SMALL CHANGES, BIG IMPACT in Num Tab 3 in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(SmallN3QPFontFamily.equals(ExpSmallN3QPFontFamily))
		{
			System.out.println("SMALL CHANGES, BIG IMPACT in Num Tab 3 in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("SMALL CHANGES, BIG IMPACT in Num Tab 3 in Quality Page Font Family is not in match with creative - Fail");
			bw.write("SMALL CHANGES, BIG IMPACT in Num Tab 3 in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(SmallN3QPContent.equals(ExpSmallN3QPContent))
		{
			System.out.println("SMALL CHANGES, BIG IMPACT in Num Tab 3 Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("SMALL CHANGES, BIG IMPACT in Num Tab 3 Content in Quality Page is not in match with creative - Fail");
			bw.write("SMALL CHANGES, BIG IMPACT in Num Tab 3 Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexSmallN3QPColor.equals(ExpSmallN3QPColor) && SmallN3QPFontSize.equals(ExpSmallN3QPFontSize) && SmallN3QPFontFamily.equals(ExpSmallN3QPFontFamily) && SmallN3QPContent.equals(ExpSmallN3QPContent))
		{
			Label SmallN3QPCSSP = new Label(5,24,"PASS"); 
			wsheet3.addCell(SmallN3QPCSSP); 
		}
		else
		{
			Label SmallN3QPCSSF = new Label(5,24,"FAIL"); 
			wsheet3.addCell(SmallN3QPCSSF); 
		}
		
		// SMALL CHANGES, BIG IMPACT - ORGANIC FARMING - Num Tab 3
		
		WebElement SmallN3OrganicQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[8]/div/div/div/div/div[3]/div[1]/div[3]/div[4]/div[1]")); 
		String SmallN3OrganicQPColor = SmallN3OrganicQP.getCssValue("color");
		System.out.println("Actual Color of ORGANIC FARMING in Num Tab 3 in Quality Page is : "	+ SmallN3OrganicQPColor );
		// Convert RGB to Hex Value
		Color HSmallN3OrganicQPColor = Color.fromString(SmallN3OrganicQPColor);
		String HexSmallN3OrganicQPColor = HSmallN3OrganicQPColor.asHex();
		System.out.println("Actual Hex Color of ORGANIC FARMING in Num Tab 3 in Quality Page is : "	+ HexSmallN3OrganicQPColor );
		
		String SmallN3OrganicQPFontSize = SmallN3OrganicQP.getCssValue("font-size");
		System.out.println("Actual Font Size of ORGANIC FARMING in Num Tab 3 in Quality Page is : "	+ SmallN3OrganicQPFontSize);
		String SmallN3OrganicQPFontFamily = SmallN3OrganicQP.getCssValue("font-family");
		System.out.println("Actual Font Family of ORGANIC FARMING in Num Tab 3 in Quality Page is : "	+ SmallN3OrganicQPFontFamily);
		String SmallN3OrganicQPContent = SmallN3OrganicQP.getText();
		System.out.println("Actual Content of ORGANIC FARMING in Num Tab 3 in Quality Page is : "	+ SmallN3OrganicQPContent);
				
		String ExpSmallN3OrganicQPColor = sheet3.getCell(1,25).getContents(); //column,row
		System.out.println("Expected Color of ORGANIC FARMING in Num Tab 3 in Quality Page is : "	+ ExpSmallN3OrganicQPColor );
		String ExpSmallN3OrganicQPFontSize = sheet3.getCell(2,25).getContents();
		System.out.println("Expected Font Size of ORGANIC FARMING in Num Tab 3 in Quality Page is : "	+ ExpSmallN3OrganicQPFontSize);
		String ExpSmallN3OrganicQPFontFamily = sheet3.getCell(3,25).getContents();
		System.out.println("Expected Font Family of ORGANIC FARMING in Num Tab 3 in Quality Page is : "	+ ExpSmallN3OrganicQPFontFamily);
		String ExpSmallN3OrganicQPContent = sheet3.getCell(4,25).getContents();
		System.out.println("Expected Content of ORGANIC FARMING in Num Tab 3 in Quality Page is : "	+ ExpSmallN3OrganicQPContent);
				
		if(HexSmallN3OrganicQPColor.equals(ExpSmallN3OrganicQPColor))
		{
			System.out.println("ORGANIC FARMING in Num Tab 3 in Quality Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("ORGANIC FARMING in Num Tab 3 in Quality Page Color is not in match with creative - Fail");
			bw.write("ORGANIC FARMING in Num Tab 3 in Quality Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(SmallN3OrganicQPFontSize.equals(ExpSmallN3OrganicQPFontSize))
		{
			System.out.println("ORGANIC FARMING in Num Tab 3 in Quality Page Font Size is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("ORGANIC FARMING in Num Tab 3 in Quality Page Font Size is not in match with creative - Fail");
			bw.write("ORGANIC FARMING in Num Tab 3 in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(SmallN3OrganicQPFontFamily.equals(ExpSmallN3OrganicQPFontFamily))
		{
			System.out.println("ORGANIC FARMING in Num Tab 3 in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("ORGANIC FARMING in Num Tab 3 in Quality Page Font Family is not in match with creative - Fail");
			bw.write("ORGANIC FARMING in Num Tab 3 in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(SmallN3OrganicQPContent.equals(ExpSmallN3OrganicQPContent))
		{
			System.out.println("ORGANIC FARMING in Num Tab 3 Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("ORGANIC FARMING in Num Tab 3 Content in Quality Page is not in match with creative - Fail");
			bw.write("ORGANIC FARMING in Num Tab 3 Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexSmallN3OrganicQPColor.equals(ExpSmallN3OrganicQPColor) && SmallN3OrganicQPFontSize.equals(ExpSmallN3OrganicQPFontSize) && SmallN3OrganicQPFontFamily.equals(ExpSmallN3OrganicQPFontFamily) && SmallN3OrganicQPContent.equals(ExpSmallN3OrganicQPContent))
		{
			Label SmallN3OrganicQPCSSP = new Label(5,25,"PASS"); 
			wsheet3.addCell(SmallN3OrganicQPCSSP); 
		}
		else
		{
			Label SmallN3OrganicQPCSSF = new Label(5,25,"FAIL"); 
			wsheet3.addCell(SmallN3OrganicQPCSSF); 
		}
		
		// SMALL CHANGES, BIG IMPACT - ORGANIC FARMING - Desc -  Num Tab 3
		
		WebElement SmallN3DescQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[8]/div/div/div/div/div[3]/div[1]/div[3]/div[4]/div[2]")); 
		String SmallN3DescQPColor = SmallN3DescQP.getCssValue("color");
		System.out.println("Actual Color of Desc in Num Tab 3 in Quality Page is : "	+ SmallN3DescQPColor );
		// Convert RGB to Hex Value
		Color HSmallN3DescQPColor = Color.fromString(SmallN3DescQPColor);
		String HexSmallN3DescQPColor = HSmallN3DescQPColor.asHex();
		System.out.println("Actual Hex Color of Desc in Num Tab 3 in Quality Page is : "	+ HexSmallN3DescQPColor );
		
		String SmallN3DescQPFontSize = SmallN3DescQP.getCssValue("font-size");
		System.out.println("Actual Font Size of Desc in Num Tab 3 in Quality Page is : "	+ SmallN3DescQPFontSize);
		String SmallN3DescQPFontFamily = SmallN3DescQP.getCssValue("font-family");
		System.out.println("Actual Font Family of Desc in Num Tab 3 in Quality Page is : "	+ SmallN3DescQPFontFamily);
		String SmallN3DescQPContent = SmallN3DescQP.getText();
		System.out.println("Actual Content of Desc in Num Tab 3 in Quality Page is : "	+ SmallN3DescQPContent);
				
		String ExpSmallN3DescQPColor = sheet3.getCell(1,26).getContents(); //column,row
		System.out.println("Expected Color of Desc in Num Tab 3 in Quality Page is : "	+ ExpSmallN3DescQPColor );
		String ExpSmallN3DescQPFontSize = sheet3.getCell(2,26).getContents();
		System.out.println("Expected Font Size of Desc in Num Tab 3 in Quality Page is : "	+ ExpSmallN3DescQPFontSize);
		String ExpSmallN3DescQPFontFamily = sheet3.getCell(3,26).getContents();
		System.out.println("Expected Font Family of Desc in Num Tab 3 in Quality Page is : "	+ ExpSmallN3DescQPFontFamily);
		String ExpSmallN3DescQPContent = sheet3.getCell(4,26).getContents();
		System.out.println("Expected Content of Desc in Num Tab 3 in Quality Page is : "	+ ExpSmallN3DescQPContent);
				
		if(HexSmallN3DescQPColor.equals(ExpSmallN3DescQPColor))
		{
			System.out.println("Desc in Num Tab 3 in Quality Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Desc in Num Tab 3 in Quality Page Color is not in match with creative - Fail");
			bw.write("Desc in Num Tab 3 in Quality Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(SmallN3DescQPFontSize.equals(ExpSmallN3DescQPFontSize))
		{
			System.out.println("Desc in Num Tab 3 in Quality Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Desc in Num Tab 3 in Quality Page Font Size is not in match with creative - Fail");
			bw.write("Desc in Num Tab 3 in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(SmallN3DescQPFontFamily.equals(ExpSmallN3DescQPFontFamily))
		{
			System.out.println("Desc in Num Tab 3 in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Desc in Num Tab 3 in Quality Page Font Family is not in match with creative - Fail");
			bw.write("Desc in Num Tab 3 in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(SmallN3DescQPContent.equals(ExpSmallN3DescQPContent))
		{
			System.out.println("Desc in Num Tab 3 Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Desc in Num Tab 3 Content in Quality Page is not in match with creative - Fail");
			bw.write("Desc in Num Tab 3 Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexSmallN3DescQPColor.equals(ExpSmallN3DescQPColor) && SmallN3DescQPFontSize.equals(ExpSmallN3DescQPFontSize) && SmallN3DescQPFontFamily.equals(ExpSmallN3DescQPFontFamily) && SmallN3DescQPContent.equals(ExpSmallN3DescQPContent))
		{
			Label SmallN3DescQPCSSP = new Label(5,26,"PASS"); 
			wsheet3.addCell(SmallN3DescQPCSSP); 
		}
		else
		{
			Label SmallN3DescQPCSSF = new Label(5,26,"FAIL"); 
			wsheet3.addCell(SmallN3DescQPCSSF); 
		}
		
		// CAN YOU SPOT THE IMPERFECT BEAN?
		
		WebElement CanQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[9]/div/div/div/div/div[3]/div[2]/div[2]")); 
		String CanQPColor = CanQP.getCssValue("color");
		System.out.println("Actual Color of CAN YOU SPOT THE IMPERFECT BEAN? in Quality Page is : "	+ CanQPColor );
		// Convert RGB to Hex Value
		Color HCanQPColor = Color.fromString(CanQPColor);
		String HexCanQPColor = HCanQPColor.asHex();
		System.out.println("Actual Hex Color of CAN YOU SPOT THE IMPERFECT BEAN? in Quality Page is : "	+ HexCanQPColor );
		
		String CanQPFontSize = CanQP.getCssValue("font-size");
		System.out.println("Actual Font Size of CAN YOU SPOT THE IMPERFECT BEAN? in Quality Page is : "	+ CanQPFontSize);
		String CanQPFontFamily = CanQP.getCssValue("font-family");
		System.out.println("Actual Font Family of CAN YOU SPOT THE IMPERFECT BEAN? in Quality Page is : "	+ CanQPFontFamily);
		String CanQPContent = CanQP.getText();
		System.out.println("Actual Content of CAN YOU SPOT THE IMPERFECT BEAN? in Quality Page is : "	+ CanQPContent);
				
		String ExpCanQPColor = sheet3.getCell(1,27).getContents(); //column,row
		System.out.println("Expected Color of CAN YOU SPOT THE IMPERFECT BEAN? in Quality Page is : "	+ ExpCanQPColor );
		String ExpCanQPFontSize = sheet3.getCell(2,27).getContents();
		System.out.println("Expected Font Size of CAN YOU SPOT THE IMPERFECT BEAN? in Quality Page is : "	+ ExpCanQPFontSize);
		String ExpCanQPFontFamily = sheet3.getCell(3,27).getContents();
		System.out.println("Expected Font Family of CAN YOU SPOT THE IMPERFECT BEAN? in Quality Page is : "	+ ExpCanQPFontFamily);
		String ExpCanQPContent = sheet3.getCell(4,27).getContents();
		System.out.println("Expected Content of CAN YOU SPOT THE IMPERFECT BEAN? in Quality Page is : "	+ ExpCanQPContent);
				
		if(HexCanQPColor.equals(ExpCanQPColor))
		{
			System.out.println("CAN YOU SPOT THE IMPERFECT BEAN? in Quality Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("CAN YOU SPOT THE IMPERFECT BEAN? in Quality Page Color is not in match with creative - Fail");
			bw.write("CAN YOU SPOT THE IMPERFECT BEAN? in Quality Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(CanQPFontSize.equals(ExpCanQPFontSize))
		{
			System.out.println("CAN YOU SPOT THE IMPERFECT BEAN? in Quality Page Font Size is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("CAN YOU SPOT THE IMPERFECT BEAN? in Quality Page Font Size is not in match with creative - Fail");
			bw.write("CAN YOU SPOT THE IMPERFECT BEAN? in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(CanQPFontFamily.equals(ExpCanQPFontFamily))
		{
			System.out.println("CAN YOU SPOT THE IMPERFECT BEAN? in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("CAN YOU SPOT THE IMPERFECT BEAN? in Quality Page Font Family is not in match with creative - Fail");
			bw.write("CAN YOU SPOT THE IMPERFECT BEAN? in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(CanQPContent.equals(ExpCanQPContent))
		{
			System.out.println("CAN YOU SPOT THE IMPERFECT BEAN? Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("CAN YOU SPOT THE IMPERFECT BEAN? Content in Quality Page is not in match with creative - Fail");
			bw.write("CAN YOU SPOT THE IMPERFECT BEAN? Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexCanQPColor.equals(ExpCanQPColor) && CanQPFontSize.equals(ExpCanQPFontSize) && CanQPFontFamily.equals(ExpCanQPFontFamily) && CanQPContent.equals(ExpCanQPContent))
		{
			Label CanQPCSSP = new Label(5,27,"PASS"); 
			wsheet3.addCell(CanQPCSSP); 
		}
		else
		{
			Label CanQPCSSF = new Label(5,27,"FAIL"); 
			wsheet3.addCell(CanQPCSSF); 
		}

		Thread.sleep(3000);
		JavascriptExecutor jsehandle = (JavascriptExecutor)driver;
		jsehandle.executeScript("window.scrollBy(0,500)", "");
		Thread.sleep(8000);
	
		// Quality Page - HANDLED  
		
		WebElement QualityHandledQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[9]/div/div/div/div/div[2]/div/div[1]")); 
		
		String QualityHandledQPFontSize = QualityHandledQP.getCssValue("font-size");
		System.out.println("Actual Font Size of HANDLED Text in Quality Page is : "	+ QualityHandledQPFontSize);
		String QualityHandledQPFontFamily = QualityHandledQP.getCssValue("font-family");
		System.out.println("Actual Font Family of HANDLED Text in Quality Page is : "	+ QualityHandledQPFontFamily);
		String QualityHandledQPContent = QualityHandledQP.getText();
		System.out.println("Actual Content of HANDLED Text in Quality Page is : "	+ QualityHandledQPContent);
				
		String ExpQualityHandledQPFontSize = sheet3.getCell(2,28).getContents();
		System.out.println("Expected Font Size of HANDLED Text in Quality Page is : "	+ ExpQualityHandledQPFontSize);
		String ExpQualityHandledQPFontFamily = sheet3.getCell(3,28).getContents();
		System.out.println("Expected Font Family of HANDLED Text in Quality Page is : "	+ ExpQualityHandledQPFontFamily);
		String ExpQualityHandledQPContent = sheet3.getCell(4,28).getContents();
		System.out.println("Expected Content of HANDLED Text in Quality Page is : "	+ ExpQualityHandledQPContent);
				
				
		if(QualityHandledQPFontSize.equals(ExpQualityHandledQPFontSize))
		{
			System.out.println("HANDLED Text in Quality Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("HANDLED Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("HANDLED Text in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(QualityHandledQPFontFamily.equals(ExpQualityHandledQPFontFamily))
		{
			System.out.println("HANDLED Text in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("HANDLED Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("HANDLED Text in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(QualityHandledQPContent.equals(ExpQualityHandledQPContent))
		{
			System.out.println("HANDLED Text Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("HANDLED Text Content in Quality Page is not in match with creative - Fail");
			bw.write("HANDLED Text Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(QualityHandledQPFontSize.equals(ExpQualityHandledQPFontSize) && QualityHandledQPFontFamily.equals(ExpQualityHandledQPFontFamily) && QualityHandledQPContent.equals(ExpQualityHandledQPContent))
		{
			Label QualityHandledQPCSSP = new Label(5,28,"PASS"); 
			wsheet3.addCell(QualityHandledQPCSSP); 
		}
		else
		{
			Label QualityHandledQPCSSF = new Label(5,28,"FAIL"); 
			wsheet3.addCell(QualityHandledQPCSSF); 
		}
		
		// Quality Page - WITH CARE  
		
		WebElement QualitywithcareQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[9]/div/div/div/div/div[2]/div/div[2]")); 
		
		String QualitywithcareQPFontSize = QualitywithcareQP.getCssValue("font-size");
		System.out.println("Actual Font Size of WITH CARE Text in Quality Page is : "	+ QualitywithcareQPFontSize);
		String QualitywithcareQPFontFamily = QualitywithcareQP.getCssValue("font-family");
		System.out.println("Actual Font Family of WITH CARE Text in Quality Page is : "	+ QualitywithcareQPFontFamily);
		String QualitywithcareQPContent = QualitywithcareQP.getText();
		System.out.println("Actual Content of WITH CARE Text in Quality Page is : "	+ QualitywithcareQPContent);
				
		String ExpQualitywithcareQPFontSize = sheet3.getCell(2,29).getContents();
		System.out.println("Expected Font Size of WITH CARE Text in Quality Page is : "	+ ExpQualitywithcareQPFontSize);
		String ExpQualitywithcareQPFontFamily = sheet3.getCell(3,29).getContents();
		System.out.println("Expected Font Family of WITH CARE Text in Quality Page is : "	+ ExpQualitywithcareQPFontFamily);
		String ExpQualitywithcareQPContent = sheet3.getCell(4,29).getContents();
		System.out.println("Expected Content of WITH CARE Text in Quality Page is : "	+ ExpQualitywithcareQPContent);
						
		if(QualitywithcareQPFontSize.equals(ExpQualitywithcareQPFontSize))
		{
			System.out.println("WITH CARE Text in Quality Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("WITH CARE Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("WITH CARE Text in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(QualitywithcareQPFontFamily.equals(ExpQualitywithcareQPFontFamily))
		{
			System.out.println("WITH CARE Text in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("WITH CARE Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("WITH CARE Text in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(QualitywithcareQPContent.equals(ExpQualitywithcareQPContent))
		{
			System.out.println("WITH CARE Text Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("WITH CARE Text Content in Quality Page is not in match with creative - Fail");
			bw.write("WITH CARE Text Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(QualitywithcareQPFontSize.equals(ExpQualitywithcareQPFontSize) && QualitywithcareQPFontFamily.equals(ExpQualitywithcareQPFontFamily) && QualitywithcareQPContent.equals(ExpQualitywithcareQPContent))
		{
			Label QualitywithcareQPCSSP = new Label(5,29,"PASS"); 
			wsheet3.addCell(QualitywithcareQPCSSP); 
		}
		else
		{
			Label QualitywithcareQPCSSF = new Label(5,29,"FAIL"); 
			wsheet3.addCell(QualitywithcareQPCSSF); 
		}
		
		// Quality Page - Handled With Care Desc
		
		WebElement HandledDescQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[9]/div/div/div/div/div[2]/div/div[4]")); 
		
		String HandledDescQPFontSize = HandledDescQP.getCssValue("font-size");
		System.out.println("Actual Font Size of Handled With Care Desc in Quality Page is : "	+ HandledDescQPFontSize);
		String HandledDescQPFontFamily = HandledDescQP.getCssValue("font-family");
		System.out.println("Actual Font Family of Handled With Care Desc in Quality Page is : "	+ HandledDescQPFontFamily);
		String HandledDescQPContent = HandledDescQP.getText();
		System.out.println("Actual Content of Handled With Care Desc in Quality Page is : "	+ HandledDescQPContent);
				
		String ExpHandledDescQPFontSize = sheet3.getCell(2,30).getContents();
		System.out.println("Expected Font Size of Handled With Care Desc in Quality Page is : "	+ ExpHandledDescQPFontSize);
		String ExpHandledDescQPFontFamily = sheet3.getCell(3,30).getContents();
		System.out.println("Expected Font Family of Handled With Care Desc in Quality Page is : "	+ ExpHandledDescQPFontFamily);
		String ExpHandledDescQPContent = sheet3.getCell(4,30).getContents();
		System.out.println("Expected Content of Handled With Care Desc in Quality Page is : "	+ ExpHandledDescQPContent);
				
		if(HandledDescQPFontSize.equals(ExpHandledDescQPFontSize))
		{
			System.out.println("Handled With Care Desc in Quality Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Handled With Care Desc in Quality Page Font Size is not in match with creative - Fail");
			bw.write("Handled With Care Desc in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(HandledDescQPFontFamily.equals(ExpHandledDescQPFontFamily))
		{
			System.out.println("Handled With Care Desc in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Handled With Care Desc in Quality Page Font Family is not in match with creative - Fail");
			bw.write("Handled With Care Desc in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(HandledDescQPContent.equals(ExpHandledDescQPContent))
		{
			System.out.println("Handled With Care Desc Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Handled With Care Desc Content in Quality Page is not in match with creative - Fail");
			bw.write("Handled With Care Desc Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HandledDescQPFontSize.equals(ExpHandledDescQPFontSize) && HandledDescQPFontFamily.equals(ExpHandledDescQPFontFamily) && HandledDescQPContent.equals(ExpHandledDescQPContent))
		{
			Label HandledDescQPCSSP = new Label(5,30,"PASS"); 
			wsheet3.addCell(HandledDescQPCSSP); 
		}
		else
		{
			Label HandledDescQPCSSF = new Label(5,30,"FAIL"); 
			wsheet3.addCell(HandledDescQPCSSF); 
		}
		
		Thread.sleep(3000);
		JavascriptExecutor jsemaking = (JavascriptExecutor)driver;
		jsemaking.executeScript("window.scrollBy(0,500)", "");
		Thread.sleep(8000);
	
		// Quality Page TEST 02
		
		WebElement Test02QP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[9]/div/div/div/div/div[6]/div/div[1]/div[1]")); 
		String Test02QPColor = Test02QP.getCssValue("color");
		System.out.println("Actual Color of TEST 02 Text in Quality Page is : "	+ Test02QPColor );
		// Convert RGB to Hex Value
		Color HTest02QPColor = Color.fromString(Test02QPColor);
		String HexTest02QPColor = HTest02QPColor.asHex();
		System.out.println("Actual Hex Color of TEST 02 Text in Quality Page is : "	+ HexTest02QPColor );
		
		String Test02QPFontSize = Test02QP.getCssValue("font-size");
		System.out.println("Actual Font Size of TEST 02 Text in Quality Page is : "	+ Test02QPFontSize);
		String Test02QPFontFamily = Test02QP.getCssValue("font-family");
		System.out.println("Actual Font Family of TEST 02 Text in Quality Page is : "	+ Test02QPFontFamily);
		String Test02QPContent = Test02QP.getText();
		System.out.println("Actual Content of TEST 02 Text in Quality Page is : "	+ Test02QPContent);
				
		String ExpTest02QPColor = sheet3.getCell(1,31).getContents(); //column,row
		System.out.println("Expected Color of TEST 02 Text in Quality Page is : "	+ ExpTest02QPColor );
		String ExpTest02QPFontSize = sheet3.getCell(2,31).getContents();
		System.out.println("Expected Font Size of TEST 02 Text in Quality Page is : "	+ ExpTest02QPFontSize);
		String ExpTest02QPFontFamily = sheet3.getCell(3,31).getContents();
		System.out.println("Expected Font Family of TEST 02 Text in Quality Page is : "	+ ExpTest02QPFontFamily);
		String ExpTest02QPContent = sheet3.getCell(4,31).getContents();
		System.out.println("Expected Content of TEST 02 Text in Quality Page is : "	+ ExpTest02QPContent);
				
		if(HexTest02QPColor.equals(ExpTest02QPColor))
		{
			System.out.println("TEST 02 Text in Quality Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 02 Text in Quality Page Color is not in match with creative - Fail");
			bw.write("TEST 02 Text in Quality Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(Test02QPFontSize.equals(ExpTest02QPFontSize))
		{
			System.out.println("TEST 02 Text in Quality Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 02 Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("TEST 02 Text in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(Test02QPFontFamily.equals(ExpTest02QPFontFamily))
		{
			System.out.println("TEST 02 Text in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 02 Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("TEST 02 Text in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(Test02QPContent.equals(ExpTest02QPContent))
		{
			System.out.println("TEST 02 Text Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 02 Text Content in Quality Page is not in match with creative - Fail");
			bw.write("TEST 02 Text Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexTest02QPColor.equals(ExpTest02QPColor) && Test02QPFontSize.equals(ExpTest02QPFontSize) && Test02QPFontFamily.equals(ExpTest02QPFontFamily) && Test02QPContent.equals(ExpTest02QPContent))
		{
			Label Test02QPCSSP = new Label(5,31,"PASS"); 
			wsheet3.addCell(Test02QPCSSP); 
		}
		else
		{
			Label Test02QPCSSF = new Label(5,31,"FAIL"); 
			wsheet3.addCell(Test02QPCSSF); 
		}
		
		// Quality Page TEST 02 - At the Mill

		
		WebElement Test02AtMillQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[9]/div/div/div/div/div[6]/div/div[2]/div[1]")); 
		String Test02AtMillQPColor = Test02AtMillQP.getCssValue("color");
		System.out.println("Actual Color of TEST 02 - At the Mill Text in Quality Page is : "	+ Test02AtMillQPColor );
		// Convert RGB to Hex Value
		Color HTest02AtMillQPColor = Color.fromString(Test02AtMillQPColor);
		String HexTest02AtMillQPColor = HTest02AtMillQPColor.asHex();
		System.out.println("Actual Hex Color of TEST 02 - At the Mill Text in Quality Page is : "	+ HexTest02AtMillQPColor );
		
		String Test02AtMillQPFontSize = Test02AtMillQP.getCssValue("font-size");
		System.out.println("Actual Font Size of TEST 02 - At the Mill Text in Quality Page is : "	+ Test02AtMillQPFontSize);
		String Test02AtMillQPFontFamily = Test02AtMillQP.getCssValue("font-family");
		System.out.println("Actual Font Family of TEST 02 - At the Mill Text in Quality Page is : "	+ Test02AtMillQPFontFamily);
		String Test02AtMillQPContent = Test02AtMillQP.getText();
		System.out.println("Actual Content of TEST 02 - At the Mill Text in Quality Page is : "	+ Test02AtMillQPContent);
				
		String ExpTest02AtMillQPColor = sheet3.getCell(1,32).getContents(); //column,row
		System.out.println("Expected Color of TEST 02 - At the Mill Text in Quality Page is : "	+ ExpTest02AtMillQPColor );
		String ExpTest02AtMillQPFontSize = sheet3.getCell(2,32).getContents();
		System.out.println("Expected Font Size of TEST 02 - At the Mill Text in Quality Page is : "	+ ExpTest02AtMillQPFontSize);
		String ExpTest02AtMillQPFontFamily = sheet3.getCell(3,32).getContents();
		System.out.println("Expected Font Family of TEST 02 - At the Mill Text in Quality Page is : "	+ ExpTest02AtMillQPFontFamily);
		String ExpTest02AtMillQPContent = sheet3.getCell(4,32).getContents();
		System.out.println("Expected Content of TEST 02 - At the Mill Text in Quality Page is : "	+ ExpTest02AtMillQPContent);
				
		if(HexTest02AtMillQPColor.equals(ExpTest02AtMillQPColor))
		{
			System.out.println("TEST 02 - At the Mill Text in Quality Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 02 - At the Mill Text in Quality Page Color is not in match with creative - Fail");
			bw.write("TEST 02 - At the Mill Text in Quality Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(Test02AtMillQPFontSize.equals(ExpTest02AtMillQPFontSize))
		{
			System.out.println("TEST 02 - At the Mill Text in Quality Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 02 - At the Mill Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("TEST 02 - At the Mill Text in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(Test02AtMillQPFontFamily.equals(ExpTest02AtMillQPFontFamily))
		{
			System.out.println("TEST 02 - At the Mill Text in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 02 - At the Mill Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("TEST 02 - At the Mill Text in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(Test02AtMillQPContent.equals(ExpTest02AtMillQPContent))
		{
			System.out.println("TEST 02 - At the Mill Text Content in Quality Page is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("TEST 02 - At the Mill Text Content in Quality Page is not in match with creative - Fail");
			bw.write("TEST 02 - At the Mill Text Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexTest02AtMillQPColor.equals(ExpTest02AtMillQPColor) && Test02AtMillQPFontSize.equals(ExpTest02AtMillQPFontSize) && Test02AtMillQPFontFamily.equals(ExpTest02AtMillQPFontFamily) && Test02AtMillQPContent.equals(ExpTest02AtMillQPContent))
		{
			Label Test02AtMillQPCSSP = new Label(5,32,"PASS"); 
			wsheet3.addCell(Test02AtMillQPCSSP); 
		}
		else
		{
			Label Test02AtMillQPCSSF = new Label(5,32,"FAIL"); 
			wsheet3.addCell(Test02AtMillQPCSSF); 
		}
		
		// Quality Page TEST 02 Desc
		
		WebElement Test02DescQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[9]/div/div/div/div/div[6]/div/div[2]/div[2]")); 
		String Test02DescQPColor = Test02DescQP.getCssValue("color");
		System.out.println("Actual Color of TEST 02 Desc in Quality Page is : "	+ Test02DescQPColor );
		// Convert RGB to Hex Value
		Color HTest02DescQPColor = Color.fromString(Test02DescQPColor);
		String HexTest02DescQPColor = HTest02DescQPColor.asHex();
		System.out.println("Actual Hex Color of TEST 02 Desc in Quality Page is : "	+ HexTest02DescQPColor );
		
		String Test02DescQPFontSize = Test02DescQP.getCssValue("font-size");
		System.out.println("Actual Font Size of TEST 02 Desc in Quality Page is : "	+ Test02DescQPFontSize);
		String Test02DescQPFontFamily = Test02DescQP.getCssValue("font-family");
		System.out.println("Actual Font Family of TEST 02 Desc in Quality Page is : "	+ Test02DescQPFontFamily);
		String Test02DescQPContent = Test02DescQP.getText();
		System.out.println("Actual Content of TEST 02 Desc in Quality Page is : "	+ Test02DescQPContent);
				
		String ExpTest02DescQPColor = sheet3.getCell(1,33).getContents(); //column,row
		System.out.println("Expected Color of TEST 02 Desc in Quality Page is : "	+ ExpTest02DescQPColor );
		String ExpTest02DescQPFontSize = sheet3.getCell(2,33).getContents();
		System.out.println("Expected Font Size of TEST 02 Desc in Quality Page is : "	+ ExpTest02DescQPFontSize);
		String ExpTest02DescQPFontFamily = sheet3.getCell(3,33).getContents();
		System.out.println("Expected Font Family of TEST 02 Desc in Quality Page is : "	+ ExpTest02DescQPFontFamily);
		String ExpTest02DescQPContent = sheet3.getCell(4,33).getContents();
		System.out.println("Expected Content of TEST 02 Desc in Quality Page is : "	+ ExpTest02DescQPContent);
				
		if(HexTest02DescQPColor.equals(ExpTest02DescQPColor))
		{
			System.out.println("TEST 02 Desc in Quality Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 02 Desc in Quality Page Color is not in match with creative - Fail");
			bw.write("TEST 02 Desc in Quality Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(Test02DescQPFontSize.equals(ExpTest02DescQPFontSize))
		{
			System.out.println("TEST 02 Desc in Quality Page Font Size is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("TEST 02 Desc in Quality Page Font Size is not in match with creative - Fail");
			bw.write("TEST 02 Desc in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(Test02DescQPFontFamily.equals(ExpTest02DescQPFontFamily))
		{
			System.out.println("TEST 02 Desc in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 02 Desc in Quality Page Font Family is not in match with creative - Fail");
			bw.write("TEST 02 Desc in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(Test02DescQPContent.equals(ExpTest02DescQPContent))
		{
			System.out.println("TEST 02 Desc Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 02 Desc Content in Quality Page is not in match with creative - Fail");
			bw.write("TEST 02 Desc Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexTest02DescQPColor.equals(ExpTest02DescQPColor) && Test02DescQPFontSize.equals(ExpTest02DescQPFontSize) && Test02DescQPFontFamily.equals(ExpTest02DescQPFontFamily) && Test02DescQPContent.equals(ExpTest02DescQPContent))
		{
			Label Test02DescQPCSSP = new Label(5,33,"PASS"); 
			wsheet3.addCell(Test02DescQPCSSP); 
		}
		else
		{
			Label Test02DescQPCSSF = new Label(5,33,"FAIL"); 
			wsheet3.addCell(Test02DescQPCSSF); 
		}
		
		// Quality Page - Making 
		
		WebElement QualityMakingQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[10]/div/div/div/div/div[2]/div/div[1]")); 
		
		String QualityMakingQPFontSize = QualityMakingQP.getCssValue("font-size");
		System.out.println("Actual Font Size of Making Text in Quality Page is : "	+ QualityMakingQPFontSize);
		String QualityMakingQPFontFamily = QualityMakingQP.getCssValue("font-family");
		System.out.println("Actual Font Family of Making Text in Quality Page is : "	+ QualityMakingQPFontFamily);
		String QualityMakingQPContent = QualityMakingQP.getText();
		System.out.println("Actual Content of Making Text in Quality Page is : "	+ QualityMakingQPContent);
				
		String ExpQualityMakingQPFontSize = sheet3.getCell(2,34).getContents();
		System.out.println("Expected Font Size of Making Text in Quality Page is : "	+ ExpQualityMakingQPFontSize);
		String ExpQualityMakingQPFontFamily = sheet3.getCell(3,34).getContents();
		System.out.println("Expected Font Family of Making Text in Quality Page is : "	+ ExpQualityMakingQPFontFamily);
		String ExpQualityMakingQPContent = sheet3.getCell(4,34).getContents();
		System.out.println("Expected Content of Making Text in Quality Page is : "	+ ExpQualityMakingQPContent);
				
				
		if(QualityMakingQPFontSize.equals(ExpQualityMakingQPFontSize))
		{
			System.out.println("Making Text in Quality Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Making Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("Making Text in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(QualityMakingQPFontFamily.equals(ExpQualityMakingQPFontFamily))
		{
			System.out.println("Making Text in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Making Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("Making Text in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(QualityMakingQPContent.equals(ExpQualityMakingQPContent))
		{
			System.out.println("Making Text Content in Quality Page is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("Making Text Content in Quality Page is not in match with creative - Fail");
			bw.write("Making Text Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(QualityMakingQPFontSize.equals(ExpQualityMakingQPFontSize) && QualityMakingQPFontFamily.equals(ExpQualityMakingQPFontFamily) && QualityMakingQPContent.equals(ExpQualityMakingQPContent))
		{
			Label QualityMakingQPCSSP = new Label(5,34,"PASS"); 
			wsheet3.addCell(QualityMakingQPCSSP); 
		}
		else
		{
			Label QualityMakingQPCSSF = new Label(5,34,"FAIL"); 
			wsheet3.addCell(QualityMakingQPCSSF); 
		}
		
		// Quality Page - THE JOURNEY
  		
		WebElement JourneyQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[10]/div/div/div/div/div[2]/div/div[2]")); 
		
		String JourneyQPFontSize = JourneyQP.getCssValue("font-size");
		System.out.println("Actual Font Size of THE JOURNEY Text in Quality Page is : "	+ JourneyQPFontSize);
		String JourneyQPFontFamily = JourneyQP.getCssValue("font-family");
		System.out.println("Actual Font Family of THE JOURNEY Text in Quality Page is : "	+ JourneyQPFontFamily);
		String JourneyQPContent = JourneyQP.getText();
		System.out.println("Actual Content of THE JOURNEY Text in Quality Page is : "	+ JourneyQPContent);
				
		String ExpJourneyQPFontSize = sheet3.getCell(2,35).getContents();
		System.out.println("Expected Font Size of THE JOURNEY Text in Quality Page is : "	+ ExpJourneyQPFontSize);
		String ExpJourneyQPFontFamily = sheet3.getCell(3,35).getContents();
		System.out.println("Expected Font Family of THE JOURNEY Text in Quality Page is : "	+ ExpJourneyQPFontFamily);
		String ExpJourneyQPContent = sheet3.getCell(4,35).getContents();
		System.out.println("Expected Content of THE JOURNEY Text in Quality Page is : "	+ ExpJourneyQPContent);
						
		if(JourneyQPFontSize.equals(ExpJourneyQPFontSize))
		{
			System.out.println("THE JOURNEY Text in Quality Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("THE JOURNEY Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("THE JOURNEY Text in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(JourneyQPFontFamily.equals(ExpJourneyQPFontFamily))
		{
			System.out.println("THE JOURNEY Text in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("THE JOURNEY Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("THE JOURNEY Text in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(JourneyQPContent.equals(ExpJourneyQPContent))
		{
			System.out.println("THE JOURNEY Text Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("THE JOURNEY Text Content in Quality Page is not in match with creative - Fail");
			bw.write("THE JOURNEY Text Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(JourneyQPFontSize.equals(ExpJourneyQPFontSize) && JourneyQPFontFamily.equals(ExpJourneyQPFontFamily) && JourneyQPContent.equals(ExpJourneyQPContent))
		{
			Label JourneyQPCSSP = new Label(5,35,"PASS"); 
			wsheet3.addCell(JourneyQPCSSP); 
		}
		else
		{
			Label JourneyQPCSSF = new Label(5,35,"FAIL"); 
			wsheet3.addCell(JourneyQPCSSF); 
		}
		
		
		// Quality Page - Making the Journey Desc
		
		WebElement JourneyDescQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[10]/div/div/div/div/div[2]/div/div[4]")); 
		
		String JourneyDescQPFontSize = JourneyDescQP.getCssValue("font-size");
		System.out.println("Actual Font Size of Making the Journey Desc in Quality Page is : "	+ JourneyDescQPFontSize);
		String JourneyDescQPFontFamily = JourneyDescQP.getCssValue("font-family");
		System.out.println("Actual Font Family of Making the Journey Desc in Quality Page is : "	+ JourneyDescQPFontFamily);
		String JourneyDescQPContent = JourneyDescQP.getText();
		System.out.println("Actual Content of Making the Journey Desc in Quality Page is : "	+ JourneyDescQPContent);
				
		String ExpJourneyDescQPFontSize = sheet3.getCell(2,36).getContents();
		System.out.println("Expected Font Size of Making the Journey Desc in Quality Page is : "	+ ExpJourneyDescQPFontSize);
		String ExpJourneyDescQPFontFamily = sheet3.getCell(3,36).getContents();
		System.out.println("Expected Font Family of Making the Journey Desc in Quality Page is : "	+ ExpJourneyDescQPFontFamily);
		String ExpJourneyDescQPContent = sheet3.getCell(4,36).getContents();
		System.out.println("Expected Content of Making the Journey Desc in Quality Page is : "	+ ExpJourneyDescQPContent);
				
		if(JourneyDescQPFontSize.equals(ExpJourneyDescQPFontSize))
		{
			System.out.println("Making the Journey Desc in Quality Page Font Size is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("Making the Journey Desc in Quality Page Font Size is not in match with creative - Fail");
			bw.write("Making the Journey Desc in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(JourneyDescQPFontFamily.equals(ExpJourneyDescQPFontFamily))
		{
			System.out.println("Making the Journey Desc in Quality Page Font Family is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("Making the Journey Desc in Quality Page Font Family is not in match with creative - Fail");
			bw.write("Making the Journey Desc in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(JourneyDescQPContent.equals(ExpJourneyDescQPContent))
		{
			System.out.println("Making the Journey Desc Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Making the Journey Desc Content in Quality Page is not in match with creative - Fail");
			bw.write("Making the Journey Desc Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(JourneyDescQPFontSize.equals(ExpJourneyDescQPFontSize) && JourneyDescQPFontFamily.equals(ExpJourneyDescQPFontFamily) && JourneyDescQPContent.equals(ExpJourneyDescQPContent))
		{
			Label JourneyDescQPCSSP = new Label(5,36,"PASS"); 
			wsheet3.addCell(JourneyDescQPCSSP); 
		}
		else
		{
			Label JourneyDescQPCSSF = new Label(5,36,"FAIL"); 
			wsheet3.addCell(JourneyDescQPCSSF); 
		}
		
		Thread.sleep(3000);
		JavascriptExecutor jseexporter = (JavascriptExecutor)driver;
		jseexporter.executeScript("window.scrollBy(0,800)", "");
		Thread.sleep(8000);
	
		// Quality Page TEST 03
		
		WebElement Test03QP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[11]/div/div/div/div/div[4]/div/div[1]/div[1]")); 
		String Test03QPColor = Test03QP.getCssValue("color");
		System.out.println("Actual Color of TEST 03 Text in Quality Page is : "	+ Test03QPColor );
		// Convert RGB to Hex Value
		Color HTest03QPColor = Color.fromString(Test03QPColor);
		String HexTest03QPColor = HTest03QPColor.asHex();
		System.out.println("Actual Hex Color of TEST 03 Text in Quality Page is : "	+ HexTest03QPColor );
		
		String Test03QPFontSize = Test03QP.getCssValue("font-size");
		System.out.println("Actual Font Size of TEST 03 Text in Quality Page is : "	+ Test03QPFontSize);
		String Test03QPFontFamily = Test03QP.getCssValue("font-family");
		System.out.println("Actual Font Family of TEST 03 Text in Quality Page is : "	+ Test03QPFontFamily);
		String Test03QPContent = Test03QP.getText();
		System.out.println("Actual Content of TEST 03 Text in Quality Page is : "	+ Test03QPContent);
				
		String ExpTest03QPColor = sheet3.getCell(1,37).getContents(); //column,row
		System.out.println("Expected Color of TEST 03 Text in Quality Page is : "	+ ExpTest03QPColor );
		String ExpTest03QPFontSize = sheet3.getCell(2,37).getContents();
		System.out.println("Expected Font Size of TEST 03 Text in Quality Page is : "	+ ExpTest03QPFontSize);
		String ExpTest03QPFontFamily = sheet3.getCell(3,37).getContents();
		System.out.println("Expected Font Family of TEST 03 Text in Quality Page is : "	+ ExpTest03QPFontFamily);
		String ExpTest03QPContent = sheet3.getCell(4,37).getContents();
		System.out.println("Expected Content of TEST 03 Text in Quality Page is : "	+ ExpTest03QPContent);
				
		if(HexTest03QPColor.equals(ExpTest03QPColor))
		{
			System.out.println("TEST 03 Text in Quality Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 03 Text in Quality Page Color is not in match with creative - Fail");
			bw.write("TEST 03 Text in Quality Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(Test03QPFontSize.equals(ExpTest03QPFontSize))
		{
			System.out.println("TEST 03 Text in Quality Page Font Size is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("TEST 03 Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("TEST 03 Text in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(Test03QPFontFamily.equals(ExpTest03QPFontFamily))
		{
			System.out.println("TEST 03 Text in Quality Page Font Family is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("TEST 03 Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("TEST 03 Text in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(Test03QPContent.equals(ExpTest03QPContent))
		{
			System.out.println("TEST 03 Text Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 03 Text Content in Quality Page is not in match with creative - Fail");
			bw.write("TEST 03 Text Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexTest03QPColor.equals(ExpTest03QPColor) && Test03QPFontSize.equals(ExpTest03QPFontSize) && Test03QPFontFamily.equals(ExpTest03QPFontFamily) && Test03QPContent.equals(ExpTest03QPContent))
		{
			Label Test03QPCSSP = new Label(5,37,"PASS"); 
			wsheet3.addCell(Test03QPCSSP); 
		}
		else
		{
			Label Test03QPCSSF = new Label(5,37,"FAIL"); 
			wsheet3.addCell(Test03QPCSSF); 
		}
		
		// Quality Page TEST 03 - At the Exporter
		
		WebElement Test03AtExporterQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[11]/div/div/div/div/div[4]/div/div[2]/div[1]")); 
		String Test03AtExporterQPColor = Test03AtExporterQP.getCssValue("color");
		System.out.println("Actual Color of TEST 03 - At the Exporter Text in Quality Page is : "	+ Test03AtExporterQPColor );
		// Convert RGB to Hex Value
		Color HTest03AtExporterQPColor = Color.fromString(Test03AtExporterQPColor);
		String HexTest03AtExporterQPColor = HTest03AtExporterQPColor.asHex();
		System.out.println("Actual Hex Color of TEST 03 - At the Exporter Text in Quality Page is : "	+ HexTest03AtExporterQPColor );
		
		String Test03AtExporterQPFontSize = Test03AtExporterQP.getCssValue("font-size");
		System.out.println("Actual Font Size of TEST 03 - At the Exporter Text in Quality Page is : "	+ Test03AtExporterQPFontSize);
		String Test03AtExporterQPFontFamily = Test03AtExporterQP.getCssValue("font-family");
		System.out.println("Actual Font Family of TEST 03 - At the Exporter Text in Quality Page is : "	+ Test03AtExporterQPFontFamily);
		String Test03AtExporterQPContent = Test03AtExporterQP.getText();
		System.out.println("Actual Content of TEST 03 - At the Exporter Text in Quality Page is : "	+ Test03AtExporterQPContent);
				
		String ExpTest03AtExporterQPColor = sheet3.getCell(1,38).getContents(); //column,row
		System.out.println("Expected Color of TEST 03 - At the Exporter Text in Quality Page is : "	+ ExpTest03AtExporterQPColor );
		String ExpTest03AtExporterQPFontSize = sheet3.getCell(2,38).getContents();
		System.out.println("Expected Font Size of TEST 03 - At the Exporter Text in Quality Page is : "	+ ExpTest03AtExporterQPFontSize);
		String ExpTest03AtExporterQPFontFamily = sheet3.getCell(3,38).getContents();
		System.out.println("Expected Font Family of TEST 03 - At the Exporter Text in Quality Page is : "	+ ExpTest03AtExporterQPFontFamily);
		String ExpTest03AtExporterQPContent = sheet3.getCell(4,38).getContents();
		System.out.println("Expected Content of TEST 03 - At the Exporter Text in Quality Page is : "	+ ExpTest03AtExporterQPContent);
				
		if(HexTest03AtExporterQPColor.equals(ExpTest03AtExporterQPColor))
		{
			System.out.println("TEST 03 - At the Exporter Text in Quality Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 03 - At the Exporter Text in Quality Page Color is not in match with creative - Fail");
			bw.write("TEST 03 - At the Exporter Text in Quality Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(Test03AtExporterQPFontSize.equals(ExpTest03AtExporterQPFontSize))
		{
			System.out.println("TEST 03 - At the Exporter Text in Quality Page Font Size is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("TEST 03 - At the Exporter Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("TEST 03 - At the Exporter Text in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(Test03AtExporterQPFontFamily.equals(ExpTest03AtExporterQPFontFamily))
		{
			System.out.println("TEST 03 - At the Exporter Text in Quality Page Font Family is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("TEST 03 - At the Exporter Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("TEST 03 - At the Exporter Text in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(Test03AtExporterQPContent.equals(ExpTest03AtExporterQPContent))
		{
			System.out.println("TEST 03 - At the Exporter Text Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 03 - At the Exporter Text Content in Quality Page is not in match with creative - Fail");
			bw.write("TEST 03 - At the Exporter Text Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexTest03AtExporterQPColor.equals(ExpTest03AtExporterQPColor) && Test03AtExporterQPFontSize.equals(ExpTest03AtExporterQPFontSize) && Test03AtExporterQPFontFamily.equals(ExpTest03AtExporterQPFontFamily) && Test03AtExporterQPContent.equals(ExpTest03AtExporterQPContent))
		{
			Label Test03AtExporterQPCSSP = new Label(5,38,"PASS"); 
			wsheet3.addCell(Test03AtExporterQPCSSP); 
		}
		else
		{
			Label Test03AtExporterQPCSSF = new Label(5,38,"FAIL"); 
			wsheet3.addCell(Test03AtExporterQPCSSF); 
		}
		
		// Quality Page TEST 03 Desc
		
		WebElement Test03DescQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[11]/div/div/div/div/div[4]/div/div[2]/div[2]")); 
		String Test03DescQPColor = Test03DescQP.getCssValue("color");
		System.out.println("Actual Color of TEST 03 Desc in Quality Page is : "	+ Test03DescQPColor );
		// Convert RGB to Hex Value
		Color HTest03DescQPColor = Color.fromString(Test03DescQPColor);
		String HexTest03DescQPColor = HTest03DescQPColor.asHex();
		System.out.println("Actual Hex Color of TEST 03 Desc in Quality Page is : "	+ HexTest03DescQPColor );
		
		String Test03DescQPFontSize = Test03DescQP.getCssValue("font-size");
		System.out.println("Actual Font Size of TEST 03 Desc in Quality Page is : "	+ Test03DescQPFontSize);
		String Test03DescQPFontFamily = Test03DescQP.getCssValue("font-family");
		System.out.println("Actual Font Family of TEST 03 Desc in Quality Page is : "	+ Test03DescQPFontFamily);
		String Test03DescQPContent = Test03DescQP.getText();
		System.out.println("Actual Content of TEST 03 Desc in Quality Page is : "	+ Test03DescQPContent);
				
		String ExpTest03DescQPColor = sheet3.getCell(1,39).getContents(); //column,row
		System.out.println("Expected Color of TEST 03 Desc in Quality Page is : "	+ ExpTest03DescQPColor );
		String ExpTest03DescQPFontSize = sheet3.getCell(2,39).getContents();
		System.out.println("Expected Font Size of TEST 03 Desc in Quality Page is : "	+ ExpTest03DescQPFontSize);
		String ExpTest03DescQPFontFamily = sheet3.getCell(3,39).getContents();
		System.out.println("Expected Font Family of TEST 03 Desc in Quality Page is : "	+ ExpTest03DescQPFontFamily);
		String ExpTest03DescQPContent = sheet3.getCell(4,39).getContents();
		System.out.println("Expected Content of TEST 03 Desc in Quality Page is : "	+ ExpTest03DescQPContent);
				
		if(HexTest03DescQPColor.equals(ExpTest03DescQPColor))
		{
			System.out.println("TEST 03 Desc in Quality Page Color is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("TEST 03 Desc in Quality Page Color is not in match with creative - Fail");
			bw.write("TEST 03 Desc in Quality Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(Test03DescQPFontSize.equals(ExpTest03DescQPFontSize))
		{
			System.out.println("TEST 03 Desc in Quality Page Font Size is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("TEST 03 Desc in Quality Page Font Size is not in match with creative - Fail");
			bw.write("TEST 03 Desc in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(Test03DescQPFontFamily.equals(ExpTest03DescQPFontFamily))
		{
			System.out.println("TEST 03 Desc in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 03 Desc in Quality Page Font Family is not in match with creative - Fail");
			bw.write("TEST 03 Desc in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(Test03DescQPContent.equals(ExpTest03DescQPContent))
		{
			System.out.println("TEST 03 Desc Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 03 Desc Content in Quality Page is not in match with creative - Fail");
			bw.write("TEST 03 Desc Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexTest03DescQPColor.equals(ExpTest03DescQPColor) && Test03DescQPFontSize.equals(ExpTest03DescQPFontSize) && Test03DescQPFontFamily.equals(ExpTest03DescQPFontFamily) && Test03DescQPContent.equals(ExpTest03DescQPContent))
		{
			Label Test03DescQPCSSP = new Label(5,39,"PASS"); 
			wsheet3.addCell(Test03DescQPCSSP); 
		}
		else
		{
			Label Test03DescQPCSSF = new Label(5,39,"FAIL"); 
			wsheet3.addCell(Test03DescQPCSSF); 
		}
		
		
		// Quality Page TEST 04
		
		WebElement Test04QP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[11]/div/div/div/div/div[5]/div/div[1]/div[1]")); 
		String Test04QPColor = Test04QP.getCssValue("color");
		System.out.println("Actual Color of TEST 04 Text in Quality Page is : "	+ Test04QPColor );
		// Convert RGB to Hex Value
		Color HTest04QPColor = Color.fromString(Test04QPColor);
		String HexTest04QPColor = HTest04QPColor.asHex();
		System.out.println("Actual Hex Color of TEST 04 Text in Quality Page is : "	+ HexTest04QPColor );
		
		String Test04QPFontSize = Test04QP.getCssValue("font-size");
		System.out.println("Actual Font Size of TEST 04 Text in Quality Page is : "	+ Test04QPFontSize);
		String Test04QPFontFamily = Test04QP.getCssValue("font-family");
		System.out.println("Actual Font Family of TEST 04 Text in Quality Page is : "	+ Test04QPFontFamily);
		String Test04QPContent = Test04QP.getText();
		System.out.println("Actual Content of TEST 04 Text in Quality Page is : "	+ Test04QPContent);
				
		String ExpTest04QPColor = sheet3.getCell(1,40).getContents(); //column,row
		System.out.println("Expected Color of TEST 04 Text in Quality Page is : "	+ ExpTest04QPColor );
		String ExpTest04QPFontSize = sheet3.getCell(2,40).getContents();
		System.out.println("Expected Font Size of TEST 04 Text in Quality Page is : "	+ ExpTest04QPFontSize);
		String ExpTest04QPFontFamily = sheet3.getCell(3,40).getContents();
		System.out.println("Expected Font Family of TEST 04 Text in Quality Page is : "	+ ExpTest04QPFontFamily);
		String ExpTest04QPContent = sheet3.getCell(4,40).getContents();
		System.out.println("Expected Content of TEST 04 Text in Quality Page is : "	+ ExpTest04QPContent);
				
		if(HexTest04QPColor.equals(ExpTest04QPColor))
		{
			System.out.println("TEST 04 Text in Quality Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 04 Text in Quality Page Color is not in match with creative - Fail");
			bw.write("TEST 04 Text in Quality Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(Test04QPFontSize.equals(ExpTest04QPFontSize))
		{
			System.out.println("TEST 04 Text in Quality Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 04 Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("TEST 04 Text in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(Test04QPFontFamily.equals(ExpTest04QPFontFamily))
		{
			System.out.println("TEST 04 Text in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 04 Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("TEST 04 Text in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(Test04QPContent.equals(ExpTest04QPContent))
		{
			System.out.println("TEST 04 Text Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 04 Text Content in Quality Page is not in match with creative - Fail");
			bw.write("TEST 04 Text Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexTest04QPColor.equals(ExpTest04QPColor) && Test04QPFontSize.equals(ExpTest04QPFontSize) && Test04QPFontFamily.equals(ExpTest04QPFontFamily) && Test04QPContent.equals(ExpTest04QPContent))
		{
			Label Test04QPCSSP = new Label(5,40,"PASS"); 
			wsheet3.addCell(Test04QPCSSP); 
		}
		else
		{
			Label Test04QPCSSF = new Label(5,40,"FAIL"); 
			wsheet3.addCell(Test04QPCSSF); 
		}
		
		// Quality Page TEST 04 - At The Trading Office
		
		WebElement Test04TradingQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[11]/div/div/div/div/div[5]/div/div[2]/div[1]")); 
		String Test04TradingQPColor = Test04TradingQP.getCssValue("color");
		System.out.println("Actual Color of TEST 04 - At The Trading Office Text in Quality Page is : "	+ Test04TradingQPColor );
		// Convert RGB to Hex Value
		Color HTest04TradingQPColor = Color.fromString(Test04TradingQPColor);
		String HexTest04TradingQPColor = HTest04TradingQPColor.asHex();
		System.out.println("Actual Hex Color of TEST 04 - At The Trading Office Text in Quality Page is : "	+ HexTest04TradingQPColor );
		
		String Test04TradingQPFontSize = Test04TradingQP.getCssValue("font-size");
		System.out.println("Actual Font Size of TEST 04 - At The Trading Office Text in Quality Page is : "	+ Test04TradingQPFontSize);
		String Test04TradingQPFontFamily = Test04TradingQP.getCssValue("font-family");
		System.out.println("Actual Font Family of TEST 04 - At The Trading Office Text in Quality Page is : "	+ Test04TradingQPFontFamily);
		String Test04TradingQPContent = Test04TradingQP.getText();
		System.out.println("Actual Content of TEST 04 - At The Trading Office Text in Quality Page is : "	+ Test04TradingQPContent);
				
		String ExpTest04TradingQPColor = sheet3.getCell(1,41).getContents(); //column,row
		System.out.println("Expected Color of TEST 04 - At The Trading Office Text in Quality Page is : "	+ ExpTest04TradingQPColor );
		String ExpTest04TradingQPFontSize = sheet3.getCell(2,41).getContents();
		System.out.println("Expected Font Size of TEST 04 - At The Trading Office Text in Quality Page is : "	+ ExpTest04TradingQPFontSize);
		String ExpTest04TradingQPFontFamily = sheet3.getCell(3,41).getContents();
		System.out.println("Expected Font Family of TEST 04 - At The Trading Office Text in Quality Page is : "	+ ExpTest04TradingQPFontFamily);
		String ExpTest04TradingQPContent = sheet3.getCell(4,41).getContents();
		System.out.println("Expected Content of TEST 04 - At The Trading Office Text in Quality Page is : "	+ ExpTest04TradingQPContent);
				
		if(HexTest04TradingQPColor.equals(ExpTest04TradingQPColor))
		{
			System.out.println("TEST 04 - At The Trading Office Text in Quality Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 04 - At The Trading Office Text in Quality Page Color is not in match with creative - Fail");
			bw.write("TEST 04 - At The Trading Office Text in Quality Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(Test04TradingQPFontSize.equals(ExpTest04TradingQPFontSize))
		{
			System.out.println("TEST 04 - At The Trading Office Text in Quality Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 04 - At The Trading Office Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("TEST 04 - At The Trading Office Text in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(Test04TradingQPFontFamily.equals(ExpTest04TradingQPFontFamily))
		{
			System.out.println("TEST 04 - At The Trading Office Text in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 04 - At The Trading Office Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("TEST 04 - At The Trading Office Text in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(Test04TradingQPContent.equals(ExpTest04TradingQPContent))
		{
			System.out.println("TEST 04 - At The Trading Office Text Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 04 - At The Trading Office Text Content in Quality Page is not in match with creative - Fail");
			bw.write("TEST 04 - At The Trading Office Text Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexTest04TradingQPColor.equals(ExpTest04TradingQPColor) && Test04TradingQPFontSize.equals(ExpTest04TradingQPFontSize) && Test04TradingQPFontFamily.equals(ExpTest04TradingQPFontFamily) && Test04TradingQPContent.equals(ExpTest04TradingQPContent))
		{
			Label Test04TradingQPCSSP = new Label(5,41,"PASS"); 
			wsheet3.addCell(Test04TradingQPCSSP); 
		}
		else
		{
			Label Test04TradingQPCSSF = new Label(5,41,"FAIL"); 
			wsheet3.addCell(Test04TradingQPCSSF); 
		}
		
		
		// Quality Page TEST 04 Desc
		
		WebElement Test04DescQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[11]/div/div/div/div/div[5]/div/div[2]/div[2]")); 
		String Test04DescQPColor = Test04DescQP.getCssValue("color");
		System.out.println("Actual Color of TEST 04 Desc in Quality Page is : "	+ Test04DescQPColor );
		// Convert RGB to Hex Value
		Color HTest04DescQPColor = Color.fromString(Test04DescQPColor);
		String HexTest04DescQPColor = HTest04DescQPColor.asHex();
		System.out.println("Actual Hex Color of TEST 04 Desc in Quality Page is : "	+ HexTest04DescQPColor );
		
		String Test04DescQPFontSize = Test04DescQP.getCssValue("font-size");
		System.out.println("Actual Font Size of TEST 04 Desc in Quality Page is : "	+ Test04DescQPFontSize);
		String Test04DescQPFontFamily = Test04DescQP.getCssValue("font-family");
		System.out.println("Actual Font Family of TEST 04 Desc in Quality Page is : "	+ Test04DescQPFontFamily);
		String Test04DescQPContent = Test04DescQP.getText();
		System.out.println("Actual Content of TEST 04 Desc in Quality Page is : "	+ Test04DescQPContent);
				
		String ExpTest04DescQPColor = sheet3.getCell(1,42).getContents(); //column,row
		System.out.println("Expected Color of TEST 04 Desc in Quality Page is : "	+ ExpTest04DescQPColor );
		String ExpTest04DescQPFontSize = sheet3.getCell(2,42).getContents();
		System.out.println("Expected Font Size of TEST 04 Desc in Quality Page is : "	+ ExpTest04DescQPFontSize);
		String ExpTest04DescQPFontFamily = sheet3.getCell(3,42).getContents();
		System.out.println("Expected Font Family of TEST 04 Desc in Quality Page is : "	+ ExpTest04DescQPFontFamily);
		String ExpTest04DescQPContent = sheet3.getCell(4,42).getContents();
		System.out.println("Expected Content of TEST 04 Desc in Quality Page is : "	+ ExpTest04DescQPContent);
				
		if(HexTest04DescQPColor.equals(ExpTest04DescQPColor))
		{
			System.out.println("TEST 04 Desc in Quality Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 04 Desc in Quality Page Color is not in match with creative - Fail");
			bw.write("TEST 04 Desc in Quality Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(Test04DescQPFontSize.equals(ExpTest04DescQPFontSize))
		{
			System.out.println("TEST 04 Desc in Quality Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 04 Desc in Quality Page Font Size is not in match with creative - Fail");
			bw.write("TEST 04 Desc in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(Test04DescQPFontFamily.equals(ExpTest04DescQPFontFamily))
		{
			System.out.println("TEST 04 Desc in Quality Page Font Family is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("TEST 04 Desc in Quality Page Font Family is not in match with creative - Fail");
			bw.write("TEST 04 Desc in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(Test04DescQPContent.equals(ExpTest04DescQPContent))
		{
			System.out.println("TEST 04 Desc Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 04 Desc Content in Quality Page is not in match with creative - Fail");
			bw.write("TEST 04 Desc Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexTest04DescQPColor.equals(ExpTest04DescQPColor) && Test04DescQPFontSize.equals(ExpTest04DescQPFontSize) && Test04DescQPFontFamily.equals(ExpTest04DescQPFontFamily) && Test04DescQPContent.equals(ExpTest04DescQPContent))
		{
			Label Test04DescQPCSSP = new Label(5,42,"PASS"); 
			wsheet3.addCell(Test04DescQPCSSP); 
		}
		else
		{
			Label Test04DescQPCSSF = new Label(5,42,"FAIL"); 
			wsheet3.addCell(Test04DescQPCSSF); 
		}
		
		// Quality Page TEST 05
		
		WebElement Test05QP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[11]/div/div/div/div/div[6]/div/div[1]/div[1]")); 
		String Test05QPColor = Test05QP.getCssValue("color");
		System.out.println("Actual Color of TEST 05 Text in Quality Page is : "	+ Test05QPColor );
		// Convert RGB to Hex Value
		Color HTest05QPColor = Color.fromString(Test05QPColor);
		String HexTest05QPColor = HTest05QPColor.asHex();
		System.out.println("Actual Hex Color of TEST 05 Text in Quality Page is : "	+ HexTest05QPColor );
		
		String Test05QPFontSize = Test05QP.getCssValue("font-size");
		System.out.println("Actual Font Size of TEST 05 Text in Quality Page is : "	+ Test05QPFontSize);
		String Test05QPFontFamily = Test05QP.getCssValue("font-family");
		System.out.println("Actual Font Family of TEST 05 Text in Quality Page is : "	+ Test05QPFontFamily);
		String Test05QPContent = Test05QP.getText();
		System.out.println("Actual Content of TEST 05 Text in Quality Page is : "	+ Test05QPContent);
				
		String ExpTest05QPColor = sheet3.getCell(1,43).getContents(); //column,row
		System.out.println("Expected Color of TEST 05 Text in Quality Page is : "	+ ExpTest05QPColor );
		String ExpTest05QPFontSize = sheet3.getCell(2,43).getContents();
		System.out.println("Expected Font Size of TEST 05 Text in Quality Page is : "	+ ExpTest05QPFontSize);
		String ExpTest05QPFontFamily = sheet3.getCell(3,43).getContents();
		System.out.println("Expected Font Family of TEST 05 Text in Quality Page is : "	+ ExpTest05QPFontFamily);
		String ExpTest05QPContent = sheet3.getCell(4,43).getContents();
		System.out.println("Expected Content of TEST 05 Text in Quality Page is : "	+ ExpTest05QPContent);
				
		if(HexTest05QPColor.equals(ExpTest05QPColor))
		{
			System.out.println("TEST 05 Text in Quality Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 05 Text in Quality Page Color is not in match with creative - Fail");
			bw.write("TEST 05 Text in Quality Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(Test05QPFontSize.equals(ExpTest05QPFontSize))
		{
			System.out.println("TEST 05 Text in Quality Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 05 Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("TEST 05 Text in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(Test05QPFontFamily.equals(ExpTest05QPFontFamily))
		{
			System.out.println("TEST 05 Text in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 05 Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("TEST 05 Text in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(Test05QPContent.equals(ExpTest05QPContent))
		{
			System.out.println("TEST 05 Text Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 05 Text Content in Quality Page is not in match with creative - Fail");
			bw.write("TEST 05 Text Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexTest05QPColor.equals(ExpTest05QPColor) && Test05QPFontSize.equals(ExpTest05QPFontSize) && Test05QPFontFamily.equals(ExpTest05QPFontFamily) && Test05QPContent.equals(ExpTest05QPContent))
		{
			Label Test05QPCSSP = new Label(5,43,"PASS"); 
			wsheet3.addCell(Test05QPCSSP); 
		}
		else
		{
			Label Test05QPCSSF = new Label(5,43,"FAIL"); 
			wsheet3.addCell(Test05QPCSSF); 
		}
		
		// Quality Page TEST 05 - At The Importer
		
		WebElement Test05AtImporterQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[11]/div/div/div/div/div[6]/div/div[2]/div[1]")); 
		String Test05AtImporterQPColor = Test05AtImporterQP.getCssValue("color");
		System.out.println("Actual Color of TEST 05 - At The Importer Text in Quality Page is : "	+ Test05AtImporterQPColor );
		// Convert RGB to Hex Value
		Color HTest05AtImporterQPColor = Color.fromString(Test05AtImporterQPColor);
		String HexTest05AtImporterQPColor = HTest05AtImporterQPColor.asHex();
		System.out.println("Actual Hex Color of TEST 05 - At The Importer Text in Quality Page is : "	+ HexTest05AtImporterQPColor );
		
		String Test05AtImporterQPFontSize = Test05AtImporterQP.getCssValue("font-size");
		System.out.println("Actual Font Size of TEST 05 - At The Importer Text in Quality Page is : "	+ Test05AtImporterQPFontSize);
		String Test05AtImporterQPFontFamily = Test05AtImporterQP.getCssValue("font-family");
		System.out.println("Actual Font Family of TEST 05 - At The Importer Text in Quality Page is : "	+ Test05AtImporterQPFontFamily);
		String Test05AtImporterQPContent = Test05AtImporterQP.getText();
		System.out.println("Actual Content of TEST 05 - At The Importer Text in Quality Page is : "	+ Test05AtImporterQPContent);
				
		String ExpTest05AtImporterQPColor = sheet3.getCell(1,44).getContents(); //column,row
		System.out.println("Expected Color of TEST 05 - At The Importer Text in Quality Page is : "	+ ExpTest05AtImporterQPColor );
		String ExpTest05AtImporterQPFontSize = sheet3.getCell(2,44).getContents();
		System.out.println("Expected Font Size of TEST 05 - At The Importer Text in Quality Page is : "	+ ExpTest05AtImporterQPFontSize);
		String ExpTest05AtImporterQPFontFamily = sheet3.getCell(3,44).getContents();
		System.out.println("Expected Font Family of TEST 05 - At The Importer Text in Quality Page is : "	+ ExpTest05AtImporterQPFontFamily);
		String ExpTest05AtImporterQPContent = sheet3.getCell(4,44).getContents();
		System.out.println("Expected Content of TEST 05 - At The Importer Text in Quality Page is : "	+ ExpTest05AtImporterQPContent);
				
		if(HexTest05AtImporterQPColor.equals(ExpTest05AtImporterQPColor))
		{
			System.out.println("TEST 05 - At The Importer Text in Quality Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 05 - At The Importer Text in Quality Page Color is not in match with creative - Fail");
			bw.write("TEST 05 - At The Importer Text in Quality Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(Test05AtImporterQPFontSize.equals(ExpTest05AtImporterQPFontSize))
		{
			System.out.println("TEST 05 - At The Importer Text in Quality Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 05 - At The Importer Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("TEST 05 - At The Importer Text in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(Test05AtImporterQPFontFamily.equals(ExpTest05AtImporterQPFontFamily))
		{
			System.out.println("TEST 05 - At The Importer Text in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 05 - At The Importer Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("TEST 05 - At The Importer Text in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(Test05AtImporterQPContent.equals(ExpTest05AtImporterQPContent))
		{
			System.out.println("TEST 05 - At The Importer Text Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 05 - At The Importer Text Content in Quality Page is not in match with creative - Fail");
			bw.write("TEST 05 - At The Importer Text Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexTest05AtImporterQPColor.equals(ExpTest05AtImporterQPColor) && Test05AtImporterQPFontSize.equals(ExpTest05AtImporterQPFontSize) && Test05AtImporterQPFontFamily.equals(ExpTest05AtImporterQPFontFamily) && Test05AtImporterQPContent.equals(ExpTest05AtImporterQPContent))
		{
			Label Test05AtImporterQPCSSP = new Label(5,44,"PASS"); 
			wsheet3.addCell(Test05AtImporterQPCSSP); 
		}
		else
		{
			Label Test05AtImporterQPCSSF = new Label(5,44,"FAIL"); 
			wsheet3.addCell(Test05AtImporterQPCSSF); 
		}
		
		
		// Quality Page TEST 05 Desc
		
		WebElement Test05DescQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[11]/div/div/div/div/div[6]/div/div[2]/div[2]")); 
		String Test05DescQPColor = Test05DescQP.getCssValue("color");
		System.out.println("Actual Color of TEST 05 Desc in Quality Page is : "	+ Test05DescQPColor );
		// Convert RGB to Hex Value
		Color HTest05DescQPColor = Color.fromString(Test05DescQPColor);
		String HexTest05DescQPColor = HTest05DescQPColor.asHex();
		System.out.println("Actual Hex Color of TEST 05 Desc in Quality Page is : "	+ HexTest05DescQPColor );
		
		String Test05DescQPFontSize = Test05DescQP.getCssValue("font-size");
		System.out.println("Actual Font Size of TEST 05 Desc in Quality Page is : "	+ Test05DescQPFontSize);
		String Test05DescQPFontFamily = Test05DescQP.getCssValue("font-family");
		System.out.println("Actual Font Family of TEST 05 Desc in Quality Page is : "	+ Test05DescQPFontFamily);
		String Test05DescQPContent = Test05DescQP.getText();
		System.out.println("Actual Content of TEST 05 Desc in Quality Page is : "	+ Test05DescQPContent);
				
		String ExpTest05DescQPColor = sheet3.getCell(1,45).getContents(); //column,row
		System.out.println("Expected Color of TEST 05 Desc in Quality Page is : "	+ ExpTest05DescQPColor );
		String ExpTest05DescQPFontSize = sheet3.getCell(2,45).getContents();
		System.out.println("Expected Font Size of TEST 05 Desc in Quality Page is : "	+ ExpTest05DescQPFontSize);
		String ExpTest05DescQPFontFamily = sheet3.getCell(3,45).getContents();
		System.out.println("Expected Font Family of TEST 05 Desc in Quality Page is : "	+ ExpTest05DescQPFontFamily);
		String ExpTest05DescQPContent = sheet3.getCell(4,45).getContents();
		System.out.println("Expected Content of TEST 05 Desc in Quality Page is : "	+ ExpTest05DescQPContent);
				
		if(HexTest05DescQPColor.equals(ExpTest05DescQPColor))
		{
			System.out.println("TEST 05 Desc in Quality Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 05 Desc in Quality Page Color is not in match with creative - Fail");
			bw.write("TEST 05 Desc in Quality Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(Test05DescQPFontSize.equals(ExpTest05DescQPFontSize))
		{
			System.out.println("TEST 05 Desc in Quality Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 05 Desc in Quality Page Font Size is not in match with creative - Fail");
			bw.write("TEST 05 Desc in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(Test05DescQPFontFamily.equals(ExpTest05DescQPFontFamily))
		{
			System.out.println("TEST 05 Desc in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 05 Desc in Quality Page Font Family is not in match with creative - Fail");
			bw.write("TEST 05 Desc in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(Test05DescQPContent.equals(ExpTest05DescQPContent))
		{
			System.out.println("TEST 05 Desc Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 05 Desc Content in Quality Page is not in match with creative - Fail");
			bw.write("TEST 05 Desc Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexTest05DescQPColor.equals(ExpTest05DescQPColor) && Test05DescQPFontSize.equals(ExpTest05DescQPFontSize) && Test05DescQPFontFamily.equals(ExpTest05DescQPFontFamily) && Test05DescQPContent.equals(ExpTest05DescQPContent))
		{
			Label Test05DescQPCSSP = new Label(5,45,"PASS"); 
			wsheet3.addCell(Test05DescQPCSSP); 
		}
		else
		{
			Label Test05DescQPCSSF = new Label(5,45,"FAIL"); 
			wsheet3.addCell(Test05DescQPCSSF); 
		}
		
		Thread.sleep(3000);
		JavascriptExecutor jsearts = (JavascriptExecutor)driver;
		jsearts.executeScript("window.scrollBy(0,600)", "");
		Thread.sleep(8000);

		// WHAT�S A Q-GRADER?
		
		WebElement WhatsQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[12]/div/div/div/div/div[3]/div[2]/div[1]")); 
		String WhatsQPColor = WhatsQP.getCssValue("color");
		System.out.println("Actual Color of WHAT�S A Q-GRADER? Text in Quality Page is : "	+ WhatsQPColor );
		// Convert RGB to Hex Value
		Color HWhatsQPColor = Color.fromString(WhatsQPColor);
		String HexWhatsQPColor = HWhatsQPColor.asHex();
		System.out.println("Actual Hex Color of WHAT�S A Q-GRADER? Text in Quality Page is : "	+ HexWhatsQPColor );
		
		String WhatsQPFontSize = WhatsQP.getCssValue("font-size");
		System.out.println("Actual Font Size of WHAT�S A Q-GRADER? Text in Quality Page is : "	+ WhatsQPFontSize);
		String WhatsQPFontFamily = WhatsQP.getCssValue("font-family");
		System.out.println("Actual Font Family of WHAT�S A Q-GRADER? Text in Quality Page is : "	+ WhatsQPFontFamily);
		String WhatsQPContent = WhatsQP.getText();
		System.out.println("Actual Content of WHAT�S A Q-GRADER? Text in Quality Page is : "	+ WhatsQPContent);
				
		String ExpWhatsQPColor = sheet3.getCell(1,46).getContents(); //column,row
		System.out.println("Expected Color of WHAT�S A Q-GRADER? Text in Quality Page is : "	+ ExpWhatsQPColor );
		String ExpWhatsQPFontSize = sheet3.getCell(2,46).getContents();
		System.out.println("Expected Font Size of WHAT�S A Q-GRADER? Text in Quality Page is : "	+ ExpWhatsQPFontSize);
		String ExpWhatsQPFontFamily = sheet3.getCell(3,46).getContents();
		System.out.println("Expected Font Family of WHAT�S A Q-GRADER? Text in Quality Page is : "	+ ExpWhatsQPFontFamily);
		String ExpWhatsQPContent = sheet3.getCell(4,46).getContents();
		System.out.println("Expected Content of WHAT�S A Q-GRADER? Text in Quality Page is : "	+ ExpWhatsQPContent);
				
		if(HexWhatsQPColor.equals(ExpWhatsQPColor))
		{
			System.out.println("WHAT�S A Q-GRADER? Text in Quality Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("WHAT�S A Q-GRADER? Text in Quality Page Color is not in match with creative - Fail");
			bw.write("WHAT�S A Q-GRADER? Text in Quality Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(WhatsQPFontSize.equals(ExpWhatsQPFontSize))
		{
			System.out.println("WHAT�S A Q-GRADER? Text in Quality Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("WHAT�S A Q-GRADER? Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("WHAT�S A Q-GRADER? Text in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(WhatsQPFontFamily.equals(ExpWhatsQPFontFamily))
		{
			System.out.println("WHAT�S A Q-GRADER? Text in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("WHAT�S A Q-GRADER? Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("WHAT�S A Q-GRADER? Text in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(WhatsQPContent.equals(ExpWhatsQPContent))
		{
			System.out.println("WHAT�S A Q-GRADER? Text Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("WHAT�S A Q-GRADER? Text Content in Quality Page is not in match with creative - Fail");
			bw.write("WHAT�S A Q-GRADER? Text Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexWhatsQPColor.equals(ExpWhatsQPColor) && WhatsQPFontSize.equals(ExpWhatsQPFontSize) && WhatsQPFontFamily.equals(ExpWhatsQPFontFamily) && WhatsQPContent.equals(ExpWhatsQPContent))
		{
			Label WhatsQPCSSP = new Label(5,46,"PASS"); 
			wsheet3.addCell(WhatsQPCSSP); 
		}
		else
		{
			Label WhatsQPCSSF = new Label(5,46,"FAIL"); 
			wsheet3.addCell(WhatsQPCSSF); 
		}
		
		
		// WHAT�S A Q-GRADER? - Desc
		
		WebElement WhatsDescQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[12]/div/div/div/div/div[3]/div[2]/div[3]")); 
		String WhatsDescQPColor = WhatsDescQP.getCssValue("color");
		System.out.println("Actual Color of WHAT�S A Q-GRADER? Desc in Quality Page is : "	+ WhatsDescQPColor );
		// Convert RGB to Hex Value
		Color HWhatsDescQPColor = Color.fromString(WhatsDescQPColor);
		String HexWhatsDescQPColor = HWhatsDescQPColor.asHex();
		System.out.println("Actual Hex Color of WHAT�S A Q-GRADER? Desc in Quality Page is : "	+ HexWhatsDescQPColor );
		
		String WhatsDescQPFontSize = WhatsDescQP.getCssValue("font-size");
		System.out.println("Actual Font Size of WHAT�S A Q-GRADER? Desc in Quality Page is : "	+ WhatsDescQPFontSize);
		String WhatsDescQPFontFamily = WhatsDescQP.getCssValue("font-family");
		System.out.println("Actual Font Family of WHAT�S A Q-GRADER? Desc in Quality Page is : "	+ WhatsDescQPFontFamily);
		String WhatsDescQPContent = WhatsDescQP.getText();
		System.out.println("Actual Content of WHAT�S A Q-GRADER? Desc in Quality Page is : "	+ WhatsDescQPContent);
				
		String ExpWhatsDescQPColor = sheet3.getCell(1,47).getContents(); //column,row
		System.out.println("Expected Color of WHAT�S A Q-GRADER? Desc in Quality Page is : "	+ ExpWhatsDescQPColor );
		String ExpWhatsDescQPFontSize = sheet3.getCell(2,47).getContents();
		System.out.println("Expected Font Size of WHAT�S A Q-GRADER? Desc in Quality Page is : "	+ ExpWhatsDescQPFontSize);
		String ExpWhatsDescQPFontFamily = sheet3.getCell(3,47).getContents();
		System.out.println("Expected Font Family of WHAT�S A Q-GRADER? Desc in Quality Page is : "	+ ExpWhatsDescQPFontFamily);
		String ExpWhatsDescQPContent = sheet3.getCell(4,47).getContents();
		System.out.println("Expected Content of WHAT�S A Q-GRADER? Desc in Quality Page is : "	+ ExpWhatsDescQPContent);
				
		if(HexWhatsDescQPColor.equals(ExpWhatsDescQPColor))
		{
			System.out.println("WHAT�S A Q-GRADER? Desc in Quality Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("WHAT�S A Q-GRADER? Desc in Quality Page Color is not in match with creative - Fail");
			bw.write("WHAT�S A Q-GRADER? Desc in Quality Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(WhatsDescQPFontSize.equals(ExpWhatsDescQPFontSize))
		{
			System.out.println("WHAT�S A Q-GRADER? Desc in Quality Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("WHAT�S A Q-GRADER? Desc in Quality Page Font Size is not in match with creative - Fail");
			bw.write("WHAT�S A Q-GRADER? Desc in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(WhatsDescQPFontFamily.equals(ExpWhatsDescQPFontFamily))
		{
			System.out.println("WHAT�S A Q-GRADER? Desc in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("WHAT�S A Q-GRADER? Desc in Quality Page Font Family is not in match with creative - Fail");
			bw.write("WHAT�S A Q-GRADER? Desc in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(WhatsDescQPContent.equals(ExpWhatsDescQPContent))
		{
			System.out.println("WHAT�S A Q-GRADER? Desc Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("WHAT�S A Q-GRADER? Desc Content in Quality Page is not in match with creative - Fail");
			bw.write("WHAT�S A Q-GRADER? Desc Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexWhatsDescQPColor.equals(ExpWhatsDescQPColor) && WhatsDescQPFontSize.equals(ExpWhatsDescQPFontSize) && WhatsDescQPFontFamily.equals(ExpWhatsDescQPFontFamily) && WhatsDescQPContent.equals(ExpWhatsDescQPContent))
		{
			Label WhatsDescQPCSSP = new Label(5,47,"PASS"); 
			wsheet3.addCell(WhatsDescQPCSSP); 
		}
		else
		{
			Label WhatsDescQPCSSF = new Label(5,47,"FAIL"); 
			wsheet3.addCell(WhatsDescQPCSSF); 
		}
			
		// Quality Page - ART AND
 		
		WebElement QualityArtQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[12]/div/div/div/div/div[2]/div/div[1]")); 
		
		String QualityArtQPFontSize = QualityArtQP.getCssValue("font-size");
		System.out.println("Actual Font Size of ART AND Text in Quality Page is : "	+ QualityArtQPFontSize);
		String QualityArtQPFontFamily = QualityArtQP.getCssValue("font-family");
		System.out.println("Actual Font Family of ART AND Text in Quality Page is : "	+ QualityArtQPFontFamily);
		String QualityArtQPContent = QualityArtQP.getText();
		System.out.println("Actual Content of ART AND Text in Quality Page is : "	+ QualityArtQPContent);
				
		String ExpQualityArtQPFontSize = sheet3.getCell(2,48).getContents();
		System.out.println("Expected Font Size of ART AND Text in Quality Page is : "	+ ExpQualityArtQPFontSize);
		String ExpQualityArtQPFontFamily = sheet3.getCell(3,48).getContents();
		System.out.println("Expected Font Family of ART AND Text in Quality Page is : "	+ ExpQualityArtQPFontFamily);
		String ExpQualityArtQPContent = sheet3.getCell(4,48).getContents();
		System.out.println("Expected Content of ART AND Text in Quality Page is : "	+ ExpQualityArtQPContent);
				
				
		if(QualityArtQPFontSize.equals(ExpQualityArtQPFontSize))
		{
			System.out.println("ART AND Text in Quality Page Font Size is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("ART AND Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("ART AND Text in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(QualityArtQPFontFamily.equals(ExpQualityArtQPFontFamily))
		{
			System.out.println("ART AND Text in Quality Page Font Family is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("ART AND Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("ART AND Text in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(QualityArtQPContent.equals(ExpQualityArtQPContent))
		{
			System.out.println("ART AND Text Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("ART AND Text Content in Quality Page is not in match with creative - Fail");
			bw.write("ART AND Text Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(QualityArtQPFontSize.equals(ExpQualityArtQPFontSize) && QualityArtQPFontFamily.equals(ExpQualityArtQPFontFamily) && QualityArtQPContent.equals(ExpQualityArtQPContent))
		{
			Label QualityArtQPCSSP = new Label(5,48,"PASS"); 
			wsheet3.addCell(QualityArtQPCSSP); 
		}
		else
		{
			Label QualityArtQPCSSF = new Label(5,48,"FAIL"); 
			wsheet3.addCell(QualityArtQPCSSF); 
		}
		
		// Quality Page - SCIENCE
  		
		WebElement ScienceQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[12]/div/div/div/div/div[2]/div/div[2]")); 
		
		String ScienceQPFontSize = ScienceQP.getCssValue("font-size");
		System.out.println("Actual Font Size of SCIENCE Text in Quality Page is : "	+ ScienceQPFontSize);
		String ScienceQPFontFamily = ScienceQP.getCssValue("font-family");
		System.out.println("Actual Font Family of SCIENCE Text in Quality Page is : "	+ ScienceQPFontFamily);
		String ScienceQPContent = ScienceQP.getText();
		System.out.println("Actual Content of SCIENCE Text in Quality Page is : "	+ ScienceQPContent);
				
		String ExpScienceQPFontSize = sheet3.getCell(2,49).getContents();
		System.out.println("Expected Font Size of SCIENCE Text in Quality Page is : "	+ ExpScienceQPFontSize);
		String ExpScienceQPFontFamily = sheet3.getCell(3,49).getContents();
		System.out.println("Expected Font Family of SCIENCE Text in Quality Page is : "	+ ExpScienceQPFontFamily);
		String ExpScienceQPContent = sheet3.getCell(4,49).getContents();
		System.out.println("Expected Content of SCIENCE Text in Quality Page is : "	+ ExpScienceQPContent);
						
		if(ScienceQPFontSize.equals(ExpScienceQPFontSize))
		{
			System.out.println("SCIENCE Text in Quality Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("SCIENCE Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("SCIENCE Text in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(ScienceQPFontFamily.equals(ExpScienceQPFontFamily))
		{
			System.out.println("SCIENCE Text in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("SCIENCE Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("SCIENCE Text in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(ScienceQPContent.equals(ExpScienceQPContent))
		{
			System.out.println("SCIENCE Text Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("SCIENCE Text Content in Quality Page is not in match with creative - Fail");
			bw.write("SCIENCE Text Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(ScienceQPFontSize.equals(ExpScienceQPFontSize) && ScienceQPFontFamily.equals(ExpScienceQPFontFamily) && ScienceQPContent.equals(ExpScienceQPContent))
		{
			Label ScienceQPCSSP = new Label(5,49,"PASS"); 
			wsheet3.addCell(ScienceQPCSSP); 
		}
		else
		{
			Label ScienceQPCSSF = new Label(5,49,"FAIL"); 
			wsheet3.addCell(ScienceQPCSSF); 
		}
		
		// Quality Page - Art and Science Desc
		
		WebElement ArtDescQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[12]/div/div/div/div/div[2]/div/div[4]")); 
		
		String ArtDescQPFontSize = ArtDescQP.getCssValue("font-size");
		System.out.println("Actual Font Size of Art and Science Desc in Quality Page is : "	+ ArtDescQPFontSize);
		String ArtDescQPFontFamily = ArtDescQP.getCssValue("font-family");
		System.out.println("Actual Font Family of Art and Science Desc in Quality Page is : "	+ ArtDescQPFontFamily);
		String ArtDescQPContent = ArtDescQP.getText();
		System.out.println("Actual Content of Art and Science Desc in Quality Page is : "	+ ArtDescQPContent);
				
		String ExpArtDescQPFontSize = sheet3.getCell(2,50).getContents();
		System.out.println("Expected Font Size of Art and Science Desc in Quality Page is : "	+ ExpArtDescQPFontSize);
		String ExpArtDescQPFontFamily = sheet3.getCell(3,50).getContents();
		System.out.println("Expected Font Family of Art and Science Desc in Quality Page is : "	+ ExpArtDescQPFontFamily);
		String ExpArtDescQPContent = sheet3.getCell(4,50).getContents();
		System.out.println("Expected Content of Art and Science Desc in Quality Page is : "	+ ExpArtDescQPContent);
				
		if(ArtDescQPFontSize.equals(ExpArtDescQPFontSize))
		{
			System.out.println("Art and Science Desc in Quality Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Art and Science Desc in Quality Page Font Size is not in match with creative - Fail");
			bw.write("Art and Science Desc in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(ArtDescQPFontFamily.equals(ExpArtDescQPFontFamily))
		{
			System.out.println("Art and Science Desc in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Art and Science Desc in Quality Page Font Family is not in match with creative - Fail");
			bw.write("Art and Science Desc in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(ArtDescQPContent.equals(ExpArtDescQPContent))
		{
			System.out.println("Art and Science Desc Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Art and Science Desc Content in Quality Page is not in match with creative - Fail");
			bw.write("Art and Science Desc Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(ArtDescQPFontSize.equals(ExpArtDescQPFontSize) && ArtDescQPFontFamily.equals(ExpArtDescQPFontFamily) && ArtDescQPContent.equals(ExpArtDescQPContent))
		{
			Label ArtDescQPCSSP = new Label(5,50,"PASS"); 
			wsheet3.addCell(ArtDescQPCSSP); 
		}
		else
		{
			Label ArtDescQPCSSF = new Label(5,50,"FAIL"); 
			wsheet3.addCell(ArtDescQPCSSF); 
		}
		
		// Quality Page TEST 06
		
		WebElement Test06QP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[12]/div/div/div/div/div[6]/div/div[1]/div[1]")); 
		String Test06QPColor = Test06QP.getCssValue("color");
		System.out.println("Actual Color of TEST 06 Text in Quality Page is : "	+ Test06QPColor );
		// Convert RGB to Hex Value
		Color HTest06QPColor = Color.fromString(Test06QPColor);
		String HexTest06QPColor = HTest06QPColor.asHex();
		System.out.println("Actual Hex Color of TEST 06 Text in Quality Page is : "	+ HexTest06QPColor );
		
		String Test06QPFontSize = Test06QP.getCssValue("font-size");
		System.out.println("Actual Font Size of TEST 06 Text in Quality Page is : "	+ Test06QPFontSize);
		String Test06QPFontFamily = Test06QP.getCssValue("font-family");
		System.out.println("Actual Font Family of TEST 06 Text in Quality Page is : "	+ Test06QPFontFamily);
		String Test06QPContent = Test06QP.getText();
		System.out.println("Actual Content of TEST 06 Text in Quality Page is : "	+ Test06QPContent);
				
		String ExpTest06QPColor = sheet3.getCell(1,51).getContents(); //column,row
		System.out.println("Expected Color of TEST 06 Text in Quality Page is : "	+ ExpTest06QPColor );
		String ExpTest06QPFontSize = sheet3.getCell(2,51).getContents();
		System.out.println("Expected Font Size of TEST 06 Text in Quality Page is : "	+ ExpTest06QPFontSize);
		String ExpTest06QPFontFamily = sheet3.getCell(3,51).getContents();
		System.out.println("Expected Font Family of TEST 06 Text in Quality Page is : "	+ ExpTest06QPFontFamily);
		String ExpTest06QPContent = sheet3.getCell(4,51).getContents();
		System.out.println("Expected Content of TEST 06 Text in Quality Page is : "	+ ExpTest06QPContent);
				
		if(HexTest06QPColor.equals(ExpTest06QPColor))
		{
			System.out.println("TEST 06 Text in Quality Page Color is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("TEST 06 Text in Quality Page Color is not in match with creative - Fail");
			bw.write("TEST 06 Text in Quality Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(Test06QPFontSize.equals(ExpTest06QPFontSize))
		{
			System.out.println("TEST 06 Text in Quality Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 06 Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("TEST 06 Text in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(Test06QPFontFamily.equals(ExpTest06QPFontFamily))
		{
			System.out.println("TEST 06 Text in Quality Page Font Family is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("TEST 06 Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("TEST 06 Text in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(Test06QPContent.equals(ExpTest06QPContent))
		{
			System.out.println("TEST 06 Text Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 06 Text Content in Quality Page is not in match with creative - Fail");
			bw.write("TEST 06 Text Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexTest06QPColor.equals(ExpTest06QPColor) && Test06QPFontSize.equals(ExpTest06QPFontSize) && Test06QPFontFamily.equals(ExpTest06QPFontFamily) && Test06QPContent.equals(ExpTest06QPContent))
		{
			Label Test06QPCSSP = new Label(5,51,"PASS"); 
			wsheet3.addCell(Test06QPCSSP); 
		}
		else
		{
			Label Test06QPCSSF = new Label(5,51,"FAIL"); 
			wsheet3.addCell(Test06QPCSSF); 
		}
		
		// Quality Page TEST 06 - At the Coffee Lab
		
		WebElement Test06AtCoffeeQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[12]/div/div/div/div/div[6]/div/div[2]/div[1]")); 
		String Test06AtCoffeeQPColor = Test06AtCoffeeQP.getCssValue("color");
		System.out.println("Actual Color of TEST 06 - At the Coffee Lab Text in Quality Page is : "	+ Test06AtCoffeeQPColor );
		// Convert RGB to Hex Value
		Color HTest06AtCoffeeQPColor = Color.fromString(Test06AtCoffeeQPColor);
		String HexTest06AtCoffeeQPColor = HTest06AtCoffeeQPColor.asHex();
		System.out.println("Actual Hex Color of TEST 06 - At the Coffee Lab Text in Quality Page is : "	+ HexTest06AtCoffeeQPColor );
		
		String Test06AtCoffeeQPFontSize = Test06AtCoffeeQP.getCssValue("font-size");
		System.out.println("Actual Font Size of TEST 06 - At the Coffee Lab Text in Quality Page is : "	+ Test06AtCoffeeQPFontSize);
		String Test06AtCoffeeQPFontFamily = Test06AtCoffeeQP.getCssValue("font-family");
		System.out.println("Actual Font Family of TEST 06 - At the Coffee Lab Text in Quality Page is : "	+ Test06AtCoffeeQPFontFamily);
		String Test06AtCoffeeQPContent = Test06AtCoffeeQP.getText();
		System.out.println("Actual Content of TEST 06 - At the Coffee Lab Text in Quality Page is : "	+ Test06AtCoffeeQPContent);
				
		String ExpTest06AtCoffeeQPColor = sheet3.getCell(1,52).getContents(); //column,row
		System.out.println("Expected Color of TEST 06 - At the Coffee Lab Text in Quality Page is : "	+ ExpTest06AtCoffeeQPColor );
		String ExpTest06AtCoffeeQPFontSize = sheet3.getCell(2,52).getContents();
		System.out.println("Expected Font Size of TEST 06 - At the Coffee Lab Text in Quality Page is : "	+ ExpTest06AtCoffeeQPFontSize);
		String ExpTest06AtCoffeeQPFontFamily = sheet3.getCell(3,52).getContents();
		System.out.println("Expected Font Family of TEST 06 - At the Coffee Lab Text in Quality Page is : "	+ ExpTest06AtCoffeeQPFontFamily);
		String ExpTest06AtCoffeeQPContent = sheet3.getCell(4,52).getContents();
		System.out.println("Expected Content of TEST 06 - At the Coffee Lab Text in Quality Page is : "	+ ExpTest06AtCoffeeQPContent);
				
		if(HexTest06AtCoffeeQPColor.equals(ExpTest06AtCoffeeQPColor))
		{
			System.out.println("TEST 06 - At the Coffee Lab Text in Quality Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 06 - At the Coffee Lab Text in Quality Page Color is not in match with creative - Fail");
			bw.write("TEST 06 - At the Coffee Lab Text in Quality Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(Test06AtCoffeeQPFontSize.equals(ExpTest06AtCoffeeQPFontSize))
		{
			System.out.println("TEST 06 - At the Coffee Lab Text in Quality Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 06 - At the Coffee Lab Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("TEST 06 - At the Coffee Lab Text in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(Test06AtCoffeeQPFontFamily.equals(ExpTest06AtCoffeeQPFontFamily))
		{
			System.out.println("TEST 06 - At the Coffee Lab Text in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 06 - At the Coffee Lab Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("TEST 06 - At the Coffee Lab Text in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(Test06AtCoffeeQPContent.equals(ExpTest06AtCoffeeQPContent))
		{
			System.out.println("TEST 06 - At the Coffee Lab Text Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 06 - At the Coffee Lab Text Content in Quality Page is not in match with creative - Fail");
			bw.write("TEST 06 - At the Coffee Lab Text Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexTest06AtCoffeeQPColor.equals(ExpTest06AtCoffeeQPColor) && Test06AtCoffeeQPFontSize.equals(ExpTest06AtCoffeeQPFontSize) && Test06AtCoffeeQPFontFamily.equals(ExpTest06AtCoffeeQPFontFamily) && Test06AtCoffeeQPContent.equals(ExpTest06AtCoffeeQPContent))
		{
			Label Test06AtCoffeeQPCSSP = new Label(5,52,"PASS"); 
			wsheet3.addCell(Test06AtCoffeeQPCSSP); 
		}
		else
		{
			Label Test06AtCoffeeQPCSSF = new Label(5,52,"FAIL"); 
			wsheet3.addCell(Test06AtCoffeeQPCSSF); 
		}
		
		
		// Quality Page TEST 06 Desc
		
		WebElement Test06DescQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[12]/div/div/div/div/div[6]/div/div[2]/div[2]")); 
		String Test06DescQPColor = Test06DescQP.getCssValue("color");
		System.out.println("Actual Color of TEST 06 Desc in Quality Page is : "	+ Test06DescQPColor );
		// Convert RGB to Hex Value
		Color HTest06DescQPColor = Color.fromString(Test06DescQPColor);
		String HexTest06DescQPColor = HTest06DescQPColor.asHex();
		System.out.println("Actual Hex Color of TEST 06 Desc in Quality Page is : "	+ HexTest06DescQPColor );
		
		String Test06DescQPFontSize = Test06DescQP.getCssValue("font-size");
		System.out.println("Actual Font Size of TEST 06 Desc in Quality Page is : "	+ Test06DescQPFontSize);
		String Test06DescQPFontFamily = Test06DescQP.getCssValue("font-family");
		System.out.println("Actual Font Family of TEST 06 Desc in Quality Page is : "	+ Test06DescQPFontFamily);
		String Test06DescQPContent = Test06DescQP.getText();
		System.out.println("Actual Content of TEST 06 Desc in Quality Page is : "	+ Test06DescQPContent);
				
		String ExpTest06DescQPColor = sheet3.getCell(1,53).getContents(); //column,row
		System.out.println("Expected Color of TEST 06 Desc in Quality Page is : "	+ ExpTest06DescQPColor );
		String ExpTest06DescQPFontSize = sheet3.getCell(2,53).getContents();
		System.out.println("Expected Font Size of TEST 06 Desc in Quality Page is : "	+ ExpTest06DescQPFontSize);
		String ExpTest06DescQPFontFamily = sheet3.getCell(3,53).getContents();
		System.out.println("Expected Font Family of TEST 06 Desc in Quality Page is : "	+ ExpTest06DescQPFontFamily);
		String ExpTest06DescQPContent = sheet3.getCell(4,53).getContents();
		System.out.println("Expected Content of TEST 06 Desc in Quality Page is : "	+ ExpTest06DescQPContent);
				
		if(HexTest06DescQPColor.equals(ExpTest06DescQPColor))
		{
			System.out.println("TEST 06 Desc in Quality Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 06 Desc in Quality Page Color is not in match with creative - Fail");
			bw.write("TEST 06 Desc in Quality Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(Test06DescQPFontSize.equals(ExpTest06DescQPFontSize))
		{
			System.out.println("TEST 06 Desc in Quality Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 06 Desc in Quality Page Font Size is not in match with creative - Fail");
			bw.write("TEST 06 Desc in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(Test06DescQPFontFamily.equals(ExpTest06DescQPFontFamily))
		{
			System.out.println("TEST 06 Desc in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 06 Desc in Quality Page Font Family is not in match with creative - Fail");
			bw.write("TEST 06 Desc in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(Test06DescQPContent.equals(ExpTest06DescQPContent))
		{
			System.out.println("TEST 06 Desc Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 06 Desc Content in Quality Page is not in match with creative - Fail");
			bw.write("TEST 06 Desc Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexTest06DescQPColor.equals(ExpTest06DescQPColor) && Test06DescQPFontSize.equals(ExpTest06DescQPFontSize) && Test06DescQPFontFamily.equals(ExpTest06DescQPFontFamily) && Test06DescQPContent.equals(ExpTest06DescQPContent))
		{
			Label Test06DescQPCSSP = new Label(5,53,"PASS"); 
			wsheet3.addCell(Test06DescQPCSSP); 
		}
		else
		{
			Label Test06DescQPCSSF = new Label(5,53,"FAIL"); 
			wsheet3.addCell(Test06DescQPCSSF); 
		}
		
		Thread.sleep(3000);
		JavascriptExecutor jsetypes = (JavascriptExecutor)driver;
		jsetypes.executeScript("window.scrollBy(0,500)", "");
		Thread.sleep(8000);
	
		// Quality Page - PERFECTLY 
 		
		WebElement QualityPerfectlyQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[13]/div/div/div/div/div[2]/div/div[1]")); 
		
		String QualityPerfectlyQPFontSize = QualityPerfectlyQP.getCssValue("font-size");
		System.out.println("Actual Font Size of PERFECTLY Text in Quality Page is : "	+ QualityPerfectlyQPFontSize);
		String QualityPerfectlyQPFontFamily = QualityPerfectlyQP.getCssValue("font-family");
		System.out.println("Actual Font Family of PERFECTLY Text in Quality Page is : "	+ QualityPerfectlyQPFontFamily);
		String QualityPerfectlyQPContent = QualityPerfectlyQP.getText();
		System.out.println("Actual Content of PERFECTLY Text in Quality Page is : "	+ QualityPerfectlyQPContent);
				
		String ExpQualityPerfectlyQPFontSize = sheet3.getCell(2,54).getContents();
		System.out.println("Expected Font Size of PERFECTLY Text in Quality Page is : "	+ ExpQualityPerfectlyQPFontSize);
		String ExpQualityPerfectlyQPFontFamily = sheet3.getCell(3,54).getContents();
		System.out.println("Expected Font Family of PERFECTLY Text in Quality Page is : "	+ ExpQualityPerfectlyQPFontFamily);
		String ExpQualityPerfectlyQPContent = sheet3.getCell(4,54).getContents();
		System.out.println("Expected Content of PERFECTLY Text in Quality Page is : "	+ ExpQualityPerfectlyQPContent);
				
				
		if(QualityPerfectlyQPFontSize.equals(ExpQualityPerfectlyQPFontSize))
		{
			System.out.println("PERFECTLY Text in Quality Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("PERFECTLY Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("PERFECTLY Text in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(QualityPerfectlyQPFontFamily.equals(ExpQualityPerfectlyQPFontFamily))
		{
			System.out.println("PERFECTLY Text in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("PERFECTLY Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("PERFECTLY Text in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(QualityPerfectlyQPContent.equals(ExpQualityPerfectlyQPContent))
		{
			System.out.println("PERFECTLY Text Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("PERFECTLY Text Content in Quality Page is not in match with creative - Fail");
			bw.write("PERFECTLY Text Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(QualityPerfectlyQPFontSize.equals(ExpQualityPerfectlyQPFontSize) && QualityPerfectlyQPFontFamily.equals(ExpQualityPerfectlyQPFontFamily) && QualityPerfectlyQPContent.equals(ExpQualityPerfectlyQPContent))
		{
			Label QualityPerfectlyQPCSSP = new Label(5,54,"PASS"); 
			wsheet3.addCell(QualityPerfectlyQPCSSP); 
		}
		else
		{
			Label QualityPerfectlyQPCSSF = new Label(5,54,"FAIL"); 
			wsheet3.addCell(QualityPerfectlyQPCSSF); 
		}
		
		// Quality Page - ROASTED 
  		
		WebElement QualityRoastedQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[13]/div/div/div/div/div[2]/div/div[2]")); 
		
		String QualityRoastedQPFontSize = QualityRoastedQP.getCssValue("font-size");
		System.out.println("Actual Font Size of ROASTED Text in Quality Page is : "	+ QualityRoastedQPFontSize);
		String QualityRoastedQPFontFamily = QualityRoastedQP.getCssValue("font-family");
		System.out.println("Actual Font Family of ROASTED Text in Quality Page is : "	+ QualityRoastedQPFontFamily);
		String QualityRoastedQPContent = QualityRoastedQP.getText();
		System.out.println("Actual Content of ROASTED Text in Quality Page is : "	+ QualityRoastedQPContent);
				
		String ExpQualityRoastedQPFontSize = sheet3.getCell(2,55).getContents();
		System.out.println("Expected Font Size of ROASTED Text in Quality Page is : "	+ ExpQualityRoastedQPFontSize);
		String ExpQualityRoastedQPFontFamily = sheet3.getCell(3,55).getContents();
		System.out.println("Expected Font Family of ROASTED Text in Quality Page is : "	+ ExpQualityRoastedQPFontFamily);
		String ExpQualityRoastedQPContent = sheet3.getCell(4,55).getContents();
		System.out.println("Expected Content of ROASTED Text in Quality Page is : "	+ ExpQualityRoastedQPContent);
						
		if(QualityRoastedQPFontSize.equals(ExpQualityRoastedQPFontSize))
		{
			System.out.println("ROASTED Text in Quality Page Font Size is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("ROASTED Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("ROASTED Text in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(QualityRoastedQPFontFamily.equals(ExpQualityRoastedQPFontFamily))
		{
			System.out.println("ROASTED Text in Quality Page Font Family is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("ROASTED Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("ROASTED Text in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(QualityRoastedQPContent.equals(ExpQualityRoastedQPContent))
		{
			System.out.println("ROASTED Text Content in Quality Page is in match with creative - Pass");
		
		}
		else
		{
			System.out.println("ROASTED Text Content in Quality Page is not in match with creative - Fail");
			bw.write("ROASTED Text Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(QualityRoastedQPFontSize.equals(ExpQualityRoastedQPFontSize) && QualityRoastedQPFontFamily.equals(ExpQualityRoastedQPFontFamily) && QualityRoastedQPContent.equals(ExpQualityRoastedQPContent))
		{
			Label QualityRoastedQPCSSP = new Label(5,55,"PASS"); 
			wsheet3.addCell(QualityRoastedQPCSSP); 
		}
		else
		{
			Label QualityRoastedQPCSSF = new Label(5,55,"FAIL"); 
			wsheet3.addCell(QualityRoastedQPCSSF); 
		}
		
		// Quality Page - Perfectly Roasted Desc
		
		WebElement PRoastedDescQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[13]/div/div/div/div/div[2]/div/div[4]")); 
		
		String PRoastedDescQPFontSize = PRoastedDescQP.getCssValue("font-size");
		System.out.println("Actual Font Size of Perfectly Roasted Desc in Quality Page is : "	+ PRoastedDescQPFontSize);
		String PRoastedDescQPFontFamily = PRoastedDescQP.getCssValue("font-family");
		System.out.println("Actual Font Family of Perfectly Roasted Desc in Quality Page is : "	+ PRoastedDescQPFontFamily);
		String PRoastedDescQPContent = PRoastedDescQP.getText();
		System.out.println("Actual Content of Perfectly Roasted Desc in Quality Page is : "	+ PRoastedDescQPContent);
				
		String ExpPRoastedDescQPFontSize = sheet3.getCell(2,56).getContents();
		System.out.println("Expected Font Size of Perfectly Roasted Desc in Quality Page is : "	+ ExpPRoastedDescQPFontSize);
		String ExpPRoastedDescQPFontFamily = sheet3.getCell(3,56).getContents();
		System.out.println("Expected Font Family of Perfectly Roasted Desc in Quality Page is : "	+ ExpPRoastedDescQPFontFamily);
		String ExpPRoastedDescQPContent = sheet3.getCell(4,56).getContents();
		System.out.println("Expected Content of Perfectly Roasted Desc in Quality Page is : "	+ ExpPRoastedDescQPContent);
				
		if(PRoastedDescQPFontSize.equals(ExpPRoastedDescQPFontSize))
		{
			System.out.println("Perfectly Roasted Desc in Quality Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Perfectly Roasted Desc in Quality Page Font Size is not in match with creative - Fail");
			bw.write("Perfectly Roasted Desc in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(PRoastedDescQPFontFamily.equals(ExpPRoastedDescQPFontFamily))
		{
			System.out.println("Perfectly Roasted Desc in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Perfectly Roasted Desc in Quality Page Font Family is not in match with creative - Fail");
			bw.write("Perfectly Roasted Desc in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(PRoastedDescQPContent.equals(ExpPRoastedDescQPContent))
		{
			System.out.println("Perfectly Roasted Desc Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Perfectly Roasted Desc Content in Quality Page is not in match with creative - Fail");
			bw.write("Perfectly Roasted Desc Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(PRoastedDescQPFontSize.equals(ExpPRoastedDescQPFontSize) && PRoastedDescQPFontFamily.equals(ExpPRoastedDescQPFontFamily) && PRoastedDescQPContent.equals(ExpPRoastedDescQPContent))
		{
			Label PRoastedDescQPCSSP = new Label(5,56,"PASS"); 
			wsheet3.addCell(PRoastedDescQPCSSP); 
		}
		else
		{
			Label PRoastedDescQPCSSF = new Label(5,56,"FAIL"); 
			wsheet3.addCell(PRoastedDescQPCSSF); 
		}
		
		// Quality Page ROASTING TYPES
		
		WebElement RTypesQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[13]/div/div/div/div/div[3]/div[1]")); 
		String RTypesQPColor = RTypesQP.getCssValue("color");
		System.out.println("Actual Color of ROASTING TYPES Text in Quality Page is : "	+ RTypesQPColor );
		// Convert RGB to Hex Value
		Color HRTypesQPColor = Color.fromString(RTypesQPColor);
		String HexRTypesQPColor = HRTypesQPColor.asHex();
		System.out.println("Actual Hex Color of ROASTING TYPES Text in Quality Page is : "	+ HexRTypesQPColor );
		
		String RTypesQPFontSize = RTypesQP.getCssValue("font-size");
		System.out.println("Actual Font Size of ROASTING TYPES Text in Quality Page is : "	+ RTypesQPFontSize);
		String RTypesQPFontFamily = RTypesQP.getCssValue("font-family");
		System.out.println("Actual Font Family of ROASTING TYPES Text in Quality Page is : "	+ RTypesQPFontFamily);
		String RTypesQPContent = RTypesQP.getText();
		System.out.println("Actual Content of ROASTING TYPES Text in Quality Page is : "	+ RTypesQPContent);
				
		String ExpRTypesQPColor = sheet3.getCell(1,57).getContents(); //column,row
		System.out.println("Expected Color of ROASTING TYPES Text in Quality Page is : "	+ ExpRTypesQPColor );
		String ExpRTypesQPFontSize = sheet3.getCell(2,57).getContents();
		System.out.println("Expected Font Size of ROASTING TYPES Text in Quality Page is : "	+ ExpRTypesQPFontSize);
		String ExpRTypesQPFontFamily = sheet3.getCell(3,57).getContents();
		System.out.println("Expected Font Family of ROASTING TYPES Text in Quality Page is : "	+ ExpRTypesQPFontFamily);
		String ExpRTypesQPContent = sheet3.getCell(4,57).getContents();
		System.out.println("Expected Content of ROASTING TYPES Text in Quality Page is : "	+ ExpRTypesQPContent);
				
		if(HexRTypesQPColor.equals(ExpRTypesQPColor))
		{
			System.out.println("ROASTING TYPES Text in Quality Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("ROASTING TYPES Text in Quality Page Color is not in match with creative - Fail");
			bw.write("ROASTING TYPES Text in Quality Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(RTypesQPFontSize.equals(ExpRTypesQPFontSize))
		{
			System.out.println("ROASTING TYPES Text in Quality Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("ROASTING TYPES Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("ROASTING TYPES Text in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(RTypesQPFontFamily.equals(ExpRTypesQPFontFamily))
		{
			System.out.println("ROASTING TYPES Text in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("ROASTING TYPES Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("ROASTING TYPES Text in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(RTypesQPContent.equals(ExpRTypesQPContent))
		{
			System.out.println("ROASTING TYPES Text Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("ROASTING TYPES Text Content in Quality Page is not in match with creative - Fail");
			bw.write("ROASTING TYPES Text Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexRTypesQPColor.equals(ExpRTypesQPColor) && RTypesQPFontSize.equals(ExpRTypesQPFontSize) && RTypesQPFontFamily.equals(ExpRTypesQPFontFamily) && RTypesQPContent.equals(ExpRTypesQPContent))
		{
			Label RTypesQPCSSP = new Label(5,57,"PASS"); 
			wsheet3.addCell(RTypesQPCSSP); 
		}
		else
		{
			Label RTypesQPCSSF = new Label(5,57,"FAIL"); 
			wsheet3.addCell(RTypesQPCSSF); 
		}
		
		// Quality Page ROASTING TYPES - Light Roast
		
		WebElement LRoastQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[13]/div/div/div/div/div[3]/div[2]/div[2]/div[1]/div[1]")); 
		String LRoastQPColor = LRoastQP.getCssValue("color");
		System.out.println("Actual Color of Light Roast Text in Quality Page is : "	+ LRoastQPColor );
		// Convert RGB to Hex Value
		Color HLRoastQPColor = Color.fromString(LRoastQPColor);
		String HexLRoastQPColor = HLRoastQPColor.asHex();
		System.out.println("Actual Hex Color of Light Roast Text in Quality Page is : "	+ HexLRoastQPColor );
		
		String LRoastQPFontSize = LRoastQP.getCssValue("font-size");
		System.out.println("Actual Font Size of Light Roast Text in Quality Page is : "	+ LRoastQPFontSize);
		String LRoastQPFontFamily = LRoastQP.getCssValue("font-family");
		System.out.println("Actual Font Family of Light Roast Text in Quality Page is : "	+ LRoastQPFontFamily);
		String LRoastQPContent = LRoastQP.getText();
		System.out.println("Actual Content of Light Roast Text in Quality Page is : "	+ LRoastQPContent);
				
		String ExpLRoastQPColor = sheet3.getCell(1,58).getContents(); //column,row
		System.out.println("Expected Color of Light Roast Text in Quality Page is : "	+ ExpLRoastQPColor );
		String ExpLRoastQPFontSize = sheet3.getCell(2,58).getContents();
		System.out.println("Expected Font Size of Light Roast Text in Quality Page is : "	+ ExpLRoastQPFontSize);
		String ExpLRoastQPFontFamily = sheet3.getCell(3,58).getContents();
		System.out.println("Expected Font Family of Light Roast Text in Quality Page is : "	+ ExpLRoastQPFontFamily);
		String ExpLRoastQPContent = sheet3.getCell(4,58).getContents();
		System.out.println("Expected Content of Light Roast Text in Quality Page is : "	+ ExpLRoastQPContent);
				
		if(HexLRoastQPColor.equals(ExpLRoastQPColor))
		{
			System.out.println("Light Roast Text in Quality Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Light Roast Text in Quality Page Color is not in match with creative - Fail");
			bw.write("Light Roast Text in Quality Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(LRoastQPFontSize.equals(ExpLRoastQPFontSize))
		{
			System.out.println("Light Roast Text in Quality Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Light Roast Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("Light Roast Text in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(LRoastQPFontFamily.equals(ExpLRoastQPFontFamily))
		{
			System.out.println("Light Roast Text in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Light Roast Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("Light Roast Text in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(LRoastQPContent.equals(ExpLRoastQPContent))
		{
			System.out.println("Light Roast Text Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Light Roast Text Content in Quality Page is not in match with creative - Fail");
			bw.write("Light Roast Text Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexLRoastQPColor.equals(ExpLRoastQPColor) && LRoastQPFontSize.equals(ExpLRoastQPFontSize) && LRoastQPFontFamily.equals(ExpLRoastQPFontFamily) && LRoastQPContent.equals(ExpLRoastQPContent))
		{
			Label LRoastQPCSSP = new Label(5,58,"PASS"); 
			wsheet3.addCell(LRoastQPCSSP); 
		}
		else
		{
			Label LRoastQPCSSF = new Label(5,58,"FAIL"); 
			wsheet3.addCell(LRoastQPCSSF); 
		}
			
		// Quality Page ROASTING TYPES - Light Roast - Bright and smooth 
		
		WebElement BsmoothQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[13]/div/div/div/div/div[3]/div[2]/div[2]/div[1]/div[2]")); 
		
		String BsmoothQPFontSize = BsmoothQP.getCssValue("font-size");
		System.out.println("Actual Font Size of Bright and smooth in Quality Page is : "	+ BsmoothQPFontSize);
		String BsmoothQPFontFamily = BsmoothQP.getCssValue("font-family");
		System.out.println("Actual Font Family of Bright and smooth in Quality Page is : "	+ BsmoothQPFontFamily);
		String BsmoothQPContent = BsmoothQP.getText();
		System.out.println("Actual Content of Bright and smooth in Quality Page is : "	+ BsmoothQPContent);
				
		String ExpBsmoothQPFontSize = sheet3.getCell(2,59).getContents();
		System.out.println("Expected Font Size of Bright and smooth in Quality Page is : "	+ ExpBsmoothQPFontSize);
		String ExpBsmoothQPFontFamily = sheet3.getCell(3,59).getContents();
		System.out.println("Expected Font Family of Bright and smooth in Quality Page is : "	+ ExpBsmoothQPFontFamily);
		String ExpBsmoothQPContent = sheet3.getCell(4,59).getContents();
		System.out.println("Expected Content of Bright and smooth in Quality Page is : "	+ ExpBsmoothQPContent);
				
				
		if(BsmoothQPFontSize.equals(ExpBsmoothQPFontSize))
		{
			System.out.println("Bright and smooth Text in Quality Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Bright and smooth Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("Bright and smooth Text in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(BsmoothQPFontFamily.equals(ExpBsmoothQPFontFamily))
		{
			System.out.println("Bright and smooth Text in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Bright and smooth Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("Bright and smooth Text in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(BsmoothQPContent.equals(ExpBsmoothQPContent))
		{
			System.out.println("Bright and smooth Text Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Bright and smooth Text Content in Quality Page is not in match with creative - Fail");
			bw.write("Bright and smooth Text Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(BsmoothQPFontSize.equals(ExpBsmoothQPFontSize) && BsmoothQPFontFamily.equals(ExpBsmoothQPFontFamily) && BsmoothQPContent.equals(ExpBsmoothQPContent))
		{
			Label BsmoothQPCSSP = new Label(5,59,"PASS"); 
			wsheet3.addCell(BsmoothQPCSSP); 
		}
		else
		{
			Label BsmoothQPCSSF = new Label(5,59,"FAIL"); 
			wsheet3.addCell(BsmoothQPCSSF); 
		}
		
		
		// Quality Page ROASTING TYPES - Medium Roast
		
		WebElement MRoastQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[13]/div/div/div/div/div[3]/div[2]/div[2]/div[2]/div[1]")); 
		String MRoastQPColor = MRoastQP.getCssValue("color");
		System.out.println("Actual Color of Medium Roast Text in Quality Page is : "	+ MRoastQPColor );
		// Convert RGB to Hex Value
		Color HMRoastQPColor = Color.fromString(MRoastQPColor);
		String HexMRoastQPColor = HMRoastQPColor.asHex();
		System.out.println("Actual Hex Color of Medium Roast Text in Quality Page is : "	+ HexMRoastQPColor );
		
		String MRoastQPFontSize = MRoastQP.getCssValue("font-size");
		System.out.println("Actual Font Size of Medium Roast Text in Quality Page is : "	+ MRoastQPFontSize);
		String MRoastQPFontFamily = MRoastQP.getCssValue("font-family");
		System.out.println("Actual Font Family of Medium Roast Text in Quality Page is : "	+ MRoastQPFontFamily);
		String MRoastQPContent = MRoastQP.getText();
		System.out.println("Actual Content of Medium Roast Text in Quality Page is : "	+ MRoastQPContent);
				
		String ExpMRoastQPColor = sheet3.getCell(1,60).getContents(); //column,row
		System.out.println("Expected Color of Medium Roast Text in Quality Page is : "	+ ExpMRoastQPColor );
		String ExpMRoastQPFontSize = sheet3.getCell(2,60).getContents();
		System.out.println("Expected Font Size of Medium Roast Text in Quality Page is : "	+ ExpMRoastQPFontSize);
		String ExpMRoastQPFontFamily = sheet3.getCell(3,60).getContents();
		System.out.println("Expected Font Family of Medium Roast Text in Quality Page is : "	+ ExpMRoastQPFontFamily);
		String ExpMRoastQPContent = sheet3.getCell(4,60).getContents();
		System.out.println("Expected Content of Medium Roast Text in Quality Page is : "	+ ExpMRoastQPContent);
				
		if(HexMRoastQPColor.equals(ExpMRoastQPColor))
		{
			System.out.println("Medium Roast Text in Quality Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Medium Roast Text in Quality Page Color is not in match with creative - Fail");
			bw.write("Medium Roast Text in Quality Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(MRoastQPFontSize.equals(ExpMRoastQPFontSize))
		{
			System.out.println("Medium Roast Text in Quality Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Medium Roast Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("Medium Roast Text in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(MRoastQPFontFamily.equals(ExpMRoastQPFontFamily))
		{
			System.out.println("Medium Roast Text in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Medium Roast Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("Medium Roast Text in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(MRoastQPContent.equals(ExpMRoastQPContent))
		{
			System.out.println("Medium Roast Text Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Medium Roast Text Content in Quality Page is not in match with creative - Fail");
			bw.write("Medium Roast Text Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexMRoastQPColor.equals(ExpMRoastQPColor) && MRoastQPFontSize.equals(ExpMRoastQPFontSize) && MRoastQPFontFamily.equals(ExpMRoastQPFontFamily) && MRoastQPContent.equals(ExpMRoastQPContent))
		{
			Label MRoastQPCSSP = new Label(5,60,"PASS"); 
			wsheet3.addCell(MRoastQPCSSP); 
		}
		else
		{
			Label MRoastQPCSSF = new Label(5,60,"FAIL"); 
			wsheet3.addCell(MRoastQPCSSF); 
		}
		
		// Quality Page ROASTING TYPES - Medium Roast - Well-balanced with subtle nuances 
		
		WebElement WBalancedQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[13]/div/div/div/div/div[3]/div[2]/div[2]/div[2]/div[2]")); 
		
		String WBalancedQPFontSize = WBalancedQP.getCssValue("font-size");
		System.out.println("Actual Font Size of Well-balanced with subtle nuances in Quality Page is : "	+ WBalancedQPFontSize);
		String WBalancedQPFontFamily = WBalancedQP.getCssValue("font-family");
		System.out.println("Actual Font Family of Well-balanced with subtle nuances in Quality Page is : "	+ WBalancedQPFontFamily);
		String WBalancedQPContent = WBalancedQP.getText();
		System.out.println("Actual Content of Well-balanced with subtle nuances in Quality Page is : "	+ WBalancedQPContent);
				
		String ExpWBalancedQPFontSize = sheet3.getCell(2,61).getContents();
		System.out.println("Expected Font Size of Well-balanced with subtle nuances in Quality Page is : "	+ ExpWBalancedQPFontSize);
		String ExpWBalancedQPFontFamily = sheet3.getCell(3,61).getContents();
		System.out.println("Expected Font Family of Well-balanced with subtle nuances in Quality Page is : "	+ ExpWBalancedQPFontFamily);
		String ExpWBalancedQPContent = sheet3.getCell(4,61).getContents();
		System.out.println("Expected Content of Well-balanced with subtle nuances in Quality Page is : "	+ ExpWBalancedQPContent);
				
				
		if(WBalancedQPFontSize.equals(ExpWBalancedQPFontSize))
		{
			System.out.println("Well-balanced with subtle nuances Text in Quality Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Well-balanced with subtle nuances Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("Well-balanced with subtle nuances Text in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(WBalancedQPFontFamily.equals(ExpWBalancedQPFontFamily))
		{
			System.out.println("Well-balanced with subtle nuances Text in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Well-balanced with subtle nuances Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("Well-balanced with subtle nuances Text in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(WBalancedQPContent.equals(ExpWBalancedQPContent))
		{
			System.out.println("Well-balanced with subtle nuances Text Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Well-balanced with subtle nuances Text Content in Quality Page is not in match with creative - Fail");
			bw.write("Well-balanced with subtle nuances Text Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(WBalancedQPFontSize.equals(ExpWBalancedQPFontSize) && WBalancedQPFontFamily.equals(ExpWBalancedQPFontFamily) && WBalancedQPContent.equals(ExpWBalancedQPContent))
		{
			Label WBalancedQPCSSP = new Label(5,61,"PASS"); 
			wsheet3.addCell(WBalancedQPCSSP); 
		}
		else
		{
			Label WBalancedQPCSSF = new Label(5,61,"FAIL"); 
			wsheet3.addCell(WBalancedQPCSSF); 
		}
		
		Thread.sleep(3000);
		JavascriptExecutor jsedark = (JavascriptExecutor)driver;
		jsedark.executeScript("window.scrollBy(0,500)", "");
		Thread.sleep(8000);
				
		// Quality Page ROASTING TYPES - Dark Roast
		
		WebElement DRoastQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[13]/div/div/div/div/div[3]/div[2]/div[2]/div[3]/div[1]")); 
		String DRoastQPColor = DRoastQP.getCssValue("color");
		System.out.println("Actual Color of Dark Roast Text in Quality Page is : "	+ DRoastQPColor );
		// Convert RGB to Hex Value
		Color HDRoastQPColor = Color.fromString(DRoastQPColor);
		String HexDRoastQPColor = HDRoastQPColor.asHex();
		System.out.println("Actual Hex Color of Dark Roast Text in Quality Page is : "	+ HexDRoastQPColor );
		
		String DRoastQPFontSize = DRoastQP.getCssValue("font-size");
		System.out.println("Actual Font Size of Dark Roast Text in Quality Page is : "	+ DRoastQPFontSize);
		String DRoastQPFontFamily = DRoastQP.getCssValue("font-family");
		System.out.println("Actual Font Family of Dark Roast Text in Quality Page is : "	+ DRoastQPFontFamily);
		String DRoastQPContent = DRoastQP.getText();
		System.out.println("Actual Content of Dark Roast Text in Quality Page is : "	+ DRoastQPContent);
				
		String ExpDRoastQPColor = sheet3.getCell(1,62).getContents(); //column,row
		System.out.println("Expected Color of Dark Roast Text in Quality Page is : "	+ ExpDRoastQPColor );
		String ExpDRoastQPFontSize = sheet3.getCell(2,62).getContents();
		System.out.println("Expected Font Size of Dark Roast Text in Quality Page is : "	+ ExpDRoastQPFontSize);
		String ExpDRoastQPFontFamily = sheet3.getCell(3,62).getContents();
		System.out.println("Expected Font Family of Dark Roast Text in Quality Page is : "	+ ExpDRoastQPFontFamily);
		String ExpDRoastQPContent = sheet3.getCell(4,62).getContents();
		System.out.println("Expected Content of Dark Roast Text in Quality Page is : "	+ ExpDRoastQPContent);
				
		if(HexDRoastQPColor.equals(ExpDRoastQPColor))
		{
			System.out.println("Dark Roast Text in Quality Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Dark Roast Text in Quality Page Color is not in match with creative - Fail");
			bw.write("Dark Roast Text in Quality Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(DRoastQPFontSize.equals(ExpDRoastQPFontSize))
		{
			System.out.println("Dark Roast Text in Quality Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Dark Roast Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("Dark Roast Text in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(DRoastQPFontFamily.equals(ExpDRoastQPFontFamily))
		{
			System.out.println("Dark Roast Text in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Dark Roast Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("Dark Roast Text in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(DRoastQPContent.equals(ExpDRoastQPContent))
		{
			System.out.println("Dark Roast Text Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Dark Roast Text Content in Quality Page is not in match with creative - Fail");
			bw.write("Dark Roast Text Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexDRoastQPColor.equals(ExpDRoastQPColor) && DRoastQPFontSize.equals(ExpDRoastQPFontSize) && DRoastQPFontFamily.equals(ExpDRoastQPFontFamily) && DRoastQPContent.equals(ExpDRoastQPContent))
		{
			Label DRoastQPCSSP = new Label(5,62,"PASS"); 
			wsheet3.addCell(DRoastQPCSSP); 
		}
		else
		{
			Label DRoastQPCSSF = new Label(5,62,"FAIL"); 
			wsheet3.addCell(DRoastQPCSSF); 
		}
		
		// Quality Page ROASTING TYPES - Dark Roast - Full-bodied and rich 
		
		WebElement FBodiedQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[13]/div/div/div/div/div[3]/div[2]/div[2]/div[3]/div[2]")); 
		
		String FBodiedQPFontSize = FBodiedQP.getCssValue("font-size");
		System.out.println("Actual Font Size of Full-bodied and rich in Quality Page is : "	+ FBodiedQPFontSize);
		String FBodiedQPFontFamily = FBodiedQP.getCssValue("font-family");
		System.out.println("Actual Font Family of Full-bodied and rich in Quality Page is : "	+ FBodiedQPFontFamily);
		String FBodiedQPContent = FBodiedQP.getText();
		System.out.println("Actual Content of Full-bodied and rich in Quality Page is : "	+ FBodiedQPContent);
				
		String ExpFBodiedQPFontSize = sheet3.getCell(2,63).getContents();
		System.out.println("Expected Font Size of Full-bodied and rich in Quality Page is : "	+ ExpFBodiedQPFontSize);
		String ExpFBodiedQPFontFamily = sheet3.getCell(3,63).getContents();
		System.out.println("Expected Font Family of Full-bodied and rich in Quality Page is : "	+ ExpFBodiedQPFontFamily);
		String ExpFBodiedQPContent = sheet3.getCell(4,63).getContents();
		System.out.println("Expected Content of Full-bodied and rich in Quality Page is : "	+ ExpFBodiedQPContent);
				
				
		if(FBodiedQPFontSize.equals(ExpFBodiedQPFontSize))
		{
			System.out.println("Full-bodied and rich Text in Quality Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Full-bodied and rich Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("Full-bodied and rich Text in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(FBodiedQPFontFamily.equals(ExpFBodiedQPFontFamily))
		{
			System.out.println("Full-bodied and rich Text in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Full-bodied and rich Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("Full-bodied and rich Text in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(FBodiedQPContent.equals(ExpFBodiedQPContent))
		{
			System.out.println("Full-bodied and rich Text Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Full-bodied and rich Text Content in Quality Page is not in match with creative - Fail");
			bw.write("Full-bodied and rich Text Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(FBodiedQPFontSize.equals(ExpFBodiedQPFontSize) && FBodiedQPFontFamily.equals(ExpFBodiedQPFontFamily) && FBodiedQPContent.equals(ExpFBodiedQPContent))
		{
			Label FBodiedQPCSSP = new Label(5,63,"PASS"); 
			wsheet3.addCell(FBodiedQPCSSP); 
		}
		else
		{
			Label FBodiedQPCSSF = new Label(5,63,"FAIL"); 
			wsheet3.addCell(FBodiedQPCSSF); 
		}
		
		// Quality Page - FINISHING 
 		
		WebElement FinishingQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[14]/div/div/div/div/div[2]/div/div[1]")); 
		
		String FinishingQPFontSize = FinishingQP.getCssValue("font-size");
		System.out.println("Actual Font Size of FINISHING Text in Quality Page is : "	+ FinishingQPFontSize);
		String FinishingQPFontFamily = FinishingQP.getCssValue("font-family");
		System.out.println("Actual Font Family of FINISHING Text in Quality Page is : "	+ FinishingQPFontFamily);
		String FinishingQPContent = FinishingQP.getText();
		System.out.println("Actual Content of FINISHING Text in Quality Page is : "	+ FinishingQPContent);
				
		String ExpFinishingQPFontSize = sheet3.getCell(2,64).getContents();
		System.out.println("Expected Font Size of FINISHING Text in Quality Page is : "	+ ExpFinishingQPFontSize);
		String ExpFinishingQPFontFamily = sheet3.getCell(3,64).getContents();
		System.out.println("Expected Font Family of FINISHING Text in Quality Page is : "	+ ExpFinishingQPFontFamily);
		String ExpFinishingQPContent = sheet3.getCell(4,64).getContents();
		System.out.println("Expected Content of FINISHING Text in Quality Page is : "	+ ExpFinishingQPContent);
				
				
		if(FinishingQPFontSize.equals(ExpFinishingQPFontSize))
		{
			System.out.println("FINISHING Text in Quality Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("FINISHING Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("FINISHING Text in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(FinishingQPFontFamily.equals(ExpFinishingQPFontFamily))
		{
			System.out.println("FINISHING Text in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("FINISHING Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("FINISHING Text in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(FinishingQPContent.equals(ExpFinishingQPContent))
		{
			System.out.println("FINISHING Text Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("FINISHING Text Content in Quality Page is not in match with creative - Fail");
			bw.write("FINISHING Text Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(FinishingQPFontSize.equals(ExpFinishingQPFontSize) && FinishingQPFontFamily.equals(ExpFinishingQPFontFamily) && FinishingQPContent.equals(ExpFinishingQPContent))
		{
			Label FinishingQPCSSP = new Label(5,64,"PASS"); 
			wsheet3.addCell(FinishingQPCSSP); 
		}
		else
		{
			Label FinishingQPCSSF = new Label(5,64,"FAIL"); 
			wsheet3.addCell(FinishingQPCSSF); 
		}
		
		// Quality Page - TOUCHES 
   		
		WebElement TouchesQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[14]/div/div/div/div/div[2]/div/div[2]")); 
		
		String TouchesQPFontSize = TouchesQP.getCssValue("font-size");
		System.out.println("Actual Font Size of TOUCHES Text in Quality Page is : "	+ TouchesQPFontSize);
		String TouchesQPFontFamily = TouchesQP.getCssValue("font-family");
		System.out.println("Actual Font Family of TOUCHES Text in Quality Page is : "	+ TouchesQPFontFamily);
		String TouchesQPContent = TouchesQP.getText();
		System.out.println("Actual Content of TOUCHES Text in Quality Page is : "	+ TouchesQPContent);
				
		String ExpTouchesQPFontSize = sheet3.getCell(2,65).getContents();
		System.out.println("Expected Font Size of TOUCHES Text in Quality Page is : "	+ ExpTouchesQPFontSize);
		String ExpTouchesQPFontFamily = sheet3.getCell(3,65).getContents();
		System.out.println("Expected Font Family of TOUCHES Text in Quality Page is : "	+ ExpTouchesQPFontFamily);
		String ExpTouchesQPContent = sheet3.getCell(4,65).getContents();
		System.out.println("Expected Content of TOUCHES Text in Quality Page is : "	+ ExpTouchesQPContent);
						
		if(TouchesQPFontSize.equals(ExpTouchesQPFontSize))
		{
			System.out.println("TOUCHES Text in Quality Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TOUCHES Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("TOUCHES Text in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(TouchesQPFontFamily.equals(ExpTouchesQPFontFamily))
		{
			System.out.println("TOUCHES Text in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TOUCHES Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("TOUCHES Text in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(TouchesQPContent.equals(ExpTouchesQPContent))
		{
			System.out.println("TOUCHES Text Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TOUCHES Text Content in Quality Page is not in match with creative - Fail");
			bw.write("TOUCHES Text Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(TouchesQPFontSize.equals(ExpTouchesQPFontSize) && TouchesQPFontFamily.equals(ExpTouchesQPFontFamily) && TouchesQPContent.equals(ExpTouchesQPContent))
		{
			Label TouchesQPCSSP = new Label(5,65,"PASS"); 
			wsheet3.addCell(TouchesQPCSSP); 
		}
		else
		{
			Label TouchesQPCSSF = new Label(5,65,"FAIL"); 
			wsheet3.addCell(TouchesQPCSSF); 
		}
		
		// Quality Page - Finishing Touches Desc
		
		WebElement FTouchesDescQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[14]/div/div/div/div/div[2]/div/div[4]")); 
		
		String FTouchesDescQPFontSize = FTouchesDescQP.getCssValue("font-size");
		System.out.println("Actual Font Size of Finishing Touches Desc in Quality Page is : "	+ FTouchesDescQPFontSize);
		String FTouchesDescQPFontFamily = FTouchesDescQP.getCssValue("font-family");
		System.out.println("Actual Font Family of Finishing Touches Desc in Quality Page is : "	+ FTouchesDescQPFontFamily);
		String FTouchesDescQPContent = FTouchesDescQP.getText();
		System.out.println("Actual Content of Finishing Touches Desc in Quality Page is : "	+ FTouchesDescQPContent);
				
		String ExpFTouchesDescQPFontSize = sheet3.getCell(2,66).getContents();
		System.out.println("Expected Font Size of Finishing Touches Desc in Quality Page is : "	+ ExpFTouchesDescQPFontSize);
		String ExpFTouchesDescQPFontFamily = sheet3.getCell(3,66).getContents();
		System.out.println("Expected Font Family of Finishing Touches Desc in Quality Page is : "	+ ExpFTouchesDescQPFontFamily);
		String ExpFTouchesDescQPContent = sheet3.getCell(4,66).getContents();
		System.out.println("Expected Content of Finishing Touches Desc in Quality Page is : "	+ ExpFTouchesDescQPContent);
				
		if(FTouchesDescQPFontSize.equals(ExpFTouchesDescQPFontSize))
		{
			System.out.println("Finishing Touches Desc in Quality Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Finishing Touches Desc in Quality Page Font Size is not in match with creative - Fail");
			bw.write("Finishing Touches Desc in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(FTouchesDescQPFontFamily.equals(ExpFTouchesDescQPFontFamily))
		{
			System.out.println("Finishing Touches Desc in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Finishing Touches Desc in Quality Page Font Family is not in match with creative - Fail");
			bw.write("Finishing Touches Desc in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(FTouchesDescQPContent.equals(ExpFTouchesDescQPContent))
		{
			System.out.println("Finishing Touches Desc Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("Finishing Touches Desc Content in Quality Page is not in match with creative - Fail");
			bw.write("Finishing Touches Desc Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(FTouchesDescQPFontSize.equals(ExpFTouchesDescQPFontSize) && FTouchesDescQPFontFamily.equals(ExpFTouchesDescQPFontFamily) && FTouchesDescQPContent.equals(ExpFTouchesDescQPContent))
		{
			Label FTouchesDescQPCSSP = new Label(5,66,"PASS"); 
			wsheet3.addCell(FTouchesDescQPCSSP); 
		}
		else
		{
			Label FTouchesDescQPCSSF = new Label(5,66,"FAIL"); 
			wsheet3.addCell(FTouchesDescQPCSSF); 
		}
		
		
		// Quality Page TEST 07
		
		WebElement Test07QP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[14]/div/div/div/div/div[6]/div/div[1]/div[1]")); 
		String Test07QPColor = Test07QP.getCssValue("color");
		System.out.println("Actual Color of TEST 07 Text in Quality Page is : "	+ Test07QPColor );
		// Convert RGB to Hex Value
		Color HTest07QPColor = Color.fromString(Test07QPColor);
		String HexTest07QPColor = HTest07QPColor.asHex();
		System.out.println("Actual Hex Color of TEST 07 Text in Quality Page is : "	+ HexTest07QPColor );
		
		String Test07QPFontSize = Test07QP.getCssValue("font-size");
		System.out.println("Actual Font Size of TEST 07 Text in Quality Page is : "	+ Test07QPFontSize);
		String Test07QPFontFamily = Test07QP.getCssValue("font-family");
		System.out.println("Actual Font Family of TEST 07 Text in Quality Page is : "	+ Test07QPFontFamily);
		String Test07QPContent = Test07QP.getText();
		System.out.println("Actual Content of TEST 07 Text in Quality Page is : "	+ Test07QPContent);
				
		String ExpTest07QPColor = sheet3.getCell(1,67).getContents(); //column,row
		System.out.println("Expected Color of TEST 07 Text in Quality Page is : "	+ ExpTest07QPColor );
		String ExpTest07QPFontSize = sheet3.getCell(2,67).getContents();
		System.out.println("Expected Font Size of TEST 07 Text in Quality Page is : "	+ ExpTest07QPFontSize);
		String ExpTest07QPFontFamily = sheet3.getCell(3,67).getContents();
		System.out.println("Expected Font Family of TEST 07 Text in Quality Page is : "	+ ExpTest07QPFontFamily);
		String ExpTest07QPContent = sheet3.getCell(4,67).getContents();
		System.out.println("Expected Content of TEST 07 Text in Quality Page is : "	+ ExpTest07QPContent);
				
		if(HexTest07QPColor.equals(ExpTest07QPColor))
		{
			System.out.println("TEST 07 Text in Quality Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 07 Text in Quality Page Color is not in match with creative - Fail");
			bw.write("TEST 07 Text in Quality Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(Test07QPFontSize.equals(ExpTest07QPFontSize))
		{
			System.out.println("TEST 07 Text in Quality Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 07 Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("TEST 07 Text in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(Test07QPFontFamily.equals(ExpTest07QPFontFamily))
		{
			System.out.println("TEST 07 Text in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 07 Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("TEST 07 Text in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(Test07QPContent.equals(ExpTest07QPContent))
		{
			System.out.println("TEST 07 Text Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 07 Text Content in Quality Page is not in match with creative - Fail");
			bw.write("TEST 07 Text Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexTest07QPColor.equals(ExpTest07QPColor) && Test07QPFontSize.equals(ExpTest07QPFontSize) && Test07QPFontFamily.equals(ExpTest07QPFontFamily) && Test07QPContent.equals(ExpTest07QPContent))
		{
			Label Test07QPCSSP = new Label(5,67,"PASS"); 
			wsheet3.addCell(Test07QPCSSP); 
		}
		else
		{
			Label Test07QPCSSF = new Label(5,67,"FAIL"); 
			wsheet3.addCell(Test07QPCSSF); 
		}
		
		
		// Quality Page TEST 07 - At the Production Plant
		
		WebElement Test07AtPlantQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[14]/div/div/div/div/div[6]/div/div[2]/div[1]")); 
		String Test07AtPlantQPColor = Test07AtPlantQP.getCssValue("color");
		System.out.println("Actual Color of TEST 07 - At the Production Plant Text in Quality Page is : "	+ Test07AtPlantQPColor );
		// Convert RGB to Hex Value
		Color HTest07AtPlantQPColor = Color.fromString(Test07AtPlantQPColor);
		String HexTest07AtPlantQPColor = HTest07AtPlantQPColor.asHex();
		System.out.println("Actual Hex Color of TEST 07 - At the Production Plant Text in Quality Page is : "	+ HexTest07AtPlantQPColor );
		
		String Test07AtPlantQPFontSize = Test07AtPlantQP.getCssValue("font-size");
		System.out.println("Actual Font Size of TEST 07 - At the Production Plant Text in Quality Page is : "	+ Test07AtPlantQPFontSize);
		String Test07AtPlantQPFontFamily = Test07AtPlantQP.getCssValue("font-family");
		System.out.println("Actual Font Family of TEST 07 - At the Production Plant Text in Quality Page is : "	+ Test07AtPlantQPFontFamily);
		String Test07AtPlantQPContent = Test07AtPlantQP.getText();
		System.out.println("Actual Content of TEST 07 - At the Production Plant Text in Quality Page is : "	+ Test07AtPlantQPContent);
				
		String ExpTest07AtPlantQPColor = sheet3.getCell(1,68).getContents(); //column,row
		System.out.println("Expected Color of TEST 07 - At the Production Plant Text in Quality Page is : "	+ ExpTest07AtPlantQPColor );
		String ExpTest07AtPlantQPFontSize = sheet3.getCell(2,68).getContents();
		System.out.println("Expected Font Size of TEST 07 - At the Production Plant Text in Quality Page is : "	+ ExpTest07AtPlantQPFontSize);
		String ExpTest07AtPlantQPFontFamily = sheet3.getCell(3,68).getContents();
		System.out.println("Expected Font Family of TEST 07 - At the Production Plant Text in Quality Page is : "	+ ExpTest07AtPlantQPFontFamily);
		String ExpTest07AtPlantQPContent = sheet3.getCell(4,68).getContents();
		System.out.println("Expected Content of TEST 07 - At the Production Plant Text in Quality Page is : "	+ ExpTest07AtPlantQPContent);
				
		if(HexTest07AtPlantQPColor.equals(ExpTest07AtPlantQPColor))
		{
			System.out.println("TEST 07 - At the Production Plant Text in Quality Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 07 - At the Production Plant Text in Quality Page Color is not in match with creative - Fail");
			bw.write("TEST 07 - At the Production Plant Text in Quality Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(Test07AtPlantQPFontSize.equals(ExpTest07AtPlantQPFontSize))
		{
			System.out.println("TEST 07 - At the Production Plant Text in Quality Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 07 - At the Production Plant Text in Quality Page Font Size is not in match with creative - Fail");
			bw.write("TEST 07 - At the Production Plant Text in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(Test07AtPlantQPFontFamily.equals(ExpTest07AtPlantQPFontFamily))
		{
			System.out.println("TEST 07 - At the Production Plant Text in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 07 - At the Production Plant Text in Quality Page Font Family is not in match with creative - Fail");
			bw.write("TEST 07 - At the Production Plant Text in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(Test07AtPlantQPContent.equals(ExpTest07AtPlantQPContent))
		{
			System.out.println("TEST 07 - At the Production Plant Text Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 07 - At the Production Plant Text Content in Quality Page is not in match with creative - Fail");
			bw.write("TEST 07 - At the Production Plant Text Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexTest07AtPlantQPColor.equals(ExpTest07AtPlantQPColor) && Test07AtPlantQPFontSize.equals(ExpTest07AtPlantQPFontSize) && Test07AtPlantQPFontFamily.equals(ExpTest07AtPlantQPFontFamily) && Test07AtPlantQPContent.equals(ExpTest07AtPlantQPContent))
		{
			Label Test07AtPlantQPCSSP = new Label(5,68,"PASS"); 
			wsheet3.addCell(Test07AtPlantQPCSSP); 
		}
		else
		{
			Label Test07AtPlantQPCSSF = new Label(5,68,"FAIL"); 
			wsheet3.addCell(Test07AtPlantQPCSSF); 
		}
		
		
		// Quality Page TEST 07 Desc
		
		WebElement Test07DescQP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[14]/div/div/div/div/div[6]/div/div[2]/div[2]")); 
		String Test07DescQPColor = Test07DescQP.getCssValue("color");
		System.out.println("Actual Color of TEST 07 Desc in Quality Page is : "	+ Test07DescQPColor );
		// Convert RGB to Hex Value
		Color HTest07DescQPColor = Color.fromString(Test07DescQPColor);
		String HexTest07DescQPColor = HTest07DescQPColor.asHex();
		System.out.println("Actual Hex Color of TEST 07 Desc in Quality Page is : "	+ HexTest07DescQPColor );
		
		String Test07DescQPFontSize = Test07DescQP.getCssValue("font-size");
		System.out.println("Actual Font Size of TEST 07 Desc in Quality Page is : "	+ Test07DescQPFontSize);
		String Test07DescQPFontFamily = Test07DescQP.getCssValue("font-family");
		System.out.println("Actual Font Family of TEST 07 Desc in Quality Page is : "	+ Test07DescQPFontFamily);
		String Test07DescQPContent = Test07DescQP.getText();
		System.out.println("Actual Content of TEST 07 Desc in Quality Page is : "	+ Test07DescQPContent);
				
		String ExpTest07DescQPColor = sheet3.getCell(1,69).getContents(); //column,row
		System.out.println("Expected Color of TEST 07 Desc in Quality Page is : "	+ ExpTest07DescQPColor );
		String ExpTest07DescQPFontSize = sheet3.getCell(2,69).getContents();
		System.out.println("Expected Font Size of TEST 07 Desc in Quality Page is : "	+ ExpTest07DescQPFontSize);
		String ExpTest07DescQPFontFamily = sheet3.getCell(3,69).getContents();
		System.out.println("Expected Font Family of TEST 07 Desc in Quality Page is : "	+ ExpTest07DescQPFontFamily);
		String ExpTest07DescQPContent = sheet3.getCell(4,69).getContents();
		System.out.println("Expected Content of TEST 07 Desc in Quality Page is : "	+ ExpTest07DescQPContent);
				
		if(HexTest07DescQPColor.equals(ExpTest07DescQPColor))
		{
			System.out.println("TEST 07 Desc in Quality Page Color is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 07 Desc in Quality Page Color is not in match with creative - Fail");
			bw.write("TEST 07 Desc in Quality Page Color is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(Test07DescQPFontSize.equals(ExpTest07DescQPFontSize))
		{
			System.out.println("TEST 07 Desc in Quality Page Font Size is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 07 Desc in Quality Page Font Size is not in match with creative - Fail");
			bw.write("TEST 07 Desc in Quality Page Font Size is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
				
		if(Test07DescQPFontFamily.equals(ExpTest07DescQPFontFamily))
		{
			System.out.println("TEST 07 Desc in Quality Page Font Family is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 07 Desc in Quality Page Font Family is not in match with creative - Fail");
			bw.write("TEST 07 Desc in Quality Page Font Family is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		

		if(Test07DescQPContent.equals(ExpTest07DescQPContent))
		{
			System.out.println("TEST 07 Desc Content in Quality Page is in match with creative - Pass");
			
		}
		else
		{
			System.out.println("TEST 07 Desc Content in Quality Page is not in match with creative - Fail");
			bw.write("TEST 07 Desc Content in Quality Page is not in match with creative - Fail"); 
			//Adding new line 
		    bw.newLine();
		}
		
		if(HexTest07DescQPColor.equals(ExpTest07DescQPColor) && Test07DescQPFontSize.equals(ExpTest07DescQPFontSize) && Test07DescQPFontFamily.equals(ExpTest07DescQPFontFamily) && Test07DescQPContent.equals(ExpTest07DescQPContent))
		{
			Label Test07DescQPCSSP = new Label(5,69,"PASS"); 
			wsheet3.addCell(Test07DescQPCSSP); 
		}
		else
		{
			Label Test07DescQPCSSF = new Label(5,69,"FAIL"); 
			wsheet3.addCell(Test07DescQPCSSF); 
		}
		
		Thread.sleep(3000);
		JavascriptExecutor jseFTouches = (JavascriptExecutor)driver;
		jseFTouches.executeScript("window.scrollBy(0,700)", "");
		Thread.sleep(8000);
		
		// Variety Link in Footer
		
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[15]/div/div/div[2]/div/div[2]/div[1]/div/a[3]/div")).click();
		Thread.sleep(5000);
		
   		bw.close();
   		
    	}
    	
    	catch(Exception e)
		{
			System.out.println("Exception Is "+e);
			 
		}
   		
    }
    
    @Test(priority = 3)
	
	public static void VarietyPageNavigation() throws InterruptedException, IOException, BiffException, RowsExceededException, WriteException {
		
    	try{
    		
		book = Workbook.getWorkbook(newFile);
		sheet2=book.getSheet("ExpectedURL'S");
		
		wsheet2 = workbookcopy.getSheet(1);
		
		 // Write text on  text file
		
		FileWriter fw = new FileWriter(file, true);  
		BufferedWriter bw = new BufferedWriter(fw);
		
		// Shop on Keurig.com Button in Variety Page Main Banner
		
		String winHandleBeforemb = driver.getWindowHandle();
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[2]/div/div/div/div[1]/a/div")).click(); 
		Thread.sleep(5000);
		//Get the list of window handles
		ArrayList<String> tabsmb = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(tabsmb.size());
		//Use the list of window handles to switch between windows
	    driver.switchTo().window((String) tabsmb.get(1));
		Thread.sleep(10000);
		String AShoponKeurigurl = driver.getCurrentUrl();
		System.out.println("Actual Shop on Keurig.com URL is: "+AShoponKeurigurl);
		String EShoponKeurigurl = sheet2.getCell(1,30).getContents();
		System.out.println("Expected Shop on Keurig.com URL is: "+EShoponKeurigurl);
		if(EShoponKeurigurl.equals(AShoponKeurigurl))
		{
			System.out.println("Shop on Keurig.com URL is Opened Correctly - Pass");
			Label ShoponKeurigNAVP = new Label(2,30,"PASS"); 
			wsheet2.addCell(ShoponKeurigNAVP); 
		}
		else
		{
			System.out.println("Shop on Keurig.com URL is not Opened Correctly - Fail");
			bw.write("Shop on Keurig.com URL is not Opened Correctly - Fail"); 
		    bw.newLine();
		    Label ShoponKeurigNAVF = new Label(2,30,"FAIL"); 
			wsheet2.addCell(ShoponKeurigNAVF); 
		}
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(winHandleBeforemb); // Home Window
		
		// Number Tab 1 - Shop Now on Keurig.com Link in Variety Page 
		
		String winHandleBeforen1 = driver.getWindowHandle();
		driver.findElement(By.xpath(".//*[@id='id_1_body']/div[2]/div[2]/a")).click(); 
		Thread.sleep(5000);
		//Get the list of window handles
		ArrayList<String> tabsn1 = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(tabsn1.size());
		//Use the list of window handles to switch between windows
	    driver.switchTo().window((String) tabsn1.get(1));
		Thread.sleep(10000);
		String AShoponKeurigN1url = driver.getCurrentUrl();
		System.out.println("Actual Shop Now on Keurig.com URL in Number Tab 1 is: "+AShoponKeurigN1url);
		String EShoponKeurigN1url = sheet2.getCell(1,31).getContents();
		System.out.println("Expected Shop Now on Keurig.com URL in Number Tab 1 is: "+EShoponKeurigN1url);
		if(EShoponKeurigN1url.equals(AShoponKeurigN1url))
		{
			System.out.println("Shop Now on Keurig.com URL in Number Tab 1 is Opened Correctly - Pass");
			Label ShoponKeurigN1NAVP = new Label(2,31,"PASS"); 
			wsheet2.addCell(ShoponKeurigN1NAVP); 
		}
		else
		{
			System.out.println("Shop Now on Keurig.com URL in Number Tab 1 is not Opened Correctly - Fail");
			bw.write("Shop Now on Keurig.com URL in Number Tab 1 is not Opened Correctly - Fail"); 
		    bw.newLine();
		    Label ShoponKeurigN1NAVF = new Label(2,31,"FAIL"); 
			wsheet2.addCell(ShoponKeurigN1NAVF);
		}
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(winHandleBeforen1); // Home Window
		
		Thread.sleep(3000);
		JavascriptExecutor jsen2 = (JavascriptExecutor)driver;
		jsen2.executeScript("window.scrollBy(0,300)", "");
		Thread.sleep(8000);
		
		// Click Action on Number Tab 2
		
		driver.findElement(By.xpath(".//*[@id='id_2_tab']")).click();
		Thread.sleep(5000);
		
		// Number Tab 2 - Shop Now on Keurig.com Link in Variety Page 
		
		String winHandleBeforen2 = driver.getWindowHandle();
		driver.findElement(By.xpath(".//*[@id='id_2_body']/div[2]/div[2]/a")).click(); 
		Thread.sleep(5000);
		//Get the list of window handles
		ArrayList<String> tabsn2 = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(tabsn2.size());
		//Use the list of window handles to switch between windows
	    driver.switchTo().window((String) tabsn2.get(1));
		Thread.sleep(10000);
		String AShoponKeurigN2url = driver.getCurrentUrl();
		System.out.println("Actual Shop Now on Keurig.com URL in Number Tab 2 is: "+AShoponKeurigN2url);
		String EShoponKeurigN2url = sheet2.getCell(1,32).getContents();
		System.out.println("Expected Shop Now on Keurig.com URL in Number Tab 2 is: "+EShoponKeurigN2url);
		if(EShoponKeurigN2url.equals(AShoponKeurigN2url))
		{
			System.out.println("Shop Now on Keurig.com URL in Number Tab 2 is Opened Correctly - Pass");
			Label ShoponKeurigN2NAVP = new Label(2,32,"PASS"); 
			wsheet2.addCell(ShoponKeurigN2NAVP); 
		}
		else
		{
			System.out.println("Shop Now on Keurig.com URL in Number Tab 2 is not Opened Correctly - Fail");
			bw.write("Shop Now on Keurig.com URL in Number Tab 2 is not Opened Correctly - Fail");  
		    bw.newLine();
		    Label ShoponKeurigN2NAVF = new Label(2,32,"FAIL"); 
			wsheet2.addCell(ShoponKeurigN2NAVF);
		}
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(winHandleBeforen2); // Home Window
		
		// Click Action on Number Tab 3
		
		driver.findElement(By.xpath(".//*[@id='id_3_tab']")).click();
		Thread.sleep(5000);
		
		// Number Tab 3 - Shop Now on Keurig.com Link in Variety Page 
		
		String winHandleBeforen3 = driver.getWindowHandle();
		driver.findElement(By.xpath(".//*[@id='id_3_body']/div[2]/div[2]/a")).click(); 
		Thread.sleep(5000);
		//Get the list of window handles
		ArrayList<String> tabsn3 = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(tabsn3.size());
		//Use the list of window handles to switch between windows
	    driver.switchTo().window((String) tabsn3.get(1));
		Thread.sleep(10000);
		String AShoponKeurigN3url = driver.getCurrentUrl();
		System.out.println("Actual Shop Now on Keurig.com URL in Number Tab 3 is: "+AShoponKeurigN3url);
		String EShoponKeurigN3url = sheet2.getCell(1,33).getContents();
		System.out.println("Expected Shop Now on Keurig.com URL in Number Tab 3 is: "+EShoponKeurigN3url);
		if(EShoponKeurigN3url.equals(AShoponKeurigN3url))
		{
			System.out.println("Shop Now on Keurig.com URL in Number Tab 3 is Opened Correctly - Pass");
			Label ShoponKeurigN3NAVP = new Label(2,33,"PASS"); 
			wsheet2.addCell(ShoponKeurigN3NAVP);
		}
		else
		{
			System.out.println("Shop Now on Keurig.com URL in Number Tab 3 is not Opened Correctly - Fail");
			bw.write("Shop Now on Keurig.com URL in Number Tab 3 is not Opened Correctly - Fail"); 
		    bw.newLine();
		    Label ShoponKeurigN3NAVF = new Label(2,33,"FAIL"); 
			wsheet2.addCell(ShoponKeurigN3NAVF);
		}
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(winHandleBeforen3); // Home Window
		
		Thread.sleep(3000);
		JavascriptExecutor jsereg = (JavascriptExecutor)driver;
		jsereg.executeScript("window.scrollBy(0,1000)", "");
		Thread.sleep(8000);
		
		// Regular - Shop on Keurig.com Link in Variety Page 
		
		String winHandleBeforereg = driver.getWindowHandle();
		driver.findElement(By.xpath(".//*[@id='sk_id_varityPrd_table']/div[1]/div[2]/div[5]/a/div")).click(); 
		Thread.sleep(5000);
		//Get the list of window handles
		ArrayList<String> tabsreg = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(tabsreg.size());
		//Use the list of window handles to switch between windows
	    driver.switchTo().window((String) tabsreg.get(1));
		Thread.sleep(10000);
		String AShoponKeurigRegurl = driver.getCurrentUrl();
		System.out.println("Actual Shop on Keurig.com URL in Regular is: "+AShoponKeurigRegurl);
		String EShoponKeurigRegurl = sheet2.getCell(1,34).getContents();
		System.out.println("Expected Shop on Keurig.com URL in Regular is: "+EShoponKeurigRegurl);
		if(EShoponKeurigRegurl.equals(AShoponKeurigRegurl))
		{
			System.out.println("Shop on Keurig.com URL in Regular is Opened Correctly - Pass");
			Label ShoponKeurigRegNAVP = new Label(2,34,"PASS"); 
			wsheet2.addCell(ShoponKeurigRegNAVP);
		}
		else
		{
			System.out.println("Shop on Keurig.com URL in Regular is not Opened Correctly - Fail");
			bw.write("Shop on Keurig.com URL in Regular is not Opened Correctly - Fail"); 
		    bw.newLine();
		    Label ShoponKeurigRegNAVF = new Label(2,34,"FAIL"); 
			wsheet2.addCell(ShoponKeurigRegNAVF);
		}
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(winHandleBeforereg); // Home Window
		
		// Flavored - Shop on Keurig.com Link in Variety Page 
		
		String winHandleBeforefla = driver.getWindowHandle();
		driver.findElement(By.xpath(".//*[@id='sk_id_varityPrd_table']/div[3]/div[2]/div[5]/a/div")).click(); 
		Thread.sleep(5000);
		//Get the list of window handles
		ArrayList<String> tabsfla = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(tabsfla.size());
		//Use the list of window handles to switch between windows
	    driver.switchTo().window((String) tabsfla.get(1));
		Thread.sleep(10000);
		String AShoponKeurigFlaurl = driver.getCurrentUrl();
		System.out.println("Actual Shop on Keurig.com URL in Flavored is: "+AShoponKeurigFlaurl);
		String EShoponKeurigFlaurl = sheet2.getCell(1,35).getContents();
		System.out.println("Expected Shop on Keurig.com URL in Flavored is: "+EShoponKeurigFlaurl);
		if(EShoponKeurigFlaurl.equals(AShoponKeurigFlaurl))
		{
			System.out.println("Shop on Keurig.com URL in Flavored is Opened Correctly - Pass");
			Label ShoponKeurigFlaNAVP = new Label(2,35,"PASS"); 
			wsheet2.addCell(ShoponKeurigFlaNAVP);
		}
		else
		{
			System.out.println("Shop on Keurig.com URL in Flavored is not Opened Correctly - Fail");
			bw.write("Shop on Keurig.com URL in Flavored is not Opened Correctly - Fail"); 
		    bw.newLine();
		    Label ShoponKeurigFlaNAVF = new Label(2,35,"FAIL"); 
			wsheet2.addCell(ShoponKeurigFlaNAVF);
		}
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(winHandleBeforefla); // Home Window
		
		// Decaf - Shop on Keurig.com Link in Variety Page 
		
		String winHandleBeforeDec = driver.getWindowHandle();
		driver.findElement(By.xpath(".//*[@id='sk_id_varityPrd_table']/div[4]/div[2]/div[5]/a/div")).click(); 
		Thread.sleep(5000);
		//Get the list of window handles
		ArrayList<String> tabsDec = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(tabsDec.size());
		//Use the list of window handles to switch between windows
	    driver.switchTo().window((String) tabsDec.get(1));
		Thread.sleep(10000);
		String AShoponKeurigDecurl = driver.getCurrentUrl();
		System.out.println("Actual Shop on Keurig.com URL in Decaf is: "+AShoponKeurigDecurl);
		String EShoponKeurigDecurl = sheet2.getCell(1,36).getContents();
		System.out.println("Expected Shop on Keurig.com URL in Decaf is: "+EShoponKeurigDecurl);
		if(EShoponKeurigDecurl.equals(AShoponKeurigDecurl))
		{
			System.out.println("Shop on Keurig.com URL in Decaf is Opened Correctly - Pass");
			Label ShoponKeurigDecNAVP = new Label(2,36,"PASS"); 
			wsheet2.addCell(ShoponKeurigDecNAVP);
		}
		else
		{
			System.out.println("Shop on Keurig.com URL in Decaf is not Opened Correctly - Fail");
			bw.write("Shop on Keurig.com URL in Decaf is not Opened Correctly - Fail"); 
		    bw.newLine();
		    Label ShoponKeurigDecNAVF = new Label(2,36,"FAIL"); 
			wsheet2.addCell(ShoponKeurigDecNAVF);
		}
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(winHandleBeforeDec); // Home Window
		
		Thread.sleep(3000);
		JavascriptExecutor jsesea = (JavascriptExecutor)driver;
		jsesea.executeScript("window.scrollBy(0,1000)", "");
		Thread.sleep(8000);
		
		// Seasonal - Shop on Keurig.com Link in Variety Page 
		
		String winHandleBeforesea = driver.getWindowHandle();
		driver.findElement(By.xpath(".//*[@id='sk_id_varityPrd_table']/div[2]/div[2]/div[5]/a/div")).click(); 
		Thread.sleep(5000);
		//Get the list of window handles
		ArrayList<String> tabssea = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(tabssea.size());
		//Use the list of window handles to switch between windows
	    driver.switchTo().window((String) tabssea.get(1));
		Thread.sleep(10000);
		String AShoponKeurigSeaurl = driver.getCurrentUrl();
		System.out.println("Actual Shop on Keurig.com URL in Seasonal is: "+AShoponKeurigSeaurl);
		String EShoponKeurigSeaurl = sheet2.getCell(1,37).getContents();
		System.out.println("Expected Shop on Keurig.com URL in Seasonal is: "+EShoponKeurigSeaurl);
		if(EShoponKeurigSeaurl.equals(AShoponKeurigSeaurl))
		{
			System.out.println("Shop on Keurig.com URL in Seasonal is Opened Correctly - Pass");
			Label ShoponKeurigSeaNAVP = new Label(2,37,"PASS"); 
			wsheet2.addCell(ShoponKeurigSeaNAVP);
		}
		else
		{
			System.out.println("Shop on Keurig.com URL in Seasonal is not Opened Correctly - Fail");
			bw.write("Shop on Keurig.com URL in Seasonal is not Opened Correctly - Fail"); 
		    bw.newLine();
		    Label ShoponKeurigSeaNAVF = new Label(2,37,"FAIL"); 
			wsheet2.addCell(ShoponKeurigSeaNAVF);
		}
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(winHandleBeforesea); // Home Window
		
		Thread.sleep(3000);
		JavascriptExecutor jsecof = (JavascriptExecutor)driver;
		jsecof.executeScript("window.scrollBy(0,200)", "");
		Thread.sleep(8000);
		
		// CoffeeHouse - Shop on Keurig.com Link in Variety Page 
		
		String winHandleBeforecof = driver.getWindowHandle();
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[5]/div/div/div/div[2]/div[4]/div[3]/div[2]/div[5]/a/div")).click(); 
		Thread.sleep(5000);
		//Get the list of window handles
		ArrayList<String> tabscof = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(tabscof.size());
		//Use the list of window handles to switch between windows
	    driver.switchTo().window((String) tabscof.get(1));
		Thread.sleep(10000);
		String AShoponKeurigCofurl = driver.getCurrentUrl();
		System.out.println("Actual Shop on Keurig.com URL in CoffeeHouse is: "+AShoponKeurigCofurl);
		String EShoponKeurigCofurl = sheet2.getCell(1,38).getContents();
		System.out.println("Expected Shop on Keurig.com URL in CoffeeHouse is: "+EShoponKeurigCofurl);
		if(EShoponKeurigCofurl.equals(AShoponKeurigCofurl))
		{
			System.out.println("Shop on Keurig.com URL in CoffeeHouse is Opened Correctly - Pass");
			Label ShoponKeurigCofNAVP = new Label(2,38,"PASS"); 
			wsheet2.addCell(ShoponKeurigCofNAVP);
		}
		else
		{
			System.out.println("Shop on Keurig.com URL in CoffeeHouse is not Opened Correctly - Fail");
			bw.write("Shop on Keurig.com URL in CoffeeHouse is not Opened Correctly - Fail"); 
		    bw.newLine();
		    Label ShoponKeurigCofNAVF = new Label(2,38,"FAIL"); 
			wsheet2.addCell(ShoponKeurigCofNAVF);
		}
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(winHandleBeforecof); // Home Window
		
		// Organic - Shop on Keurig.com Link in Variety Page 
		
		String winHandleBeforeorg = driver.getWindowHandle();
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[5]/div/div/div/div[2]/div[4]/div[4]/div[2]/div[5]/a/div")).click(); 
		Thread.sleep(5000);
		//Get the list of window handles
		ArrayList<String> tabsorg = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(tabsorg.size());
		//Use the list of window handles to switch between windows
	    driver.switchTo().window((String) tabsorg.get(1));
		Thread.sleep(10000);
		String AShoponKeurigOrgurl = driver.getCurrentUrl();
		System.out.println("Actual Shop on Keurig.com URL in Organic is: "+AShoponKeurigOrgurl);
		String EShoponKeurigOrgurl = sheet2.getCell(1,39).getContents();
		System.out.println("Expected Shop on Keurig.com URL in Organic is: "+EShoponKeurigOrgurl);
		if(EShoponKeurigOrgurl.equals(AShoponKeurigOrgurl))
		{
			System.out.println("Shop on Keurig.com URL in Organic is Opened Correctly - Pass");
			Label ShoponKeurigOrgNAVP = new Label(2,39,"PASS"); 
			wsheet2.addCell(ShoponKeurigOrgNAVP);
		}
		else
		{
			System.out.println("Shop on Keurig.com URL in Organic is not Opened Correctly - Fail");
			bw.write("Shop on Keurig.com URL in Organic is not Opened Correctly - Fail"); 
		    bw.newLine();
		    Label ShoponKeurigOrgNAVF = new Label(2,39,"FAIL"); 
			wsheet2.addCell(ShoponKeurigOrgNAVF);
		}
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(winHandleBeforeorg); // Home Window
		
		Thread.sleep(3000);
		JavascriptExecutor jseft = (JavascriptExecutor)driver;
		jseft.executeScript("window.scrollBy(0,100)", "");
		Thread.sleep(8000);
		
		// Click Action on GMC Logo in Footer in Variety Page
		
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[6]/div/div/div[2]/div/div[1]/a/div")).click(); 
		Thread.sleep(15000);
		
		// Click Action on Variety Link in Header in Home Page
		
		driver.findElement(By.xpath(".//*[@id='id_skgm_hw_headerLayoutCell']/div[1]/div[2]/div/div[1]/a[2]/div")).click(); 
		Thread.sleep(5000);
		
		bw.close();
		
    	}
    	
    	catch(Exception e)
		{
			System.out.println("Exception Is "+e);
			 
		}
		
    }
		
		@Test(priority = 4)
		
		public static void VarietyPageCSS() throws InterruptedException, IOException, BiffException, RowsExceededException, WriteException {
			
			try{
			
			book = Workbook.getWorkbook(newFile);
			sheet4=book.getSheet("VarietyPageCSS");
			
			wsheet4 = workbookcopy.getSheet(3);
			
			 // Write text on  text file
			
			FileWriter fw = new FileWriter(file, true);  
			BufferedWriter bw = new BufferedWriter(fw);
			
			// Variety Page Main Banner - Variety
			
			WebElement VarietyMainVP = driver.findElement(By.className("sk_pageImage_title")); 
			String VarietyMainVPColor = VarietyMainVP.getCssValue("color");
			System.out.println("Actual Color of Variety Text in Variety Page is : "	+ VarietyMainVPColor );
			// Convert RGB to Hex Value
			Color HVarietyMainVPColor = Color.fromString(VarietyMainVPColor);
			String HexVarietyMainVPColor = HVarietyMainVPColor.asHex();
			System.out.println("Actual Hex Color of Variety Text in Variety Page is : "	+ HexVarietyMainVPColor );
			
			String VarietyMainVPFontSize = VarietyMainVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Variety Text in Variety Page is : "	+ VarietyMainVPFontSize);
			String VarietyMainVPFontFamily = VarietyMainVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Variety Text in Variety Page is : "	+ VarietyMainVPFontFamily);
			String VarietyMainVPContent = VarietyMainVP.getText();
			System.out.println("Actual Content of Variety Text in Variety Page is : "	+ VarietyMainVPContent);
					
			String ExpVarietyMainVPColor = sheet4.getCell(1,1).getContents(); //column,row
			System.out.println("Expected Color of Variety Text in Variety Page is : "	+ ExpVarietyMainVPColor );
			String ExpVarietyMainVPFontSize = sheet4.getCell(2,1).getContents();
			System.out.println("Expected Font Size of Variety Text in Variety Page is : "	+ ExpVarietyMainVPFontSize);
			String ExpVarietyMainVPFontFamily = sheet4.getCell(3,1).getContents();
			System.out.println("Expected Font Family of Variety Text in Variety Page is : "	+ ExpVarietyMainVPFontFamily);
			String ExpVarietyMainVPContent = sheet4.getCell(4,1).getContents();
			System.out.println("Expected Content of Variety Text in Variety Page is : "	+ ExpVarietyMainVPContent);
					
			if(HexVarietyMainVPColor.equals(ExpVarietyMainVPColor))
			{
				System.out.println("Variety Text in Variety Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("Variety Text in Variety Page Color is not in match with creative - Fail");
				bw.write("Variety Text in Variety Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(VarietyMainVPFontSize.equals(ExpVarietyMainVPFontSize))
			{
				System.out.println("Variety Text in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Variety Text in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Variety Text in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(VarietyMainVPFontFamily.equals(ExpVarietyMainVPFontFamily))
			{
				System.out.println("Variety Text in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Variety Text in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Variety Text in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(VarietyMainVPContent.equals(ExpVarietyMainVPContent))
			{
				System.out.println("Variety Text Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Variety Text Content in Variety Page is not in match with creative - Fail");
				bw.write("Variety Text Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}
			
			if(HexVarietyMainVPColor.equals(ExpVarietyMainVPColor) && VarietyMainVPFontSize.equals(ExpVarietyMainVPFontSize) && VarietyMainVPFontFamily.equals(ExpVarietyMainVPFontFamily) && VarietyMainVPContent.equals(ExpVarietyMainVPContent))
			{
				Label VarietyMainVPCSSP = new Label(5,1,"PASS"); 
				wsheet4.addCell(VarietyMainVPCSSP); 
			}
			else
			{
				Label VarietyMainVPCSSF = new Label(5,1,"FAIL"); 
				wsheet4.addCell(VarietyMainVPCSSF); 
			}
			
			// Variety Page Main Banner - Variety Desc
			
			WebElement VarietyDescVP = driver.findElement(By.className("sk_pageImage_Description")); 
			String VarietyDescVPColor = VarietyDescVP.getCssValue("color");
			System.out.println("Actual Color of Variety Desc in Variety Page is : "	+ VarietyDescVPColor );
			// Convert RGB to Hex Value
			Color HVarietyDescVPColor = Color.fromString(VarietyDescVPColor);
			String HexVarietyDescVPColor = HVarietyDescVPColor.asHex();
			System.out.println("Actual Hex Color of Variety Desc in Variety Page is : "	+ HexVarietyDescVPColor );
			
			String VarietyDescVPFontSize = VarietyDescVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Variety Desc in Variety Page is : "	+ VarietyDescVPFontSize);
			String VarietyDescVPFontFamily = VarietyDescVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Variety Desc in Variety Page is : "	+ VarietyDescVPFontFamily);
			String VarietyDescVPContent = VarietyDescVP.getText();
			System.out.println("Actual Content of Variety Desc in Variety Page is : "	+ VarietyDescVPContent);
					
			String ExpVarietyDescVPColor = sheet4.getCell(1,2).getContents(); //column,row
			System.out.println("Expected Color of Variety Desc in Variety Page is : "	+ ExpVarietyDescVPColor );
			String ExpVarietyDescVPFontSize = sheet4.getCell(2,2).getContents();
			System.out.println("Expected Font Size of Variety Desc in Variety Page is : "	+ ExpVarietyDescVPFontSize);
			String ExpVarietyDescVPFontFamily = sheet4.getCell(3,2).getContents();
			System.out.println("Expected Font Family of Variety Desc in Variety Page is : "	+ ExpVarietyDescVPFontFamily);
			String ExpVarietyDescVPContent = sheet4.getCell(4,2).getContents();
			System.out.println("Expected Content of Variety Desc in Variety Page is : "	+ ExpVarietyDescVPContent);
					
			if(HexVarietyDescVPColor.equals(ExpVarietyDescVPColor))
			{
				System.out.println("Variety Desc in Variety Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("Variety Desc in Variety Page Color is not in match with creative - Fail");
				bw.write("Variety Desc in Variety Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(VarietyDescVPFontSize.equals(ExpVarietyDescVPFontSize))
			{
				System.out.println("Variety Desc in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Variety Desc in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Variety Desc in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(VarietyDescVPFontFamily.equals(ExpVarietyDescVPFontFamily))
			{
				System.out.println("Variety Desc in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Variety Desc in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Variety Desc in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(VarietyDescVPContent.equals(ExpVarietyDescVPContent))
			{
				System.out.println("Variety Desc Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Variety Desc Content in Variety Page is not in match with creative - Fail");
				bw.write("Variety Desc Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}
			
			if(HexVarietyDescVPColor.equals(ExpVarietyDescVPColor) && VarietyDescVPFontSize.equals(ExpVarietyDescVPFontSize) && VarietyDescVPFontFamily.equals(ExpVarietyDescVPFontFamily) && VarietyDescVPContent.equals(ExpVarietyDescVPContent))
			{
				Label VarietyDescVPCSSP = new Label(5,2,"PASS"); 
				wsheet4.addCell(VarietyDescVPCSSP); 
			}
			else
			{
				Label VarietyDescVPCSSF = new Label(5,2,"FAIL"); 
				wsheet4.addCell(VarietyDescVPCSSF); 
			}
			
			// Variety Page Main Banner - Shop on Keurig.com
			
			WebElement MainShopVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[2]/div/div/div/div[1]/a/div")); 
			String MainShopVPColor = MainShopVP.getCssValue("color");
			System.out.println("Actual Color of Shop on Keurig.com in Variety Page is : "	+ MainShopVPColor );
			// Convert RGB to Hex Value
			Color HMainShopVPColor = Color.fromString(MainShopVPColor);
			String HexMainShopVPColor = HMainShopVPColor.asHex();
			System.out.println("Actual Hex Color of Shop on Keurig.com in Variety Page is : "	+ HexMainShopVPColor );
			
			String MainShopVPFontSize = MainShopVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Shop on Keurig.com in Variety Page is : "	+ MainShopVPFontSize);
			String MainShopVPFontFamily = MainShopVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Shop on Keurig.com in Variety Page is : "	+ MainShopVPFontFamily);
			String MainShopVPContent = MainShopVP.getText();
			System.out.println("Actual Content of Shop on Keurig.com in Variety Page is : "	+ MainShopVPContent);
					
			String ExpMainShopVPColor = sheet4.getCell(1,3).getContents(); //column,row
			System.out.println("Expected Color of Shop on Keurig.com in Variety Page is : "	+ ExpMainShopVPColor );
			String ExpMainShopVPFontSize = sheet4.getCell(2,3).getContents();
			System.out.println("Expected Font Size of Shop on Keurig.com in Variety Page is : "	+ ExpMainShopVPFontSize);
			String ExpMainShopVPFontFamily = sheet4.getCell(3,3).getContents();
			System.out.println("Expected Font Family of Shop on Keurig.com in Variety Page is : "	+ ExpMainShopVPFontFamily);
			String ExpMainShopVPContent = sheet4.getCell(4,3).getContents();
			System.out.println("Expected Content of Shop on Keurig.com in Variety Page is : "	+ ExpMainShopVPContent);
					
			if(HexMainShopVPColor.equals(ExpMainShopVPColor))
			{
				System.out.println("Shop on Keurig.com in Variety Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("Shop on Keurig.com in Variety Page Color is not in match with creative - Fail");
				bw.write("Shop on Keurig.com in Variety Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(MainShopVPFontSize.equals(ExpMainShopVPFontSize))
			{
				System.out.println("Shop on Keurig.com in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Shop on Keurig.com in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Shop on Keurig.com in Variety Page Font Size is not in match with creative - Fail");  
			    bw.newLine();
			}
					
			if(MainShopVPFontFamily.equals(ExpMainShopVPFontFamily))
			{
				System.out.println("Shop on Keurig.com in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Shop on Keurig.com in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Shop on Keurig.com in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(MainShopVPContent.equals(ExpMainShopVPContent))
			{
				System.out.println("Shop on Keurig.com Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Shop on Keurig.com Content in Variety Page is not in match with creative - Fail");
				bw.write("Shop on Keurig.com Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}	
			
			if(HexMainShopVPColor.equals(ExpMainShopVPColor) && MainShopVPFontSize.equals(ExpMainShopVPFontSize) && MainShopVPFontFamily.equals(ExpMainShopVPFontFamily) && MainShopVPContent.equals(ExpMainShopVPContent))
			{
				Label MainShopVPCSSP = new Label(5,3,"PASS"); 
				wsheet4.addCell(MainShopVPCSSP); 
			}
			else
			{
				Label MainShopVPCSSF = new Label(5,3,"FAIL"); 
				wsheet4.addCell(MainShopVPCSSF); 
			}
			
			Thread.sleep(3000);
			JavascriptExecutor jsecssn2 = (JavascriptExecutor)driver;
			jsecssn2.executeScript("window.scrollBy(0,300)", "");
			Thread.sleep(8000);
			
			// Variety Page Num Tab 1 - Title
			
			WebElement N1TitleVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[2]/div/div/div/div[2]/div/div/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]")); 
			String N1TitleVPColor = N1TitleVP.getCssValue("color");
			System.out.println("Actual Color of Number Tab 1 Title in Variety Page is : "	+ N1TitleVPColor );
			// Convert RGB to Hex Value
			Color HN1TitleVPColor = Color.fromString(N1TitleVPColor);
			String HexN1TitleVPColor = HN1TitleVPColor.asHex();
			System.out.println("Actual Hex Color of Number Tab 1 Title in Variety Page is : "	+ HexN1TitleVPColor );
			
			String N1TitleVPFontSize = N1TitleVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Number Tab 1 Title in Variety Page is : "	+ N1TitleVPFontSize);
			String N1TitleVPFontFamily = N1TitleVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Number Tab 1 Title in Variety Page is : "	+ N1TitleVPFontFamily);
			String N1TitleVPContent = N1TitleVP.getText();
			System.out.println("Actual Content of Number Tab 1 Title in Variety Page is : "	+ N1TitleVPContent);
					
			String ExpN1TitleVPColor = sheet4.getCell(1,4).getContents(); //column,row
			System.out.println("Expected Color of Number Tab 1 Title in Variety Page is : "	+ ExpN1TitleVPColor );
			String ExpN1TitleVPFontSize = sheet4.getCell(2,4).getContents();
			System.out.println("Expected Font Size of Number Tab 1 Title in Variety Page is : "	+ ExpN1TitleVPFontSize);
			String ExpN1TitleVPFontFamily = sheet4.getCell(3,4).getContents();
			System.out.println("Expected Font Family of Number Tab 1 Title in Variety Page is : "	+ ExpN1TitleVPFontFamily);
			String ExpN1TitleVPContent = sheet4.getCell(4,4).getContents();
			System.out.println("Expected Content of Number Tab 1 Title in Variety Page is : "	+ ExpN1TitleVPContent);
					
			if(HexN1TitleVPColor.equals(ExpN1TitleVPColor))
			{
				System.out.println("Number Tab 1 Title in Variety Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("Number Tab 1 Title in Variety Page Color is not in match with creative - Fail");
				bw.write("Number Tab 1 Title in Variety Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(N1TitleVPFontSize.equals(ExpN1TitleVPFontSize))
			{
				System.out.println("Number Tab 1 Title in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Number Tab 1 Title in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Number Tab 1 Title in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(N1TitleVPFontFamily.equals(ExpN1TitleVPFontFamily))
			{
				System.out.println("Number Tab 1 Title in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Number Tab 1 Title in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Number Tab 1 Title in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(N1TitleVPContent.equals(ExpN1TitleVPContent))
			{
				System.out.println("Number Tab 1 Title Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Number Tab 1 Title Content in Variety Page is not in match with creative - Fail");
				bw.write("Number Tab 1 Title Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}	
			
			if(HexN1TitleVPColor.equals(ExpN1TitleVPColor) && N1TitleVPFontSize.equals(ExpN1TitleVPFontSize) && N1TitleVPFontFamily.equals(ExpN1TitleVPFontFamily) && N1TitleVPContent.equals(ExpN1TitleVPContent))
			{
				Label N1TitleVPCSSP = new Label(5,4,"PASS"); 
				wsheet4.addCell(N1TitleVPCSSP); 
			}
			else
			{
				Label N1TitleVPCSSF = new Label(5,4,"FAIL"); 
				wsheet4.addCell(N1TitleVPCSSF); 
			}
			
			// Variety Page Num Tab 1 - Desc
			
			WebElement N1DescVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[2]/div/div/div/div[2]/div/div/div[1]/div[1]/div[1]/div[2]/div[2]/div[2]")); 
			String N1DescVPColor = N1DescVP.getCssValue("color");
			System.out.println("Actual Color of Number Tab 1 Desc in Variety Page is : "	+ N1DescVPColor );
			// Convert RGB to Hex Value
			Color HN1DescVPColor = Color.fromString(N1DescVPColor);
			String HexN1DescVPColor = HN1DescVPColor.asHex();
			System.out.println("Actual Hex Color of Number Tab 1 Desc in Variety Page is : "	+ HexN1DescVPColor );
			
			String N1DescVPFontSize = N1DescVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Number Tab 1 Desc in Variety Page is : "	+ N1DescVPFontSize);
			String N1DescVPFontFamily = N1DescVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Number Tab 1 Desc in Variety Page is : "	+ N1DescVPFontFamily);
			String N1DescVPContent = N1DescVP.getText();
			System.out.println("Actual Content of Number Tab 1 Desc in Variety Page is : "	+ N1DescVPContent);
					
			String ExpN1DescVPColor = sheet4.getCell(1,5).getContents(); //column,row
			System.out.println("Expected Color of Number Tab 1 Desc in Variety Page is : "	+ ExpN1DescVPColor );
			String ExpN1DescVPFontSize = sheet4.getCell(2,5).getContents();
			System.out.println("Expected Font Size of Number Tab 1 Desc in Variety Page is : "	+ ExpN1DescVPFontSize);
			String ExpN1DescVPFontFamily = sheet4.getCell(3,5).getContents();
			System.out.println("Expected Font Family of Number Tab 1 Desc in Variety Page is : "	+ ExpN1DescVPFontFamily);
			String ExpN1DescVPContent = sheet4.getCell(4,5).getContents();
			System.out.println("Expected Content of Number Tab 1 Desc in Variety Page is : "	+ ExpN1DescVPContent);
					
			if(HexN1DescVPColor.equals(ExpN1DescVPColor))
			{
				System.out.println("Number Tab 1 Desc in Variety Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("Number Tab 1 Desc in Variety Page Color is not in match with creative - Fail");
				bw.write("Number Tab 1 Desc in Variety Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(N1DescVPFontSize.equals(ExpN1DescVPFontSize))
			{
				System.out.println("Number Tab 1 Desc in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Number Tab 1 Desc in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Number Tab 1 Desc in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(N1DescVPFontFamily.equals(ExpN1DescVPFontFamily))
			{
				System.out.println("Number Tab 1 Desc in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Number Tab 1 Desc in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Number Tab 1 Desc in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(N1DescVPContent.equals(ExpN1DescVPContent))
			{
				System.out.println("Number Tab 1 Desc Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Number Tab 1 Desc Content in Variety Page is not in match with creative - Fail");
				bw.write("Number Tab 1 Desc Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}	
			
			if(HexN1DescVPColor.equals(ExpN1DescVPColor) && N1DescVPFontSize.equals(ExpN1DescVPFontSize) && N1DescVPFontFamily.equals(ExpN1DescVPFontFamily) && N1DescVPContent.equals(ExpN1DescVPContent))
			{
				Label N1DescVPCSSP = new Label(5,5,"PASS"); 
				wsheet4.addCell(N1DescVPCSSP); 
			}
			else
			{
				Label N1DescVPCSSF = new Label(5,5,"FAIL"); 
				wsheet4.addCell(N1DescVPCSSF); 
			}
			
		   // Variety Page Num Tab 1 - Shop Now on Keurig.com
			
			WebElement N1ShopVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[2]/div/div/div/div[2]/div/div/div[1]/div[1]/div[1]/div[2]/div[2]/a")); 
			String N1ShopVPColor = N1ShopVP.getCssValue("color");
			System.out.println("Actual Color of Number Tab 1 Shop Now on Keurig.com in Variety Page is : "	+ N1ShopVPColor );
			// Convert RGB to Hex Value
			Color HN1ShopVPColor = Color.fromString(N1ShopVPColor);
			String HexN1ShopVPColor = HN1ShopVPColor.asHex();
			System.out.println("Actual Hex Color of Number Tab 1 Shop Now on Keurig.com in Variety Page is : "	+ HexN1ShopVPColor );
			
			String N1ShopVPFontSize = N1ShopVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Number Tab 1 Shop Now on Keurig.com in Variety Page is : "	+ N1ShopVPFontSize);
			String N1ShopVPFontFamily = N1ShopVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Number Tab 1 Shop Now on Keurig.com in Variety Page is : "	+ N1ShopVPFontFamily);
			String N1ShopVPContent = N1ShopVP.getText();
			System.out.println("Actual Content of Number Tab 1 Shop Now on Keurig.com in Variety Page is : "	+ N1ShopVPContent);
					
			String ExpN1ShopVPColor = sheet4.getCell(1,6).getContents(); //column,row
			System.out.println("Expected Color of Number Tab 1 Shop Now on Keurig.com in Variety Page is : "	+ ExpN1ShopVPColor );
			String ExpN1ShopVPFontSize = sheet4.getCell(2,6).getContents();
			System.out.println("Expected Font Size of Number Tab 1 Shop Now on Keurig.com in Variety Page is : "	+ ExpN1ShopVPFontSize);
			String ExpN1ShopVPFontFamily = sheet4.getCell(3,6).getContents();
			System.out.println("Expected Font Family of Number Tab 1 Shop Now on Keurig.com in Variety Page is : "	+ ExpN1ShopVPFontFamily);
			String ExpN1ShopVPContent = sheet4.getCell(4,6).getContents();
			System.out.println("Expected Content of Number Tab 1 Shop Now on Keurig.com in Variety Page is : "	+ ExpN1ShopVPContent);
					
			if(HexN1ShopVPColor.equals(ExpN1ShopVPColor))
			{
				System.out.println("Number Tab 1 Shop Now on Keurig.com in Variety Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("Number Tab 1 Shop Now on Keurig.com in Variety Page Color is not in match with creative - Fail");
				bw.write("Number Tab 1 Shop Now on Keurig.com in Variety Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(N1ShopVPFontSize.equals(ExpN1ShopVPFontSize))
			{
				System.out.println("Number Tab 1 Shop Now on Keurig.com in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Number Tab 1 Shop Now on Keurig.com in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Number Tab 1 Shop Now on Keurig.com in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(N1ShopVPFontFamily.equals(ExpN1ShopVPFontFamily))
			{
				System.out.println("Number Tab 1 Shop Now on Keurig.com in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Number Tab 1 Shop Now on Keurig.com in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Number Tab 1 Shop Now on Keurig.com in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(N1ShopVPContent.equals(ExpN1ShopVPContent))
			{
				System.out.println("Number Tab 1 Shop Now on Keurig.com Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Number Tab 1 Shop Now on Keurig.com Content in Variety Page is not in match with creative - Fail");
				bw.write("Number Tab 1 Shop Now on Keurig.com Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}	
			
			if(HexN1ShopVPColor.equals(ExpN1ShopVPColor) && N1ShopVPFontSize.equals(ExpN1ShopVPFontSize) && N1ShopVPFontFamily.equals(ExpN1ShopVPFontFamily) && N1ShopVPContent.equals(ExpN1ShopVPContent))
			{
				Label N1ShopVPCSSP = new Label(5,6,"PASS"); 
				wsheet4.addCell(N1ShopVPCSSP); 
			}
			else
			{
				Label N1ShopVPCSSF = new Label(5,6,"FAIL"); 
				wsheet4.addCell(N1ShopVPCSSF); 
			}
			
			// Click Action on Number Tab 2 in Variety Page
			
			driver.findElement(By.xpath(".//*[@id='id_2_tab']")).click();
			Thread.sleep(5000);
			
			// Variety Page Num Tab 2 - Title
			
			WebElement N2TitleVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[2]/div/div/div/div[2]/div/div/div[1]/div[1]/div[2]/div[2]/div[2]/div[1]")); 
			String N2TitleVPColor = N2TitleVP.getCssValue("color");
			System.out.println("Actual Color of Number Tab 02 Title in Variety Page is : "	+ N2TitleVPColor );
			// Convert RGB to Hex Value
			Color HN2TitleVPColor = Color.fromString(N2TitleVPColor);
			String HexN2TitleVPColor = HN2TitleVPColor.asHex();
			System.out.println("Actual Hex Color of Number Tab 02 Title in Variety Page is : "	+ HexN2TitleVPColor );
			
			String N2TitleVPFontSize = N2TitleVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Number Tab 02 Title in Variety Page is : "	+ N2TitleVPFontSize);
			String N2TitleVPFontFamily = N2TitleVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Number Tab 02 Title in Variety Page is : "	+ N2TitleVPFontFamily);
			String N2TitleVPContent = N2TitleVP.getText();
			System.out.println("Actual Content of Number Tab 02 Title in Variety Page is : "	+ N2TitleVPContent);
					
			String ExpN2TitleVPColor = sheet4.getCell(1,7).getContents(); //column,row
			System.out.println("Expected Color of Number Tab 02 Title in Variety Page is : "	+ ExpN2TitleVPColor );
			String ExpN2TitleVPFontSize = sheet4.getCell(2,7).getContents();
			System.out.println("Expected Font Size of Number Tab 02 Title in Variety Page is : "	+ ExpN2TitleVPFontSize);
			String ExpN2TitleVPFontFamily = sheet4.getCell(3,7).getContents();
			System.out.println("Expected Font Family of Number Tab 02 Title in Variety Page is : "	+ ExpN2TitleVPFontFamily);
			String ExpN2TitleVPContent = sheet4.getCell(4,7).getContents();
			System.out.println("Expected Content of Number Tab 02 Title in Variety Page is : "	+ ExpN2TitleVPContent);
					
			if(HexN2TitleVPColor.equals(ExpN2TitleVPColor))
			{
				System.out.println("Number Tab 02 Title in Variety Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("Number Tab 02 Title in Variety Page Color is not in match with creative - Fail");
				bw.write("Number Tab 02 Title in Variety Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(N2TitleVPFontSize.equals(ExpN2TitleVPFontSize))
			{
				System.out.println("Number Tab 02 Title in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Number Tab 02 Title in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Number Tab 02 Title in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(N2TitleVPFontFamily.equals(ExpN2TitleVPFontFamily))
			{
				System.out.println("Number Tab 02 Title in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Number Tab 02 Title in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Number Tab 02 Title in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(N2TitleVPContent.equals(ExpN2TitleVPContent))
			{
				System.out.println("Number Tab 02 Title Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Number Tab 02 Title Content in Variety Page is not in match with creative - Fail");
				bw.write("Number Tab 02 Title Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}	
			
			if(HexN2TitleVPColor.equals(ExpN2TitleVPColor) && N2TitleVPFontSize.equals(ExpN2TitleVPFontSize) && N2TitleVPFontFamily.equals(ExpN2TitleVPFontFamily) && N2TitleVPContent.equals(ExpN2TitleVPContent))
			{
				Label N2TitleVPCSSP = new Label(5,7,"PASS"); 
				wsheet4.addCell(N2TitleVPCSSP); 
			}
			else
			{
				Label N2TitleVPCSSF = new Label(5,7,"FAIL"); 
				wsheet4.addCell(N2TitleVPCSSF); 
			}
			
			// Variety Page Num Tab 2 - Desc
			
			WebElement N2DescVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[2]/div/div/div/div[2]/div/div/div[1]/div[1]/div[2]/div[2]/div[2]/div[2]")); 
			String N2DescVPColor = N2DescVP.getCssValue("color");
			System.out.println("Actual Color of Number Tab 02 Desc in Variety Page is : "	+ N2DescVPColor );
			// Convert RGB to Hex Value
			Color HN2DescVPColor = Color.fromString(N2DescVPColor);
			String HexN2DescVPColor = HN2DescVPColor.asHex();
			System.out.println("Actual Hex Color of Number Tab 02 Desc in Variety Page is : "	+ HexN2DescVPColor );
			
			String N2DescVPFontSize = N2DescVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Number Tab 02 Desc in Variety Page is : "	+ N2DescVPFontSize);
			String N2DescVPFontFamily = N2DescVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Number Tab 02 Desc in Variety Page is : "	+ N2DescVPFontFamily);
			String N2DescVPContent = N2DescVP.getText();
			System.out.println("Actual Content of Number Tab 02 Desc in Variety Page is : "	+ N2DescVPContent);
					
			String ExpN2DescVPColor = sheet4.getCell(1,8).getContents(); //column,row
			System.out.println("Expected Color of Number Tab 02 Desc in Variety Page is : "	+ ExpN2DescVPColor );
			String ExpN2DescVPFontSize = sheet4.getCell(2,8).getContents();
			System.out.println("Expected Font Size of Number Tab 02 Desc in Variety Page is : "	+ ExpN2DescVPFontSize);
			String ExpN2DescVPFontFamily = sheet4.getCell(3,8).getContents();
			System.out.println("Expected Font Family of Number Tab 02 Desc in Variety Page is : "	+ ExpN2DescVPFontFamily);
			String ExpN2DescVPContent = sheet4.getCell(4,8).getContents();
			System.out.println("Expected Content of Number Tab 02 Desc in Variety Page is : "	+ ExpN2DescVPContent);
					
			if(HexN2DescVPColor.equals(ExpN2DescVPColor))
			{
				System.out.println("Number Tab 02 Desc in Variety Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("Number Tab 02 Desc in Variety Page Color is not in match with creative - Fail");
				bw.write("Number Tab 02 Desc in Variety Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(N2DescVPFontSize.equals(ExpN2DescVPFontSize))
			{
				System.out.println("Number Tab 02 Desc in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Number Tab 02 Desc in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Number Tab 02 Desc in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(N2DescVPFontFamily.equals(ExpN2DescVPFontFamily))
			{
				System.out.println("Number Tab 02 Desc in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Number Tab 02 Desc in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Number Tab 02 Desc in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(N2DescVPContent.equals(ExpN2DescVPContent))
			{
				System.out.println("Number Tab 02 Desc Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Number Tab 02 Desc Content in Variety Page is not in match with creative - Fail");
				bw.write("Number Tab 02 Desc Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}
			
			if(HexN2DescVPColor.equals(ExpN2DescVPColor) && N2DescVPFontSize.equals(ExpN2DescVPFontSize) && N2DescVPFontFamily.equals(ExpN2DescVPFontFamily) && N2DescVPContent.equals(ExpN2DescVPContent))
			{
				Label N2DescVPCSSP = new Label(5,8,"PASS"); 
				wsheet4.addCell(N2DescVPCSSP); 
			}
			else
			{
				Label N2DescVPCSSF = new Label(5,8,"FAIL"); 
				wsheet4.addCell(N2DescVPCSSF); 
			}
			
		   // Variety Page Num Tab 2 - Shop Now on Keurig.com
			
			WebElement N2ShopVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[2]/div/div/div/div[2]/div/div/div[1]/div[1]/div[2]/div[2]/div[2]/a")); 
			String N2ShopVPColor = N2ShopVP.getCssValue("color");
			System.out.println("Actual Color of Number Tab 02 Shop Now on Keurig.com in Variety Page is : "	+ N2ShopVPColor );
			// Convert RGB to Hex Value
			Color HN2ShopVPColor = Color.fromString(N2ShopVPColor);
			String HexN2ShopVPColor = HN2ShopVPColor.asHex();
			System.out.println("Actual Hex Color of Number Tab 02 Shop Now on Keurig.com in Variety Page is : "	+ HexN2ShopVPColor );
			
			String N2ShopVPFontSize = N2ShopVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Number Tab 02 Shop Now on Keurig.com in Variety Page is : "	+ N2ShopVPFontSize);
			String N2ShopVPFontFamily = N2ShopVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Number Tab 02 Shop Now on Keurig.com in Variety Page is : "	+ N2ShopVPFontFamily);
			String N2ShopVPContent = N2ShopVP.getText();
			System.out.println("Actual Content of Number Tab 02 Shop Now on Keurig.com in Variety Page is : "	+ N2ShopVPContent);
					
			String ExpN2ShopVPColor = sheet4.getCell(1,9).getContents(); //column,row
			System.out.println("Expected Color of Number Tab 02 Shop Now on Keurig.com in Variety Page is : "	+ ExpN2ShopVPColor );
			String ExpN2ShopVPFontSize = sheet4.getCell(2,9).getContents();
			System.out.println("Expected Font Size of Number Tab 02 Shop Now on Keurig.com in Variety Page is : "	+ ExpN2ShopVPFontSize);
			String ExpN2ShopVPFontFamily = sheet4.getCell(3,9).getContents();
			System.out.println("Expected Font Family of Number Tab 02 Shop Now on Keurig.com in Variety Page is : "	+ ExpN2ShopVPFontFamily);
			String ExpN2ShopVPContent = sheet4.getCell(4,9).getContents();
			System.out.println("Expected Content of Number Tab 02 Shop Now on Keurig.com in Variety Page is : "	+ ExpN2ShopVPContent);
					
			if(HexN2ShopVPColor.equals(ExpN2ShopVPColor))
			{
				System.out.println("Number Tab 02 Shop Now on Keurig.com in Variety Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("Number Tab 02 Shop Now on Keurig.com in Variety Page Color is not in match with creative - Fail");
				bw.write("Number Tab 02 Shop Now on Keurig.com in Variety Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(N2ShopVPFontSize.equals(ExpN2ShopVPFontSize))
			{
				System.out.println("Number Tab 02 Shop Now on Keurig.com in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Number Tab 02 Shop Now on Keurig.com in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Number Tab 02 Shop Now on Keurig.com in Variety Page Font Size is not in match with creative - Fail");  
			    bw.newLine();
			}
					
			if(N2ShopVPFontFamily.equals(ExpN2ShopVPFontFamily))
			{
				System.out.println("Number Tab 02 Shop Now on Keurig.com in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Number Tab 02 Shop Now on Keurig.com in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Number Tab 02 Shop Now on Keurig.com in Variety Page Font Family is not in match with creative - Fail");  
			    bw.newLine();
			}
			

			if(N2ShopVPContent.equals(ExpN2ShopVPContent))
			{
				System.out.println("Number Tab 02 Shop Now on Keurig.com Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Number Tab 02 Shop Now on Keurig.com Content in Variety Page is not in match with creative - Fail");
				bw.write("Number Tab 02 Shop Now on Keurig.com Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}	
			
			if(HexN2ShopVPColor.equals(ExpN2ShopVPColor) && N2ShopVPFontSize.equals(ExpN2ShopVPFontSize) && N2ShopVPFontFamily.equals(ExpN2ShopVPFontFamily) && N2ShopVPContent.equals(ExpN2ShopVPContent))
			{
				Label N2ShopVPCSSP = new Label(5,9,"PASS"); 
				wsheet4.addCell(N2ShopVPCSSP); 
			}
			else
			{
				Label N2ShopVPCSSF = new Label(5,9,"FAIL"); 
				wsheet4.addCell(N2ShopVPCSSF); 
			}
			
			// Click Action on Number Tab 3 in Variety Page
			
			driver.findElement(By.xpath(".//*[@id='id_3_tab']")).click();
			Thread.sleep(5000);
			
			// Variety Page Num Tab 3 - Title
			
			WebElement N3TitleVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[2]/div/div/div/div[2]/div/div/div[1]/div[1]/div[3]/div[2]/div[2]/div[1]")); 
			String N3TitleVPColor = N3TitleVP.getCssValue("color");
			System.out.println("Actual Color of Number Tab 03 Title in Variety Page is : "	+ N3TitleVPColor );
			// Convert RGB to Hex Value
			Color HN3TitleVPColor = Color.fromString(N3TitleVPColor);
			String HexN3TitleVPColor = HN3TitleVPColor.asHex();
			System.out.println("Actual Hex Color of Number Tab 03 Title in Variety Page is : "	+ HexN3TitleVPColor );
			
			String N3TitleVPFontSize = N3TitleVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Number Tab 03 Title in Variety Page is : "	+ N3TitleVPFontSize);
			String N3TitleVPFontFamily = N3TitleVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Number Tab 03 Title in Variety Page is : "	+ N3TitleVPFontFamily);
			String N3TitleVPContent = N3TitleVP.getText();
			System.out.println("Actual Content of Number Tab 03 Title in Variety Page is : "	+ N3TitleVPContent);
					
			String ExpN3TitleVPColor = sheet4.getCell(1,10).getContents(); //column,row
			System.out.println("Expected Color of Number Tab 03 Title in Variety Page is : "	+ ExpN3TitleVPColor );
			String ExpN3TitleVPFontSize = sheet4.getCell(2,10).getContents();
			System.out.println("Expected Font Size of Number Tab 03 Title in Variety Page is : "	+ ExpN3TitleVPFontSize);
			String ExpN3TitleVPFontFamily = sheet4.getCell(3,10).getContents();
			System.out.println("Expected Font Family of Number Tab 03 Title in Variety Page is : "	+ ExpN3TitleVPFontFamily);
			String ExpN3TitleVPContent = sheet4.getCell(4,10).getContents();
			System.out.println("Expected Content of Number Tab 03 Title in Variety Page is : "	+ ExpN3TitleVPContent);
					
			if(HexN3TitleVPColor.equals(ExpN3TitleVPColor))
			{
				System.out.println("Number Tab 03 Title in Variety Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("Number Tab 03 Title in Variety Page Color is not in match with creative - Fail");
				bw.write("Number Tab 03 Title in Variety Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(N3TitleVPFontSize.equals(ExpN3TitleVPFontSize))
			{
				System.out.println("Number Tab 03 Title in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Number Tab 03 Title in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Number Tab 03 Title in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(N3TitleVPFontFamily.equals(ExpN3TitleVPFontFamily))
			{
				System.out.println("Number Tab 03 Title in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Number Tab 03 Title in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Number Tab 03 Title in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(N3TitleVPContent.equals(ExpN3TitleVPContent))
			{
				System.out.println("Number Tab 03 Title Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Number Tab 03 Title Content in Variety Page is not in match with creative - Fail");
				bw.write("Number Tab 03 Title Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}	
			
			if(HexN3TitleVPColor.equals(ExpN3TitleVPColor) && N3TitleVPFontSize.equals(ExpN3TitleVPFontSize) && N3TitleVPFontFamily.equals(ExpN3TitleVPFontFamily) && N3TitleVPContent.equals(ExpN3TitleVPContent))
			{
				Label N3TitleVPCSSP = new Label(5,10,"PASS"); 
				wsheet4.addCell(N3TitleVPCSSP); 
			}
			else
			{
				Label N3TitleVPCSSF = new Label(5,10,"FAIL"); 
				wsheet4.addCell(N3TitleVPCSSF); 
			}
			
			// Variety Page Num Tab 3 - Desc
			
			WebElement N3DescVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[2]/div/div/div/div[2]/div/div/div[1]/div[1]/div[3]/div[2]/div[2]/div[2]")); 
			String N3DescVPColor = N3DescVP.getCssValue("color");
			System.out.println("Actual Color of Number Tab 03 Desc in Variety Page is : "	+ N3DescVPColor );
			// Convert RGB to Hex Value
			Color HN3DescVPColor = Color.fromString(N3DescVPColor);
			String HexN3DescVPColor = HN3DescVPColor.asHex();
			System.out.println("Actual Hex Color of Number Tab 03 Desc in Variety Page is : "	+ HexN3DescVPColor );
			
			String N3DescVPFontSize = N3DescVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Number Tab 03 Desc in Variety Page is : "	+ N3DescVPFontSize);
			String N3DescVPFontFamily = N3DescVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Number Tab 03 Desc in Variety Page is : "	+ N3DescVPFontFamily);
			String N3DescVPContent = N3DescVP.getText();
			System.out.println("Actual Content of Number Tab 03 Desc in Variety Page is : "	+ N3DescVPContent);
					
			String ExpN3DescVPColor = sheet4.getCell(1,11).getContents(); //column,row
			System.out.println("Expected Color of Number Tab 03 Desc in Variety Page is : "	+ ExpN3DescVPColor );
			String ExpN3DescVPFontSize = sheet4.getCell(2,11).getContents();
			System.out.println("Expected Font Size of Number Tab 03 Desc in Variety Page is : "	+ ExpN3DescVPFontSize);
			String ExpN3DescVPFontFamily = sheet4.getCell(3,11).getContents();
			System.out.println("Expected Font Family of Number Tab 03 Desc in Variety Page is : "	+ ExpN3DescVPFontFamily);
			String ExpN3DescVPContent = sheet4.getCell(4,11).getContents();
			System.out.println("Expected Content of Number Tab 03 Desc in Variety Page is : "	+ ExpN3DescVPContent);
					
			if(HexN3DescVPColor.equals(ExpN3DescVPColor))
			{
				System.out.println("Number Tab 03 Desc in Variety Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("Number Tab 03 Desc in Variety Page Color is not in match with creative - Fail");
				bw.write("Number Tab 03 Desc in Variety Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(N3DescVPFontSize.equals(ExpN3DescVPFontSize))
			{
				System.out.println("Number Tab 03 Desc in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Number Tab 03 Desc in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Number Tab 03 Desc in Variety Page Font Size is not in match with creative - Fail");  
			    bw.newLine();
			}
					
			if(N3DescVPFontFamily.equals(ExpN3DescVPFontFamily))
			{
				System.out.println("Number Tab 03 Desc in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Number Tab 03 Desc in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Number Tab 03 Desc in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(N3DescVPContent.equals(ExpN3DescVPContent))
			{
				System.out.println("Number Tab 03 Desc Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Number Tab 03 Desc Content in Variety Page is not in match with creative - Fail");
				bw.write("Number Tab 03 Desc Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}	
			
			if(HexN3DescVPColor.equals(ExpN3DescVPColor) && N3DescVPFontSize.equals(ExpN3DescVPFontSize) && N3DescVPFontFamily.equals(ExpN3DescVPFontFamily) && N3DescVPContent.equals(ExpN3DescVPContent))
			{
				Label N3DescVPCSSP = new Label(5,11,"PASS"); 
				wsheet4.addCell(N3DescVPCSSP); 
			}
			else
			{
				Label N3DescVPCSSF = new Label(5,11,"FAIL"); 
				wsheet4.addCell(N3DescVPCSSF); 
			}
			
		   // Variety Page Num Tab 3 - Shop Now on Keurig.com
			
			WebElement N3ShopVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[2]/div/div/div/div[2]/div/div/div[1]/div[1]/div[3]/div[2]/div[2]/a")); 
			String N3ShopVPColor = N3ShopVP.getCssValue("color");
			System.out.println("Actual Color of Number Tab 03 Shop Now on Keurig.com in Variety Page is : "	+ N3ShopVPColor );
			// Convert RGB to Hex Value
			Color HN3ShopVPColor = Color.fromString(N3ShopVPColor);
			String HexN3ShopVPColor = HN3ShopVPColor.asHex();
			System.out.println("Actual Hex Color of Number Tab 03 Shop Now on Keurig.com in Variety Page is : "	+ HexN3ShopVPColor );
			
			String N3ShopVPFontSize = N3ShopVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Number Tab 03 Shop Now on Keurig.com in Variety Page is : "	+ N3ShopVPFontSize);
			String N3ShopVPFontFamily = N3ShopVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Number Tab 03 Shop Now on Keurig.com in Variety Page is : "	+ N3ShopVPFontFamily);
			String N3ShopVPContent = N3ShopVP.getText();
			System.out.println("Actual Content of Number Tab 03 Shop Now on Keurig.com in Variety Page is : "	+ N3ShopVPContent);
					
			String ExpN3ShopVPColor = sheet4.getCell(1,12).getContents(); //column,row
			System.out.println("Expected Color of Number Tab 03 Shop Now on Keurig.com in Variety Page is : "	+ ExpN3ShopVPColor );
			String ExpN3ShopVPFontSize = sheet4.getCell(2,12).getContents();
			System.out.println("Expected Font Size of Number Tab 03 Shop Now on Keurig.com in Variety Page is : "	+ ExpN3ShopVPFontSize);
			String ExpN3ShopVPFontFamily = sheet4.getCell(3,12).getContents();
			System.out.println("Expected Font Family of Number Tab 03 Shop Now on Keurig.com in Variety Page is : "	+ ExpN3ShopVPFontFamily);
			String ExpN3ShopVPContent = sheet4.getCell(4,12).getContents();
			System.out.println("Expected Content of Number Tab 03 Shop Now on Keurig.com in Variety Page is : "	+ ExpN3ShopVPContent);
					
			if(HexN3ShopVPColor.equals(ExpN3ShopVPColor))
			{
				System.out.println("Number Tab 03 Shop Now on Keurig.com in Variety Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("Number Tab 03 Shop Now on Keurig.com in Variety Page Color is not in match with creative - Fail");
				bw.write("Number Tab 03 Shop Now on Keurig.com in Variety Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(N3ShopVPFontSize.equals(ExpN3ShopVPFontSize))
			{
				System.out.println("Number Tab 03 Shop Now on Keurig.com in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Number Tab 03 Shop Now on Keurig.com in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Number Tab 03 Shop Now on Keurig.com in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(N3ShopVPFontFamily.equals(ExpN3ShopVPFontFamily))
			{
				System.out.println("Number Tab 03 Shop Now on Keurig.com in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Number Tab 03 Shop Now on Keurig.com in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Number Tab 03 Shop Now on Keurig.com in Variety Page Font Family is not in match with creative - Fail");  
			    bw.newLine();
			}
			

			if(N3ShopVPContent.equals(ExpN3ShopVPContent))
			{
				System.out.println("Number Tab 03 Shop Now on Keurig.com Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Number Tab 03 Shop Now on Keurig.com Content in Variety Page is not in match with creative - Fail");
				bw.write("Number Tab 03 Shop Now on Keurig.com Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}	
			
			if(HexN3ShopVPColor.equals(ExpN3ShopVPColor) && N3ShopVPFontSize.equals(ExpN3ShopVPFontSize) && N3ShopVPFontFamily.equals(ExpN3ShopVPFontFamily) && N3ShopVPContent.equals(ExpN3ShopVPContent))
			{
				Label N3ShopVPCSSP = new Label(5,12,"PASS"); 
				wsheet4.addCell(N3ShopVPCSSP); 
			}
			else
			{
				Label N3ShopVPCSSF = new Label(5,12,"FAIL"); 
				wsheet4.addCell(N3ShopVPCSSF); 
			}
			
			// Variety Page - Our Coffee Collection
			
			WebElement OurCoffeeVP = driver.findElement(By.className("sk_variety_ourcoffee")); 
			
			String OurCoffeeVPFontSize = OurCoffeeVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection in Variety Page is : "	+ OurCoffeeVPFontSize);
			String OurCoffeeVPFontFamily = OurCoffeeVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection in Variety Page is : "	+ OurCoffeeVPFontFamily);
			String OurCoffeeVPContent = OurCoffeeVP.getText();
			System.out.println("Actual Content of Our Coffee Collection in Variety Page is : "	+ OurCoffeeVPContent);
					
			String ExpOurCoffeeVPFontSize = sheet4.getCell(2,13).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection in Variety Page is : "	+ ExpOurCoffeeVPFontSize);
			String ExpOurCoffeeVPFontFamily = sheet4.getCell(3,13).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection in Variety Page is : "	+ ExpOurCoffeeVPFontFamily);
			String ExpOurCoffeeVPContent = sheet4.getCell(4,13).getContents();
			System.out.println("Expected Content of Our Coffee Collection in Variety Page is : "	+ ExpOurCoffeeVPContent);
			
			if(OurCoffeeVPFontSize.equals(ExpOurCoffeeVPFontSize))
			{
				System.out.println("Our Coffee Collection in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(OurCoffeeVPFontFamily.equals(ExpOurCoffeeVPFontFamily))
			{
				System.out.println("Our Coffee Collection in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(OurCoffeeVPContent.equals(ExpOurCoffeeVPContent))
			{
				System.out.println("Our Coffee Collection Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}	
			
			if(OurCoffeeVPFontSize.equals(ExpOurCoffeeVPFontSize) && OurCoffeeVPFontFamily.equals(ExpOurCoffeeVPFontFamily) && OurCoffeeVPContent.equals(ExpOurCoffeeVPContent))
			{
				Label OurCoffeeVPCSSP = new Label(5,13,"PASS"); 
				wsheet4.addCell(OurCoffeeVPCSSP); 
			}
			else
			{
				Label OurCoffeeVPCSSF = new Label(5,13,"FAIL"); 
				wsheet4.addCell(OurCoffeeVPCSSF); 
			}
			
			// Variety Page - Our Coffee Collection - Desc
			
			WebElement OurCoffeeDescVP = driver.findElement(By.className("sk_variety_ourcoffeeDesc")); 
			
			String OurCoffeeDescVPFontSize = OurCoffeeDescVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection Desc in Variety Page is : "	+ OurCoffeeDescVPFontSize);
			String OurCoffeeDescVPFontFamily = OurCoffeeDescVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection Desc in Variety Page is : "	+ OurCoffeeDescVPFontFamily);
			String OurCoffeeDescVPContent = OurCoffeeDescVP.getText();
			System.out.println("Actual Content of Our Coffee Collection Desc in Variety Page is : "	+ OurCoffeeDescVPContent);
					
			String ExpOurCoffeeDescVPFontSize = sheet4.getCell(2,14).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection Desc in Variety Page is : "	+ ExpOurCoffeeDescVPFontSize);
			String ExpOurCoffeeDescVPFontFamily = sheet4.getCell(3,14).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection Desc in Variety Page is : "	+ ExpOurCoffeeDescVPFontFamily);
			String ExpOurCoffeeDescVPContent = sheet4.getCell(4,14).getContents();
			System.out.println("Expected Content of Our Coffee Collection Desc in Variety Page is : "	+ ExpOurCoffeeDescVPContent);
			
			if(OurCoffeeDescVPFontSize.equals(ExpOurCoffeeDescVPFontSize))
			{
				System.out.println("Our Coffee Collection Desc in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Desc in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection Desc in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(OurCoffeeDescVPFontFamily.equals(ExpOurCoffeeDescVPFontFamily))
			{
				System.out.println("Our Coffee Collection Desc in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Desc in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection Desc in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(OurCoffeeDescVPContent.equals(ExpOurCoffeeDescVPContent))
			{
				System.out.println("Our Coffee Collection Desc Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Desc Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection Desc Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}
			
			if(OurCoffeeDescVPFontSize.equals(ExpOurCoffeeDescVPFontSize) && OurCoffeeDescVPFontFamily.equals(ExpOurCoffeeDescVPFontFamily) && OurCoffeeDescVPContent.equals(ExpOurCoffeeDescVPContent))
			{
				Label OurCoffeeDescVPCSSP = new Label(5,14,"PASS"); 
				wsheet4.addCell(OurCoffeeDescVPCSSP); 
			}
			else
			{
				Label OurCoffeeDescVPCSSF = new Label(5,14,"FAIL"); 
				wsheet4.addCell(OurCoffeeDescVPCSSF); 
			}
				
			Thread.sleep(3000);
			JavascriptExecutor jsefavcss = (JavascriptExecutor)driver;
			jsefavcss.executeScript("window.scrollBy(0,500)", "");
			Thread.sleep(8000);
			
			// Variety Page - Our Coffee Collection - FAVORITES
			
			WebElement OurCoffeeFavVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[4]/div/div/div/div/div[3]")); 
			
			String OurCoffeeFavVPFontSize = OurCoffeeFavVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection FAVORITES in Variety Page is : "	+ OurCoffeeFavVPFontSize);
			String OurCoffeeFavVPFontFamily = OurCoffeeFavVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection FAVORITES in Variety Page is : "	+ OurCoffeeFavVPFontFamily);
			String OurCoffeeFavVPContent = OurCoffeeFavVP.getText();
			System.out.println("Actual Content of Our Coffee Collection FAVORITES in Variety Page is : "	+ OurCoffeeFavVPContent);
					
			String ExpOurCoffeeFavVPFontSize = sheet4.getCell(2,15).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection FAVORITES in Variety Page is : "	+ ExpOurCoffeeFavVPFontSize);
			String ExpOurCoffeeFavVPFontFamily = sheet4.getCell(3,15).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection FAVORITES in Variety Page is : "	+ ExpOurCoffeeFavVPFontFamily);
			String ExpOurCoffeeFavVPContent = sheet4.getCell(4,15).getContents();
			System.out.println("Expected Content of Our Coffee Collection FAVORITES in Variety Page is : "	+ ExpOurCoffeeFavVPContent);
			
			if(OurCoffeeFavVPFontSize.equals(ExpOurCoffeeFavVPFontSize))
			{
				System.out.println("Our Coffee Collection FAVORITES in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection FAVORITES in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection FAVORITES in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(OurCoffeeFavVPFontFamily.equals(ExpOurCoffeeFavVPFontFamily))
			{
				System.out.println("Our Coffee Collection FAVORITES in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection FAVORITES in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection FAVORITES in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(OurCoffeeFavVPContent.equals(ExpOurCoffeeFavVPContent))
			{
				System.out.println("Our Coffee Collection FAVORITES Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection FAVORITES Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection FAVORITES Content in Variety Page is not in match with creative - Fail");  
			    bw.newLine();
			}	
			
			if(OurCoffeeFavVPFontSize.equals(ExpOurCoffeeFavVPFontSize) && OurCoffeeFavVPFontFamily.equals(ExpOurCoffeeFavVPFontFamily) && OurCoffeeFavVPContent.equals(ExpOurCoffeeFavVPContent))
			{
				Label OurCoffeeFavVPCSSP = new Label(5,15,"PASS"); 
				wsheet4.addCell(OurCoffeeFavVPCSSP); 
			}
			else
			{
				Label OurCoffeeFavVPCSSF = new Label(5,15,"FAIL"); 
				wsheet4.addCell(OurCoffeeFavVPCSSF); 
			}
			
			// Variety Page - Our Coffee Collection - FAVORITES - REGULAR
			
			WebElement OurCoffeeFavRegVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[4]/div/div/div/div/div[4]/div[1]/div[2]/div[1]")); 
			
			String OurCoffeeFavRegVPFontSize = OurCoffeeFavRegVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection REGULAR in Variety Page is : "	+ OurCoffeeFavRegVPFontSize);
			String OurCoffeeFavRegVPFontFamily = OurCoffeeFavRegVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection REGULAR in Variety Page is : "	+ OurCoffeeFavRegVPFontFamily);
			String OurCoffeeFavRegVPContent = OurCoffeeFavRegVP.getText();
			System.out.println("Actual Content of Our Coffee Collection REGULAR in Variety Page is : "	+ OurCoffeeFavRegVPContent);
					
			String ExpOurCoffeeFavRegVPFontSize = sheet4.getCell(2,16).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection REGULAR in Variety Page is : "	+ ExpOurCoffeeFavRegVPFontSize);
			String ExpOurCoffeeFavRegVPFontFamily = sheet4.getCell(3,16).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection REGULAR in Variety Page is : "	+ ExpOurCoffeeFavRegVPFontFamily);
			String ExpOurCoffeeFavRegVPContent = sheet4.getCell(4,16).getContents();
			System.out.println("Expected Content of Our Coffee Collection REGULAR in Variety Page is : "	+ ExpOurCoffeeFavRegVPContent);
			
			if(OurCoffeeFavRegVPFontSize.equals(ExpOurCoffeeFavRegVPFontSize))
			{
				System.out.println("Our Coffee Collection REGULAR in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection REGULAR in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection REGULAR in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(OurCoffeeFavRegVPFontFamily.equals(ExpOurCoffeeFavRegVPFontFamily))
			{
				System.out.println("Our Coffee Collection REGULAR in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection REGULAR in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection REGULAR in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(OurCoffeeFavRegVPContent.equals(ExpOurCoffeeFavRegVPContent))
			{
				System.out.println("Our Coffee Collection REGULAR Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection REGULAR Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection REGULAR Content in Variety Page is not in match with creative - Fail");  
			    bw.newLine();
			}	
			
			if(OurCoffeeFavRegVPFontSize.equals(ExpOurCoffeeFavRegVPFontSize) && OurCoffeeFavRegVPFontFamily.equals(ExpOurCoffeeFavRegVPFontFamily) && OurCoffeeFavRegVPContent.equals(ExpOurCoffeeFavRegVPContent))
			{
				Label OurCoffeeFavRegVPCSSP = new Label(5,16,"PASS"); 
				wsheet4.addCell(OurCoffeeFavRegVPCSSP); 
			}
			else
			{
				Label OurCoffeeFavRegVPCSSF = new Label(5,16,"FAIL"); 
				wsheet4.addCell(OurCoffeeFavRegVPCSSF); 
			}
			
			// Variety Page - Our Coffee Collection - FAVORITES - REGULAR Desc
			
			WebElement FavRegDescVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[4]/div/div/div/div/div[4]/div[1]/div[2]/div[2]")); 
			
			String FavRegDescVPFontSize = FavRegDescVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection REGULAR Desc in Variety Page is : "	+ FavRegDescVPFontSize);
			String FavRegDescVPFontFamily = FavRegDescVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection REGULAR Desc in Variety Page is : "	+ FavRegDescVPFontFamily);
			String FavRegDescVPContent = FavRegDescVP.getText();
			System.out.println("Actual Content of Our Coffee Collection REGULAR Desc in Variety Page is : "	+ FavRegDescVPContent);
					
			String ExpFavRegDescVPFontSize = sheet4.getCell(2,17).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection REGULAR Desc in Variety Page is : "	+ ExpFavRegDescVPFontSize);
			String ExpFavRegDescVPFontFamily = sheet4.getCell(3,17).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection REGULAR Desc in Variety Page is : "	+ ExpFavRegDescVPFontFamily);
			String ExpFavRegDescVPContent = sheet4.getCell(4,17).getContents();
			System.out.println("Expected Content of Our Coffee Collection REGULAR Desc in Variety Page is : "	+ ExpFavRegDescVPContent);
			
			if(FavRegDescVPFontSize.equals(ExpFavRegDescVPFontSize))
			{
				System.out.println("Our Coffee Collection REGULAR Desc in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection REGULAR Desc in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection REGULAR Desc in Variety Page Font Size is not in match with creative - Fail");  
			    bw.newLine();
			}
					
			if(FavRegDescVPFontFamily.equals(ExpFavRegDescVPFontFamily))
			{
				System.out.println("Our Coffee Collection REGULAR Desc in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection REGULAR Desc in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection REGULAR Desc in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(FavRegDescVPContent.equals(ExpFavRegDescVPContent))
			{
				System.out.println("Our Coffee Collection REGULAR Desc Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection REGULAR Desc Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection REGULAR Desc Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}	
			
			if(FavRegDescVPFontSize.equals(ExpFavRegDescVPFontSize) && FavRegDescVPFontFamily.equals(ExpFavRegDescVPFontFamily) && FavRegDescVPContent.equals(ExpFavRegDescVPContent))
			{
				Label FavRegDescVPCSSP = new Label(5,17,"PASS"); 
				wsheet4.addCell(FavRegDescVPCSSP); 
			}
			else
			{
				Label FavRegDescVPCSSF = new Label(5,17,"FAIL"); 
				wsheet4.addCell(FavRegDescVPCSSF); 
			}
			
			// Variety Page - Our Coffee Collection - FAVORITES - REGULAR Popular Varieties
			
			WebElement FavRegPVVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[4]/div/div/div/div/div[4]/div[1]/div[2]/div[3]")); 
			
			String FavRegPVVPFontSize = FavRegPVVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection REGULAR Popular Varieties in Variety Page is : "	+ FavRegPVVPFontSize);
			String FavRegPVVPFontFamily = FavRegPVVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection REGULAR Popular Varieties in Variety Page is : "	+ FavRegPVVPFontFamily);
			String FavRegPVVPContent = FavRegPVVP.getText();
			System.out.println("Actual Content of Our Coffee Collection REGULAR Popular Varieties in Variety Page is : "	+ FavRegPVVPContent);
					
			String ExpFavRegPVVPFontSize = sheet4.getCell(2,18).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection REGULAR Popular Varieties in Variety Page is : "	+ ExpFavRegPVVPFontSize);
			String ExpFavRegPVVPFontFamily = sheet4.getCell(3,18).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection REGULAR Popular Varieties in Variety Page is : "	+ ExpFavRegPVVPFontFamily);
			String ExpFavRegPVVPContent = sheet4.getCell(4,18).getContents();
			System.out.println("Expected Content of Our Coffee Collection REGULAR Popular Varieties in Variety Page is : "	+ ExpFavRegPVVPContent);
			
			if(FavRegPVVPFontSize.equals(ExpFavRegPVVPFontSize))
			{
				System.out.println("Our Coffee Collection REGULAR Popular Varieties in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection REGULAR Popular Varieties in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection REGULAR Popular Varieties in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(FavRegPVVPFontFamily.equals(ExpFavRegPVVPFontFamily))
			{
				System.out.println("Our Coffee Collection REGULAR Popular Varieties in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection REGULAR Popular Varieties in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection REGULAR Popular Varieties in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(FavRegPVVPContent.equals(ExpFavRegPVVPContent))
			{
				System.out.println("Our Coffee Collection REGULAR Popular Varieties Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection REGULAR Popular Varieties Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection REGULAR Popular Varieties Content in Variety Page is not in match with creative - Fail");  
			    bw.newLine();
			}
			
			if(FavRegPVVPFontSize.equals(ExpFavRegPVVPFontSize) && FavRegPVVPFontFamily.equals(ExpFavRegPVVPFontFamily) && FavRegPVVPContent.equals(ExpFavRegPVVPContent))
			{
				Label FavRegPVVPCSSP = new Label(5,18,"PASS"); 
				wsheet4.addCell(FavRegPVVPCSSP); 
			}
			else
			{
				Label FavRegPVVPCSSF = new Label(5,18,"FAIL"); 
				wsheet4.addCell(FavRegPVVPCSSF); 
			}
			
			// Variety Page - Our Coffee Collection - FAVORITES - REGULAR Popular Varieties - Breakfast Blend
			
			WebElement FavRegBBVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[4]/div/div/div/div/div[4]/div[1]/div[2]/div[4]/div[1]")); 
			
			String FavRegBBVPFontSize = FavRegBBVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection REGULAR Breakfast Blend in Variety Page is : "	+ FavRegBBVPFontSize);
			String FavRegBBVPFontFamily = FavRegBBVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection REGULAR Breakfast Blend in Variety Page is : "	+ FavRegBBVPFontFamily);
			String FavRegBBVPContent = FavRegBBVP.getText();
			System.out.println("Actual Content of Our Coffee Collection REGULAR Breakfast Blend in Variety Page is : "	+ FavRegBBVPContent);
					
			String ExpFavRegBBVPFontSize = sheet4.getCell(2,19).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection REGULAR Breakfast Blend in Variety Page is : "	+ ExpFavRegBBVPFontSize);
			String ExpFavRegBBVPFontFamily = sheet4.getCell(3,19).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection REGULAR Breakfast Blend in Variety Page is : "	+ ExpFavRegBBVPFontFamily);
			String ExpFavRegBBVPContent = sheet4.getCell(4,19).getContents();
			System.out.println("Expected Content of Our Coffee Collection REGULAR Breakfast Blend in Variety Page is : "	+ ExpFavRegBBVPContent);
			
			if(FavRegBBVPFontSize.equals(ExpFavRegBBVPFontSize))
			{
				System.out.println("Our Coffee Collection REGULAR Breakfast Blend in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection REGULAR Breakfast Blend in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection REGULAR Breakfast Blend in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(FavRegBBVPFontFamily.equals(ExpFavRegBBVPFontFamily))
			{
				System.out.println("Our Coffee Collection REGULAR Breakfast Blend in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection REGULAR Breakfast Blend in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection REGULAR Breakfast Blend in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(FavRegBBVPContent.equals(ExpFavRegBBVPContent))
			{
				System.out.println("Our Coffee Collection REGULAR Breakfast Blend Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection REGULAR Breakfast Blend Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection REGULAR Breakfast Blend Content in Variety Page is not in match with creative - Fail");  
			    bw.newLine();
			}	
			
			if(FavRegBBVPFontSize.equals(ExpFavRegBBVPFontSize) && FavRegBBVPFontFamily.equals(ExpFavRegBBVPFontFamily) && FavRegBBVPContent.equals(ExpFavRegBBVPContent))
			{
				Label FavRegBBVPCSSP = new Label(5,19,"PASS"); 
				wsheet4.addCell(FavRegBBVPCSSP); 
			}
			else
			{
				Label FavRegBBVPCSSF = new Label(5,19,"FAIL"); 
				wsheet4.addCell(FavRegBBVPCSSF); 
			}
			
			// Variety Page - Our Coffee Collection - FAVORITES - REGULAR Popular Varieties - Nantucket Blend�
			
			WebElement FavRegNBVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[4]/div/div/div/div/div[4]/div[1]/div[2]/div[4]/div[2]")); 
			
			String FavRegNBVPFontSize = FavRegNBVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection REGULAR Nantucket Blend� in Variety Page is : "	+ FavRegNBVPFontSize);
			String FavRegNBVPFontFamily = FavRegNBVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection REGULAR Nantucket Blend� in Variety Page is : "	+ FavRegNBVPFontFamily);
			String FavRegNBVPContent = FavRegNBVP.getText();
			System.out.println("Actual Content of Our Coffee Collection REGULAR Nantucket Blend� in Variety Page is : "	+ FavRegNBVPContent);
					
			String ExpFavRegNBVPFontSize = sheet4.getCell(2,20).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection REGULAR Nantucket Blend� in Variety Page is : "	+ ExpFavRegNBVPFontSize);
			String ExpFavRegNBVPFontFamily = sheet4.getCell(3,20).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection REGULAR Nantucket Blend� in Variety Page is : "	+ ExpFavRegNBVPFontFamily);
			String ExpFavRegNBVPContent = sheet4.getCell(4,20).getContents();
			System.out.println("Expected Content of Our Coffee Collection REGULAR Nantucket Blend� in Variety Page is : "	+ ExpFavRegNBVPContent);
			
			if(FavRegNBVPFontSize.equals(ExpFavRegNBVPFontSize))
			{
				System.out.println("Our Coffee Collection REGULAR Nantucket Blend� in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection REGULAR Nantucket Blend� in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection REGULAR Nantucket Blend� in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(FavRegNBVPFontFamily.equals(ExpFavRegNBVPFontFamily))
			{
				System.out.println("Our Coffee Collection REGULAR Nantucket Blend� in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection REGULAR Nantucket Blend� in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection REGULAR Nantucket Blend� in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(FavRegNBVPContent.equals(ExpFavRegNBVPContent))
			{
				System.out.println("Our Coffee Collection REGULAR Nantucket Blend� Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection REGULAR Nantucket Blend� Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection REGULAR Nantucket Blend� Content in Variety Page is not in match with creative - Fail");  
			    bw.newLine();
			}	
			
			if(FavRegNBVPFontSize.equals(ExpFavRegNBVPFontSize) && FavRegNBVPFontFamily.equals(ExpFavRegNBVPFontFamily) && FavRegNBVPContent.equals(ExpFavRegNBVPContent))
			{
				Label FavRegNBVPCSSP = new Label(5,20,"PASS"); 
				wsheet4.addCell(FavRegNBVPCSSP); 
			}
			else
			{
				Label FavRegNBVPCSSF = new Label(5,20,"FAIL"); 
				wsheet4.addCell(FavRegNBVPCSSF); 
			}
			
			// Variety Page - Our Coffee Collection - FAVORITES - REGULAR Popular Varieties - Dark Magic�
			
			WebElement FavRegDMVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[4]/div/div/div/div/div[4]/div[1]/div[2]/div[4]/div[3]")); 
			
			String FavRegDMVPFontSize = FavRegDMVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection REGULAR Dark Magic� in Variety Page is : "	+ FavRegDMVPFontSize);
			String FavRegDMVPFontFamily = FavRegDMVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection REGULAR Dark Magic� in Variety Page is : "	+ FavRegDMVPFontFamily);
			String FavRegDMVPContent = FavRegDMVP.getText();
			System.out.println("Actual Content of Our Coffee Collection REGULAR Dark Magic� in Variety Page is : "	+ FavRegDMVPContent);
					
			String ExpFavRegDMVPFontSize = sheet4.getCell(2,21).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection REGULAR Dark Magic� in Variety Page is : "	+ ExpFavRegDMVPFontSize);
			String ExpFavRegDMVPFontFamily = sheet4.getCell(3,21).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection REGULAR Dark Magic� in Variety Page is : "	+ ExpFavRegDMVPFontFamily);
			String ExpFavRegDMVPContent = sheet4.getCell(4,21).getContents();
			System.out.println("Expected Content of Our Coffee Collection REGULAR Dark Magic� in Variety Page is : "	+ ExpFavRegDMVPContent);
			
			if(FavRegDMVPFontSize.equals(ExpFavRegDMVPFontSize))
			{
				System.out.println("Our Coffee Collection REGULAR Dark Magic� in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection REGULAR Dark Magic� in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection REGULAR Dark Magic� in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(FavRegDMVPFontFamily.equals(ExpFavRegDMVPFontFamily))
			{
				System.out.println("Our Coffee Collection REGULAR Dark Magic� in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection REGULAR Dark Magic� in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection REGULAR Dark Magic� in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(FavRegDMVPContent.equals(ExpFavRegDMVPContent))
			{
				System.out.println("Our Coffee Collection REGULAR Dark Magic� Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection REGULAR Dark Magic� Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection REGULAR Dark Magic� Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}	
			
			if(FavRegDMVPFontSize.equals(ExpFavRegDMVPFontSize) && FavRegDMVPFontFamily.equals(ExpFavRegDMVPFontFamily) && FavRegDMVPContent.equals(ExpFavRegDMVPContent))
			{
				Label FavRegDMVPCSSP = new Label(5,21,"PASS"); 
				wsheet4.addCell(FavRegDMVPCSSP); 
			}
			else
			{
				Label FavRegDMVPCSSF = new Label(5,21,"FAIL"); 
				wsheet4.addCell(FavRegDMVPCSSF); 
			}
			
			// Variety Page - Our Coffee Collection - FAVORITES - REGULAR - Shop on Keurig.com 
			
			WebElement RegShopVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[4]/div/div/div/div/div[4]/div[1]/div[2]/div[5]/a/div")); 
			String RegShopVPColor = RegShopVP.getCssValue("color");
			System.out.println("Actual Color of Shop on Keurig.com in Regular in Variety Page is : "	+ RegShopVPColor );
			// Convert RGB to Hex Value
			Color HRegShopVPColor = Color.fromString(RegShopVPColor);
			String HexRegShopVPColor = HRegShopVPColor.asHex();
			System.out.println("Actual Hex Color of Shop on Keurig.com in Regular in Variety Page is : "	+ HexRegShopVPColor );
			
			String RegShopVPFontSize = RegShopVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Shop on Keurig.com in Regular in Variety Page is : "	+ RegShopVPFontSize);
			String RegShopVPFontFamily = RegShopVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Shop on Keurig.com in Regular in Variety Page is : "	+ RegShopVPFontFamily);
			String RegShopVPContent = RegShopVP.getText();
			System.out.println("Actual Content of Shop on Keurig.com in Regular in Variety Page is : "	+ RegShopVPContent);
					
			String ExpRegShopVPColor = sheet4.getCell(1,22).getContents(); //column,row
			System.out.println("Expected Color of Shop on Keurig.com in Regular in Variety Page is : "	+ ExpRegShopVPColor );
			String ExpRegShopVPFontSize = sheet4.getCell(2,22).getContents();
			System.out.println("Expected Font Size of Shop on Keurig.com in Regular in Variety Page is : "	+ ExpRegShopVPFontSize);
			String ExpRegShopVPFontFamily = sheet4.getCell(3,22).getContents();
			System.out.println("Expected Font Family of Shop on Keurig.com in Regular in Variety Page is : "	+ ExpRegShopVPFontFamily);
			String ExpRegShopVPContent = sheet4.getCell(4,22).getContents();
			System.out.println("Expected Content of Shop on Keurig.com in Regular in Variety Page is : "	+ ExpRegShopVPContent);
					
			if(HexRegShopVPColor.equals(ExpRegShopVPColor))
			{
				System.out.println("Shop on Keurig.com in Regular in Variety Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("Shop on Keurig.com in Regular in Variety Page Color is not in match with creative - Fail");
				bw.write("Shop on Keurig.com in Regular in Variety Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(RegShopVPFontSize.equals(ExpRegShopVPFontSize))
			{
				System.out.println("Shop on Keurig.com in Regular in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Shop on Keurig.com in Regular in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Shop on Keurig.com in Regular in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(RegShopVPFontFamily.equals(ExpRegShopVPFontFamily))
			{
				System.out.println("Shop on Keurig.com in Regular in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Shop on Keurig.com in Regular in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Shop on Keurig.com in Regular in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(RegShopVPContent.equals(ExpRegShopVPContent))
			{
				System.out.println("Shop on Keurig.com in Regular Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Shop on Keurig.com in Regular Content in Variety Page is not in match with creative - Fail");
				bw.write("Shop on Keurig.com in Regular Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}	
			
			if(HexRegShopVPColor.equals(ExpRegShopVPColor) && RegShopVPFontSize.equals(ExpRegShopVPFontSize) && RegShopVPFontFamily.equals(ExpRegShopVPFontFamily) && RegShopVPContent.equals(ExpRegShopVPContent))
			{
				Label RegShopVPCSSP = new Label(5,22,"PASS"); 
				wsheet4.addCell(RegShopVPCSSP); 
			}
			else
			{
				Label RegShopVPCSSF = new Label(5,22,"FAIL"); 
				wsheet4.addCell(RegShopVPCSSF); 
			}
		
			Thread.sleep(3000);
			JavascriptExecutor jseflacss = (JavascriptExecutor)driver;
			jseflacss.executeScript("window.scrollBy(0,600)", "");
			Thread.sleep(8000);
			
			// Variety Page - Our Coffee Collection - FAVORITES - Flavored
			
			WebElement OurCoffeeFavFlaVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[4]/div/div/div/div/div[4]/div[3]/div[2]/div[1]")); 
			
			String OurCoffeeFavFlaVPFontSize = OurCoffeeFavFlaVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection FLAVORED in Variety Page is : "	+ OurCoffeeFavFlaVPFontSize);
			String OurCoffeeFavFlaVPFontFamily = OurCoffeeFavFlaVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection FLAVORED in Variety Page is : "	+ OurCoffeeFavFlaVPFontFamily);
			String OurCoffeeFavFlaVPContent = OurCoffeeFavFlaVP.getText();
			System.out.println("Actual Content of Our Coffee Collection FLAVORED in Variety Page is : "	+ OurCoffeeFavFlaVPContent);
					
			String ExpOurCoffeeFavFlaVPFontSize = sheet4.getCell(2,23).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection FLAVORED in Variety Page is : "	+ ExpOurCoffeeFavFlaVPFontSize);
			String ExpOurCoffeeFavFlaVPFontFamily = sheet4.getCell(3,23).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection FLAVORED in Variety Page is : "	+ ExpOurCoffeeFavFlaVPFontFamily);
			String ExpOurCoffeeFavFlaVPContent = sheet4.getCell(4,23).getContents();
			System.out.println("Expected Content of Our Coffee Collection FLAVORED in Variety Page is : "	+ ExpOurCoffeeFavFlaVPContent);
			
			if(OurCoffeeFavFlaVPFontSize.equals(ExpOurCoffeeFavFlaVPFontSize))
			{
				System.out.println("Our Coffee Collection FLAVORED in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection FLAVORED in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection FLAVORED in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(OurCoffeeFavFlaVPFontFamily.equals(ExpOurCoffeeFavFlaVPFontFamily))
			{
				System.out.println("Our Coffee Collection FLAVORED in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection FLAVORED in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection FLAVORED in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(OurCoffeeFavFlaVPContent.equals(ExpOurCoffeeFavFlaVPContent))
			{
				System.out.println("Our Coffee Collection FLAVORED Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection FLAVORED Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection FLAVORED Content in Variety Page is not in match with creative - Fail");  
			    bw.newLine();
			}	
			
			if(OurCoffeeFavFlaVPFontSize.equals(ExpOurCoffeeFavFlaVPFontSize) && OurCoffeeFavFlaVPFontFamily.equals(ExpOurCoffeeFavFlaVPFontFamily) && OurCoffeeFavFlaVPContent.equals(ExpOurCoffeeFavFlaVPContent))
			{
				Label OurCoffeeFavFlaVPCSSP = new Label(5,23,"PASS"); 
				wsheet4.addCell(OurCoffeeFavFlaVPCSSP); 
			}
			else
			{
				Label OurCoffeeFavFlaVPCSSF = new Label(5,23,"FAIL"); 
				wsheet4.addCell(OurCoffeeFavFlaVPCSSF); 
			}
			
			// Variety Page - Our Coffee Collection - FAVORITES - FLAVORED Desc
			
			WebElement FavFlaDescVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[4]/div/div/div/div/div[4]/div[3]/div[2]/div[2]")); 
			
			String FavFlaDescVPFontSize = FavFlaDescVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection FLAVORED Desc in Variety Page is : "	+ FavFlaDescVPFontSize);
			String FavFlaDescVPFontFamily = FavFlaDescVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection FLAVORED Desc in Variety Page is : "	+ FavFlaDescVPFontFamily);
			String FavFlaDescVPContent = FavFlaDescVP.getText();
			System.out.println("Actual Content of Our Coffee Collection FLAVORED Desc in Variety Page is : "	+ FavFlaDescVPContent);
					
			String ExpFavFlaDescVPFontSize = sheet4.getCell(2,24).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection FLAVORED Desc in Variety Page is : "	+ ExpFavFlaDescVPFontSize);
			String ExpFavFlaDescVPFontFamily = sheet4.getCell(3,24).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection FLAVORED Desc in Variety Page is : "	+ ExpFavFlaDescVPFontFamily);
			String ExpFavFlaDescVPContent = sheet4.getCell(4,24).getContents();
			System.out.println("Expected Content of Our Coffee Collection FLAVORED Desc in Variety Page is : "	+ ExpFavFlaDescVPContent);
			
			if(FavFlaDescVPFontSize.equals(ExpFavFlaDescVPFontSize))
			{
				System.out.println("Our Coffee Collection FLAVORED Desc in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection FLAVORED Desc in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection FLAVORED Desc in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(FavFlaDescVPFontFamily.equals(ExpFavFlaDescVPFontFamily))
			{
				System.out.println("Our Coffee Collection FLAVORED Desc in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection FLAVORED Desc in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection FLAVORED Desc in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(FavFlaDescVPContent.equals(ExpFavFlaDescVPContent))
			{
				System.out.println("Our Coffee Collection FLAVORED Desc Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection FLAVORED Desc Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection FLAVORED Desc Content in Variety Page is not in match with creative - Fail");  
			    bw.newLine();
			}	
			
			if(FavFlaDescVPFontSize.equals(ExpFavFlaDescVPFontSize) && FavFlaDescVPFontFamily.equals(ExpFavFlaDescVPFontFamily) && FavFlaDescVPContent.equals(ExpFavFlaDescVPContent))
			{
				Label FavFlaDescVPCSSP = new Label(5,24,"PASS"); 
				wsheet4.addCell(FavFlaDescVPCSSP); 
			}
			else
			{
				Label FavFlaDescVPCSSF = new Label(5,24,"FAIL"); 
				wsheet4.addCell(FavFlaDescVPCSSF); 
			}
		
			// Variety Page - Our Coffee Collection - FAVORITES - FLAVORED Popular Varieties
			
			WebElement FavFlaPVVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[4]/div/div/div/div/div[4]/div[3]/div[2]/div[3]")); 
			
			String FavFlaPVVPFontSize = FavFlaPVVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection FLAVORED Popular Varieties in Variety Page is : "	+ FavFlaPVVPFontSize);
			String FavFlaPVVPFontFamily = FavFlaPVVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection FLAVORED Popular Varieties in Variety Page is : "	+ FavFlaPVVPFontFamily);
			String FavFlaPVVPContent = FavFlaPVVP.getText();
			System.out.println("Actual Content of Our Coffee Collection FLAVORED Popular Varieties in Variety Page is : "	+ FavFlaPVVPContent);
					
			String ExpFavFlaPVVPFontSize = sheet4.getCell(2,25).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection FLAVORED Popular Varieties in Variety Page is : "	+ ExpFavFlaPVVPFontSize);
			String ExpFavFlaPVVPFontFamily = sheet4.getCell(3,25).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection FLAVORED Popular Varieties in Variety Page is : "	+ ExpFavFlaPVVPFontFamily);
			String ExpFavFlaPVVPContent = sheet4.getCell(4,25).getContents();
			System.out.println("Expected Content of Our Coffee Collection FLAVORED Popular Varieties in Variety Page is : "	+ ExpFavFlaPVVPContent);
			
			if(FavFlaPVVPFontSize.equals(ExpFavFlaPVVPFontSize))
			{
				System.out.println("Our Coffee Collection FLAVORED Popular Varieties in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection FLAVORED Popular Varieties in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection FLAVORED Popular Varieties in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(FavFlaPVVPFontFamily.equals(ExpFavFlaPVVPFontFamily))
			{
				System.out.println("Our Coffee Collection FLAVORED Popular Varieties in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection FLAVORED Popular Varieties in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection FLAVORED Popular Varieties in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(FavFlaPVVPContent.equals(ExpFavFlaPVVPContent))
			{
				System.out.println("Our Coffee Collection FLAVORED Popular Varieties Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection FLAVORED Popular Varieties Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection FLAVORED Popular Varieties Content in Variety Page is not in match with creative - Fail");  
			    bw.newLine();
			}	
			
			if(FavFlaPVVPFontSize.equals(ExpFavFlaPVVPFontSize) && FavFlaPVVPFontFamily.equals(ExpFavFlaPVVPFontFamily) && FavFlaPVVPContent.equals(ExpFavFlaPVVPContent))
			{
				Label FavFlaPVVPCSSP = new Label(5,25,"PASS"); 
				wsheet4.addCell(FavFlaPVVPCSSP); 
			}
			else
			{
				Label FavFlaPVVPCSSF = new Label(5,25,"FAIL"); 
				wsheet4.addCell(FavFlaPVVPCSSF); 
			}
			
			// Variety Page - Our Coffee Collection - FAVORITES - FLAVORED Popular Varieties - Hazelnut
			
			WebElement FavFlaHNVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[4]/div/div/div/div/div[4]/div[3]/div[2]/div[4]/div[1]")); 
			
			String FavFlaHNVPFontSize = FavFlaHNVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection FLAVORED Hazelnut in Variety Page is : "	+ FavFlaHNVPFontSize);
			String FavFlaHNVPFontFamily = FavFlaHNVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection FLAVORED Hazelnut in Variety Page is : "	+ FavFlaHNVPFontFamily);
			String FavFlaHNVPContent = FavFlaHNVP.getText();
			System.out.println("Actual Content of Our Coffee Collection FLAVORED Hazelnut in Variety Page is : "	+ FavFlaHNVPContent);
					
			String ExpFavFlaHNVPFontSize = sheet4.getCell(2,26).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection FLAVORED Hazelnut in Variety Page is : "	+ ExpFavFlaHNVPFontSize);
			String ExpFavFlaHNVPFontFamily = sheet4.getCell(3,26).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection FLAVORED Hazelnut in Variety Page is : "	+ ExpFavFlaHNVPFontFamily);
			String ExpFavFlaHNVPContent = sheet4.getCell(4,26).getContents();
			System.out.println("Expected Content of Our Coffee Collection FLAVORED Hazelnut in Variety Page is : "	+ ExpFavFlaHNVPContent);
			
			if(FavFlaHNVPFontSize.equals(ExpFavFlaHNVPFontSize))
			{
				System.out.println("Our Coffee Collection FLAVORED Hazelnut in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection FLAVORED Hazelnut in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection FLAVORED Hazelnut in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(FavFlaHNVPFontFamily.equals(ExpFavFlaHNVPFontFamily))
			{
				System.out.println("Our Coffee Collection FLAVORED Hazelnut in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection FLAVORED Hazelnut in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection FLAVORED Hazelnut in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(FavFlaHNVPContent.equals(ExpFavFlaHNVPContent))
			{
				System.out.println("Our Coffee Collection FLAVORED Hazelnut Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection FLAVORED Hazelnut Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection FLAVORED Hazelnut Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}	
			
			if(FavFlaHNVPFontSize.equals(ExpFavFlaHNVPFontSize) && FavFlaHNVPFontFamily.equals(ExpFavFlaHNVPFontFamily) && FavFlaHNVPContent.equals(ExpFavFlaHNVPContent))
			{
				Label FavFlaHNVPCSSP = new Label(5,26,"PASS"); 
				wsheet4.addCell(FavFlaHNVPCSSP); 
			}
			else
			{
				Label FavFlaHNVPCSSF = new Label(5,26,"FAIL"); 
				wsheet4.addCell(FavFlaHNVPCSSF); 
			}
			
			
			// Variety Page - Our Coffee Collection - FAVORITES - FLAVORED Popular Varieties - Caramel Vanilla Cream
			
			WebElement FavFlaCVCVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[4]/div/div/div/div/div[4]/div[3]/div[2]/div[4]/div[2]")); 
			
			String FavFlaCVCVPFontSize = FavFlaCVCVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection FLAVORED Caramel Vanilla Cream in Variety Page is : "	+ FavFlaCVCVPFontSize);
			String FavFlaCVCVPFontFamily = FavFlaCVCVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection FLAVORED Caramel Vanilla Cream in Variety Page is : "	+ FavFlaCVCVPFontFamily);
			String FavFlaCVCVPContent = FavFlaCVCVP.getText();
			System.out.println("Actual Content of Our Coffee Collection FLAVORED Caramel Vanilla Cream in Variety Page is : "	+ FavFlaCVCVPContent);
					
			String ExpFavFlaCVCVPFontSize = sheet4.getCell(2,27).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection FLAVORED Caramel Vanilla Cream in Variety Page is : "	+ ExpFavFlaCVCVPFontSize);
			String ExpFavFlaCVCVPFontFamily = sheet4.getCell(3,27).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection FLAVORED Caramel Vanilla Cream in Variety Page is : "	+ ExpFavFlaCVCVPFontFamily);
			String ExpFavFlaCVCVPContent = sheet4.getCell(4,27).getContents();
			System.out.println("Expected Content of Our Coffee Collection FLAVORED Caramel Vanilla Cream in Variety Page is : "	+ ExpFavFlaCVCVPContent);
			
			if(FavFlaCVCVPFontSize.equals(ExpFavFlaCVCVPFontSize))
			{
				System.out.println("Our Coffee Collection FLAVORED Caramel Vanilla Cream in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection FLAVORED Caramel Vanilla Cream in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection FLAVORED Caramel Vanilla Cream in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(FavFlaCVCVPFontFamily.equals(ExpFavFlaCVCVPFontFamily))
			{
				System.out.println("Our Coffee Collection FLAVORED Caramel Vanilla Cream in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection FLAVORED Caramel Vanilla Cream in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection FLAVORED Caramel Vanilla Cream in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(FavFlaCVCVPContent.equals(ExpFavFlaCVCVPContent))
			{
				System.out.println("Our Coffee Collection FLAVORED Caramel Vanilla Cream Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection FLAVORED Caramel Vanilla Cream Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection FLAVORED Caramel Vanilla Cream Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}	
			
			if(FavFlaCVCVPFontSize.equals(ExpFavFlaCVCVPFontSize) && FavFlaCVCVPFontFamily.equals(ExpFavFlaCVCVPFontFamily) && FavFlaCVCVPContent.equals(ExpFavFlaCVCVPContent))
			{
				Label FavFlaCVCVPCSSP = new Label(5,27,"PASS"); 
				wsheet4.addCell(FavFlaCVCVPCSSP); 
			}
			else
			{
				Label FavFlaCVCVPCSSF = new Label(5,27,"FAIL"); 
				wsheet4.addCell(FavFlaCVCVPCSSF); 
			}
			
			
			// Variety Page - Our Coffee Collection - FAVORITES - FLAVORED Popular Varieties - Wild Mountain Blueberry�
			
			WebElement FavFlaWMBVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[4]/div/div/div/div/div[4]/div[3]/div[2]/div[4]/div[3]")); 
			
			String FavFlaWMBVPFontSize = FavFlaWMBVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection FLAVORED Wild Mountain Blueberry� in Variety Page is : "	+ FavFlaWMBVPFontSize);
			String FavFlaWMBVPFontFamily = FavFlaWMBVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection FLAVORED Wild Mountain Blueberry� in Variety Page is : "	+ FavFlaWMBVPFontFamily);
			String FavFlaWMBVPContent = FavFlaWMBVP.getText();
			System.out.println("Actual Content of Our Coffee Collection FLAVORED Wild Mountain Blueberry� in Variety Page is : "	+ FavFlaWMBVPContent);
					
			String ExpFavFlaWMBVPFontSize = sheet4.getCell(2,28).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection FLAVORED Wild Mountain Blueberry� in Variety Page is : "	+ ExpFavFlaWMBVPFontSize);
			String ExpFavFlaWMBVPFontFamily = sheet4.getCell(3,28).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection FLAVORED Wild Mountain Blueberry� in Variety Page is : "	+ ExpFavFlaWMBVPFontFamily);
			String ExpFavFlaWMBVPContent = sheet4.getCell(4,28).getContents();
			System.out.println("Expected Content of Our Coffee Collection FLAVORED Wild Mountain Blueberry� in Variety Page is : "	+ ExpFavFlaWMBVPContent);
			
			if(FavFlaWMBVPFontSize.equals(ExpFavFlaWMBVPFontSize))
			{
				System.out.println("Our Coffee Collection FLAVORED Wild Mountain Blueberry� in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection FLAVORED Wild Mountain Blueberry� in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection FLAVORED Wild Mountain Blueberry� in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(FavFlaWMBVPFontFamily.equals(ExpFavFlaWMBVPFontFamily))
			{
				System.out.println("Our Coffee Collection FLAVORED Wild Mountain Blueberry� in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection FLAVORED Wild Mountain Blueberry� in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection FLAVORED Wild Mountain Blueberry� in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(FavFlaWMBVPContent.equals(ExpFavFlaWMBVPContent))
			{
				System.out.println("Our Coffee Collection FLAVORED Wild Mountain Blueberry� Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection FLAVORED Wild Mountain Blueberry� Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection FLAVORED Wild Mountain Blueberry� Content in Variety Page is not in match with creative - Fail");  
			    bw.newLine();
			}	
			
			if(FavFlaWMBVPFontSize.equals(ExpFavFlaWMBVPFontSize) && FavFlaWMBVPFontFamily.equals(ExpFavFlaWMBVPFontFamily) && FavFlaWMBVPContent.equals(ExpFavFlaWMBVPContent))
			{
				Label FavFlaWMBVPCSSP = new Label(5,28,"PASS"); 
				wsheet4.addCell(FavFlaWMBVPCSSP); 
			}
			else
			{
				Label FavFlaWMBVPCSSF = new Label(5,28,"FAIL"); 
				wsheet4.addCell(FavFlaWMBVPCSSF); 
			}
			
			// Variety Page - Our Coffee Collection - FAVORITES - FLAVORED - Shop on Keurig.com 
			
			WebElement FlaShopVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[4]/div/div/div/div/div[4]/div[3]/div[2]/div[5]/a/div")); 
			String FlaShopVPColor = FlaShopVP.getCssValue("color");
			System.out.println("Actual Color of Shop on Keurig.com in FLAVORED in Variety Page is : "	+ FlaShopVPColor );
			// Convert RGB to Hex Value
			Color HFlaShopVPColor = Color.fromString(FlaShopVPColor);
			String HexFlaShopVPColor = HFlaShopVPColor.asHex();
			System.out.println("Actual Hex Color of Shop on Keurig.com in FLAVORED in Variety Page is : "	+ HexFlaShopVPColor );
			
			String FlaShopVPFontSize = FlaShopVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Shop on Keurig.com in FLAVORED in Variety Page is : "	+ FlaShopVPFontSize);
			String FlaShopVPFontFamily = FlaShopVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Shop on Keurig.com in FLAVORED in Variety Page is : "	+ FlaShopVPFontFamily);
			String FlaShopVPContent = FlaShopVP.getText();
			System.out.println("Actual Content of Shop on Keurig.com in FLAVORED in Variety Page is : "	+ FlaShopVPContent);
					
			String ExpFlaShopVPColor = sheet4.getCell(1,29).getContents(); //column,row
			System.out.println("Expected Color of Shop on Keurig.com in FLAVORED in Variety Page is : "	+ ExpFlaShopVPColor );
			String ExpFlaShopVPFontSize = sheet4.getCell(2,29).getContents();
			System.out.println("Expected Font Size of Shop on Keurig.com in FLAVORED in Variety Page is : "	+ ExpFlaShopVPFontSize);
			String ExpFlaShopVPFontFamily = sheet4.getCell(3,29).getContents();
			System.out.println("Expected Font Family of Shop on Keurig.com in FLAVORED in Variety Page is : "	+ ExpFlaShopVPFontFamily);
			String ExpFlaShopVPContent = sheet4.getCell(4,29).getContents();
			System.out.println("Expected Content of Shop on Keurig.com in FLAVORED in Variety Page is : "	+ ExpFlaShopVPContent);
					
			if(HexFlaShopVPColor.equals(ExpFlaShopVPColor))
			{
				System.out.println("Shop on Keurig.com in FLAVORED in Variety Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("Shop on Keurig.com in FLAVORED in Variety Page Color is not in match with creative - Fail");
				bw.write("Shop on Keurig.com in FLAVORED in Variety Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(FlaShopVPFontSize.equals(ExpFlaShopVPFontSize))
			{
				System.out.println("Shop on Keurig.com in FLAVORED in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Shop on Keurig.com in FLAVORED in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Shop on Keurig.com in FLAVORED in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(FlaShopVPFontFamily.equals(ExpFlaShopVPFontFamily))
			{
				System.out.println("Shop on Keurig.com in FLAVORED in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Shop on Keurig.com in FLAVORED in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Shop on Keurig.com in FLAVORED in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(FlaShopVPContent.equals(ExpFlaShopVPContent))
			{
				System.out.println("Shop on Keurig.com in FLAVORED Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Shop on Keurig.com in FLAVORED Content in Variety Page is not in match with creative - Fail");
				bw.write("Shop on Keurig.com in FLAVORED Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}
			
			if(HexFlaShopVPColor.equals(ExpFlaShopVPColor) && FlaShopVPFontSize.equals(ExpFlaShopVPFontSize) && FlaShopVPFontFamily.equals(ExpFlaShopVPFontFamily) && FlaShopVPContent.equals(ExpFlaShopVPContent))
			{
				Label FlaShopVPCSSP = new Label(5,29,"PASS"); 
				wsheet4.addCell(FlaShopVPCSSP); 
			}
			else
			{
				Label FlaShopVPCSSF = new Label(5,29,"FAIL"); 
				wsheet4.addCell(FlaShopVPCSSF); 
			}
			
			// Variety Page - Our Coffee Collection - FAVORITES - DECAF
			
			WebElement OurCoffeeFavDecVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[4]/div/div/div/div/div[4]/div[4]/div[2]/div[1]")); 
			
			String OurCoffeeFavDecVPFontSize = OurCoffeeFavDecVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection DECAF in Variety Page is : "	+ OurCoffeeFavDecVPFontSize);
			String OurCoffeeFavDecVPFontFamily = OurCoffeeFavDecVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection DECAF in Variety Page is : "	+ OurCoffeeFavDecVPFontFamily);
			String OurCoffeeFavDecVPContent = OurCoffeeFavDecVP.getText();
			System.out.println("Actual Content of Our Coffee Collection DECAF in Variety Page is : "	+ OurCoffeeFavDecVPContent);
					
			String ExpOurCoffeeFavDecVPFontSize = sheet4.getCell(2,30).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection DECAF in Variety Page is : "	+ ExpOurCoffeeFavDecVPFontSize);
			String ExpOurCoffeeFavDecVPFontFamily = sheet4.getCell(3,30).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection DECAF in Variety Page is : "	+ ExpOurCoffeeFavDecVPFontFamily);
			String ExpOurCoffeeFavDecVPContent = sheet4.getCell(4,30).getContents();
			System.out.println("Expected Content of Our Coffee Collection DECAF in Variety Page is : "	+ ExpOurCoffeeFavDecVPContent);
			
			if(OurCoffeeFavDecVPFontSize.equals(ExpOurCoffeeFavDecVPFontSize))
			{
				System.out.println("Our Coffee Collection DECAF in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection DECAF in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection DECAF in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(OurCoffeeFavDecVPFontFamily.equals(ExpOurCoffeeFavDecVPFontFamily))
			{
				System.out.println("Our Coffee Collection DECAF in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection DECAF in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection DECAF in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(OurCoffeeFavDecVPContent.equals(ExpOurCoffeeFavDecVPContent))
			{
				System.out.println("Our Coffee Collection DECAF Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection DECAF Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection DECAF Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}	
			
			if(OurCoffeeFavDecVPFontSize.equals(ExpOurCoffeeFavDecVPFontSize) && OurCoffeeFavDecVPFontFamily.equals(ExpOurCoffeeFavDecVPFontFamily) && OurCoffeeFavDecVPContent.equals(ExpOurCoffeeFavDecVPContent))
			{
				Label OurCoffeeFavDecVPCSSP = new Label(5,30,"PASS"); 
				wsheet4.addCell(OurCoffeeFavDecVPCSSP); 
			}
			else
			{
				Label OurCoffeeFavDecVPCSSF = new Label(5,30,"FAIL"); 
				wsheet4.addCell(OurCoffeeFavDecVPCSSF); 
			}
			
			// Variety Page - Our Coffee Collection - FAVORITES - DECAF Desc
			
			WebElement FavDecDescVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[4]/div/div/div/div/div[4]/div[4]/div[2]/div[2]")); 
			
			String FavDecDescVPFontSize = FavDecDescVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection DECAF Desc in Variety Page is : "	+ FavDecDescVPFontSize);
			String FavDecDescVPFontFamily = FavDecDescVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection DECAF Desc in Variety Page is : "	+ FavDecDescVPFontFamily);
			String FavDecDescVPContent = FavDecDescVP.getText();
			System.out.println("Actual Content of Our Coffee Collection DECAF Desc in Variety Page is : "	+ FavDecDescVPContent);
					
			String ExpFavDecDescVPFontSize = sheet4.getCell(2,31).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection DECAF Desc in Variety Page is : "	+ ExpFavDecDescVPFontSize);
			String ExpFavDecDescVPFontFamily = sheet4.getCell(3,31).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection DECAF Desc in Variety Page is : "	+ ExpFavDecDescVPFontFamily);
			String ExpFavDecDescVPContent = sheet4.getCell(4,31).getContents();
			System.out.println("Expected Content of Our Coffee Collection DECAF Desc in Variety Page is : "	+ ExpFavDecDescVPContent);
			
			if(FavDecDescVPFontSize.equals(ExpFavDecDescVPFontSize))
			{
				System.out.println("Our Coffee Collection DECAF Desc in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection DECAF Desc in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection DECAF Desc in Variety Page Font Size is not in match with creative - Fail");  
			    bw.newLine();
			}
					
			if(FavDecDescVPFontFamily.equals(ExpFavDecDescVPFontFamily))
			{
				System.out.println("Our Coffee Collection DECAF Desc in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection DECAF Desc in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection DECAF Desc in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(FavDecDescVPContent.equals(ExpFavDecDescVPContent))
			{
				System.out.println("Our Coffee Collection DECAF Desc Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection DECAF Desc Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection DECAF Desc Content in Variety Page is not in match with creative - Fail");  
			    bw.newLine();
			}	
			
			if(FavDecDescVPFontSize.equals(ExpFavDecDescVPFontSize) && FavDecDescVPFontFamily.equals(ExpFavDecDescVPFontFamily) && FavDecDescVPContent.equals(ExpFavDecDescVPContent))
			{
				Label FavDecDescVPCSSP = new Label(5,31,"PASS"); 
				wsheet4.addCell(FavDecDescVPCSSP); 
			}
			else
			{
				Label FavDecDescVPCSSF = new Label(5,31,"FAIL"); 
				wsheet4.addCell(FavDecDescVPCSSF); 
			}
			
			// Variety Page - Our Coffee Collection - FAVORITES - DECAF Popular Varieties
			
			WebElement FavDecPVVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[4]/div/div/div/div/div[4]/div[4]/div[2]/div[3]")); 
			
			String FavDecPVVPFontSize = FavDecPVVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection DECAF Popular Varieties in Variety Page is : "	+ FavDecPVVPFontSize);
			String FavDecPVVPFontFamily = FavDecPVVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection DECAF Popular Varieties in Variety Page is : "	+ FavDecPVVPFontFamily);
			String FavDecPVVPContent = FavDecPVVP.getText();
			System.out.println("Actual Content of Our Coffee Collection DECAF Popular Varieties in Variety Page is : "	+ FavDecPVVPContent);
					
			String ExpFavDecPVVPFontSize = sheet4.getCell(2,32).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection DECAF Popular Varieties in Variety Page is : "	+ ExpFavDecPVVPFontSize);
			String ExpFavDecPVVPFontFamily = sheet4.getCell(3,32).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection DECAF Popular Varieties in Variety Page is : "	+ ExpFavDecPVVPFontFamily);
			String ExpFavDecPVVPContent = sheet4.getCell(4,32).getContents();
			System.out.println("Expected Content of Our Coffee Collection DECAF Popular Varieties in Variety Page is : "	+ ExpFavDecPVVPContent);
			
			if(FavDecPVVPFontSize.equals(ExpFavDecPVVPFontSize))
			{
				System.out.println("Our Coffee Collection DECAF Popular Varieties in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection DECAF Popular Varieties in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection DECAF Popular Varieties in Variety Page Font Size is not in match with creative - Fail");  
			    bw.newLine();
			}
					
			if(FavDecPVVPFontFamily.equals(ExpFavDecPVVPFontFamily))
			{
				System.out.println("Our Coffee Collection DECAF Popular Varieties in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection DECAF Popular Varieties in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection DECAF Popular Varieties in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(FavDecPVVPContent.equals(ExpFavDecPVVPContent))
			{
				System.out.println("Our Coffee Collection DECAF Popular Varieties Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection DECAF Popular Varieties Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection DECAF Popular Varieties Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}	
			
			if(FavDecPVVPFontSize.equals(ExpFavDecPVVPFontSize) && FavDecPVVPFontFamily.equals(ExpFavDecPVVPFontFamily) && FavDecPVVPContent.equals(ExpFavDecPVVPContent))
			{
				Label FavDecPVVPCSSP = new Label(5,32,"PASS"); 
				wsheet4.addCell(FavDecPVVPCSSP); 
			}
			else
			{
				Label FavDecPVVPCSSF = new Label(5,32,"FAIL"); 
				wsheet4.addCell(FavDecPVVPCSSF); 
			}
			
			// Variety Page - Our Coffee Collection - FAVORITES - DECAF Popular Varieties - Colombian Fair Trade Select Decaf
			
			WebElement FavDecCFTVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[4]/div/div/div/div/div[4]/div[4]/div[2]/div[4]/div[1]")); 
			
			String FavDecCFTVPFontSize = FavDecCFTVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection DECAF Colombian Fair Trade Select Decaf in Variety Page is : "	+ FavDecCFTVPFontSize);
			String FavDecCFTVPFontFamily = FavDecCFTVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection DECAF Colombian Fair Trade Select Decaf in Variety Page is : "	+ FavDecCFTVPFontFamily);
			String FavDecCFTVPContent = FavDecCFTVP.getText();
			System.out.println("Actual Content of Our Coffee Collection DECAF Colombian Fair Trade Select Decaf in Variety Page is : "	+ FavDecCFTVPContent);
					
			String ExpFavDecCFTVPFontSize = sheet4.getCell(2,33).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection DECAF Colombian Fair Trade Select Decaf in Variety Page is : "	+ ExpFavDecCFTVPFontSize);
			String ExpFavDecCFTVPFontFamily = sheet4.getCell(3,33).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection DECAF Colombian Fair Trade Select Decaf in Variety Page is : "	+ ExpFavDecCFTVPFontFamily);
			String ExpFavDecCFTVPContent = sheet4.getCell(4,33).getContents();
			System.out.println("Expected Content of Our Coffee Collection DECAF Colombian Fair Trade Select Decaf in Variety Page is : "	+ ExpFavDecCFTVPContent);
			
			if(FavDecCFTVPFontSize.equals(ExpFavDecCFTVPFontSize))
			{
				System.out.println("Our Coffee Collection DECAF Colombian Fair Trade Select Decaf in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection DECAF Colombian Fair Trade Select Decaf in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection DECAF Colombian Fair Trade Select Decaf in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(FavDecCFTVPFontFamily.equals(ExpFavDecCFTVPFontFamily))
			{
				System.out.println("Our Coffee Collection DECAF Colombian Fair Trade Select Decaf in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection DECAF Colombian Fair Trade Select Decaf in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection DECAF Colombian Fair Trade Select Decaf in Variety Page Font Family is not in match with creative - Fail");  
			    bw.newLine();
			}
			

			if(FavDecCFTVPContent.equals(ExpFavDecCFTVPContent))
			{
				System.out.println("Our Coffee Collection DECAF Colombian Fair Trade Select Decaf Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection DECAF Colombian Fair Trade Select Decaf Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection DECAF Colombian Fair Trade Select Decaf Content in Variety Page is not in match with creative - Fail");  
			    bw.newLine();
			}	
			
			if(FavDecCFTVPFontSize.equals(ExpFavDecCFTVPFontSize) && FavDecCFTVPFontFamily.equals(ExpFavDecCFTVPFontFamily) && FavDecCFTVPContent.equals(ExpFavDecCFTVPContent))
			{
				Label FavDecCFTVPCSSP = new Label(5,33,"PASS"); 
				wsheet4.addCell(FavDecCFTVPCSSP); 
			}
			else
			{
				Label FavDecCFTVPCSSF = new Label(5,33,"FAIL"); 
				wsheet4.addCell(FavDecCFTVPCSSF); 
			}
			
			// Variety Page - Our Coffee Collection - FAVORITES - DECAF Popular Varieties - Vermont Country Blend� Decaf
			
			WebElement FavDecVCBVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[4]/div/div/div/div/div[4]/div[4]/div[2]/div[4]/div[2]")); 
			
			String FavDecVCBVPFontSize = FavDecVCBVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection DECAF Vermont Country Blend� Decaf in Variety Page is : "	+ FavDecVCBVPFontSize);
			String FavDecVCBVPFontFamily = FavDecVCBVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection DECAF Vermont Country Blend� Decaf in Variety Page is : "	+ FavDecVCBVPFontFamily);
			String FavDecVCBVPContent = FavDecVCBVP.getText();
			System.out.println("Actual Content of Our Coffee Collection DECAF Vermont Country Blend� Decaf in Variety Page is : "	+ FavDecVCBVPContent);
					
			String ExpFavDecVCBVPFontSize = sheet4.getCell(2,34).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection DECAF Vermont Country Blend� Decaf in Variety Page is : "	+ ExpFavDecVCBVPFontSize);
			String ExpFavDecVCBVPFontFamily = sheet4.getCell(3,34).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection DECAF Vermont Country Blend� Decaf in Variety Page is : "	+ ExpFavDecVCBVPFontFamily);
			String ExpFavDecVCBVPContent = sheet4.getCell(4,34).getContents();
			System.out.println("Expected Content of Our Coffee Collection DECAF Vermont Country Blend� Decaf in Variety Page is : "	+ ExpFavDecVCBVPContent);
			
			if(FavDecVCBVPFontSize.equals(ExpFavDecVCBVPFontSize))
			{
				System.out.println("Our Coffee Collection DECAF Vermont Country Blend� Decaf in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection DECAF Vermont Country Blend� Decaf in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection DECAF Vermont Country Blend� Decaf in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(FavDecVCBVPFontFamily.equals(ExpFavDecVCBVPFontFamily))
			{
				System.out.println("Our Coffee Collection DECAF Vermont Country Blend� Decaf in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection DECAF Vermont Country Blend� Decaf in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection DECAF Vermont Country Blend� Decaf in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(FavDecVCBVPContent.equals(ExpFavDecVCBVPContent))
			{
				System.out.println("Our Coffee Collection DECAF Vermont Country Blend� Decaf Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection DECAF Vermont Country Blend� Decaf Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection DECAF Vermont Country Blend� Decaf Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}	
			
			if(FavDecVCBVPFontSize.equals(ExpFavDecVCBVPFontSize) && FavDecVCBVPFontFamily.equals(ExpFavDecVCBVPFontFamily) && FavDecVCBVPContent.equals(ExpFavDecVCBVPContent))
			{
				Label FavDecVCBVPCSSP = new Label(5,34,"PASS"); 
				wsheet4.addCell(FavDecVCBVPCSSP); 
			}
			else
			{
				Label FavDecVCBVPCSSF = new Label(5,34,"FAIL"); 
				wsheet4.addCell(FavDecVCBVPCSSF); 
			}
			
			// Variety Page - Our Coffee Collection - FAVORITES - DECAF Popular Varieties - Half-Caff
			
			WebElement FavDecHCVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[4]/div/div/div/div/div[4]/div[4]/div[2]/div[4]/div[3]")); 
			
			String FavDecHCVPFontSize = FavDecHCVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection DECAF Half-Caff in Variety Page is : "	+ FavDecHCVPFontSize);
			String FavDecHCVPFontFamily = FavDecHCVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection DECAF Half-Caff in Variety Page is : "	+ FavDecHCVPFontFamily);
			String FavDecHCVPContent = FavDecHCVP.getText();
			System.out.println("Actual Content of Our Coffee Collection DECAF Half-Caff in Variety Page is : "	+ FavDecHCVPContent);
					
			String ExpFavDecHCVPFontSize = sheet4.getCell(2,35).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection DECAF Half-Caff in Variety Page is : "	+ ExpFavDecHCVPFontSize);
			String ExpFavDecHCVPFontFamily = sheet4.getCell(3,35).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection DECAF Half-Caff in Variety Page is : "	+ ExpFavDecHCVPFontFamily);
			String ExpFavDecHCVPContent = sheet4.getCell(4,35).getContents();
			System.out.println("Expected Content of Our Coffee Collection DECAF Half-Caff in Variety Page is : "	+ ExpFavDecHCVPContent);
			
			if(FavDecHCVPFontSize.equals(ExpFavDecHCVPFontSize))
			{
				System.out.println("Our Coffee Collection DECAF Half-Caff in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection DECAF Half-Caff in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection DECAF Half-Caff in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(FavDecHCVPFontFamily.equals(ExpFavDecHCVPFontFamily))
			{
				System.out.println("Our Coffee Collection DECAF Half-Caff in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection DECAF Half-Caff in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection DECAF Half-Caff in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(FavDecHCVPContent.equals(ExpFavDecHCVPContent))
			{
				System.out.println("Our Coffee Collection DECAF Half-Caff Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection DECAF Half-Caff Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection DECAF Half-Caff Content in Variety Page is not in match with creative - Fail");  
			    bw.newLine();
			}	
			
			if(FavDecHCVPFontSize.equals(ExpFavDecHCVPFontSize) && FavDecHCVPFontFamily.equals(ExpFavDecHCVPFontFamily) && FavDecHCVPContent.equals(ExpFavDecHCVPContent))
			{
				Label FavDecHCVPCSSP = new Label(5,35,"PASS"); 
				wsheet4.addCell(FavDecHCVPCSSP); 
			}
			else
			{
				Label FavDecHCVPCSSF = new Label(5,35,"FAIL"); 
				wsheet4.addCell(FavDecHCVPCSSF); 
			}
			
			// Variety Page - Our Coffee Collection - FAVORITES - DECAF - Shop on Keurig.com 
			
			WebElement DecShopVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[4]/div/div/div/div/div[4]/div[4]/div[2]/div[5]/a/div")); 
			String DecShopVPColor = DecShopVP.getCssValue("color");
			System.out.println("Actual Color of Shop on Keurig.com in DECAF in Variety Page is : "	+ DecShopVPColor );
			// Convert RGB to Hex Value
			Color HDecShopVPColor = Color.fromString(DecShopVPColor);
			String HexDecShopVPColor = HDecShopVPColor.asHex();
			System.out.println("Actual Hex Color of Shop on Keurig.com in DECAF in Variety Page is : "	+ HexDecShopVPColor );
			
			String DecShopVPFontSize = DecShopVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Shop on Keurig.com in DECAF in Variety Page is : "	+ DecShopVPFontSize);
			String DecShopVPFontFamily = DecShopVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Shop on Keurig.com in DECAF in Variety Page is : "	+ DecShopVPFontFamily);
			String DecShopVPContent = DecShopVP.getText();
			System.out.println("Actual Content of Shop on Keurig.com in DECAF in Variety Page is : "	+ DecShopVPContent);
					
			String ExpDecShopVPColor = sheet4.getCell(1,36).getContents(); //column,row
			System.out.println("Expected Color of Shop on Keurig.com in DECAF in Variety Page is : "	+ ExpDecShopVPColor );
			String ExpDecShopVPFontSize = sheet4.getCell(2,36).getContents();
			System.out.println("Expected Font Size of Shop on Keurig.com in DECAF in Variety Page is : "	+ ExpDecShopVPFontSize);
			String ExpDecShopVPFontFamily = sheet4.getCell(3,36).getContents();
			System.out.println("Expected Font Family of Shop on Keurig.com in DECAF in Variety Page is : "	+ ExpDecShopVPFontFamily);
			String ExpDecShopVPContent = sheet4.getCell(4,36).getContents();
			System.out.println("Expected Content of Shop on Keurig.com in DECAF in Variety Page is : "	+ ExpDecShopVPContent);
					
			if(HexDecShopVPColor.equals(ExpDecShopVPColor))
			{
				System.out.println("Shop on Keurig.com in DECAF in Variety Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("Shop on Keurig.com in DECAF in Variety Page Color is not in match with creative - Fail");
				bw.write("Shop on Keurig.com in DECAF in Variety Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(DecShopVPFontSize.equals(ExpDecShopVPFontSize))
			{
				System.out.println("Shop on Keurig.com in DECAF in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Shop on Keurig.com in DECAF in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Shop on Keurig.com in DECAF in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(DecShopVPFontFamily.equals(ExpDecShopVPFontFamily))
			{
				System.out.println("Shop on Keurig.com in DECAF in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Shop on Keurig.com in DECAF in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Shop on Keurig.com in DECAF in Variety Page Font Family is not in match with creative - Fail");  
			    bw.newLine();
			}
			

			if(DecShopVPContent.equals(ExpDecShopVPContent))
			{
				System.out.println("Shop on Keurig.com in DECAF Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Shop on Keurig.com in DECAF Content in Variety Page is not in match with creative - Fail");
				bw.write("Shop on Keurig.com in DECAF Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}	
			
			if(HexDecShopVPColor.equals(ExpDecShopVPColor) && DecShopVPFontSize.equals(ExpDecShopVPFontSize) && DecShopVPFontFamily.equals(ExpDecShopVPFontFamily) && DecShopVPContent.equals(ExpDecShopVPContent))
			{
				Label DecShopVPCSSP = new Label(5,36,"PASS"); 
				wsheet4.addCell(DecShopVPCSSP); 
			}
			else
			{
				Label DecShopVPCSSF = new Label(5,36,"FAIL"); 
				wsheet4.addCell(DecShopVPCSSF); 
			}
			
			// Variety Page - Our Coffee Collection - COLLECTIONS
			
			WebElement OurCoffeeColVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[5]/div/div/div/div[2]/div[3]")); 
			
			String OurCoffeeColVPFontSize = OurCoffeeColVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection COLLECTIONS in Variety Page is : "	+ OurCoffeeColVPFontSize);
			String OurCoffeeColVPFontFamily = OurCoffeeColVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection COLLECTIONS in Variety Page is : "	+ OurCoffeeColVPFontFamily);
			String OurCoffeeColVPContent = OurCoffeeColVP.getText();
			System.out.println("Actual Content of Our Coffee Collection COLLECTIONS in Variety Page is : "	+ OurCoffeeColVPContent);
					
			String ExpOurCoffeeColVPFontSize = sheet4.getCell(2,37).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection COLLECTIONS in Variety Page is : "	+ ExpOurCoffeeColVPFontSize);
			String ExpOurCoffeeColVPFontFamily = sheet4.getCell(3,37).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection COLLECTIONS in Variety Page is : "	+ ExpOurCoffeeColVPFontFamily);
			String ExpOurCoffeeColVPContent = sheet4.getCell(4,37).getContents();
			System.out.println("Expected Content of Our Coffee Collection COLLECTIONS in Variety Page is : "	+ ExpOurCoffeeColVPContent);
			
			if(OurCoffeeColVPFontSize.equals(ExpOurCoffeeColVPFontSize))
			{
				System.out.println("Our Coffee Collection COLLECTIONS in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection COLLECTIONS in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection COLLECTIONS in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(OurCoffeeColVPFontFamily.equals(ExpOurCoffeeColVPFontFamily))
			{
				System.out.println("Our Coffee Collection COLLECTIONS in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection COLLECTIONS in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection COLLECTIONS in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(OurCoffeeColVPContent.equals(ExpOurCoffeeColVPContent))
			{
				System.out.println("Our Coffee Collection COLLECTIONS Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection COLLECTIONS Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection COLLECTIONS Content in Variety Page is not in match with creative - Fail");  
			    bw.newLine();
			}
			
			if(OurCoffeeColVPFontSize.equals(ExpOurCoffeeColVPFontSize) && OurCoffeeColVPFontFamily.equals(ExpOurCoffeeColVPFontFamily) && OurCoffeeColVPContent.equals(ExpOurCoffeeColVPContent))
			{
				Label OurCoffeeColVPCSSP = new Label(5,37,"PASS"); 
				wsheet4.addCell(OurCoffeeColVPCSSP); 
			}
			else
			{
				Label OurCoffeeColVPCSSF = new Label(5,37,"FAIL"); 
				wsheet4.addCell(OurCoffeeColVPCSSF); 
			}
			
			Thread.sleep(3000);
			JavascriptExecutor jseseacss = (JavascriptExecutor)driver;
			jseseacss.executeScript("window.scrollBy(0,500)", "");
			Thread.sleep(8000);
		
			// Variety Page - Our Coffee Collection - COLLECTIONS - Seasonal
			
			WebElement OurCoffeeColSeaVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[5]/div/div/div/div[2]/div[4]/div[2]/div[2]/div[1]")); 
			
			String OurCoffeeColSeaVPFontSize = OurCoffeeColSeaVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection Seasonal in Variety Page is : "	+ OurCoffeeColSeaVPFontSize);
			String OurCoffeeColSeaVPFontFamily = OurCoffeeColSeaVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection Seasonal in Variety Page is : "	+ OurCoffeeColSeaVPFontFamily);
			String OurCoffeeColSeaVPContent = OurCoffeeColSeaVP.getText();
			System.out.println("Actual Content of Our Coffee Collection Seasonal in Variety Page is : "	+ OurCoffeeColSeaVPContent);
					
			String ExpOurCoffeeColSeaVPFontSize = sheet4.getCell(2,38).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection Seasonal in Variety Page is : "	+ ExpOurCoffeeColSeaVPFontSize);
			String ExpOurCoffeeColSeaVPFontFamily = sheet4.getCell(3,38).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection Seasonal in Variety Page is : "	+ ExpOurCoffeeColSeaVPFontFamily);
			String ExpOurCoffeeColSeaVPContent = sheet4.getCell(4,38).getContents();
			System.out.println("Expected Content of Our Coffee Collection Seasonal in Variety Page is : "	+ ExpOurCoffeeColSeaVPContent);
			
			if(OurCoffeeColSeaVPFontSize.equals(ExpOurCoffeeColSeaVPFontSize))
			{
				System.out.println("Our Coffee Collection Seasonal in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Seasonal in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection Seasonal in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(OurCoffeeColSeaVPFontFamily.equals(ExpOurCoffeeColSeaVPFontFamily))
			{
				System.out.println("Our Coffee Collection Seasonal in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Seasonal in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection Seasonal in Variety Page Font Family is not in match with creative - Fail");  
			    bw.newLine();
			}
			

			if(OurCoffeeColSeaVPContent.equals(ExpOurCoffeeColSeaVPContent))
			{
				System.out.println("Our Coffee Collection Seasonal Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Seasonal Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection Seasonal Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}	
			
			if(OurCoffeeColSeaVPFontSize.equals(ExpOurCoffeeColSeaVPFontSize) && OurCoffeeColSeaVPFontFamily.equals(ExpOurCoffeeColSeaVPFontFamily) && OurCoffeeColSeaVPContent.equals(ExpOurCoffeeColSeaVPContent))
			{
				Label OurCoffeeColSeaVPCSSP = new Label(5,38,"PASS"); 
				wsheet4.addCell(OurCoffeeColSeaVPCSSP); 
			}
			else
			{
				Label OurCoffeeColSeaVPCSSF = new Label(5,38,"FAIL"); 
				wsheet4.addCell(OurCoffeeColSeaVPCSSF); 
			}
			
			// Variety Page - Our Coffee Collection - COLLECTIONS - Seasonal Desc
			
			WebElement ColSeaDescVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[5]/div/div/div/div[2]/div[4]/div[2]/div[2]/div[2]")); 
			
			String ColSeaDescVPFontSize = ColSeaDescVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection Seasonal Desc in Variety Page is : "	+ ColSeaDescVPFontSize);
			String ColSeaDescVPFontFamily = ColSeaDescVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection Seasonal Desc in Variety Page is : "	+ ColSeaDescVPFontFamily);
			String ColSeaDescVPContent = ColSeaDescVP.getText();
			System.out.println("Actual Content of Our Coffee Collection Seasonal Desc in Variety Page is : "	+ ColSeaDescVPContent);
					
			String ExpColSeaDescVPFontSize = sheet4.getCell(2,39).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection Seasonal Desc in Variety Page is : "	+ ExpColSeaDescVPFontSize);
			String ExpColSeaDescVPFontFamily = sheet4.getCell(3,39).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection Seasonal Desc in Variety Page is : "	+ ExpColSeaDescVPFontFamily);
			String ExpColSeaDescVPContent = sheet4.getCell(4,39).getContents();
			System.out.println("Expected Content of Our Coffee Collection Seasonal Desc in Variety Page is : "	+ ExpColSeaDescVPContent);
			
			if(ColSeaDescVPFontSize.equals(ExpColSeaDescVPFontSize))
			{
				System.out.println("Our Coffee Collection Seasonal Desc in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Seasonal Desc in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection Seasonal Desc in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(ColSeaDescVPFontFamily.equals(ExpColSeaDescVPFontFamily))
			{
				System.out.println("Our Coffee Collection Seasonal Desc in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Seasonal Desc in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection Seasonal Desc in Variety Page Font Family is not in match with creative - Fail");  
			    bw.newLine();
			}
			

			if(ColSeaDescVPContent.equals(ExpColSeaDescVPContent))
			{
				System.out.println("Our Coffee Collection Seasonal Desc Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Seasonal Desc Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection Seasonal Desc Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}	
			
			if(ColSeaDescVPFontSize.equals(ExpColSeaDescVPFontSize) && ColSeaDescVPFontFamily.equals(ExpColSeaDescVPFontFamily) && ColSeaDescVPContent.equals(ExpColSeaDescVPContent))
			{
				Label ColSeaDescVPCSSP = new Label(5,39,"PASS"); 
				wsheet4.addCell(ColSeaDescVPCSSP); 
			}
			else
			{
				Label ColSeaDescVPCSSF = new Label(5,39,"FAIL"); 
				wsheet4.addCell(ColSeaDescVPCSSF); 
			}
			
			// Variety Page - Our Coffee Collection - COLLECTIONS - Seasonal - Popular Flavor Varieties
			
			WebElement ColSeaPFVVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[5]/div/div/div/div[2]/div[4]/div[2]/div[2]/div[3]")); 
			
			String ColSeaPFVVPFontSize = ColSeaPFVVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection Seasonal - Popular Flavor Varieties in Variety Page is : "	+ ColSeaPFVVPFontSize);
			String ColSeaPFVVPFontFamily = ColSeaPFVVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection Seasonal - Popular Flavor Varieties in Variety Page is : "	+ ColSeaPFVVPFontFamily);
			String ColSeaPFVVPContent = ColSeaPFVVP.getText();
			System.out.println("Actual Content of Our Coffee Collection Seasonal - Popular Flavor Varieties in Variety Page is : "	+ ColSeaPFVVPContent);
					
			String ExpColSeaPFVVPFontSize = sheet4.getCell(2,40).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection Seasonal - Popular Flavor Varieties in Variety Page is : "	+ ExpColSeaPFVVPFontSize);
			String ExpColSeaPFVVPFontFamily = sheet4.getCell(3,40).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection Seasonal - Popular Flavor Varieties in Variety Page is : "	+ ExpColSeaPFVVPFontFamily);
			String ExpColSeaPFVVPContent = sheet4.getCell(4,40).getContents();
			System.out.println("Expected Content of Our Coffee Collection Seasonal - Popular Flavor Varieties in Variety Page is : "	+ ExpColSeaPFVVPContent);
			
			if(ColSeaPFVVPFontSize.equals(ExpColSeaPFVVPFontSize))
			{
				System.out.println("Our Coffee Collection Seasonal - Popular Flavor Varieties in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Seasonal - Popular Flavor Varieties in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection Seasonal - Popular Flavor Varieties in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(ColSeaPFVVPFontFamily.equals(ExpColSeaPFVVPFontFamily))
			{
				System.out.println("Our Coffee Collection Seasonal - Popular Flavor Varieties in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Seasonal - Popular Flavor Varieties in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection Seasonal - Popular Flavor Varieties in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(ColSeaPFVVPContent.equals(ExpColSeaPFVVPContent))
			{
				System.out.println("Our Coffee Collection Seasonal - Popular Flavor Varieties Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Seasonal - Popular Flavor Varieties Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection Seasonal - Popular Flavor Varieties Content in Variety Page is not in match with creative - Fail");  
			    bw.newLine();
			}	
			
			if(ColSeaPFVVPFontSize.equals(ExpColSeaPFVVPFontSize) && ColSeaPFVVPFontFamily.equals(ExpColSeaPFVVPFontFamily) && ColSeaPFVVPContent.equals(ExpColSeaPFVVPContent))
			{
				Label ColSeaPFVVPCSSP = new Label(5,40,"PASS"); 
				wsheet4.addCell(ColSeaPFVVPCSSP); 
			}
			else
			{
				Label ColSeaPFVVPCSSF = new Label(5,40,"FAIL"); 
				wsheet4.addCell(ColSeaPFVVPCSSF); 
			}
			
			// Variety Page - Our Coffee Collection - COLLECTIONS - Seasonal - Popular Flavor Varieties - Pumpkin Spice
			
			WebElement ColSeaPSVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[5]/div/div/div/div[2]/div[4]/div[2]/div[2]/div[4]/div[1]")); 
			
			String ColSeaPSVPFontSize = ColSeaPSVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection Seasonal Pumpkin Spice in Variety Page is : "	+ ColSeaPSVPFontSize);
			String ColSeaPSVPFontFamily = ColSeaPSVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection Seasonal Pumpkin Spice in Variety Page is : "	+ ColSeaPSVPFontFamily);
			String ColSeaPSVPContent = ColSeaPSVP.getText();
			System.out.println("Actual Content of Our Coffee Collection Seasonal Pumpkin Spice in Variety Page is : "	+ ColSeaPSVPContent);
					
			String ExpColSeaPSVPFontSize = sheet4.getCell(2,41).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection Seasonal Pumpkin Spice in Variety Page is : "	+ ExpColSeaPSVPFontSize);
			String ExpColSeaPSVPFontFamily = sheet4.getCell(3,41).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection Seasonal Pumpkin Spice in Variety Page is : "	+ ExpColSeaPSVPFontFamily);
			String ExpColSeaPSVPContent = sheet4.getCell(4,41).getContents();
			System.out.println("Expected Content of Our Coffee Collection Seasonal Pumpkin Spice in Variety Page is : "	+ ExpColSeaPSVPContent);
			
			if(ColSeaPSVPFontSize.equals(ExpColSeaPSVPFontSize))
			{
				System.out.println("Our Coffee Collection Seasonal Pumpkin Spice in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Seasonal Pumpkin Spice in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection Seasonal Pumpkin Spice in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(ColSeaPSVPFontFamily.equals(ExpColSeaPSVPFontFamily))
			{
				System.out.println("Our Coffee Collection Seasonal Pumpkin Spice in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Seasonal Pumpkin Spice in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection Seasonal Pumpkin Spice in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(ColSeaPSVPContent.equals(ExpColSeaPSVPContent))
			{
				System.out.println("Our Coffee Collection Seasonal Pumpkin Spice Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Seasonal Pumpkin Spice Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection Seasonal Pumpkin Spice Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}	
			
			if(ColSeaPSVPFontSize.equals(ExpColSeaPSVPFontSize) && ColSeaPSVPFontFamily.equals(ExpColSeaPSVPFontFamily) && ColSeaPSVPContent.equals(ExpColSeaPSVPContent))
			{
				Label ColSeaPSVPCSSP = new Label(5,41,"PASS"); 
				wsheet4.addCell(ColSeaPSVPCSSP); 
			}
			else
			{
				Label ColSeaPSVPCSSF = new Label(5,41,"FAIL"); 
				wsheet4.addCell(ColSeaPSVPCSSF); 
			}
			
			// Variety Page - Our Coffee Collection - COLLECTIONS - Seasonal - Popular Flavor Varieties - Island Coconut�
			
			WebElement ColSeaICVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[5]/div/div/div/div[2]/div[4]/div[2]/div[2]/div[4]/div[2]")); 
			
			String ColSeaICVPFontSize = ColSeaICVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection Seasonal Island Coconut� in Variety Page is : "	+ ColSeaICVPFontSize);
			String ColSeaICVPFontFamily = ColSeaICVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection Seasonal Island Coconut� in Variety Page is : "	+ ColSeaICVPFontFamily);
			String ColSeaICVPContent = ColSeaICVP.getText();
			System.out.println("Actual Content of Our Coffee Collection Seasonal Island Coconut� in Variety Page is : "	+ ColSeaICVPContent);
					
			String ExpColSeaICVPFontSize = sheet4.getCell(2,42).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection Seasonal Island Coconut� in Variety Page is : "	+ ExpColSeaICVPFontSize);
			String ExpColSeaICVPFontFamily = sheet4.getCell(3,42).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection Seasonal Island Coconut� in Variety Page is : "	+ ExpColSeaICVPFontFamily);
			String ExpColSeaICVPContent = sheet4.getCell(4,42).getContents();
			System.out.println("Expected Content of Our Coffee Collection Seasonal Island Coconut� in Variety Page is : "	+ ExpColSeaICVPContent);
			
			if(ColSeaICVPFontSize.equals(ExpColSeaICVPFontSize))
			{
				System.out.println("Our Coffee Collection Seasonal Island Coconut� in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Seasonal Island Coconut� in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection Seasonal Island Coconut� in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(ColSeaICVPFontFamily.equals(ExpColSeaICVPFontFamily))
			{
				System.out.println("Our Coffee Collection Seasonal Island Coconut� in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Seasonal Island Coconut� in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection Seasonal Island Coconut� in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(ColSeaICVPContent.equals(ExpColSeaICVPContent))
			{
				System.out.println("Our Coffee Collection Seasonal Island Coconut� Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Seasonal Island Coconut� Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection Seasonal Island Coconut� Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}
			
			if(ColSeaICVPFontSize.equals(ExpColSeaICVPFontSize) && ColSeaICVPFontFamily.equals(ExpColSeaICVPFontFamily) && ColSeaICVPContent.equals(ExpColSeaICVPContent))
			{
				Label ColSeaICVPCSSP = new Label(5,42,"PASS"); 
				wsheet4.addCell(ColSeaICVPCSSP); 
			}
			else
			{
				Label ColSeaICVPCSSF = new Label(5,42,"FAIL"); 
				wsheet4.addCell(ColSeaICVPCSSF); 
			}
			
			
			// Variety Page - Our Coffee Collection - COLLECTIONS - Seasonal - Popular Flavor Varieties - Cinnamon Sugar Cookie
			
			WebElement ColSeaCSCVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[5]/div/div/div/div[2]/div[4]/div[2]/div[2]/div[4]/div[3]")); 
			
			String ColSeaCSCVPFontSize = ColSeaCSCVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection Seasonal Cinnamon Sugar Cookie in Variety Page is : "	+ ColSeaCSCVPFontSize);
			String ColSeaCSCVPFontFamily = ColSeaCSCVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection Seasonal Cinnamon Sugar Cookie in Variety Page is : "	+ ColSeaCSCVPFontFamily);
			String ColSeaCSCVPContent = ColSeaCSCVP.getText();
			System.out.println("Actual Content of Our Coffee Collection Seasonal Cinnamon Sugar Cookie in Variety Page is : "	+ ColSeaCSCVPContent);
					
			String ExpColSeaCSCVPFontSize = sheet4.getCell(2,43).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection Seasonal Cinnamon Sugar Cookie in Variety Page is : "	+ ExpColSeaCSCVPFontSize);
			String ExpColSeaCSCVPFontFamily = sheet4.getCell(3,43).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection Seasonal Cinnamon Sugar Cookie in Variety Page is : "	+ ExpColSeaCSCVPFontFamily);
			String ExpColSeaCSCVPContent = sheet4.getCell(4,43).getContents();
			System.out.println("Expected Content of Our Coffee Collection Seasonal Cinnamon Sugar Cookie in Variety Page is : "	+ ExpColSeaCSCVPContent);
			
			if(ColSeaCSCVPFontSize.equals(ExpColSeaCSCVPFontSize))
			{
				System.out.println("Our Coffee Collection Seasonal Cinnamon Sugar Cookie in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Seasonal Cinnamon Sugar Cookie in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection Seasonal Cinnamon Sugar Cookie in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(ColSeaCSCVPFontFamily.equals(ExpColSeaCSCVPFontFamily))
			{
				System.out.println("Our Coffee Collection Seasonal Cinnamon Sugar Cookie in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Seasonal Cinnamon Sugar Cookie in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection Seasonal Cinnamon Sugar Cookie in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(ColSeaCSCVPContent.equals(ExpColSeaCSCVPContent))
			{
				System.out.println("Our Coffee Collection Seasonal Cinnamon Sugar Cookie Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Seasonal Cinnamon Sugar Cookie Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection Seasonal Cinnamon Sugar Cookie Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}	
			
			if(ColSeaCSCVPFontSize.equals(ExpColSeaCSCVPFontSize) && ColSeaCSCVPFontFamily.equals(ExpColSeaCSCVPFontFamily) && ColSeaCSCVPContent.equals(ExpColSeaCSCVPContent))
			{
				Label ColSeaCSCVPCSSP = new Label(5,43,"PASS"); 
				wsheet4.addCell(ColSeaCSCVPCSSP); 
			}
			else
			{
				Label ColSeaCSCVPCSSF = new Label(5,43,"FAIL"); 
				wsheet4.addCell(ColSeaCSCVPCSSF); 
			}
			
			
			// Variety Page - Our Coffee Collection - COLLECTIONS - Seasonal - Shop on Keurig.com 
			
			WebElement SeaShopVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[5]/div/div/div/div[2]/div[4]/div[2]/div[2]/div[5]/a/div")); 
			String SeaShopVPColor = SeaShopVP.getCssValue("color");
			System.out.println("Actual Color of Shop on Keurig.com in Seasonal in Variety Page is : "	+ SeaShopVPColor );
			// Convert RGB to Hex Value
			Color HSeaShopVPColor = Color.fromString(SeaShopVPColor);
			String HexSeaShopVPColor = HSeaShopVPColor.asHex();
			System.out.println("Actual Hex Color of Shop on Keurig.com in Seasonal in Variety Page is : "	+ HexSeaShopVPColor );
			
			String SeaShopVPFontSize = SeaShopVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Shop on Keurig.com in Seasonal in Variety Page is : "	+ SeaShopVPFontSize);
			String SeaShopVPFontFamily = SeaShopVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Shop on Keurig.com in Seasonal in Variety Page is : "	+ SeaShopVPFontFamily);
			String SeaShopVPContent = SeaShopVP.getText();
			System.out.println("Actual Content of Shop on Keurig.com in Seasonal in Variety Page is : "	+ SeaShopVPContent);
					
			String ExpSeaShopVPColor = sheet4.getCell(1,44).getContents(); //column,row
			System.out.println("Expected Color of Shop on Keurig.com in Seasonal in Variety Page is : "	+ ExpSeaShopVPColor );
			String ExpSeaShopVPFontSize = sheet4.getCell(2,44).getContents();
			System.out.println("Expected Font Size of Shop on Keurig.com in Seasonal in Variety Page is : "	+ ExpSeaShopVPFontSize);
			String ExpSeaShopVPFontFamily = sheet4.getCell(3,44).getContents();
			System.out.println("Expected Font Family of Shop on Keurig.com in Seasonal in Variety Page is : "	+ ExpSeaShopVPFontFamily);
			String ExpSeaShopVPContent = sheet4.getCell(4,44).getContents();
			System.out.println("Expected Content of Shop on Keurig.com in Seasonal in Variety Page is : "	+ ExpSeaShopVPContent);
					
			if(HexSeaShopVPColor.equals(ExpSeaShopVPColor))
			{
				System.out.println("Shop on Keurig.com in Seasonal in Variety Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("Shop on Keurig.com in Seasonal in Variety Page Color is not in match with creative - Fail");
				bw.write("Shop on Keurig.com in Seasonal in Variety Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(SeaShopVPFontSize.equals(ExpSeaShopVPFontSize))
			{
				System.out.println("Shop on Keurig.com in Seasonal in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Shop on Keurig.com in Seasonal in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Shop on Keurig.com in Seasonal in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(SeaShopVPFontFamily.equals(ExpSeaShopVPFontFamily))
			{
				System.out.println("Shop on Keurig.com in Seasonal in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Shop on Keurig.com in Seasonal in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Shop on Keurig.com in Seasonal in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(SeaShopVPContent.equals(ExpSeaShopVPContent))
			{
				System.out.println("Shop on Keurig.com in Seasonal Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Shop on Keurig.com in Seasonal Content in Variety Page is not in match with creative - Fail");
				bw.write("Shop on Keurig.com in Seasonal Content in Variety Page is not in match with creative - Fail");  
			    bw.newLine();
			}	
			
			if(HexSeaShopVPColor.equals(ExpSeaShopVPColor) && SeaShopVPFontSize.equals(ExpSeaShopVPFontSize) && SeaShopVPFontFamily.equals(ExpSeaShopVPFontFamily) && SeaShopVPContent.equals(ExpSeaShopVPContent))
			{
				Label SeaShopVPCSSP = new Label(5,44,"PASS"); 
				wsheet4.addCell(SeaShopVPCSSP); 
			}
			else
			{
				Label SeaShopVPCSSF = new Label(5,44,"FAIL"); 
				wsheet4.addCell(SeaShopVPCSSF); 
			}
			
			Thread.sleep(3000);
			JavascriptExecutor jsecofcss = (JavascriptExecutor)driver;
			jsecofcss.executeScript("window.scrollBy(0,600)", "");
			Thread.sleep(8000);

			// Variety Page - Our Coffee Collection - COLLECTIONS - COFFEEHOUSE
			
			WebElement OurCoffeeColCofVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[5]/div/div/div/div[2]/div[4]/div[3]/div[2]/div[1]")); 
			
			String OurCoffeeColCofVPFontSize = OurCoffeeColCofVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection COFFEEHOUSE in Variety Page is : "	+ OurCoffeeColCofVPFontSize);
			String OurCoffeeColCofVPFontFamily = OurCoffeeColCofVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection COFFEEHOUSE in Variety Page is : "	+ OurCoffeeColCofVPFontFamily);
			String OurCoffeeColCofVPContent = OurCoffeeColCofVP.getText();
			System.out.println("Actual Content of Our Coffee Collection COFFEEHOUSE in Variety Page is : "	+ OurCoffeeColCofVPContent);
					
			String ExpOurCoffeeColCofVPFontSize = sheet4.getCell(2,45).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection COFFEEHOUSE in Variety Page is : "	+ ExpOurCoffeeColCofVPFontSize);
			String ExpOurCoffeeColCofVPFontFamily = sheet4.getCell(3,45).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection COFFEEHOUSE in Variety Page is : "	+ ExpOurCoffeeColCofVPFontFamily);
			String ExpOurCoffeeColCofVPContent = sheet4.getCell(4,45).getContents();
			System.out.println("Expected Content of Our Coffee Collection COFFEEHOUSE in Variety Page is : "	+ ExpOurCoffeeColCofVPContent);
			
			if(OurCoffeeColCofVPFontSize.equals(ExpOurCoffeeColCofVPFontSize))
			{
				System.out.println("Our Coffee Collection COFFEEHOUSE in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection COFFEEHOUSE in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection COFFEEHOUSE in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(OurCoffeeColCofVPFontFamily.equals(ExpOurCoffeeColCofVPFontFamily))
			{
				System.out.println("Our Coffee Collection COFFEEHOUSE in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection COFFEEHOUSE in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection COFFEEHOUSE in Variety Page Font Family is not in match with creative - Fail");  
			    bw.newLine();
			}
			

			if(OurCoffeeColCofVPContent.equals(ExpOurCoffeeColCofVPContent))
			{
				System.out.println("Our Coffee Collection COFFEEHOUSE Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection COFFEEHOUSE Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection COFFEEHOUSE Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}	
			
			if(OurCoffeeColCofVPFontSize.equals(ExpOurCoffeeColCofVPFontSize) && OurCoffeeColCofVPFontFamily.equals(ExpOurCoffeeColCofVPFontFamily) && OurCoffeeColCofVPContent.equals(ExpOurCoffeeColCofVPContent))
			{
				Label OurCoffeeColCofVPCSSP = new Label(5,45,"PASS"); 
				wsheet4.addCell(OurCoffeeColCofVPCSSP); 
			}
			else
			{
				Label OurCoffeeColCofVPCSSF = new Label(5,45,"FAIL"); 
				wsheet4.addCell(OurCoffeeColCofVPCSSF); 
			}
			
			// Variety Page - Our Coffee Collection - COLLECTIONS - COFFEEHOUSE Desc
			
			WebElement ColCofDescVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[5]/div/div/div/div[2]/div[4]/div[3]/div[2]/div[2]")); 
			
			String ColCofDescVPFontSize = ColCofDescVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection COFFEEHOUSE Desc in Variety Page is : "	+ ColCofDescVPFontSize);
			String ColCofDescVPFontFamily = ColCofDescVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection COFFEEHOUSE Desc in Variety Page is : "	+ ColCofDescVPFontFamily);
			String ColCofDescVPContent = ColCofDescVP.getText();
			System.out.println("Actual Content of Our Coffee Collection COFFEEHOUSE Desc in Variety Page is : "	+ ColCofDescVPContent);
					
			String ExpColCofDescVPFontSize = sheet4.getCell(2,46).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection COFFEEHOUSE Desc in Variety Page is : "	+ ExpColCofDescVPFontSize);
			String ExpColCofDescVPFontFamily = sheet4.getCell(3,46).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection COFFEEHOUSE Desc in Variety Page is : "	+ ExpColCofDescVPFontFamily);
			String ExpColCofDescVPContent = sheet4.getCell(4,46).getContents();
			System.out.println("Expected Content of Our Coffee Collection COFFEEHOUSE Desc in Variety Page is : "	+ ExpColCofDescVPContent);
			
			if(ColCofDescVPFontSize.equals(ExpColCofDescVPFontSize))
			{
				System.out.println("Our Coffee Collection COFFEEHOUSE Desc in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection COFFEEHOUSE Desc in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection COFFEEHOUSE Desc in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(ColCofDescVPFontFamily.equals(ExpColCofDescVPFontFamily))
			{
				System.out.println("Our Coffee Collection COFFEEHOUSE Desc in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection COFFEEHOUSE Desc in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection COFFEEHOUSE Desc in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(ColCofDescVPContent.equals(ExpColCofDescVPContent))
			{
				System.out.println("Our Coffee Collection COFFEEHOUSE Desc Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection COFFEEHOUSE Desc Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection COFFEEHOUSE Desc Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}	
			
			if(ColCofDescVPFontSize.equals(ExpColCofDescVPFontSize) && ColCofDescVPFontFamily.equals(ExpColCofDescVPFontFamily) && ColCofDescVPContent.equals(ExpColCofDescVPContent))
			{
				Label ColCofDescVPCSSP = new Label(5,46,"PASS"); 
				wsheet4.addCell(ColCofDescVPCSSP); 
			}
			else
			{
				Label ColCofDescVPCSSF = new Label(5,46,"FAIL"); 
				wsheet4.addCell(ColCofDescVPCSSF); 
			}
		
			// Variety Page - Our Coffee Collection - COLLECTIONS - COFFEEHOUSE - Popular Flavor Varieties
			
			WebElement ColCofPFVVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[5]/div/div/div/div[2]/div[4]/div[3]/div[2]/div[3]")); 
			
			String ColCofPFVVPFontSize = ColCofPFVVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection COFFEEHOUSE - Popular Flavor Varieties in Variety Page is : "	+ ColCofPFVVPFontSize);
			String ColCofPFVVPFontFamily = ColCofPFVVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection COFFEEHOUSE - Popular Flavor Varieties in Variety Page is : "	+ ColCofPFVVPFontFamily);
			String ColCofPFVVPContent = ColCofPFVVP.getText();
			System.out.println("Actual Content of Our Coffee Collection COFFEEHOUSE - Popular Flavor Varieties in Variety Page is : "	+ ColCofPFVVPContent);
					
			String ExpColCofPFVVPFontSize = sheet4.getCell(2,47).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection COFFEEHOUSE - Popular Flavor Varieties in Variety Page is : "	+ ExpColCofPFVVPFontSize);
			String ExpColCofPFVVPFontFamily = sheet4.getCell(3,47).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection COFFEEHOUSE - Popular Flavor Varieties in Variety Page is : "	+ ExpColCofPFVVPFontFamily);
			String ExpColCofPFVVPContent = sheet4.getCell(4,47).getContents();
			System.out.println("Expected Content of Our Coffee Collection COFFEEHOUSE - Popular Flavor Varieties in Variety Page is : "	+ ExpColCofPFVVPContent);
			
			if(ColCofPFVVPFontSize.equals(ExpColCofPFVVPFontSize))
			{
				System.out.println("Our Coffee Collection COFFEEHOUSE - Popular Flavor Varieties in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection COFFEEHOUSE - Popular Flavor Varieties in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection COFFEEHOUSE - Popular Flavor Varieties in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(ColCofPFVVPFontFamily.equals(ExpColCofPFVVPFontFamily))
			{
				System.out.println("Our Coffee Collection COFFEEHOUSE - Popular Flavor Varieties in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection COFFEEHOUSE - Popular Flavor Varieties in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection COFFEEHOUSE - Popular Flavor Varieties in Variety Page Font Family is not in match with creative - Fail");  
			    bw.newLine();
			}
			

			if(ColCofPFVVPContent.equals(ExpColCofPFVVPContent))
			{
				System.out.println("Our Coffee Collection COFFEEHOUSE - Popular Flavor Varieties Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection COFFEEHOUSE - Popular Flavor Varieties Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection COFFEEHOUSE - Popular Flavor Varieties Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}	
			
			if(ColCofPFVVPFontSize.equals(ExpColCofPFVVPFontSize) && ColCofPFVVPFontFamily.equals(ExpColCofPFVVPFontFamily) && ColCofPFVVPContent.equals(ExpColCofPFVVPContent))
			{
				Label ColCofPFVVPCSSP = new Label(5,47,"PASS"); 
				wsheet4.addCell(ColCofPFVVPCSSP); 
			}
			else
			{
				Label ColCofPFVVPCSSF = new Label(5,47,"FAIL"); 
				wsheet4.addCell(ColCofPFVVPCSSF); 
			}
			
			// Variety Page - Our Coffee Collection - COLLECTIONS - COFFEEHOUSE - Popular Flavor Varieties - Salted Caramel Macchiato
			
			WebElement ColCofSCMVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[5]/div/div/div/div[2]/div[4]/div[3]/div[2]/div[4]/div[1]")); 
			
			String ColCofSCMVPFontSize = ColCofSCMVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection COFFEEHOUSE Salted Caramel Macchiato in Variety Page is : "	+ ColCofSCMVPFontSize);
			String ColCofSCMVPFontFamily = ColCofSCMVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection COFFEEHOUSE Salted Caramel Macchiato in Variety Page is : "	+ ColCofSCMVPFontFamily);
			String ColCofSCMVPContent = ColCofSCMVP.getText();
			System.out.println("Actual Content of Our Coffee Collection COFFEEHOUSE Salted Caramel Macchiato in Variety Page is : "	+ ColCofSCMVPContent);
					
			String ExpColCofSCMVPFontSize = sheet4.getCell(2,48).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection COFFEEHOUSE Salted Caramel Macchiato in Variety Page is : "	+ ExpColCofSCMVPFontSize);
			String ExpColCofSCMVPFontFamily = sheet4.getCell(3,48).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection COFFEEHOUSE Salted Caramel Macchiato in Variety Page is : "	+ ExpColCofSCMVPFontFamily);
			String ExpColCofSCMVPContent = sheet4.getCell(4,48).getContents();
			System.out.println("Expected Content of Our Coffee Collection COFFEEHOUSE Salted Caramel Macchiato in Variety Page is : "	+ ExpColCofSCMVPContent);
			
			if(ColCofSCMVPFontSize.equals(ExpColCofSCMVPFontSize))
			{
				System.out.println("Our Coffee Collection COFFEEHOUSE Salted Caramel Macchiato in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection COFFEEHOUSE Salted Caramel Macchiato in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection COFFEEHOUSE Salted Caramel Macchiato in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(ColCofSCMVPFontFamily.equals(ExpColCofSCMVPFontFamily))
			{
				System.out.println("Our Coffee Collection COFFEEHOUSE Salted Caramel Macchiato in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection COFFEEHOUSE Salted Caramel Macchiato in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection COFFEEHOUSE Salted Caramel Macchiato in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(ColCofSCMVPContent.equals(ExpColCofSCMVPContent))
			{
				System.out.println("Our Coffee Collection COFFEEHOUSE Salted Caramel Macchiato Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection COFFEEHOUSE Salted Caramel Macchiato Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection COFFEEHOUSE Salted Caramel Macchiato Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}	
			
			if(ColCofSCMVPFontSize.equals(ExpColCofSCMVPFontSize) && ColCofSCMVPFontFamily.equals(ExpColCofSCMVPFontFamily) && ColCofSCMVPContent.equals(ExpColCofSCMVPContent))
			{
				Label ColCofSCMVPCSSP = new Label(5,48,"PASS"); 
				wsheet4.addCell(ColCofSCMVPCSSP); 
			}
			else
			{
				Label ColCofSCMVPCSSF = new Label(5,48,"FAIL"); 
				wsheet4.addCell(ColCofSCMVPCSSF); 
			}
			
			// Variety Page - Our Coffee Collection - COLLECTIONS - COFFEEHOUSE - Popular Flavor Varieties - Vanilla Latte
			
			WebElement ColCofVLVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[5]/div/div/div/div[2]/div[4]/div[3]/div[2]/div[4]/div[2]")); 
			
			String ColCofVLVPFontSize = ColCofVLVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection COFFEEHOUSE Vanilla Latte in Variety Page is : "	+ ColCofVLVPFontSize);
			String ColCofVLVPFontFamily = ColCofVLVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection COFFEEHOUSE Vanilla Latte in Variety Page is : "	+ ColCofVLVPFontFamily);
			String ColCofVLVPContent = ColCofVLVP.getText();
			System.out.println("Actual Content of Our Coffee Collection COFFEEHOUSE Vanilla Latte in Variety Page is : "	+ ColCofVLVPContent);
					
			String ExpColCofVLVPFontSize = sheet4.getCell(2,49).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection COFFEEHOUSE Vanilla Latte in Variety Page is : "	+ ExpColCofVLVPFontSize);
			String ExpColCofVLVPFontFamily = sheet4.getCell(3,49).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection COFFEEHOUSE Vanilla Latte in Variety Page is : "	+ ExpColCofVLVPFontFamily);
			String ExpColCofVLVPContent = sheet4.getCell(4,49).getContents();
			System.out.println("Expected Content of Our Coffee Collection COFFEEHOUSE Vanilla Latte in Variety Page is : "	+ ExpColCofVLVPContent);
			
			if(ColCofVLVPFontSize.equals(ExpColCofVLVPFontSize))
			{
				System.out.println("Our Coffee Collection COFFEEHOUSE Vanilla Latte in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection COFFEEHOUSE Vanilla Latte in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection COFFEEHOUSE Vanilla Latte in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(ColCofVLVPFontFamily.equals(ExpColCofVLVPFontFamily))
			{
				System.out.println("Our Coffee Collection COFFEEHOUSE Vanilla Latte in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection COFFEEHOUSE Vanilla Latte in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection COFFEEHOUSE Vanilla Latte in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(ColCofVLVPContent.equals(ExpColCofVLVPContent))
			{
				System.out.println("Our Coffee Collection COFFEEHOUSE Vanilla Latte Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection COFFEEHOUSE Vanilla Latte Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection COFFEEHOUSE Vanilla Latte Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}	
			
			if(ColCofVLVPFontSize.equals(ExpColCofVLVPFontSize) && ColCofVLVPFontFamily.equals(ExpColCofVLVPFontFamily) && ColCofVLVPContent.equals(ExpColCofVLVPContent))
			{
				Label ColCofVLVPCSSP = new Label(5,49,"PASS"); 
				wsheet4.addCell(ColCofVLVPCSSP); 
			}
			else
			{
				Label ColCofVLVPCSSF = new Label(5,49,"FAIL"); 
				wsheet4.addCell(ColCofVLVPCSSF); 
			}
			
			// Variety Page - Our Coffee Collection - COLLECTIONS - COFFEEHOUSE - Popular Flavor Varieties - Cappuccino
			
			WebElement ColCofCVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[5]/div/div/div/div[2]/div[4]/div[3]/div[2]/div[4]/div[3]")); 
			
			String ColCofCVPFontSize = ColCofCVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection COFFEEHOUSE Cappuccino in Variety Page is : "	+ ColCofCVPFontSize);
			String ColCofCVPFontFamily = ColCofCVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection COFFEEHOUSE Cappuccino in Variety Page is : "	+ ColCofCVPFontFamily);
			String ColCofCVPContent = ColCofCVP.getText();
			System.out.println("Actual Content of Our Coffee Collection COFFEEHOUSE Cappuccino in Variety Page is : "	+ ColCofCVPContent);
					
			String ExpColCofCVPFontSize = sheet4.getCell(2,50).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection COFFEEHOUSE Cappuccino in Variety Page is : "	+ ExpColCofCVPFontSize);
			String ExpColCofCVPFontFamily = sheet4.getCell(3,50).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection COFFEEHOUSE Cappuccino in Variety Page is : "	+ ExpColCofCVPFontFamily);
			String ExpColCofCVPContent = sheet4.getCell(4,50).getContents();
			System.out.println("Expected Content of Our Coffee Collection COFFEEHOUSE Cappuccino in Variety Page is : "	+ ExpColCofCVPContent);
			
			if(ColCofCVPFontSize.equals(ExpColCofCVPFontSize))
			{
				System.out.println("Our Coffee Collection COFFEEHOUSE Cappuccino in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection COFFEEHOUSE Cappuccino in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection COFFEEHOUSE Cappuccino in Variety Page Font Size is not in match with creative - Fail");  
			    bw.newLine();
			}
					
			if(ColCofCVPFontFamily.equals(ExpColCofCVPFontFamily))
			{
				System.out.println("Our Coffee Collection COFFEEHOUSE Cappuccino in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection COFFEEHOUSE Cappuccino in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection COFFEEHOUSE Cappuccino in Variety Page Font Family is not in match with creative - Fail");  
			    bw.newLine();
			}
			

			if(ColCofCVPContent.equals(ExpColCofCVPContent))
			{
				System.out.println("Our Coffee Collection COFFEEHOUSE Cappuccino Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection COFFEEHOUSE Cappuccino Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection COFFEEHOUSE Cappuccino Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}	
			
			if(ColCofCVPFontSize.equals(ExpColCofCVPFontSize) && ColCofCVPFontFamily.equals(ExpColCofCVPFontFamily) && ColCofCVPContent.equals(ExpColCofCVPContent))
			{
				Label ColCofCVPCSSP = new Label(5,50,"PASS"); 
				wsheet4.addCell(ColCofCVPCSSP); 
			}
			else
			{
				Label ColCofCVPCSSF = new Label(5,50,"FAIL"); 
				wsheet4.addCell(ColCofCVPCSSF); 
			}
			
			// Variety Page - Our Coffee Collection - COLLECTIONS - COFFEEHOUSE - Shop on Keurig.com 
			
			WebElement CofShopVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[5]/div/div/div/div[2]/div[4]/div[3]/div[2]/div[5]/a/div")); 
			String CofShopVPColor = CofShopVP.getCssValue("color");
			System.out.println("Actual Color of Shop on Keurig.com in COFFEEHOUSE in Variety Page is : "	+ CofShopVPColor );
			// Convert RGB to Hex Value
			Color HCofShopVPColor = Color.fromString(CofShopVPColor);
			String HexCofShopVPColor = HCofShopVPColor.asHex();
			System.out.println("Actual Hex Color of Shop on Keurig.com in COFFEEHOUSE in Variety Page is : "	+ HexCofShopVPColor );
			
			String CofShopVPFontSize = CofShopVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Shop on Keurig.com in COFFEEHOUSE in Variety Page is : "	+ CofShopVPFontSize);
			String CofShopVPFontFamily = CofShopVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Shop on Keurig.com in COFFEEHOUSE in Variety Page is : "	+ CofShopVPFontFamily);
			String CofShopVPContent = CofShopVP.getText();
			System.out.println("Actual Content of Shop on Keurig.com in COFFEEHOUSE in Variety Page is : "	+ CofShopVPContent);
					
			String ExpCofShopVPColor = sheet4.getCell(1,51).getContents(); //column,row
			System.out.println("Expected Color of Shop on Keurig.com in COFFEEHOUSE in Variety Page is : "	+ ExpCofShopVPColor );
			String ExpCofShopVPFontSize = sheet4.getCell(2,51).getContents();
			System.out.println("Expected Font Size of Shop on Keurig.com in COFFEEHOUSE in Variety Page is : "	+ ExpCofShopVPFontSize);
			String ExpCofShopVPFontFamily = sheet4.getCell(3,51).getContents();
			System.out.println("Expected Font Family of Shop on Keurig.com in COFFEEHOUSE in Variety Page is : "	+ ExpCofShopVPFontFamily);
			String ExpCofShopVPContent = sheet4.getCell(4,51).getContents();
			System.out.println("Expected Content of Shop on Keurig.com in COFFEEHOUSE in Variety Page is : "	+ ExpCofShopVPContent);
					
			if(HexCofShopVPColor.equals(ExpCofShopVPColor))
			{
				System.out.println("Shop on Keurig.com in COFFEEHOUSE in Variety Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("Shop on Keurig.com in COFFEEHOUSE in Variety Page Color is not in match with creative - Fail");
				bw.write("Shop on Keurig.com in COFFEEHOUSE in Variety Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(CofShopVPFontSize.equals(ExpCofShopVPFontSize))
			{
				System.out.println("Shop on Keurig.com in COFFEEHOUSE in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Shop on Keurig.com in COFFEEHOUSE in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Shop on Keurig.com in COFFEEHOUSE in Variety Page Font Size is not in match with creative - Fail");  
			    bw.newLine();
			}
					
			if(CofShopVPFontFamily.equals(ExpCofShopVPFontFamily))
			{
				System.out.println("Shop on Keurig.com in COFFEEHOUSE in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Shop on Keurig.com in COFFEEHOUSE in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Shop on Keurig.com in COFFEEHOUSE in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(CofShopVPContent.equals(ExpCofShopVPContent))
			{
				System.out.println("Shop on Keurig.com in COFFEEHOUSE Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Shop on Keurig.com in COFFEEHOUSE Content in Variety Page is not in match with creative - Fail");
				bw.write("Shop on Keurig.com in COFFEEHOUSE Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}	
			
			if(HexCofShopVPColor.equals(ExpCofShopVPColor) && CofShopVPFontSize.equals(ExpCofShopVPFontSize) && CofShopVPFontFamily.equals(ExpCofShopVPFontFamily) && CofShopVPContent.equals(ExpCofShopVPContent))
			{
				Label CofShopVPCSSP = new Label(5,51,"PASS"); 
				wsheet4.addCell(CofShopVPCSSP); 
			}
			else
			{
				Label CofShopVPCSSF = new Label(5,51,"FAIL"); 
				wsheet4.addCell(CofShopVPCSSF); 
			}
			
			// Variety Page - Our Coffee Collection - COLLECTIONS - Organic
			
			WebElement OurCoffeeColOrgVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[5]/div/div/div/div[2]/div[4]/div[4]/div[2]/div[1]")); 
			
			String OurCoffeeColOrgVPFontSize = OurCoffeeColOrgVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection Organic in Variety Page is : "	+ OurCoffeeColOrgVPFontSize);
			String OurCoffeeColOrgVPFontFamily = OurCoffeeColOrgVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection Organic in Variety Page is : "	+ OurCoffeeColOrgVPFontFamily);
			String OurCoffeeColOrgVPContent = OurCoffeeColOrgVP.getText();
			System.out.println("Actual Content of Our Coffee Collection Organic in Variety Page is : "	+ OurCoffeeColOrgVPContent);
					
			String ExpOurCoffeeColOrgVPFontSize = sheet4.getCell(2,52).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection Organic in Variety Page is : "	+ ExpOurCoffeeColOrgVPFontSize);
			String ExpOurCoffeeColOrgVPFontFamily = sheet4.getCell(3,52).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection Organic in Variety Page is : "	+ ExpOurCoffeeColOrgVPFontFamily);
			String ExpOurCoffeeColOrgVPContent = sheet4.getCell(4,52).getContents();
			System.out.println("Expected Content of Our Coffee Collection Organic in Variety Page is : "	+ ExpOurCoffeeColOrgVPContent);
			
			if(OurCoffeeColOrgVPFontSize.equals(ExpOurCoffeeColOrgVPFontSize))
			{
				System.out.println("Our Coffee Collection Organic in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Organic in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection Organic in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(OurCoffeeColOrgVPFontFamily.equals(ExpOurCoffeeColOrgVPFontFamily))
			{
				System.out.println("Our Coffee Collection Organic in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Organic in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection Organic in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(OurCoffeeColOrgVPContent.equals(ExpOurCoffeeColOrgVPContent))
			{
				System.out.println("Our Coffee Collection Organic Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Organic Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection Organic Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}	
			
			if(OurCoffeeColOrgVPFontSize.equals(ExpOurCoffeeColOrgVPFontSize) && OurCoffeeColOrgVPFontFamily.equals(ExpOurCoffeeColOrgVPFontFamily) && OurCoffeeColOrgVPContent.equals(ExpOurCoffeeColOrgVPContent))
			{
				Label OurCoffeeColOrgVPCSSP = new Label(5,52,"PASS"); 
				wsheet4.addCell(OurCoffeeColOrgVPCSSP); 
			}
			else
			{
				Label OurCoffeeColOrgVPCSSF = new Label(5,52,"FAIL"); 
				wsheet4.addCell(OurCoffeeColOrgVPCSSF); 
			}
			
			// Variety Page - Our Coffee Collection - COLLECTIONS - Organic Desc
			
			WebElement ColOrgDescVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[5]/div/div/div/div[2]/div[4]/div[4]/div[2]/div[2]")); 
			
			String ColOrgDescVPFontSize = ColOrgDescVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection Organic Desc in Variety Page is : "	+ ColOrgDescVPFontSize);
			String ColOrgDescVPFontFamily = ColOrgDescVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection Organic Desc in Variety Page is : "	+ ColOrgDescVPFontFamily);
			String ColOrgDescVPContent = ColOrgDescVP.getText();
			System.out.println("Actual Content of Our Coffee Collection Organic Desc in Variety Page is : "	+ ColOrgDescVPContent);
					
			String ExpColOrgDescVPFontSize = sheet4.getCell(2,53).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection Organic Desc in Variety Page is : "	+ ExpColOrgDescVPFontSize);
			String ExpColOrgDescVPFontFamily = sheet4.getCell(3,53).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection Organic Desc in Variety Page is : "	+ ExpColOrgDescVPFontFamily);
			String ExpColOrgDescVPContent = sheet4.getCell(4,53).getContents();
			System.out.println("Expected Content of Our Coffee Collection Organic Desc in Variety Page is : "	+ ExpColOrgDescVPContent);
			
			if(ColOrgDescVPFontSize.equals(ExpColOrgDescVPFontSize))
			{
				System.out.println("Our Coffee Collection Organic Desc in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Organic Desc in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection Organic Desc in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(ColOrgDescVPFontFamily.equals(ExpColOrgDescVPFontFamily))
			{
				System.out.println("Our Coffee Collection Organic Desc in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Organic Desc in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection Organic Desc in Variety Page Font Family is not in match with creative - Fail");  
			    bw.newLine();
			}
			

			if(ColOrgDescVPContent.equals(ExpColOrgDescVPContent))
			{
				System.out.println("Our Coffee Collection Organic Desc Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Organic Desc Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection Organic Desc Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}	
			
			if(ColOrgDescVPFontSize.equals(ExpColOrgDescVPFontSize) && ColOrgDescVPFontFamily.equals(ExpColOrgDescVPFontFamily) && ColOrgDescVPContent.equals(ExpColOrgDescVPContent))
			{
				Label ColOrgDescVPCSSP = new Label(5,53,"PASS"); 
				wsheet4.addCell(ColOrgDescVPCSSP); 
			}
			else
			{
				Label ColOrgDescVPCSSF = new Label(5,53,"FAIL"); 
				wsheet4.addCell(ColOrgDescVPCSSF); 
			}
			
			// Variety Page - Our Coffee Collection - COLLECTIONS - Organic - Popular Varieties
			
			WebElement ColOrgPVVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[5]/div/div/div/div[2]/div[4]/div[4]/div[2]/div[3]")); 
			
			String ColOrgPVVPFontSize = ColOrgPVVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection Organic - Popular Varieties in Variety Page is : "	+ ColOrgPVVPFontSize);
			String ColOrgPVVPFontFamily = ColOrgPVVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection Organic - Popular Varieties in Variety Page is : "	+ ColOrgPVVPFontFamily);
			String ColOrgPVVPContent = ColOrgPVVP.getText();
			System.out.println("Actual Content of Our Coffee Collection Organic - Popular Varieties in Variety Page is : "	+ ColOrgPVVPContent);
					
			String ExpColOrgPVVPFontSize = sheet4.getCell(2,54).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection Organic - Popular Varieties in Variety Page is : "	+ ExpColOrgPVVPFontSize);
			String ExpColOrgPVVPFontFamily = sheet4.getCell(3,54).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection Organic - Popular Varieties in Variety Page is : "	+ ExpColOrgPVVPFontFamily);
			String ExpColOrgPVVPContent = sheet4.getCell(4,54).getContents();
			System.out.println("Expected Content of Our Coffee Collection Organic - Popular Varieties in Variety Page is : "	+ ExpColOrgPVVPContent);
			
			if(ColOrgPVVPFontSize.equals(ExpColOrgPVVPFontSize))
			{
				System.out.println("Our Coffee Collection Organic - Popular Varieties in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Organic - Popular Varieties in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection Organic - Popular Varieties in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(ColOrgPVVPFontFamily.equals(ExpColOrgPVVPFontFamily))
			{
				System.out.println("Our Coffee Collection Organic - Popular Varieties in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Organic - Popular Varieties in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection Organic - Popular Varieties in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(ColOrgPVVPContent.equals(ExpColOrgPVVPContent))
			{
				System.out.println("Our Coffee Collection Organic - Popular Varieties Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Organic - Popular Varieties Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection Organic - Popular Varieties Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}	
			
			if(ColOrgPVVPFontSize.equals(ExpColOrgPVVPFontSize) && ColOrgPVVPFontFamily.equals(ExpColOrgPVVPFontFamily) && ColOrgPVVPContent.equals(ExpColOrgPVVPContent))
			{
				Label ColOrgPVVPCSSP = new Label(5,54,"PASS"); 
				wsheet4.addCell(ColOrgPVVPCSSP); 
			}
			else
			{
				Label ColOrgPVVPCSSF = new Label(5,54,"FAIL"); 
				wsheet4.addCell(ColOrgPVVPCSSF); 
			}
			
			// Variety Page - Our Coffee Collection - COLLECTIONS - Organic - Popular Varieties - Ethiopia Yirgacheffe
			
			WebElement ColOrgEYVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[5]/div/div/div/div[2]/div[4]/div[4]/div[2]/div[4]/div[1]")); 
			
			String ColOrgEYVPFontSize = ColOrgEYVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection Organic Ethiopia Yirgacheffe in Variety Page is : "	+ ColOrgEYVPFontSize);
			String ColOrgEYVPFontFamily = ColOrgEYVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection Organic Ethiopia Yirgacheffe in Variety Page is : "	+ ColOrgEYVPFontFamily);
			String ColOrgEYVPContent = ColOrgEYVP.getText();
			System.out.println("Actual Content of Our Coffee Collection Organic Ethiopia Yirgacheffe in Variety Page is : "	+ ColOrgEYVPContent);
					
			String ExpColOrgEYVPFontSize = sheet4.getCell(2,55).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection Organic Ethiopia Yirgacheffe in Variety Page is : "	+ ExpColOrgEYVPFontSize);
			String ExpColOrgEYVPFontFamily = sheet4.getCell(3,55).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection Organic Ethiopia Yirgacheffe in Variety Page is : "	+ ExpColOrgEYVPFontFamily);
			String ExpColOrgEYVPContent = sheet4.getCell(4,55).getContents();
			System.out.println("Expected Content of Our Coffee Collection Organic Ethiopia Yirgacheffe in Variety Page is : "	+ ExpColOrgEYVPContent);
			
			if(ColOrgEYVPFontSize.equals(ExpColOrgEYVPFontSize))
			{
				System.out.println("Our Coffee Collection Organic Ethiopia Yirgacheffe in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Organic Ethiopia Yirgacheffe in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection Organic Ethiopia Yirgacheffe in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(ColOrgEYVPFontFamily.equals(ExpColOrgEYVPFontFamily))
			{
				System.out.println("Our Coffee Collection Organic Ethiopia Yirgacheffe in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Organic Ethiopia Yirgacheffe in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection Organic Ethiopia Yirgacheffe in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(ColOrgEYVPContent.equals(ExpColOrgEYVPContent))
			{
				System.out.println("Our Coffee Collection Organic Ethiopia Yirgacheffe Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Organic Ethiopia Yirgacheffe Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection Organic Ethiopia Yirgacheffe Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}	
			
			if(ColOrgEYVPFontSize.equals(ExpColOrgEYVPFontSize) && ColOrgEYVPFontFamily.equals(ExpColOrgEYVPFontFamily) && ColOrgEYVPContent.equals(ExpColOrgEYVPContent))
			{
				Label ColOrgEYVPCSSP = new Label(5,55,"PASS"); 
				wsheet4.addCell(ColOrgEYVPCSSP); 
			}
			else
			{
				Label ColOrgEYVPCSSF = new Label(5,55,"FAIL"); 
				wsheet4.addCell(ColOrgEYVPCSSF); 
			}
		
			// Variety Page - Our Coffee Collection - COLLECTIONS - Organic - Popular Varieties - Peru Cajamarca
			
			WebElement ColOrgPCVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[5]/div/div/div/div[2]/div[4]/div[4]/div[2]/div[4]/div[2]")); 
			
			String ColOrgPCVPFontSize = ColOrgPCVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection Organic Peru Cajamarca in Variety Page is : "	+ ColOrgPCVPFontSize);
			String ColOrgPCVPFontFamily = ColOrgPCVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection Organic Peru Cajamarca in Variety Page is : "	+ ColOrgPCVPFontFamily);
			String ColOrgPCVPContent = ColOrgPCVP.getText();
			System.out.println("Actual Content of Our Coffee Collection Organic Peru Cajamarca in Variety Page is : "	+ ColOrgPCVPContent);
					
			String ExpColOrgPCVPFontSize = sheet4.getCell(2,56).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection Organic Peru Cajamarca in Variety Page is : "	+ ExpColOrgPCVPFontSize);
			String ExpColOrgPCVPFontFamily = sheet4.getCell(3,56).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection Organic Peru Cajamarca in Variety Page is : "	+ ExpColOrgPCVPFontFamily);
			String ExpColOrgPCVPContent = sheet4.getCell(4,56).getContents();
			System.out.println("Expected Content of Our Coffee Collection Organic Peru Cajamarca in Variety Page is : "	+ ExpColOrgPCVPContent);
			
			if(ColOrgPCVPFontSize.equals(ExpColOrgPCVPFontSize))
			{
				System.out.println("Our Coffee Collection Organic Peru Cajamarca in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Organic Peru Cajamarca in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection Organic Peru Cajamarca in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(ColOrgPCVPFontFamily.equals(ExpColOrgPCVPFontFamily))
			{
				System.out.println("Our Coffee Collection Organic Peru Cajamarca in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Organic Peru Cajamarca in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection Organic Peru Cajamarca in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(ColOrgPCVPContent.equals(ExpColOrgPCVPContent))
			{
				System.out.println("Our Coffee Collection Organic Peru Cajamarca Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Organic Peru Cajamarca Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection Organic Peru Cajamarca Content in Variety Page is not in match with creative - Fail");  
			    bw.newLine();
			}
			
			if(ColOrgPCVPFontSize.equals(ExpColOrgPCVPFontSize) && ColOrgPCVPFontFamily.equals(ExpColOrgPCVPFontFamily) && ColOrgPCVPContent.equals(ExpColOrgPCVPContent))
			{
				Label ColOrgPCVPCSSP = new Label(5,56,"PASS"); 
				wsheet4.addCell(ColOrgPCVPCSSP); 
			}
			else
			{
				Label ColOrgPCVPCSSF = new Label(5,56,"FAIL"); 
				wsheet4.addCell(ColOrgPCVPCSSF); 
			}
			
			// Variety Page - Our Coffee Collection - COLLECTIONS - Organic - Popular Varieties - Sumatra Aceh
			
			WebElement ColOrgSAVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[5]/div/div/div/div[2]/div[4]/div[4]/div[2]/div[4]/div[3]")); 
			
			String ColOrgSAVPFontSize = ColOrgSAVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Our Coffee Collection Organic Sumatra Aceh in Variety Page is : "	+ ColOrgSAVPFontSize);
			String ColOrgSAVPFontFamily = ColOrgSAVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Our Coffee Collection Organic Sumatra Aceh in Variety Page is : "	+ ColOrgSAVPFontFamily);
			String ColOrgSAVPContent = ColOrgSAVP.getText();
			System.out.println("Actual Content of Our Coffee Collection Organic Sumatra Aceh in Variety Page is : "	+ ColOrgSAVPContent);
					
			String ExpColOrgSAVPFontSize = sheet4.getCell(2,57).getContents();
			System.out.println("Expected Font Size of Our Coffee Collection Organic Sumatra Aceh in Variety Page is : "	+ ExpColOrgSAVPFontSize);
			String ExpColOrgSAVPFontFamily = sheet4.getCell(3,57).getContents();
			System.out.println("Expected Font Family of Our Coffee Collection Organic Sumatra Aceh in Variety Page is : "	+ ExpColOrgSAVPFontFamily);
			String ExpColOrgSAVPContent = sheet4.getCell(4,57).getContents();
			System.out.println("Expected Content of Our Coffee Collection Organic Sumatra Aceh in Variety Page is : "	+ ExpColOrgSAVPContent);
			
			if(ColOrgSAVPFontSize.equals(ExpColOrgSAVPFontSize))
			{
				System.out.println("Our Coffee Collection Organic Sumatra Aceh in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Organic Sumatra Aceh in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Our Coffee Collection Organic Sumatra Aceh in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(ColOrgSAVPFontFamily.equals(ExpColOrgSAVPFontFamily))
			{
				System.out.println("Our Coffee Collection Organic Sumatra Aceh in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Organic Sumatra Aceh in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Our Coffee Collection Organic Sumatra Aceh in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(ColOrgSAVPContent.equals(ExpColOrgSAVPContent))
			{
				System.out.println("Our Coffee Collection Organic Sumatra Aceh Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Our Coffee Collection Organic Sumatra Aceh Content in Variety Page is not in match with creative - Fail");
				bw.write("Our Coffee Collection Organic Sumatra Aceh Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}	
			
			if(ColOrgSAVPFontSize.equals(ExpColOrgSAVPFontSize) && ColOrgSAVPFontFamily.equals(ExpColOrgSAVPFontFamily) && ColOrgSAVPContent.equals(ExpColOrgSAVPContent))
			{
				Label ColOrgSAVPCSSP = new Label(5,57,"PASS"); 
				wsheet4.addCell(ColOrgSAVPCSSP); 
			}
			else
			{
				Label ColOrgSAVPCSSF = new Label(5,57,"FAIL"); 
				wsheet4.addCell(ColOrgSAVPCSSF); 
			}
			
			// Variety Page - Our Coffee Collection - COLLECTIONS - Organic - Shop on Keurig.com 
			
			WebElement OrgShopVP = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[5]/div/div/div/div[2]/div[4]/div[4]/div[2]/div[5]/a/div")); 
			String OrgShopVPColor = OrgShopVP.getCssValue("color");
			System.out.println("Actual Color of Shop on Keurig.com in Organic in Variety Page is : "	+ OrgShopVPColor );
			// Convert RGB to Hex Value
			Color HOrgShopVPColor = Color.fromString(OrgShopVPColor);
			String HexOrgShopVPColor = HOrgShopVPColor.asHex();
			System.out.println("Actual Hex Color of Shop on Keurig.com in Organic in Variety Page is : "	+ HexOrgShopVPColor );
			
			String OrgShopVPFontSize = OrgShopVP.getCssValue("font-size");
			System.out.println("Actual Font Size of Shop on Keurig.com in Organic in Variety Page is : "	+ OrgShopVPFontSize);
			String OrgShopVPFontFamily = OrgShopVP.getCssValue("font-family");
			System.out.println("Actual Font Family of Shop on Keurig.com in Organic in Variety Page is : "	+ OrgShopVPFontFamily);
			String OrgShopVPContent = OrgShopVP.getText();
			System.out.println("Actual Content of Shop on Keurig.com in Organic in Variety Page is : "	+ OrgShopVPContent);
					
			String ExpOrgShopVPColor = sheet4.getCell(1,58).getContents(); //column,row
			System.out.println("Expected Color of Shop on Keurig.com in Organic in Variety Page is : "	+ ExpOrgShopVPColor );
			String ExpOrgShopVPFontSize = sheet4.getCell(2,58).getContents();
			System.out.println("Expected Font Size of Shop on Keurig.com in Organic in Variety Page is : "	+ ExpOrgShopVPFontSize);
			String ExpOrgShopVPFontFamily = sheet4.getCell(3,58).getContents();
			System.out.println("Expected Font Family of Shop on Keurig.com in Organic in Variety Page is : "	+ ExpOrgShopVPFontFamily);
			String ExpOrgShopVPContent = sheet4.getCell(4,58).getContents();
			System.out.println("Expected Content of Shop on Keurig.com in Organic in Variety Page is : "	+ ExpOrgShopVPContent);
					
			if(HexOrgShopVPColor.equals(ExpOrgShopVPColor))
			{
				System.out.println("Shop on Keurig.com in Organic in Variety Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("Shop on Keurig.com in Organic in Variety Page Color is not in match with creative - Fail");
				bw.write("Shop on Keurig.com in Organic in Variety Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(OrgShopVPFontSize.equals(ExpOrgShopVPFontSize))
			{
				System.out.println("Shop on Keurig.com in Organic in Variety Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Shop on Keurig.com in Organic in Variety Page Font Size is not in match with creative - Fail");
				bw.write("Shop on Keurig.com in Organic in Variety Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(OrgShopVPFontFamily.equals(ExpOrgShopVPFontFamily))
			{
				System.out.println("Shop on Keurig.com in Organic in Variety Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Shop on Keurig.com in Organic in Variety Page Font Family is not in match with creative - Fail");
				bw.write("Shop on Keurig.com in Organic in Variety Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(OrgShopVPContent.equals(ExpOrgShopVPContent))
			{
				System.out.println("Shop on Keurig.com in Organic Content in Variety Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Shop on Keurig.com in Organic Content in Variety Page is not in match with creative - Fail");
				bw.write("Shop on Keurig.com in Organic Content in Variety Page is not in match with creative - Fail"); 
			    bw.newLine();
			}	
			
			if(HexOrgShopVPColor.equals(ExpOrgShopVPColor) && OrgShopVPFontSize.equals(ExpOrgShopVPFontSize) && OrgShopVPFontFamily.equals(ExpOrgShopVPFontFamily) && OrgShopVPContent.equals(ExpOrgShopVPContent))
			{
				Label OrgShopVPCSSP = new Label(5,58,"PASS"); 
				wsheet4.addCell(OrgShopVPCSSP); 
			}
			else
			{
				Label OrgShopVPCSSF = new Label(5,58,"FAIL"); 
				wsheet4.addCell(OrgShopVPCSSF); 
			}
		
			Thread.sleep(3000);
			JavascriptExecutor jseftcss = (JavascriptExecutor)driver;
			jseftcss.executeScript("window.scrollBy(0,200)", "");
			Thread.sleep(8000);
			
			// Click Action on Sustainability Link in Footer
			
			driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[6]/div/div/div[2]/div/div[2]/div[1]/div/a[4]/div")).click();
			Thread.sleep(5000);
			
		bw.close();
		
			}
			
			catch(Exception e)
			{
				System.out.println("Exception Is "+e);
				 
			}
		
		}
		
		@Test(priority = 5)
		
		public static void SustainabilityPage() throws InterruptedException, IOException, BiffException, RowsExceededException, WriteException {
			
			try{
				
			book = Workbook.getWorkbook(newFile);
			sheet5=book.getSheet("SustainabilityPageCSS");
			sheet2=book.getSheet("ExpectedURL'S");
			//workbook = Workbook.getWorkbook(new File("D:\\Viyantha\\Automation\\Selenium Test Report\\KEURIG\\Keurig_FileInput.xls"));
			//workbookcopy = Workbook.createWorkbook(new File("D:\\Viyantha\\Automation\\Selenium Test Report\\KEURIG\\Keurig_FileOutput_" + idForTxtFile + ".xls"), workbook);
			wsheet2 = workbookcopy.getSheet(1);
			wsheet5 = workbookcopy.getSheet(4);
			
			 // Write text on  text file
			
			FileWriter fw = new FileWriter(file, true);  
			BufferedWriter bw = new BufferedWriter(fw);
			
			// Click Action on Sustainability Link in Header
			
			//driver.findElement(By.xpath(".//*[@id='id_skgm_hw_headerLayoutCell']/div[1]/div[2]/div/div[1]/a[3]/div")).click();
			//Thread.sleep(5000);
			
			Thread.sleep(3000);
			JavascriptExecutor jsespdesc = (JavascriptExecutor)driver;
			jsespdesc.executeScript("window.scrollBy(0,400)", "");
			Thread.sleep(8000);
			
			// Sustainability Page - Income Desc
			
			WebElement IncomeDescSP = driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_0']/div[1]/div[1]/div[1]/div[2]")); 
			String IncomeDescSPColor = IncomeDescSP.getCssValue("color");
			System.out.println("Actual Color of Income Desc Text in Sustainability Page is : "	+ IncomeDescSPColor );
			// Convert RGB to Hex Value
			Color HIncomeDescSPColor = Color.fromString(IncomeDescSPColor);
			String HexIncomeDescSPColor = HIncomeDescSPColor.asHex();
			System.out.println("Actual Hex Color of Income Desc Text in Sustainability Page is : "	+ HexIncomeDescSPColor );
			
			String IncomeDescSPFontSize = IncomeDescSP.getCssValue("font-size");
			System.out.println("Actual Font Size of Income Desc Text in Sustainability Page is : "	+ IncomeDescSPFontSize);
			String IncomeDescSPFontFamily = IncomeDescSP.getCssValue("font-family");
			System.out.println("Actual Font Family of Income Desc Text in Sustainability Page is : "	+ IncomeDescSPFontFamily);
			String IncomeDescSPContent = IncomeDescSP.getText();
			System.out.println("Actual Content of Income Desc Text in Sustainability Page is : "	+ IncomeDescSPContent);
					
			String ExpIncomeDescSPColor = sheet5.getCell(1,2).getContents(); //column,row
			System.out.println("Expected Color of Income Desc Text in Sustainability Page is : "	+ ExpIncomeDescSPColor );
			String ExpIncomeDescSPFontSize = sheet5.getCell(2,2).getContents();
			System.out.println("Expected Font Size of Income Desc Text in Sustainability Page is : "	+ ExpIncomeDescSPFontSize);
			String ExpIncomeDescSPFontFamily = sheet5.getCell(3,2).getContents();
			System.out.println("Expected Font Family of Income Desc Text in Sustainability Page is : "	+ ExpIncomeDescSPFontFamily);
			String ExpIncomeDescSPContent = sheet5.getCell(4,2).getContents();
			System.out.println("Expected Content of Income Desc Text in Sustainability Page is : "	+ ExpIncomeDescSPContent);
			
			if(HexIncomeDescSPColor.equals(ExpIncomeDescSPColor))
			{
				System.out.println("Income Desc Text in Sustainability Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("Income Desc Text in Sustainability Page Color is not in match with creative - Fail");
				bw.write("Income Desc Text in Sustainability Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(IncomeDescSPFontSize.equals(ExpIncomeDescSPFontSize))
			{
				System.out.println("Income Desc Text in Sustainability Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Income Desc Text in Sustainability Page Font Size is not in match with creative - Fail");
				bw.write("Income Desc Text in Sustainability Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(IncomeDescSPFontFamily.equals(ExpIncomeDescSPFontFamily))
			{
				System.out.println("Income Desc Text in Sustainability Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Income Desc Text in Sustainability Page Font Family is not in match with creative - Fail");
				bw.write("Income Desc Text in Sustainability Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(IncomeDescSPContent.equals(ExpIncomeDescSPContent))
			{
				System.out.println("Income Desc Content in Sustainability Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Income Desc Content in Sustainability Page is not in match with creative - Fail");
				bw.write("Income Desc Content in Sustainability Page is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(HexIncomeDescSPColor.equals(ExpIncomeDescSPColor) && IncomeDescSPFontSize.equals(ExpIncomeDescSPFontSize) && IncomeDescSPFontFamily.equals(ExpIncomeDescSPFontFamily) && IncomeDescSPContent.equals(ExpIncomeDescSPContent) )
			{
			    Label IncomeDescCSSP = new Label(5,2,"PASS"); 
				wsheet5.addCell(IncomeDescCSSP); 
			}
			else
			{
			    Label IncomeDescCSSF = new Label(5,2,"FAIL"); 
				wsheet5.addCell(IncomeDescCSSF); 	
			}
				
			Thread.sleep(3000);
			JavascriptExecutor jsespdesc1 = (JavascriptExecutor)driver;
			jsespdesc1.executeScript("window.scrollBy(0,500)", "");
			Thread.sleep(8000);
			
			// Sustainability Page - Income Desc1
			
			WebElement IncomeDesc1SP = driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_0']/div[1]/div[1]/div[1]/div[3]")); 
			String IncomeDesc1SPColor = IncomeDesc1SP.getCssValue("color");
			System.out.println("Actual Color of Income Desc1 Text in Sustainability Page is : "	+ IncomeDesc1SPColor );
			// Convert RGB to Hex Value
			Color HIncomeDesc1SPColor = Color.fromString(IncomeDesc1SPColor);
			String HexIncomeDesc1SPColor = HIncomeDesc1SPColor.asHex();
			System.out.println("Actual Hex Color of Income Desc1 Text in Sustainability Page is : "	+ HexIncomeDesc1SPColor );
			
			String IncomeDesc1SPFontSize = IncomeDesc1SP.getCssValue("font-size");
			System.out.println("Actual Font Size of Income Desc1 Text in Sustainability Page is : "	+ IncomeDesc1SPFontSize);
			String IncomeDesc1SPFontFamily = IncomeDesc1SP.getCssValue("font-family");
			System.out.println("Actual Font Family of Income Desc1 Text in Sustainability Page is : "	+ IncomeDesc1SPFontFamily);
			String IncomeDesc1SPContent = IncomeDesc1SP.getText();
			System.out.println("Actual Content of Income Desc1 Text in Sustainability Page is : "	+ IncomeDesc1SPContent);
					
			String ExpIncomeDesc1SPColor = sheet5.getCell(1,3).getContents(); //column,row
			System.out.println("Expected Color of Income Desc1 Text in Sustainability Page is : "	+ ExpIncomeDesc1SPColor );
			String ExpIncomeDesc1SPFontSize = sheet5.getCell(2,3).getContents();
			System.out.println("Expected Font Size of Income Desc1 Text in Sustainability Page is : "	+ ExpIncomeDesc1SPFontSize);
			String ExpIncomeDesc1SPFontFamily = sheet5.getCell(3,3).getContents();
			System.out.println("Expected Font Family of Income Desc1 Text in Sustainability Page is : "	+ ExpIncomeDesc1SPFontFamily);
			String ExpIncomeDesc1SPContent = sheet5.getCell(4,3).getContents();
			System.out.println("Expected Content of Income Desc1 Text in Sustainability Page is : "	+ ExpIncomeDesc1SPContent);
					
			if(HexIncomeDesc1SPColor.equals(ExpIncomeDesc1SPColor))
			{
				System.out.println("Income Desc1 Text in Sustainability Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("Income Desc1 Text in Sustainability Page Color is not in match with creative - Fail");
				bw.write("Income Desc1 Text in Sustainability Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(IncomeDesc1SPFontSize.equals(ExpIncomeDesc1SPFontSize))
			{
				System.out.println("Income Desc1 Text in Sustainability Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Income Desc1 Text in Sustainability Page Font Size is not in match with creative - Fail");
				bw.write("Income Desc1 Text in Sustainability Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(IncomeDesc1SPFontFamily.equals(ExpIncomeDesc1SPFontFamily))
			{
				System.out.println("Income Desc1 Text in Sustainability Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Income Desc1 Text in Sustainability Page Font Family is not in match with creative - Fail");
				bw.write("Income Desc1 Text in Sustainability Page Font Family is not in match with creative - Fail");  
			    bw.newLine();
			}
			
			if(IncomeDesc1SPContent.equals(ExpIncomeDesc1SPContent))
			{
				System.out.println("Income Desc1 Content in Sustainability Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Income Desc1 Content in Sustainability Page is not in match with creative - Fail");
				bw.write("Income Desc1 Content in Sustainability Page is not in match with creative - Fail"); 
			    bw.newLine();
			}
			
			if(HexIncomeDesc1SPColor.equals(ExpIncomeDesc1SPColor) && IncomeDesc1SPFontSize.equals(ExpIncomeDesc1SPFontSize) && IncomeDesc1SPFontFamily.equals(ExpIncomeDesc1SPFontFamily) && IncomeDesc1SPContent.equals(ExpIncomeDesc1SPContent))
			{
				Label IncomeDesc1CSSP = new Label(5,3,"PASS"); 
				wsheet5.addCell(IncomeDesc1CSSP); 
			}
			else
			{
				Label IncomeDesc1CSSF = new Label(5,3,"FAIL"); 
				wsheet5.addCell(IncomeDesc1CSSF); 
			}
			
			// Sustainability Page - Income Image1 Desc
			
			WebElement IncomeImage1SP = driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_0']/div[1]/div[1]/div[2]/div[3]")); 
		
			String IncomeImage1SPFontSize = IncomeImage1SP.getCssValue("font-size");
			System.out.println("Actual Font Size of Income Image1 Text in Sustainability Page is : "	+ IncomeImage1SPFontSize);
			String IncomeImage1SPFontFamily = IncomeImage1SP.getCssValue("font-family");
			System.out.println("Actual Font Family of Income Image1 Text in Sustainability Page is : "	+ IncomeImage1SPFontFamily);
			String IncomeImage1SPContent = IncomeImage1SP.getText();
			System.out.println("Actual Content of Income Image1 Text in Sustainability Page is : "	+ IncomeImage1SPContent);
					
			String ExpIncomeImage1SPFontSize = sheet5.getCell(2,4).getContents();
			System.out.println("Expected Font Size of Income Image1 Text in Sustainability Page is : "	+ ExpIncomeImage1SPFontSize);
			String ExpIncomeImage1SPFontFamily = sheet5.getCell(3,4).getContents();
			System.out.println("Expected Font Family of Income Image1 Text in Sustainability Page is : "	+ ExpIncomeImage1SPFontFamily);
			String ExpIncomeImage1SPContent = sheet5.getCell(4,4).getContents();
			System.out.println("Expected Content of Income Image1 Text in Sustainability Page is : "	+ ExpIncomeImage1SPContent);
			
			if(IncomeImage1SPFontSize.equals(ExpIncomeImage1SPFontSize))
			{
				System.out.println("Income Image1 Text in Sustainability Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Income Image1 Text in Sustainability Page Font Size is not in match with creative - Fail");
				bw.write("Income Image1 Text in Sustainability Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(IncomeImage1SPFontFamily.equals(ExpIncomeImage1SPFontFamily))
			{
				System.out.println("Income Image1 Text in Sustainability Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Income Image1 Text in Sustainability Page Font Family is not in match with creative - Fail");
				bw.write("Income Image1 Text in Sustainability Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(IncomeImage1SPContent.equals(ExpIncomeImage1SPContent))
			{
				System.out.println("Income Image1 Content in Sustainability Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Income Image1 Content in Sustainability Page is not in match with creative - Fail");
				bw.write("Income Image1 Content in Sustainability Page is not in match with creative - Fail"); 
			    bw.newLine();
			}
			
			if(IncomeImage1SPFontSize.equals(ExpIncomeImage1SPFontSize) && IncomeImage1SPFontFamily.equals(ExpIncomeImage1SPFontFamily) && IncomeImage1SPContent.equals(ExpIncomeImage1SPContent) )
			{
				Label IncomeImage1CSSP = new Label(5,4,"PASS"); 
				wsheet5.addCell(IncomeImage1CSSP); 
			}
			else
			{
				Label IncomeImage1CSSF = new Label(5,4,"FAIL"); 
				wsheet5.addCell(IncomeImage1CSSF); 
			}
			
			// Sustainability Page - Income - How can we improve the situation?
			
			WebElement IncomeHowSP = driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_0']/div[1]/div[2]/div[2]/div[1]")); 
			String IncomeHowSPColor = IncomeHowSP.getCssValue("color");
			System.out.println("Actual Color of Income How can we improve the situation? Text in Sustainability Page is : "	+ IncomeHowSPColor );
			// Convert RGB to Hex Value
			Color HIncomeHowSPColor = Color.fromString(IncomeHowSPColor);
			String HexIncomeHowSPColor = HIncomeHowSPColor.asHex();
			System.out.println("Actual Hex Color of Income How can we improve the situation? Text in Sustainability Page is : "	+ HexIncomeHowSPColor );
			
			String IncomeHowSPFontSize = IncomeHowSP.getCssValue("font-size");
			System.out.println("Actual Font Size of Income How can we improve the situation? Text in Sustainability Page is : "	+ IncomeHowSPFontSize);
			String IncomeHowSPFontFamily = IncomeHowSP.getCssValue("font-family");
			System.out.println("Actual Font Family of Income How can we improve the situation? Text in Sustainability Page is : "	+ IncomeHowSPFontFamily);
			String IncomeHowSPContent = IncomeHowSP.getText();
			System.out.println("Actual Content of Income How can we improve the situation? Text in Sustainability Page is : "	+ IncomeHowSPContent);
					
			String ExpIncomeHowSPColor = sheet5.getCell(1,5).getContents(); //column,row
			System.out.println("Expected Color of Income How can we improve the situation? Text in Sustainability Page is : "	+ ExpIncomeHowSPColor );
			String ExpIncomeHowSPFontSize = sheet5.getCell(2,5).getContents();
			System.out.println("Expected Font Size of Income How can we improve the situation? Text in Sustainability Page is : "	+ ExpIncomeHowSPFontSize);
			String ExpIncomeHowSPFontFamily = sheet5.getCell(3,5).getContents();
			System.out.println("Expected Font Family of Income How can we improve the situation? Text in Sustainability Page is : "	+ ExpIncomeHowSPFontFamily);
			String ExpIncomeHowSPContent = sheet5.getCell(4,5).getContents();
			System.out.println("Expected Content of Income How can we improve the situation? Text in Sustainability Page is : "	+ ExpIncomeHowSPContent);
					
			if(HexIncomeHowSPColor.equals(ExpIncomeHowSPColor))
			{
				System.out.println("Income How can we improve the situation? Text in Sustainability Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("Income How can we improve the situation? Text in Sustainability Page Color is not in match with creative - Fail");
				bw.write("Income How can we improve the situation? Text in Sustainability Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(IncomeHowSPFontSize.equals(ExpIncomeHowSPFontSize))
			{
				System.out.println("Income How can we improve the situation? Text in Sustainability Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Income How can we improve the situation? Text in Sustainability Page Font Size is not in match with creative - Fail");
				bw.write("Income How can we improve the situation? Text in Sustainability Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(IncomeHowSPFontFamily.equals(ExpIncomeHowSPFontFamily))
			{
				System.out.println("Income How can we improve the situation? Text in Sustainability Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Income How can we improve the situation? Text in Sustainability Page Font Family is not in match with creative - Fail");
				bw.write("Income How can we improve the situation? Text in Sustainability Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			
			if(IncomeHowSPContent.equals(ExpIncomeHowSPContent))
			{
				System.out.println("Income How can we improve the situation? Content in Sustainability Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Income How can we improve the situation? Content in Sustainability Page is not in match with creative - Fail");
				bw.write("Income How can we improve the situation? Content in Sustainability Page is not in match with creative - Fail"); 
			    bw.newLine();
			}
			
			if(HexIncomeHowSPColor.equals(ExpIncomeHowSPColor) && IncomeHowSPFontSize.equals(ExpIncomeHowSPFontSize) && IncomeHowSPFontFamily.equals(ExpIncomeHowSPFontFamily) && IncomeHowSPContent.equals(ExpIncomeHowSPContent) )
			{
				Label IncomeHowCSSP = new Label(5,5,"PASS"); 
				wsheet5.addCell(IncomeHowCSSP); 
			}
			else
			{
				Label IncomeHowCSSF = new Label(5,5,"FAIL"); 
				wsheet5.addCell(IncomeHowCSSF); 
			}
			
			// Sustainability Page - Income - How can we improve the situation? - Desc
			
			WebElement IncomeHowDescSP = driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_0']/div[1]/div[2]/div[2]/div[2]")); 
			String IncomeHowDescSPColor = IncomeHowDescSP.getCssValue("color");
			System.out.println("Actual Color of Income How can we improve the situation? Desc in Sustainability Page is : "	+ IncomeHowDescSPColor );
			// Convert RGB to Hex Value
			Color HIncomeHowDescSPColor = Color.fromString(IncomeHowDescSPColor);
			String HexIncomeHowDescSPColor = HIncomeHowDescSPColor.asHex();
			System.out.println("Actual Hex Color of Income How can we improve the situation? Desc in Sustainability Page is : "	+ HexIncomeHowDescSPColor );
			
			String IncomeHowDescSPFontSize = IncomeHowDescSP.getCssValue("font-size");
			System.out.println("Actual Font Size of Income How can we improve the situation? Desc in Sustainability Page is : "	+ IncomeHowDescSPFontSize);
			String IncomeHowDescSPFontFamily = IncomeHowDescSP.getCssValue("font-family");
			System.out.println("Actual Font Family of Income How can we improve the situation? Desc in Sustainability Page is : "	+ IncomeHowDescSPFontFamily);
			String IncomeHowDescSPContent = IncomeHowDescSP.getText();
			System.out.println("Actual Content of Income How can we improve the situation? Desc in Sustainability Page is : "	+ IncomeHowDescSPContent);
					
			String ExpIncomeHowDescSPColor = sheet5.getCell(1,6).getContents(); //column,row
			System.out.println("Expected Color of Income How can we improve the situation? Desc in Sustainability Page is : "	+ ExpIncomeHowDescSPColor );
			String ExpIncomeHowDescSPFontSize = sheet5.getCell(2,6).getContents();
			System.out.println("Expected Font Size of Income How can we improve the situation? Desc in Sustainability Page is : "	+ ExpIncomeHowDescSPFontSize);
			String ExpIncomeHowDescSPFontFamily = sheet5.getCell(3,6).getContents();
			System.out.println("Expected Font Family of Income How can we improve the situation? Desc in Sustainability Page is : "	+ ExpIncomeHowDescSPFontFamily);
			String ExpIncomeHowDescSPContent = sheet5.getCell(4,6).getContents();
			System.out.println("Expected Content of Income How can we improve the situation? Desc in Sustainability Page is : "	+ ExpIncomeHowDescSPContent);
					
			if(HexIncomeHowDescSPColor.equals(ExpIncomeHowDescSPColor))
			{
				System.out.println("Income How can we improve the situation? Desc in Sustainability Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("Income How can we improve the situation? Desc in Sustainability Page Color is not in match with creative - Fail");
				bw.write("Income How can we improve the situation? Desc in Sustainability Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(IncomeHowDescSPFontSize.equals(ExpIncomeHowDescSPFontSize))
			{
				System.out.println("Income How can we improve the situation? Desc in Sustainability Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Income How can we improve the situation? Desc in Sustainability Page Font Size is not in match with creative - Fail");
				bw.write("Income How can we improve the situation? Desc in Sustainability Page Font Size is not in match with creative - Fail");  
			    bw.newLine();
			}
					
			if(IncomeHowDescSPFontFamily.equals(ExpIncomeHowDescSPFontFamily))
			{
				System.out.println("Income How can we improve the situation? Desc in Sustainability Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Income How can we improve the situation? Desc in Sustainability Page Font Family is not in match with creative - Fail");
				bw.write("Income How can we improve the situation? Desc in Sustainability Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(IncomeHowDescSPContent.equals(ExpIncomeHowDescSPContent))
			{
				System.out.println("Income How can we improve the situation? Desc Content in Sustainability Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Income How can we improve the situation? Desc Content in Sustainability Page is not in match with creative - Fail");
				bw.write("Income How can we improve the situation? Desc Content in Sustainability Page is not in match with creative - Fail"); 
			    bw.newLine();
			}
			
			if(HexIncomeHowDescSPColor.equals(ExpIncomeHowDescSPColor) && IncomeHowDescSPFontSize.equals(ExpIncomeHowDescSPFontSize) && IncomeHowDescSPFontFamily.equals(ExpIncomeHowDescSPFontFamily) && IncomeHowDescSPContent.equals(ExpIncomeHowDescSPContent))
			{
				Label IncomeHowDescCSSP = new Label(5,6,"PASS"); 
				wsheet5.addCell(IncomeHowDescCSSP); 
			}
			else
			{
				Label IncomeHowDescCSSF = new Label(5,6,"FAIL"); 
				wsheet5.addCell(IncomeHowDescCSSF); 
			}
			
			Thread.sleep(3000);
			JavascriptExecutor jsespbetter = (JavascriptExecutor)driver;
			jsespbetter.executeScript("window.scrollBy(0,700)", "");
			Thread.sleep(8000);
		
			// Sustainability Page - Income - What�s changed for the better?
			
			WebElement IncomeWhatsSP = driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_0']/div[1]/div[3]/div[1]/div[1]")); 
			String IncomeWhatsSPColor = IncomeWhatsSP.getCssValue("color");
			System.out.println("Actual Color of Income What�s changed for the better? Text in Sustainability Page is : "	+ IncomeWhatsSPColor );
			// Convert RGB to Hex Value
			Color HIncomeWhatsSPColor = Color.fromString(IncomeWhatsSPColor);
			String HexIncomeWhatsSPColor = HIncomeWhatsSPColor.asHex();
			System.out.println("Actual Hex Color of Income What�s changed for the better? Text in Sustainability Page is : "	+ HexIncomeWhatsSPColor );
			
			String IncomeWhatsSPFontSize = IncomeWhatsSP.getCssValue("font-size");
			System.out.println("Actual Font Size of Income What�s changed for the better? Text in Sustainability Page is : "	+ IncomeWhatsSPFontSize);
			String IncomeWhatsSPFontFamily = IncomeWhatsSP.getCssValue("font-family");
			System.out.println("Actual Font Family of Income What�s changed for the better? Text in Sustainability Page is : "	+ IncomeWhatsSPFontFamily);
			String IncomeWhatsSPContent = IncomeWhatsSP.getText();
			System.out.println("Actual Content of Income What�s changed for the better? Text in Sustainability Page is : "	+ IncomeWhatsSPContent);
					
			String ExpIncomeWhatsSPColor = sheet5.getCell(1,7).getContents(); //column,row
			System.out.println("Expected Color of Income What�s changed for the better? Text in Sustainability Page is : "	+ ExpIncomeWhatsSPColor );
			String ExpIncomeWhatsSPFontSize = sheet5.getCell(2,7).getContents();
			System.out.println("Expected Font Size of Income What�s changed for the better? Text in Sustainability Page is : "	+ ExpIncomeWhatsSPFontSize);
			String ExpIncomeWhatsSPFontFamily = sheet5.getCell(3,7).getContents();
			System.out.println("Expected Font Family of Income What�s changed for the better? Text in Sustainability Page is : "	+ ExpIncomeWhatsSPFontFamily);
			String ExpIncomeWhatsSPContent = sheet5.getCell(4,7).getContents();
			System.out.println("Expected Content of Income What�s changed for the better? Text in Sustainability Page is : "	+ ExpIncomeWhatsSPContent);
					
			if(HexIncomeWhatsSPColor.equals(ExpIncomeWhatsSPColor))
			{
				System.out.println("Income What�s changed for the better? Text in Sustainability Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("Income What�s changed for the better? Text in Sustainability Page Color is not in match with creative - Fail");
				bw.write("Income What�s changed for the better? Text in Sustainability Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(IncomeWhatsSPFontSize.equals(ExpIncomeWhatsSPFontSize))
			{
				System.out.println("Income What�s changed for the better? Text in Sustainability Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Income What�s changed for the better? Text in Sustainability Page Font Size is not in match with creative - Fail");
				bw.write("Income What�s changed for the better? Text in Sustainability Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(IncomeWhatsSPFontFamily.equals(ExpIncomeWhatsSPFontFamily))
			{
				System.out.println("Income What�s changed for the better? Text in Sustainability Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Income What�s changed for the better? Text in Sustainability Page Font Family is not in match with creative - Fail");
				bw.write("Income What�s changed for the better? Text in Sustainability Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(IncomeWhatsSPContent.equals(ExpIncomeWhatsSPContent))
			{
				System.out.println("Income What�s changed for the better? Content in Sustainability Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Income What�s changed for the better? Content in Sustainability Page is not in match with creative - Fail");
				bw.write("Income What�s changed for the better? Content in Sustainability Page is not in match with creative - Fail"); 
			    bw.newLine();
			}
			
			if(HexIncomeWhatsSPColor.equals(ExpIncomeWhatsSPColor) && IncomeWhatsSPFontSize.equals(ExpIncomeWhatsSPFontSize) && IncomeWhatsSPFontFamily.equals(ExpIncomeWhatsSPFontFamily) && IncomeWhatsSPContent.equals(ExpIncomeWhatsSPContent))
			{
				Label IncomeWhatsCSSP = new Label(5,7,"PASS"); 
				wsheet5.addCell(IncomeWhatsCSSP); 
			}
			else
			{
				Label IncomeWhatsCSSF = new Label(5,7,"FAIL"); 
				wsheet5.addCell(IncomeWhatsCSSF); 
			}
			
			// Sustainability Page - Income - What�s changed for the better? - Desc
			
			WebElement IncomeWhatsDescSP = driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_0']/div[1]/div[3]/div[1]/div[2]")); 
			String IncomeWhatsDescSPColor = IncomeWhatsDescSP.getCssValue("color");
			System.out.println("Actual Color of Income What�s changed for the better? Desc in Sustainability Page is : "	+ IncomeWhatsDescSPColor );
			// Convert RGB to Hex Value
			Color HIncomeWhatsDescSPColor = Color.fromString(IncomeWhatsDescSPColor);
			String HexIncomeWhatsDescSPColor = HIncomeWhatsDescSPColor.asHex();
			System.out.println("Actual Hex Color of Income What�s changed for the better? Desc in Sustainability Page is : "	+ HexIncomeWhatsDescSPColor );
			
			String IncomeWhatsDescSPFontSize = IncomeWhatsDescSP.getCssValue("font-size");
			System.out.println("Actual Font Size of Income What�s changed for the better? Desc in Sustainability Page is : "	+ IncomeWhatsDescSPFontSize);
			String IncomeWhatsDescSPFontFamily = IncomeWhatsDescSP.getCssValue("font-family");
			System.out.println("Actual Font Family of Income What�s changed for the better? Desc in Sustainability Page is : "	+ IncomeWhatsDescSPFontFamily);
			String IncomeWhatsDescSPContent = IncomeWhatsDescSP.getText();
			System.out.println("Actual Content of Income What�s changed for the better? Desc in Sustainability Page is : "	+ IncomeWhatsDescSPContent);
					
			String ExpIncomeWhatsDescSPColor = sheet5.getCell(1,8).getContents(); //column,row
			System.out.println("Expected Color of Income What�s changed for the better? Desc in Sustainability Page is : "	+ ExpIncomeWhatsDescSPColor );
			String ExpIncomeWhatsDescSPFontSize = sheet5.getCell(2,8).getContents();
			System.out.println("Expected Font Size of Income What�s changed for the better? Desc in Sustainability Page is : "	+ ExpIncomeWhatsDescSPFontSize);
			String ExpIncomeWhatsDescSPFontFamily = sheet5.getCell(3,8).getContents();
			System.out.println("Expected Font Family of Income What�s changed for the better? Desc in Sustainability Page is : "	+ ExpIncomeWhatsDescSPFontFamily);
			String ExpIncomeWhatsDescSPContent = sheet5.getCell(4,8).getContents();
			System.out.println("Expected Content of Income What�s changed for the better? Desc in Sustainability Page is : "	+ ExpIncomeWhatsDescSPContent);
					
			if(HexIncomeWhatsDescSPColor.equals(ExpIncomeWhatsDescSPColor))
			{
				System.out.println("Income What�s changed for the better? Desc in Sustainability Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("Income What�s changed for the better? Desc in Sustainability Page Color is not in match with creative - Fail");
				bw.write("Income What�s changed for the better? Desc in Sustainability Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(IncomeWhatsDescSPFontSize.equals(ExpIncomeWhatsDescSPFontSize))
			{
				System.out.println("Income What�s changed for the better? Desc in Sustainability Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Income What�s changed for the better? Desc in Sustainability Page Font Size is not in match with creative - Fail");
				bw.write("Income What�s changed for the better? Desc in Sustainability Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(IncomeWhatsDescSPFontFamily.equals(ExpIncomeWhatsDescSPFontFamily))
			{
				System.out.println("Income What�s changed for the better? Desc in Sustainability Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Income What�s changed for the better? Desc in Sustainability Page Font Family is not in match with creative - Fail");
				bw.write("Income What�s changed for the better? Desc in Sustainability Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(IncomeWhatsDescSPContent.equals(ExpIncomeWhatsDescSPContent))
			{
				System.out.println("Income What�s changed for the better? Desc Content in Sustainability Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Income What�s changed for the better? Desc Content in Sustainability Page is not in match with creative - Fail");
				bw.write("Income What�s changed for the better? Desc Content in Sustainability Page is not in match with creative - Fail"); 
			    bw.newLine();
			}
			
			if(HexIncomeWhatsDescSPColor.equals(ExpIncomeWhatsDescSPColor) && IncomeWhatsDescSPFontSize.equals(ExpIncomeWhatsDescSPFontSize) && IncomeWhatsDescSPFontFamily.equals(ExpIncomeWhatsDescSPFontFamily) && IncomeWhatsDescSPContent.equals(ExpIncomeWhatsDescSPContent))
			{
				Label IncomeWhatsDescCSSP = new Label(5,8,"PASS"); 
				wsheet5.addCell(IncomeWhatsDescCSSP); 
			}
			else
			{
				Label IncomeWhatsDescCSSF = new Label(5,8,"FAIL"); 
				wsheet5.addCell(IncomeWhatsDescCSSF); 
			}
			
			Thread.sleep(3000);
			JavascriptExecutor jsespft = (JavascriptExecutor)driver;
			jsespft.executeScript("window.scrollBy(0,400)", "");
			Thread.sleep(8000);
		
			// Sustainability Page - Income - READ THE SUSTAINABILITY REPORT
			
			WebElement IncomeReadSP = driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_0']/div[2]/div[1]/a")); 
			String IncomeReadSPColor = IncomeReadSP.getCssValue("color");
			System.out.println("Actual Color of Income READ THE SUSTAINABILITY REPORT Text in Sustainability Page is : "	+ IncomeReadSPColor );
			// Convert RGB to Hex Value
			Color HIncomeReadSPColor = Color.fromString(IncomeReadSPColor);
			String HexIncomeReadSPColor = HIncomeReadSPColor.asHex();
			System.out.println("Actual Hex Color of Income READ THE SUSTAINABILITY REPORT Text in Sustainability Page is : "	+ HexIncomeReadSPColor );
			
			String IncomeReadSPFontSize = IncomeReadSP.getCssValue("font-size");
			System.out.println("Actual Font Size of Income READ THE SUSTAINABILITY REPORT Text in Sustainability Page is : "	+ IncomeReadSPFontSize);
			String IncomeReadSPFontFamily = IncomeReadSP.getCssValue("font-family");
			System.out.println("Actual Font Family of Income READ THE SUSTAINABILITY REPORT Text in Sustainability Page is : "	+ IncomeReadSPFontFamily);
			String IncomeReadSPContent = IncomeReadSP.getText();
			System.out.println("Actual Content of Income READ THE SUSTAINABILITY REPORT Text in Sustainability Page is : "	+ IncomeReadSPContent);
					
			String ExpIncomeReadSPColor = sheet5.getCell(1,9).getContents(); //column,row
			System.out.println("Expected Color of Income READ THE SUSTAINABILITY REPORT Text in Sustainability Page is : "	+ ExpIncomeReadSPColor );
			String ExpIncomeReadSPFontSize = sheet5.getCell(2,9).getContents();
			System.out.println("Expected Font Size of Income READ THE SUSTAINABILITY REPORT Text in Sustainability Page is : "	+ ExpIncomeReadSPFontSize);
			String ExpIncomeReadSPFontFamily = sheet5.getCell(3,9).getContents();
			System.out.println("Expected Font Family of Income READ THE SUSTAINABILITY REPORT Text in Sustainability Page is : "	+ ExpIncomeReadSPFontFamily);
			String ExpIncomeReadSPContent = sheet5.getCell(4,9).getContents();
			System.out.println("Expected Content of Income READ THE SUSTAINABILITY REPORT Text in Sustainability Page is : "	+ ExpIncomeReadSPContent);
					
			if(HexIncomeReadSPColor.equals(ExpIncomeReadSPColor))
			{
				System.out.println("Income READ THE SUSTAINABILITY REPORT Text in Sustainability Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("Income READ THE SUSTAINABILITY REPORT Text in Sustainability Page Color is not in match with creative - Fail");
				bw.write("Income READ THE SUSTAINABILITY REPORT Text in Sustainability Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(IncomeReadSPFontSize.equals(ExpIncomeReadSPFontSize))
			{
				System.out.println("Income READ THE SUSTAINABILITY REPORT Text in Sustainability Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Income READ THE SUSTAINABILITY REPORT Text in Sustainability Page Font Size is not in match with creative - Fail");
				bw.write("Income READ THE SUSTAINABILITY REPORT Text in Sustainability Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(IncomeReadSPFontFamily.equals(ExpIncomeReadSPFontFamily))
			{
				System.out.println("Income READ THE SUSTAINABILITY REPORT Text in Sustainability Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Income READ THE SUSTAINABILITY REPORT Text in Sustainability Page Font Family is not in match with creative - Fail");
				bw.write("Income READ THE SUSTAINABILITY REPORT Text in Sustainability Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(IncomeReadSPContent.equals(ExpIncomeReadSPContent))
			{
				System.out.println("Income READ THE SUSTAINABILITY REPORT Content in Sustainability Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Income READ THE SUSTAINABILITY REPORT Content in Sustainability Page is not in match with creative - Fail");
				bw.write("Income READ THE SUSTAINABILITY REPORT Content in Sustainability Page is not in match with creative - Fail"); 
			    bw.newLine();
			}
			
			if(HexIncomeReadSPColor.equals(ExpIncomeReadSPColor) && IncomeReadSPFontSize.equals(ExpIncomeReadSPFontSize) && IncomeReadSPFontFamily.equals(ExpIncomeReadSPFontFamily) && IncomeReadSPContent.equals(ExpIncomeReadSPContent))
			{
				Label IncomeReadCSSP = new Label(5,9,"PASS"); 
				wsheet5.addCell(IncomeReadCSSP); 
			}
			else
			{
				Label IncomeReadCSSF = new Label(5,9,"FAIL"); 
				wsheet5.addCell(IncomeReadCSSF); 
			}
			
			// Sustainability Page - Income - LEARN ABOUT FOOD SUPPORT
			
			WebElement IncomeLearnSP = driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_0']/div[2]/div[2]/span[1]")); 
			String IncomeLearnSPColor = IncomeLearnSP.getCssValue("color");
			System.out.println("Actual Color of Income LEARN ABOUT FOOD SUPPORT Text in Sustainability Page is : "	+ IncomeLearnSPColor );
			// Convert RGB to Hex Value
			Color HIncomeLearnSPColor = Color.fromString(IncomeLearnSPColor);
			String HexIncomeLearnSPColor = HIncomeLearnSPColor.asHex();
			System.out.println("Actual Hex Color of Income LEARN ABOUT FOOD SUPPORT Text in Sustainability Page is : "	+ HexIncomeLearnSPColor );
			
			String IncomeLearnSPFontSize = IncomeLearnSP.getCssValue("font-size");
			System.out.println("Actual Font Size of Income LEARN ABOUT FOOD SUPPORT Text in Sustainability Page is : "	+ IncomeLearnSPFontSize);
			String IncomeLearnSPFontFamily = IncomeLearnSP.getCssValue("font-family");
			System.out.println("Actual Font Family of Income LEARN ABOUT FOOD SUPPORT Text in Sustainability Page is : "	+ IncomeLearnSPFontFamily);
			String IncomeLearnSPContent = IncomeLearnSP.getText();
			System.out.println("Actual Content of Income LEARN ABOUT FOOD SUPPORT Text in Sustainability Page is : "	+ IncomeLearnSPContent);
					
			String ExpIncomeLearnSPColor = sheet5.getCell(1,10).getContents(); //column,row
			System.out.println("Expected Color of Income LEARN ABOUT FOOD SUPPORT Text in Sustainability Page is : "	+ ExpIncomeLearnSPColor );
			String ExpIncomeLearnSPFontSize = sheet5.getCell(2,10).getContents();
			System.out.println("Expected Font Size of Income LEARN ABOUT FOOD SUPPORT Text in Sustainability Page is : "	+ ExpIncomeLearnSPFontSize);
			String ExpIncomeLearnSPFontFamily = sheet5.getCell(3,10).getContents();
			System.out.println("Expected Font Family of Income LEARN ABOUT FOOD SUPPORT Text in Sustainability Page is : "	+ ExpIncomeLearnSPFontFamily);
			String ExpIncomeLearnSPContent = sheet5.getCell(4,10).getContents();
			System.out.println("Expected Content of Income LEARN ABOUT FOOD SUPPORT Text in Sustainability Page is : "	+ ExpIncomeLearnSPContent);
					
			if(HexIncomeLearnSPColor.equals(ExpIncomeLearnSPColor))
			{
				System.out.println("Income LEARN ABOUT FOOD SUPPORT Text in Sustainability Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("Income LEARN ABOUT FOOD SUPPORT Text in Sustainability Page Color is not in match with creative - Fail");
				bw.write("Income LEARN ABOUT FOOD SUPPORT Text in Sustainability Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(IncomeLearnSPFontSize.equals(ExpIncomeLearnSPFontSize))
			{
				System.out.println("Income LEARN ABOUT FOOD SUPPORT Text in Sustainability Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Income LEARN ABOUT FOOD SUPPORT Text in Sustainability Page Font Size is not in match with creative - Fail");
				bw.write("Income LEARN ABOUT FOOD SUPPORT Text in Sustainability Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(IncomeLearnSPFontFamily.equals(ExpIncomeLearnSPFontFamily))
			{
				System.out.println("Income LEARN ABOUT FOOD SUPPORT Text in Sustainability Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Income LEARN ABOUT FOOD SUPPORT Text in Sustainability Page Font Family is not in match with creative - Fail");
				bw.write("Income LEARN ABOUT FOOD SUPPORT Text in Sustainability Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			
			if(IncomeLearnSPContent.equals(ExpIncomeLearnSPContent))
			{
				System.out.println("Income LEARN ABOUT FOOD SUPPORT Content in Sustainability Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Income LEARN ABOUT FOOD SUPPORT Content in Sustainability Page is not in match with creative - Fail");
				bw.write("Income LEARN ABOUT FOOD SUPPORT Content in Sustainability Page is not in match with creative - Fail"); 
			    bw.newLine();
			}
			
			if(HexIncomeLearnSPColor.equals(ExpIncomeLearnSPColor) && IncomeLearnSPFontSize.equals(ExpIncomeLearnSPFontSize) && IncomeLearnSPFontFamily.equals(ExpIncomeLearnSPFontFamily) && IncomeLearnSPContent.equals(ExpIncomeLearnSPContent))
			{
				Label IncomeLearnCSSP = new Label(5,10,"PASS"); 
				wsheet5.addCell(IncomeLearnCSSP); 
			}
			else
			{
				Label IncomeLearnCSSF = new Label(5,10,"FAIL"); 
				wsheet5.addCell(IncomeLearnCSSF); 
			}
			
			// Sustainability Page - Income - READ THE SUSTAINABILITY REPORT - Click
			
			String winHandleBeforeIR = driver.getWindowHandle();
			driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_0']/div[2]/div[1]/a")).click(); 
			Thread.sleep(5000);
			//Get the list of window handles
			ArrayList<String> tabsIR = new ArrayList<String>(driver.getWindowHandles());
			System.out.println(tabsIR.size());
			//Use the list of window handles to switch between windows
		    driver.switchTo().window((String) tabsIR.get(1));
			Thread.sleep(10000);
			String AIReadurl = driver.getCurrentUrl();
			System.out.println("Actual Income - READ THE SUSTAINABILITY REPORT URL is: "+AIReadurl);
			String EIReadurl = sheet2.getCell(1,42).getContents();
			System.out.println("Expected Income - READ THE SUSTAINABILITY REPORT URL is: "+EIReadurl);
			if(EIReadurl.equals(AIReadurl))
			{
				System.out.println("Income - READ THE SUSTAINABILITY REPORT Page is Opened Correctly - Pass");
				Label IncomeReadNAVP = new Label(2,42,"PASS"); 
				wsheet2.addCell(IncomeReadNAVP); 
			}
			else
			{
				System.out.println("Income - READ THE SUSTAINABILITY REPORT Page is not Opened Correctly - Fail");
				bw.write("Income - READ THE SUSTAINABILITY REPORT Page is not Opened Correctly - Fail"); 
			    bw.newLine();
			    Label IncomeReadNAVF = new Label(2,42,"FAIL"); 
				wsheet2.addCell(IncomeReadNAVF); 
			}
			driver.close();
			Thread.sleep(5000);
			driver.switchTo().window(winHandleBeforeIR); // Home Window
			
			// Sustainability Page - Income - LEARN ABOUT FOOD SUPPORT - Click
			
			driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_0']/div[2]/div[2]/span[1]")).click(); 
			Thread.sleep(10000);
			String AILearnurl = driver.getCurrentUrl();
			System.out.println("Actual Income - LEARN ABOUT FOOD SUPPORT URL is: "+AILearnurl);
			String EILearnurl = sheet2.getCell(1,43).getContents();
			System.out.println("Expected Income - LEARN ABOUT FOOD SUPPORT URL is: "+EILearnurl);
			if(EILearnurl.equals(AILearnurl))
			{
				System.out.println("Income - LEARN ABOUT FOOD SUPPORT Page is Opened Correctly - Pass");
				Label IncomeLearnNAVP = new Label(2,43,"PASS"); 
				wsheet2.addCell(IncomeLearnNAVP); 
			}
			else
			{
				System.out.println("Income - LEARN ABOUT FOOD SUPPORT is not Opened Correctly - Fail");
				bw.write("Income - LEARN ABOUT FOOD SUPPORT is not Opened Correctly - Fail"); 
			    bw.newLine();
			    Label IncomeLearnNAVF = new Label(2,43,"FAIL"); 
				wsheet2.addCell(IncomeLearnNAVF); 
			}
			
			// FOOD TAB
			
			Thread.sleep(3000);
			JavascriptExecutor jsespfooddesc = (JavascriptExecutor)driver;
			jsespfooddesc.executeScript("window.scrollBy(0,400)", "");
			Thread.sleep(8000);
			
			// Sustainability Page - FOOD Desc
			
			WebElement FoodDescSP = driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_1']/div[1]/div[1]/div[1]/div[2]")); 
			String FoodDescSPColor = FoodDescSP.getCssValue("color");
			System.out.println("Actual Color of FOOD Desc Text in Sustainability Page is : "	+ FoodDescSPColor );
			// Convert RGB to Hex Value
			Color HFoodDescSPColor = Color.fromString(FoodDescSPColor);
			String HexFoodDescSPColor = HFoodDescSPColor.asHex();
			System.out.println("Actual Hex Color of FOOD Desc Text in Sustainability Page is : "	+ HexFoodDescSPColor );
			
			String FoodDescSPFontSize = FoodDescSP.getCssValue("font-size");
			System.out.println("Actual Font Size of FOOD Desc Text in Sustainability Page is : "	+ FoodDescSPFontSize);
			String FoodDescSPFontFamily = FoodDescSP.getCssValue("font-family");
			System.out.println("Actual Font Family of FOOD Desc Text in Sustainability Page is : "	+ FoodDescSPFontFamily);
			String FoodDescSPContent = FoodDescSP.getText();
			System.out.println("Actual Content of FOOD Desc Text in Sustainability Page is : "	+ FoodDescSPContent);
					
			String ExpFoodDescSPColor = sheet5.getCell(1,12).getContents(); //column,row
			System.out.println("Expected Color of FOOD Desc Text in Sustainability Page is : "	+ ExpFoodDescSPColor );
			String ExpFoodDescSPFontSize = sheet5.getCell(2,12).getContents();
			System.out.println("Expected Font Size of FOOD Desc Text in Sustainability Page is : "	+ ExpFoodDescSPFontSize);
			String ExpFoodDescSPFontFamily = sheet5.getCell(3,12).getContents();
			System.out.println("Expected Font Family of FOOD Desc Text in Sustainability Page is : "	+ ExpFoodDescSPFontFamily);
			String ExpFoodDescSPContent = sheet5.getCell(4,12).getContents();
			System.out.println("Expected Content of FOOD Desc Text in Sustainability Page is : "	+ ExpFoodDescSPContent);
			
			if(HexFoodDescSPColor.equals(ExpFoodDescSPColor))
			{
				System.out.println("FOOD Desc Text in Sustainability Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("FOOD Desc Text in Sustainability Page Color is not in match with creative - Fail");
				bw.write("FOOD Desc Text in Sustainability Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(FoodDescSPFontSize.equals(ExpFoodDescSPFontSize))
			{
				System.out.println("FOOD Desc Text in Sustainability Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("FOOD Desc Text in Sustainability Page Font Size is not in match with creative - Fail");
				bw.write("FOOD Desc Text in Sustainability Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(FoodDescSPFontFamily.equals(ExpFoodDescSPFontFamily))
			{
				System.out.println("FOOD Desc Text in Sustainability Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("FOOD Desc Text in Sustainability Page Font Family is not in match with creative - Fail");
				bw.write("FOOD Desc Text in Sustainability Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(FoodDescSPContent.equals(ExpFoodDescSPContent))
			{
				System.out.println("FOOD Desc Content in Sustainability Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("FOOD Desc Content in Sustainability Page is not in match with creative - Fail");
				bw.write("FOOD Desc Content in Sustainability Page is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(HexFoodDescSPColor.equals(ExpFoodDescSPColor) && FoodDescSPFontSize.equals(ExpFoodDescSPFontSize) && FoodDescSPFontFamily.equals(ExpFoodDescSPFontFamily) && FoodDescSPContent.equals(ExpFoodDescSPContent))
			{
			    Label FoodDescCSSP = new Label(5,12,"PASS"); 
				wsheet5.addCell(FoodDescCSSP); 
			}
			else
			{
			    Label FoodDescCSSF = new Label(5,12,"FAIL"); 
				wsheet5.addCell(FoodDescCSSF); 	
			}
			
			// Sustainability Page - FOOD Desc1
			
			WebElement FoodDesc1SP = driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_1']/div[1]/div[1]/div[1]/div[3]")); 
			String FoodDesc1SPColor = FoodDesc1SP.getCssValue("color");
			System.out.println("Actual Color of FOOD Desc1 Text in Sustainability Page is : "	+ FoodDesc1SPColor );
			// Convert RGB to Hex Value
			Color HFoodDesc1SPColor = Color.fromString(FoodDesc1SPColor);
			String HexFoodDesc1SPColor = HFoodDesc1SPColor.asHex();
			System.out.println("Actual Hex Color of FOOD Desc1 Text in Sustainability Page is : "	+ HexFoodDesc1SPColor );
			
			String FoodDesc1SPFontSize = FoodDesc1SP.getCssValue("font-size");
			System.out.println("Actual Font Size of FOOD Desc1 Text in Sustainability Page is : "	+ FoodDesc1SPFontSize);
			String FoodDesc1SPFontFamily = FoodDesc1SP.getCssValue("font-family");
			System.out.println("Actual Font Family of FOOD Desc1 Text in Sustainability Page is : "	+ FoodDesc1SPFontFamily);
			String FoodDesc1SPContent = FoodDesc1SP.getText();
			System.out.println("Actual Content of FOOD Desc1 Text in Sustainability Page is : "	+ FoodDesc1SPContent);
					
			String ExpFoodDesc1SPColor = sheet5.getCell(1,13).getContents(); //column,row
			System.out.println("Expected Color of FOOD Desc1 Text in Sustainability Page is : "	+ ExpFoodDesc1SPColor );
			String ExpFoodDesc1SPFontSize = sheet5.getCell(2,13).getContents();
			System.out.println("Expected Font Size of FOOD Desc1 Text in Sustainability Page is : "	+ ExpFoodDesc1SPFontSize);
			String ExpFoodDesc1SPFontFamily = sheet5.getCell(3,13).getContents();
			System.out.println("Expected Font Family of FOOD Desc1 Text in Sustainability Page is : "	+ ExpFoodDesc1SPFontFamily);
			String ExpFoodDesc1SPContent = sheet5.getCell(4,13).getContents();
			System.out.println("Expected Content of FOOD Desc1 Text in Sustainability Page is : "	+ ExpFoodDesc1SPContent);
			
			if(HexFoodDesc1SPColor.equals(ExpFoodDesc1SPColor))
			{
				System.out.println("FOOD Desc1 Text in Sustainability Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("FOOD Desc1 Text in Sustainability Page Color is not in match with creative - Fail");
				bw.write("FOOD Desc1 Text in Sustainability Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(FoodDesc1SPFontSize.equals(ExpFoodDesc1SPFontSize))
			{
				System.out.println("FOOD Desc1 Text in Sustainability Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("FOOD Desc1 Text in Sustainability Page Font Size is not in match with creative - Fail");
				bw.write("FOOD Desc1 Text in Sustainability Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(FoodDesc1SPFontFamily.equals(ExpFoodDesc1SPFontFamily))
			{
				System.out.println("FOOD Desc1 Text in Sustainability Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("FOOD Desc1 Text in Sustainability Page Font Family is not in match with creative - Fail");
				bw.write("FOOD Desc1 Text in Sustainability Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(FoodDesc1SPContent.equals(ExpFoodDesc1SPContent))
			{
				System.out.println("FOOD Desc1 Content in Sustainability Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("FOOD Desc1 Content in Sustainability Page is not in match with creative - Fail");
				bw.write("FOOD Desc1 Content in Sustainability Page is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(HexFoodDesc1SPColor.equals(ExpFoodDesc1SPColor) && FoodDesc1SPFontSize.equals(ExpFoodDesc1SPFontSize) && FoodDesc1SPFontFamily.equals(ExpFoodDesc1SPFontFamily) && FoodDesc1SPContent.equals(ExpFoodDesc1SPContent))
			{
			    Label FoodDesc1CSSP = new Label(5,13,"PASS"); 
				wsheet5.addCell(FoodDesc1CSSP); 
			}
			else
			{
			    Label FoodDesc1CSSF = new Label(5,13,"FAIL"); 
				wsheet5.addCell(FoodDesc1CSSF); 	
			}
			
			// Sustainability Page - FOOD Image1 Desc
			
			WebElement FoodImage1SP = driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_1']/div[1]/div[1]/div[2]/div[3]")); 
			
			String FoodImage1SPFontSize = FoodImage1SP.getCssValue("font-size");
			System.out.println("Actual Font Size of FOOD Image1 Text in Sustainability Page is : "	+ FoodImage1SPFontSize);
			String FoodImage1SPFontFamily = FoodImage1SP.getCssValue("font-family");
			System.out.println("Actual Font Family of FOOD Image1 Text in Sustainability Page is : "	+ FoodImage1SPFontFamily);
			String FoodImage1SPContent = FoodImage1SP.getText();
			System.out.println("Actual Content of FOOD Image1 Text in Sustainability Page is : "	+ FoodImage1SPContent);
				
			String ExpFoodImage1SPFontSize = sheet5.getCell(2,14).getContents();
			System.out.println("Expected Font Size of FOOD Image1 Text in Sustainability Page is : "	+ ExpFoodImage1SPFontSize);
			String ExpFoodImage1SPFontFamily = sheet5.getCell(3,14).getContents();
			System.out.println("Expected Font Family of FOOD Image1 Text in Sustainability Page is : "	+ ExpFoodImage1SPFontFamily);
			String ExpFoodImage1SPContent = sheet5.getCell(4,14).getContents();
			System.out.println("Expected Content of FOOD Image1 Text in Sustainability Page is : "	+ ExpFoodImage1SPContent);
				
			if(FoodImage1SPFontSize.equals(ExpFoodImage1SPFontSize))
			{
				System.out.println("FOOD Image1 Text in Sustainability Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("FOOD Image1 Text in Sustainability Page Font Size is not in match with creative - Fail");
				bw.write("FOOD Image1 Text in Sustainability Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(FoodImage1SPFontFamily.equals(ExpFoodImage1SPFontFamily))
			{
				System.out.println("FOOD Image1 Text in Sustainability Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("FOOD Image1 Text in Sustainability Page Font Family is not in match with creative - Fail");
				bw.write("FOOD Image1 Text in Sustainability Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(FoodImage1SPContent.equals(ExpFoodImage1SPContent))
			{
				System.out.println("FOOD Image1 Content in Sustainability Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("FOOD Image1 Content in Sustainability Page is not in match with creative - Fail");
				bw.write("FOOD Image1 Content in Sustainability Page is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(FoodImage1SPFontSize.equals(ExpFoodImage1SPFontSize) && FoodImage1SPFontFamily.equals(ExpFoodImage1SPFontFamily) && FoodImage1SPContent.equals(ExpFoodImage1SPContent))
			{
			    Label FoodImage1CSSP = new Label(5,14,"PASS"); 
				wsheet5.addCell(FoodImage1CSSP); 
			}
			else
			{
			    Label FoodImage1CSSF = new Label(5,14,"FAIL"); 
				wsheet5.addCell(FoodImage1CSSF); 	
			}
				
			Thread.sleep(3000);
			JavascriptExecutor jsespfoodwhat = (JavascriptExecutor)driver;
			jsespfoodwhat.executeScript("window.scrollBy(0,500)", "");
			Thread.sleep(8000);
			
			// Sustainability Page - FOOD TAB - What can we do? Text
			
			WebElement FoodWhatSP = driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_1']/div[1]/div[2]/div[2]/div[1]")); 
			String FoodWhatSPColor = FoodWhatSP.getCssValue("color");
			System.out.println("Actual Color of FOOD What can we do? Text in Sustainability Page is : "	+ FoodWhatSPColor );
			// Convert RGB to Hex Value
			Color HFoodWhatSPColor = Color.fromString(FoodWhatSPColor);
			String HexFoodWhatSPColor = HFoodWhatSPColor.asHex();
			System.out.println("Actual Hex Color of FOOD What can we do? Text in Sustainability Page is : "	+ HexFoodWhatSPColor );
			
			String FoodWhatSPFontSize = FoodWhatSP.getCssValue("font-size");
			System.out.println("Actual Font Size of FOOD What can we do? Text in Sustainability Page is : "	+ FoodWhatSPFontSize);
			String FoodWhatSPFontFamily = FoodWhatSP.getCssValue("font-family");
			System.out.println("Actual Font Family of FOOD What can we do? Text in Sustainability Page is : "	+ FoodWhatSPFontFamily);
			String FoodWhatSPContent = FoodWhatSP.getText();
			System.out.println("Actual Content of FOOD What can we do? Text in Sustainability Page is : "	+ FoodWhatSPContent);
					
			String ExpFoodWhatSPColor = sheet5.getCell(1,15).getContents(); //column,row
			System.out.println("Expected Color of FOOD What can we do? Text in Sustainability Page is : "	+ ExpFoodWhatSPColor );
			String ExpFoodWhatSPFontSize = sheet5.getCell(2,15).getContents();
			System.out.println("Expected Font Size of FOOD What can we do? Text in Sustainability Page is : "	+ ExpFoodWhatSPFontSize);
			String ExpFoodWhatSPFontFamily = sheet5.getCell(3,15).getContents();
			System.out.println("Expected Font Family of FOOD What can we do? Text in Sustainability Page is : "	+ ExpFoodWhatSPFontFamily);
			String ExpFoodWhatSPContent = sheet5.getCell(4,15).getContents();
			System.out.println("Expected Content of FOOD What can we do? Text in Sustainability Page is : "	+ ExpFoodWhatSPContent);
			
			if(HexFoodWhatSPColor.equals(ExpFoodWhatSPColor))
			{
				System.out.println("FOOD What can we do? Text in Sustainability Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("FOOD What can we do? Text in Sustainability Page Color is not in match with creative - Fail");
				bw.write("FOOD What can we do? Text in Sustainability Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(FoodWhatSPFontSize.equals(ExpFoodWhatSPFontSize))
			{
				System.out.println("FOOD What can we do? Text in Sustainability Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("FOOD What can we do? Text in Sustainability Page Font Size is not in match with creative - Fail");
				bw.write("FOOD What can we do? Text in Sustainability Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(FoodWhatSPFontFamily.equals(ExpFoodWhatSPFontFamily))
			{
				System.out.println("FOOD What can we do? Text in Sustainability Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("FOOD What can we do? Text in Sustainability Page Font Family is not in match with creative - Fail");
				bw.write("FOOD What can we do? Text in Sustainability Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(FoodWhatSPContent.equals(ExpFoodWhatSPContent))
			{
				System.out.println("FOOD What can we do? Content in Sustainability Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("FOOD What can we do? Content in Sustainability Page is not in match with creative - Fail");
				bw.write("FOOD What can we do? Content in Sustainability Page is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(HexFoodWhatSPColor.equals(ExpFoodWhatSPColor) && FoodWhatSPFontSize.equals(ExpFoodWhatSPFontSize) && FoodWhatSPFontFamily.equals(ExpFoodWhatSPFontFamily) && FoodWhatSPContent.equals(ExpFoodWhatSPContent))
			{
			    Label FoodWhatCSSP = new Label(5,15,"PASS"); 
				wsheet5.addCell(FoodWhatCSSP); 
			}
			else
			{
			    Label FoodWhatCSSF = new Label(5,15,"FAIL"); 
				wsheet5.addCell(FoodWhatCSSF); 	
			}
			
			// Sustainability Page - FOOD TAB - What can we do? Desc
			
			WebElement FoodWhatDescSP = driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_1']/div[1]/div[2]/div[2]/div[2]")); 
			String FoodWhatDescSPColor = FoodWhatDescSP.getCssValue("color");
			System.out.println("Actual Color of FOOD What can we do? Desc in Sustainability Page is : "	+ FoodWhatDescSPColor );
			// Convert RGB to Hex Value
			Color HFoodWhatDescSPColor = Color.fromString(FoodWhatDescSPColor);
			String HexFoodWhatDescSPColor = HFoodWhatDescSPColor.asHex();
			System.out.println("Actual Hex Color of FOOD What can we do? Desc in Sustainability Page is : "	+ HexFoodWhatDescSPColor );
			
			String FoodWhatDescSPFontSize = FoodWhatDescSP.getCssValue("font-size");
			System.out.println("Actual Font Size of FOOD What can we do? Desc in Sustainability Page is : "	+ FoodWhatDescSPFontSize);
			String FoodWhatDescSPFontFamily = FoodWhatDescSP.getCssValue("font-family");
			System.out.println("Actual Font Family of FOOD What can we do? Desc in Sustainability Page is : "	+ FoodWhatDescSPFontFamily);
			String FoodWhatDescSPContent = FoodWhatDescSP.getText();
			System.out.println("Actual Content of FOOD What can we do? Desc in Sustainability Page is : "	+ FoodWhatDescSPContent);
					
			String ExpFoodWhatDescSPColor = sheet5.getCell(1,16).getContents(); //column,row
			System.out.println("Expected Color of FOOD What can we do? Desc in Sustainability Page is : "	+ ExpFoodWhatDescSPColor );
			String ExpFoodWhatDescSPFontSize = sheet5.getCell(2,16).getContents();
			System.out.println("Expected Font Size of FOOD What can we do? Desc in Sustainability Page is : "	+ ExpFoodWhatDescSPFontSize);
			String ExpFoodWhatDescSPFontFamily = sheet5.getCell(3,16).getContents();
			System.out.println("Expected Font Family of FOOD What can we do? Desc in Sustainability Page is : "	+ ExpFoodWhatDescSPFontFamily);
			String ExpFoodWhatDescSPContent = sheet5.getCell(4,16).getContents();
			System.out.println("Expected Content of FOOD What can we do? Desc in Sustainability Page is : "	+ ExpFoodWhatDescSPContent);
			
			if(HexFoodWhatDescSPColor.equals(ExpFoodWhatDescSPColor))
			{
				System.out.println("FOOD What can we do? Desc in Sustainability Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("FOOD What can we do? Desc in Sustainability Page Color is not in match with creative - Fail");
				bw.write("FOOD What can we do? Desc in Sustainability Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(FoodWhatDescSPFontSize.equals(ExpFoodWhatDescSPFontSize))
			{
				System.out.println("FOOD What can we do? Desc in Sustainability Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("FOOD What can we do? Desc in Sustainability Page Font Size is not in match with creative - Fail");
				bw.write("FOOD What can we do? Desc in Sustainability Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(FoodWhatDescSPFontFamily.equals(ExpFoodWhatDescSPFontFamily))
			{
				System.out.println("FOOD What can we do? Desc in Sustainability Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("FOOD What can we do? Desc in Sustainability Page Font Family is not in match with creative - Fail");
				bw.write("FOOD What can we do? Desc in Sustainability Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(FoodWhatDescSPContent.equals(ExpFoodWhatDescSPContent))
			{
				System.out.println("FOOD What can we do? Desc Content in Sustainability Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("FOOD What can we do? Desc Content in Sustainability Page is not in match with creative - Fail");
				bw.write("FOOD What can we do? Desc Content in Sustainability Page is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(HexFoodWhatDescSPColor.equals(ExpFoodWhatDescSPColor) && FoodWhatDescSPFontSize.equals(ExpFoodWhatDescSPFontSize) && FoodWhatDescSPFontFamily.equals(ExpFoodWhatDescSPFontFamily) && FoodWhatDescSPContent.equals(ExpFoodWhatDescSPContent))
			{
			    Label FoodWhatDescCSSP = new Label(5,16,"PASS"); 
				wsheet5.addCell(FoodWhatDescCSSP); 
			}
			else
			{
			    Label FoodWhatDescCSSF = new Label(5,16,"FAIL"); 
				wsheet5.addCell(FoodWhatDescCSSF); 	
			}
			
			Thread.sleep(3000);
			JavascriptExecutor jsespfoodhow = (JavascriptExecutor)driver;
			jsespfoodhow.executeScript("window.scrollBy(0,900)", "");
			Thread.sleep(4000);
			
			// Sustainability Page - FOOD TAB - How does it help? Text
			
			WebElement FoodHowSP = driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_1']/div[1]/div[4]/div[1]/div[1]")); 
			String FoodHowSPColor = FoodHowSP.getCssValue("color");
			System.out.println("Actual Color of FOOD How does it help? Text in Sustainability Page is : "	+ FoodHowSPColor );
			// Convert RGB to Hex Value
			Color HFoodHowSPColor = Color.fromString(FoodHowSPColor);
			String HexFoodHowSPColor = HFoodHowSPColor.asHex();
			System.out.println("Actual Hex Color of FOOD How does it help? Text in Sustainability Page is : "	+ HexFoodHowSPColor );
			
			String FoodHowSPFontSize = FoodHowSP.getCssValue("font-size");
			System.out.println("Actual Font Size of FOOD How does it help? Text in Sustainability Page is : "	+ FoodHowSPFontSize);
			String FoodHowSPFontFamily = FoodHowSP.getCssValue("font-family");
			System.out.println("Actual Font Family of FOOD How does it help? Text in Sustainability Page is : "	+ FoodHowSPFontFamily);
			String FoodHowSPContent = FoodHowSP.getText();
			System.out.println("Actual Content of FOOD How does it help? Text in Sustainability Page is : "	+ FoodHowSPContent);
					
			String ExpFoodHowSPColor = sheet5.getCell(1,17).getContents(); //column,row
			System.out.println("Expected Color of FOOD How does it help? Text in Sustainability Page is : "	+ ExpFoodHowSPColor );
			String ExpFoodHowSPFontSize = sheet5.getCell(2,17).getContents();
			System.out.println("Expected Font Size of FOOD How does it help? Text in Sustainability Page is : "	+ ExpFoodHowSPFontSize);
			String ExpFoodHowSPFontFamily = sheet5.getCell(3,17).getContents();
			System.out.println("Expected Font Family of FOOD How does it help? Text in Sustainability Page is : "	+ ExpFoodHowSPFontFamily);
			String ExpFoodHowSPContent = sheet5.getCell(4,17).getContents();
			System.out.println("Expected Content of FOOD How does it help? Text in Sustainability Page is : "	+ ExpFoodHowSPContent);
			
			if(HexFoodHowSPColor.equals(ExpFoodHowSPColor))
			{
				System.out.println("FOOD How does it help? Text in Sustainability Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("FOOD How does it help? Text in Sustainability Page Color is not in match with creative - Fail");
				bw.write("FOOD How does it help? Text in Sustainability Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(FoodHowSPFontSize.equals(ExpFoodHowSPFontSize))
			{
				System.out.println("FOOD How does it help? Text in Sustainability Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("FOOD How does it help? Text in Sustainability Page Font Size is not in match with creative - Fail");
				bw.write("FOOD How does it help? Text in Sustainability Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(FoodHowSPFontFamily.equals(ExpFoodHowSPFontFamily))
			{
				System.out.println("FOOD How does it help? Text in Sustainability Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("FOOD How does it help? Text in Sustainability Page Font Family is not in match with creative - Fail");
				bw.write("FOOD How does it help? Text in Sustainability Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(FoodHowSPContent.equals(ExpFoodHowSPContent))
			{
				System.out.println("FOOD How does it help? Content in Sustainability Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("FOOD How does it help? Content in Sustainability Page is not in match with creative - Fail");
				bw.write("FOOD How does it help? Content in Sustainability Page is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(HexFoodHowSPColor.equals(ExpFoodHowSPColor) && FoodHowSPFontSize.equals(ExpFoodHowSPFontSize) && FoodHowSPFontFamily.equals(ExpFoodHowSPFontFamily) && FoodHowSPContent.equals(ExpFoodHowSPContent))
			{
			    Label FoodHowCSSP = new Label(5,17,"PASS"); 
				wsheet5.addCell(FoodHowCSSP); 
			}
			else
			{
			    Label FoodHowCSSF = new Label(5,17,"FAIL"); 
				wsheet5.addCell(FoodHowCSSF); 	
			}
			
			// Sustainability Page - FOOD TAB - How does it help? Desc
			
			WebElement FoodHowDescSP = driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_1']/div[1]/div[4]/div[1]/div[2]")); 
			String FoodHowDescSPColor = FoodHowDescSP.getCssValue("color");
			System.out.println("Actual Color of FOOD How does it help? Desc in Sustainability Page is : "	+ FoodHowDescSPColor );
			// Convert RGB to Hex Value
			Color HFoodHowDescSPColor = Color.fromString(FoodHowDescSPColor);
			String HexFoodHowDescSPColor = HFoodHowDescSPColor.asHex();
			System.out.println("Actual Hex Color of FOOD How does it help? Desc in Sustainability Page is : "	+ HexFoodHowDescSPColor );
			
			String FoodHowDescSPFontSize = FoodHowDescSP.getCssValue("font-size");
			System.out.println("Actual Font Size of FOOD How does it help? Desc in Sustainability Page is : "	+ FoodHowDescSPFontSize);
			String FoodHowDescSPFontFamily = FoodHowDescSP.getCssValue("font-family");
			System.out.println("Actual Font Family of FOOD How does it help? Desc in Sustainability Page is : "	+ FoodHowDescSPFontFamily);
			String FoodHowDescSPContent = FoodHowDescSP.getText();
			System.out.println("Actual Content of FOOD How does it help? Desc in Sustainability Page is : "	+ FoodHowDescSPContent);
					
			String ExpFoodHowDescSPColor = sheet5.getCell(1,18).getContents(); //column,row
			System.out.println("Expected Color of FOOD How does it help? Desc in Sustainability Page is : "	+ ExpFoodHowDescSPColor );
			String ExpFoodHowDescSPFontSize = sheet5.getCell(2,18).getContents();
			System.out.println("Expected Font Size of FOOD How does it help? Desc in Sustainability Page is : "	+ ExpFoodHowDescSPFontSize);
			String ExpFoodHowDescSPFontFamily = sheet5.getCell(3,18).getContents();
			System.out.println("Expected Font Family of FOOD How does it help? Desc in Sustainability Page is : "	+ ExpFoodHowDescSPFontFamily);
			String ExpFoodHowDescSPContent = sheet5.getCell(4,18).getContents();
			System.out.println("Expected Content of FOOD How does it help? Desc in Sustainability Page is : "	+ ExpFoodHowDescSPContent);
			
			if(HexFoodHowDescSPColor.equals(ExpFoodHowDescSPColor))
			{
				System.out.println("FOOD How does it help? Desc in Sustainability Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("FOOD How does it help? Desc in Sustainability Page Color is not in match with creative - Fail");
				bw.write("FOOD How does it help? Desc in Sustainability Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(FoodHowDescSPFontSize.equals(ExpFoodHowDescSPFontSize))
			{
				System.out.println("FOOD How does it help? Desc in Sustainability Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("FOOD How does it help? Desc in Sustainability Page Font Size is not in match with creative - Fail");
				bw.write("FOOD How does it help? Desc in Sustainability Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(FoodHowDescSPFontFamily.equals(ExpFoodHowDescSPFontFamily))
			{
				System.out.println("FOOD How does it help? Desc in Sustainability Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("FOOD How does it help? Desc in Sustainability Page Font Family is not in match with creative - Fail");
				bw.write("FOOD How does it help? Desc in Sustainability Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(FoodHowDescSPContent.equals(ExpFoodHowDescSPContent))
			{
				System.out.println("FOOD How does it help? Desc Content in Sustainability Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("FOOD How does it help? Desc Content in Sustainability Page is not in match with creative - Fail");
				bw.write("FOOD How does it help? Desc Content in Sustainability Page is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(HexFoodHowDescSPColor.equals(ExpFoodHowDescSPColor) && FoodHowDescSPFontSize.equals(ExpFoodHowDescSPFontSize) && FoodHowDescSPFontFamily.equals(ExpFoodHowDescSPFontFamily) && FoodHowDescSPContent.equals(ExpFoodHowDescSPContent))
			{
			    Label FoodHowDescCSSP = new Label(5,18,"PASS"); 
				wsheet5.addCell(FoodHowDescCSSP); 
			}
			else
			{
			    Label FoodHowDescCSSF = new Label(5,18,"FAIL"); 
				wsheet5.addCell(FoodHowDescCSSF); 	
			}
			
			// Sustainability Page - FOOD Image2 Desc
			
			WebElement FoodImage2SP = driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_1']/div[1]/div[4]/div[2]/div[3]")); 
			
			String FoodImage2SPFontSize = FoodImage2SP.getCssValue("font-size");
			System.out.println("Actual Font Size of FOOD Image2 Desc in Sustainability Page is : "	+ FoodImage2SPFontSize);
			String FoodImage2SPFontFamily = FoodImage2SP.getCssValue("font-family");
			System.out.println("Actual Font Family of FOOD Image2 Desc in Sustainability Page is : "	+ FoodImage2SPFontFamily);
			String FoodImage2SPContent = FoodImage2SP.getText();
			System.out.println("Actual Content of FOOD Image2 Desc in Sustainability Page is : "	+ FoodImage2SPContent);
				
			String ExpFoodImage2SPFontSize = sheet5.getCell(2,19).getContents();
			System.out.println("Expected Font Size of FOOD Image2 Desc in Sustainability Page is : "	+ ExpFoodImage2SPFontSize);
			String ExpFoodImage2SPFontFamily = sheet5.getCell(3,19).getContents();
			System.out.println("Expected Font Family of FOOD Image2 Desc in Sustainability Page is : "	+ ExpFoodImage2SPFontFamily);
			String ExpFoodImage2SPContent = sheet5.getCell(4,19).getContents();
			System.out.println("Expected Content of FOOD Image2 Desc in Sustainability Page is : "	+ ExpFoodImage2SPContent);
				
			if(FoodImage2SPFontSize.equals(ExpFoodImage2SPFontSize))
			{
				System.out.println("FOOD Image2 Desc in Sustainability Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("FOOD Image2 Desc in Sustainability Page Font Size is not in match with creative - Fail");
				bw.write("FOOD Image2 Desc in Sustainability Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(FoodImage2SPFontFamily.equals(ExpFoodImage2SPFontFamily))
			{
				System.out.println("FOOD Image2 Desc in Sustainability Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("FOOD Image2 Desc in Sustainability Page Font Family is not in match with creative - Fail");
				bw.write("FOOD Image2 Desc in Sustainability Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(FoodImage2SPContent.equals(ExpFoodImage2SPContent))
			{
				System.out.println("FOOD Image2 Desc Content in Sustainability Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("FOOD Image2 Desc Content in Sustainability Page is not in match with creative - Fail");
				bw.write("FOOD Image2 Desc Content in Sustainability Page is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(FoodImage2SPFontSize.equals(ExpFoodImage2SPFontSize) && FoodImage2SPFontFamily.equals(ExpFoodImage2SPFontFamily) && FoodImage2SPContent.equals(ExpFoodImage2SPContent))
			{
			    Label FoodImage2CSSP = new Label(5,19,"PASS"); 
				wsheet5.addCell(FoodImage2CSSP); 
			}
			else
			{
			    Label FoodImage2CSSF = new Label(5,19,"FAIL"); 
				wsheet5.addCell(FoodImage2CSSF); 	
			}
			
			// Sustainability Page - FOOD - READ THE SUSTAINABILITY REPORT
			
			WebElement FoodReadSP = driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_1']/div[2]/div[1]/a")); 
			String FoodReadSPColor = FoodReadSP.getCssValue("color");
			System.out.println("Actual Color of FOOD READ THE SUSTAINABILITY REPORT Text in Sustainability Page is : "	+ FoodReadSPColor );
			// Convert RGB to Hex Value
			Color HFoodReadSPColor = Color.fromString(FoodReadSPColor);
			String HexFoodReadSPColor = HFoodReadSPColor.asHex();
			System.out.println("Actual Hex Color of FOOD READ THE SUSTAINABILITY REPORT Text in Sustainability Page is : "	+ HexFoodReadSPColor );
			
			String FoodReadSPFontSize = FoodReadSP.getCssValue("font-size");
			System.out.println("Actual Font Size of FOOD READ THE SUSTAINABILITY REPORT Text in Sustainability Page is : "	+ FoodReadSPFontSize);
			String FoodReadSPFontFamily = FoodReadSP.getCssValue("font-family");
			System.out.println("Actual Font Family of FOOD READ THE SUSTAINABILITY REPORT Text in Sustainability Page is : "	+ FoodReadSPFontFamily);
			String FoodReadSPSPContent = FoodReadSP.getText();
			System.out.println("Actual Content of FOOD READ THE SUSTAINABILITY REPORT Text in Sustainability Page is : "	+ FoodReadSPSPContent);
					
			String ExpFoodReadSPColor = sheet5.getCell(1,20).getContents(); //column,row
			System.out.println("Expected Color of FOOD READ THE SUSTAINABILITY REPORT Text in Sustainability Page is : "	+ ExpFoodReadSPColor );
			String ExpFoodReadSPFontSize = sheet5.getCell(2,20).getContents();
			System.out.println("Expected Font Size of FOOD READ THE SUSTAINABILITY REPORT Text in Sustainability Page is : "	+ ExpFoodReadSPFontSize);
			String ExpFoodReadSPFontFamily = sheet5.getCell(3,20).getContents();
			System.out.println("Expected Font Family of FOOD READ THE SUSTAINABILITY REPORT Text in Sustainability Page is : "	+ ExpFoodReadSPFontFamily);
			String ExpFoodReadSPSPContent = sheet5.getCell(4,20).getContents();
			System.out.println("Expected Content of FOOD READ THE SUSTAINABILITY REPORT Text in Sustainability Page is : "	+ ExpFoodReadSPSPContent);
					
			if(HexFoodReadSPColor.equals(ExpFoodReadSPColor))
			{
				System.out.println("FOOD READ THE SUSTAINABILITY REPORT Text in Sustainability Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("FOOD READ THE SUSTAINABILITY REPORT Text in Sustainability Page Color is not in match with creative - Fail");
				bw.write("FOOD READ THE SUSTAINABILITY REPORT Text in Sustainability Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(FoodReadSPFontSize.equals(ExpFoodReadSPFontSize))
			{
				System.out.println("FOOD READ THE SUSTAINABILITY REPORT Text in Sustainability Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("FOOD READ THE SUSTAINABILITY REPORT Text in Sustainability Page Font Size is not in match with creative - Fail");
				bw.write("FOOD READ THE SUSTAINABILITY REPORT Text in Sustainability Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(FoodReadSPFontFamily.equals(ExpFoodReadSPFontFamily))
			{
				System.out.println("FOOD READ THE SUSTAINABILITY REPORT Text in Sustainability Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("FOOD READ THE SUSTAINABILITY REPORT Text in Sustainability Page Font Family is not in match with creative - Fail");
				bw.write("FOOD READ THE SUSTAINABILITY REPORT Text in Sustainability Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(FoodReadSPSPContent.equals(ExpFoodReadSPSPContent))
			{
				System.out.println("FOOD READ THE SUSTAINABILITY REPORT Content in Sustainability Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("FOOD READ THE SUSTAINABILITY REPORT Content in Sustainability Page is not in match with creative - Fail");
				bw.write("FOOD READ THE SUSTAINABILITY REPORT Content in Sustainability Page is not in match with creative - Fail"); 
			    bw.newLine();
			}
			
			if(HexFoodReadSPColor.equals(ExpFoodReadSPColor) && FoodReadSPFontSize.equals(ExpFoodReadSPFontSize) && FoodReadSPFontFamily.equals(ExpFoodReadSPFontFamily) && FoodReadSPSPContent.equals(ExpFoodReadSPSPContent))
			{
				Label FoodReadCSSP = new Label(5,20,"PASS"); 
				wsheet5.addCell(FoodReadCSSP); 
			}
			else
			{
				Label FoodReadCSSF = new Label(5,20,"FAIL"); 
				wsheet5.addCell(FoodReadCSSF); 
			}
			
			// Sustainability Page - FOOD - LEARN ABOUT WATER ACCESS
			
			WebElement FoodLearnSP = driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_1']/div[2]/div[2]/span[1]")); 
			String FoodLearnSPColor = FoodLearnSP.getCssValue("color");
			System.out.println("Actual Color of FOOD - LEARN ABOUT WATER ACCESS Text in Sustainability Page is : "	+ FoodLearnSPColor );
			// Convert RGB to Hex Value
			Color HFoodLearnSPColor = Color.fromString(FoodLearnSPColor);
			String HexFoodLearnSPColor = HFoodLearnSPColor.asHex();
			System.out.println("Actual Hex Color of FOOD - LEARN ABOUT WATER ACCESS Text in Sustainability Page is : "	+ HexFoodLearnSPColor );
			
			String FoodLearnSPFontSize = FoodLearnSP.getCssValue("font-size");
			System.out.println("Actual Font Size of FOOD - LEARN ABOUT WATER ACCESS Text in Sustainability Page is : "	+ FoodLearnSPFontSize);
			String FoodLearnSPFontFamily = FoodLearnSP.getCssValue("font-family");
			System.out.println("Actual Font Family of FOOD - LEARN ABOUT WATER ACCESS Text in Sustainability Page is : "	+ FoodLearnSPFontFamily);
			String FoodLearnSPContent = FoodLearnSP.getText();
			System.out.println("Actual Content of FOOD - LEARN ABOUT WATER ACCESS Text in Sustainability Page is : "	+ FoodLearnSPContent);
					
			String ExpFoodLearnSPColor = sheet5.getCell(1,21).getContents(); //column,row
			System.out.println("Expected Color of FOOD - LEARN ABOUT WATER ACCESS Text in Sustainability Page is : "	+ ExpFoodLearnSPColor );
			String ExpFoodLearnSPFontSize = sheet5.getCell(2,21).getContents();
			System.out.println("Expected Font Size of FOOD - LEARN ABOUT WATER ACCESS Text in Sustainability Page is : "	+ ExpFoodLearnSPFontSize);
			String ExpFoodLearnSPFontFamily = sheet5.getCell(3,21).getContents();
			System.out.println("Expected Font Family of FOOD - LEARN ABOUT WATER ACCESS Text in Sustainability Page is : "	+ ExpFoodLearnSPFontFamily);
			String ExpFoodLearnSPContent = sheet5.getCell(4,21).getContents();
			System.out.println("Expected Content of FOOD - LEARN ABOUT WATER ACCESS in Sustainability Page is : "	+ ExpFoodLearnSPContent);
					
			if(HexFoodLearnSPColor.equals(ExpFoodLearnSPColor))
			{
				System.out.println("FOOD - LEARN ABOUT WATER ACCESS Text in Sustainability Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("FOOD - LEARN ABOUT WATER ACCESS Text in Sustainability Page Color is not in match with creative - Fail");
				bw.write("FOOD - LEARN ABOUT WATER ACCESS Text in Sustainability Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(FoodLearnSPFontSize.equals(ExpFoodLearnSPFontSize))
			{
				System.out.println("FOOD - LEARN ABOUT WATER ACCESS Text in Sustainability Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("FOOD - LEARN ABOUT WATER ACCESS Text in Sustainability Page Font Size is not in match with creative - Fail");
				bw.write("FOOD - LEARN ABOUT WATER ACCESS Text in Sustainability Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(FoodLearnSPFontFamily.equals(ExpFoodLearnSPFontFamily))
			{
				System.out.println("FOOD - LEARN ABOUT WATER ACCESS Text in Sustainability Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("FOOD - LEARN ABOUT WATER ACCESS Text in Sustainability Page Font Family is not in match with creative - Fail");
				bw.write("FOOD - LEARN ABOUT WATER ACCESS Text in Sustainability Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			
			if(FoodLearnSPContent.equals(ExpFoodLearnSPContent))
			{
				System.out.println("FOOD - LEARN ABOUT WATER ACCESS Content in Sustainability Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("FOOD - LEARN ABOUT WATER ACCESS Content in Sustainability Page is not in match with creative - Fail");
				bw.write("FOOD - LEARN ABOUT WATER ACCESS Content in Sustainability Page is not in match with creative - Fail"); 
			    bw.newLine();
			}
			
			if(HexFoodLearnSPColor.equals(ExpFoodLearnSPColor) && FoodLearnSPFontSize.equals(ExpFoodLearnSPFontSize) && FoodLearnSPFontFamily.equals(ExpFoodLearnSPFontFamily) && FoodLearnSPContent.equals(ExpFoodLearnSPContent))
			{
				Label FoodLearnCSSP = new Label(5,21,"PASS"); 
				wsheet5.addCell(FoodLearnCSSP); 
			}
			else
			{
				Label FoodLearnCSSF = new Label(5,21,"FAIL"); 
				wsheet5.addCell(FoodLearnCSSF); 
			}
			
			// Sustainability Page - FOOD - READ THE SUSTAINABILITY REPORT - Click
			
			String winHandleBeforeFR = driver.getWindowHandle();
			driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_1']/div[2]/div[1]/a")).click(); 
			Thread.sleep(5000);
			//Get the list of window handles
			ArrayList<String> tabsFR = new ArrayList<String>(driver.getWindowHandles());
			System.out.println(tabsFR.size());
			//Use the list of window handles to switch between windows
		    driver.switchTo().window((String) tabsFR.get(1));
			Thread.sleep(10000);
			String AFReadurl = driver.getCurrentUrl();
			System.out.println("Actual FOOD - READ THE SUSTAINABILITY REPORT URL is: "+AFReadurl);
			String EFReadurl = sheet2.getCell(1,45).getContents();
			System.out.println("Expected FOOD - READ THE SUSTAINABILITY REPORT URL is: "+EFReadurl);
			if(EFReadurl.equals(AFReadurl))
			{
				System.out.println("FOOD - READ THE SUSTAINABILITY REPORT Page is Opened Correctly - Pass");
				Label FoodReadNAVP = new Label(2,45,"PASS"); 
				wsheet2.addCell(FoodReadNAVP); 
			}
			else
			{
				System.out.println("FOOD - READ THE SUSTAINABILITY REPORT Page is not Opened Correctly - Fail");
				bw.write("FOOD - READ THE SUSTAINABILITY REPORT Page is not Opened Correctly - Fail"); 
			    bw.newLine();
			    Label FoodReadNAVF = new Label(2,45,"FAIL"); 
				wsheet2.addCell(FoodReadNAVF); 
			}
			driver.close();
			Thread.sleep(5000);
			driver.switchTo().window(winHandleBeforeFR); // Home Window
			
			// Sustainability Page - FOOD - Learn about Water Access - Click
			
			driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_1']/div[2]/div[2]/span[1]")).click(); 
			Thread.sleep(10000);
			String AFLearnurl = driver.getCurrentUrl();
			System.out.println("Actual FOOD - Learn about Water Access URL is: "+AFLearnurl);
			String EFLearnurl = sheet2.getCell(1,46).getContents();
			System.out.println("Expected FOOD - Learn about Water Access URL is: "+EFLearnurl);
			if(EFLearnurl.equals(AFLearnurl))
			{
				System.out.println("FOOD - Learn about Water Access Page is Opened Correctly - Pass");
				Label FoodLearnNAVP = new Label(2,46,"PASS"); 
				wsheet2.addCell(FoodLearnNAVP); 
			}
			else
			{
				System.out.println("FOOD - Learn about Water Access is not Opened Correctly - Fail");
				bw.write("FOOD - Learn about Water Access is not Opened Correctly - Fail"); 
			    bw.newLine();
			    Label FoodLearnNAVF = new Label(2,46,"FAIL"); 
				wsheet2.addCell(FoodLearnNAVF); 
			}
			
			
			// WATER TAB
			
			Thread.sleep(3000);
			JavascriptExecutor jsespWaterdesc = (JavascriptExecutor)driver;
			jsespWaterdesc.executeScript("window.scrollBy(0,400)", "");
			Thread.sleep(8000);
			
			// Sustainability Page - WATER Desc
			
			WebElement WaterDescSP = driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_2']/div[1]/div[1]/div[2]/div[2]")); 
			String WaterDescSPColor = WaterDescSP.getCssValue("color");
			System.out.println("Actual Color of WATER Desc Text in Sustainability Page is : "	+ WaterDescSPColor );
			// Convert RGB to Hex Value
			Color HWaterDescSPColor = Color.fromString(WaterDescSPColor);
			String HexWaterDescSPColor = HWaterDescSPColor.asHex();
			System.out.println("Actual Hex Color of WATER Desc Text in Sustainability Page is : "	+ HexWaterDescSPColor );
			
			String WaterDescSPFontSize = WaterDescSP.getCssValue("font-size");
			System.out.println("Actual Font Size of WATER Desc Text in Sustainability Page is : "	+ WaterDescSPFontSize);
			String WaterDescSPFontFamily = WaterDescSP.getCssValue("font-family");
			System.out.println("Actual Font Family of WATER Desc Text in Sustainability Page is : "	+ WaterDescSPFontFamily);
			String WaterDescSPContent = WaterDescSP.getText();
			System.out.println("Actual Content of WATER Desc Text in Sustainability Page is : "	+ WaterDescSPContent);
					
			String ExpWaterDescSPColor = sheet5.getCell(1,23).getContents(); //column,row
			System.out.println("Expected Color of WATER Desc Text in Sustainability Page is : "	+ ExpWaterDescSPColor );
			String ExpWaterDescSPFontSize = sheet5.getCell(2,23).getContents();
			System.out.println("Expected Font Size of WATER Desc Text in Sustainability Page is : "	+ ExpWaterDescSPFontSize);
			String ExpWaterDescSPFontFamily = sheet5.getCell(3,23).getContents();
			System.out.println("Expected Font Family of WATER Desc Text in Sustainability Page is : "	+ ExpWaterDescSPFontFamily);
			String ExpWaterDescSPContent = sheet5.getCell(4,23).getContents();
			System.out.println("Expected Content of WATER Desc Text in Sustainability Page is : "	+ ExpWaterDescSPContent);
			
			if(HexWaterDescSPColor.equals(ExpWaterDescSPColor))
			{
				System.out.println("WATER Desc Text in Sustainability Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("WATER Desc Text in Sustainability Page Color is not in match with creative - Fail");
				bw.write("WATER Desc Text in Sustainability Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(WaterDescSPFontSize.equals(ExpWaterDescSPFontSize))
			{
				System.out.println("WATER Desc Text in Sustainability Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("WATER Desc Text in Sustainability Page Font Size is not in match with creative - Fail");
				bw.write("WATER Desc Text in Sustainability Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(WaterDescSPFontFamily.equals(ExpWaterDescSPFontFamily))
			{
				System.out.println("WATER Desc Text in Sustainability Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("WATER Desc Text in Sustainability Page Font Family is not in match with creative - Fail");
				bw.write("WATER Desc Text in Sustainability Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(WaterDescSPContent.equals(ExpWaterDescSPContent))
			{
				System.out.println("WATER Desc Content in Sustainability Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("WATER Desc Content in Sustainability Page is not in match with creative - Fail");
				bw.write("WATER Desc Content in Sustainability Page is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(HexWaterDescSPColor.equals(ExpWaterDescSPColor) && WaterDescSPFontSize.equals(ExpWaterDescSPFontSize) && WaterDescSPFontFamily.equals(ExpWaterDescSPFontFamily) && WaterDescSPContent.equals(ExpWaterDescSPContent))
			{
			    Label WaterDescCSSP = new Label(5,23,"PASS"); 
				wsheet5.addCell(WaterDescCSSP); 
			}
			else
			{
			    Label WaterDescCSSF = new Label(5,23,"FAIL"); 
				wsheet5.addCell(WaterDescCSSF); 	
			}
			
			// Sustainability Page - WATER Desc1
			
			WebElement WaterDesc1SP = driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_2']/div[1]/div[1]/div[2]/div[3]")); 
			String WaterDesc1SPColor = WaterDesc1SP.getCssValue("color");
			System.out.println("Actual Color of WATER Desc1 Text in Sustainability Page is : "	+ WaterDesc1SPColor );
			// Convert RGB to Hex Value
			Color HWaterDesc1SPColor = Color.fromString(WaterDesc1SPColor);
			String HexWaterDesc1SPColor = HWaterDesc1SPColor.asHex();
			System.out.println("Actual Hex Color of WATER Desc1 Text in Sustainability Page is : "	+ HexWaterDesc1SPColor );
			
			String WaterDesc1SPFontSize = WaterDesc1SP.getCssValue("font-size");
			System.out.println("Actual Font Size of WATER Desc1 Text in Sustainability Page is : "	+ WaterDesc1SPFontSize);
			String WaterDesc1SPFontFamily = WaterDesc1SP.getCssValue("font-family");
			System.out.println("Actual Font Family of WATER Desc1 Text in Sustainability Page is : "	+ WaterDesc1SPFontFamily);
			String WaterDesc1SPContent = WaterDesc1SP.getText();
			System.out.println("Actual Content of WATER Desc1 Text in Sustainability Page is : "	+ WaterDesc1SPContent);
					
			String ExpWaterDesc1SPColor = sheet5.getCell(1,24).getContents(); //column,row
			System.out.println("Expected Color of WATER Desc1 Text in Sustainability Page is : "	+ ExpWaterDesc1SPColor );
			String ExpWaterDesc1SPFontSize = sheet5.getCell(2,24).getContents();
			System.out.println("Expected Font Size of WATER Desc1 Text in Sustainability Page is : "	+ ExpWaterDesc1SPFontSize);
			String ExpWaterDesc1SPFontFamily = sheet5.getCell(3,24).getContents();
			System.out.println("Expected Font Family of WATER Desc1 Text in Sustainability Page is : "	+ ExpWaterDesc1SPFontFamily);
			String ExpWaterDesc1SPContent = sheet5.getCell(4,24).getContents();
			System.out.println("Expected Content of WATER Desc1 Text in Sustainability Page is : "	+ ExpWaterDesc1SPContent);
			
			if(HexWaterDesc1SPColor.equals(ExpWaterDesc1SPColor))
			{
				System.out.println("WATER Desc1 Text in Sustainability Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("WATER Desc1 Text in Sustainability Page Color is not in match with creative - Fail");
				bw.write("WATER Desc1 Text in Sustainability Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(WaterDesc1SPFontSize.equals(ExpWaterDesc1SPFontSize))
			{
				System.out.println("WATER Desc1 Text in Sustainability Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("WATER Desc1 Text in Sustainability Page Font Size is not in match with creative - Fail");
				bw.write("WATER Desc1 Text in Sustainability Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(WaterDesc1SPFontFamily.equals(ExpWaterDesc1SPFontFamily))
			{
				System.out.println("WATER Desc1 Text in Sustainability Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("WATER Desc1 Text in Sustainability Page Font Family is not in match with creative - Fail");
				bw.write("WATER Desc1 Text in Sustainability Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(WaterDesc1SPContent.equals(ExpWaterDesc1SPContent))
			{
				System.out.println("WATER Desc1 Content in Sustainability Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("WATER Desc1 Content in Sustainability Page is not in match with creative - Fail");
				bw.write("WATER Desc1 Content in Sustainability Page is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(HexWaterDesc1SPColor.equals(ExpWaterDesc1SPColor) && WaterDesc1SPFontSize.equals(ExpWaterDesc1SPFontSize) && WaterDesc1SPFontFamily.equals(ExpWaterDesc1SPFontFamily) && WaterDesc1SPContent.equals(ExpWaterDesc1SPContent))
			{
			    Label WaterDesc1CSSP = new Label(5,24,"PASS"); 
				wsheet5.addCell(WaterDesc1CSSP); 
			}
			else
			{
			    Label WaterDesc1CSSF = new Label(5,24,"FAIL"); 
				wsheet5.addCell(WaterDesc1CSSF); 	
			}
			
			Thread.sleep(3000);
			JavascriptExecutor jsespsolving = (JavascriptExecutor)driver;
			jsespsolving.executeScript("window.scrollBy(0,400)", "");
			Thread.sleep(8000);
			
			// Sustainability Page - WATER - How are we solving the problem?
			
			WebElement WatersolvingSP = driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_2']/div[1]/div[2]/div[1]/div[1]")); 
			String WatersolvingSPColor = WatersolvingSP.getCssValue("color");
			System.out.println("Actual Color of How are we solving the problem? Text in Sustainability Page is : "	+ WatersolvingSPColor );
			// Convert RGB to Hex Value
			Color HWatersolvingSPColor = Color.fromString(WatersolvingSPColor);
			String HexWatersolvingSPColor = HWatersolvingSPColor.asHex();
			System.out.println("Actual Hex Color of How are we solving the problem? Text in Sustainability Page is : "	+ HexWatersolvingSPColor );
			
			String WatersolvingSPFontSize = WatersolvingSP.getCssValue("font-size");
			System.out.println("Actual Font Size of How are we solving the problem? Text in Sustainability Page is : "	+ WatersolvingSPFontSize);
			String WatersolvingSPFontFamily = WatersolvingSP.getCssValue("font-family");
			System.out.println("Actual Font Family of How are we solving the problem? Text in Sustainability Page is : "	+ WatersolvingSPFontFamily);
			String WatersolvingSPContent = WatersolvingSP.getText();
			System.out.println("Actual Content of How are we solving the problem? Text in Sustainability Page is : "	+ WatersolvingSPContent);
					
			String ExpWatersolvingSPColor = sheet5.getCell(1,25).getContents(); //column,row
			System.out.println("Expected Color of How are we solving the problem? Text in Sustainability Page is : "	+ ExpWatersolvingSPColor );
			String ExpWatersolvingSPFontSize = sheet5.getCell(2,25).getContents();
			System.out.println("Expected Font Size of How are we solving the problem? Text in Sustainability Page is : "	+ ExpWatersolvingSPFontSize);
			String ExpWatersolvingSPFontFamily = sheet5.getCell(3,25).getContents();
			System.out.println("Expected Font Family of How are we solving the problem? Text in Sustainability Page is : "	+ ExpWatersolvingSPFontFamily);
			String ExpWatersolvingSPContent = sheet5.getCell(4,25).getContents();
			System.out.println("Expected Content of How are we solving the problem? Text in Sustainability Page is : "	+ ExpWatersolvingSPContent);
			
			if(HexWatersolvingSPColor.equals(ExpWatersolvingSPColor))
			{
				System.out.println("How are we solving the problem? Text in Sustainability Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("How are we solving the problem? Text in Sustainability Page Color is not in match with creative - Fail");
				bw.write("How are we solving the problem? Text in Sustainability Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(WatersolvingSPFontSize.equals(ExpWatersolvingSPFontSize))
			{
				System.out.println("How are we solving the problem? Text in Sustainability Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("How are we solving the problem? Text in Sustainability Page Font Size is not in match with creative - Fail");
				bw.write("How are we solving the problem? Text in Sustainability Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(WatersolvingSPFontFamily.equals(ExpWatersolvingSPFontFamily))
			{
				System.out.println("How are we solving the problem? Text in Sustainability Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("How are we solving the problem? Text in Sustainability Page Font Family is not in match with creative - Fail");
				bw.write("How are we solving the problem? Text in Sustainability Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(WatersolvingSPContent.equals(ExpWatersolvingSPContent))
			{
				System.out.println("How are we solving the problem? Content in Sustainability Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("How are we solving the problem? Content in Sustainability Page is not in match with creative - Fail");
				bw.write("How are we solving the problem? Content in Sustainability Page is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(HexWatersolvingSPColor.equals(ExpWatersolvingSPColor) && WatersolvingSPFontSize.equals(ExpWatersolvingSPFontSize) && WatersolvingSPFontFamily.equals(ExpWatersolvingSPFontFamily) && WatersolvingSPContent.equals(ExpWatersolvingSPContent))
			{
			    Label WatersolvingCSSP = new Label(5,25,"PASS"); 
				wsheet5.addCell(WatersolvingCSSP); 
			}
			else
			{
			    Label WatersolvingCSSF = new Label(5,25,"FAIL"); 
				wsheet5.addCell(WatersolvingCSSF); 	
			}
			
			// Sustainability Page - WATER - How are we solving the problem? Desc
			
			WebElement WatersolvingDescSP = driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_2']/div[1]/div[2]/div[1]/div[2]")); 
			String WatersolvingDescSPColor = WatersolvingDescSP.getCssValue("color");
			System.out.println("Actual Color of How are we solving the problem? Desc in Sustainability Page is : "	+ WatersolvingDescSPColor );
			// Convert RGB to Hex Value
			Color HWatersolvingDescSPColor = Color.fromString(WatersolvingDescSPColor);
			String HexWatersolvingDescSPColor = HWatersolvingDescSPColor.asHex();
			System.out.println("Actual Hex Color of How are we solving the problem? Desc in Sustainability Page is : "	+ HexWatersolvingDescSPColor );
			
			String WatersolvingDescSPFontSize = WatersolvingDescSP.getCssValue("font-size");
			System.out.println("Actual Font Size of How are we solving the problem? Desc in Sustainability Page is : "	+ WatersolvingDescSPFontSize);
			String WatersolvingDescSPFontFamily = WatersolvingDescSP.getCssValue("font-family");
			System.out.println("Actual Font Family of How are we solving the problem? Desc in Sustainability Page is : "	+ WatersolvingDescSPFontFamily);
			String WatersolvingDescSPContent = WatersolvingDescSP.getText();
			System.out.println("Actual Content of How are we solving the problem? Desc in Sustainability Page is : "	+ WatersolvingDescSPContent);
					
			String ExpWatersolvingDescSPColor = sheet5.getCell(1,26).getContents(); //column,row
			System.out.println("Expected Color of How are we solving the problem? Desc in Sustainability Page is : "	+ ExpWatersolvingDescSPColor );
			String ExpWatersolvingDescSPFontSize = sheet5.getCell(2,26).getContents();
			System.out.println("Expected Font Size of How are we solving the problem? Desc in Sustainability Page is : "	+ ExpWatersolvingDescSPFontSize);
			String ExpWatersolvingDescSPFontFamily = sheet5.getCell(3,26).getContents();
			System.out.println("Expected Font Family of How are we solving the problem? Desc in Sustainability Page is : "	+ ExpWatersolvingDescSPFontFamily);
			String ExpWatersolvingDescSPContent = sheet5.getCell(4,26).getContents();
			System.out.println("Expected Content of How are we solving the problem? Desc in Sustainability Page is : "	+ ExpWatersolvingDescSPContent);
			
			if(HexWatersolvingDescSPColor.equals(ExpWatersolvingDescSPColor))
			{
				System.out.println("How are we solving the problem? Desc in Sustainability Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("How are we solving the problem? Desc in Sustainability Page Color is not in match with creative - Fail");
				bw.write("How are we solving the problem? Desc in Sustainability Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(WatersolvingDescSPFontSize.equals(ExpWatersolvingDescSPFontSize))
			{
				System.out.println("How are we solving the problem? Desc in Sustainability Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("How are we solving the problem? Desc in Sustainability Page Font Size is not in match with creative - Fail");
				bw.write("How are we solving the problem? Desc in Sustainability Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(WatersolvingDescSPFontFamily.equals(ExpWatersolvingDescSPFontFamily))
			{
				System.out.println("How are we solving the problem? Desc in Sustainability Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("How are we solving the problem? Desc in Sustainability Page Font Family is not in match with creative - Fail");
				bw.write("How are we solving the problem? Desc in Sustainability Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(WatersolvingDescSPContent.equals(ExpWatersolvingDescSPContent))
			{
				System.out.println("How are we solving the problem? Desc Content in Sustainability Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("How are we solving the problem? Desc Content in Sustainability Page is not in match with creative - Fail");
				bw.write("How are we solving the problem? Desc Content in Sustainability Page is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(HexWatersolvingDescSPColor.equals(ExpWatersolvingDescSPColor) && WatersolvingDescSPFontSize.equals(ExpWatersolvingDescSPFontSize) && WatersolvingDescSPFontFamily.equals(ExpWatersolvingDescSPFontFamily) && WatersolvingDescSPContent.equals(ExpWatersolvingDescSPContent))
			{
			    Label WatersolvingDCSSP = new Label(5,26,"PASS"); 
				wsheet5.addCell(WatersolvingDCSSP); 
			}
			else
			{
			    Label WatersolvingDCSSF = new Label(5,26,"FAIL"); 
				wsheet5.addCell(WatersolvingDCSSF); 	
			}
			
			Thread.sleep(3000);
			JavascriptExecutor jsespimpact = (JavascriptExecutor)driver;
			jsespimpact.executeScript("window.scrollBy(0,500)", "");
			Thread.sleep(8000);
			
			// Sustainability Page - WATER - What impact are we making?
			
			WebElement WaterimpactSP = driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_2']/div[1]/div[3]/div[2]/div[1]")); 
			String WaterimpactSPColor = WaterimpactSP.getCssValue("color");
			System.out.println("Actual Color of What impact are we making? Text in Sustainability Page is : "	+ WaterimpactSPColor );
			// Convert RGB to Hex Value
			Color HWaterimpactSPColor = Color.fromString(WaterimpactSPColor);
			String HexWaterimpactSPColor = HWaterimpactSPColor.asHex();
			System.out.println("Actual Hex Color of What impact are we making? Text in Sustainability Page is : "	+ HexWaterimpactSPColor );
			
			String WaterimpactSPFontSize = WaterimpactSP.getCssValue("font-size");
			System.out.println("Actual Font Size of What impact are we making? Text in Sustainability Page is : "	+ WaterimpactSPFontSize);
			String WaterimpactSPFontFamily = WaterimpactSP.getCssValue("font-family");
			System.out.println("Actual Font Family of What impact are we making? Text in Sustainability Page is : "	+ WaterimpactSPFontFamily);
			String WaterimpactSPContent = WaterimpactSP.getText();
			System.out.println("Actual Content of What impact are we making? Text in Sustainability Page is : "	+ WaterimpactSPContent);
					
			String ExpWaterimpactSPColor = sheet5.getCell(1,27).getContents(); //column,row
			System.out.println("Expected Color of What impact are we making? Text in Sustainability Page is : "	+ ExpWaterimpactSPColor );
			String ExpWaterimpactSPFontSize = sheet5.getCell(2,27).getContents();
			System.out.println("Expected Font Size of What impact are we making? Text in Sustainability Page is : "	+ ExpWaterimpactSPFontSize);
			String ExpWaterimpactSPFontFamily = sheet5.getCell(3,27).getContents();
			System.out.println("Expected Font Family of What impact are we making? Text in Sustainability Page is : "	+ ExpWaterimpactSPFontFamily);
			String ExpWaterimpactSPContent = sheet5.getCell(4,27).getContents();
			System.out.println("Expected Content of What impact are we making? Text in Sustainability Page is : "	+ ExpWaterimpactSPContent);
			
			if(HexWaterimpactSPColor.equals(ExpWaterimpactSPColor))
			{
				System.out.println("What impact are we making? Text in Sustainability Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("What impact are we making? Text in Sustainability Page Color is not in match with creative - Fail");
				bw.write("What impact are we making? Text in Sustainability Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(WaterimpactSPFontSize.equals(ExpWaterimpactSPFontSize))
			{
				System.out.println("What impact are we making? Text in Sustainability Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("What impact are we making? Text in Sustainability Page Font Size is not in match with creative - Fail");
				bw.write("What impact are we making? Text in Sustainability Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(WaterimpactSPFontFamily.equals(ExpWaterimpactSPFontFamily))
			{
				System.out.println("What impact are we making? Text in Sustainability Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("What impact are we making? Text in Sustainability Page Font Family is not in match with creative - Fail");
				bw.write("What impact are we making? Text in Sustainability Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(WaterimpactSPContent.equals(ExpWaterimpactSPContent))
			{
				System.out.println("What impact are we making? Content in Sustainability Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("What impact are we making? Content in Sustainability Page is not in match with creative - Fail");
				bw.write("What impact are we making? Content in Sustainability Page is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(HexWaterimpactSPColor.equals(ExpWaterimpactSPColor) && WaterimpactSPFontSize.equals(ExpWaterimpactSPFontSize) && WaterimpactSPFontFamily.equals(ExpWaterimpactSPFontFamily) && WaterimpactSPContent.equals(ExpWaterimpactSPContent))
			{
			    Label WaterimpactCSSP = new Label(5,27,"PASS"); 
				wsheet5.addCell(WaterimpactCSSP); 
			}
			else
			{
			    Label WaterimpactCSSF = new Label(5,27,"FAIL"); 
				wsheet5.addCell(WaterimpactCSSF); 	
			}
			
			// Sustainability Page - WATER - What impact are we making? Desc
			
			WebElement WaterimpactDescSP = driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_2']/div[1]/div[3]/div[2]/div[2]")); 
			String WaterimpactDescSPColor = WaterimpactDescSP.getCssValue("color");
			System.out.println("Actual Color of What impact are we making? Desc in Sustainability Page is : "	+ WaterimpactDescSPColor );
			// Convert RGB to Hex Value
			Color HWaterimpactDescSPColor = Color.fromString(WaterimpactDescSPColor);
			String HexWaterimpactDescSPColor = HWaterimpactDescSPColor.asHex();
			System.out.println("Actual Hex Color of What impact are we making? Desc in Sustainability Page is : "	+ HexWaterimpactDescSPColor );
			
			String WaterimpactDescSPFontSize = WaterimpactDescSP.getCssValue("font-size");
			System.out.println("Actual Font Size of What impact are we making? Desc in Sustainability Page is : "	+ WaterimpactDescSPFontSize);
			String WaterimpactDescSPFontFamily = WaterimpactDescSP.getCssValue("font-family");
			System.out.println("Actual Font Family of What impact are we making? Desc in Sustainability Page is : "	+ WaterimpactDescSPFontFamily);
			String WaterimpactDescSPContent = WaterimpactDescSP.getText();
			System.out.println("Actual Content of What impact are we making? Desc in Sustainability Page is : "	+ WaterimpactDescSPContent);
					
			String ExpWaterimpactDescSPColor = sheet5.getCell(1,28).getContents(); //column,row
			System.out.println("Expected Color of What impact are we making? Desc in Sustainability Page is : "	+ ExpWaterimpactDescSPColor );
			String ExpWaterimpactDescSPFontSize = sheet5.getCell(2,28).getContents();
			System.out.println("Expected Font Size of What impact are we making? Desc in Sustainability Page is : "	+ ExpWaterimpactDescSPFontSize);
			String ExpWaterimpactDescSPFontFamily = sheet5.getCell(3,28).getContents();
			System.out.println("Expected Font Family of What impact are we making? Desc in Sustainability Page is : "	+ ExpWaterimpactDescSPFontFamily);
			String ExpWaterimpactDescSPContent = sheet5.getCell(4,28).getContents();
			System.out.println("Expected Content of What impact are we making? Desc in Sustainability Page is : "	+ ExpWaterimpactDescSPContent);
			
			if(HexWaterimpactDescSPColor.equals(ExpWaterimpactDescSPColor))
			{
				System.out.println("What impact are we making? Desc in Sustainability Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("What impact are we making? Desc in Sustainability Page Color is not in match with creative - Fail");
				bw.write("What impact are we making? Desc in Sustainability Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(WaterimpactDescSPFontSize.equals(ExpWaterimpactDescSPFontSize))
			{
				System.out.println("What impact are we making? Desc in Sustainability Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("What impact are we making? Desc in Sustainability Page Font Size is not in match with creative - Fail");
				bw.write("What impact are we making? Desc in Sustainability Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(WaterimpactDescSPFontFamily.equals(ExpWaterimpactDescSPFontFamily))
			{
				System.out.println("What impact are we making? Desc in Sustainability Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("What impact are we making? Desc in Sustainability Page Font Family is not in match with creative - Fail");
				bw.write("What impact are we making? Desc in Sustainability Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(WaterimpactDescSPContent.equals(ExpWaterimpactDescSPContent))
			{
				System.out.println("What impact are we making? Desc Content in Sustainability Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("What impact are we making? Desc Content in Sustainability Page is not in match with creative - Fail");
				bw.write("What impact are we making? Desc Content in Sustainability Page is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(HexWaterimpactDescSPColor.equals(ExpWaterimpactDescSPColor) && WaterimpactDescSPFontSize.equals(ExpWaterimpactDescSPFontSize) && WaterimpactDescSPFontFamily.equals(ExpWaterimpactDescSPFontFamily) && WaterimpactDescSPContent.equals(ExpWaterimpactDescSPContent))
			{
			    Label WaterimpactDCSSP = new Label(5,28,"PASS"); 
				wsheet5.addCell(WaterimpactDCSSP); 
			}
			else
			{
			    Label WaterimpactDCSSF = new Label(5,28,"FAIL"); 
				wsheet5.addCell(WaterimpactDCSSF); 	
			}
			
			Thread.sleep(3000);
			JavascriptExecutor jsespimpactD = (JavascriptExecutor)driver;
			jsespimpactD.executeScript("window.scrollBy(0,600)", "");
			Thread.sleep(8000);
			
			// Sustainability Page - WATER - What impact are we making? Desc1
			
			WebElement WaterimpactDesc1SP = driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_2']/div[1]/div[5]/div[2]/div")); 
			String WaterimpactDesc1SPColor = WaterimpactDesc1SP.getCssValue("color");
			System.out.println("Actual Color of What impact are we making? Desc1 in Sustainability Page is : "	+ WaterimpactDesc1SPColor );
			// Convert RGB to Hex Value
			Color HWaterimpactDesc1SPColor = Color.fromString(WaterimpactDesc1SPColor);
			String HexWaterimpactDesc1SPColor = HWaterimpactDesc1SPColor.asHex();
			System.out.println("Actual Hex Color of What impact are we making? Desc1 in Sustainability Page is : "	+ HexWaterimpactDesc1SPColor );
			
			String WaterimpactDesc1SPFontSize = WaterimpactDesc1SP.getCssValue("font-size");
			System.out.println("Actual Font Size of What impact are we making? Desc1 in Sustainability Page is : "	+ WaterimpactDesc1SPFontSize);
			String WaterimpactDesc1SPFontFamily = WaterimpactDesc1SP.getCssValue("font-family");
			System.out.println("Actual Font Family of What impact are we making? Desc1 in Sustainability Page is : "	+ WaterimpactDesc1SPFontFamily);
			String WaterimpactDesc1SPContent = WaterimpactDesc1SP.getText();
			System.out.println("Actual Content of What impact are we making? Desc1 in Sustainability Page is : "	+ WaterimpactDesc1SPContent);
					
			String ExpWaterimpactDesc1SPColor = sheet5.getCell(1,29).getContents(); //column,row
			System.out.println("Expected Color of What impact are we making? Desc1 in Sustainability Page is : "	+ ExpWaterimpactDesc1SPColor );
			String ExpWaterimpactDesc1SPFontSize = sheet5.getCell(2,29).getContents();
			System.out.println("Expected Font Size of What impact are we making? Desc1 in Sustainability Page is : "	+ ExpWaterimpactDesc1SPFontSize);
			String ExpWaterimpactDesc1SPFontFamily = sheet5.getCell(3,29).getContents();
			System.out.println("Expected Font Family of What impact are we making? Desc1 in Sustainability Page is : "	+ ExpWaterimpactDesc1SPFontFamily);
			String ExpWaterimpactDesc1SPContent = sheet5.getCell(4,29).getContents();
			System.out.println("Expected Content of What impact are we making? Desc1 in Sustainability Page is : "	+ ExpWaterimpactDesc1SPContent);
			
			if(HexWaterimpactDesc1SPColor.equals(ExpWaterimpactDesc1SPColor))
			{
				System.out.println("What impact are we making? Desc1 in Sustainability Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("What impact are we making? Desc1 in Sustainability Page Color is not in match with creative - Fail");
				bw.write("What impact are we making? Desc1 in Sustainability Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(WaterimpactDesc1SPFontSize.equals(ExpWaterimpactDesc1SPFontSize))
			{
				System.out.println("What impact are we making? Desc1 in Sustainability Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("What impact are we making? Desc1 in Sustainability Page Font Size is not in match with creative - Fail");
				bw.write("What impact are we making? Desc1 in Sustainability Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(WaterimpactDesc1SPFontFamily.equals(ExpWaterimpactDesc1SPFontFamily))
			{
				System.out.println("What impact are we making? Desc1 in Sustainability Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("What impact are we making? Desc1 in Sustainability Page Font Family is not in match with creative - Fail");
				bw.write("What impact are we making? Desc1 in Sustainability Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(WaterimpactDesc1SPContent.equals(ExpWaterimpactDesc1SPContent))
			{
				System.out.println("What impact are we making? Desc1 Content in Sustainability Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("What impact are we making? Desc1 Content in Sustainability Page is not in match with creative - Fail");
				bw.write("What impact are we making? Desc1 Content in Sustainability Page is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(HexWaterimpactDesc1SPColor.equals(ExpWaterimpactDesc1SPColor) && WaterimpactDesc1SPFontSize.equals(ExpWaterimpactDesc1SPFontSize) && WaterimpactDesc1SPFontFamily.equals(ExpWaterimpactDesc1SPFontFamily) && WaterimpactDesc1SPContent.equals(ExpWaterimpactDesc1SPContent))
			{
			    Label WaterimpactD1CSSP = new Label(5,29,"PASS"); 
				wsheet5.addCell(WaterimpactD1CSSP); 
			}
			else
			{
			    Label WaterimpactD1CSSF = new Label(5,29,"FAIL"); 
				wsheet5.addCell(WaterimpactD1CSSF); 	
			}
			
			// Sustainability Page - Water Image1 Desc
			
			WebElement WaterImage1SP = driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_2']/div[1]/div[5]/div[1]/div[3]")); 
		
			String WaterImage1SPFontSize = WaterImage1SP.getCssValue("font-size");
			System.out.println("Actual Font Size of Water Image1 Text in Sustainability Page is : "	+ WaterImage1SPFontSize);
			String WaterImage1SPFontFamily = WaterImage1SP.getCssValue("font-family");
			System.out.println("Actual Font Family of Water Image1 Text in Sustainability Page is : "	+ WaterImage1SPFontFamily);
			String WaterImage1SPContent = WaterImage1SP.getText();
			System.out.println("Actual Content of Water Image1 Text in Sustainability Page is : "	+ WaterImage1SPContent);
					
			String ExpWaterImage1SPFontSize = sheet5.getCell(2,30).getContents();
			System.out.println("Expected Font Size of Water Image1 Text in Sustainability Page is : "	+ ExpWaterImage1SPFontSize);
			String ExpWaterImage1SPFontFamily = sheet5.getCell(3,30).getContents();
			System.out.println("Expected Font Family of Water Image1 Text in Sustainability Page is : "	+ ExpWaterImage1SPFontFamily);
			String ExpWaterImage1SPContent = sheet5.getCell(4,30).getContents();
			System.out.println("Expected Content of Water Image1 Text in Sustainability Page is : "	+ ExpWaterImage1SPContent);
			
			if(WaterImage1SPFontSize.equals(ExpWaterImage1SPFontSize))
			{
				System.out.println("Water Image1 Text in Sustainability Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("Water Image1 Text in Sustainability Page Font Size is not in match with creative - Fail");
				bw.write("Water Image1 Text in Sustainability Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(WaterImage1SPFontFamily.equals(ExpWaterImage1SPFontFamily))
			{
				System.out.println("Water Image1 Text in Sustainability Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("Water Image1 Text in Sustainability Page Font Family is not in match with creative - Fail");
				bw.write("Water Image1 Text in Sustainability Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(WaterImage1SPContent.equals(ExpWaterImage1SPContent))
			{
				System.out.println("Water Image1 Content in Sustainability Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("Water Image1 Content in Sustainability Page is not in match with creative - Fail");
				bw.write("Water Image1 Content in Sustainability Page is not in match with creative - Fail"); 
			    bw.newLine();
			}
			
			if(WaterImage1SPFontSize.equals(ExpWaterImage1SPFontSize) && WaterImage1SPFontFamily.equals(ExpWaterImage1SPFontFamily) && WaterImage1SPContent.equals(ExpWaterImage1SPContent))
			{
				Label WaterImage1CSSP = new Label(5,30,"PASS"); 
				wsheet5.addCell(WaterImage1CSSP); 
			}
			else
			{
				Label WaterImage1CSSF = new Label(5,30,"FAIL"); 
				wsheet5.addCell(WaterImage1CSSF); 
			}
			
			// Sustainability Page - WATER - READ THE SUSTAINABILITY REPORT
			
			WebElement WaterReadSP = driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_2']/div[2]/div[1]/a")); 
			String WaterReadSPColor = WaterReadSP.getCssValue("color");
			System.out.println("Actual Color of WATER READ THE SUSTAINABILITY REPORT Text in Sustainability Page is : "	+ WaterReadSPColor );
			// Convert RGB to Hex Value
			Color HWaterReadSPColor = Color.fromString(WaterReadSPColor);
			String HexWaterReadSPColor = HWaterReadSPColor.asHex();
			System.out.println("Actual Hex Color of WATER READ THE SUSTAINABILITY REPORT Text in Sustainability Page is : "	+ HexWaterReadSPColor );
			
			String WaterReadSPFontSize = WaterReadSP.getCssValue("font-size");
			System.out.println("Actual Font Size of WATER READ THE SUSTAINABILITY REPORT Text in Sustainability Page is : "	+ WaterReadSPFontSize);
			String WaterReadSPFontFamily = WaterReadSP.getCssValue("font-family");
			System.out.println("Actual Font Family of WATER READ THE SUSTAINABILITY REPORT Text in Sustainability Page is : "	+ WaterReadSPFontFamily);
			String WaterReadSPContent = WaterReadSP.getText();
			System.out.println("Actual Content of WATER READ THE SUSTAINABILITY REPORT Text in Sustainability Page is : "	+ WaterReadSPContent);
					
			String ExpWaterReadSPColor = sheet5.getCell(1,31).getContents(); //column,row
			System.out.println("Expected Color of WATER READ THE SUSTAINABILITY REPORT Text in Sustainability Page is : "	+ ExpWaterReadSPColor );
			String ExpWaterReadSPFontSize = sheet5.getCell(2,31).getContents();
			System.out.println("Expected Font Size of WATER READ THE SUSTAINABILITY REPORT Text in Sustainability Page is : "	+ ExpWaterReadSPFontSize);
			String ExpWaterReadSPFontFamily = sheet5.getCell(3,31).getContents();
			System.out.println("Expected Font Family of WATER READ THE SUSTAINABILITY REPORT Text in Sustainability Page is : "	+ ExpWaterReadSPFontFamily);
			String ExpWaterReadSPContent = sheet5.getCell(4,31).getContents();
			System.out.println("Expected Content of WATER READ THE SUSTAINABILITY REPORT Text in Sustainability Page is : "	+ ExpWaterReadSPContent);
					
			if(HexWaterReadSPColor.equals(ExpWaterReadSPColor))
			{
				System.out.println("WATER READ THE SUSTAINABILITY REPORT Text in Sustainability Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("WATER READ THE SUSTAINABILITY REPORT Text in Sustainability Page Color is not in match with creative - Fail");
				bw.write("WATER READ THE SUSTAINABILITY REPORT Text in Sustainability Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(WaterReadSPFontSize.equals(ExpWaterReadSPFontSize))
			{
				System.out.println("WATER READ THE SUSTAINABILITY REPORT Text in Sustainability Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("WATER READ THE SUSTAINABILITY REPORT Text in Sustainability Page Font Size is not in match with creative - Fail");
				bw.write("WATER READ THE SUSTAINABILITY REPORT Text in Sustainability Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(WaterReadSPFontFamily.equals(ExpWaterReadSPFontFamily))
			{
				System.out.println("WATER READ THE SUSTAINABILITY REPORT Text in Sustainability Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("WATER READ THE SUSTAINABILITY REPORT Text in Sustainability Page Font Family is not in match with creative - Fail");
				bw.write("WATER READ THE SUSTAINABILITY REPORT Text in Sustainability Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			

			if(WaterReadSPContent.equals(ExpWaterReadSPContent))
			{
				System.out.println("WATER READ THE SUSTAINABILITY REPORT Content in Sustainability Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("WATER READ THE SUSTAINABILITY REPORT Content in Sustainability Page is not in match with creative - Fail");
				bw.write("WATER READ THE SUSTAINABILITY REPORT Content in Sustainability Page is not in match with creative - Fail"); 
			    bw.newLine();
			}
			
			if(HexWaterReadSPColor.equals(ExpWaterReadSPColor) && WaterReadSPFontSize.equals(ExpWaterReadSPFontSize) && WaterReadSPFontFamily.equals(ExpWaterReadSPFontFamily) && WaterReadSPContent.equals(ExpWaterReadSPContent))
			{
				Label WaterReadCSSP = new Label(5,31,"PASS"); 
				wsheet5.addCell(WaterReadCSSP); 
			}
			else
			{
				Label WaterReadCSSF = new Label(5,31,"FAIL"); 
				wsheet5.addCell(WaterReadCSSF); 
			}
			
			// Sustainability Page - WATER - LEARN ABOUT INCOME STABILITY
			
			WebElement WaterLearnSP = driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_2']/div[2]/div[2]/span[1]")); 
			String WaterLearnSPColor = WaterLearnSP.getCssValue("color");
			System.out.println("Actual Color of WATER - LEARN ABOUT INCOME STABILITY Text in Sustainability Page is : "	+ WaterLearnSPColor );
			// Convert RGB to Hex Value
			Color HWaterLearnSPColor = Color.fromString(WaterLearnSPColor);
			String HexWaterLearnSPColor = HWaterLearnSPColor.asHex();
			System.out.println("Actual Hex Color of WATER - LEARN ABOUT INCOME STABILITY Text in Sustainability Page is : "	+ HexWaterLearnSPColor );
			
			String WaterLearnSPFontSize = WaterLearnSP.getCssValue("font-size");
			System.out.println("Actual Font Size of WATER - LEARN ABOUT INCOME STABILITY Text in Sustainability Page is : "	+ WaterLearnSPFontSize);
			String WaterLearnSPFontFamily = WaterLearnSP.getCssValue("font-family");
			System.out.println("Actual Font Family of WATER - LEARN ABOUT INCOME STABILITY Text in Sustainability Page is : "	+ WaterLearnSPFontFamily);
			String WaterLearnSPContent = WaterLearnSP.getText();
			System.out.println("Actual Content of WATER - LEARN ABOUT INCOME STABILITY Text in Sustainability Page is : "	+ WaterLearnSPContent);
					
			String ExpWaterLearnSPColor = sheet5.getCell(1,32).getContents(); //column,row
			System.out.println("Expected Color of WATER - LEARN ABOUT INCOME STABILITY Text in Sustainability Page is : "	+ ExpWaterLearnSPColor );
			String ExpWaterLearnSPFontSize = sheet5.getCell(2,32).getContents();
			System.out.println("Expected Font Size of WATER - LEARN ABOUT INCOME STABILITY Text in Sustainability Page is : "	+ ExpWaterLearnSPFontSize);
			String ExpWaterLearnSPFontFamily = sheet5.getCell(3,32).getContents();
			System.out.println("Expected Font Family of WATER - LEARN ABOUT INCOME STABILITY Text in Sustainability Page is : "	+ ExpWaterLearnSPFontFamily);
			String ExpWaterLearnSPContent = sheet5.getCell(4,32).getContents();
			System.out.println("Expected Content of WATER - LEARN ABOUT INCOME STABILITY in Sustainability Page is : "	+ ExpWaterLearnSPContent);
					
			if(HexWaterLearnSPColor.equals(ExpWaterLearnSPColor))
			{
				System.out.println("WATER - LEARN ABOUT INCOME STABILITY Text in Sustainability Page Color is in match with creative - Pass");
			}
			else
			{
				System.out.println("WATER - LEARN ABOUT INCOME STABILITY Text in Sustainability Page Color is not in match with creative - Fail");
				bw.write("WATER - LEARN ABOUT INCOME STABILITY Text in Sustainability Page Color is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(WaterLearnSPFontSize.equals(ExpWaterLearnSPFontSize))
			{
				System.out.println("WATER - LEARN ABOUT INCOME STABILITY Text in Sustainability Page Font Size is in match with creative - Pass");
			}
			else
			{
				System.out.println("WATER - LEARN ABOUT INCOME STABILITY Text in Sustainability Page Font Size is not in match with creative - Fail");
				bw.write("WATER - LEARN ABOUT INCOME STABILITY Text in Sustainability Page Font Size is not in match with creative - Fail"); 
			    bw.newLine();
			}
					
			if(WaterLearnSPFontFamily.equals(ExpWaterLearnSPFontFamily))
			{
				System.out.println("WATER - LEARN ABOUT INCOME STABILITY Text in Sustainability Page Font Family is in match with creative - Pass");
			}
			else
			{
				System.out.println("WATER - LEARN ABOUT INCOME STABILITY Text in Sustainability Page Font Family is not in match with creative - Fail");
				bw.write("WATER - LEARN ABOUT INCOME STABILITY Text in Sustainability Page Font Family is not in match with creative - Fail"); 
			    bw.newLine();
			}
			
			if(WaterLearnSPContent.equals(ExpWaterLearnSPContent))
			{
				System.out.println("WATER - LEARN ABOUT INCOME STABILITY Content in Sustainability Page is in match with creative - Pass");
			}
			else
			{
				System.out.println("WATER - LEARN ABOUT INCOME STABILITY Content in Sustainability Page is not in match with creative - Fail");
				bw.write("WATER - LEARN ABOUT INCOME STABILITY Content in Sustainability Page is not in match with creative - Fail"); 
			    bw.newLine();
			}
			
			if(HexWaterLearnSPColor.equals(ExpWaterLearnSPColor) && WaterLearnSPFontSize.equals(ExpWaterLearnSPFontSize) && WaterLearnSPFontFamily.equals(ExpWaterLearnSPFontFamily) && WaterLearnSPContent.equals(ExpWaterLearnSPContent))
			{
				Label WaterLearnCSSP = new Label(5,32,"PASS"); 
				wsheet5.addCell(WaterLearnCSSP); 
			}
			else
			{
				Label WaterLearnCSSF = new Label(5,32,"FAIL"); 
				wsheet5.addCell(WaterLearnCSSF); 
			}
			
			// Sustainability Page - WATER - READ THE SUSTAINABILITY REPORT - Click
			
			String winHandleBeforeWR = driver.getWindowHandle();
			driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_2']/div[2]/div[1]/a")).click(); 
			Thread.sleep(5000);
			//Get the list of window handles
			ArrayList<String> tabsWR = new ArrayList<String>(driver.getWindowHandles());
			System.out.println(tabsWR.size());
			//Use the list of window handles to switch between windows
		    driver.switchTo().window((String) tabsWR.get(1));
			Thread.sleep(10000);
			String AWReadurl = driver.getCurrentUrl();
			System.out.println("Actual WATER - READ THE SUSTAINABILITY REPORT URL is: "+AWReadurl);
			String EWReadurl = sheet2.getCell(1,48).getContents();
			System.out.println("Expected WATER - READ THE SUSTAINABILITY REPORT URL is: "+EWReadurl);
			if(EWReadurl.equals(AWReadurl))
			{
				System.out.println("WATER - READ THE SUSTAINABILITY REPORT Page is Opened Correctly - Pass");
				Label WaterReadNAVP = new Label(2,48,"PASS"); 
				wsheet2.addCell(WaterReadNAVP); 
			}
			else
			{
				System.out.println("WATER - READ THE SUSTAINABILITY REPORT Page is not Opened Correctly - Fail");
				bw.write("WATER - READ THE SUSTAINABILITY REPORT Page is not Opened Correctly - Fail"); 
			    bw.newLine();
			    Label WaterReadNAVF = new Label(2,48,"FAIL"); 
				wsheet2.addCell(WaterReadNAVF); 
			}
			driver.close();
			Thread.sleep(5000);
			driver.switchTo().window(winHandleBeforeWR); // Home Window
			
			// Sustainability Page - WATER - Learn about Income Stability - Click
			
			driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_2']/div[2]/div[2]/span[1]")).click(); 
			Thread.sleep(10000);
			String AWLearnurl = driver.getCurrentUrl();
			System.out.println("Actual WATER - Learn about Income Stability URL is: "+AWLearnurl);
			String EWLearnurl = sheet2.getCell(1,49).getContents();
			System.out.println("Expected WATER - Learn about Income Stability URL is: "+EWLearnurl);
			if(EWLearnurl.equals(AWLearnurl))
			{
				System.out.println("WATER - Learn about Income Stability Page is Opened Correctly - Pass");
				Label WaterLearnNAVP = new Label(2,49,"PASS"); 
				wsheet2.addCell(WaterLearnNAVP); 
			}
			else
			{
				System.out.println("WATER - Learn about Income Stability is not Opened Correctly - Fail");
				bw.write("WATER - Learn about Income Stability is not Opened Correctly - Fail"); 
			    bw.newLine();
			    Label WaterLearnNAVF = new Label(2,49,"FAIL"); 
				wsheet2.addCell(WaterLearnNAVF); 
			}
			
			Thread.sleep(3000);
			workbookcopy.write();
			workbookcopy.close();
			workbook.close();
			bw.close();
  
		driver.close();
		driver.quit();
		
			}
			
			catch(Exception e)
			{
				System.out.println("Exception Is "+e);
				 
			}
				
    }
    
}
