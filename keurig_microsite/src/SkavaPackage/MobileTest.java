package SkavaPackage;

import java.util.*;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;


public class MobileTest {
	
    @Test()
    public void mobileEmulation() throws InterruptedException{
    	
    
        Map<String, String> mobileEmulation = new HashMap<String, String>();
        mobileEmulation.put("deviceName", "Apple iPhone 6");
 
        Map<String, Object> chromeOptions = new HashMap<String, Object>();
        chromeOptions.put("mobileEmulation", mobileEmulation);
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
        capabilities.setCapability("chrome.binary","C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe");
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\viyantharani.v\\Downloads\\chromedriver (2).exe");
        WebDriver driver = new ChromeDriver(capabilities);
 
        driver.get("https://www.greenmountaincoffee.com/");
        driver.manage().window().maximize();
        Thread.sleep(15000);
        
        /*
        
        // Click on PanCake
        
        driver.findElement(By.id("id_skgm_hw_panecakeIcon")).click();  
		Thread.sleep(5000);
		
		// Click on Quality Link in PanCake
        
        driver.findElement(By.xpath(".//*[@id='id_skgm_hw_accrdnCell']/div[2]/div/div[1]/a[2]/div")).click();  
		Thread.sleep(8000);
		driver.navigate().back();  // Browser Back
		
		// Click on Variety Link in PanCake
		driver.findElement(By.id("id_skgm_hw_panecakeIcon")).click();  
		Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='id_skgm_hw_accrdnCell']/div[2]/div/div[1]/a[3]/div")).click();  
		Thread.sleep(8000);
		driver.navigate().back();  // Browser Back
		
		// Click on Sustainability Link in PanCake
		driver.findElement(By.id("id_skgm_hw_panecakeIcon")).click();  
		Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='id_skgm_hw_accrdnCell']/div[2]/div/div[1]/a[4]/div")).click();  
		Thread.sleep(8000);
		driver.navigate().back();  // Browser Back
		
		// Click on Try a Sample Button in PanCake
		driver.findElement(By.id("id_skgm_hw_panecakeIcon")).click();  
		Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='id_skgm_hw_smplBtn']")).click();  
		Thread.sleep(5000);
		
		// Close Try a Sample Window
		
		driver.findElement(By.xpath(".//*[@id='background-header']/input")).click();  
		Thread.sleep(5000);
	
		// FaceBook in Pancake
		
		driver.findElement(By.id("id_skgm_hw_panecakeIcon")).click();  
		Thread.sleep(5000);
		String winHandleBeforef = driver.getWindowHandle();
		driver.findElement(By.xpath(".//*[@id='id_skgm_hw_accrdnCell']/div[2]/div/div[4]/div[2]/div[1]/div")).click(); 
		Thread.sleep(5000);
		//Get the list of window handles
		ArrayList<String> tabsf = new ArrayList<String>(driver.getWindowHandles());
		//System.out.println(tabsf.size());
		//Use the list of window handles to switch between windows
	    driver.switchTo().window((String) tabsf.get(1));
		Thread.sleep(10000);
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(winHandleBeforef); // Home Window
		Thread.sleep(5000);
		
		// Twitter in Pancake
	
		String winHandleBeforet = driver.getWindowHandle();
		driver.findElement(By.xpath(".//*[@id='id_skgm_hw_accrdnCell']/div[2]/div/div[4]/div[2]/div[3]/div")).click(); 
		Thread.sleep(5000);
		//Get the list of window handles
		ArrayList<String> tabst = new ArrayList<String>(driver.getWindowHandles());
		//System.out.println(tabst.size());
		//Use the list of window handles to switch between windows
	    driver.switchTo().window((String) tabst.get(1));
		Thread.sleep(10000);
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(winHandleBeforet); // Home Window
		Thread.sleep(5000);
		
		// Instagram in Pancake
		
		String winHandleBeforei = driver.getWindowHandle();
		driver.findElement(By.xpath(".//*[@id='id_skgm_hw_accrdnCell']/div[2]/div/div[4]/div[2]/div[5]/div")).click(); 
		Thread.sleep(5000);
		//Get the list of window handles
		ArrayList<String> tabsi = new ArrayList<String>(driver.getWindowHandles());
		//System.out.println(tabsi.size());
		//Use the list of window handles to switch between windows
	    driver.switchTo().window((String) tabsi.get(1));
		Thread.sleep(10000);
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(winHandleBeforei); // Home Window
		Thread.sleep(5000);
		
		// Close of Pancake
		driver.findElement(By.xpath(".//*[@id='id_skgm_hw_AccrdnCloseIcon']")).click();  
		Thread.sleep(5000);
		
		// Try a Sample Button in Home Page
		
		driver.findElement(By.xpath(".//*[@id='idSkcbannerTryButtonTxt_mob']")).click(); 
		Thread.sleep(5000);
		
		// Close Try a Sample Window in Home Page
		
		driver.findElement(By.xpath(".//*[@id='background-header']/input")).click();  
		Thread.sleep(5000);
        
        
        // Learn about our Keurig Brewers
		
		driver.findElement(By.id("idSkcbannerLearnMore_mob")).click();  
		Thread.sleep(5000);
		
		// Close Button of Learn about our Keurig Brewers
		
		driver.findElement(By.xpath(".//*[@id='sk_cls_lmPopupCont']/div[1]/div/div[1]")).click();  
		Thread.sleep(5000);
		
		// Watch the TV Commercial Video Icon in Home Page
		
		driver.findElement(By.xpath(".//*[@id='idSkcbannerWatchVideoFreeSample_mob']")).click(); 
		Thread.sleep(25000);
		
		// Close the Watch the TV Commercial Video Icon in Home Page
		
		driver.findElement(By.xpath(".//*[@id='id_skbcVdoClsBtn']/div[2]")).click(); 
		Thread.sleep(10000);
		
		*/
		
		JavascriptExecutor jsequality = (JavascriptExecutor)driver;
		jsequality.executeScript("window.scrollBy(0,1000)", "");
		Thread.sleep(5000);
		
		/*
		
		// Quality Video Icon in Home Page
		
		driver.findElement(By.xpath(".//*[@id='sk_id_quality']/div[2]/img")).click(); 
		Thread.sleep(25000);
		
		// Close the Quality Video Icon in Home Page
		
		driver.findElement(By.xpath(".//*[@id='id_skbcVdoClsBtn']")).click(); 
		Thread.sleep(10000);
		
		*/
		
		JavascriptExecutor jseMquality = (JavascriptExecutor)driver;
		jseMquality.executeScript("window.scrollBy(0,1000)", "");
		Thread.sleep(5000);
		
		/*
		
		// Click on More about Quality Button in Home Page
        
        driver.findElement(By.xpath(".//*[@id='sk_quality_more_Mob']/div[1]")).click();  
		Thread.sleep(8000);
		driver.navigate().back();  // Browser Back
		
		// Quality Carousel Dot 2
		
		driver.findElement(By.xpath(".//*[@id='mobTabContainer_quality']/div/div/div[3]/div[2]")).click();  
		Thread.sleep(8000);
		
		// Quality Carousel Dot 3
		
		driver.findElement(By.xpath(".//*[@id='mobTabContainer_quality']/div/div/div[3]/div[3]")).click();  
		Thread.sleep(8000);
		
		*/
		
		JavascriptExecutor jseVariety = (JavascriptExecutor)driver;
		jseVariety.executeScript("window.scrollBy(0,1000)", "");
		Thread.sleep(5000);
		
		/*
		
		// Variety Video Icon in Home Page
		
		driver.findElement(By.xpath(".//*[@id='sk_id_variety']/div[2]/img")).click(); 
		Thread.sleep(25000);
		
		// Close the Variety Video Icon in Home Page
		
		driver.findElement(By.xpath(".//*[@id='id_skbcVdoClsBtn']")).click(); 
		Thread.sleep(10000);
		
		JavascriptExecutor jsemoreV = (JavascriptExecutor)driver;
		jsemoreV.executeScript("window.scrollBy(0,1000)", "");
		Thread.sleep(5000);
		
		// Click on More about Variety Button in Home Page
        
        driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[4]/div/div/div/div[3]/div/div/div[1]/div/div[6]/div[1]")).click();  
		Thread.sleep(8000);
		driver.navigate().back();  // Browser Back
		
		*/
		
		JavascriptExecutor jseourcof = (JavascriptExecutor)driver;
		jseourcof.executeScript("window.scrollBy(0,600)", "");
		Thread.sleep(5000);
		
		// Click on Flavored in Variety - OUR COFFEES
		
		driver.findElement(By.xpath(".//*[@id='id_ourCoffScroller']/div[2]/img")).click(); 
		Thread.sleep(5000);
		
		// Click on Decaf in Variety - OUR COFFEES
		
		driver.findElement(By.xpath(".//*[@id='id_ourCoffScroller']/div[3]/img")).click(); 
		Thread.sleep(5000);
		
		// Click on Arrow in Variety - OUR COFFEES
		
		driver.findElement(By.xpath(".//*[@id='id_ourCoffeeSubCon']/div[3]")).click(); 
		Thread.sleep(5000);
		
		// Click on Seasonal in Variety - OUR COFFEES
		
		driver.findElement(By.xpath(".//*[@id='id_ourCoffScroller']/div[4]/img")).click(); 
		Thread.sleep(5000);
		
		// Click on CoffeeHouse in Variety - OUR COFFEES
		
		driver.findElement(By.xpath(".//*[@id='id_ourCoffScroller']/div[5]/img")).click(); 
		Thread.sleep(5000);
		
		// Click on Organic in Variety - OUR COFFEES
		
		driver.findElement(By.xpath(".//*[@id='id_ourCoffScroller']/div[6]/img")).click(); 
		Thread.sleep(5000);
		
		JavascriptExecutor jseSus = (JavascriptExecutor)driver;
		jseSus.executeScript("window.scrollBy(0,600)", "");
		Thread.sleep(5000);
		
        
        driver.close();
        driver.quit();
        
    }   
}



