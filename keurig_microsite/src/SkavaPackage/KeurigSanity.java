
// KEURIG SANITY

package SkavaPackage;

//import org.testng.annotations.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

import java.util.ArrayList;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;



public class KeurigSanity {
 
	public static WebDriver driver;
	public static File newFile = new File("D:\\Viyantha\\Automation\\Selenium Test Report\\KEURIG\\Keurig_FileInput.xls"); // Input Data for the Application
	public static Workbook workbook;
	public static WritableWorkbook workbookcopy;
	public static Workbook book;  // Excel Workbook Object
	public static Sheet sheet2;
	public static WritableSheet wsheet2;
	
	public static String idForTxtFile = new SimpleDateFormat("dd.MM.yyyy_HH.mm.ss").format(new Date());
	public static File file = new File("D:\\Viyantha\\Automation\\Selenium Test Report\\KEURIG\\KeurigOutputFile_" + idForTxtFile + ".txt");
	
	public static void main(String[] args) throws InterruptedException, IOException
	{
	
			try
			{
				HomePageNavigation();
				//QualityPage();
				//VarietyPageNavigation();
				//SustainabilityPageNavigation();
	
			}
			catch(Exception e)
			{
				System.out.println("Exception Is "+e);
				 
			}
	}
	
	public static void HomePageNavigation() throws InterruptedException, IOException, BiffException, RowsExceededException, WriteException {
		
		try{
			
		book = Workbook.getWorkbook(newFile);
		sheet2=book.getSheet("ExpectedURL'S");
		
		 // Write text on  text file
		
		FileWriter fw = new FileWriter(file, true);  
		BufferedWriter bw = new BufferedWriter(fw);
		
		workbook = Workbook.getWorkbook(new File("D:\\Viyantha\\Automation\\Selenium Test Report\\KEURIG\\Keurig_FileInput.xls"));
		workbookcopy = Workbook.createWorkbook(new File("D:\\Viyantha\\Automation\\Selenium Test Report\\KEURIG\\Keurig_FileOutput_" + idForTxtFile + ".xls"), workbook);
		
		wsheet2 = workbookcopy.getSheet(1);
		
		//DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		//capabilities.setCapability("chrome.binary","C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe");
		//System.setProperty("webdriver.chrome.driver", "C:\\Users\\viyantharani.v\\Downloads\\chromedriver (2).exe");
		//driver = new ChromeDriver(capabilities);
		
		driver = new FirefoxDriver();
		
		//DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
		//capabilities.setCapability("internetExplorer.binary","C:\\Program Files (x86)\\Internet Explorer\\iexplore.exe");
		//System.setProperty("webdriver.ie.driver", "C:\\Users\\viyantharani.v\\Downloads\\IEDriverServer_x64_2.53.0\\IEDriverServer.exe");
		//driver = new InternetExplorerDriver(capabilities);
		
		driver.manage().deleteAllCookies();
		String EHPurl = sheet2.getCell(1,1).getContents(); //column,row
		driver.get("https://www.greenmountaincoffee.com/"); // Keurig Production Home Page
		driver.manage().window().maximize();
		Thread.sleep(5000);
		String AHPurl = driver.getCurrentUrl();
		System.out.println("Expected Home Page URL is: "+EHPurl);
		System.out.println("Actual Home Page URL is: "+AHPurl);
		//Assert.assertEquals(AHPurl, "https://www.greenmountaincoffee.com/" );  //Actual,Expected - If Failed Execution Stops here
		if(EHPurl.equals(AHPurl))
		{
			System.out.println("Keurig URL is Opened Correctly - Pass");
			Label HPurlNAVP = new Label(2,1,"PASS"); 
			wsheet2.addCell(HPurlNAVP); 
		}
		else
		{
			System.out.println("Keurig URL is not Opened Correctly - Fail");
			bw.write("Keurig URL is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label HPurlNAVF = new Label(2,1,"FAIL"); 
			wsheet2.addCell(HPurlNAVF);
		}
			 
		// Header
		
		// Quality Link in Header
		
		driver.findElement(By.xpath(".//*[@id='id_skgm_hw_headerLayoutCell']/div[1]/div[2]/div/div[1]/a[1]/div")).click();  
		Thread.sleep(5000);
		String AQualityurl = driver.getCurrentUrl();
		System.out.println("Actual Quality Page URL is: "+AQualityurl);
		String EQualityurl = sheet2.getCell(1,4).getContents();
		System.out.println("Expected Quality Page URL is: "+EQualityurl);
		//Assert.assertEquals(AQualityurl, "https://www.greenmountaincoffee.com/quality" );
		if(EQualityurl.equals(AQualityurl))
		{
			System.out.println("Quality Page is Opened Correctly - Pass");
			Label QualityurlNAVP = new Label(2,4,"PASS"); 
			wsheet2.addCell(QualityurlNAVP); 
		}
		else
		{
			System.out.println("Quality Page is not Opened Correctly - Fail");
			bw.write("Quality Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label QualityurlNAVF = new Label(2,4,"FAIL"); 
			wsheet2.addCell(QualityurlNAVF);
		}
		
		driver.navigate().back();  // Browser Back
		Thread.sleep(10000);
		
		// Variety Link in Header
		
		driver.findElement(By.xpath(".//*[@id='id_skgm_hw_headerLayoutCell']/div[1]/div[2]/div/div[1]/a[2]/div")).click();  
		Thread.sleep(5000);
		String AVarietyurl = driver.getCurrentUrl();
		System.out.println("Actual Variety Page URL is: "+AVarietyurl);
		String EVarietyurl = sheet2.getCell(1,5).getContents();
		System.out.println("Expected Variety Page URL is: "+EVarietyurl);
		//Assert.assertEquals(AVarietyurl, "https://www.greenmountaincoffee.com/variety" );
		if(EVarietyurl.equals(AVarietyurl))
		{
			System.out.println("Variety Page is Opened Correctly - Pass");
			Label VarietyurlNAVP = new Label(2,5,"PASS"); 
			wsheet2.addCell(VarietyurlNAVP);
		}
		else
		{
			System.out.println("Variety Page is not Opened Correctly - Fail");
			bw.write("Variety Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label VarietyurlNAVF = new Label(2,5,"FAIL"); 
			wsheet2.addCell(VarietyurlNAVF);
		}
		driver.navigate().back();  // Browser Back
		Thread.sleep(5000);
	
		// Sustainability Link in Header
		
		driver.findElement(By.xpath(".//*[@id='id_skgm_hw_headerLayoutCell']/div[1]/div[2]/div/div[1]/a[3]/div")).click();  
		Thread.sleep(5000);
		String ASustainabilityurl = driver.getCurrentUrl();
		System.out.println("Actual Sustainability Page URL is: "+ASustainabilityurl);
		String ESustainabilityurl = sheet2.getCell(1,6).getContents();
		System.out.println("Expected Sustainability Page URL is: "+ESustainabilityurl);
		//Assert.assertEquals(ASustainabilityurl, "https://www.greenmountaincoffee.com/sustainability" );
		if(ESustainabilityurl.equals(ASustainabilityurl))
		{
			System.out.println("Sustainability Page is Opened Correctly - Pass");
			Label SustainabilityurlNAVP = new Label(2,6,"PASS"); 
			wsheet2.addCell(SustainabilityurlNAVP);
		}
		else
		{
			System.out.println("Sustainability Page is not Opened Correctly - Fail");
			bw.write("Sustainability Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label SustainabilityurlNAVF = new Label(2,6,"FAIL"); 
			wsheet2.addCell(SustainabilityurlNAVF);
		}
		driver.navigate().back();  // Browser Back
		Thread.sleep(5000);
			
		 // Facebook Icon in Header
		
		String winHandleBeforef = driver.getWindowHandle();
		driver.findElement(By.xpath(".//*[@id='id_skgm_hw_headerLayoutCell']/div[1]/div[2]/div/div[2]/div[3]")).click(); 
		Thread.sleep(5000);
		//Get the list of window handles
		ArrayList<String> tabsf = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(tabsf.size());
		//Use the list of window handles to switch between windows
	    driver.switchTo().window((String) tabsf.get(1));
		Thread.sleep(15000);
		String AFacebookurl = driver.getCurrentUrl();
		System.out.println("Actual Facebook Page URL is: "+AFacebookurl);
		String EFacebookurl = sheet2.getCell(1,7).getContents();
		System.out.println("Expected Facebook Page URL is: "+EFacebookurl);
		if(EFacebookurl.equals(AFacebookurl))
		{
			System.out.println("Facebook Page is Opened Correctly - Pass");
			Label FacebookurlNAVP = new Label(2,7,"PASS"); 
			wsheet2.addCell(FacebookurlNAVP);
		}
		else
		{
			System.out.println("Facebook Page is not Opened Correctly - Fail");
			bw.write("Facebook Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label FacebookurlNAVF = new Label(2,7,"FAIL"); 
			wsheet2.addCell(FacebookurlNAVF);
		}
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(winHandleBeforef); // Home Window
		
		 // Twitter Icon in Header
		
		String winHandleBeforet = driver.getWindowHandle();
		driver.findElement(By.xpath(".//*[@id='id_skgm_hw_headerLayoutCell']/div[1]/div[2]/div/div[2]/div[5]/div")).click(); 
		Thread.sleep(5000);
		//Get the list of window handles
		ArrayList<String> tabst = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(tabst.size());
		//Use the list of window handles to switch between windows
		driver.switchTo().window((String) tabst.get(1));
		Thread.sleep(15000);
		String ATwitterurl = driver.getCurrentUrl();
		System.out.println("Actual Twitter Page URL is: "+ATwitterurl);
		String ETwitterurl = sheet2.getCell(1,8).getContents();
		System.out.println("Expected Twitter Page URL is: "+ETwitterurl);
		if(ETwitterurl.equals(ATwitterurl))
		{
			System.out.println("Twitter Page is Opened Correctly - Pass");
			Label TwitterurlNAVP = new Label(2,8,"PASS"); 
			wsheet2.addCell(TwitterurlNAVP);
		}
		else
		{
			System.out.println("Twitter Page is not Opened Correctly - Fail");
			bw.write("Twitter Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label TwitterurlNAVF = new Label(2,8,"FAIL"); 
			wsheet2.addCell(TwitterurlNAVF);
		}
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(winHandleBeforet);
	
		// Pinterest Icon in Header
		
		String winHandleBeforep = driver.getWindowHandle();
		driver.findElement(By.xpath(".//*[@id='id_skgm_hw_headerLayoutCell']/div[1]/div[2]/div/div[2]/div[7]")).click();  
		Thread.sleep(5000);
		//Get the list of window handles
		ArrayList<String> tabsp = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(tabsp.size());
		//Use the list of window handles to switch between windows
		driver.switchTo().window((String) tabsp.get(1));
		Thread.sleep(15000);
		String APinteresturl = driver.getCurrentUrl();
		System.out.println("Actual Pinterest Page URL is: "+APinteresturl);
		String EPinteresturl = sheet2.getCell(1,9).getContents();
		System.out.println("Expected Pinterest Page URL is: "+EPinteresturl);
		if(EPinteresturl.equals(APinteresturl))
		{
			System.out.println("Pinterest Page is Opened Correctly - Pass");
			Label PinteresturlNAVP = new Label(2,9,"PASS"); 
			wsheet2.addCell(PinteresturlNAVP);
		}
		else
		{
			System.out.println("Pinterest Page is not Opened Correctly - Fail");
			bw.write("Pinterest Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label PinteresturlNAVF = new Label(2,9,"FAIL"); 
			wsheet2.addCell(PinteresturlNAVF);
		}
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(winHandleBeforep);
		
		 // Shop at Keurig.com in Header
		
		String winHandleBefores = driver.getWindowHandle();
		driver.findElement(By.xpath(".//*[@id='id_skgm_hw_headerLayoutCell']/div[1]/div[2]/div/div[2]/a/div[1]")).click(); 
		Thread.sleep(5000);
		//Get the list of window handles
		ArrayList<String> tabss = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(tabss.size());
		//Use the list of window handles to switch between windows
		driver.switchTo().window((String) tabss.get(1));
		Thread.sleep(15000);
		String AShopurl = driver.getCurrentUrl();
		System.out.println("Actual Shop at Keurig.com URL is: "+AShopurl);
		String EShopurl = sheet2.getCell(1,10).getContents();
		System.out.println("Expected Shop at Keurig.com URL is: "+EShopurl);
		if(EShopurl.equals(AShopurl))
		{
			System.out.println("Shop at Keurig.com Page is Opened Correctly - Pass");
			Label ShopurlNAVP = new Label(2,10,"PASS"); 
			wsheet2.addCell(ShopurlNAVP);
		}
		else
		{
			System.out.println("Shop at Keurig.com Page is not Opened Correctly - Fail");
			bw.write("Shop at Keurig.com Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label ShopurlNAVF = new Label(2,10,"FAIL"); 
			wsheet2.addCell(ShopurlNAVF);
		}
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(winHandleBefores);
		
		 // Try a Sample Button
		
		driver.findElement(By.id("idSkcbannerTryButtonTxt")).click(); 
		Thread.sleep(5000);
		String ATrySampleurl = driver.getCurrentUrl();
		System.out.println("Actual Try a Sample Page URL is: "+ATrySampleurl);
		String ETrySampleurl = sheet2.getCell(1,12).getContents();
		System.out.println("Expected Try a Sample Page URL is: "+ETrySampleurl);
		if(ETrySampleurl.equals(ATrySampleurl))
		{
			System.out.println("Try a Sample Page is Opened Correctly - Pass");
			Label TrySampleurlNAVP = new Label(2,12,"PASS"); 
			wsheet2.addCell(TrySampleurlNAVP);
		}
		else
		{
			System.out.println("Try a Sample Page is not Opened Correctly - Fail");
			bw.write("Try a Sample Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label TrySampleurlNAVF = new Label(2,12,"FAIL"); 
			wsheet2.addCell(TrySampleurlNAVF);
		}
		driver.navigate().back();  // Browser Back
		Thread.sleep(5000);
		
		// Learn about our Keurig Brewers
		
		driver.findElement(By.id("idSkcbannerfreesampleLinkTxt")).click();  
		Thread.sleep(5000);
		
		// Learn More at Keurig.com Button
		
		String winHandleBeforeL = driver.getWindowHandle();
		driver.findElement(By.className("sk_cls_lmPopupLearnMore")).click();  
		Thread.sleep(5000);
		//Get the list of window handles
		ArrayList<String> tabsl = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(tabsl.size());
		//Use the list of window handles to switch between windows
		driver.switchTo().window((String) tabsl.get(1));
		Thread.sleep(5000);
		String ALearnmoreurl = driver.getCurrentUrl();
		System.out.println("Actual Learn More at Keurig.com Page URL is: "+ALearnmoreurl);
		String ELearnmoreurl = sheet2.getCell(1,13).getContents();
		System.out.println("Expected Try a Sample Page URL is: "+ELearnmoreurl);
		if(ELearnmoreurl.equals(ALearnmoreurl))
		{
			System.out.println("Learn More at Keurig.com Page is Opened Correctly - Pass");
			Label LearnmoreurlNAVP = new Label(2,13,"PASS"); 
			wsheet2.addCell(LearnmoreurlNAVP);
		}
		else
		{
			System.out.println("Learn More at Keurig.com Page is not Opened Correctly - Fail");
			bw.write("Learn More at Keurig.com Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label LearnmoreurlNAVF = new Label(2,13,"FAIL"); 
			wsheet2.addCell(LearnmoreurlNAVF);
		}
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(winHandleBeforeL);
		
		// Close Learn about our Keurig Brewers
		
		driver.findElement(By.className("sk_cls_closeButtonText")).click();  
		Thread.sleep(5000);
		
		// Watch TV Commercials Video
		
		driver.findElement(By.xpath(".//*[@id='idSkcbannerWatchVideoFreeSample']/div[1]/div")).click();  
		Thread.sleep(25000);
		
		// Close - Watch TV Commercials Video
		
		driver.findElement(By.xpath(".//*[@id='id_skbcVdoClsBtn']/div[2]")).click();  
		Thread.sleep(5000);
		
		// Quality Video
		
		driver.findElement(By.xpath(".//*[@id='sk_id_quality']/div[2]/img")).click();  
		Thread.sleep(25000);
		
		// Close - Quality Video
		
		driver.findElement(By.xpath(".//*[@id='id_skbcVdoClsBtn']")).click();  
		Thread.sleep(5000);
		
		// FireFox
		
		JavascriptExecutor jsemq = (JavascriptExecutor)driver;
		jsemq.executeScript("window.scrollBy(0,250)", "");
		Thread.sleep(5000);
		
		// Chrome
		
		//JavascriptExecutor jsemq = (JavascriptExecutor)driver;
		//jsemq.executeScript("window.scrollBy(0,300)", "");
		//Thread.sleep(5000);
		
		// More about Quality Button
		
		driver.findElement(By.className("sk_quality_more_text")).click(); 
		Thread.sleep(5000);
		String AMoreQualityurl = driver.getCurrentUrl();
		System.out.println("Actual More about Quality Page URL is: "+AMoreQualityurl);
		String EMoreQualityurl = sheet2.getCell(1,14).getContents();
		System.out.println("Expected More about Quality Page URL is: "+EMoreQualityurl);
		if(EMoreQualityurl.equals(AMoreQualityurl))
		{
			System.out.println("More about Quality Page is Opened Correctly - Pass");
			Label MoreQualityurlNAVP = new Label(2,14,"PASS"); 
			wsheet2.addCell(MoreQualityurlNAVP);
		}
		else
		{
			System.out.println("More about Quality Page is not Opened Correctly - Fail");
			bw.write("More about Quality Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label MoreQualityurlNAVF = new Label(2,14,"FAIL"); 
			wsheet2.addCell(MoreQualityurlNAVF);
		}
		driver.navigate().back();  // Browser Back
		
		// FireFox
		Thread.sleep(10000);
		
		// Chrome
		//Thread.sleep(13000);
		
		// Quality Number tab 2
		// Used Absolute X-Path
		
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[4]/div/div[2]/div[2]/div[2]")).click();  
		Thread.sleep(5000);
		
		// Quality Number tab 3
		
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[4]/div/div[2]/div[2]/div[3]")).click();  
		Thread.sleep(5000);
		
		// Quality Number tab 1
		
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[4]/div/div[2]/div[2]/div[1]")).click();  
		Thread.sleep(5000);

		JavascriptExecutor jsevv = (JavascriptExecutor)driver;
		jsevv.executeScript("window.scrollBy(0,250)", "");
		Thread.sleep(10000);
		
		WebDriverWait waitvarietyvideo = new WebDriverWait(driver, 15);
		waitvarietyvideo.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='sk_id_variety']/div[2]/img")));
		// Variety Video
		driver.findElement(By.xpath(".//*[@id='sk_id_variety']/div[2]/img")).click();  
		Thread.sleep(25000);
		
		// Close - Variety Video
		
		driver.findElement(By.id("id_skbcVdoClsBtn")).click();  
		Thread.sleep(10000);
		
		// FireFox - Comment and Chrome - Uncomment
		
		//JavascriptExecutor jsevf = (JavascriptExecutor)driver;
		//jsevf.executeScript("window.scrollBy(0,800)", "");
		//Thread.sleep(10000);
		
		driver.findElement(By.xpath(".//*[@id='id_ourCoffScroller']/div[3]/img")).click();  // Variety - Our Coffees - Decaf
		Thread.sleep(8000);
		
		driver.findElement(By.xpath(".//*[@id='id_ourCoffeeSubCon']/div[3]")).click();  // Variety - Our Coffees - Arrow Symbol
		Thread.sleep(8000);
		
		driver.findElement(By.xpath(".//*[@id='id_ourCoffScroller']/div[5]/img")).click();  // Variety - Our Coffees - Coffeehouse
		Thread.sleep(8000);
		
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[6]/div/div[3]/div/div[5]/div")).click(); // More about Variety Button
		Thread.sleep(10000);
		String AMoreVarietyurl = driver.getCurrentUrl();
		System.out.println("Actual More about Variety Page URL is: "+AMoreVarietyurl);
		String EMoreVarietyurl = sheet2.getCell(1,15).getContents();
		System.out.println("Expected More about Variety Page URL is: "+AMoreVarietyurl);
		if(EMoreVarietyurl.equals(AMoreVarietyurl))
		{
			System.out.println("More about Variety Page is Opened Correctly - Pass");
			Label MoreVarietyurlNAVP = new Label(2,15,"PASS"); 
			wsheet2.addCell(MoreVarietyurlNAVP);
		}
		else
		{
			System.out.println("More about Variety Page is not Opened Correctly - Fail");
			bw.write("More about Variety Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label MoreVarietyurlNAVF = new Label(2,15,"FAIL"); 
			wsheet2.addCell(MoreVarietyurlNAVF);
		}
		driver.navigate().back();  // Browser Back
		Thread.sleep(15000);
		
		JavascriptExecutor jsesv = (JavascriptExecutor)driver;
		jsesv.executeScript("window.scrollBy(0,900)", "");
		Thread.sleep(15000);
		
		driver.findElement(By.xpath(".//*[@id='sk_id_responsibility']/div[2]/img")).click();  // Sustainability Video
		Thread.sleep(25000);
		
		driver.findElement(By.xpath(".//*[@id='id_skbcVdoClsBtn']")).click();  // Close -Sustainability Video
		Thread.sleep(15000);
		
		JavascriptExecutor jsess = (JavascriptExecutor)driver;
		jsess.executeScript("window.scrollBy(0,600)", "");
		Thread.sleep(15000);
		
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[8]/div/div[2]/div[2]/div[2]")).click();  // Sustainability Number tab 2
		Thread.sleep(5000);
		
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[8]/div/div[2]/div[2]/div[3]")).click();  // Sustainability Number tab 3
		Thread.sleep(5000);
		
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[8]/div/div[2]/div[2]/div[1]")).click();  // Sustainability Number tab 1
		Thread.sleep(10000);
		
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[8]/div/div[1]/div/div[5]/div")).click(); // More about Sustainability Button
		Thread.sleep(5000);
		String AMoreSustainabilityurl = driver.getCurrentUrl();
		System.out.println("Actual More about Sustainability Page URL is: "+AMoreSustainabilityurl);
		String EMoreSustainabilityurl = sheet2.getCell(1,16).getContents();
		System.out.println("Expected More about Sustainability Page URL is: "+EMoreSustainabilityurl);
		if(EMoreSustainabilityurl.equals(AMoreSustainabilityurl))
		{
			System.out.println("More about Sustainability Page is Opened Correctly - Pass");
			Label MoreSustainabilityurlNAVP = new Label(2,16,"PASS"); 
			wsheet2.addCell(MoreSustainabilityurlNAVP);
		}
		else
		{
			System.out.println("More about Sustainability Page is not Opened Correctly - Fail");
			bw.write("More about Sustainability Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label MoreSustainabilityurlNAVF = new Label(2,16,"FAIL"); 
			wsheet2.addCell(MoreSustainabilityurlNAVF);
		}
		driver.navigate().back();  // Browser Back
		Thread.sleep(5000);
		
		JavascriptExecutor jseft = (JavascriptExecutor)driver;
		jseft.executeScript("window.scrollBy(0,250)", "");
		Thread.sleep(15000);
	
		// Footer 
		
		driver.findElement(By.xpath(".//*[@id='id_skgm_fw_footerLayoutCell']/div/div[2]/div[1]/div/a[3]/div")).click();  // Variety Link in Footer
		Thread.sleep(5000);
		String AVarietyurlF = driver.getCurrentUrl();
		System.out.println("Footer - Actual Variety Page URL is: "+AVarietyurlF);
		String EVarietyurlF = sheet2.getCell(1,5).getContents();
		System.out.println("Footer - Expected Variety Page URL is: "+EVarietyurlF);
		if(EVarietyurlF.equals(AVarietyurlF))
		{
			System.out.println("Variety Page is Opened Correctly in Footer - Pass");
			Label VarietyurlFNAVP = new Label(2,20,"PASS"); 
			wsheet2.addCell(VarietyurlFNAVP);
		}
		else
		{
			System.out.println("Variety Page is not Opened Correctly in Footer - Fail");
			bw.write("Variety Page is not Opened Correctly in Footer - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label VarietyurlFNAVF = new Label(2,20,"FAIL"); 
			wsheet2.addCell(VarietyurlFNAVF);
		}
		driver.navigate().back();  // Browser Back
		Thread.sleep(5000);
		
		driver.findElement(By.xpath(".//*[@id='id_skgm_fw_footerLayoutCell']/div/div[2]/div[1]/div/a[4]/div")).click();  // Sustainability Link in Footer
		Thread.sleep(5000);
		String ASustainabilityurlF = driver.getCurrentUrl();
		System.out.println("Footer - Actual Sustainability Page URL is: "+ASustainabilityurlF);
		String ESustainabilityurlF = sheet2.getCell(1,6).getContents();
		System.out.println("Footer - Expected Sustainability Page URL is: "+ESustainabilityurlF);
		if(ESustainabilityurlF.equals(ASustainabilityurlF))
		{
			System.out.println("Sustainability Page is Opened Correctly in Footer - Pass");
			Label SustainabilityurlFNAVP = new Label(2,21,"PASS"); 
			wsheet2.addCell(SustainabilityurlFNAVP);
		}
		else
		{
			System.out.println("Sustainability Page is not Opened Correctly in Footer - Fail");
			bw.write("Sustainability Page is not Opened Correctly in Footer - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label SustainabilityurlFNAVF = new Label(2,21,"FAIL"); 
			wsheet2.addCell(SustainabilityurlFNAVF);
		}
		driver.navigate().back();  // Browser Back
		Thread.sleep(5000);
		
		String FwinHandleBeforefb = driver.getWindowHandle();
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[9]/div/div/div[2]/div/div[2]/div[2]/div[2]/div[2]/div")).click();  // Facebook Icon in Footer
		Thread.sleep(5000);
		//Get the list of window handles
		ArrayList<String> Ftabsfb = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(Ftabsfb.size());
		//Use the list of window handles to switch between windows
	    driver.switchTo().window((String) Ftabsfb.get(1));
		Thread.sleep(15000);
		String FBAFacebookurl = driver.getCurrentUrl();
		System.out.println("Footer - Actual Facebook Page URL is: "+FBAFacebookurl);
		String FBEFacebookurl = sheet2.getCell(1,22).getContents();
		System.out.println("Footer - Expected Facebook Page URL is: "+FBEFacebookurl);
		if(FBEFacebookurl.equals(FBAFacebookurl))
		{
			System.out.println("Footer - Facebook Page is Opened Correctly - Pass");
			Label FFBAFacebookurlNAVP = new Label(2,22,"PASS"); 
			wsheet2.addCell(FFBAFacebookurlNAVP);
		}
		else
		{
			System.out.println("Footer - Facebook Page is not Opened Correctly - Fail");
			bw.write("Footer - Facebook Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label FFBAFacebookurlNAVF = new Label(2,22,"FAIL"); 
			wsheet2.addCell(FFBAFacebookurlNAVF);
		}
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(FwinHandleBeforefb); // Home Window
		
		String FwinHandleBeforetf = driver.getWindowHandle();
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[9]/div/div/div[2]/div/div[2]/div[2]/div[2]/div[3]/div")).click();  // Twitter Icon in Footer
		Thread.sleep(5000);
		//Get the list of window handles
		ArrayList<String> Ftabstf = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(Ftabstf.size());
		//Use the list of window handles to switch between windows
		driver.switchTo().window((String) Ftabstf.get(1));
		Thread.sleep(20000);
		String FATwitterurlf = driver.getCurrentUrl();
		System.out.println("Footer - Actual Twitter Page URL is: "+FATwitterurlf);
		String FETwitterurlf = sheet2.getCell(1,23).getContents();
		System.out.println("Footer - Expected Twitter Page URL is: "+FETwitterurlf);
		if(FETwitterurlf.equals(FATwitterurlf))
		{
			System.out.println("Footer - Twitter Page is Opened Correctly - Pass");
			Label FATwitterurlfNAVP = new Label(2,23,"PASS"); 
			wsheet2.addCell(FATwitterurlfNAVP);
		}
		else
		{
			System.out.println("Twitter Page is not Opened Correctly - Fail");
			bw.write("Twitter Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label FATwitterurlfNAVF = new Label(2,23,"FAIL"); 
			wsheet2.addCell(FATwitterurlfNAVF);
		}
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(FwinHandleBeforetf);
		
		String FwinHandleBeforei = driver.getWindowHandle();
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[9]/div/div/div[2]/div/div[2]/div[2]/div[2]/div[4]/div")).click();  // Instagram Icon in Footer
		Thread.sleep(5000);
		//Get the list of window handles
		ArrayList<String> Ftabsi = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(Ftabsi.size());
		//Use the list of window handles to switch between windows
		driver.switchTo().window((String) Ftabsi.get(1));
		Thread.sleep(15000);
		String Ainstagramurli = driver.getCurrentUrl();
		System.out.println(" Footer - Actual Instagram Page URL is: "+Ainstagramurli);
		String Einstagramurli = sheet2.getCell(1,24).getContents();
		System.out.println(" Footer - Expected Instagram Page URL is: "+Einstagramurli);
		if(Einstagramurli.equals(Ainstagramurli))
		{
			System.out.println("Footer - Instagram Page is Opened Correctly - Pass");
			Label FAinstagramurliNAVP = new Label(2,24,"PASS"); 
			wsheet2.addCell(FAinstagramurliNAVP);
		}
		else
		{
			System.out.println("Footer - Instagram Page is not Opened Correctly - Fail");
			bw.write("Footer - Instagram Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label FAinstagramurliNAVF = new Label(2,24,"FAIL"); 
			wsheet2.addCell(FAinstagramurliNAVF);
		}
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(FwinHandleBeforei);
		
		String winHandleBeforetou = driver.getWindowHandle();
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[9]/div/div/div[2]/div/div[3]/div/div[1]/a/div")).click();  // Terms of Use Link in Footer
		Thread.sleep(5000);
		//Get the list of window handles
		ArrayList<String> Ftabstou = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(Ftabstou.size());
		//Use the list of window handles to switch between windows
	    driver.switchTo().window((String) Ftabstou.get(1));
		Thread.sleep(15000);
		String ATermsofUseurl = driver.getCurrentUrl();
		System.out.println("Footer - Actual Terms of Use Page URL is: "+ATermsofUseurl);
		String ETermsofUseurl = sheet2.getCell(1,25).getContents();
		System.out.println("Footer - Expected Terms of Use Page URL is: "+ETermsofUseurl);
		if(ETermsofUseurl.equals(ATermsofUseurl))
		{
			System.out.println("Footer - Terms of Use Page is Opened Correctly - Pass");
			Label TermsofUseurlNAVP = new Label(2,25,"PASS"); 
			wsheet2.addCell(TermsofUseurlNAVP);
		}
		else
		{
			System.out.println("Footer - Terms of Use Page is not Opened Correctly - Fail");
			bw.write("Footer - Terms of Use Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label TermsofUseurlNAVF = new Label(2,25,"FAIL"); 
			wsheet2.addCell(TermsofUseurlNAVF);
		}
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(winHandleBeforetou); // Home Window
		
		String winHandleBeforepp = driver.getWindowHandle();
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[9]/div/div/div[2]/div/div[3]/div/div[2]/a/div")).click();  // Privacy Link in Footer
		Thread.sleep(5000);
		//Get the list of window handles
		ArrayList<String> Ftabspp = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(Ftabspp.size());
		//Use the list of window handles to switch between windows
	    driver.switchTo().window((String) Ftabspp.get(1));
		Thread.sleep(15000);
		String Aprivacyurl = driver.getCurrentUrl();
		System.out.println("Footer - Actual Privacy Page URL is: "+Aprivacyurl);
		String Eprivacyurl = sheet2.getCell(1,26).getContents();
		System.out.println("Footer - Expected Privacy Page URL is: "+Eprivacyurl);
		if(Eprivacyurl.equals(Aprivacyurl))
		{
			System.out.println("Footer - Privacy Page is Opened Correctly - Pass");
			Label privacyurlNAVP = new Label(2,26,"PASS"); 
			wsheet2.addCell(privacyurlNAVP);
		}
		else
		{
			System.out.println("Footer - Privacy Page is not Opened Correctly - Fail");
			bw.write("Footer - Privacy Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label privacyurlNAVF = new Label(2,26,"FAIL"); 
			wsheet2.addCell(privacyurlNAVF);
		}
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(winHandleBeforepp); // Home Window
		
		// Contact Us Link in Footer
		
		// driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[9]/div/div/div[2]/div/div[3]/div/div[3]/a/div")).click();  
		// Thread.sleep(5000);
		
		String winHandleBeforeca = driver.getWindowHandle();
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[9]/div/div/div[2]/div/div[3]/div/div[4]/a/div")).click();  // CA Transparency Act Link in Footer
		Thread.sleep(5000);
		//Get the list of window handles
		ArrayList<String> Ftabsca = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(Ftabsca.size());
		//Use the list of window handles to switch between windows
	    driver.switchTo().window((String) Ftabsca.get(1));
		Thread.sleep(15000);
		String ACAurl = driver.getCurrentUrl();
		System.out.println("Footer - Actual CA Transparency Act Page URL is: "+ACAurl);
		String ECAurl = sheet2.getCell(1,28).getContents();
		System.out.println("Footer - Expected CA Transparency Act Page URL is: "+ECAurl);
		if(ECAurl.equals(ACAurl))
		{
			System.out.println("Footer - CA Transparency Act Page is Opened Correctly - Pass");
			Label CAurlNAVP = new Label(2,28,"PASS"); 
			wsheet2.addCell(CAurlNAVP);
		}
		else
		{
			System.out.println("Footer - CA Transparency Act Page is not Opened Correctly - Fail");
			bw.write("Footer - CA Transparency Act Page is not Opened Correctly - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label CAurlNAVF = new Label(2,28,"FAIL"); 
			wsheet2.addCell(CAurlNAVF);
		}
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(winHandleBeforeca); // Home Window
		
		
		driver.findElement(By.xpath(".//*[@id='id_skgm_fw_footerLayoutCell']/div/div[2]/div[1]/div/a[2]/div")).click();  // Quality Link in Footer
		Thread.sleep(5000);
		String AQualityurlF = driver.getCurrentUrl();
		System.out.println("Footer - Actual Quality Page URL is: "+AQualityurlF);
		String EQualityurlF = sheet2.getCell(1,4).getContents();
		System.out.println("Footer - Expected Quality Page URL is: "+EQualityurlF);
		if(EQualityurlF.equals(AQualityurlF))
		{
			System.out.println("Quality Page is Opened Correctly in Footer - Pass");
			Label QualityurlFNAVP = new Label(2,19,"PASS"); 
			wsheet2.addCell(QualityurlFNAVP);
		}
		else
		{
			System.out.println("Quality Page is not Opened Correctly in Footer - Fail");
			bw.write("Quality Page is not Opened Correctly in Footer - Fail"); 
			//Adding new line 
		    bw.newLine();
		    Label QualityurlFNAVF = new Label(2,19,"FAIL"); 
			wsheet2.addCell(QualityurlFNAVF);
		}
	
		bw.close();
			
		Thread.sleep(10000);
		workbookcopy.write();
		workbookcopy.close();
		workbook.close();
		
		driver.close();
		driver.quit();
		
		}
		
		catch(Exception e)
		{
			System.out.println("Exception Is "+e);
			 
		}
		
	}
	
	
	public static void QualityPage() throws InterruptedException, BiffException, IOException, RowsExceededException, WriteException {	
    	
    	try{
         	
       	book = Workbook.getWorkbook(newFile);
   		//sheet3=book.getSheet("QualityPageCSS");
   		
   		//workbook = Workbook.getWorkbook(new File("D:\\Viyantha\\Automation\\Selenium Test Report\\KEURIG\\Keurig_FileInput.xls"));
		//workbookcopy = Workbook.createWorkbook(new File("D:\\Viyantha\\Automation\\Selenium Test Report\\KEURIG\\Keurig_FileOutput_" + idForTxtFile + ".xls"), workbook);
		
		//wsheet3 = workbookcopy.getSheet(2);
   		
   		// Write text on  text file
   		
   		FileWriter fw = new FileWriter(file, true);  
   		BufferedWriter bw = new BufferedWriter(fw);
   		
   		Thread.sleep(3000);
		JavascriptExecutor jseqpFind = (JavascriptExecutor)driver;
		jseqpFind.executeScript("window.scrollBy(0,1000)", "");
		Thread.sleep(3000);
   		
		JavascriptExecutor jseqpnt = (JavascriptExecutor)driver;
		jseqpnt.executeScript("window.scrollBy(0,1000)", "");
		Thread.sleep(5000);
		
		// Click Action on Number Tab 2 in QP
		
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[8]/div/div/div/div/div[3]/div[2]/div[2]")).click(); 
		Thread.sleep(5000);
   		
		//Click Action on Number Tab 3
		
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[8]/div/div/div/div/div[3]/div[2]/div[3]")).click(); 
		Thread.sleep(5000);
		
		JavascriptExecutor jsemaking = (JavascriptExecutor)driver;
		jsemaking.executeScript("window.scrollBy(0,500)", "");
		Thread.sleep(4000);
		
		JavascriptExecutor jseexporter = (JavascriptExecutor)driver;
		jseexporter.executeScript("window.scrollBy(0,800)", "");
		Thread.sleep(5000);
	
		JavascriptExecutor jsearts = (JavascriptExecutor)driver;
		jsearts.executeScript("window.scrollBy(0,600)", "");
		Thread.sleep(4000);
		
		JavascriptExecutor jsetypes = (JavascriptExecutor)driver;
		jsetypes.executeScript("window.scrollBy(0,500)", "");
		Thread.sleep(4000);
	
		JavascriptExecutor jsedark = (JavascriptExecutor)driver;
		jsedark.executeScript("window.scrollBy(0,500)", "");
		Thread.sleep(4000);
		
		JavascriptExecutor jseFTouches = (JavascriptExecutor)driver;
		jseFTouches.executeScript("window.scrollBy(0,700)", "");
		Thread.sleep(10000);
		
		// Variety Link in Header of QP
		
		driver.findElement(By.xpath(".//*[@id='id_skgm_hw_headerLayoutCell']/div[1]/div[2]/div/div[1]/a[2]/div")).click();
		Thread.sleep(10000);
   		
   		
   		bw.close();
   		
   		
    	}
    	
    	catch(Exception e)
		{
			System.out.println("Exception Is "+e);
			 
		}
    	
	}
		
		public static void VarietyPageNavigation() throws InterruptedException, IOException, BiffException, RowsExceededException, WriteException {
			
	    	try{
	    		
			book = Workbook.getWorkbook(newFile);
			sheet2=book.getSheet("ExpectedURL'S");
			
			wsheet2 = workbookcopy.getSheet(1);
			
			 // Write text on  text file
			
			FileWriter fw = new FileWriter(file, true);  
			BufferedWriter bw = new BufferedWriter(fw);
			
			// Shop on Keurig.com Button in Variety Page Main Banner
			
			String winHandleBeforemb = driver.getWindowHandle();
			driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[2]/div/div/div/div[1]/a/div")).click(); 
			Thread.sleep(5000);
			//Get the list of window handles
			ArrayList<String> tabsmb = new ArrayList<String>(driver.getWindowHandles());
			System.out.println(tabsmb.size());
			//Use the list of window handles to switch between windows
		    driver.switchTo().window((String) tabsmb.get(1));
			Thread.sleep(10000);
			String AShoponKeurigurl = driver.getCurrentUrl();
			System.out.println("Actual Shop on Keurig.com URL is: "+AShoponKeurigurl);
			String EShoponKeurigurl = sheet2.getCell(1,30).getContents();
			System.out.println("Expected Shop on Keurig.com URL is: "+EShoponKeurigurl);
			if(EShoponKeurigurl.equals(AShoponKeurigurl))
			{
				System.out.println("Shop on Keurig.com URL is Opened Correctly - Pass");
				Label ShoponKeurigNAVP = new Label(2,30,"PASS"); 
				wsheet2.addCell(ShoponKeurigNAVP); 
			}
			else
			{
				System.out.println("Shop on Keurig.com URL is not Opened Correctly - Fail");
				bw.write("Shop on Keurig.com URL is not Opened Correctly - Fail"); 
			    bw.newLine();
			    Label ShoponKeurigNAVF = new Label(2,30,"FAIL"); 
				wsheet2.addCell(ShoponKeurigNAVF); 
			}
			driver.close();
			Thread.sleep(5000);
			driver.switchTo().window(winHandleBeforemb); // Home Window
			
			// Number Tab 1 - Shop Now on Keurig.com Link in Variety Page 
			
			String winHandleBeforen1 = driver.getWindowHandle();
			driver.findElement(By.xpath(".//*[@id='id_1_body']/div[2]/div[2]/a")).click(); 
			Thread.sleep(5000);
			//Get the list of window handles
			ArrayList<String> tabsn1 = new ArrayList<String>(driver.getWindowHandles());
			System.out.println(tabsn1.size());
			//Use the list of window handles to switch between windows
		    driver.switchTo().window((String) tabsn1.get(1));
			Thread.sleep(10000);
			String AShoponKeurigN1url = driver.getCurrentUrl();
			System.out.println("Actual Shop Now on Keurig.com URL in Number Tab 1 is: "+AShoponKeurigN1url);
			String EShoponKeurigN1url = sheet2.getCell(1,31).getContents();
			System.out.println("Expected Shop Now on Keurig.com URL in Number Tab 1 is: "+EShoponKeurigN1url);
			if(EShoponKeurigN1url.equals(AShoponKeurigN1url))
			{
				System.out.println("Shop Now on Keurig.com URL in Number Tab 1 is Opened Correctly - Pass");
				Label ShoponKeurigN1NAVP = new Label(2,31,"PASS"); 
				wsheet2.addCell(ShoponKeurigN1NAVP); 
			}
			else
			{
				System.out.println("Shop Now on Keurig.com URL in Number Tab 1 is not Opened Correctly - Fail");
				bw.write("Shop Now on Keurig.com URL in Number Tab 1 is not Opened Correctly - Fail"); 
			    bw.newLine();
			    Label ShoponKeurigN1NAVF = new Label(2,31,"FAIL"); 
				wsheet2.addCell(ShoponKeurigN1NAVF);
			}
			driver.close();
			Thread.sleep(5000);
			driver.switchTo().window(winHandleBeforen1); // Home Window
			
			Thread.sleep(3000);
			JavascriptExecutor jsen2 = (JavascriptExecutor)driver;
			jsen2.executeScript("window.scrollBy(0,300)", "");
			Thread.sleep(8000);
			
			// Click Action on Number Tab 2
			
			driver.findElement(By.xpath(".//*[@id='id_2_tab']")).click();
			Thread.sleep(5000);
			
			// Number Tab 2 - Shop Now on Keurig.com Link in Variety Page 
			
			String winHandleBeforen2 = driver.getWindowHandle();
			driver.findElement(By.xpath(".//*[@id='id_2_body']/div[2]/div[2]/a")).click(); 
			Thread.sleep(5000);
			//Get the list of window handles
			ArrayList<String> tabsn2 = new ArrayList<String>(driver.getWindowHandles());
			System.out.println(tabsn2.size());
			//Use the list of window handles to switch between windows
		    driver.switchTo().window((String) tabsn2.get(1));
			Thread.sleep(10000);
			String AShoponKeurigN2url = driver.getCurrentUrl();
			System.out.println("Actual Shop Now on Keurig.com URL in Number Tab 2 is: "+AShoponKeurigN2url);
			String EShoponKeurigN2url = sheet2.getCell(1,32).getContents();
			System.out.println("Expected Shop Now on Keurig.com URL in Number Tab 2 is: "+EShoponKeurigN2url);
			if(EShoponKeurigN2url.equals(AShoponKeurigN2url))
			{
				System.out.println("Shop Now on Keurig.com URL in Number Tab 2 is Opened Correctly - Pass");
				Label ShoponKeurigN2NAVP = new Label(2,32,"PASS"); 
				wsheet2.addCell(ShoponKeurigN2NAVP); 
			}
			else
			{
				System.out.println("Shop Now on Keurig.com URL in Number Tab 2 is not Opened Correctly - Fail");
				bw.write("Shop Now on Keurig.com URL in Number Tab 2 is not Opened Correctly - Fail");  
			    bw.newLine();
			    Label ShoponKeurigN2NAVF = new Label(2,32,"FAIL"); 
				wsheet2.addCell(ShoponKeurigN2NAVF);
			}
			driver.close();
			Thread.sleep(5000);
			driver.switchTo().window(winHandleBeforen2); // Home Window
			
			// Click Action on Number Tab 3
			
			driver.findElement(By.xpath(".//*[@id='id_3_tab']")).click();
			Thread.sleep(5000);
			
			// Number Tab 3 - Shop Now on Keurig.com Link in Variety Page 
			
			String winHandleBeforen3 = driver.getWindowHandle();
			driver.findElement(By.xpath(".//*[@id='id_3_body']/div[2]/div[2]/a")).click(); 
			Thread.sleep(5000);
			//Get the list of window handles
			ArrayList<String> tabsn3 = new ArrayList<String>(driver.getWindowHandles());
			System.out.println(tabsn3.size());
			//Use the list of window handles to switch between windows
		    driver.switchTo().window((String) tabsn3.get(1));
			Thread.sleep(10000);
			String AShoponKeurigN3url = driver.getCurrentUrl();
			System.out.println("Actual Shop Now on Keurig.com URL in Number Tab 3 is: "+AShoponKeurigN3url);
			String EShoponKeurigN3url = sheet2.getCell(1,33).getContents();
			System.out.println("Expected Shop Now on Keurig.com URL in Number Tab 3 is: "+EShoponKeurigN3url);
			if(EShoponKeurigN3url.equals(AShoponKeurigN3url))
			{
				System.out.println("Shop Now on Keurig.com URL in Number Tab 3 is Opened Correctly - Pass");
				Label ShoponKeurigN3NAVP = new Label(2,33,"PASS"); 
				wsheet2.addCell(ShoponKeurigN3NAVP);
			}
			else
			{
				System.out.println("Shop Now on Keurig.com URL in Number Tab 3 is not Opened Correctly - Fail");
				bw.write("Shop Now on Keurig.com URL in Number Tab 3 is not Opened Correctly - Fail"); 
			    bw.newLine();
			    Label ShoponKeurigN3NAVF = new Label(2,33,"FAIL"); 
				wsheet2.addCell(ShoponKeurigN3NAVF);
			}
			driver.close();
			Thread.sleep(5000);
			driver.switchTo().window(winHandleBeforen3); // Home Window
			
			Thread.sleep(3000);
			JavascriptExecutor jsereg = (JavascriptExecutor)driver;
			jsereg.executeScript("window.scrollBy(0,1000)", "");
			Thread.sleep(8000);
			
			// Regular - Shop on Keurig.com Link in Variety Page 
			
			String winHandleBeforereg = driver.getWindowHandle();
			driver.findElement(By.xpath(".//*[@id='sk_id_varityPrd_table']/div[1]/div[2]/div[5]/a/div")).click(); 
			Thread.sleep(5000);
			//Get the list of window handles
			ArrayList<String> tabsreg = new ArrayList<String>(driver.getWindowHandles());
			System.out.println(tabsreg.size());
			//Use the list of window handles to switch between windows
		    driver.switchTo().window((String) tabsreg.get(1));
			Thread.sleep(10000);
			String AShoponKeurigRegurl = driver.getCurrentUrl();
			System.out.println("Actual Shop on Keurig.com URL in Regular is: "+AShoponKeurigRegurl);
			String EShoponKeurigRegurl = sheet2.getCell(1,34).getContents();
			System.out.println("Expected Shop on Keurig.com URL in Regular is: "+EShoponKeurigRegurl);
			if(EShoponKeurigRegurl.equals(AShoponKeurigRegurl))
			{
				System.out.println("Shop on Keurig.com URL in Regular is Opened Correctly - Pass");
				Label ShoponKeurigRegNAVP = new Label(2,34,"PASS"); 
				wsheet2.addCell(ShoponKeurigRegNAVP);
			}
			else
			{
				System.out.println("Shop on Keurig.com URL in Regular is not Opened Correctly - Fail");
				bw.write("Shop on Keurig.com URL in Regular is not Opened Correctly - Fail"); 
			    bw.newLine();
			    Label ShoponKeurigRegNAVF = new Label(2,34,"FAIL"); 
				wsheet2.addCell(ShoponKeurigRegNAVF);
			}
			driver.close();
			Thread.sleep(5000);
			driver.switchTo().window(winHandleBeforereg); // Home Window
			
			// Flavored - Shop on Keurig.com Link in Variety Page 
			
			String winHandleBeforefla = driver.getWindowHandle();
			driver.findElement(By.xpath(".//*[@id='sk_id_varityPrd_table']/div[3]/div[2]/div[5]/a/div")).click(); 
			Thread.sleep(5000);
			//Get the list of window handles
			ArrayList<String> tabsfla = new ArrayList<String>(driver.getWindowHandles());
			System.out.println(tabsfla.size());
			//Use the list of window handles to switch between windows
		    driver.switchTo().window((String) tabsfla.get(1));
			Thread.sleep(10000);
			String AShoponKeurigFlaurl = driver.getCurrentUrl();
			System.out.println("Actual Shop on Keurig.com URL in Flavored is: "+AShoponKeurigFlaurl);
			String EShoponKeurigFlaurl = sheet2.getCell(1,35).getContents();
			System.out.println("Expected Shop on Keurig.com URL in Flavored is: "+EShoponKeurigFlaurl);
			if(EShoponKeurigFlaurl.equals(AShoponKeurigFlaurl))
			{
				System.out.println("Shop on Keurig.com URL in Flavored is Opened Correctly - Pass");
				Label ShoponKeurigFlaNAVP = new Label(2,35,"PASS"); 
				wsheet2.addCell(ShoponKeurigFlaNAVP);
			}
			else
			{
				System.out.println("Shop on Keurig.com URL in Flavored is not Opened Correctly - Fail");
				bw.write("Shop on Keurig.com URL in Flavored is not Opened Correctly - Fail"); 
			    bw.newLine();
			    Label ShoponKeurigFlaNAVF = new Label(2,35,"FAIL"); 
				wsheet2.addCell(ShoponKeurigFlaNAVF);
			}
			driver.close();
			Thread.sleep(5000);
			driver.switchTo().window(winHandleBeforefla); // Home Window
			
			// Decaf - Shop on Keurig.com Link in Variety Page 
			
			String winHandleBeforeDec = driver.getWindowHandle();
			driver.findElement(By.xpath(".//*[@id='sk_id_varityPrd_table']/div[4]/div[2]/div[5]/a/div")).click(); 
			Thread.sleep(5000);
			//Get the list of window handles
			ArrayList<String> tabsDec = new ArrayList<String>(driver.getWindowHandles());
			System.out.println(tabsDec.size());
			//Use the list of window handles to switch between windows
		    driver.switchTo().window((String) tabsDec.get(1));
			Thread.sleep(10000);
			String AShoponKeurigDecurl = driver.getCurrentUrl();
			System.out.println("Actual Shop on Keurig.com URL in Decaf is: "+AShoponKeurigDecurl);
			String EShoponKeurigDecurl = sheet2.getCell(1,36).getContents();
			System.out.println("Expected Shop on Keurig.com URL in Decaf is: "+EShoponKeurigDecurl);
			if(EShoponKeurigDecurl.equals(AShoponKeurigDecurl))
			{
				System.out.println("Shop on Keurig.com URL in Decaf is Opened Correctly - Pass");
				Label ShoponKeurigDecNAVP = new Label(2,36,"PASS"); 
				wsheet2.addCell(ShoponKeurigDecNAVP);
			}
			else
			{
				System.out.println("Shop on Keurig.com URL in Decaf is not Opened Correctly - Fail");
				bw.write("Shop on Keurig.com URL in Decaf is not Opened Correctly - Fail"); 
			    bw.newLine();
			    Label ShoponKeurigDecNAVF = new Label(2,36,"FAIL"); 
				wsheet2.addCell(ShoponKeurigDecNAVF);
			}
			driver.close();
			Thread.sleep(5000);
			driver.switchTo().window(winHandleBeforeDec); // Home Window
			
			Thread.sleep(3000);
			JavascriptExecutor jsesea = (JavascriptExecutor)driver;
			jsesea.executeScript("window.scrollBy(0,1000)", "");
			Thread.sleep(8000);
			
			// Seasonal - Shop on Keurig.com Link in Variety Page 
			
			String winHandleBeforesea = driver.getWindowHandle();
			driver.findElement(By.xpath(".//*[@id='sk_id_varityPrd_table']/div[2]/div[2]/div[5]/a/div")).click(); 
			Thread.sleep(5000);
			//Get the list of window handles
			ArrayList<String> tabssea = new ArrayList<String>(driver.getWindowHandles());
			System.out.println(tabssea.size());
			//Use the list of window handles to switch between windows
		    driver.switchTo().window((String) tabssea.get(1));
			Thread.sleep(10000);
			String AShoponKeurigSeaurl = driver.getCurrentUrl();
			System.out.println("Actual Shop on Keurig.com URL in Seasonal is: "+AShoponKeurigSeaurl);
			String EShoponKeurigSeaurl = sheet2.getCell(1,37).getContents();
			System.out.println("Expected Shop on Keurig.com URL in Seasonal is: "+EShoponKeurigSeaurl);
			if(EShoponKeurigSeaurl.equals(AShoponKeurigSeaurl))
			{
				System.out.println("Shop on Keurig.com URL in Seasonal is Opened Correctly - Pass");
				Label ShoponKeurigSeaNAVP = new Label(2,37,"PASS"); 
				wsheet2.addCell(ShoponKeurigSeaNAVP);
			}
			else
			{
				System.out.println("Shop on Keurig.com URL in Seasonal is not Opened Correctly - Fail");
				bw.write("Shop on Keurig.com URL in Seasonal is not Opened Correctly - Fail"); 
			    bw.newLine();
			    Label ShoponKeurigSeaNAVF = new Label(2,37,"FAIL"); 
				wsheet2.addCell(ShoponKeurigSeaNAVF);
			}
			driver.close();
			Thread.sleep(5000);
			driver.switchTo().window(winHandleBeforesea); // Home Window
			
			Thread.sleep(3000);
			JavascriptExecutor jsecof = (JavascriptExecutor)driver;
			jsecof.executeScript("window.scrollBy(0,200)", "");
			Thread.sleep(8000);
			
			// CoffeeHouse - Shop on Keurig.com Link in Variety Page 
			
			String winHandleBeforecof = driver.getWindowHandle();
			driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[5]/div/div/div/div[2]/div[4]/div[3]/div[2]/div[5]/a/div")).click(); 
			Thread.sleep(5000);
			//Get the list of window handles
			ArrayList<String> tabscof = new ArrayList<String>(driver.getWindowHandles());
			System.out.println(tabscof.size());
			//Use the list of window handles to switch between windows
		    driver.switchTo().window((String) tabscof.get(1));
			Thread.sleep(10000);
			String AShoponKeurigCofurl = driver.getCurrentUrl();
			System.out.println("Actual Shop on Keurig.com URL in CoffeeHouse is: "+AShoponKeurigCofurl);
			String EShoponKeurigCofurl = sheet2.getCell(1,38).getContents();
			System.out.println("Expected Shop on Keurig.com URL in CoffeeHouse is: "+EShoponKeurigCofurl);
			if(EShoponKeurigCofurl.equals(AShoponKeurigCofurl))
			{
				System.out.println("Shop on Keurig.com URL in CoffeeHouse is Opened Correctly - Pass");
				Label ShoponKeurigCofNAVP = new Label(2,38,"PASS"); 
				wsheet2.addCell(ShoponKeurigCofNAVP);
			}
			else
			{
				System.out.println("Shop on Keurig.com URL in CoffeeHouse is not Opened Correctly - Fail");
				bw.write("Shop on Keurig.com URL in CoffeeHouse is not Opened Correctly - Fail"); 
			    bw.newLine();
			    Label ShoponKeurigCofNAVF = new Label(2,38,"FAIL"); 
				wsheet2.addCell(ShoponKeurigCofNAVF);
			}
			driver.close();
			Thread.sleep(5000);
			driver.switchTo().window(winHandleBeforecof); // Home Window
			
			// Organic - Shop on Keurig.com Link in Variety Page 
			
			String winHandleBeforeorg = driver.getWindowHandle();
			driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[5]/div/div/div/div[2]/div[4]/div[4]/div[2]/div[5]/a/div")).click(); 
			Thread.sleep(5000);
			//Get the list of window handles
			ArrayList<String> tabsorg = new ArrayList<String>(driver.getWindowHandles());
			System.out.println(tabsorg.size());
			//Use the list of window handles to switch between windows
		    driver.switchTo().window((String) tabsorg.get(1));
			Thread.sleep(10000);
			String AShoponKeurigOrgurl = driver.getCurrentUrl();
			System.out.println("Actual Shop on Keurig.com URL in Organic is: "+AShoponKeurigOrgurl);
			String EShoponKeurigOrgurl = sheet2.getCell(1,39).getContents();
			System.out.println("Expected Shop on Keurig.com URL in Organic is: "+EShoponKeurigOrgurl);
			if(EShoponKeurigOrgurl.equals(AShoponKeurigOrgurl))
			{
				System.out.println("Shop on Keurig.com URL in Organic is Opened Correctly - Pass");
				Label ShoponKeurigOrgNAVP = new Label(2,39,"PASS"); 
				wsheet2.addCell(ShoponKeurigOrgNAVP);
			}
			else
			{
				System.out.println("Shop on Keurig.com URL in Organic is not Opened Correctly - Fail");
				bw.write("Shop on Keurig.com URL in Organic is not Opened Correctly - Fail"); 
			    bw.newLine();
			    Label ShoponKeurigOrgNAVF = new Label(2,39,"FAIL"); 
				wsheet2.addCell(ShoponKeurigOrgNAVF);
			}
			driver.close();
			Thread.sleep(5000);
			driver.switchTo().window(winHandleBeforeorg); // Home Window
			
			Thread.sleep(3000);
			JavascriptExecutor jseft = (JavascriptExecutor)driver;
			jseft.executeScript("window.scrollBy(0,230)", "");
			Thread.sleep(8000);
			
			// Click Action on Sustainability Link in Footer
			
			driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div[6]/div/div/div[2]/div/div[2]/div[1]/div/a[4]/div")).click(); 
			Thread.sleep(15000);
			
			bw.close();
			
	    	}
	    	
	    	catch(Exception e)
			{
				System.out.println("Exception Is "+e);
				 
			}
			
	    }
		
		public static void SustainabilityPageNavigation() throws InterruptedException, IOException, BiffException, RowsExceededException, WriteException {
			
			try{
				
			book = Workbook.getWorkbook(newFile);
			//sheet5=book.getSheet("SustainabilityPageCSS");
			sheet2=book.getSheet("ExpectedURL'S");
			//workbook = Workbook.getWorkbook(new File("D:\\Viyantha\\Automation\\Selenium Test Report\\KEURIG\\Keurig_FileInput.xls"));
			//workbookcopy = Workbook.createWorkbook(new File("D:\\Viyantha\\Automation\\Selenium Test Report\\KEURIG\\Keurig_FileOutput_" + idForTxtFile + ".xls"), workbook);
			wsheet2 = workbookcopy.getSheet(1);
			//wsheet5 = workbookcopy.getSheet(4);
			
			 // Write text on  text file
			
			FileWriter fw = new FileWriter(file, true);  
			BufferedWriter bw = new BufferedWriter(fw);
			
			Thread.sleep(3000);
			JavascriptExecutor jsespdesc = (JavascriptExecutor)driver;
			jsespdesc.executeScript("window.scrollBy(0,400)", "");
				
			Thread.sleep(3000);
			JavascriptExecutor jsespdesc1 = (JavascriptExecutor)driver;
			jsespdesc1.executeScript("window.scrollBy(0,500)", "");
			
			Thread.sleep(3000);
			JavascriptExecutor jsespbetter = (JavascriptExecutor)driver;
			jsespbetter.executeScript("window.scrollBy(0,700)", "");
	
			Thread.sleep(3000);
			JavascriptExecutor jsespft = (JavascriptExecutor)driver;
			jsespft.executeScript("window.scrollBy(0,400)", "");
			Thread.sleep(3000);
			
			// Sustainability Page - Income - READ THE SUSTAINABILITY REPORT - Click
			
			String winHandleBeforeIR = driver.getWindowHandle();
			driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_0']/div[2]/div[1]/a")).click(); 
			Thread.sleep(5000);
			//Get the list of window handles
			ArrayList<String> tabsIR = new ArrayList<String>(driver.getWindowHandles());
			System.out.println(tabsIR.size());
			//Use the list of window handles to switch between windows
		    driver.switchTo().window((String) tabsIR.get(1));
			Thread.sleep(10000);
			String AIReadurl = driver.getCurrentUrl();
			System.out.println("Actual Income - READ THE SUSTAINABILITY REPORT URL is: "+AIReadurl);
			String EIReadurl = sheet2.getCell(1,42).getContents();
			System.out.println("Expected Income - READ THE SUSTAINABILITY REPORT URL is: "+EIReadurl);
			if(EIReadurl.equals(AIReadurl))
			{
				System.out.println("Income - READ THE SUSTAINABILITY REPORT Page is Opened Correctly - Pass");
				Label IncomeReadNAVP = new Label(2,42,"PASS"); 
				wsheet2.addCell(IncomeReadNAVP); 
			}
			else
			{
				System.out.println("Income - READ THE SUSTAINABILITY REPORT Page is not Opened Correctly - Fail");
				bw.write("Income - READ THE SUSTAINABILITY REPORT Page is not Opened Correctly - Fail"); 
			    bw.newLine();
			    Label IncomeReadNAVF = new Label(2,42,"FAIL"); 
				wsheet2.addCell(IncomeReadNAVF); 
			}
			driver.close();
			Thread.sleep(5000);
			driver.switchTo().window(winHandleBeforeIR); // Home Window
			
			// Sustainability Page - Income - LEARN ABOUT FOOD SUPPORT - Click
			
			driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_0']/div[2]/div[2]/span[1]")).click(); 
			Thread.sleep(10000);
			String AILearnurl = driver.getCurrentUrl();
			System.out.println("Actual Income - LEARN ABOUT FOOD SUPPORT URL is: "+AILearnurl);
			String EILearnurl = sheet2.getCell(1,43).getContents();
			System.out.println("Expected Income - LEARN ABOUT FOOD SUPPORT URL is: "+EILearnurl);
			if(EILearnurl.equals(AILearnurl))
			{
				System.out.println("Income - LEARN ABOUT FOOD SUPPORT Page is Opened Correctly - Pass");
				Label IncomeLearnNAVP = new Label(2,43,"PASS"); 
				wsheet2.addCell(IncomeLearnNAVP); 
			}
			else
			{
				System.out.println("Income - LEARN ABOUT FOOD SUPPORT is not Opened Correctly - Fail");
				bw.write("Income - LEARN ABOUT FOOD SUPPORT is not Opened Correctly - Fail"); 
			    bw.newLine();
			    Label IncomeLearnNAVF = new Label(2,43,"FAIL"); 
				wsheet2.addCell(IncomeLearnNAVF); 
			}
			
			// FOOD TAB
			
			Thread.sleep(3000);
			JavascriptExecutor jsespfooddesc = (JavascriptExecutor)driver;
			jsespfooddesc.executeScript("window.scrollBy(0,400)", "");
			
			Thread.sleep(3000);
			JavascriptExecutor jsespfoodwhat = (JavascriptExecutor)driver;
			jsespfoodwhat.executeScript("window.scrollBy(0,500)", "");
			
			Thread.sleep(3000);
			JavascriptExecutor jsespfoodhow = (JavascriptExecutor)driver;
			jsespfoodhow.executeScript("window.scrollBy(0,900)", "");
			Thread.sleep(3000);
	
			// Sustainability Page - FOOD - READ THE SUSTAINABILITY REPORT - Click
			
			String winHandleBeforeFR = driver.getWindowHandle();
			driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_1']/div[2]/div[1]/a")).click(); 
			Thread.sleep(5000);
			//Get the list of window handles
			ArrayList<String> tabsFR = new ArrayList<String>(driver.getWindowHandles());
			System.out.println(tabsFR.size());
			//Use the list of window handles to switch between windows
		    driver.switchTo().window((String) tabsFR.get(1));
			Thread.sleep(10000);
			String AFReadurl = driver.getCurrentUrl();
			System.out.println("Actual FOOD - READ THE SUSTAINABILITY REPORT URL is: "+AFReadurl);
			String EFReadurl = sheet2.getCell(1,45).getContents();
			System.out.println("Expected FOOD - READ THE SUSTAINABILITY REPORT URL is: "+EFReadurl);
			if(EFReadurl.equals(AFReadurl))
			{
				System.out.println("FOOD - READ THE SUSTAINABILITY REPORT Page is Opened Correctly - Pass");
				Label FoodReadNAVP = new Label(2,45,"PASS"); 
				wsheet2.addCell(FoodReadNAVP); 
			}
			else
			{
				System.out.println("FOOD - READ THE SUSTAINABILITY REPORT Page is not Opened Correctly - Fail");
				bw.write("FOOD - READ THE SUSTAINABILITY REPORT Page is not Opened Correctly - Fail"); 
			    bw.newLine();
			    Label FoodReadNAVF = new Label(2,45,"FAIL"); 
				wsheet2.addCell(FoodReadNAVF); 
			}
			driver.close();
			Thread.sleep(5000);
			driver.switchTo().window(winHandleBeforeFR); // Home Window
			
			// Sustainability Page - FOOD - Learn about Water Access - Click
			
			driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_1']/div[2]/div[2]/span[1]")).click(); 
			Thread.sleep(10000);
			String AFLearnurl = driver.getCurrentUrl();
			System.out.println("Actual FOOD - Learn about Water Access URL is: "+AFLearnurl);
			String EFLearnurl = sheet2.getCell(1,46).getContents();
			System.out.println("Expected FOOD - Learn about Water Access URL is: "+EFLearnurl);
			if(EFLearnurl.equals(AFLearnurl))
			{
				System.out.println("FOOD - Learn about Water Access Page is Opened Correctly - Pass");
				Label FoodLearnNAVP = new Label(2,46,"PASS"); 
				wsheet2.addCell(FoodLearnNAVP); 
			}
			else
			{
				System.out.println("FOOD - Learn about Water Access is not Opened Correctly - Fail");
				bw.write("FOOD - Learn about Water Access is not Opened Correctly - Fail"); 
			    bw.newLine();
			    Label FoodLearnNAVF = new Label(2,46,"FAIL"); 
				wsheet2.addCell(FoodLearnNAVF); 
			}
			
			
			// WATER TAB
			
			Thread.sleep(3000);
			JavascriptExecutor jsespWaterdesc = (JavascriptExecutor)driver;
			jsespWaterdesc.executeScript("window.scrollBy(0,400)", "");
			
			Thread.sleep(3000);
			JavascriptExecutor jsespsolving = (JavascriptExecutor)driver;
			jsespsolving.executeScript("window.scrollBy(0,400)", "");
		
			Thread.sleep(3000);
			JavascriptExecutor jsespimpact = (JavascriptExecutor)driver;
			jsespimpact.executeScript("window.scrollBy(0,500)", "");
			
			Thread.sleep(3000);
			JavascriptExecutor jsespimpactD = (JavascriptExecutor)driver;
			jsespimpactD.executeScript("window.scrollBy(0,600)", "");
			Thread.sleep(3000);
			
			// Sustainability Page - WATER - READ THE SUSTAINABILITY REPORT - Click
			
			String winHandleBeforeWR = driver.getWindowHandle();
			driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_2']/div[2]/div[1]/a")).click(); 
			Thread.sleep(5000);
			//Get the list of window handles
			ArrayList<String> tabsWR = new ArrayList<String>(driver.getWindowHandles());
			System.out.println(tabsWR.size());
			//Use the list of window handles to switch between windows
		    driver.switchTo().window((String) tabsWR.get(1));
			Thread.sleep(10000);
			String AWReadurl = driver.getCurrentUrl();
			System.out.println("Actual WATER - READ THE SUSTAINABILITY REPORT URL is: "+AWReadurl);
			String EWReadurl = sheet2.getCell(1,48).getContents();
			System.out.println("Expected WATER - READ THE SUSTAINABILITY REPORT URL is: "+EWReadurl);
			if(EWReadurl.equals(AWReadurl))
			{
				System.out.println("WATER - READ THE SUSTAINABILITY REPORT Page is Opened Correctly - Pass");
				Label WaterReadNAVP = new Label(2,48,"PASS"); 
				wsheet2.addCell(WaterReadNAVP); 
			}
			else
			{
				System.out.println("WATER - READ THE SUSTAINABILITY REPORT Page is not Opened Correctly - Fail");
				bw.write("WATER - READ THE SUSTAINABILITY REPORT Page is not Opened Correctly - Fail"); 
			    bw.newLine();
			    Label WaterReadNAVF = new Label(2,48,"FAIL"); 
				wsheet2.addCell(WaterReadNAVF); 
			}
			driver.close();
			Thread.sleep(5000);
			driver.switchTo().window(winHandleBeforeWR); // Home Window
			
			// Sustainability Page - WATER - Learn about Income Stability - Click
			
			driver.findElement(By.xpath(".//*[@id='id_sksbw_menuSection_2']/div[2]/div[2]/span[1]")).click(); 
			Thread.sleep(10000);
			String AWLearnurl = driver.getCurrentUrl();
			System.out.println("Actual WATER - Learn about Income Stability URL is: "+AWLearnurl);
			String EWLearnurl = sheet2.getCell(1,49).getContents();
			System.out.println("Expected WATER - Learn about Income Stability URL is: "+EWLearnurl);
			if(EWLearnurl.equals(AWLearnurl))
			{
				System.out.println("WATER - Learn about Income Stability Page is Opened Correctly - Pass");
				Label WaterLearnNAVP = new Label(2,49,"PASS"); 
				wsheet2.addCell(WaterLearnNAVP); 
			}
			else
			{
				System.out.println("WATER - Learn about Income Stability is not Opened Correctly - Fail");
				bw.write("WATER - Learn about Income Stability is not Opened Correctly - Fail"); 
			    bw.newLine();
			    Label WaterLearnNAVF = new Label(2,49,"FAIL"); 
				wsheet2.addCell(WaterLearnNAVF); 
			}
			
			Thread.sleep(3000);
			workbookcopy.write();
			workbookcopy.close();
			workbook.close();
			bw.close();

		driver.close();
		driver.quit();
		
			}
			
			catch(Exception e)
			{
				System.out.println("Exception Is "+e);
				 
			}
				
 }
 
	 
}
